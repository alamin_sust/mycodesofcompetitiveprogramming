/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node{
ll y1,y2;
};

node arr[100010];

ll comp(node a,node b)
{
    if(a.y1==b.y1)
    return a.y2<b.y2;
    return a.y1<b.y1;
}

ll m,c,n,x1,x2,i,mxy1,mxy2;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %I64d",&n);
    scanf(" %I64d %I64d",&x1,&x2);

    for(i=0LL;i<n;i++)
    {
        scanf(" %I64d %I64d",&m,&c);
        arr[i].y1=m*x1+c;
        arr[i].y2=m*x2+c;
    }
    sort(arr,arr+n,comp);
    mxy1=arr[0].y1;
    mxy2=arr[0].y2;
    for(i=1LL;i<n;i++)
    {
        if(arr[i].y2<mxy2)
        {
            printf("YES\n");
            return 0;
        }
        mxy2=arr[i].y2;
    }
    printf("NO\n");
    return 0;
}


