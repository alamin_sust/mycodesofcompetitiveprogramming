/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll n,M=1000000007;
vector<ll>v;
map<vector<ll>,bool>mpp;

ll rec(vector<ll> vec) {
    ll ret=0,sz=vec.size();

    for(int i=0;i<vec.size();i++) {
        printf("%lld,",vec[i]);
    }
    printf("\n");

    for(int i=0;i<sz-1;i++) {
        if(vec[i]&&vec[i+1]) {
            vec[i]--;
            vec[i+1]--;
            if((i+2)<sz) {
                vec[i+2]++;
            } else {
                vec.push_back(1);
            }
            if(!mpp[vec])
            {
                mpp[vec]=true;
                ret+=(1+rec(vec))%M;
            }
            vec[i]++;
            vec[i+1]++;
            if((i+2)<sz) {
                vec[i+2]--;
            } else {
                vec.pop_back();
            }
        }
    }

    return ret;
}

int main()
{
    ll t,p,i,in;
    scanf(" %lld",&t);
    for(p=1;p<=t;p++) {
        scanf(" %lld",&n);
        v.clear();
        mpp.clear();
        for(i=0;i<n;i++) {
            scanf(" %lld",&in);
            v.push_back(in);
        }
        mpp[v]=true;
        printf("%lld\n",(1+rec(v))%M);
    }
    return 0;
}
