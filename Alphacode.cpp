/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll dp[5010],l;
char arr[5010];

ll rec(ll pos)
{
    if(pos<l&&arr[pos]=='0')
        return 0;
    if(pos==l)
        return 1;
    ll &ret=dp[pos];
    if(ret!=-1)
        return ret;
    ret=0;
    if((pos+1)<l&&((arr[pos]-'0')*10+arr[pos+1]-'0')<=26)
        ret+=rec(pos+2);
    ret+=rec(pos+1);
    return ret;
}

main()
{
    while(scanf("%s",&arr))
    {
        if(arr[0]=='0')
            break;
        l=strlen(arr);
        memset(dp,-1,sizeof(dp));
        printf("%lld\n",rec(0));
    }
    return 0;
}

