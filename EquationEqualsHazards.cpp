/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))
#define MAX 1000010LL
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll t,p,a,b,k;

ll _gcd(ll a,ll b)
{
    if(b==0)
        return a;
    return _gcd(b,a%b);
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    //sieve_();

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++)
    {
        scanf(" %lld %lld",&a,&b);
        //printf("gcd: %lld\n",_gcd(a,b));
        if(a==b||b==1LL)
            printf("No\n");
        else if(a<=2LL)
            printf("Yes\n");
        else
        {
            k=1000010LL;
            while(k--)
            {
                if(((a*k)%b)==1LL)
                {k=-100LL;
                break;}

            }
            if(k==-100LL)
                printf("Yes\n");
            else
                printf("No\n");
        }
    }

    return 0;
}
/*
10
1 3
7 18
1 1
1 4
4 1
1123 1234
23 42341
4 553
21 35
43 5236
*/
