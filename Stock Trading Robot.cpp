#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};
int d,n,a,b,shares,i,arr[100010];
main()
{
    cin>>n>>d>>a>>b;
    for(i=1;i<=n;i++)
    {
        scanf(" %d",&arr[i]);
    }
    int up=0,down=0,shares=0;
    for(i=2;i<=n;i++)
    {
        if(arr[i]>arr[i-1])
            {up++;
            int price_buyable=up*a*arr[i];
            int share_buyable1=up*a;
            int share_buyable2=d/arr[i];
            int money_expended=min(share_buyable1,share_buyable2)*arr[i];
            d-=money_expended;
            shares+=min(share_buyable1,share_buyable2);
            down=0;}
        else if(arr[i]<arr[i-1])
            {down++;
            int price_sellable=down*b*arr[i];
            int share_sellable1=down*b;
            int share_sellable2=shares;
            int money_gained=min(share_sellable1,share_sellable2)*arr[i];
            d+=money_gained;
            shares-=min(share_sellable1,share_sellable2);
            up=0;}
        else
        up=down=0;
    }
    printf("%d %d\n",d,shares);
    return 0;
}
