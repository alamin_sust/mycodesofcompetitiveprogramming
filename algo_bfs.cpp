#include<stdio.h>
#include<queue>
#include<vector>
#include<string.h>
#include<iostream>
using namespace std;

vector<int>adj[1010];
queue<int>q;
int status[10010],val[10010],nodes,edges;

void bfs(int f)
{
    memset(status,0,sizeof(status));
    q.push(f);
    status[f]=1;
    val[f]=0;
    while(!q.empty())
    {
        int u=q.front();
        q.pop();
        for(int i=0;i<adj[u].size();i++)
        {
            int v=adj[u][i];
            if(status[v]==0)
            {
                val[v]=val[u]+1;
                status[v]=1;
                q.push(v);
            }
        }
    }
    return;
}

main()
{
    int i,t,f;
    cin>>nodes>>edges;
    for(i=1;i<=edges;i++)
    {
        cin>>f>>t;
        adj[f].push_back(t);
        adj[t].push_back(f);
    }
    cin>>f;
    bfs(f);
    for(i=1;i<=nodes;i++)
        printf("%d ",val[i]);
    return 0;
}
