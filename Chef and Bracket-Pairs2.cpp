/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

map<string,ll>mpp;
ll n,M=1000000007LL,cnt=0LL;
string arr[110];

ll rec(ll pos,string dp,stack<string>stk)
{
    cout<<dp<<endl;
    //cnt++;
    //if((cnt%100LL)==0LL)
    //    printf("%lld..\n",cnt);
    if(pos>n)
    {
        if(stk.empty())
        {
            //cout<<dp<<endl;
            return 1LL;}
        return 0LL;
    }

    ll &ret=mpp[dp];
    if(ret!=0LL)
        return ret;
    ret=M;
    if(arr[pos][0]=='-')
    {
    //    printf("..");
        stk.push(arr[pos]);
        ret=(ret+rec(pos+1,dp+arr[pos],stk))%M+M;
        stk.pop();
    }
    else if((!stk.empty())&&('-'+arr[pos])==stk.top())
    {
      //  printf("...");
        stk.pop();
        for(int i=dp.size();i>=0;i--)
        {
            if(dp[i]=='-')
                {dp[i]='\0';
                break;}
            dp[i]='\0';
        }

        ret=(ret+rec(pos+1,dp,stk))%M+M;
        stk.push('-'+arr[pos]);
        dp+='-';
        dp+=arr[pos];
    }
    //cout<<arr[pos]<<" "<<('-'+q.front())<<endl;
    ret=(ret+rec(pos+1,dp+'#',stk))%M+M;
    return ret;
}


int main()
{
    cin>>n;
    for(int i=1;i<=n;i++)
        cin>>arr[i];
    stack<string>stk;
    while(!stk.empty())
        stk.pop();
    cout<<(rec(1LL,"",stk)%M)<<endl;
    return 0;
}

