#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
#define ull long long unsigned
using namespace std;

ull res[6][6],mat[50][6][6];

void calc(ull power)
{
    ull temp[6][6];
    for(ull i=1; i<=4; i++)
    {
        for(ull j=1; j<=4; j++)
        {
            temp[i][j]=res[i][j];
        }
    }
    for(ull i=1; i<=4; i++)
    {
        for(ull j=1; j<=4; j++)
        {
            res[i][j]=0;
            for(ull k=1; k<=4; k++)
            {
                res[i][j]+=(temp[i][k]*mat[power][k][j])%10007;

            }
            res[i][j]%=10007;
        }
    }
    return;
}

void bigmod(ull power)
{
    for(ull i=1; i<=4; i++)
    {
        for(ull j=1; j<=4; j++)
        {
            mat[power][i][j]=0;
            for(ull k=1; k<=4; k++)
            {
                mat[power][i][j]+=(mat[power-1][i][k]*mat[power-1][k][j])%10007;
                //mat[power][i][j]%=100000;
            }
            mat[power][i][j]%=10007;
        }
    }
    return;
}
/*
void prnt(void)
{
    int i,j;
    for(i=1;i<=4;i++)
        {for(j=1;j<=4;j++)
        printf("%llu ",mat[1][i][j]);
       printf("\n");}
}
*/
main()
{
    ull t,p,a,b,n,m,c;
    cin>>t;
    //for(ull i=1;i<=2;i++)
    //  for(ull j=1;j<=2;j++)
    //printf("%llu ",mat[10][i][j]);
    for(p=1; p<=t; p++)
    {
        cin>>n>>a>>b>>c;
        mat[0][1][1]=a;
        mat[0][1][2]=0;
        mat[0][1][3]=b;
        mat[0][1][4]=1;
        mat[0][2][1]=1;
        mat[0][2][2]=0;
        mat[0][2][3]=0;
        mat[0][2][4]=0;
        mat[0][3][1]=0;
        mat[0][3][2]=1;
        mat[0][3][3]=0;
        mat[0][3][4]=0;
        mat[0][4][1]=0;
        mat[0][4][2]=0;
        mat[0][4][3]=0;
        mat[0][4][4]=1;
        for(ull i=1; i<=32; i++)
            bigmod(i);
        //prnt();
        if(n<=2)
        {
            printf("Case %llu: 0\n",p);
            continue;
        }
        n-=2;
        //printf("%llu\n",MOD);
        ull k=0;
        ull fg=0;
        while((n>>k)|0)
        {
            //printf(".");
            if(((n>>k)&1)==1)
            {
                if(fg==0)
                {
                    fg=1;
                    for(ull i=1; i<=4; i++)
                        for(ull j=1; j<=4; j++)
                            res[i][j]=mat[k][i][j];
                }
                else
                    calc(k);
            }
            k++;
        }
        ull result=(res[1][4]*c)%10007;
        printf("Case %llu: %llu\n",p,result);
    }
    return 0;
}

