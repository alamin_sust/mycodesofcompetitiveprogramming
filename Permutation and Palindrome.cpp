/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

vector<int> v[28];
int t,p,l,odd,flag[28],i,fs,j;
char arr[100010];

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);


    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {

        scanf(" %s",&arr);

        memset(flag,0,sizeof(flag));

        l=strlen(arr);

        for(i=0; i<26; i++)
        {
            v[i].clear();
        }

        for(i=0; i<l; i++)
        {
            flag[arr[i]-'a']++;
            v[arr[i]-'a'].push_back(i);
        }

        odd=-2;

        for(i=0; i<26; i++)
        {

            if(flag[i]%2)
            {
                if(odd!=-2)
                {
                    odd=-1;
                    break;
                }
                else odd = i;
            }
        }

        if(odd==-1) printf("-1\n");
        else
        {
            fs=0;
            for(i=0; i<26; i++)
            {
                for(j=0; j<v[i].size()/2; j++)
                {
                    if(fs) printf(" ");
                    printf("%d",v[i][j]+1);
                    fs=1;
                }
                if(v[i].size()%2) {odd=v[i][j];}
            }
            if(odd>=0)
            {
                printf(" %d",odd+1);
            }
            for(i=25; i>=0; i--)
            {
                for(j=(v[i].size()+1)/2; j<v[i].size(); j++)
                {
                    printf(" ");
                    printf("%d",v[i][j]+1);
                }
            }
            printf("\n");
        }

    }

    return 0;
}
