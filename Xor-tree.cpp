#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

vector<int>adj[100010];
int res[100010],k=0,stat[100010],val[100010],terg[100010];

void rec(int node,int p,int pp)
{
    stat[node]=1;
    for(int i=0;i<adj[node].size();i++)
    {
        int x=adj[node][i];
        if(stat[x]==1)
            continue;
        int y=pp%2;
        //printf("%d\n",x);
        if((y==0&&val[x]!=terg[x])||(y==1&&val[x]==terg[x]))
        {
            res[k++]=x;
            rec(x,pp+1,p);
        }
        else
            rec(x,pp,p);
    }
    return;
}

main()
{
    int i,n,f,t;
    cin>>n;
    for(i=1;i<n;i++)
    {
       scanf(" %d %d",&f,&t);
       adj[f].push_back(t);
       adj[t].push_back(f);
    }
    for(i=1;i<=n;i++)
    {
       scanf(" %d",&val[i]);
    }
    for(i=1;i<=n;i++)
    {
       scanf(" %d",&terg[i]);
    }
    adj[0].push_back(1);
    rec(0,0,0);
    printf("%d\n",k);
    for(i=0;i<k;i++)
    {
        printf("%d\n",res[i]);
    }
    return 0;
}
