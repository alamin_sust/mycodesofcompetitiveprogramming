/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll xx[6],yy[6],x[6],y[6],a[6],b[6],A[6];

ll dist_(ll f,ll t)
{
    return ((xx[f]-xx[t])*(xx[f]-xx[t]))+((yy[f]-yy[t])*(yy[f]-yy[t]));
}

void gen(ll c,ll i,ll X,ll Y,ll A,ll B)
{
    if(c==0)
    {
        xx[i]=X;
        yy[i]=Y;
        return;
    }
    xx[i]=X-A;
    yy[i]=Y-B;
    if(xx[i]>=0&&yy[i]>=0)
    {
        swap(xx[i],yy[i]);
        xx[i]*=(-1);
    }
    else if(xx[i]<=0&&yy[i]>=0)
    {
        swap(xx[i],yy[i]);
        xx[i]*=(-1);
        yy[i]*=(-1);
        yy[i]*=(-1);
    }
    else if(xx[i]<=0&&yy[i]<=0)
    {
        swap(xx[i],yy[i]);
        xx[i]*=(-1);
    }
    else if(xx[i]>=0&&yy[i]<=0)
    {
        swap(xx[i],yy[i]);
        xx[i]*=(-1);
        yy[i]*=(-1);
        yy[i]*=(-1);
    }
    xx[i]+=A;
    yy[i]+=B;
    gen(c-1,i,xx[i],yy[i],A,B);
    return;
}

main()
{
    ll n,p,i,j,k,l,res;
    cin>>n;
    for(p=1; p<=n; p++)
    {
        scanf(" %I64d %I64d %I64d %I64d",&x[1],&y[1],&a[1],&b[1]);
        scanf(" %I64d %I64d %I64d %I64d",&x[2],&y[2],&a[2],&b[2]);
        scanf(" %I64d %I64d %I64d %I64d",&x[3],&y[3],&a[3],&b[3]);
        scanf(" %I64d %I64d %I64d %I64d",&x[4],&y[4],&a[4],&b[4]);
        res=99999999;
        //a[1]=0;
        //b[1]=1;
        //gen(4,1,2,2,0,1);
        //printf("%d %d\n",xx[1],yy[1]);
        for(i=0; i<4; i++)
        {
            for(j=0; j<4; j++)
            {
                for(k=0; k<4; k++)
                {
                    for(l=0; l<4; l++)
                    {
                        gen(i,1,x[1],y[1],a[1],b[1]);
                        gen(j,2,x[2],y[2],a[2],b[2]);
                        gen(k,3,x[3],y[3],a[3],b[3]);
                        gen(l,4,x[4],y[4],a[4],b[4]);

                        A[0]=dist_(1,2);
                        A[1]=dist_(2,3);
                        A[2]=dist_(3,4);
                        A[3]=dist_(4,1);
                        A[4]=dist_(2,4);
                        A[5]=dist_(1,3);

                        sort(A,A+6);

                        if(A[0]>0&&A[0]==A[3]&&A[4]==A[5])
                        {
                            res=min(res,i+j+k+l);
                        }
                    }
                }
            }
        }
        if(res==99999999)
            printf("-1\n");
        else
            printf("%I64d\n",res);
    }

    return 0;
}

