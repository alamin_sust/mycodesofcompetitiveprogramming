#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int i,j,k,row,col,ones,l,res,val[110][110];
char arr[110][110];

main()
{
    while(cin>>row)
    {
        if(row==0)
            break;
        cin>>col;
        getchar();
        for(i=1;i<=row;i++)
        {
            for(k=0,j=1;j<=col;j++)
            {
                scanf("%c",&arr[i][j]);
                k+=arr[i][j]-'0';
                val[i][j]=val[i-1][j]+k;

            }
            getchar();
        }
        res=0;
        for(i=1;i<=row;i++)
        {
            for(j=1;j<=col;j++)
            {
                for(k=i;k<=row;k++)
                {
                    for(l=j;l<=col;l++)
                    {
                        ones=val[k][l]+val[i-1][j-1]-val[i-1][l]-val[k][j-1];
                        if((k-i+1)*(l-j+1)==ones)
                        {
                            res++;
                        }
                    }
                }
            }
        }
        printf("%d\n",res);
    }
    return 0;
}
