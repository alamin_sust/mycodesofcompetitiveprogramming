/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.0000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node
{
    double col;
    int x,y,z;
};

node tri[1010];
int dp[1010][1010][7],n;

int comp(node a,node b)
{
    return a.col<b.col;
}

int rec(int pos,int prev,int side)
{
    if(pos>n)
        return 0;
    int &ret=dp[pos][prev][side];
    if(ret!=-1)
        return ret;
    ret=0;
    if(prev==0)
        ret=max(rec(pos+1,pos,0)+1,rec(pos+1,0,0));
    else
    {
            if((tri[prev].col+eps)<tri[pos].col&&side!=1&&tri[prev].x==tri[pos].x)
                ret=max(ret,max(rec(pos+1,prev,side),rec(pos+1,pos,1)+1));
            else ret=max(ret,rec(pos+1,prev,side));
            if((tri[prev].col+eps)<tri[pos].col&&side!=1&&tri[prev].x==tri[pos].y)
                ret=max(ret,max(rec(pos+1,prev,side),rec(pos+1,pos,2)+1));
            else ret=max(ret,rec(pos+1,prev,side));
            if((tri[prev].col+eps)<tri[pos].col&&side!=1&&tri[prev].x==tri[pos].z)
                ret=max(ret,max(rec(pos+1,prev,side),rec(pos+1,pos,3)+1));
            else ret=max(ret,rec(pos+1,prev,side));
            if((tri[prev].col+eps)<tri[pos].col&&side!=2&&tri[prev].y==tri[pos].x)
                ret=max(ret,max(rec(pos+1,prev,side),rec(pos+1,pos,1)+1));
            else ret=max(ret,rec(pos+1,prev,side));
            if((tri[prev].col+eps)<tri[pos].col&&side!=2&&tri[prev].y==tri[pos].y)
                ret=max(ret,max(rec(pos+1,prev,side),rec(pos+1,pos,2)+1));
            else ret=max(ret,rec(pos+1,prev,side));
            if((tri[prev].col+eps)<tri[pos].col&&side!=2&&tri[prev].y==tri[pos].z)
                ret=max(ret,max(rec(pos+1,prev,side),rec(pos+1,pos,3)+1));
            else ret=max(ret,rec(pos+1,prev,side));
            if((tri[prev].col+eps)<tri[pos].col&&side!=3&&tri[prev].z==tri[pos].x)
                ret=max(ret,max(rec(pos+1,prev,side),rec(pos+1,pos,1)+1));
            else ret=max(ret,rec(pos+1,prev,side));
            if((tri[prev].col+eps)<tri[pos].col&&side!=3&&tri[prev].z==tri[pos].y)
                ret=max(ret,max(rec(pos+1,prev,side),rec(pos+1,pos,2)+1));
            else ret=max(ret,rec(pos+1,prev,side));
            if((tri[prev].col+eps)<tri[pos].col&&side!=3&&tri[prev].z==tri[pos].z)
                ret=max(ret,max(rec(pos+1,prev,side),rec(pos+1,pos,3)+1));
            else ret=max(ret,rec(pos+1,prev,side));
    }
    return ret;
}


int main()
{

    while(cin>>n)
    {for(int i=1; i<=n; i++)
    {
        scanf(" %d %d %d %lf",&tri[i].x,&tri[i].y,&tri[i].z,&tri[i].col);
    }
    sort(tri+1,tri+n+1,comp);
   // for(int i=1;i<=n;i++)
    //{
      //  printf("%lf %.10lf\n",tri[i].col,tri[i].col+eps);
    //}
    memset(dp,-1,sizeof(dp));
    printf("%d\n",rec(1,0,0));}
    return 0;
}

