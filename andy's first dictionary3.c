#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#define MAX 8001
using namespace std;

int comp(const void *a, const void *b)
{
    return strcmp((char *)a,(char *)b);
}

int main(){
   char str[2000],*p;
   char word[MAX][1010];
   char dil[] = " .:,\"'";
   int k = 0,i,j;
   while(scanf("%s",str) == 1){
      p = strtok(str,dil);
      if(p)
         strcpy(word[k++],p);

      while(p){
         p = strtok(NULL,dil);
         if(p)
            strcpy(word[k++],p);
      }
   }
   for(i = 0; i<k; i++){
      for(j = 0;word[i][j]; j++){
         word[i][j] = tolower(word[i][j]);
      }
   }
   qsort(word,k,sizeof(word[0]),comp);
   for(i = 1; i < k+1; i++){
      if(strcmp(word[i],word[i-1]))
         printf("%s\n",word[i-1]);
   }
   return 0;
}
