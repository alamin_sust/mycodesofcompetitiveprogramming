#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class MinimalTriangle
{
public:
	double maximalArea(int length)
	{
	    double ret=(double)length;
	    double c=sqrt(ret*ret*2.0-2.0*ret*ret*cos(120.0*2.0*acos(0)/180.0));
	    double s=(ret+ret+c)/2.0;
	    return sqrt(s*(s-c)*(s-ret)*(s-ret));
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	MinimalTriangle objectMinimalTriangle;

	//test case0
	int param00 = 5;
	double ret0 = objectMinimalTriangle.maximalArea(param00);
	double need0 = 10.825317547305485;
	assert_eq(0,ret0,need0);

	//test case1
	int param10 = 10;
	double ret1 = objectMinimalTriangle.maximalArea(param10);
	double need1 = 43.30127018922194;
	assert_eq(1,ret1,need1);

	//test case2
	int param20 = 100000;
	double ret2 = objectMinimalTriangle.maximalArea(param20);
	double need2 = 4.330127018922194E9;
	assert_eq(2,ret2,need2);
}
