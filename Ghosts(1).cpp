/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int i,j,k,l,a,b,c,d,res;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    while(cin>>a>>b>>c>>d)
    {
        res=0;
        for(int i=1; i<=a; i++)
        {
            for(int j=1; j<=b; j++)
            {
                for(int k=1; k<=c; k++)
                {
                    for(int l=1; l<=d; l++)
                    {
                        if((abs(i-j)%3)==0&&((j+k)%5)==0&&((i*k)%4)==0&&__gcd(i,l)==1)
                            res++;

                    }
                }
            }
        }
        printf("%d\n",res);
    }

    return 0;
}


