#include<stdio.h>

int coin,stack[110],desired,arr[110],n,res,curr,maxcoin;

void backtrack(int in,int ans_in)
{
    if(ans_in>n||(ans_in>=maxcoin&&curr==desired))
    return;
    if(in>n)
    {
        res=stack[ans_in-1];
        if(res>=desired&&curr>=res)
        {
            curr=res;
            if(ans_in<=maxcoin)
            maxcoin=ans_in-1;
        }
        return;
    }
    backtrack(in+1,ans_in+1);
    backtrack(in+1,ans_in);
}

main()
{
   int temp,t,i,j;
   scanf(" %d",&t);
   for(i=1;i<=t;i++)
   {
       curr=999999;
       maxcoin=999999;
       scanf(" %d",&desired);
       scanf(" %d",&n);
       for(temp=0,j=1;j<=n;j++)
       {scanf(" %d",&arr[j]);
       temp+=arr[j];
       stack[j]=temp;}
       backtrack(1,1);
       printf("%d %d\n",curr,maxcoin);
   }
   return 0;
}
