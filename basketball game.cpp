#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int t,p,i,N,tme[35],M,P,flag[35],tm,j,tm2,out,n,in;
char res[55][35][24];
struct node
{
    int shot,height;
    char name[24];
};

node arr[55][35];

int comp(node a,node b)
{
    return a.shot>b.shot;
}

int comp2(node a,node b)
{
    if(a.shot==b.shot)
        return a.height>b.height;
    else
        return 0;
}

main()
{
    FILE *read,*write;
    read=fopen("read35.txt","r");
    write=fopen("write35.txt","w");
    fscanf(read," %d",&t);
    for(p=1; p<=t; p++)
    {
        memset(flag,0,sizeof(flag));
        memset(tme,0,sizeof(tme));
        fscanf(read," %d %d %d",&N,&M,&P);
        for(i=1; i<=N; i++)
        {
            fscanf(read," %s %d %d",&arr[p][i].name,&arr[p][i].shot,&arr[p][i].height);
        }
        sort(arr[p]+1,arr[p]+1+N,comp);
        sort(arr[p]+1,arr[p]+1+N,comp2);
        for(i=1; i<=(2*P); i++)
        {
            flag[i]=1;
        }
        for(i=1; i<=M; i++)
        {
            tm=-1;
            for(j=N; j>=1; j-=2)
            {
                if(flag[j]==1)
                {
                    tme[j]++;
                    if(tm<tme[j])
                    {
                        out=j;
                        tm=tme[j];
                    }
                }
            }
            tm2=999999;
            j=1;
            if(N%2==0)
                j++;
            for(; j<=N; j+=2)
            {
                if(flag[j]==0&&tm2>tme[j])
                {
                    in=j;
                    tm2=tme[j];
                }
            }
            if(tm2!=999999&&tm!=-1)
            {flag[out]=0;
            flag[in]=1;}
            tm=-1;
            for(j=N-1; j>=1; j-=2)
            {
                if(flag[j]==1)
                {
                    tme[j]++;
                    if(tm<tme[j])
                    {
                        out=j;
                        tm=tme[j];
                    }
                }
            }
            tm2=999999;
            j=1;
            if(N%2==1)
                j++;
            for(; j<=N; j+=2)
            {
                if(flag[j]==0&&tm2>tme[j])
                {
                    in=j;
                    tm2=tme[j];
                }
            }
            if(tm2!=999999&&tm!=-1)
            {flag[out]=0;
            flag[in]=1;}
        }
        for(i=1,j=1;j<=N;j++)
        {
            if(flag[j]==1)
            {
                strcpy(res[p][i++],arr[p][j].name);
            }
        }
        n=i-1;
        for(i=1;i<=n;i++)
        {
            for(j=2;j<=n;j++)
            {
                if(strcmp(res[p][j],res[p][j-1])<0)
                {
                    swap(res[p][j],res[p][j-1]);
                }
            }
        }
        fprintf(write,"Case #%d:",p);
        for(i=1;i<=n;i++)
            fprintf(write," %s",res[p][i]);
        fprintf(write,"\n");
    }
    return 0;
}

