/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

main()
{
    ll t,res,p,MOD=1000000007LL;
    char arr[100010];
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %s",&arr);
        ll now=1LL;
        ll l=strlen(arr);
        for(ll i=0;i<l;i++)
        {
            if((i%2)==0) // odd level
            {
                if(arr[i]=='l')
                {
                    now=(now*2)%MOD;
                }
                else
                    now=(now*2+2)%MOD;
            }
            else
            {
                if(arr[i]=='l')
                    now=(now*2-1)%MOD;
                else
                    now=(now*2+1)%MOD;
            }
        }
        printf("%lld\n",now);
    }
    return 0;
}


