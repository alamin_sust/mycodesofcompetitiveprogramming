#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll n,m,i,j,k,path[110][110],a,b,xx,l,yy;
double tp,res,eps,adj[110][110],kk,x[10010],y[10010];

main()
{
    eps=1.0/100000000.0;
    while(cin>>n>>m)
    {
        if(n==0&&m==0)
            break;
        memset(path,0,sizeof(path));
        for(i=1; i<=n; i++)
        {
            for(j=1; j<=n; j++)
                if(i!=j)
                    adj[i][j]=999999999999.0;
                else
                    adj[i][j]=0.0;
        }
        for(i=1; i<=n; i++)
        {
            scanf(" %lf %lf",&x[i],&y[i]);
        }
        for(i=1; i<=m; i++)
        {
            scanf(" %lld %lld",&a,&b);
            path[a][b]=path[b][a]=1;
            adj[a][b]=adj[b][a]=dist(x[a],y[a],x[b],y[b]);
        }
        for(k=1; k<=n; k++)
        {
            for(i=1; i<=n; i++)
            {
                for(j=1; j<=n; j++)
                {
                    if(adj[i][j]>adj[i][k]+adj[k][j]+eps)
                        adj[i][j]=adj[i][k]+adj[k][j];
                }
            }
        }
        res=0.0;
        for(i=1; i<=n; i++)
        {
            for(j=i+1; j<=n; j++)
            {
                tp=dist(x[i],y[i],x[j],y[j]);
                kk=0.0;
                for(k=1;k<=n;k++)
                {
                    for(l=k+1;l<=n;l++)
                    {
                        kk+=max(0.0,adj[k][l]-min(adj[k][i]+tp+adj[j][l],adj[k][j]+tp+adj[i][l]));
                    }
                }
                if(res<kk)
                {
                    res=kk;
                    xx=i;
                    yy=j;
                    //printf("..%lld %lld %lf %lf\n",xx,yy,adj[i][j],tp);
                }
            }
        }
        if(res<=1.0+eps)
            printf("No road required\n");
        else
            printf("%lld %lld\n",xx,yy);
    }
    return 0;
}

