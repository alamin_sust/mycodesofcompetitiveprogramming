#include<iostream>
using namespace std;

int calc(int *input, int n, int *sum)
{

    int result=sum[0];
    for(int i=0; i<n; i++)
    {
        int min = sum[0];
        for(int j=0; j<=i; j++)
        {
            int temp = sum[j]-input[i];
            if(temp>=0&&temp<min)
                min = temp;
        }
        sum[i+1]=min;
        if(min<result)
            result = min;
    }
    return result;
}

main()
{

    int m, n;
    cin>>m;
    for(int j=0; j<m; j++)
    {
        cin>>n;
        if(n==0)
        {
            cout<<"0"<<endl;
            continue;
        }
        int *input = new int[n+1];
        int *sum = new int[n+2];
        sum[0]=0;
        for(int i=0; i<n; i++)
        {
            cin>>input[i];
            sum[0]+=input[i];
            input[i]*=2;
            sum[i+1]=99999999;
        }
        cout<<calc(input, n, sum)<<endl;

        delete input;
        delete sum;
    }
    return 0;
}
