#include<stdio.h>

int ontree[100010*4], upd[100010*4], cnt;

void update(int ind, int s, int e, int us, int ue);
void query(int ind, int s, int e, int qs, int qe);
void pushup(int ind, int s, int e);
void pushdown(int ind, int s, int e);

int main()
{
    int n, m, i, j, k;
    while(scanf("%d %d", &n, &m)!=EOF)
    {
        j=n*4;
        for(i=1;i<=j;i++)
            ontree[i]=0, upd[i]=0;
        while(m--)
        {
            scanf("%d", &i);
            if(i)
            {
                cnt=0;
                scanf("%d %d", &j, &k);
                query(1, 1, n, j, k);
                printf("%d\n", cnt);
            }
            else
            {
                scanf("%d %d", &j, &k);
                update(1, 1, n, j, k);
            }
        }
    }
    return 0;
}

void update(int ind, int s, int e, int us, int ue)
{
    if(upd[ind])
        pushdown(ind, s, e);
    if(e<us||s>ue)
        return ;
    if(s>=us&&e<=ue)
    {
        upd[ind]+=1;
        pushdown(ind, s, e);
        return ;
    }
    int mid=(s+e)/2;
    update(ind*2, s, mid, us, ue);
    update(ind*2+1, mid+1, e, us, ue);
    pushup(ind, s, e);
    return ;
}

void pushdown(int ind, int s, int e)
{
    upd[ind]%=2;
    if(upd[ind])
    {
        ontree[ind]=(e-s+1)-ontree[ind];
        if(s!=e)
        {
            upd[ind*2]+=upd[ind];
            upd[ind*2+1]+=upd[ind];
        }
        upd[ind]=0;
    }
    return ;
}

void pushup(int ind, int s, int e)
{
    ontree[ind]=ontree[ind*2]+ontree[ind*2+1];
    return ;
}

void query(int ind, int s, int e, int qs, int qe)
{
    if(upd[ind])
        pushdown(ind, s, e);
    if(s>qe||e<qs)
        return ;
    if(s>=qs&&e<=qe)
    {
        cnt+=ontree[ind];
        return ;
    }
    int mid=(s+e)/2;
    query(ind*2, s, mid, qs, qe);
    query(ind*2+1, mid+1, e, qs,  qe);
    return ;
}
