#include<iostream>
#include<cstdio>
#include<cmath>
using namespace std;
float x,y,c,s,func,U,D,e,f;
int main()
{
    while(scanf("%f%f%f",&x,&y,&c)==3)
    {
        if(x==y&&c==0.00000)
        {
            s = sqrt(x*x-4*c*c);
            printf("%.3f\n",s);
        }
        else if(c<0.00001)
        {
            printf("0.000\n");
        }
        else
        {
            if(x>y)
            U = x;
            else
            U = y;
            D = 0.0;
            s = (U+D)/2.0;
            e = sqrt(x*x-s*s);
            f = sqrt(y*y-s*s);
            func = (1.0/c)-(1.0/e)-(1.0/f);
            while(abs(func)>0.00000)
            {
                if(func>0.00000)
                D = s;
                else
                U = s;
                s = (U+D)/2.0;
                e = sqrt(x*x-s*s);
                f = sqrt(y*y-s*s);
                func = (1.0/c)-(1.0/e)-(1.0/f);
            }
            printf("%.3f\n",s);
        }
    }
    return 0;
}
