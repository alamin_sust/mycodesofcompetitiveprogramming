/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int mn1,mn2,mn3,mx1,mx2,mx3,i,res1,res2,n;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    cin>>n;
    cin>>mn1>>mx1;
    cin>>mn2>>mx2;
    cin>>mn3>>mx3;

    for(i=mx1;i>=mn1;i--)
    {
        if((i+mn2+mn3)<=n)
            {res1=i;
            break;}
    }
    n-=res1;
    for(i=mx2;i>=mn2;i--)
    {
        if((i+mn3)<=n)
            {res2=i;
            break;}
    }
    printf("%d %d %d\n",res1,res2,n-res2);

    return 0;
}


