/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

pair<char,int>pii;

pair<char,int>arr[100010],res[1000010];
char ch[20];
int i,n;


priority_queue<int>pq;

int M=1000000000;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);


    scanf(" %d",&n);
    int cnt=0;
    for(i=0;i<n;i++)
    {
        scanf(" %s",ch);
        arr[i].second=0;
        if(ch[0]!='r')
             scanf(" %d",&arr[i].second);

        arr[i].first=ch[0];
    }

    for(i=0;i<n;)
    {
        pii.first=arr[i].first;
        pii.second=arr[i].second;
        if(pii.first=='i')
        {
            pq.push(M-pii.second);
            res[cnt++]=pii;
            i++;
        }
        else if(pii.first=='r')
        {
            if(pq.empty())
            {
                pq.push(M-1);
                res[cnt++]=make_pair('i',1);
            }
            else
            {
               res[cnt++]=pii;
               pq.pop();
               i++;
            }
        }
        else
        {
            if(pq.empty())
            {
                pq.push(M-pii.second);
                res[cnt++]=make_pair('i',pii.second);
            }
            else if((M-pq.top())==pii.second)
            {
                res[cnt++]=pii;
                i++;
            }
            else if((M-pq.top())<pii.second)
            {
                pq.pop();
                res[cnt++]=make_pair('r',0);
            }
            else if((M-pq.top())>pii.second)
            {
                pq.push(M-pii.second);
                res[cnt++]=make_pair('i',pii.second);
            }
        }
    }

    printf("%d\n",cnt);
    for(i=0;i<cnt;i++)
    {
        if(res[i].first=='i')
        {
            printf("insert %d\n",res[i].second);
        }
        else if(res[i].first=='r')
        {
            printf("removeMin\n");
        }
        else
        {
            printf("getMin %d\n",res[i].second);
        }
    }

    return 0;
}

