/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
int rr[]= {1,0,1,-1,-1,0,1,-1};
int cc[]= {1,1,0,-1,0,-1,-1,1};
using namespace std;

int arr[8][8],ax,ay,bx,by;

void color(int x, int y)
{
    int i,j;
    for(i=0; i<8; i++)
    {
        if(i!=y) arr[x][i]++;
    }
    for(i=0; i<8; i++)
    {
        if(i!=x) arr[i][y]++;
    }
    for(i=x+1,j=y+1; i<8&&j<8; i++,j++)
    {
        arr[i][j]++;
    }
    for(i=x+1,j=y-1; i<8&&j>=0; i++,j--)
    {
        arr[i][j]++;
    }
    for(i=x-1,j=y-1; i>=0&&j>=0; i--,j--)
    {
        arr[i][j]++;
    }
    for(i=x-1,j=y+1; i>=0&&j<8; i--,j++)
    {
        arr[i][j]++;
    }
    return;
}

bool chk()
{
    if(arr[ax][ay]==0||arr[bx][by]==0) return false;
    for(int i=0; i<8; i++)
    {
        if(ax+rr[i]>=0 && ax+rr[i]<8 && ay+cc[i]>=0 && ay+cc[i]<8&&arr[ax+rr[i]][ay+cc[i]]==0) return false;
    }
    for(int i=0; i<8; i++)
    {
        if(bx+rr[i]>=0 && bx+rr[i]<8 && by+cc[i]>=0 && by+cc[i]<8&&arr[bx+rr[i]][by+cc[i]]==0) return false;
    }
    return true;
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    int i,j,t,p,I,J,K,L,M,N,k,l,m,n;

    scanf(" %d",&t);

    for(p=0; p<t; p++)
    {
        scanf(" %d %d %d %d",&ax,&ay,&bx,&by);
        int fg=0;

//        for(k=0; k<8&&fg==0; k++)
//        {
//            for(l=0; l<8&&fg==0; l++)
//            {
//                for(m=0; m<8&&fg==0; m++)
//                {
//                    for(n=0; n<8 &&fg==0; n++)
//                    {
//                        memset(arr,0,sizeof(arr));
//
//                        color(k,l);
//                        color(m,n);
//
////                                for(int ii=0;ii<8;ii++)
////                                {
////
////                                    for(int jj=0;jj<8;jj++) {
////                                        printf(" %d %d\n",arr[ii][jj]);
////                                    }
////                                }
////                                getchar();
//
//                        if(chk())
//                        {
//                            //printf("...");
//                            fg=1;
//
//                            K=k;
//                            L=l;
//                            M=m;
//                            N=n;
//                        }
//                    }
//                }
//            }
//        }
//
//        if(fg) {
//            printf("Q %d %d\n",K,L);
//        printf("Q %d %d\n",M,N);
//        continue;
//
//        }


        for(i=0; i<8&&fg==0; i++)
        {
            for(j=0; j<8&&fg==0; j++)
            {
                for(k=0; k<8&&fg==0; k++)
                {
                    for(l=0; l<8&&fg==0; l++)
                    {
                        for(m=0; m<8&&fg==0; m++)
                        {
                            for(n=0; n<8 &&fg==0; n++)
                            {
                                memset(arr,0,sizeof(arr));
                                color(i,j);
                                color(k,l);
                                color(m,n);

//                                for(int ii=0;ii<8;ii++)
//                                {
//
//                                    for(int jj=0;jj<8;jj++) {
//                                        printf(" %d %d\n",arr[ii][jj]);
//                                    }
//                                }
//                                getchar();

                                if(chk())
                                {
                                    //printf("...");
                                    fg=1;
                                    I=i;
                                    J=j;
                                    K=k;
                                    L=l;
                                    M=m;
                                    N=n;
                                }
                            }
                        }
                    }
                }
            }
        }
        printf("Q %d %d\n",I,J);
        printf("Q %d %d\n",K,L);
        printf("Q %d %d\n",M,N);
    }

    return 0;
}

