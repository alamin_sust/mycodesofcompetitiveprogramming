#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};
double adj[110][110],res;
int par[110];

struct node2{
int x,y;
double val;
};

struct node1{
double x,y;
};

node1 arr[100010];
node2 lst[100010];

int comp(node2 a,node2 b)
{
    return a.val<b.val;
}

void rst(int i,int j)
{
    adj[i][j]=adj[j][i]=sqrt( ((arr[i].x-arr[j].x)*(arr[i].x-arr[j].x)) + ((arr[i].y-arr[j].y)*(arr[i].y-arr[j].y)) );
    return;
}

main()
{
    int t,i,j,k,p,n;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
                adj[i][j]=inf;
            scanf(" %lf %lf",&arr[i].x,&arr[i].y);
        }
        for(i=1;i<=n;i++)
        {
            for(j=i+1;j<=n;j++)
                rst(i,j);
        }
        for(k=0,i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
            {
                lst[k].val=adj[i][j];
                lst[k].x=i;
                lst[k].y=j;
                k++;
            }
        }
        sort(lst,lst+k,comp);
        for(i=1;i<=n;i++)
            par[i]=i;
        for(res=0.0,j=1,i=0;j<n&&i<k;i++)
        {
            int u=lst[i].x;
            int v=lst[i].y;
            while(par[v]!=v)
                v=par[v];
            while(par[u]!=u)
                u=par[u];
            if(v!=u)
            {par[v]=u;
            res+=lst[i].val;
            j++;}
        }
        printf("%.2lf\n",res);
        if(p!=t)
        printf("\n");
    }
    return 0;
}

