#include <iostream>
#include <stdio.h>
#include <math.h>
using namespace std;
main ()
{
    float x1, x2, x3, y1, y2, y3;
    float c, d, e, h, k, r;
    while(scanf("%f%f%f%f%f%f",&x1,&y1,&x2,&y2,&x3,&y3)==6)
    {
        c=((x2*x2+y2*y2-x3*x3-y3*y3)*(y1-y2) - (x1*x1 + y1*y1 - x2*x2 - y2*y2)*(y2-y3))/((x1-x2)*(y2-y3)-(x2-x3)*(y1-y2));
        if(y3-y1!=0)
            d=(x1*x1+y1*y1-x3*x3-y3*y3-c*(x3-x1))/(y3-y1);
        else
            d=(x1*x1+y1*y1-x2*x2-y2*y2-c*(x2-x1))/(y2-y1);
        e=-1*(x1*x1+y1*y1+c*x1+d*y1);
        h=c/(-2.0);
        k=d/(-2.0);
        r=sqrt(h*h+k*k-e);
        printf("(x %c %.3f)^2 + (y %c %.3f)^2 = %.3f^2\n",(h<0)?'+':'-', (h<0)?(-h):h, (k<0)?'+':'-',(k<0)?(-k):k,r);
        printf("x^2 + y^2 %c %.3fx %c %.3fy %c %.3f = 0\n\n",(c<0)?'-':'+',(c<0)?(-c):c, (d<0)?'-':'+',(d<0)?(-d):d, (e<=0)?'-':'+', (e<0)?(-e):e);
    }
    return 0;
}

