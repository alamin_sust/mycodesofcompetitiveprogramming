#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

stack<char>stk1,stk2;
char arr1[100010],arr2[100010];
int hand,p,t,i,j;

void pop1(int val)
{
    char fg='0';
    while(!stk1.empty())
    {
        if(stk1.top()=='(')
        {
            stk1.pop();
            if((!stk1.empty())&&stk1.top()=='!')
            {
                hand=!hand;
                stk1.pop();
            }
            break;
        }
        else if(stk1.top()=='|')
        {
            stk1.pop();
            fg='|';
        }
        else if(stk1.top()=='&')
        {
            stk1.pop();
            fg='&';
        }
        else if(stk1.top()!='0'&&stk1.top()!='1')
        {
            char tp=stk1.top();
            int h;
            stk1.pop();
            h=(val>>(tp-'a'))&1;
            if((!stk1.empty())&&stk1.top()=='!')
            {
                stk1.pop();
                h=!h;
            }
            if(fg=='0')
                hand=h;
            else
            {
                if(fg=='|')
                    hand|=h;
                else
                    hand&=h;
                fg='0';
            }
        }
        else if(stk1.top()=='0')
        {
            if(fg=='|')
                hand|=0;
            else if(fg=='&')
                hand&=0;
            else hand=0;
            fg='0';
            stk1.pop();
        }
        else if(stk1.top()=='1')
        {
            if(fg=='|')
                hand|=1;
            else if(fg=='&')
                hand&=1;
            else hand=1;
            fg='0';
            stk1.pop();
        }
    }
    //printf("%d<-\n",hand);
    stk1.push(hand+'0');
    return;
}

void pop2(int val)
{
    char fg='0';
    while(!stk2.empty())
    {
        if(stk2.top()=='(')
        {
            stk2.pop();
      //      printf("%diiiii\n",hand);
            if((!stk2.empty())&&stk2.top()=='!')
            {
               // printf("%diiiii\n",hand);
                hand=!hand;
                stk2.pop();
            }
            break;
        }
        else if(stk2.top()=='|')
        {
            stk2.pop();
            fg='|';
        }
        else if(stk2.top()=='&')
        {
            stk2.pop();
            fg='&';
        }
        else if(stk2.top()!='0'&&stk2.top()!='1')
        {
            char tp=stk2.top();
            int h;
            stk2.pop();
            h=(val>>(tp-'a'))&1;
            if((!stk2.empty())&&stk2.top()=='!')
            {
                stk2.pop();
                h=!h;
            }
            if(fg=='0')
                hand=h;
            else
            {
                if(fg=='|')
                    hand|=h;
                else
                    hand&=h;
                fg='0';
            }
        }
        else if(stk2.top()=='0')
        {
            if(fg=='|')
                hand|=0;
            else if(fg=='&')
                hand&=0;
            else hand=0;
            fg='0';
            stk2.pop();
        }
        else if(stk2.top()=='1')
        {
            if(fg=='|')
                hand|=1;
            else if(fg=='&')
                hand&=1;
            else hand=1;
            fg='0';
            stk2.pop();
        }
    }
    //printf("%d->\n",hand);
    stk2.push(hand+'0');
    return;
}

main()
{
    cin>>t;
    getchar();
    for(p=1; p<=t; p++)
    {
        int flag=0;
        gets(arr1);
        gets(arr2);
        for(i=0; i<1024; i++)
        {

            for(j=0; j<strlen(arr1); j++)
            {
                if(arr1[j]==')')
                    hand=1,pop1(i);
                else
                {
                    if(arr1[j]=='!'&&(j+1)<strlen(arr1)&&arr1[j+1]=='!')
                        j++;
                    else
                    stk1.push(arr1[j]);
                }
            }
            if(!stk1.empty())
            hand=1,pop1(i);
            for(j=0; j<strlen(arr2); j++)
            {
                if(arr2[j]==')')
                    hand=1,pop2(i);
                else
                {
                    if(arr2[j]=='!'&&(j+1)<strlen(arr2)&&arr2[j+1]=='!')
                        j++;
                    else
                    stk2.push(arr2[j]);
                }
            }
            if(!stk2.empty())
            hand=1,pop2(i);
            if(stk1.top()!=stk2.top())
            {
                stk1.pop();
                stk2.pop();
                flag=1;
                break;
            }
            stk1.pop();
            stk2.pop();
        }
     //   printf("%dttt\n",i);
        if(flag==0)
            printf("Case %d: Equivalent\n",p);
        else
            printf("Case %d: Not Equivalent\n",p);
    }
    return 0;
}
/*
5
a|b
!(!a&!b)
!(!a|!b)
a&b
a|b|c
a|(b&c)
(a|!a)|(c&!c)
!(b&!b)
a|(b&c)
(a&b)|(a&c)
*/
