/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node
{
    int val,x,y;
};
node flag[10010];
queue<node>q;
node det,det2;
int color[10][10],RET,row,col,pos[6],a[6],b[6];

int stat[8][8],pp=0LL;

/*int bfs(int iter,int x,int y)
{
    pp++;
    int ret=0;
    det.x=x;
    det.y=y;
    //memset(stat,0,sizeof(stat));
    stat[x][y]=pp;
    q.push(det);
    while(!q.empty())
    {
        det=q.front();
        q.pop();
        for(int i=iter; i<=4; i++)
        {
            if(det.x==flag[pos[i]].x&&det.y==flag[pos[i]].y)
                ret++;
        }
        if((det.x+1)<row)
        if(color[det.x+1][det.y]==0&&stat[det.x+1][det.y]!=pp)
        {
            det2.x=det.x+1;
            det2.y=det.y;
            stat[det2.x][det2.y]=pp;
            q.push(det2);
        }
        if((det.y+1)<col)
        if(color[det.x][det.y+1]==0&&stat[det.x][det.y+1]!=pp)
        {
            det2.x=det.x;
            det2.y=det.y+1;
            stat[det2.x][det2.y]=pp;
            q.push(det2);
        }
        if((det.x>0))
        if(color[det.x-1][det.y]==0&&stat[det.x-1][det.y]!=pp)
        {
            det2.x=det.x-1;
            det2.y=det.y;
            stat[det2.x][det2.y]=pp;
            q.push(det2);
        }
        if((det.y>0))
        if(color[det.x][det.y-1]==0&&stat[det.x][det.y-1]!=pp)
        {
            det2.x=det.x;
            det2.y=det.y-1;
            stat[det2.x][det2.y]=pp;
            q.push(det2);
        }
    }
    //printf("%d\n",ret);
    return ret;
}*/

int dfs(int x,int y,int r)
{
    int ret=1;
    stat[x][y]=pp;
    if((x+1)<row&&stat[x+1][y]!=pp&&color[x+1][y]==0)
        ret+=dfs(x+1,y,ret);
    if((y+1)<col&&stat[x][y+1]!=pp&&color[x][y+1]==0)
        ret+=dfs(x,y+1,ret);
    if(x>0&&stat[x-1][y]!=pp&&color[x-1][y]==0)
        ret+=dfs(x-1,y,ret);
    if(y>0&&stat[x][y-1]!=pp&&color[x][y-1]==0)
        ret+=dfs(x,y-1,ret);
    return ret;
}

int rec(int x,int y,int num,int iter)
{
    //printf("...%d\n",num);
    if(flag[num].val==1&&(x!=flag[num].x||y!=flag[num].y))
        return 0;
    if(abs(x-flag[pos[iter]].x)+abs(y-flag[pos[iter]].y)>(pos[iter]-num))
        return 0;

    if(num==pos[4])
    {
        if(x==0&&y==1)
            return 1;
        return 0;
    }
    if(flag[num].val==0)
    {
        for(int i=iter; i<=4; i++)
        {
            if((x==flag[pos[i]].x&&y==flag[pos[i]].y))
                return 0;
        }
    }
    pp++;
    //printf("%d................\n",dfs(0,1,0));
    if(dfs(0,1,0)!=pos[4]-num+1)
        return 0;
    int ret=0;
    color[x][y]=1;
    if((y+1)<col&&color[x][y+1]==0)
        ret+=rec(x,y+1,num+1,iter+flag[num].val);
    if((x+1)<row&&color[x+1][y]==0)
        ret+=rec(x+1,y,num+1,iter+flag[num].val);
    if(y>0&&color[x][y-1]==0)
        ret+=rec(x,y-1,num+1,iter+flag[num].val);
    if(x>0&&color[x-1][y]==0)
        ret+=rec(x-1,y,num+1,iter+flag[num].val);
    color[x][y]=0;
    return ret;
}

int main()
{
    int x,y,p=1;
    while(1)
    {
        scanf(" %d %d",&row,&col);
        if(row==0&&col==0)
            break;

        pos[0]=1;

        scanf(" %d %d",&x,&y);
        flag[(row*col)/4].val=1;
        flag[(row*col)/4].x=x;
        flag[(row*col)/4].y=y;
        pos[1]=(row*col)/4;

        scanf(" %d %d",&x,&y);
        flag[(row*col)/2].val=1;
        flag[(row*col)/2].x=x;
        flag[(row*col)/2].y=y;
        pos[2]=(row*col)/2;

        scanf(" %d %d",&x,&y);
        flag[3*(row*col)/4].val=1;
        flag[3*(row*col)/4].x=x;
        flag[3*(row*col)/4].y=y;
        pos[3]=3*(row*col)/4;

        flag[(row*col)].val=1;
        flag[(row*col)].x=0;
        flag[(row*col)].y=1;
        pos[4]=(row*col);

        a[1]=(pos[1]-pos[0]);
        a[2]=(pos[2]-pos[1]);
        a[3]=(pos[3]-pos[2]);
        a[4]=(pos[4]-pos[3]);

        b[1]=(flag[pos[1]].x+flag[pos[1]].y);
        b[2]=abs(flag[pos[2]].x-flag[pos[1]].x)+abs(flag[pos[2]].y-flag[pos[1]].y);
        b[3]=abs(flag[pos[3]].x-flag[pos[2]].x)+abs(flag[pos[3]].y-flag[pos[2]].y);
        b[4]=abs(flag[pos[4]].x-flag[pos[3]].x)+abs(flag[pos[4]].y-flag[pos[3]].y);

        if(a[1]<b[1]||a[2]<b[2]||a[3]<b[3]||a[4]<b[4]||(a[1]%2)!=(b[1]%2)||(a[2]%2)!=(b[2]%2)||(a[3]%2)!=(b[3]%2)||(a[4]%2)!=(b[4]%2))
            printf("Case %d: 0\n",p++);
        else
            printf("Case %d: %d\n",p++,rec(0,0,1,1));

        flag[(row*col)/4].val=0;
        flag[(row*col)/2].val=0;
        flag[3*(row*col)/4].val=0;
        flag[row*col].val=0;
    }
    return 0;
}

