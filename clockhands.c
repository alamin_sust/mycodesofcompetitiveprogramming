#include<stdio.h>
char time1[4];

int hour,minute,mutex,ex;
float deg;

int Digit(int *d)
{
    int digit,g=*d,b10=0;
    while(isdigit(time1[g]))
    {
        b10*=10;
        digit=time1[*d]-48+b10;
        b10=digit;
        g++;
        *d=g;
    }
    return digit;
}

int main()
{

    while(1)
    {
        int i=0;
        scanf("%s",time1);

        for(i=0; i<4; i++)
        {
            if(!mutex)
            {
                if(isdigit(time1[i]))
                {
                    hour=Digit(&i);
                    mutex=1;
                }
            }
            else
            {
                minute=Digit(&i);
                mutex=0;
            }
        }

        if(!hour && !minute)
            break;
        if(hour>12)
            hour=0;
        else
            hour=12-hour;

        deg=((hour*30)+minute*6+minute/60-(.5*minute));
        if(deg<0)
            deg*=-1;
        if(deg>180.0)
        {
            deg=360.0-deg;
            deg*=-1;

        }
        if(deg>180.0)
            deg=360.0-deg;
        if(deg<0.0)
            deg=-deg;
        printf("%.3f\n",deg);
    }
    return 0;
}
