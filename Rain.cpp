/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int rr[]={0,0,1,-1};
int cc[]={-1,1,0,0};

struct node{
int x,y,val;
};

node det,u,v;
queue<node>q;
int arr[55][55];
int r,c,col[55][55];

int fc(int x,int y,int rain)
{
    memset(col,0,sizeof(col));
    while(!q.empty())
        q.pop();
    det.x=x;
    det.y=y;
    det.val=arr[x][y]+rain;
    q.push(det);
    col[x][y]=1;
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        if(u.x==1||u.x==r||u.y==1||u.y==c)
                    return 0;
        for(int i=0;i<4;i++)
        {
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            v.val=u.val;
            if(col[v.x][v.y]==0&&v.val>arr[v.x][v.y])
            {
                if(v.x==1||v.x==r||v.y==1||v.y==c)
                    return 0;
                q.push(v);
            }
            col[v.x][v.y]=1;
        }
    }
    return 1;
}

int main()
{
    freopen("B-largea.in","r",stdin);
    freopen("B-largeaOut.txt","w",stdout);

    int t,p,i,j,res,low,high,mid,ans;

    scanf(" %d",&t);


    for(p=1;p<=t;p++)
    {
        scanf(" %d %d",&r,&c);
        for(i=1;i<=r;i++)
        {
            for(j=1;j<=c;j++)
            {
                scanf(" %d",&arr[i][j]);
            }
        }
        res=0;
        for(i=1;i<=r;i++)
        {
            for(j=1;j<=c;j++)
            {
                low=0;
                high=1005;
                ans=0;
                while(low<=high)
                {
                    mid=(low+high)/2;
                    if(fc(i,j,mid))
                    {
                        ans=max(ans,mid);
                        low=mid+1;
                    }
                    else
                    {
                        high=mid-1;
                    }
                }
                res+=ans;
            }
        }
        printf("Case #%d: %d\n",p,res);
    }

    return 0;
}






