/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

char arr[1010];
int s,i,val,l;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    while(scanf("%d",&s)==1&&s)
    {
        scanf(" %s",&arr);
        l=strlen(arr);
        for(i=l-1;i>=0;i--)
        {
            if(arr[i]=='_')
                val=26;
            else if(arr[i]=='.')
                val=27;
            else val=arr[i]-'A';
            val=(val+s)%28;
            if(val==26)
                printf("_");
            else if(val==27)
                printf(".");
            else printf("%c",'A'+val);
        }
        printf("\n");
    }

    return 0;
}


