/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int arr[5010][5010],n,m,k,fr[5010],fc[5010],val[100010],com[100010],rc[100010],i,j;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    scanf(" %d %d %d",&n,&m,&k);

    for(i=1;i<=k;i++)
    {
        scanf(" %d %d %d",&com[i],&rc[i],&val[i]);
    }
    for(i=k;i>=1;i--)
    {
        if(com[i]==1)
        {
            if(fr[rc[i]]==0)
            {
                fr[rc[i]]=1;
                for(j=1;j<=m;j++)
                {
                    if(arr[rc[i]][j]==0)
                        arr[rc[i]][j]=val[i];
                }
            }
        }
        else
        {
            if(fc[rc[i]]==0)
            {
                fc[rc[i]]=1;
                for(j=1;j<=n;j++)
                {
                    if(arr[j][rc[i]]==0)
                        arr[j][rc[i]]=val[i];
                }
            }
        }
    }

    for(i=1;i<=n;i++)
    {
        for(j=1;j<=m;j++)
        {
            printf("%d",arr[i][j]);
            if(j==m)
                printf("\n");
            else
                printf(" ");
        }
    }


    return 0;
}

