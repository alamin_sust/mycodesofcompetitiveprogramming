/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int n,m,i,j,res,curr_cont_left,curr_pack_left;
pair<int,int> pack[2510],cont[2510];

bool comp(pair<int,int> a,pair<int,int> b) {
    return a.first<b.first;
}

bool can_inserted(double edge, double r) {

    return (r*r*2.0*2.0)>(2.0*edge*edge);

}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d %d",&n,&m);

    for(i=0;i<n;i++) {
        scanf(" %d",&pack[i].first);
    }
    for(i=0;i<n;i++) {
        scanf(" %d",&pack[i].second);
    }

    for(i=0;i<m;i++) {
        scanf(" %d",&cont[i].first);
    }
    for(i=0;i<m;i++) {
        scanf(" %d",&cont[i].second);
    }

    sort(pack,pack+n,comp);
    sort(cont,cont+m,comp);

    res=0;
    curr_pack_left=pack[0].second;
    curr_cont_left=cont[0].second;
    for(i=0,j=0;i<n&&j<m;) {
        if(!can_inserted((double)pack[i].first,(double)cont[j].first)) {
            curr_cont_left=cont[++j].second;
            continue;
        }
        if(curr_pack_left==0) {
            i++;
            if(i<n)
            curr_pack_left=pack[i].second;
            continue;
        }
        if(curr_cont_left==0) {
            j++;
            if(j<m)
            curr_cont_left=cont[j].second;
            continue;
        }

        if(curr_pack_left==curr_cont_left) {
            res+=curr_pack_left;
            curr_pack_left=curr_cont_left=0;
        } else if(curr_pack_left<curr_cont_left) {
            res+=curr_pack_left;
            curr_cont_left-=curr_pack_left;
            curr_pack_left=0;
        } else {
            res+=curr_cont_left;
            curr_pack_left-=curr_cont_left;
            curr_cont_left=0;
        }
    }

    printf("%d\n",res);

    return 0;
}
