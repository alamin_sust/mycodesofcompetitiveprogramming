#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll func(ll n)
{
    return n*(n+1)/2;
}

main()
{
    ll t,n,p,en,st,mid,r,i;
    scanf(" %lld",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %lld",&n);
        n--;
        st=1;
        en=n;
        mid=en/2;
        r=func(n);
        for(i=1;i<=20;i++)
        {
            ll fm=func(mid);
            fm=abs(fm-(r-fm));
            ll fm2=func(mid+1);
            fm2=abs(fm2-(r-fm2));
            ll fm3=func(mid-1);
            fm3=abs(fm3-(r-fm3));
            //printf("%lld %lld %lld %lld\n",mid,fm,fm2,fm3);
            if(fm>fm2)
                st=mid;
            else if(fm>fm3)
                en=mid;
            mid=(st+en)/2;
        }
        printf("Case %lld: %lld\n",p,abs(func(mid)-(r-func(mid))));
    }
    return 0;
}

