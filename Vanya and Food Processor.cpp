/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll n,h,k,i,arr[100010],res,fil;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    scanf(" %I64d %I64d %I64d",&n,&h,&k);

    for(i=1LL;i<=n;i++)
    {
        scanf(" %I64d",&arr[i]);
    }
    i=1LL;
    res=0LL;
    fil=0LL;
    arr[n+1]=1000000005LL;
    while(i<=n)
    {
        while(i<=n&&(fil+arr[i])<=h)
        {
            fil+=arr[i];
            i++;
        }
        res+=(fil/k);
        fil=fil%k;
        if(fil>0LL&&(fil+arr[i])>h)
        {
            res++;
            fil=0LL;
        }
    }
    printf("%I64d\n",res);
    return 0;
}

