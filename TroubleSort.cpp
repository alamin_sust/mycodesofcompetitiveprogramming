/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

vector<int> v,v1,v2;
int t,p,i,n,res,in;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&t);
    for(p=1;p<=t;p++) {
        scanf(" %d",&n);
        v1.clear();
        v2.clear();
        for(i=0;i<n;i++) {
          scanf(" %d",&in);
          if(i%2) {
            v2.push_back(in);
          } else {
            v1.push_back(in);
          }
        }
        sort(v1.begin(),v1.end());
        sort(v2.begin(),v2.end());
        v.clear();
        i=0;
        for(;i<(n/2);i++) {
            v.push_back(v1[i]);
            v.push_back(v2[i]);
        }
        if(v.size()<n) {
            v.push_back(v1[i]);
        }

        res=-1;
        for(i=1;i<n;i++) {
            if(v[i]<v[i-1]) {
                res=i-1;
                break;
            }
        }

        printf("Case #%d: ",p);
        if(res==-1) {
            printf("OK\n");
        } else {
            printf("%d\n",res);
        }

    }

    return 0;
}
