/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define down 1
#define up 2
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

char arr[100010];
int node,i,l,dir,tree[1000010];

int main()
{
    while(scanf("%s",arr))
    {
        l=strlen(arr);
        if(l<=2)
            break;
        node=1;
        dir=down;
        for(i=1; i<l-1; i++)
        {
            if(arr[i]=='(')
            {
                if(dir==down)
                    node*=2;
                else
                    node=node*2+1,dir=down;
            }
            else if(arr[i]==')')
            {
                if(dir==up)
                {
                    node/=2;
                }
            }
            else
            {

                if(dir==down)
                {
                    dir=up;
                    if(arr[i]=='-')
                    {
                        tree[node*2]=(arr[i+1]-'0')*(-1);
                        i++;
                    }
                    else
                        tree[node*2]=arr[i]-'0'+1;

                }
                else
                {
                    if(arr[i]=='-')
                    {
                        tree[node*2+1]=(arr[i+1]-'0')*(-1);
                        i++;
                    }
                    else
                        tree[node*2+1]=arr[i]-'0'+1;
                }
            }
        }
        for(i=1;i<=40;i++)
            printf("%d ",tree[i]);
    }
    return 0;
}


