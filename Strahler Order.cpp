/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int i,mouth,t,p,cs,n,m,incoming[1010],dis[1010],flag[1010],prev[1010],from,to,lev,u,v;
vector<int>adj[1010];
queue<int>q;

int main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %d %d %d",&cs,&n,&m);
        for(i=1;i<=n;i++)
            adj[i].clear(),incoming[i]=0;
        for(i=0;i<m;i++)
        {
            scanf(" %d %d",&from,&to);
            adj[from].push_back(to);
            incoming[to]++;
        }
        for(i=1;i<=n;i++)
        {
            dis[i]=flag[i]=0;
            prev[i]=-1;
            if(incoming[i]==0)
            {

                dis[i]=1;
                flag[i]=1;
                q.push(i);
            }
        }
        while(!q.empty())
        {
            u=q.front();
            //lev=dis[u];
            q.pop();
            for(i=0;i<adj[u].size();i++)
            {
                v=adj[u][i];
                if(prev[v]<dis[u])
                {
                    dis[v]=prev[v]=dis[u];
                    flag[v]=1;
                }
                else if(prev[v]==dis[u]&&flag[v]==1)
                {
                    dis[v]=dis[u]+1;
                }
                incoming[v]--;
                if(incoming[v]==0)
                    q.push(v);
            }
        }
        lev=0;
        for(i=1;i<=n;i++)
            lev=max(lev,dis[i]);
       // printf("%d\n",mouth);
        printf("%d %d\n",cs,lev);
    }
    return 0;
}
