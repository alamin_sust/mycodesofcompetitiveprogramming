/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int t,p,mins,i;
double deg,now_deg_min,now_deg_hr,diff1,diff2,lim;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);


    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %lf",&deg);
        now_deg_min=0.0;
        now_deg_hr=0.0;
        mins=60*12;
        lim=1.0/120.0;
        for(i=0;i<mins;i++)
        {
            diff1=now_deg_min-now_deg_hr;

            if(diff1<0.0)
                diff1=-diff1;
            diff2=360.0-diff1;
            diff1-=deg;
            if(diff1<0.0)
                diff1=-diff1;

            if(diff2<0.0)
                diff2=-diff2;
            diff2-=deg;
            if(diff2<0.0)
                diff2=-diff2;
            if((diff1+eps)<lim||(diff2+eps)<lim)
                printf("%02d:%02d\n",i/60,i%60);
            now_deg_hr+=0.5;
            now_deg_min+=6.0;
            if(now_deg_min>=360.0)
                now_deg_min-=360.0;
        }
    }

    return 0;
}













