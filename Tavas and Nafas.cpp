/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

string arr[110];
int n;


int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    arr[0]="zero";
    arr[1]="one";
    arr[2]="two";
    arr[3]="three";
    arr[4]="four";
    arr[5]="five";
    arr[6]="six";
    arr[7]="seven";
    arr[8]="eight";
    arr[9]="nine";

    arr[10]="ten";
    arr[11]="eleven";
    arr[12]="twelve";
    arr[13]="thirteen";
    arr[14]="fourteen";
    arr[15]="fifteen";
    arr[16]="sixteen";
    arr[17]="seventeen";
    arr[18]="eighteen";
    arr[19]="nineteen";



    arr[20]="twenty";
    arr[21]="twenty-one";
    arr[22]="twenty-two";
    arr[23]="twenty-three";
    arr[24]="twenty-four";
    arr[25]="twenty-five";
    arr[26]="twenty-six";
    arr[27]="twenty-seven";
    arr[28]="twenty-eight";
    arr[29]="twenty-nine";

    arr[30]="thirty";
    arr[31]="thirty-one";
    arr[32]="thirty-two";
    arr[33]="thirty-three";
    arr[34]="thirty-four";
    arr[35]="thirty-five";
    arr[36]="thirty-six";
    arr[37]="thirty-seven";
    arr[38]="thirty-eight";
    arr[39]="thirty-nine";

arr[40]="forty";
    arr[41]="forty-one";
    arr[42]="forty-two";
    arr[43]="forty-three";
    arr[44]="forty-four";
    arr[45]="forty-five";
    arr[46]="forty-six";
    arr[47]="forty-seven";
    arr[48]="forty-eight";
    arr[49]="forty-nine";

arr[50]="fifty";
    arr[51]="fifty-one";
    arr[52]="fifty-two";
    arr[53]="fifty-three";
    arr[54]="fifty-four";
    arr[55]="fifty-five";
    arr[56]="fifty-six";
    arr[57]="fifty-seven";
    arr[58]="fifty-eight";
    arr[59]="fifty-nine";

arr[60]="sixty";
    arr[61]="sixty-one";
    arr[62]="sixty-two";
    arr[63]="sixty-three";
    arr[64]="sixty-four";
    arr[65]="sixty-five";
    arr[66]="sixty-six";
    arr[67]="sixty-seven";
    arr[68]="sixty-eight";
    arr[69]="sixty-nine";

arr[70]="seventy";
    arr[71]="seventy-one";
    arr[72]="seventy-two";
    arr[73]="seventy-three";
    arr[74]="seventy-four";
    arr[75]="seventy-five";
    arr[76]="seventy-six";
    arr[77]="seventy-seven";
    arr[78]="seventy-eight";
    arr[79]="seventy-nine";

arr[80]="eighty";
    arr[81]="eighty-one";
    arr[82]="eighty-two";
    arr[83]="eighty-three";
    arr[84]="eighty-four";
    arr[85]="eighty-five";
    arr[86]="eighty-six";
    arr[87]="eighty-seven";
    arr[88]="eighty-eight";
    arr[89]="eighty-nine";

arr[90]="ninety";
    arr[91]="ninety-one";
    arr[92]="ninety-two";
    arr[93]="ninety-three";
    arr[94]="ninety-four";
    arr[95]="ninety-five";
    arr[96]="ninety-six";
    arr[97]="ninety-seven";
    arr[98]="ninety-eight";
    arr[99]="ninety-nine";

    cin>>n;
    cout<<arr[n]<<"\n";
    return 0;
}





