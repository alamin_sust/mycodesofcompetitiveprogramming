/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define MAX 100500
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

 ll sieve[MAX],cnt,prime[MAX],flag[MAX],glob,res[MAX],M,N,Q;

void sieve_(void)
{
    cnt=0;
    sieve[0]=sieve[1]=1;
    for(ll i=2;i<=MAX;i++)
    {
        if(sieve[i]==0)
        {
            prime[cnt++]=i;
            //printf("%lld\n",i);
            for(ll j=2;i*j<=MAX;j++)
                sieve[i*j]=1;
        }
    }
    return;
}

ll calculate(ll num,ll pw)
{
    printf("..");
    if(pw==1LL)
        return num;
    return (calculate(num,pw/2LL)*calculate(num,pw-pw/2LL))%M;
}

void relaxup(ll val,ll pw)
{
    if(sieve[val]==0)
    {
        glob=(glob*calculate(val,val))%M;
        return;
    }
    for(ll i=0LL;prime[i]<=val;i++)
    {
        if(sieve[val]==0LL)
        {
           flag[val]+=pw;
           break;
        }
        while((val%prime[i])==0LL)
        {
           flag[prime[i]]+=pw;
           val/=prime[i];
        }
    }
    return;
}

void relaxdown(ll val,ll pw)
{
    for(ll i=0LL;prime[i]<=val;i++)
    {
        if(sieve[val]==0LL)
        {
           flag[val]-=pw;
           break;
        }
        while((val%prime[i])==0LL)
        {
           flag[prime[i]]-=pw;
           val/=prime[i];
        }
    }
    return;
}

ll get_result(ll mx)
{
    ll ret=1LL;
    for(ll i=0;prime[i]<=mx;i++)
    {
        if(flag[prime[i]])
        ret=(ret*calculate(prime[i],flag[prime[i]]))%M;
    }
    return ret;
}

void func(void)
{
    ll i,j;
    res[0]=1LL;
    glob=1LL;
    for(i=1LL,j=N;i<=j;i++,j--)
    {
        relaxup(i,i);
        relaxdown(j,j);
        res[i]=(glob*get_result(j-1))%M;
        printf("res[%lld]=%lld\n",i,res[i]);
    }
    return;
}

int main()
{
    ll p,t,i;
    sieve_();
    cin>>t;
    for(p=1; p<=t; p++)
    {
        scanf(" %lld %lld %lld",&N,&M,&Q);
        func();
        for(i=1;i<=Q;i++)
        {

        }

    }
    return 0;
}


