/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
int rr[]={0,0,-1,1};
int cc[]={-1,1,0,0};
using namespace std;


struct node {
    int x,y,val;
};
vector<node>adj;
queue<node> q;

int comp(node a,node b) {
    return a.val>b.val;
}


node nd,td;
int col[1010][1010],arr[1010][1010],t,p,i,j,n,m;



void bfs(node snode) {

    while(!q.empty()) q.pop();

    col[snode.x][snode.y]=1;

    q.push(snode);

    while(!q.empty()) {
        node nd=q.front();
        q.pop();

        for(i=0;i<4;i++) {
            node td;
            if((nd.x+rr[i])>=1&&(nd.x+rr[i])<=n&&(nd.y+cc[i])>=1&&(nd.y+cc[i])<=m && col[nd.x+rr[i]][nd.y+cc[i]]==0 && arr[nd.x+rr[i]][nd.y+cc[i]]!=-1) {
                col[nd.x+rr[i]][nd.y+cc[i]]=1;
                td.x=nd.x+rr[i];
                td.y=nd.y+cc[i];
                td.val=max(arr[td.x][td.y],nd.val-1);
                if(td.val>0)
                q.push(td);
            }
        }
    }

    return;
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++) {
        scanf(" %d %d",&n,&m);
        adj.clear();

        for(i=1;i<=n;i++)
            for(j=1;j<=m;j++)
            col[i][j]=0;

        for(i=1;i<=n;i++) {
            for(j=1;j<=m;j++) {
                scanf(" %d",&arr[i][j]);
                if(arr[i][j]>0) {
                    node tp;
                    tp.x=i;
                    tp.y=j;
                    tp.val=arr[i][j];
                    adj.push_back(tp);
                }
            }
        }

        sort(adj.begin(),adj.end(),comp);

        for(int ii=0;ii<adj.size();ii++) {



            node now = adj[ii];

            //printf("%d %d %d\n",now.x,now.y,now.val);

            if(col[now.x][now.y]!=1)
            {
                bfs(now);
            }
        }


        for(i=1;i<=n;i++) {
            for(j=1;j<=m;j++) {
                if(col[i][j]) printf("Y");
                else {
                    if(arr[i][j]==-1) {
                        printf("B");
                    } else printf("N");

                }

            }
            printf("\n");
        }

    }


    return 0;
}

