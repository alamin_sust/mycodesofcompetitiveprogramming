/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int rr[]={0,0,1,-1};
int cc[]={-1,1,0,0};

struct node{

int x,y;

};

node u,v;

int check,stat[510][510],r1,r2,c1,c2,n,m;
char arr[510][510],arr2[510][510];
queue<node>q;

void bfs(void)
{
    memset(stat,0,sizeof(stat));
    u.x=r1;
    u.y=c1;
    q.push(u);
    stat[r1][c1]=1;

    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(int i=0;i<4;i++)
        {
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            if(v.x>n||v.x<1||v.y>m||v.y<1)
                continue;
            if(v.x==r2&&v.y==c2&&stat[r2][c2]==0&&arr2[r2][c2]=='X')
            {
            printf("YES\n");
            return;
            }
            if(v.x==r2&&v.y==c2&&stat[r2][c2]==1)
            {
            printf("YES\n");
            return;
            }
            if(v.x==r2&&v.y==c2&&check>=2)
            {
            printf("YES\n");
            return;
            }

            if(stat[v.x][v.y]==0&&arr2[v.x][v.y]=='.')
            {
                stat[v.x][v.y]=1;
                q.push(v);
            }
        }
    }
    printf("NO\n");
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    int i,j;

    cin>>n>>m;
    getchar();
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=m;j++)
        {
            scanf("%c",&arr2[i][j]);
            arr[i][j]=arr2[i][j];
        }
        getchar();
    }
    cin>>r1>>c1;
    cin>>r2>>c2;
    if(n==1&&m==1)
        {printf("NO\n");
        return 0;}

    for(i=0;i<4;i++)
    {
        v.x=r2+rr[i];
        v.y=c2+cc[i];
        if(v.x>=1&&v.x<=n&&v.y>=1&&v.y<=m&&arr2[v.x][v.y]=='.')
            check++;
    }

    arr[r1][c1]=arr2[r1][c1]='.';

    bfs();

    return 0;
}





