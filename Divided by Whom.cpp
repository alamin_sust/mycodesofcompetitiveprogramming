/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


ll t,p,i,n,k,arr[100010];

struct node{
ll x,y;
};

node det;
vector<node>v;

map<ll,ll>mpp,mpp2;

queue<ll>q[100010];


ll comp(node a,node b)
{
    if(a.x==b.x)
        return a.y<b.y;
    return a.x<b.x;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    scanf(" %lld",&t);


    for(p=1;p<=t;p++)
    {
        mpp.clear();
        mpp2.clear();
        scanf(" %lld %lld",&n,&k);
        ll cnt=0;
        for(i=0;i<n;i++)
        {
            scanf(" %lld",&arr[i]);
            if(mpp2[arr[i]%k]==0)
                mpp2[arr[i]%k]=++cnt;

            mpp[arr[i]%k]++;
            q[mpp2[arr[i]%k]].push(arr[i]);
        }
        v.clear();
        for(i=0;i<n;i++)
        {
            ll now=(arr[i]%k),need;
            if(now==0)
            need=0;
            else
            need=k-now;
            if(mpp[now]>0&&mpp[need]>0)
            {
                mpp[now]--;
                mpp[need]--;
                if(!q[mpp2[now]].empty())
                {
                det.x=q[mpp2[now]].front();
                q[mpp2[now]].pop();
                }
                else continue;
                if(!q[mpp2[need]].empty())
                {
                det.y=q[mpp2[need]].front();
                q[mpp2[need]].pop();
                }
                else continue;
                if(det.x>det.y)
                    swap(det.x,det.y);
                v.push_back(det);
            }
        }

        sort(v.begin(),v.end(),comp);
        printf("Case %lld:\n",p);
        if((v.size()*2)==n)
        for(i=0;i<v.size();i++)
            printf("%lld %lld\n",v[i].x,v[i].y);
        else
        printf("Not Possible\n");

        if(p!=t)
        for(i=0;i<n;i++)
        {
            ll now=(arr[i]%k);
            while(!q[mpp2[now]].empty())
                q[mpp2[now]].pop();
        }

    }


    return 0;
}






