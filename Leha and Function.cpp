/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;


struct node{
    int pos,val;
};

node arr1[200010],arr2[200010];
int arr3[200010],i,j,m;

int comp(node a,node b) {
    return a.val<b.val;
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&m);

    for(i=0;i<m;i++) {
        scanf(" %d",&arr1[i].val);
        arr1[i].pos=i;
    }
    for(i=0;i<m;i++) {
        scanf(" %d",&arr2[i].val);
        arr2[i].pos=i;
    }

    sort(arr1,arr1+m,comp);
    sort(arr2,arr2+m,comp);

    for(i=m-1,j=0;i>=0;j++,i--) {
        arr3[arr2[j].pos]=arr1[i].val;
    }

    for(i=0;i<m;i++) {
        printf("%d ",arr3[i]);
    }
    printf("\n");
    return 0;
}

