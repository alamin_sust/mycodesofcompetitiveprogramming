/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int i,j,k,t,n,p,res,arr[100010];

int main()
{
    freopen("coding_contest_creation.txt","r",stdin);
    freopen("CodingContestCreationOut.txt","w",stdout);
    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        res=0;
        for(i=1;i<=n;i++)
        {
            scanf(" %d",&arr[i]);
        }
        for(i=1;i<=n;)
        {
            k=1;
            for(j=2;j<=4&&(i+j-1)<=n;j++)
            {
                if(j==2)
                {
                    if(arr[i]<arr[i+1]&&(arr[i]+30)>=arr[i+1])
                        k=2;
                    else
                        break;
                }
                else if(j==3)
                {
                    if(arr[i]<arr[i+1]&&arr[i+1]<arr[i+2]&&((arr[i]+10)>=arr[i+1]||(arr[i+1]+10)>=arr[i+2])&&(arr[i]+20)>=arr[i+1]&&(arr[i+1]+20)>=arr[i+2])
                        k=3;
                    else
                        break;
                }
                else
                {
                    if(arr[i]<arr[i+1]&&arr[i+1]<arr[i+2]&&arr[i+2]<arr[i+3]&&(arr[i]+10)>=arr[i+1]&&(arr[i+1]+10)>=arr[i+2]&&(arr[i+2]+10)>=arr[i+3])
                        k=4;
                    else
                        break;
                }
            }
            res+=(4-k);
            i+=k;
        }
        printf("Case #%d: %d\n",p,res);
    }

    return 0;
}
