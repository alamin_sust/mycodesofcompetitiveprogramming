#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
using namespace std;
int res,com;
struct node
{
    int val;
    int upd;
};
node arr[800010];
int node_calc(int n)
{
    int i;
    for(i=1; i<n; i*=2)
    {}
    return i;
}

void push_down(int k1,int k2,int k)
{
    arr[k].val=k2-k1+1-arr[k].val;
    arr[k].upd=(arr[k].upd+1)%2;
    return;
}

void segment_tree(int from,int to,int k1,int k2,int k)
{
    if(from<=k1&&to>=k2)
    {
        if(com==1)
            res+=arr[k].val;
        else
        {
            arr[k].upd=(arr[k].upd+1)%2;
            arr[k].val=k2-k1+1-arr[k].val;
        }
    }
    else
    {
        int mid=(k1+k2)/2;
        if(arr[k].upd)
        {
            arr[k].upd=0;
            push_down(k1,mid,k*2);
            push_down(mid+1,k2,k*2+1);
        }
        if(from<=mid)
            segment_tree(from,to,k1,mid,k*2);
        if(to>mid)
            segment_tree(from,to,mid+1,k2,k*2+1);
        arr[k].val=arr[k*2].val+arr[k*2+1].val;
    }
    return;
}

main()
{
    int n,m,i,max_node,from,to;
    cin>>n>>m;
    max_node=node_calc(n);
    for(i=1; i<=m; i++)
    {
        scanf(" %d %d %d",&com,&from,&to);
        res=0;
        segment_tree(from,to,1,max_node,1);
        if(com==1)
            printf("%d\n",res);
    }
    return 0;
}

