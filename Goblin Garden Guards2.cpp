/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) ( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
int rr[]={0,0,1,-1,-1,-1,1,1};
int cc[]={1,-1,0,0,-1,1,-1,1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

bool arr[11005][11005];
map<pair<ll,ll> ,ll>mpp;

void filll(ll x,ll y,ll r)
{
    ll R=r*r;
    for(ll i=0LL;i<=r;i++)
    {
        for(ll j=0LL;j<=r;j++)
        {
            //printf("...");
            if(dist(x-i,y-j,x,y)<=R)
            {
                arr[x-i+300LL][y-j+300LL]=true;
                arr[x-j+300LL][y+i+300LL]=true;
                arr[x+i+300LL][y+j+300LL]=true;
                arr[x+j+300LL][y-i+300LL]=true;
            }
        }
    }
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    ll res,X,Y,r,x[100010],y[100010];
    ll ng,ns,i;

    scanf(" %I64d",&ng);
    res=ng;
    for(i=0LL;i<ng;i++)
    {
        scanf(" %I64d %I64d",&x[i],&y[i]);
    }

    scanf(" %I64d",&ns);
    for(i=0LL;i<ns;i++)
    {
        scanf(" %I64d %I64d %I64d",&X,&Y,&r);
        if(mpp[make_pair(X,Y)]==0||(mpp[make_pair(X,Y)]>0&&mpp[make_pair(X,Y)]<r))
        {
        filll(X,Y,r);
        mpp[make_pair(X,Y)]=max(r,mpp[make_pair(X,Y)]);
        }
    }



    for(i=0LL;i<ng;i++)
    {
        if(arr[x[i]+300LL][y[i]+300LL])
            res--;
    }

    printf("%I64d\n",res);

    return 0;
}



