#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class BuyingTshirts
{
public:
	int meet(int T, vector <int> Q, vector <int> P)
	{
	    int ret=0,a=0,b=0;
	    for(int i=0;i<Q.size();i++)
        {
            a+=Q[i];
            b+=P[i];
            if(a>=T&&b>=T)
            {
                a-=T;
                b-=T;
                ret++;
            }
            else if(a>=T)
            {
                a-=T;
            }
            else if(b>=T)
            {
                b-=T;
            }
        }
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	BuyingTshirts objectBuyingTshirts;

	//test case0
	int param00 = 5;
	vector <int> param01;
	param01.push_back(1);
	param01.push_back(2);
	param01.push_back(3);
	param01.push_back(4);
	param01.push_back(5);
	vector <int> param02;
	param02.push_back(5);
	param02.push_back(4);
	param02.push_back(3);
	param02.push_back(2);
	param02.push_back(1);
	int ret0 = objectBuyingTshirts.meet(param00,param01,param02);
	int need0 = 2;
	assert_eq(0,ret0,need0);

	//test case1
	int param10 = 10;
	vector <int> param11;
	param11.push_back(10);
	param11.push_back(10);
	param11.push_back(10);
	vector <int> param12;
	param12.push_back(10);
	param12.push_back(10);
	param12.push_back(10);
	int ret1 = objectBuyingTshirts.meet(param10,param11,param12);
	int need1 = 3;
	assert_eq(1,ret1,need1);

	//test case2
	int param20 = 2;
	vector <int> param21;
	param21.push_back(1);
	param21.push_back(2);
	param21.push_back(1);
	param21.push_back(2);
	param21.push_back(1);
	param21.push_back(2);
	param21.push_back(1);
	param21.push_back(2);
	vector <int> param22;
	param22.push_back(1);
	param22.push_back(1);
	param22.push_back(1);
	param22.push_back(1);
	param22.push_back(2);
	param22.push_back(2);
	param22.push_back(2);
	param22.push_back(2);
	int ret2 = objectBuyingTshirts.meet(param20,param21,param22);
	int need2 = 5;
	assert_eq(2,ret2,need2);

	//test case3
	int param30 = 100;
	vector <int> param31;
	param31.push_back(1);
	param31.push_back(2);
	param31.push_back(3);
	vector <int> param32;
	param32.push_back(4);
	param32.push_back(5);
	param32.push_back(6);
	int ret3 = objectBuyingTshirts.meet(param30,param31,param32);
	int need3 = 0;
	assert_eq(3,ret3,need3);

	//test case4
	int param40 = 10;
	vector <int> param41;
	param41.push_back(10);
	param41.push_back(1);
	param41.push_back(10);
	param41.push_back(1);
	vector <int> param42;
	param42.push_back(1);
	param42.push_back(10);
	param42.push_back(1);
	param42.push_back(10);
	int ret4 = objectBuyingTshirts.meet(param40,param41,param42);
	int need4 = 0;
	assert_eq(4,ret4,need4);

}
