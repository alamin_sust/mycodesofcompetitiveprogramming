/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
    ll val,ind;
};

ll comp(node a,node b)
{
    return a.val<b.val;
}

node arr[100010];
ll indx[100010],i,n,k,f,t,p,low[100010];

main()
{
    cin>>n>>k>>p;
    for(i=1;i<=n;i++)
    {
        scanf(" %lld",&arr[i].val);
        arr[i].ind=i;
    }
    sort(arr+1,arr+n+1,comp);
    for(i=1;i<=n;i++)
    {
        indx[arr[i].ind]=i;
    }
    low[1]=1;
    for(i=2;i<=n;i++)
    {
        if((arr[i].val-arr[i-1].val)>k)
        {
            low[i]=i;
        }
        else low[i]=low[i-1];
    }
   // for(i=1;i<=n;i++)
     //   printf("..%lld\n",low[i]);
    for(i=1;i<=p;i++)
    {
        scanf(" %lld %lld",&f,&t);
        //printf("%lld %lld\n",indx[f],indx[t]);
        if(indx[f]<indx[t])
        {
            if(low[indx[t]]<=indx[f])
            printf("Yes\n");
            else
            printf("No\n");
        }
        else
        {
            if(low[indx[f]]<=indx[t])
            printf("Yes\n");
            else
            printf("No\n");
        }
    }
    return 0;
}
