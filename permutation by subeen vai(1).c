#include<stdio.h>

int taken[10010];

void process(int *x,int n)
{
    int i;
    for(i=0;i<n;i++)
    printf("%d",x[i]);
    printf("\n");
    return;
}

void permute(int *arr,int *x,int n,int i)
{
    int j;
    if(i==n)
    {process(x,n);
    return;}
    for(j=0;j<n;j++)
    {
        if(taken[j]==0)
        {
            taken[j]=1;
            x[i]=arr[j];
            permute(arr,x,n,i+1);
            taken[j]=0;
        }
    }
    return;
}

main()
{
    int i,n,arr[10010],x[10010];
    scanf(" %d",&n);
    for(i=0;i<n;i++)
    scanf(" %d",&arr[i]);
    permute(arr,x,n,0);
    return 0;
}
