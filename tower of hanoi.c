#include<stdio.h>
void transfer(int n,char from,char to,char temp)
{
    if(n==0)
        return;
    else
    {
        transfer(n-1,from,temp,to);
        printf("Move disc %d from %c to %c\n",n,from,to);
        transfer(n-1,temp,to,from);
    }
}
int main()
{
    char from,to,temp;
    int n;
    printf("Disc number : ");
    scanf("%d",&n);
    transfer(n,'L','R','C');
    return 0;
}
