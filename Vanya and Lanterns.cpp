/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

double l,arr[1010];
int n;

int func(double rng)
{
    double now=0.0;
    for(int i=1;i<=n;i++)
    {
        if((now+rng)<arr[i])
            return 0;
        now=arr[i]+rng;
    }
    if(now<l)
        return 0;
    return 1;
}

int main()
{
    int cnt;
    double low,mid,high,res;
    cin>>n>>l;
    for(int i=1;i<=n;i++)
        scanf(" %lf",&arr[i]);
    sort(arr+1,arr+n+1);
    cnt=152;
    low=0.0;
    high=l+10.0;
    res=high;
    while(cnt--)
    {
        mid=(low+high)/2.0;
        if(func(mid))
        {
            res=min(res,mid);
            high=mid;
        }
        else
        {
            low=mid;
        }
    }
    printf("%.10lf\n",res);
    return 0;
}

