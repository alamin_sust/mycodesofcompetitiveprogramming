#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

struct node{
    int x,y,val;
};

node adj[10010];

int comp(node a,node b)
{
    return a.val<b.val;
}

int t,p,i,j,k,res,n,in,par[110],nd;

main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n;
        for(res=0,k=0,i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
            {
                scanf(" %d",&in);
                if(in>0)
                {
                    res+=in;
                    if(i!=j)
                    {
                    adj[k].x=i;
                    adj[k].y=j;
                    adj[k++].val=in;
                    }
                }
            }
        }
        sort(adj,adj+k,comp);
        for(i=1;i<=n;i++)
            par[i]=i;
        for(nd=1,i=0;i<k;i++)
        {
            int u=adj[i].x;
            int v=adj[i].y;
            while(par[u]!=u)
                u=par[u];
            while(par[v]!=v)
                v=par[v];
            if(u!=v)
            {
                par[v]=u;
                res-=adj[i].val;
                nd++;
            }
        }
        if(nd<n)
            printf("Case %d: -1\n",p);
        else
            printf("Case %d: %d\n",p,res);
    }
    return 0;
}
