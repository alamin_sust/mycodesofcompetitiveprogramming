#include<stdio.h>
#include<string.h>

char st[112][10],dst[112][10],ch,city[112][510][10];
int p=0,que[110][10010],adj[110][110][110],front,rear,flag=0,status[110][110],from,to,orig[110][110],temp,nodes,edges,res[110][110];

void enq(int x)
{
    que[p][rear]=x;
    return;
}

void bfs(int from,int to)
{
    int i,j;
    front=rear=1;
    enq(from);
    status[p][from]=2;
    orig[p][from]=0;
    while(front<=rear)
    {
        temp=que[p][front];
        front++;
        for(i=1; i<=nodes; i++)
        {
            if(adj[p][temp][i]==1&&status[p][i]==1)
            {
                orig[p][i]=temp;
                rear++;
                enq(i);
                status[p][i]=2;
            }
        }
    }
    return;
}

main()
{

    int i,j,k,l,value,ult;
    while(scanf("%d",&edges)!=EOF)
    {
        getchar();
       for(ult=0,i=1,j=1; i<=edges; i++)
        {
            char in1[110],in2[110];
            scanf(" %s %s",in1,in2);
            for(flag=0,k=1; k<j; k++)
            {
                if(strcmp(in1,city[p][k])==0)
                {
                    from=k;
                    flag=1;
                }
            }
            if(flag==0)
            {
                l=strlen(in1);
                for(k=0; k<l; k++)
                    city[p][j][k]=in1[k];
                from=j;
                j++;
            }
            for(flag=0,k=1; k<j; k++)
            {
                if(strcmp(in2,city[p][k])==0)
                {
                    to=k;
                    flag=1;
                }
            }
            if(flag==0)
            {
                l=strlen(in2);
                for(k=0; k<l; k++)
                    city[p][j][k]=in2[k];
                to=j;
                j++;
            }
            adj[p][from][to]=1;
            adj[p][to][from]=1;
        }
        nodes=j-1;
        scanf(" %s %s",st[p],dst[p]);
        if(strcmp(st[p],dst[p])==0)
        {printf("%s %s\n",st[p],st[p]);
        ult=-10;
        continue;}
        for(i=1; i<=nodes; i++)
        {
            if(strcmp(st[p],city[p][i])==0)
            {
                from=i;
                ult++;
                break;
            }
        }
        for(i=1; i<=nodes; i++)
        {
            if(strcmp(dst[p],city[p][i])==0)
            {
                to=i;
                ult++;
                break;
            }
        }
        for(i=1; i<=nodes; i++)
        {
            status[p][i]=1;
            for(j=1; j<=nodes; j++)
            {
                if(adj[p][i][j]!=1)
                    adj[p][i][j]=0;
            }
        }
        if(ult>1)
        {bfs(from,to);
        i=to;
        j=0;
        while(i!=0)
        {
            res[p][j++]=i;
            i=orig[p][i];
        }
        }
        else if(ult>=0)
        j=1;
        if(ult<0)
        printf("\n");
        else if(j<2)
            printf("No route\n");
        else
        {
            for(i=j-1; i>=1; i--)
                printf("%s %s\n",city[p][res[p][i]],city[p][res[p][i-1]]);

        }
        p++;
        printf("\n");
    }
    return 0;
}
