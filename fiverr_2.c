#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<string.h>

void reverse_bytes(unsigned char *buffer, int n)
{
    int iter; //for loop iteration
    int total_bits=8*n; // total bits of the buffer
    int left=0; //current left of buffer index
    int right=n-1; //current right of buffer index
    for(iter=0; iter<(total_bits/2); iter++) { // we need to iterate through the half of the total bits
        if((iter%8)==0 && iter!=0) { //0'th index of a new buffer other than the first buffer
            left++; //increment of current left of buffer index
            right--; //decrement of current right of buffer index
        }
        unsigned char left_bit = buffer[left]&(1<<((8-(iter+1)+8)%8)); // finding the left bit
        unsigned char right_bit = buffer[right]&(1<<(iter%8)); // finding the right bit
        if(left_bit) { // left bit is not 0
            buffer[right]|=(1<<(iter%8)); // left bit is assigned to the right bit
        }
        else {
            buffer[right]&=((1<<8)-1)^(1<<(iter%8)); // left bit is assigned to the right bit
        }

        if(right_bit) { // right bit is not 0
            buffer[left]|=(1<<((8-(iter+1)+8)%8)); // right bit is assigned to the left bit
        }
        else {
            buffer[left]&=((1<<8)-1)^(1<<((8-(iter+1)+8)%8)); // right bit is assigned to the left bit
        }
    }

    for(iter=0;iter<n;iter++) { // printing buffer
        printf("%c\n",buffer[iter]);
    }
}

int main() {
    unsigned char buffer[4];
    buffer[0] = 'a';
    buffer[1] = 'z';
    buffer[2] = 'A';
    buffer[3] = 'Z';
    reverse_bytes(buffer, 4); // calling function
    return 0;
}
