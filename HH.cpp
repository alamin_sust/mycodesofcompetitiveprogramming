/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
#define SZ 1000003
#define llu long long unsigned
llu lcm(llu a,llu b)
{
    return (a*b)/__gcd(a,b);
}

int main()
{
    int n,T;
    string str;
    cin>>T;
    for(int t=1;t<=T;t++)
    {
        cin>>n;
        cin>>str;
        ull dLcm = 1;
        if(str[0]=='0')
        {
            cout<<0<<endl;
            continue;
        }
        for(int i=1;i<6;i++)
        {
            if(str[i]=='1')dLcm = lcm(dLcm,i+1);
            cout<<dLcm<<" "<<i<<endl;
        }
        ull ndLcm = 1;
        for(int i=1;i<6;i++)
        {
            if(str[i]=='0'&&dLcm%i==0)
            {
                cout<<0<<endl;
                continue;
            }
        }
    }
    return 0;
}

