/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll req[55],aa[110],bb[110],t,p,dp2[55][55],n,r,mpp[110],k,dp[53][53][53];
struct node{
ll type,val;
};

node arr[55];

ll comp(node a,node b)
{
    return a.type<b.type;
}

ll nCr(ll nn,ll rr)
{
    if(rr==1LL) return nn;
    if(nn==rr) return 1LL;
    ll &ret=dp2[nn][rr];
    if(ret!=-1LL) return ret;
    ret=nCr(nn-1LL,rr)+nCr(nn-1LL,rr-1LL);
    return ret;
}

ll rec(ll pos,ll till,ll taken)
{
    if(pos>=n)
    {
        if(till>=r&&taken==k)
        {
            return dp[pos][till][taken]=1LL;
        }
        return dp[pos][till][taken]=0LL;
    }
    if(taken>k)
        return dp[pos][till][taken]=0LL;
    ll &ret=dp[pos][till][taken];
    if(ret!=-1LL)
    {
        return ret;
    }
    ret=0LL;
    if(arr[pos].type==req[till])
    {
        ret=ret+rec(pos+1LL,till+1LL,taken+1LL);
    }
    else if(mpp[arr[pos].type])
        ret=ret+rec(pos+1LL,till,taken+1LL);
    ret=ret+rec(pos+1LL,till,taken);
    return ret;
}

ll _gcd(ll a,ll b)
{
    if(b==0LL)
        return a;
    return _gcd(b,a%b);
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    ll i,m;


    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++)
    {
        scanf(" %lld %lld %lld %lld",&n,&m,&r,&k);
        memset(mpp,0LL,sizeof(mpp));
        memset(dp2,-1LL,sizeof(dp2));
        memset(dp,-1LL,sizeof(dp));
        for(i=0LL;i<n;i++)
        {
            scanf(" %lld",&arr[i].type);
        }
        for(i=0LL;i<n;i++)
        {
            scanf(" %lld",&arr[i].val);
        }
        for(i=0LL;i<r;i++)
        {
            scanf(" %lld",&req[i]);
            mpp[req[i]]=1LL;
        }
        sort(arr,arr+n,comp);
        sort(req,req+r);
        ll res=rec(0,0,0);
        for(i=k+1LL;i<=n;i++)
        {
            aa[i]=i;
            ll ggg=_gcd(aa[i],res);
            res/=ggg;
            aa[i]/=ggg;
        }
        for(i=1LL;i<=k;i++)
        {
            bb[i]=i;
        }
        for(i=k+1LL;i<=n;i++)
        {
            for(ll j=1LL;j<=k;j++)
            {
            ll ggg=_gcd(aa[i],bb[j]);
            bb[j]/=ggg;
            aa[i]/=ggg;
            }
        }
        ll ncr=1LL;
        for(i=k+1LL;i<=n;i++)
        {
            ncr*=aa[i];
        }
        for(i=1LL;i<=k;i++)
        {
            ncr/=bb[i];
        }
        ll gcd=_gcd(res,ncr);
        res/=gcd;
        ncr/=gcd;
        if(res!=ncr)
        printf("%lld/%lld\n",res,ncr);
        else
        printf("%lld\n",res);
    }


    return 0;
}
/*
2
4 2 2 2
1 1 2 2
10 15 10 20
1 2
3 45 1 2
45 45 45
79398 42833 42833
45
*/

