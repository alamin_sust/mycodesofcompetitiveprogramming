#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

priority_queue<int>pq;

class CombiningSlimes
{
public:
	int maxMascots(vector <int> a)
	{
	    while(!pq.empty())
            pq.pop();
        for(int i=0;i<a.size();i++)
        {
            pq.push(a[i]);
        }
        int ret=0;
        while(pq.size()>1)
        {
            int a=pq.top();
            pq.pop();
            int b=pq.top();
            pq.pop();
            ret+=a*b;
            pq.push(a+b);
        }
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	CombiningSlimes objectCombiningSlimes;

	//test case0
	vector <int> param00;
	param00.push_back(3);
	param00.push_back(4);
	int ret0 = objectCombiningSlimes.maxMascots(param00);
	int need0 = 12;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(2);
	param10.push_back(2);
	param10.push_back(2);
	int ret1 = objectCombiningSlimes.maxMascots(param10);
	int need1 = 12;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(1);
	param20.push_back(2);
	param20.push_back(3);
	int ret2 = objectCombiningSlimes.maxMascots(param20);
	int need2 = 11;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(3);
	param30.push_back(1);
	param30.push_back(2);
	int ret3 = objectCombiningSlimes.maxMascots(param30);
	int need3 = 11;
	assert_eq(3,ret3,need3);

	//test case4
	vector <int> param40;
	param40.push_back(7);
	param40.push_back(6);
	param40.push_back(5);
	param40.push_back(3);
	param40.push_back(4);
	param40.push_back(6);
	int ret4 = objectCombiningSlimes.maxMascots(param40);
	int need4 = 395;
	assert_eq(4,ret4,need4);

}
