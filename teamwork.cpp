#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include <fstream>
#include<vector>
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int arr[10005];
int dp[10005][1005],n,k;

int rec(int pos, int taken,int currmax)
{
    if(pos>n)
        return 0;
    int &ret = dp[pos][taken];
    if(ret!=-1)
        return ret;
    ret = 0;
    currmax = max(currmax,arr[pos]);
    if((taken+1)==k)
    {
        ret = (taken+1)*currmax+rec(pos+1,0,0);
    }
    else
    {
        ret = max((taken+1)*currmax+rec(pos+1,0,0),rec(pos+1,taken+1,currmax));
    }
    return ret;
}

int main()
{
    freopen("teamwork.in","r",stdin);
    freopen("teamwork.out","w",stdout);

    int i;
    scanf(" %d %d",&n,&k);
    for(i = 1; i<=n; i++)
    {
        scanf(" %d",&arr[i]);
    }
    //initMax(1,1,n);

    memset(dp,-1,sizeof(dp));

    printf("%d\n",rec(1,0,0));

    return 0;
}


