#include <map>
#include <queue>
#include <stack>
#include <cmath>
#include <cctype>
#include <set>
#include <bitset>
#include <algorithm>
#include <list>
#include <vector>
#include <sstream>
#include <iostream>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <assert.h>

using namespace std;

typedef long long ll;
typedef pair<int,int> paii;
typedef pair< ll, ll > pall;


#define PI (2.0*acos(0))
#define ERR 1e-5
#define mem(a,b) memset(a,b,sizeof a)
#define pb push_back
#define popb pop_back
#define all(x) (x).begin(),(x).end()
#define mp make_pair
#define SZ(x) (int)x.size()
#define oo (1<<25)
#define FOREACH(it,x) for(__typeof((x).begin()) it=(x.begin()); it!=(x).end(); ++it)
#define Contains(X,item)        ((X).find(item) != (X).end())
#define popc(i) (__builtin_popcount(i))
#define fs      first
#define sc      second
#define EQ(a,b)     (fabs(a-b)<ERR)


template<class T1> void deb(T1 e){cout<<e<<endl;}
template<class T1,class T2> void deb(T1 e1,T2 e2){cout<<e1<<" "<<e2<<endl;}
template<class T1,class T2,class T3> void deb(T1 e1,T2 e2,T3 e3){cout<<e1<<" "<<e2<<" "<<e3<<endl;}
template<class T1,class T2,class T3,class T4> void deb(T1 e1,T2 e2,T3 e3,T4 e4){cout<<e1<<" "<<e2<<" "<<e3<<" "<<e4<<endl;}
template<class T1,class T2,class T3,class T4,class T5> void deb(T1 e1,T2 e2,T3 e3,T4 e4,T5 e5){cout<<e1<<" "<<e2<<" "<<e3<<" "<<e4<<" "<<e5<<endl;}
template<class T1,class T2,class T3,class T4,class T5,class T6> void deb(T1 e1,T2 e2,T3 e3,T4 e4,T5 e5,T6 e6){cout<<e1<<" "<<e2<<" "<<e3<<" "<<e4<<" "<<e5<<" "<<e6<<endl;}


template<class T> T Abs(T x) {return x > 0 ? x : -x;}
template<class T> inline T sqr(T x){return x*x;}
ll Pow(ll B,ll P){      ll R=1; while(P>0)      {if(P%2==1)     R=(R*B);P/=2;B=(B*B);}return R;}
int BigMod(ll B,ll P,ll M){     ll R=1; while(P>0)      {if(P%2==1){R=(R*B)%M;}P/=2;B=(B*B)%M;} return (int)R;} /// (B^P)%M


vector<int>prefix,full;

ll dp[30][30][2][2];

ll rec(int indx ,int prefIndx, int prv , int prv2 )
{
    if(indx>=SZ(full)) return 1;
    if(prefIndx>=SZ(prefix) && prv2) return 0;

    ll &ret=dp[indx][prefIndx][prv][prv2];
    if(ret!=-1) return ret;
    ret=0;

    int nowprv,nowprv2;
    int mx= ( ( prv == 1 ) ? full[indx] : 9 );
    int mx2=9;

    if(prefIndx<SZ(prefix)) mx2=( ( prv2 == 1 ) ? prefix[prefIndx] : 9 );



    for(int i=0;i<=min(mx,mx2);i++)
    {
        if(prv==1 && i==mx) nowprv=1;
        else nowprv=0;

        if(prv2==1 && i==mx2) nowprv2=1;
        else if(i==0 && prefIndx==0) nowprv2=1;
        else nowprv2=0;

        if(i || prefIndx) ret+=rec(indx+1,prefIndx+1,nowprv,nowprv2);
        else ret+=rec(indx+1,0,nowprv,nowprv2 );
    }

    return ret;
}

ll actualPosition(ll x , ll N)
{
    full.clear();
    while(N)
    {
        full.pb(N%10);
        N/=10;
    }
    reverse(all(full));

    prefix.clear();

    while(x)
    {
        prefix.pb(x%10);
        x/=10;
    }
    reverse(all(prefix));

    mem(dp,-1);
    ll ans=rec(0,0,1,1);

    return ans-1;
}



int main(void)
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);

    ll k,m,lo,mid,hi , now;
    ll bestN;
    //printf("%lld\n",actualPosition(2,11));
    while(scanf("%lld %lld",&k,&m)==2)
    {
        lo=k;
        bestN=-1;
        hi=4223372036854770685LL;

        while(hi>=lo)
        {
            mid=(hi+lo)/2;
            now=actualPosition(k,mid);

            if(now==m)
            {
                if(bestN==-1) bestN=mid;
                else bestN=min(bestN,mid);
            }

            if( now >= m ) hi=mid-1;
            else lo=mid+1;
        }
        if(bestN==-1) puts("0");
        else printf("%lld\n",bestN);
    }
    return 0;
}


