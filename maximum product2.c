#include<iostream>
#include<string>

using namespace std;

int main()
{
   int n,count = 1;
   string empty;

   while(cin>>n)
   {
      long long ans = 1;
      long long cur;
      long long res = 0;

      for(int i=0;i<n;i++)
      {
         cin >> cur;
         if(cur > 0)
         {
            ans = ans*cur;
            res = max(res,ans);
         }
         else
            ans = 1;
      }

      cout << "Case #" << count << ": The maximum product is " << res << ".\n\n";
      count++;
      getline(cin,empty);
   }
   return 0;
}
