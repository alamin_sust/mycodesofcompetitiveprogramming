/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int n,i,j,p,x,xx,z,zz;
char arr[10][10];


main()
{
    cin>>n;
    for(p=1; p<=n; p++)
    {
        xx=zz=0;
        z=x=0;
        for(i=0; i<3; i++)
        {
            scanf(" %s",arr[i]);
            for(j=0; j<3; j++)
            {
                if(arr[i][j]=='X')
                    x++;
                else if(arr[i][j]=='O')
                    z++;
            }
        }
        if(x!=z&&x!=(z+1))
        {
            printf("no\n");
            continue;
        }
        if((arr[0][0]=='X'&&arr[0][1]=='X'&&arr[0][2]=='X')||
                (arr[1][0]=='X'&&arr[1][1]=='X'&&arr[1][2]=='X')||
                (arr[2][0]=='X'&&arr[2][1]=='X'&&arr[2][2]=='X')||
                (arr[0][0]=='X'&&arr[1][0]=='X'&&arr[2][0]=='X')||
                (arr[0][1]=='X'&&arr[1][1]=='X'&&arr[2][1]=='X')||
                (arr[0][2]=='X'&&arr[1][2]=='X'&&arr[2][2]=='X')||
                (arr[0][0]=='X'&&arr[1][1]=='X'&&arr[2][2]=='X')||
                (arr[2][0]=='X'&&arr[1][1]=='X'&&arr[0][2]=='X'))
            xx=1;
        if((arr[0][0]=='O'&&arr[0][1]=='O'&&arr[0][2]=='O')||
                (arr[1][0]=='O'&&arr[1][1]=='O'&&arr[1][2]=='O')||
                (arr[2][0]=='O'&&arr[2][1]=='O'&&arr[2][2]=='O')||
                (arr[0][0]=='O'&&arr[1][0]=='O'&&arr[2][0]=='O')||
                (arr[0][1]=='O'&&arr[1][1]=='O'&&arr[2][1]=='O')||
                (arr[0][2]=='O'&&arr[1][2]=='O'&&arr[2][2]=='O')||
                (arr[0][0]=='O'&&arr[1][1]=='O'&&arr[2][2]=='O')||
                (arr[2][0]=='O'&&arr[1][1]=='O'&&arr[0][2]=='O'))
            zz=1;
        if(zz+xx>1)
            printf("no\n");
        else if(x==z&&xx==1)
            printf("no\n");
        else if(x==(z+1)&&zz==1)
            printf("no\n");
        else
            printf("yes\n");

    }
    return 0;
}


