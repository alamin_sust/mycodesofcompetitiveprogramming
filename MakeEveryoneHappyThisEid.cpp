/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int dp[52][840][840],arr[55],n;

int rec(int pos,int rem1,int rem2)
{
    if(rem1<0||rem2<0)
        return 0;
    if(rem1==0&&rem2==0)
        return 1;
    if(pos>=n)
        return 0;
    int &ret=dp[pos][rem1][rem2];
    if(ret!=-1)
        return ret;
    ret=0;
    ret|=rec(pos+1,rem1-arr[pos],rem2);
    ret|=rec(pos+1,rem1,rem2-arr[pos]);
    ret|=rec(pos+1,rem1,rem2);
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    int i,j,k,p,t,sum,res;
    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {
        scanf(" %d",&n);
        sum=0;
        for(i=0; i<n; i++)
            scanf(" %d",&arr[i]),sum+=arr[i];
        sort(arr,arr+n);
        reverse(arr,arr+n);
        printf("Case %d: ",p);
        if((sum%3)!=0)
        {
            printf(":(");
           // if(p!=t)
                printf("\n");
            continue;
        }
        for(i=0; i<=n; i++)
        {
            for(j=0; j<=(sum/3); j++)
            {
                for(k=0; k<=(sum/3); k++)
                {
                    dp[i][j][k]=-1;
                }

            }
        }
        res=rec(0,sum/3,sum/3);
        if(res)
            printf("Eid Makes Happy Everyone");
        else
            printf(":(");
       // if(p!=t)
                printf("\n");
    }

    return 0;
}






