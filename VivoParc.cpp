/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int col[103],n;
vector<int>adj[103];

int bfs(int cage)
{
    if(cage>n)
        return 1;
    int ret=0;
    for(int i=1;i<=4;i++)
    {
        col[cage]=i;
        for(int j=0;j<adj[cage].size();j++)
        {
            if(col[adj[cage][j]]==col[cage])
                break;
            if(j==adj[cage].size()-1)
                ret|=bfs(cage+1);
        }
        if(ret)
            return ret;
        col[cage]=0;
    }
    return ret;
}

int main()
{
    int l,p=0,from,to;
    char arr[110];
    while(cin>>n)
    {
        if(p)
            printf("\n");
        p++;
        for(int i=1;i<=n;i++)
            adj[i].clear(),col[i]=0;
        getchar();
        while(gets(arr))
        {
            l=strlen(arr);
            if(!l)
                break;
            from=to=0;
            int i=0;
            while(arr[i]!='-')
            {
                from=(from*10+arr[i]-'0');
                i++;
            }
            i++;
            while(i<l)
            {
                to=(to*10+arr[i]-'0');
                i++;
            }
            adj[from].push_back(to);
            adj[to].push_back(from);
        }
        bfs(1);
        for(int i=1;i<=n;i++)
            printf("%d %d\n",i,col[i]);
        if(l)
            break;
    }
    return 0;
}

