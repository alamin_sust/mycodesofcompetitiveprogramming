/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll t,p,a,b,c,i,j,res,n,aa,bb,arr[1000010];

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++) {
        res=0;
        scanf(" %lld %lld %lld %lld",&n,&a,&b,&c);

//        for(i=1LL;i<=a;i++) {
//            if(n%i)continue;
//            aa=n/i;
//            for(j=max(1LL,aa/c);j<=b;j++) {
//                if(aa%j)continue;
//                if((i*j)>n)break;
//                bb=aa/j;
//                if(bb<=c)res++;
//                //else break;
//            }
//        }

        ll mx=max(a,max(b,c));

        ll k=0LL;
        for(i=1LL;i<=mx;i++) {
            if(n%i)continue;
            arr[k++]=i;
        }
        //printf("%lld\n",k);
        for(i=0;i<k&&arr[i]<=a;i++) {
            if(n%arr[i])continue;
            aa=n/arr[i];
            for(j=0;j<k&&arr[j]<=b;j++) {
                if(aa%arr[j])continue;
                if((arr[i]*arr[j])>n)break;
                bb=aa/arr[j];
                if(bb<=c)res++;
                //else break;
            }
        }

        printf("%lld\n",res);
    }


    return 0;
}
