/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int dp[103][1003],n,m,cost[110],flag[1000010],len[110],arr[110],l,adj[110][110];

int rec(int pos,int taken,int length)
{
    if(pos==m)
        return 1;
    int &ret=dp[pos][length];
    if(ret!=-1)
        return ret;
    ret=0;
    flag[length]=1;
    if((taken+cost[pos])<=l)
    {
        flag[length+len[pos]]=1;
        ret=rec(pos+1,taken+cost[pos],length+len[pos]);
    }
    ret=rec(pos+1,taken,length);
    return ret;
}


int main()
{
    int t,p,i,j,k;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        memset(flag,0,sizeof(flag));
        scanf(" %d %d %d",&n,&m,&l);
        for(i=0;i<n;i++)
        {
            scanf(" %d",&arr[i]);
        }
        sort(arr,arr+n);
        for(i=0;i<m;i++)
        {
            scanf(" %d %d",&cost[i],&len[i]);
        }
        memset(dp,-1,sizeof(dp));
        rec(0,0,0);
         //  for(i=0;i<100;i++)
          //  {
           //     printf("%d ",flag[i]);
           // }
        for(i=0;i<n;i++)
        {
            for(j=0;j<n;j++)
            {
                if(i==j)
                    adj[i][j]=0;
                else if(flag[abs(arr[i]-arr[j])])
                    {
                    adj[i][j]=1;
                    //printf("%d %d..\n",i,j);
                    }
                else adj[i][j]=99999999;
            }
        }
       // printf("\n");
        for(k=0;k<n;k++)
        {
            for(i=0;i<n;i++)
            {
                for(j=0;j<n;j++)
                {
                    if(adj[i][j]>(adj[i][k]+adj[k][j]))
                        adj[i][j]=adj[i][k]+adj[k][j];
                        //printf("%d %d==%d\n",i,j,adj[i][j]);
                }
            }
        }
        if(adj[0][n-1]>=99999999)
            printf("-1\n");
        else
            printf("%d\n",adj[0][n-1]);
    }
    return 0;
}

