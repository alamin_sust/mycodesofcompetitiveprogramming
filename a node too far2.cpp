#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};

int val[40],n,flag[40],adj[40][40],res,det[1000010];

void bfs(int x,int ttl)
{
    queue<int>q;
    q.push(x);
    res++;
    while(!q.empty())
    {
        int pr=q.front();
        flag[pr]=1;
        //printf("%d %d\n",n,ttl);
        q.pop();
        if(val[pr]<ttl)
        for(int i=1;i<=n;i++)
        {
           // printf("]]");
            if(adj[pr][i]==1&&flag[i]==0)
            {
              val[i]=val[pr]+1;
              flag[i]=1;
              res++;
              q.push(i);
             // printf("%d\n",i);
            }
        }
        else
            break;
    }
    return;
}

main()
{
    int m,i,f,t,x,ttl,p=1;
    while(1)
    {
        cin>>m;
        if(m==0)
            break;
        memset(adj,0,sizeof(adj));
        memset(det,0,sizeof(det));
        for(n=0,i=1;i<=m;i++)
        {
            scanf(" %d %d",&f,&t);
            if(det[f]==0)
                det[f]=++n;
            if(det[t]==0)
                det[t]=++n;
            adj[det[f]][det[t]]=1;
            adj[det[t]][det[f]]=1;
            //printf("%d.%d\n",det[f],det[t]);
        }
        while(1)
        {
            memset(flag,0,sizeof(flag));
            memset(val,0,sizeof(val));
            scanf(" %d %d",&x,&ttl);
            if(x==0&&ttl==0)
                break;
            res=0;
           // printf("%d\n",det[x]);
            bfs(det[x],ttl);
            printf("Case %d: %d nodes not reachable from node %d with TTL = %d.\n",p++,n-res,x,ttl);
        }
    }
    return 0;
}

