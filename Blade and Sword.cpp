/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int rr[]={0,0,1,-1};
int cc[]={1,-1,0,0};

queue<pair<int,int> >q;
int cnt,cnt2,flag,row,col,stat[205][205],val[205][205],p,res,star1x,star1y,star2x,star2y,star1,star2;
char arr[205][205];
struct node{
int x,y,val;
};

vector<node>v1,v2;
node det;

int comp(node a,node b)
{
    return a.val<b.val;
}

void bfs(int x,int y)
{
    memset(stat,0,sizeof(stat));
    memset(val,0,sizeof(val));

    //for(int i=0;i<10;i++)
    //{
    //    for(int j=0;j<10;j++)
    //    {
    //        printf("%d.",val[i][j]);
    //    }
    //    printf("\n");
    //}

    while(!q.empty())
        q.pop();
    q.push(make_pair(x,y));
    stat[x][y]=1;
    val[x][y]=0;

    while(!q.empty())
    {
        int ux=q.front().first;
        int uy=q.front().second;
        q.pop();
        int vx,vy;
        for(int i=0; i<4; i++)
        {
            vx=ux+rr[i];
            vy=uy+cc[i];
            if(vx<0||vy<0||vx>=row||vy>=col)
                continue;
            if((arr[vx][vy]=='.'||arr[vx][vy]=='D')&&stat[vx][vy]==0)
            {
                val[vx][vy]=val[ux][uy]+1;
                if(arr[vx][vy]=='D')
                {
                    res=min(res,val[vx][vy]);
                    return;
                }
                q.push(make_pair(vx,vy));
                stat[vx][vy]=1;
            }
        }
    }
    return;
}

void bfs2(int x,int y)
{
    memset(stat,0,sizeof(stat));
    memset(val,0,sizeof(val));
    while(!q.empty())
        q.pop();
    q.push(make_pair(x,y));
    stat[x][y]=1;
    val[x][y]=0;

    while(!q.empty())
    {
        int ux=q.front().first;
        int uy=q.front().second;
        q.pop();
        int vx,vy;
        for(int i=0; i<4; i++)
        {
            vx=ux+rr[i];
            vy=uy+cc[i];
            if(vx<0||vy<0||vx>=row||vy>=col)
                continue;
            if(arr[vx][vy]!='#'&&stat[vx][vy]==0)
            {
               if(arr[vx][vy]=='.')
               {
                    q.push(make_pair(vx,vy));
                    val[vx][vy]=val[ux][uy]+1;
                }
                if(arr[vx][vy]=='*')
                {
                    det.val=val[ux][uy]+1;
                    det.x=vx;
                    det.y=vy;
                    v1.push_back(det);
                }
                stat[vx][vy]=1;
            }
        }
    }
    return;
}


void bfs3(int x,int y)
{
    memset(stat,0,sizeof(stat));
    memset(val,0,sizeof(val));
    while(!q.empty())
        q.pop();
    q.push(make_pair(x,y));
    stat[x][y]=1;
    val[x][y]=0;

    while(!q.empty())
    {
        int ux=q.front().first;
        int uy=q.front().second;
        q.pop();
        int vx,vy;
        for(int i=0; i<4; i++)
        {
            vx=ux+rr[i];
            vy=uy+cc[i];
            if(vx<0||vy<0||vx>=row||vy>=col)
                continue;
            if(arr[vx][vy]!='#'&&stat[vx][vy]==0)
            {
               if(arr[vx][vy]=='.')
                {
                    q.push(make_pair(vx,vy));
                    val[vx][vy]=val[ux][uy]+1;
                }
                if(arr[vx][vy]=='*')
                {
                    det.val=val[ux][uy]+1;
                    det.x=vx;
                    det.y=vy;
                    v2.push_back(det);
                }
                stat[vx][vy]=1;
            }
        }
    }
    return;
}


int main()
{

    int t,i,j,sx,sy,dx,dy;
    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {
        res=star1=star2=9999999;
        v1.clear();
        v2.clear();
        int cnt=0;
        scanf(" %d %d",&row,&col);
        for(i=0;i<=row+1;i++)
            for(j=0;j<=col+1;j++)
            arr[i][j]='#';
       // getchar();
        for(i=0; i<row; i++)
        {
            scanf(" %s",&arr[i]);
            for(j=0; j<col; j++)
            {
                if(arr[i][j]=='*')
                    cnt++;
                if(arr[i][j]=='P')
                    sx=i,sy=j;
                if(arr[i][j]=='D')
                    dx=i,dy=j;
            }
            //getchar();
        }
        bfs(sx,sy);
        bfs2(sx,sy);
        bfs3(dx,dy);
        //res=9999999;
        sort(v1.begin(),v1.end(),comp);
        sort(v2.begin(),v2.end(),comp);
       /* if(v1.size()>=1&&v2.size()>=1&&(v1[0].x!=v2[0].x||v1[0].y!=v2[0].y))
        {
            res=min(res,v1[0].val+v2[0].val+1);
        }
        else if(v1.size()>1&&v2.size()>1)
        {
            res=min(res,min(v1[0].val+v2[1].val+1,v1[1].val+v2[0].val+1));
        }
        else if(v1.size()>1&&v2.size()>=1)
        {
            res=min(res,v1[1].val+v2[0].val+1);
        }
        else if(v2.size()>1&&v1.size()>=1)
        {
            res=min(res,v1[0].val+v2[1].val+1);
        }
        */

        /*if(v1.size()>=1&&v2.size()>=1)
        {
            if((v1[0].x!=v2[0].x||v1[0].y!=v2[0].y))
            res=min(res,v1[0].val+v2[0].val+1);
            else if(v1.size()>1&&v2.size()>1)
            {
                res=min(res,min(min(v1[1].val+v2[0].val+1,v1[0].val+v2[1].val+1),min(v1[0].val+v2[0].val+2,v1[1].val+v2[1].val+1)));
            }
            else if(v1.size()>=1&&v2.size()>1)
            {
                res=min(res,min(v1[0].val+v2[1].val+1,v1[0].val+v2[0].val+2));
            }
            else if(v1.size()>1&&v2.size()>=1)
            {
                res=min(res,min(v1[1].val+v2[0].val+1,v1[0].val+v2[0].val+2));
            }
        }*/

        if(v1.size()>0&&v2.size()>0&&(v1[0].x!=v2[0].x||v1[0].y!=v2[0].y))
            res=min(res,v1[0].val+v2[0].val+1);
        else if(v1.size()>0&&v2.size()>0&&v1[0].x==v2[0].x&&v1[0].y==v2[0].y)
        {
            if(v1.size()>1&&v2.size()>1)
            {
                res=min(res,min(min(v1[1].val+v2[0].val+1,v1[0].val+v2[1].val+1),v1[0].val+v2[0].val+2));
            }
            else if(v1.size()>=1&&v2.size()>1)
            {
                res=min(res,min(v1[0].val+v2[1].val+1,v1[0].val+v2[0].val+2));
            }
            else if(v1.size()>1&&v2.size()>=1)
            {
                res=min(res,min(v1[1].val+v2[0].val+1,v1[0].val+v2[0].val+2));
            }
            else if(cnt>1)
            {
                res=min(res,v1[0].val+v2[0].val+2);
            }
        }

        if(res<9999999)
            printf("Case %d: %d\n",p,res);
        else
            printf("Case %d: impossible\n",p);
    }
    return 0;
}
/*
100
4 10
##########
#.P..#*..#
#*......D#
##########
3 9
#########
#P.#..D.#
#########
4 5
#####
##*##
#P*D#
#####
4 4
####
#P*#
#*D#
####
5 5
#####
#*#*#
#DP*#
#####
#####
5 5
#####
##*##
#P*D#
#*###
#####
5 5
#####
##*##
#D*P#
#*###
#####
5 5
#####
#***#
#D*P#
#***#
#####
5 5
#####
#.*.#
#D*P#
#####
#####
5 5
#####
#.**#
#D*P#
#####
#####
5 5
#####
##D*#
#**##
##P##
#####
5 5
#####
#*#P#
#D**#
#...#
#####
6 5
#####
##D*#
#**##
##P##
##*##
#####
6 5
#####
##D##
##*##
#####
#*P##
#####
6 7
#######
#.*.###
#D#*###
#####P#
###***#
#######
7 5
#####
#*D*#
#####
#*.P#
##..#
##*##
#####
7 5
#####
#*.*#
##*##
#D*P#
#**.#
##*##
#####
6 7
#######
#D##P.#
#.#*#.#
#.###.#
#.*#*.#
#######
6 7
#######
#D..P.#
#.#*#.#
#.###.#
#.*#*.#
#######
6 5
#####
#P#.#
#.#.#
#*.D#
##*##
#####
4 6
######
#D*#P#
#*####
######
7 6
######
#.*.##
#.#.##
#P#D##
#.#.##
#.*.##
######
*/
