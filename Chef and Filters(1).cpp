/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))
#define MOD 1000000007LL

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

//map<pair<ll,ll> ,ll>mppncr;
ll val,n,m,arr[1030],dp[1027][1027],mpp[1030],N,R,fact[100010],mfact[100010];
char str[12];
vector<ll>v[100010];

ll pow(ll a,ll b,ll M)
{
    ll x=1,y=a;
    while(b>0)
    {
        if(b%2)
        {
            x=(x*y);
            if(x>M)
                x%=M;
        }
        y=(y*y);
        if(y>M)
            y%=M;
        b/=2;
    }
    return x;
}

ll modInverse(ll a)
{
    return pow(a,MOD-2,MOD);
}

ll rec(ll pos,ll nowval)
{
    if(pos==n)
    {
        if(nowval==0)
            return 1;
        return 0;
    }
    ll &ret=dp[pos][nowval];
    if(ret!=-1)
        return ret;
    ret=0;
    if(mpp[arr[pos]]%2)
    {
    ret=(ret+(v[pos][mpp[arr[pos]]-1]*rec(pos+1,nowval))%MOD)%MOD;
    ret=(ret+(v[pos][mpp[arr[pos]]]*rec(pos+1,nowval^arr[pos]))%MOD)%MOD;
    }
    else
    {
    ret=(ret+(v[pos][mpp[arr[pos]]]*rec(pos+1,nowval))%MOD)%MOD;
    ret=(ret+(v[pos][mpp[arr[pos]]-1]*rec(pos+1,nowval^arr[pos]))%MOD)%MOD;
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    ll t,p,i,j,val2;
    fact[0]=1LL;
    mfact[0]=modInverse(fact[0]);
    for(i=1;i<=100000;i++)
    {
        fact[i]=(i*fact[i-1])%MOD;
        mfact[i]=modInverse(fact[i]);
    }

    scanf(" %lld",&t);

    for(p=1;p<=t;p++)
    {
       // mppncr.clear();
        memset(mpp,0,sizeof(mpp));
        scanf(" %s",str);
        val=0;
        for(i=0;i<10;i++)
            val=(val<<1)+(str[i]=='w'?1:0);
        scanf(" %lld",&m);
        n=0;
        for(i=0;i<m;i++)
        {
            scanf(" %s",str);
            val2=0;
            for(j=0;j<10;j++)
            val2=(val2<<1)+(str[j]=='+'?1:0);
            if(mpp[val2]==0)
            {
                arr[n++]=val2;
            }
            mpp[val2]++;
        }
        for(i=0;i<n;i++)
        {
            v[i].clear();
            N=mpp[arr[i]];
            for(j=0;j<=N;j++)
            {
                R=j;
                v[i].push_back((((fact[N]*mfact[N-R])%MOD)*mfact[R])%MOD);
            }
            for(j=2;j<=N;j++)
            {
                v[i][j]=(v[i][j]+v[i][j-2])%MOD;
            }
        }
        memset(dp,-1,sizeof(dp));
        printf("%lld\n",rec(0,val));
    }

    return 0;
}

