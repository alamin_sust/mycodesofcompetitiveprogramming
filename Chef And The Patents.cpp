/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll t,p,n,m,x,k,i,e,o,res;
char arr[1000010];

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++) {

        scanf(" %lld %lld %lld %lld",&n,&m,&x,&k);

        scanf(" %s",arr);

        e=o=0;

        for(i=0LL;i<k;i++) {
            if(arr[i]=='O')o++;
            else e++;
        }

        res=min((m+1LL)/2LL*x,o)+min(m/2LL*x,e);

        if(res>=n)printf("yes\n");
        else printf("no\n");
    }


    return 0;
}
