/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node{
int sk,pos;
};

node det,arr[200010];

int i,n,now,res[200010];

priority_queue<node>pq[5];

bool operator<(node a,node b)
{
    return a.sk>b.sk;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d",&n);

    for(i=1;i<=n;i++)
    {
        scanf(" %d",&arr[i].sk);
        arr[i].pos=i;
        if((arr[i].sk%3)==0)
            pq[0].push(arr[i]);
        else if((arr[i].sk%3)==1)
            pq[1].push(arr[i]);
        else if((arr[i].sk%3)==2)
            pq[2].push(arr[i]);
    }
    now=0;
    for(i=1;i<=n;i++)
    {
        if(pq[now].empty())
        {
            printf("Impossible\n");
            return 0;
        }
        det=pq[now].top();
        pq[now].pop();
        if(det.sk>avail)
        {
            printf("Impossible\n");
            return 0;
        }
        now++;
        if(now>2)
            now=0;
        res[i]=det.pos;
        avail=det.sk+1;
    }
    printf("Possible\n");
    for(i=1;i<=n;i++)
    {
        printf("%d ",res[i]);
    }
    printf("\n");
    return 0;
}





