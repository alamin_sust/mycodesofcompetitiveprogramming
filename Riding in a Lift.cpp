/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll dp[5010][5010],n,a,b,k;

ll rec(ll pos,ll trip)
{
    //printf("%lld %lld\n",pos,trip);
    if(trip==k)
        return 1LL;
    ll &ret=dp[pos][trip];
    if(ret!=-1)
        return ret;
    ret=0;
    ll mx=abs(pos-b)-1LL;
    for(ll i=1;i<=mx;i++)
    {
        if(pos-i>=1)
            ret=(ret+rec(pos-i,trip+1))%1000000007LL;
        if(pos+i<=n)
            ret=(ret+rec(pos+i,trip+1))%1000000007LL;
    }
    return ret;
}

int main()
{
    cin>>n>>a>>b>>k;
    memset(dp,-1,sizeof(dp));
    cout<<rec(a,0)<<endl;
    return 0;
}








