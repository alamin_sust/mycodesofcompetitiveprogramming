
/*
 *  * @ author : Amit Jaspal(IIIT-Allahabad)
 *   */
#include <list>
#include<iostream>
#include <vector>
#include<map>
#include<string>
//#include<sstream>
#include<iomanip>
#include<vector>
#include<algorithm>
#include<stdio.h>
using namespace std;

#define vi vector<int>
#define vs vector<string>
#define pb push_back
#define ll long long int
#define sm (int)13e7 /* MAX_INT */

/* Functions */

struct node{
   int i;
};
bool operator<(const node &leftNode, const node &rightNode) {
       if (leftNode.i != rightNode.i) return leftNode.i < rightNode.i; //for increasing order;
}
//string inttostr(int i){   ostringstream o;o<<i;return o.str();}


///////////////////////////.............Code starts from here............///////////////////////////////////////
int memo[600][600];
int x,y;
vector<string>a,b;

int lcs(int i,int j){
   if(i<x && j<y){
      if(memo[i][j]==0){
            if(a[i]==b[j]){
                  memo[i][j]=1+lcs(i+1,j+1);// ?? why not i+1,j+1
                  return memo[i][j];
            }else{
                  memo[i][j]=max(lcs(i,j+1),lcs(i+1,j));
                  return memo[i][j];
            }
      }else{
            return memo[i][j];
      }
   }else{
         return 0;
   }
}

int main(){

   int i,j,count=0;;

   char ch;
   while(1){
         string s[600],s1[600];
         int i1=0,flag=0,i2=0;
         int yy=0,zz=0;
         // Taking the first string as input
         while(ch=getchar()){

            if(ch=='\n' || ch==EOF){
               if(s[i1].size()>0){
               a.push_back(s[i1]);}
               if(ch==EOF){
                  flag=1;}
               break;
            }
            else if((ch>=65 && ch<=90) || (ch>=97 && ch <=122) ||(ch>=48 && ch<=57)){
               s[i1]=s[i1]+ch;
            }
            else{
               if(s[i1].size()>0){
                  a.push_back(s[i1]);
               }
               i1++;
            }
            yy++;
         }
         /*for(i=0;i<a.size();i++){
               cout<<a[i]<<"  ";
         }
         cout<<endl;*/

         if(flag==1){break;}

         // Taking the second string as input
         while(ch=getchar()){

            if(ch=='\n' || ch==EOF){
               if(s1[i2].size()>0){
               b.push_back(s1[i2]);}
               if(ch==EOF){
                  flag=1;}
               break;
            }
            else if((ch>=65 && ch<=90) || (ch>=97 && ch <=122) || (ch>=48 && ch<=57)){
               s1[i2]=s1[i2]+ch;
            }
            else{
               if(s1[i2].size()>0){
                  b.push_back(s1[i2]);
               }
               i2++;
            }
            zz++;
         }
         /*for(i=0;i<b.size();i++){
               cout<<b[i]<<"  ";
         }
         cout<<endl;*/
      //   cout<<yy<<"          "<<zz<<endl;
         count++;
         x=a.size();
         y=b.size();
         if(yy<=1 || zz<=1){
            cout<<count<<". Blank!"<<endl;
            //count++;
            if(flag==1){break;}
            else {continue;}
            }
         //cout<<x<<"    kkkkkkkkkkkkkkk    "<<y<<endl;


         // Applying D.P
         for( i=0;i<=x;i++){
            for( j=0;j<=y;j++){
               memo[i][j]=0;
            }
         }
         //a.clear();
         //b.clear();
         int length=lcs(0,0);
         //if(length!=0){
         cout<<count<<". Length of longest match: "<<length<<endl;
         //}else{}
         a.clear();
         b.clear();

         if(flag==1){
            break;
         }
   }

   return 0;
}
