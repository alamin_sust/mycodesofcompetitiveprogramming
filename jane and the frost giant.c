#include<stdio.h>
#include<string.h>

int val1[210][210],val2[210][210];
char arr[210][210];

void rec1(int i,int j,int curr)
{
    if(val1[i][j-1]>curr&&arr[i][j-1]!='#')
    {
        val1[i][j-1]=curr;
        rec1(i,j-1,curr+1);
    }
    if(val1[i-1][j]>curr&&arr[i-1][j]!='#')
    {
        val1[i-1][j]=curr;
        rec1(i-1,j,curr+1);
    }
    if(val1[i][j+1]>curr&&arr[i][j+1]!='#')
    {
        val1[i][j+1]=curr;
        rec1(i,j+1,curr+1);
    }
    if(val1[i+1][j]>curr&&arr[i+1][j]!='#')
    {
        val1[i+1][j]=curr;
        rec1(i+1,j,curr+1);
    }
    return;
}

void rec2(int i,int j,int curr)
{
    if(val2[i][j-1]>curr&&arr[i][j-1]!='#')
    {
        val2[i][j-1]=curr;
        rec2(i,j-1,curr+1);
    }
    if(val2[i-1][j]>curr&&arr[i-1][j]!='#')
    {
        val2[i-1][j]=curr;
        rec2(i-1,j,curr+1);
    }
    if(val2[i][j+1]>curr&&arr[i][j+1]!='#')
    {
        val2[i][j+1]=curr;
        rec2(i,j+1,curr+1);
    }
    if(val2[i+1][j]>curr&&arr[i+1][j]!='#')
    {
        val2[i+1][j]=curr;
        rec2(i+1,j,curr+1);
    }
    return;
}

main()
{
    int i,j,row,col,t,x1,x2,y1,y2,p,mx;
    scanf(" %d",&t);
    for(p=1; p<=t; p++)
    {
        scanf(" %d %d",&row,&col);
        getchar();
        for(i=0; i<=row+1; i++)
        {
            for(j=0; j<=col+1; j++)
            {
                arr[i][j]='#';
                val1[i][j]=999999;
                val2[i][j]=999999;
            }
        }
        for(i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
            {
                scanf("%c",&arr[i][j]);
                if(arr[i][j]=='J')
                {
                    x1=i;
                    y1=j;
                }
                if(arr[i][j]=='F')
                {
                    x2=i;
                    y2=j;
                }
            }
            getchar();
        }
        val1[x1][y1]=0;
        val2[x2][y2]=0;
        rec1(x1,y1,1);
        rec2(x2,y2,1);
        for(i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
            {
                if(val1[i][j]>=val2[i][j])
                    val1[i][j]=999999;
            }
        }
        for(mx=999999,j=1,i=1; i<=row; i++)
        {
            if(val1[i][j]<mx)
                mx=val1[i][j];
        }
        for(j=1,i=1; i<=col; i++)
        {
            if(val1[j][i]<mx)
                mx=val1[j][i];
        }
        for(j=col,i=1; i<=row; i++)
        {
            if(val1[i][j]<mx)
                mx=val1[i][j];
        }
        for(j=row,i=1; i<=col; i++)
        {
            if(val1[j][i]<mx)
                mx=val1[j][i];
        }
        if(mx==999999)
            printf("Case 2: IMPOSSIBLE\n",p);
        else
            printf("Case %d: %d\n",p,mx+1);
    }
    return 0;
}
