/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define llu long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

char arr1[110],arr2[110];
int need,pos,i,k,res,wp,qs,l,cp,ones;

int main()
{
    int xxx=2;
    int yyy=3;
    double mmm=(double)(yyy/xxx);
    printf("%lf",mmm);
    scanf(" %s",&arr1);
    scanf(" %s",&arr2);
    l=strlen(arr1);
    cp=0;
    for(i=0;i<l;i++)
    {
        if(arr1[i]=='+')
            cp++;
        else
            cp--;
    }
    wp=0;
    qs=0;
    for(i=0;i<l;i++)
    {
        if(arr2[i]=='+')
            wp++;
        else if(arr2[i]=='-')
            wp--;
        else qs++;
    }
    if(qs==0)
    {
        if(wp==cp)
            printf("1.000000000000\n");
        else
            printf("0.000000000000\n");
        return 0;
    }
    need=cp-wp;
    k=(1<<qs);
    res=0;
    for(i=0;i<k;i++)
    {
        ones=__builtin_popcount(i);
        pos=(qs-ones)-ones;
        if(pos==need)
            res++;
    }
    double aa,bb;
    aa=(double)k;
    bb=(double)res;
    printf("%.12lf\n",bb/aa);
    return 0;
}

