#include<stdio.h>
#include<iostream>
using namespace std;
struct node{
    int val;
    node *ptr;
};

struct singly{
     node *head,*tail;
     singly(){head=tail=NULL;}
     void insert(int x);
     void print();
     void delhead();
     void deltail();
     void delelement(int x);
     void insertinto(int x,int pos);
};

void singly::insert(int x)
{
    node *temp= new node;
    temp->val=x;
    temp->ptr=NULL;
    if(head==NULL)
    {head=tail=temp;
    return;}
    tail->ptr=temp;
    tail=temp;
    return;
}

void singly::print()
{
    node *temp=head;
    if(head==NULL)
    {printf("EMPTY\n");
    return;}
    while(temp!=NULL)
    {
        cout<<temp->val<<" ";
        temp=temp->ptr;
    }
    printf("\n");
    return;
}

void singly::delhead()
{
    if(head==NULL)
    {printf("ARE U KIDDING?\n");
    return;}
    head=head->ptr;
    return;
}

void singly::deltail()
{
    node *temp=head;
    node *temp2=new node;
    if(temp->ptr==NULL)
    {head=tail=NULL;
    return;}
    while(1)
    {
        if(temp->ptr==NULL)
        {temp2->ptr=NULL;
        tail=temp2;
        break;}
        temp2=temp;
        temp=temp->ptr;
    }
    return;
}

void singly::delelement(int x)
{
    node *temp=head,*temp2=new node;
    if(head->val==x)
    {delhead();
    return;}
    while(1)
    {
        if(temp->val==x)
        {
            if(temp->ptr==NULL)
            {deltail();
            break;}
            else
            {
                temp2->ptr=temp->ptr;
                break;
            }
        }
        temp2=temp;
        temp=temp->ptr;
    }
    return;
}

void singly::insertinto(int x,int pos)
{
    int i;
    node *temp=new node,*temp2=new node,*temp3=new node;
    temp3->val=x;
    temp3->ptr=NULL;
    if(pos==1)
    {
        temp->val=x;
        temp->ptr=head;
        head=temp;
        return;
    }
    temp=head;
    for(i=1;;i++)
    {
        if(i==pos)
        {
            if(temp2->ptr==NULL)
            {
                temp2->ptr=temp3;
                tail=temp3;
            }
            else
            {
                temp3->ptr=temp;
                temp2->ptr=temp3;
            }
            break;

        }
        temp2=temp;
        temp=temp->ptr;
    }
    return;
}

main()
{
    singly s;
    s.insert(5);
    s.insert(6);
    s.insert(7);
    s.insert(8);
    s.print();
    s.delelement(7);
    //s.delhead();
    s.print();
    s.insertinto(99,1);
    //s.deltail();
    //s.deltail();
    s.print();
    return 0;
}
