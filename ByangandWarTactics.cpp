/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll t,p,i,j,k,n,q,arr[1010],arr2[1010],com,x,y,flag[1010];
map<ll,ll>mpp,mpp2;


int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %lld",&t);

    for(p=1;p<=t;p++)
    {
        mpp.clear();
        mpp2.clear();
        k=0;
        scanf(" %lld %lld",&n,&q);
        for(i=1;i<=n;i++)
        {
            scanf(" %lld",&arr[i]);
            if(mpp[arr[i]]==0)
            mpp[arr[i]]=++k;
            arr2[i]=mpp[arr[i]];
            mpp2[mpp[arr[i]]]=arr[i];
        }
        printf("Case %lld:\n",p);
        for(i=1;i<=q;i++)
        {
            scanf(" %lld %lld %lld",&com,&x,&y);
            if(com==1)
            {
                for(j=x,k=y;j<k;j++,k--)
                {
                    swap(arr2[j],arr2[k]);
                }
            }
            else
            {
                memset(flag,0,sizeof(flag));
                for(j=x;j<=y;j++)
                {
                    flag[arr2[j]]++;
                }
                ll cum=0;
                for(j=0;j<=1005;j++)
                {
                    cum+=flag[j];
                    if(cum>=(x-y+1)/2+1)
                        {printf("%lld\n",mpp[j]);
                        break;}
                }
                //stable_sort(arr2+x,arr2+y+1);
                //printf("%lld\n",arr2[(x+y)/2]);
            }
        }
    }

    return 0;
}

