    #include <iostream>
    #include <string>

    using namespace std;

    void makeCombinations(string prefix, string chars){
        int len = chars.length();
        int i=0;

        if (prefix.length() < len){
            for (i=0; i<len; i++){
                cout << prefix << chars[i] <<endl;
                makeCombinations(prefix+chars[i], chars);
            }
        }
    }

    int main(int argc, char **argv){
        string charsToUse;

        cout << "Enter a list of characters to use. Eg, abcde\n";
        cin >> charsToUse;

        makeCombinations("", charsToUse);
        return 0;
    }
