#include<stdio.h>

int main()
{
    long long int n,ct=1,i,j,res,max,arr[30];
    while(scanf("%lld",&n)!=EOF)
    {
        for(i=0;i<n;i++)
        {
            scanf(" %lld",&arr[i]);
        }
        for(res=0,i=0;i<n;i++)
        {
            for(max=1,j=i;j<n;j++)
            {
                max*=arr[j];
                if(max>res)
                res=max;
            }
        }
        printf("Case #%lld: The maximum product is %lld.\n\n",ct,res);
        ct++;
    }
    return 0;
}
