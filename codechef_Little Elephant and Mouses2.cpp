#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
#define up 1
#define left 2
#define both 3
using namespace std;

int t,i,j,row,p,arr[110][110],col,a,b;
char in[110][110];

typedef struct
{
    int val;
    int dir;
} details;

details res[110][110];

main()
{
    scanf(" %d",&t);
    for(p=1; p<=t; p++)
    {
        memset(res,0,sizeof(res));
        memset(arr,0,sizeof(arr));
        scanf(" %d %d",&row,&col);
        getchar();
        for(i=1; i<=row; i++)
        {
            gets(in[i]);
        }
        for(i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
            {

                arr[i][j]=in[i][j-1]-'0';
                //printf("%d ",arr[i][j]);
            }
            //printf("\n");
        }
        for(i=0; i<=col; i++)
        {
            res[0][i].dir=up;
            // res[0][i].val=arr[1][i];
        }
        for(i=0; i<=row; i++)
        {
            // res[i][0].val=arr[i][1];
            res[i][0].dir=left;
        }
        for(i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
            {
                a=b=9999999;
                if(i!=1)
                {
                    a=res[i-1][j].val+arr[i+1][j]+arr[i][j+1]+arr[i][j-1];
                    if(res[i-1][j].dir!=up)
                    {
                        a-=arr[i][j-1];
                    }
                }
                if(j!=1)
                {
                    b=res[i][j-1].val+arr[i-1][j]+arr[i+1][j]+arr[i][j+1];
                    if(res[i][j-1].dir!=left)
                    {
                        b-=arr[i-1][j];
                    }
                }
                if(a==9999999&&b==9999999)
                {
                    res[i][j].dir=left;
                    res[i][j].val=arr[i][j]+arr[i+1][j]+arr[i][j+1];
                }
                else if(a<b)
                {
                    res[i][j].dir=up;
                    res[i][j].val=a;
                }
                else if(b<a)
                {
                    res[i][j].dir=left;
                    res[i][j].val=b;
                }
                else if(a==b)
                {
                    res[i][j].dir=both;
                    res[i][j].val=a;
                }
            }
        }
        //for(i=0; i<=row; i++)
        //{
        //    for(j=0; j<=col; j++)
        //        printf("%d ",res[i][j].val);
        //    printf("\n");
        //}
        printf("%d\n",res[row][col].val);
    }
    return 0;
}

