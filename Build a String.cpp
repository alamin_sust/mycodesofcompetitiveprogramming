/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int p,t,n,a,b,th,l,i,res,k;
size_t bef;
string arr,str;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    cin>>t;

    for(p=1; p<=t; p++)
    {
        cin>>n>>a>>b;
        cin>>arr;

        res=0;
        th=b/a+1;
        l=n;
        str="";
        if(th>=l)
        {
            cout<<(l*a)<<endl;
            continue;
        }
        for(i=0; i<l; )
        {
            k=0;
            bef=0;
            while((i+th+k)<=l)
            {
                //printf("==%d %d %d==\n",bef,i,i+th+k);
           //     cout<<str.substr(bef,i-bef)<<" "<<arr.substr(i,th+k)<<endl;
           //     cout<<"------"<<bef<<endl;
                if((th+k)>i)
                    break;
           //     cout<<"------"<<bef<<endl;
                if(str.find(arr.substr(i,th+k),0)!=-1)
                    k++;
                else
                    break;
            }
            if(k==0)
                str+=arr[i],res+=a,i++;//printf("-%d-\n",a);
            else
            {
                str+=arr.substr(i,th+k-1);
                res+=b;
                i+=(th+k-1);
                //printf("-%d-\n",b);
            }
        }
        cout<<res<<endl;
    }
    return 0;
}

