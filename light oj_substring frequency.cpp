#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
using namespace std;

char S[1000010],W[1000010];
int T[1000010],res;

void make_table(int length)
{
    int i=0,j=2;
    T[0]=-1;
    T[1]=0;
    while(j<length)
    {
        if(W[j-1]==W[i])
        {
            i++;
            T[j]=i;
            j++;
        }
        else if(i>0)
        {
            i=T[i];
        }
        else
        {
            T[j]=0;
            j++;
        }
    }
    return;
}

void kmp_search(int l_S,int l_W)
{
    int i=0,j=0;
    while(i<l_S)
    {
        if(S[i+j]==W[j])
        {
            if(j==l_W-1)
            {
                res++;
                i=i+j-T[j];
                if(T[j]<0)
                    j=0;
                else
                    j=T[j];
            }
            else
                j++;
        }
        else
        {
            i=i+j-T[j];
            if(T[j]<0)
                j=0;
            else
                j=T[j];
        }
    }
    return;
}

main()
{
    int l1,l2,i,n;
    cin>>n;
    getchar();
    for(i=1; i<=n; i++)
    {
        memset(T,0,sizeof(T));
        gets(S);
        gets(W);
        l1=strlen(S);
        l2=strlen(W);
        make_table(l2);
        res=0;
        kmp_search(l1,l2);
        printf("Case %d: %d\n",i,res);
    }
    return 0;
}
/*
4
axbyczd
abc
abcabcabcabc
abc
aabacbaabbaaz
aab
aaaaaa
aa
*/

