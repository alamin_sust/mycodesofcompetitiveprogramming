#include <sstream>
#include <string.h>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class UnrelatedPaths
{
public:
	int maxUnrelatedPaths(vector <int> parent)
	{
       int ret=0;
	   int n=parent.size();
	   int arr[55];
	   memset(arr,0,sizeof(arr));
	   for(int i=0;i<n;i++)
       {
           arr[parent[i]]=1;
       }
       for(int i=0;i<=n;i++)
       {
           if(arr[i]==0)
            ret++;
       }
       return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	UnrelatedPaths objectUnrelatedPaths;

	//test case0
	vector <int> param00;
	param00.push_back(0);
	param00.push_back(1);
	param00.push_back(1);
	param00.push_back(2);
	param00.push_back(3);
	int ret0 = objectUnrelatedPaths.maxUnrelatedPaths(param00);
	int need0 = 2;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(0);
	param10.push_back(0);
	param10.push_back(1);
	param10.push_back(1);
	param10.push_back(2);
	param10.push_back(2);
	int ret1 = objectUnrelatedPaths.maxUnrelatedPaths(param10);
	int need1 = 4;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(0);
	param20.push_back(1);
	param20.push_back(2);
	param20.push_back(3);
	int ret2 = objectUnrelatedPaths.maxUnrelatedPaths(param20);
	int need2 = 1;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(0);
	param30.push_back(1);
	param30.push_back(1);
	param30.push_back(2);
	param30.push_back(2);
	param30.push_back(2);
	param30.push_back(4);
	param30.push_back(6);
	param30.push_back(5);
	param30.push_back(0);
	param30.push_back(10);
	param30.push_back(5);
	param30.push_back(12);
	param30.push_back(12);
	param30.push_back(10);
	param30.push_back(4);
	param30.push_back(16);
	param30.push_back(12);
	param30.push_back(5);
	param30.push_back(3);
	param30.push_back(20);
	param30.push_back(12);
	param30.push_back(11);
	param30.push_back(21);
	param30.push_back(9);
	param30.push_back(5);
	param30.push_back(1);
	param30.push_back(20);
	param30.push_back(15);
	param30.push_back(24);
	param30.push_back(6);
	param30.push_back(8);
	param30.push_back(15);
	int ret3 = objectUnrelatedPaths.maxUnrelatedPaths(param30);
	int need3 = 17;
	assert_eq(3,ret3,need3);

	//test case4
	vector <int> param40;
	param40.push_back(0);
	param40.push_back(1);
	param40.push_back(1);
	param40.push_back(1);
	param40.push_back(1);
	param40.push_back(0);
	param40.push_back(2);
	param40.push_back(5);
	param40.push_back(1);
	param40.push_back(6);
	param40.push_back(7);
	param40.push_back(10);
	param40.push_back(5);
	param40.push_back(10);
	param40.push_back(8);
	param40.push_back(5);
	param40.push_back(16);
	param40.push_back(14);
	param40.push_back(8);
	param40.push_back(14);
	param40.push_back(4);
	param40.push_back(14);
	param40.push_back(15);
	param40.push_back(21);
	param40.push_back(0);
	param40.push_back(24);
	param40.push_back(11);
	param40.push_back(1);
	param40.push_back(9);
	param40.push_back(18);
	param40.push_back(13);
	param40.push_back(20);
	param40.push_back(6);
	param40.push_back(28);
	param40.push_back(19);
	param40.push_back(28);
	param40.push_back(14);
	param40.push_back(11);
	param40.push_back(38);
	param40.push_back(26);
	param40.push_back(25);
	param40.push_back(10);
	param40.push_back(23);
	param40.push_back(43);
	int ret4 = objectUnrelatedPaths.maxUnrelatedPaths(param40);
	int need4 = 19;
	assert_eq(4,ret4,need4);

}

