/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( (((double)x2-(double)x1)*((double)x2-(double)x1)) + (((double)y2-(double)y1)*((double)y2-(double)y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

double mx;
int xx[110],yy[110],px1,px2,px3,px4,py1,py2,py3,py4;
int fg[1010][1010],n,m;

double chk(int i,int j,int k,int l)
{
    double res=dist(xx[i],yy[i],xx[j],yy[j])+dist(xx[j],yy[j],xx[k],yy[k])+dist(xx[k],yy[k],xx[l],yy[l]);
    return res;
}

main()
{
    int i,j,kk,l,k;
    cin>>n>>m;
    xx[0]=0;
    yy[0]=0;
    xx[1]=n;
    yy[1]=m;
    fg[0][0]=fg[n][m]=1;
    k=2;
    if(fg[n][0]==0)
    {
        fg[n][0]=1;
        xx[k]=n;
        yy[k++]=0;
    }
    if(fg[0][m]==0)
    {
        fg[0][m]=1;
        xx[k]=0;
        yy[k++]=m;
    }
    if(n>0&&fg[n-1][0]==0)
    {
        fg[n-1][0]=1;
        xx[k]=(n-1);
        yy[k++]=0;
    }
    if(m>0&&fg[n][1]==0)
    {
        fg[n][1]=1;
        xx[k]=(n);
        yy[k++]=1;
    }
    if(n>0&&fg[n-1][m]==0)
    {
        fg[n-1][m]=1;
        xx[k]=(n-1);
        yy[k++]=m;
    }
    if(m>0&&fg[0][1]==0)
    {
        fg[0][1]=1;
        xx[k]=(0);
        yy[k++]=1;
    }
    if(m>0&&fg[0][m-1]==0)
    {
        fg[0][m-1]=1;
        xx[k]=0;
        yy[k++]=(m-1);
    }
    if(n>0&&fg[1][m]==0)
    {
        fg[1][m]=1;
        xx[k]=1;
        yy[k++]=(m);
    }
    if(m>0&&fg[n][m-1]==0)
    {
        fg[n][m-1]=1;
        xx[k]=n;
        yy[k++]=(m-1);
    }
    if(n>0&&fg[1][0]==0)
    {
        fg[1][0]=1;
        xx[k]=1;
        yy[k++]=(0);
    }
    for(i=0; i<k; i++)
    {
        for(j=0; j<k; j++)
        {
            if(j==i)
                continue;
            for(kk=0; kk<k; kk++)
            {
                if(j==kk||kk==i)
                continue;
                for(l=0; l<k; l++)
                {
                    if(l==kk||l==i||l==j)
                    continue;
                    if(chk(i,j,kk,l)>mx)
                    {
                        mx=chk(i,j,kk,l);
                        px1=xx[i];
                        py1=yy[i];
                        px2=xx[j];
                        py2=yy[j];
                        px3=xx[kk];
                        py3=yy[kk];
                        px4=xx[l];
                        py4=yy[l];
                    }
                }
            }
        }
    }
    printf("%d %d\n",px1,py1);
    printf("%d %d\n",px2,py2);
    printf("%d %d\n",px3,py3);
    printf("%d %d\n",px4,py4);
    return 0;
}


