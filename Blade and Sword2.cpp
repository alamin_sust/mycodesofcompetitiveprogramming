/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int rr[]={0,0,1,-1,0,0,1,-1};
int cc[]={-1,1,0,0,-1,1,0,0};
int pp[]={0,0,0,0,1,1,1,1};


int bfs(int x,int y)
{
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
        {
            stat[i][j][0]=stat[i][j][1]=0;
            val[i][j][0]=val[i][j][1]=inf;
        }
    }
    while(!q.empty())
        q.pop();

    q.push(x);
    q.push(y);
    q.push(0);
    stat[x][y][0]=1;
    stat[x][y][1]=1;
    val[x][y][0]=0;
    val[x][y][1]=0;

    while(!q.empty())
    {
        int ux=q.front();
        q.pop();
        int uy=q.front();
        q.pop();
        int uz=q.front();
        q.pop();
        for(int i=0; i<8; i++)
        {
            int vx=ux+rr[i];
            int vy=uy+cc[i];
            int vz=uz+pp[i];
            if(vx<0||vy<0||vx>=row||vy>=col||arr[vx][vy]=='#')
                continue;
            if(stat[vx][vy][vz]==0)
            {
                stat[vx][vy][vz]=1;
                val[vx][vy][vz]=min(val[vx][vy][vz],min(val[ux][uy][1]+1,val[ux][uy][0]+1))
                q.push(vx);
                q.push(vy);
                q.push(vz);
            }
        }
    }

    return min(val[ex][ey][0],val[ex][ey][1]);
}


int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {
        scanf(" %d %d",&row,&col);
        stars.clear();
        for(i=0; i<row; i++)
        {
            scanf(" %s",&arr[i]);
        }
        for(i=0; i<row; i++)
        {
            for(j=0; j<col; j++)
            {
                if(arr[i][j]=='*')
                {
                    stars.push_back(make_pair(i,j));
                }
                if(arr[i][j]=='P')
                {
                    sx=i;
                    sy=j;
                }
                if(arr[i][j]=='D')
                {
                    dx=i;
                    dy=j;
                }
            }
        }
        res=bfs(sx,sy);
        if(res==inf)
        {
            printf("Case %d: impossible\n",p);
        }
        else
        {
            printf("Case %d: %d\n",p,res);
        }
    }


    return 0;
}


