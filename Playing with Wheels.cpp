/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};
ll bigmod(ll a,ll b,ll m)
{
    ll ret=1;    //a^b%m
    while(b>0)
    {
        if(b%2==1)
        {
            ret=(ret*a)%m;
        }
        b/=2;
        a=(a*a)%m;
    }
    return ret;
}
ll inverse_mod(ll b, ll m)
{
    return bigmod(b,m-2,m);   //(1/b)%m
}

struct node
{
    int w1,w2,w3,w4,val;
};
node u,v;
queue<node>q;
int s1,s2,s3,s4,e1,e2,e3,e4,stat[10][10][10][10],mpp[10][10][10][10];

int bfs(void)
{
    int i,j,k,l;
    memset(stat,0,sizeof(stat));
    stat[s1][s2][s3][s4]=1;
    u.w1=s1;
    u.w2=s2;
    u.w3=s3;
    u.w4=s4;
    u.val=0;
    if(u.w1==e1&&u.w2==e2&&u.w3==e3&&u.w4==e4)
        return 0;
    while(!q.empty())
        q.pop();
    q.push(u);
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(i=1; i<=4; i++)
        {
            for(j=-1; j<=1; j+=2)
            {
                v=u;
                if(i==1)
                v.w1=(u.w1+j+10)%10;
                else if(i==2)
                v.w2=(u.w2+j+10)%10;
                else if(i==3)
                v.w3=(u.w3+j+10)%10;
                else if(i==4)
                v.w4=(u.w4+j+10)%10;
                if(stat[v.w1][v.w2][v.w3][v.w4]==0&&mpp[v.w1][v.w2][v.w3][v.w4]==0)
                {
                    stat[v.w1][v.w2][v.w3][v.w4]=1;
                    v.val=u.val+1;
                    if(v.w1==e1&&v.w2==e2&&v.w3==e3&&v.w4==e4)
                        return v.val;
                    q.push(v);
                }

            }

        }
    }
    return -1;
}


int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    int t,p,i,n;
    cin>>t;

    for(p=1; p<=t; p++)
    {
        scanf(" %d %d %d %d",&s1,&s2,&s3,&s4);
        scanf(" %d %d %d %d",&e1,&e2,&e3,&e4);
        cin>>n;
        memset(mpp,0,sizeof(mpp));
        for(i=1; i<=n; i++)
        {
            scanf(" %d %d %d %d",&u.w1,&u.w2,&u.w3,&u.w4);
            u.val=0;
            mpp[u.w1][u.w2][u.w3][u.w4]=1;
        }
        printf("%d\n",bfs());
    }



    return 0;
}

