/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 100000010
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int flag[5010],cnt,root,mxval,adjsz[5010],val[5010],n,nodes;
queue<int>q;
vector<int>adj[5010];

int getmaxdist(int mxdist)
{
    int ret=0;
    root=0;
    cnt++;
    while(!q.empty())
        q.pop();
    for(int i=1; i<=n; i++)
    {
        if(val[i]==-1&&adjsz[i]==1)
        {
            while(!q.empty())
                q.pop();
            root=i;
            flag[i]=cnt;
            q.push(i);
            nodes++;
            val[i]=0;
            break;
        }
    }
    mxval=0;
    while((!q.empty())&&nodes!=n)
    {
        int u=q.front();
        q.pop();
        for(int i=0;nodes!=n && i<adjsz[u]; i++)
        {
            int v=adj[u][i];
            if(flag[v]!=cnt)
            {
                if(val[v]==-1)
                nodes++;
                val[v]=val[u]+1;
                root=v;
                mxval=val[v];

                flag[v]=cnt;
                q.push(v);
            }
        }
    }
    if((((mxval/2)+(mxval%2))>mxdist)||(nodes!=n))
        return 0;
    mxval=0;
    if(root!=0)
    {
        while(!q.empty())
            q.pop();
        q.push(root);
        val[root]=0;
        flag[root]=cnt-1;
        while(!q.empty())
        {
            int u=q.front();
            q.pop();
            for(int i=0; i<adj[u].size(); i++)
            {
                int v=adj[u][i];
                if(flag[v]==cnt)
                {
                    val[v]=val[u]+1;
                    mxval=val[v];
                    flag[v]=cnt-1;
                    q.push(v);
                }
            }
        }
    }
    ret=((mxval/2)+(mxval%2));
    if(ret>mxdist)
        return 0;
    return 1;
}

int func(int mxdist)
{
    for(int i=1; i<=n; i++)
    {
        nodes=0;
        cnt++;
        while(!q.empty())
            q.pop();
        memset(val,-1,sizeof(val));
        if(mxdist>0)
            q.push(i);
        val[i]=0;
        flag[i]=cnt;
        nodes++;
        while(!q.empty())
        {
            int u=q.front();
            q.pop();
            for(int j=0; j<adjsz[u]; j++)
            {
                int v=adj[u][j];
                if(val[v]==-1)
                {
                    val[v]=val[u]+1;
                    flag[v]=cnt;
                    nodes++;
                    if(mxdist>val[v])
                        q.push(v);
                }
            }
        }
        //printf("%d %d\n",nodes,mxdist);
        if(getmaxdist(mxdist)==1&&nodes==n)
            return 1;
    }
    return 0;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    int a,b,res,low,high,mid;
    scanf(" %d",&n);

    for(int i=1; i<n; i++)
    {
        scanf(" %d %d",&a,&b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
    for(int i=1;i<=n;i++)
        adjsz[i]=adj[i].size();

    res=inf;
    low=0;
    high=n/2+1;

    while(low<=high)
    {
        mid=(low+high)/2;
        if(func(mid)==1)
        {
            res=min(res,mid);
            high=mid-1;
        }
        else
        {
            low=mid+1;
        }
    }

    printf("%d\n",res);

    return 0;
}

/*
27
1 17
1 16
1 2
1 3
3 15
15 24
24 25
24 26
3 14
14 23
3 13
13 19
19 22
2 5
2 4
5 10
10 11
10 12
11 20
12 21
4 8
8 9
4 7
4 6
6 18
25 27

9
1 2
2 3
3 4
4 5
5 6
5 7
5 8
5 9
*/
