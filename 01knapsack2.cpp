#include<stdio.h>
#include<iostream>

using namespace std;
bool taken[110][110];
int n,val[110],w[110];

int knapsack(int ind,int remain)
{
    if(ind>n)
        return 0;
    int ans=0,ret1=0,ret2=0;
    if(remain>=w[ind])
    ret1=knapsack(ind+1,remain-w[ind])+val[ind];
    ret2=knapsack(ind+1,remain);
    if(ret2>ret1)
    {
        taken[ind][remain]=false;
        ans=ret2;
    }
    else
    {
        taken[ind][remain]=true;
        ans=ret1;
    }
    //cout<<ret1<<" "<<ret2<<" "<<remain<<endl;
    return ans;
}

void printpath(int ind,int remain)
{
    if(ind>n)
        return;
    if(taken[ind][remain]==true)
    {
        printf("%d\n",ind);
        printpath(ind+1,remain-w[ind]);
    }
    else
    {
        printpath(ind+1,remain);
    }
    return;
}

main()
{
    int i;
    cin>>n;
    for(i=1;i<=n;i++)
    {
        cin>>w[i]>>val[i];
    }
    printf("%d\n",knapsack(1,50));
    printpath(1,50);
    return 0;
}
