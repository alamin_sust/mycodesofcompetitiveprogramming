#define INFIN 10000

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
using std::fixed;

#include <iomanip>
using std::setprecision;

#include <algorithm>
using std::sort;

#include <cmath>
using std::sqrt;
using std::fabs;

using std::min;

struct Point
{
  int index;
  double x, y;
};

Point xP [ 10000 ];
Point yP [ 10000 ];
double xMess [ 10000 ];
double yMess [ 10000 ];


bool isLeft [ 10000 ];
Point yStrip [ 10000 ];
int n;

void process();
bool cmpFnX( Point p1, Point p2 );
bool cmpFnY( Point p1, Point p2 );
double cpp( int stInd, int endInd, int yInds [] );
double distance( Point p1, Point p2 );

int main()
{
  cin >> n;
  while ( n != 0 )
  {
     double tX, tY;
     for ( int i = 0; i < n; i++ )
     {
        cin >> tX >> tY;
        xP[ i ].index = yP[ i ].index = i;
        xMess[ i ] = xP[ i ].x = yP[ i ].x = tX;
        yMess[ i ] = xP[ i ].y = yP[ i ].y = tY;
     }
     process();
     cin >> n;
  }

  return 0;
}

void process()
{
  sort( &xP[ 0 ], &xP[ n ], cmpFnX );
  sort( &yP[ 0 ], &yP[ n ], cmpFnY );

  int firstYInds [ 10000 ];
  for ( int i = 0; i < n; i++ )
  {
     firstYInds[ i ] = yP[ i ].index;
  }

  double result = cpp( 0, n - 1, firstYInds );
  if ( result >= INFIN )
  {
     cout << "INFINTIY" << endl;
  }
  else
  {
     cout << fixed << setprecision( 4 ) << result << endl;
  }
}

double cpp( int stInd, int endInd, int yInds [] )
{
  int pNo = endInd - stInd + 1;
  int yIndsL [ 5000 ];
  int yIndsR [ 5000 ];

  if ( pNo == 1 )
  {
     return INFIN;
  }
  else if ( pNo == 2 )
  {
     return distance( xP[ stInd ], xP[ endInd ] );
  }
  else if ( pNo >= 3 )
  {
     int mid = ( stInd + endInd ) / 2;

     for ( int i = stInd; i <= mid; i++ )
     {
        isLeft[ xP[ i ].index ] = true;
     }

     for ( int i = mid + 1; i <= endInd; i++ )
     {
        isLeft[ xP[ i ].index ] = false;
     }

     int k1 = 0;
     int k2 = 0;
     for ( int i = 0; i < pNo; i++ )
     {
        if ( isLeft[ yInds[ i ] ] == true )
        {
           yIndsL[ k1++ ] = yInds[ i ];
        }
        else
        {
           yIndsR[ k2++ ] = yInds[ i ];
        }
     }

     double distL, distR;

     if ( stInd <= mid )
     {
        distL = cpp( stInd, mid, yIndsL );
     }
     else
     {
        distL = INFIN;
     }

     if ( mid + 1 <= endInd )
     {
        distR = cpp( mid + 1, endInd, yIndsR );
     }
     else
     {
        distR = INFIN;
     }

     double lrDist = min( distL, distR );
     Point midPoint = xP[ mid ];


     int strCnt = 0;
     for ( int i = 0; i < pNo; i++ )
     {
        if ( fabs( xMess[ yInds[ i ] ] - midPoint.x ) < lrDist )
        {
           yStrip[ strCnt ].x = xMess[ yInds[ i ] ];
           yStrip[ strCnt ].y = yMess[ yInds[ i ] ];

           strCnt++;
        }
     }



     double minDist = lrDist;
     for ( int i = 0; i < strCnt - 1; i++ )
     {
        double tDist;
        for ( int j = i + 1; j < strCnt && yStrip[ j ].y - yStrip[ i ].y < lrDist; j++ ) // why lrDist, why not minDist?!
        {
           tDist = distance( yStrip[ j ], yStrip[ i ] );
           minDist = min( tDist, minDist );
        }
     }

     return minDist;
  }
}

double distance( Point p1, Point p2 )
{
  return sqrt( ( p1.x - p2.x ) * ( p1.x - p2.x ) +
               ( p1.y - p2.y ) * ( p1.y - p2.y ) + 0.0 );
}

bool cmpFnX( Point p1, Point p2 )
{
  return p1.x < p2.x;
}

bool cmpFnY( Point p1, Point p2 )
{
  return p1.y < p2.y;
}
