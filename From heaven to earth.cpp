/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll t;
double n,v1,v2;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %lld",&t);

    for(ll p=1LL;p<=t;p++){
        scanf(" %lf %lf %lf",&n,&v1,&v2);

        if((sqrt(2.0)*n/v1)<(2.0*n/v2)) {
            printf("Stairs\n");
        } else {
            printf("Elevator\n");
        }

    }

    return 0;
}
