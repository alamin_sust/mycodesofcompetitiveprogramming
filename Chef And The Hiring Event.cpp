/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


vector<ll>v;
ll dp[15][2],digs;

ll rec(ll pos,ll flag)
{
    if(pos==digs)
        return 1LL;
    ll &ret=dp[pos][flag];
    if(ret!=-1LL)
        return ret;
    ret=0LL;
    if(flag==0LL)
    {
        for(ll i=0LL;i<v[pos];i+=2)
        {
            ret+=rec(pos+1LL,1LL);
        }
        if((v[pos]%2LL)==0LL)
        ret+=rec(pos+1LL,0LL);
    }
    else
    {
        for(ll i=0LL;i<4LL;i++)
        {
            ret+=rec(pos+1LL,1LL);
        }
    }
    return ret;
}

ll getdigs(ll x)
{
    ll ret=0LL;
    v.clear();
    while(x)
    {
        v.push_back(x%10LL);
        x/=10LL;
        ret++;
    }
    reverse(v.begin(),v.end());
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    ll t,p,n;

    scanf(" %lld",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %lld",&n);
        digs=getdigs(n);
        memset(dp,-1,sizeof(dp));
        printf("%lld\n",rec(0,0));
    }

    return 0;
}

