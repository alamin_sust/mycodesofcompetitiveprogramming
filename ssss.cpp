/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

#define fu(i,a,b) for(int i=a;i<=b;i++)
#define N 100005

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    int n,a[N],sum,ans;

    scanf("%d",&n);
    fu(i,1,n) scanf("%d",&a[i]);
    sort(a+1,a+n+1);
    ans=1; sum=a[1];

    fu(i,2,n)
    if(a[i]>=sum)
    {
        sum=sum+a[i];
        ans++;
    }
    printf("%d",ans);

    return 0;
}


/*100000000 100000000 200000000 400000000 800000000 1600000000
3200000000
*/

