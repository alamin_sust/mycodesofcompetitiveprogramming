/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll M=1000000007LL,fact[100010];

ll power(ll a, ll b, ll mod){
	ll x = 1LL, y = a;
	while (b > 0LL){
		if (b%2LL){
			x = (x*y);
			if(x>mod) x%=mod;
		}
		y = (y*y);
		if(y>mod) y%=mod;
		b /= 2LL;
	}
	return x;
}

ll mod_inv(ll n, ll mod){
	return power(n, mod-2LL, mod);
}

ll nCr(ll n, ll k, ll mod){
	return (fact[n]* ((mod_inv(fact[k],mod)*mod_inv(fact[n-k],mod))%mod) )%mod;
}



int main()
{
    ll t,p,i,n,k,arr[100010];
    fact[0]=1LL;
    for(ll i=1LL;i<=100005LL;i++)
    {
        fact[i]=(fact[i-1]*i)%M;

    }

    /*printf("..%lld\n",nCr(10LL,10LL,M));
    printf("..%lld\n",nCr(9LL,7LL,M));
    printf("..%lld\n",nCr(10LL,6LL,M));
    printf("..%lld\n",nCr(10LL,4LL,M));
    printf("..%lld\n",nCr(10LL,2LL,M));
    printf("..%lld\n",nCr(10LL,0LL,M));

    printf("..%lld\n",fact[2]);
    printf("..%lld\n",fact[8]);
    printf("..%lld\n",fact[10]);
*/
    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++)
    {
        scanf(" %lld %lld",&n,&k);

        ll z=0LL;

        for(i=1LL;i<=n;i++)
        {
           scanf(" %lld",&arr[i]);
           if(arr[i]==0LL)
            z++;
        }

        n-=z;
        ll h=(z==0LL?2LL:1LL);
        while(k>n)
            k-=h;

        ll res=0LL;

        if(n>=0LL&&k>=0LL)
        for(i=k;i>=0LL;i-=h)
        {
            res=(res+nCr(n,i,M))%M;
        }
        printf("%lld\n",max(1LL,res));
    }

    return 0;
}

