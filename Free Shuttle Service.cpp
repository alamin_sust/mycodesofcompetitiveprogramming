/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


void precal(void)
{
    int res,p,i,j,k,now,flag;
    for(int p=2;p<=100;p++)
    {
        res=0;
        for(i=1;i<p;i++)
        {
            for(j=2;j<=p;j++)
            {
            k=100000;
            now=j;
            flag=1;
            while(k--)
            {
                if(now==1)
                {
                    flag=0;
                    break;
                }
                now+=i;
                if(now>p)
                    now-=p;
            }
            if(flag==1)
                break;
            }
            if(flag==0)
                res++;
        }
        printf("%d: %d\n",p,res);
    }
    return;
}

int getres(int p)
{
        int res,i,j,k,now,flag;
        res=0;
        for(i=1;i<p;i++)
        {
            for(j=2;j<=p;j++)
            {
            k=10000;
            //now=j;
            flag=1;
            for(int x=0;x<=k;x++)
            {
                if(((x*p+p-j+1)%i)==0)
                {
                    flag=0;
                    break;
                }
            }
            if(flag==1)
                break;
            }
            if(flag==0)
                res++;
        }
        return res;
}


int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    precal();
    int t,p,n;
    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        printf("%d\n",getres(n));
    }


    return 0;
}


/*
100
10000
10000
10000
10000
10000
10000
10000
10000
10000
10000
*/

