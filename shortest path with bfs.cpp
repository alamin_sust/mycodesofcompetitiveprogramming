#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
using namespace std;

vector <int> edge[110],cost[110];


int bfs(int source,int destination)
{
    int i,d[110];
    for(i=0;i<=100;i++)
        d[i]=10000000;
    d[source]=0;
    queue <int> q;
    q.push(source);
    while(q.empty()==false)
    {
        int u=q.front();
        q.pop();
        int ucost=d[u];
        for(i=0;i<edge[u].size();i++)
        {
           int v=edge[u][i];
           int vcost=ucost+cost[u][i];
           if(d[v]>vcost)
           {
               d[v]=vcost;
               q.push(v);
           }
        }
    }
    return d[destination];
}

main()
{
    int n,e,i,from,to,c;
    cin>>n>>e;
    for(i=0;i<e;i++)
    {
        cin>>from>>to>>c;
        edge[from].push_back(to);
        cost[from].push_back(c);
    }
    cin>>from>>to;
    printf("%d\n",bfs(from,to));
    return 0;
}

