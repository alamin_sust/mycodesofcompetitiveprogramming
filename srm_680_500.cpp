#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

priority_queue<int> pq;
queue<int> q;
vector<int> ret,v;

class BearChairs
{
public:
    vector <int> findPositions(vector <int> atLeast, int d)
    {
        v.clear();
        ret.clear();
        while(!q.empty())q.pop();
        while(!pq.empty())pq.pop();

        for(int i=0; i<atLeast.size(); i++)
        {
            while(!q.empty())q.pop();
            while(!pq.empty())
            {
                q.push(pq.top());
                pq.pop();
            }
            v.clear();
            while(!q.empty())
            {
                pq.push(q.front());
                v.push_back(q.front());
                q.pop();
            }

            if(i==0)
            {
                pq.push(atLeast[i]);
                ret.push_back(atLeast[i]);
                continue;
            }
            reverse(v.begin(),v.end());
            int prev=-10000010;
            int flag=0;
            for(int j=0; j<v.size(); j++)
            {
                if((v[j]-prev)>=(2*d)&&atLeast[i]<=(v[j]-d)&&(v[j]-d)>=1)
                {
                    pq.push(max(1,max(prev+d,atLeast[i])));
                    ret.push_back(max(1,max(prev+d,atLeast[i])));
                    flag=1;
                    break;
                }
                prev=v[j];
            }
            if(flag==0)
            {
                pq.push(max(prev+d,atLeast[i]));
                ret.push_back(max(prev+d,atLeast[i]));
            }
        }
        return ret;
    }
};

template<typename T> void print( T a )
{
    cerr << a;
}

void print( long long a )
{
    cerr << a << "L";
}

void print( string a )
{
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a )
{
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ )
    {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need )
{
    if ( have == need )
    {
        cerr << "Case " << n << " passed." << endl;
    }
    else
    {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need )
{
    if( have.size() != need.size() )
    {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ )
    {
        if( have[i] != need[i] )
        {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need )
{
    if ( have == need )
    {
        cerr << "Case " << n << " passed." << endl;
    }
    else
    {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

    BearChairs objectBearChairs;

    //test case0
    vector <int> param00;
    param00.push_back(1);
    param00.push_back(21);
    param00.push_back(11);
    param00.push_back(7);
    int param01 = 10;
    vector <int> ret0 = objectBearChairs.findPositions(param00,param01);
    vector <int> need0;
    need0.push_back(1);
    need0.push_back(21);
    need0.push_back(11);
    need0.push_back(31);
    assert_eq(0,ret0,need0);

    //test case1
    vector <int> param10;
    param10.push_back(1);
    param10.push_back(21);
    param10.push_back(11);
    param10.push_back(7);
    int param11 = 11;
    vector <int> ret1 = objectBearChairs.findPositions(param10,param11);
    vector <int> need1;
    need1.push_back(1);
    need1.push_back(21);
    need1.push_back(32);
    need1.push_back(43);
    assert_eq(1,ret1,need1);

    //test case2
    vector <int> param20;
    param20.push_back(1000000);
    param20.push_back(1000000);
    param20.push_back(1000000);
    param20.push_back(1);
    int param21 = 1000000;
    vector <int> ret2 = objectBearChairs.findPositions(param20,param21);
    vector <int> need2;
    need2.push_back(1000000);
    need2.push_back(2000000);
    need2.push_back(3000000);
    need2.push_back(4000000);
    assert_eq(2,ret2,need2);

    //test case3
    vector <int> param30;
    param30.push_back(1000000);
    param30.push_back(1000000);
    param30.push_back(1000000);
    param30.push_back(1);
    int param31 = 999999;
    vector <int> ret3 = objectBearChairs.findPositions(param30,param31);
    vector <int> need3;
    need3.push_back(1000000);
    need3.push_back(1999999);
    need3.push_back(2999998);
    need3.push_back(1);
    assert_eq(3,ret3,need3);

}

