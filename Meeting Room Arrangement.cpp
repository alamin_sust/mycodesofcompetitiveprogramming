#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
int st,en;
};

node arr[25];
int t,p,n,i,res,curr;

int comp(node a,node b)
{
    return a.en<b.en;
}

main()
{
    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        n=0;
        while(scanf("%d%d",&arr[n].st,&arr[n].en)!=EOF&&(arr[n].st||arr[n].en))
        {
            n++;
        }
        sort(arr,arr+n,comp);
        res=0;
        curr=0;
        for(i=0;i<n;i++)
        {
            if(arr[i].st>=curr)
            {
                curr=arr[i].en;
                res++;
            }
        }
        printf("%d\n",res);
    }
    return 0;
}

