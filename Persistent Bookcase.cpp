/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll arr[100010][22];
ll books[100010];
ll n,m,q,i,j,x,com,kk,ii,jj;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);


    scanf(" %I64d %I64d %I64d",&n,&m,&q);

    x=36028797018963967LL;

    for(i=1LL; i<=q; i++)
    {
        scanf(" %I64d",&com);
        if(com==1LL)
        {
            for(j=1LL; j<=20LL; j++)
                arr[i][j]=arr[i-1LL][j];
            books[i]=books[i-1LL];
            scanf(" %I64d %I64d",&ii,&jj);
            if(((1LL<<jj)&arr[i-1LL][ii])==0LL)
            {
                books[i]++;
                arr[i][ii]=(arr[i-1LL][ii]|(1LL<<jj));
            }
        }
        else if(com==2LL)
        {
            for(j=1LL; j<=20LL; j++)
                arr[i][j]=arr[i-1][j];
            books[i]=books[i-1LL];
            scanf(" %I64d %I64d",&ii,&jj);
            if(((1LL<<jj)&arr[i-1LL][ii])!=0LL)
            {
                books[i]--;
                arr[i][ii]=(arr[i-1LL][ii]^(1LL<<jj));
            }
        }
        else if(com==3LL)
        {
            for(j=1LL; j<=20LL; j++)
                arr[i][j]=arr[i-1LL][j];
            books[i]=books[i-1LL];
            scanf(" %I64d",&ii);

            ll tp=0LL;
            for(j=1; j<=m; j++)
            {
                if(((1LL<<j)&arr[i-1LL][ii])==0LL)
                    tp++;
                else
                    tp--;
            }
            books[i]+=tp;
            arr[i][ii]=(arr[i-1LL][ii]^x);
        }
        else
        {
            scanf(" %I64d",&kk);
            books[i]=books[kk];
            for(j=1LL; j<=20LL; j++)
                arr[i][j]=arr[kk][j];
        }
        printf("%I64d\n",books[i]);
    }

    return 0;
}






