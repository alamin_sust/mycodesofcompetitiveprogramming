#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#define pi 2*acos(0)
#define inf INT_MAX
using namespace std;

long long dp[25][10010],coin[25];

long long rec(long long i,long long amount)
{
    if(i>=22)
    {
        if(amount==0)
        return 1;
        else
        return 0;
    }
    if(dp[i][amount]!=-1)
    return dp[i][amount];
    long long ret1=0,ret2;
    if(amount-coin[i]>=0)
    ret1=rec(i,amount-coin[i]);
    ret2=rec(i+1,amount);
    return dp[i][amount]=ret1+ret2;
}

main()
{
    long long i,n;
    for(i=1;i<=21;i++)
    {
        coin[i]=i*i*i;
    }
    memset(dp,-1,sizeof(dp));
    while(scanf("%lld",&n)!=EOF)
    {
        printf("%lld\n",rec(1,n));
    }
    return 0;
}
