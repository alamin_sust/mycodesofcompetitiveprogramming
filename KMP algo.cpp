#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
using namespace std;

char S[100010],W[100010];
int T[100010];

int kmp_search(void)
{
    int m=0,i=0;
    //printf("%d\n",m);
    while((m+i)<strlen(S))
    {
       // printf("llllllllllllllllll");
        if(W[i]==S[m+i])
        {
            if(i==(strlen(W)-1))
                return m;
            i=i+1;
        }
        else
        {
            m=m+i-T[i];
            if(T[i]>-1)
                i=T[i];
            else
                i=0;
        }
    }
    //printf("^^^");
    return 0;
}

void kmp_table(void)
{
    int pos=2,cnd=0;
    T[0]=-1;
    T[1]=0;
    while(pos<strlen(W))
    {
        //(first case: the substring continues)
        if(W[pos-1]==W[cnd])
        {
            cnd=cnd+1;
            T[pos]=cnd;
            pos=pos+1;
        }
        //(second case: it doesn't, but we can fall back)
        else if(cnd>0)
            cnd=T[cnd];
        //(third case: we have run out of candidates.  Note cnd = 0)
        else
        {
            T[pos]=0;
            pos=pos+1;
        }
    }
    return;
}

main()
{
    int res;
    gets(S);
    gets(W);
    kmp_table();
    //printf("$$$");
    for(int i=0;i<strlen(W);i++)
        printf("%d ",T[i]);
    cout<<endl;
    res=kmp_search();
    printf("%d\n",res);
    return 0;
}

