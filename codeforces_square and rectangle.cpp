#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
using namespace std;

main()
{
    long long int temp,x1,x2,y1,y2,xmax,xmin,ymin,ymax,area,area2,n,i;
    scanf(" %I64d",&n);
    xmax=ymax=-1;
    xmin=ymin=999999999;
    for(area=0,i=1;i<=n;i++)
        {
            scanf(" %I64d %I64d %I64d %I64d",&x1,&y1,&x2,&y2);
            temp=(x2-x1)*(y2-y1);
            if(temp<0)
                temp=-temp;
            area+=temp;
            ymax=max(ymax,max(y1,y2));
            xmax=max(xmax,max(x1,x2));
            ymin=min(ymin,min(y1,y2));
            xmin=min(xmin,min(x1,x2));
        }
    if((ymax-ymin)==(xmax-xmin))
    {
        area2=(xmax-xmin)*(ymax-ymin);
        //printf(" %I64d %I64d\n",area,area2);
        if(area==area2)
            printf("YES\n");
        else printf("NO\n");
    }
    else printf("NO\n");
    return 0;
}
