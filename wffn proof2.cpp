#include<stdio.h>
#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
#include<iostream>
#include<string.h>
using namespace std;

char s[1010];
vector<char> op,var;
vector<char>::iterator iter;
string res;

int f(char a, char b)
{
    if (a == 'N') return 1;
    if (b == 'N') return 0;
    return a < b;
}

main()
{
    int i,j;
    while(gets(s))
    {
        if (s[0] == '0') break;
        op.clear();
        var.clear();
        for(i=0; i<strlen(s); i++)
            if ((s[i] <='Z') && (s[i] >= 'A')) op.push_back(s[i]);
            else var.push_back(s[i]);
        for(iter=var.begin(); iter!=var.end();)
            if (!((*iter == 'p') || (*iter == 'q') || (*iter == 'r') || (*iter == 's') || (*iter == 't'))) var.erase(iter);
            else iter++;
        for(iter=op.begin(); iter!=op.end();)
            if (!((*iter == 'K') || (*iter == 'A') || (*iter == 'N') || (*iter == 'C') || (*iter == 'E'))) op.erase(iter);
            else iter++;
        sort(op.begin(),op.end(),f);
        if (var.empty())
        {
            printf("no WFF possible\n");
            continue;
        }
        res = "";
        i=0;
        j=0;
        if (op.size() > 0)
        {
            while((op[i] == 'N') && (i < op.size())) res += op[i],i++;
            while((i < op.size()) && (j < var.size()-1))
            {
                res = res + op[i] + var[j];
                i++;
                j++;
            }
        }
        res += var[j];
        printf("%s\n",res.c_str());
    }
    return 0;
}
