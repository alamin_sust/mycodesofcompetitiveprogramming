#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
char in[200010],arr[200010];
int n;

struct node{
int x,y,z;
};

node seg[1000010];

int update1(int id,int l,int r )
{
    //printf("%d %d\n",l,r);
    if((l<1)||(r>n))
    {//printf("..\n");
    return 0;
    }
    if(l==r)
    {
    if(arr[l]=='x')
        return seg[id].x=1;
    else return seg[id].x=0;
    }
    int mid=(l+r)/2;
       int a1=update1(2*id,l,mid);
       int a2=update1(2*id+1,mid+1,r);
        //printf("%d.\n",min(a1,a2));
    return seg[id].x=a1+a2;

}

int update2(int id,int l,int r)
{
    //printf("%d %d\n",l,r);
    if((l<1)||(r>n))
    {//printf("..\n");
    return 0;
    }
    if(l==r)
    {
    if(arr[l]=='y')
    return seg[id].y=1;
    else return seg[id].y=0;
    }
    int mid=(l+r)/2;
       int a1=update2(2*id,l,mid);
       int a2=update2(2*id+1,mid+1,r);
        //printf("%d.\n",min(a1,a2));
        return seg[id].y=a1+a2;

}

int update3(int id,int l,int r)
{
    //printf("%d %d\n",l,r);
    if((l<1)||(r>n))
    {//printf("..\n");
    return 0;
    }
    if(l==r)
    {
    if(arr[l]=='z')
            return seg[id].z=1;
    else return seg[id].z=0;
    }
    int mid=(l+r)/2;
       int a1=update3(2*id,l,mid);
       int a2=update3(2*id+1,mid+1,r);
        //printf("%d.\n",min(a1,a2));
        return seg[id].z=a1+a2;

}

int query1(int id,int l,int r,int ql,int qr)
{
    //printf("%d %d\n",l,r);
    if((l<ql&&r<ql)||(l>qr&&r>qr))
    {//printf("..\n");
    return 0;
    }
    if(ql<=l&&qr>=r)
    {
    return seg[id].x;
    }
    int mid=(l+r)/2;
    int a1=query1(2*id,l,mid,ql,qr);
    int a2=query1(2*id+1,mid+1,r,ql,qr);
        //printf("%d.\n",min(a1,a2));
    return a1+a2;

}

int query2(int id,int l,int r,int ql,int qr)
{
    //printf("%d %d\n",l,r);
    if((l<ql&&r<ql)||(l>qr&&r>qr))
    {//printf("..\n");
    return 0;
    }
    if(ql<=l&&qr>=r)
    {
    return seg[id].y;
    }
    int mid=(l+r)/2;
       int a1=query2(2*id,l,mid,ql,qr);
       int a2=query2(2*id+1,mid+1,r,ql,qr);
        //printf("%d.\n",min(a1,a2));
        return a1+a2;

}

int query3(int id,int l,int r,int ql,int qr)
{
    //printf("%d %d\n",l,r);
    if((l<ql&&r<ql)||(l>qr&&r>qr))
    {//printf("..\n");
    return 0;
    }
    if(ql<=l&&qr>=r)
    {
    return seg[id].z;
    }
    int mid=(l+r)/2;
       int a1=query3(2*id,l,mid,ql,qr);
       int a2=query3(2*id+1,mid+1,r,ql,qr);
        //printf("%d.\n",min(a1,a2));
        return a1+a2;

}

main()
{
    int i,m,f,t,p=1;
    gets(in);
    n=strlen(in);
    for(i=1;i<=n;i++)
        arr[i]=in[i-1];
    cin>>m;
    update1(1,1,n);
    update2(1,1,n);
    update3(1,1,n);
    for(i=1;i<=m;i++)
    {
        scanf("%d %d",&f,&t);
        int a=query1(1,1,n,f,t);
        int b=query2(1,1,n,f,t);
        int c=query3(1,1,n,f,t);
        //for(int j=f;j<=t;j++)
          //  printf("%c",arr[j]);
        //printf("%d, %d %d %d %d...\n",p++,n,a,b,c);
        if((abs(a-b)<=1&&abs(c-a)<=1&&abs(c-b)<=1)||(((a+b)==0||b+c==0||a+c==0)&&a+b+c<3))
            printf("YES\n");
        else printf("NO\n");
    }
    return 0;
}


