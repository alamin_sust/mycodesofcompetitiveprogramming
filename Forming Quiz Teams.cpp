#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

double dp[1<<17+5],x[17],y[17];
int n;

double rec(int mask)
{
    if(mask==((1<<n)-1))
    return 0.0;
    double &ret=dp[mask];
    if(ret>-0.5)
        return ret;
    ret=9999999.0;
    int a,tp;
    for(int i=0;i<n-1;i++)
    if(((1<<i)&mask)==0)
    {
        a=i;
        tp=(1<<i)|mask;
        break;
    }
    for(int i=a+1;i<n;i++)
    {
        if(((1<<i)&mask)==0)
        {
            //printf("ll");
            ret=min(ret,dist(x[a],y[a],x[i],y[i])+rec((1<<i)|tp));
        }
    }
    return ret;
}

main()
{
    int i,p=1,k;
    char name[25];
    while(cin>>n&&n)
    {
        n*=2;
        for(i=0;i<n;i++)
        {
            scanf(" %s %lf %lf",name,&x[i],&y[i]);
        }
        int j=(1<<17)+5;
        for(i=0;i<j;i++)
            dp[i]=-1.0;
        printf("Case %d: %.2lf\n",p++,rec(0));
    }
    return 0;
}
/*
5
sohel 10 10
mahmud 20 10
sanny 5 5
prince 1 1
per 120 3
mf 6 6
kugel 50 60
joey 3 24
limon 6 9
manzoor 0 0
*/
