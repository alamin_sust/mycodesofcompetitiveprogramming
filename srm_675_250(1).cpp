#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class LengthUnitCalculator
{
public:
	double calc(int amount, string fromUnit, string toUnit)
	{
	    double ret=(double)amount;
	    if(fromUnit=="ft"&&toUnit=="in")
        return (ret*12.0);
	    else if(fromUnit=="ft"&&toUnit=="yd")//
	    return (ret/3.0);
	    else if(fromUnit=="yd"&&toUnit=="in")
	    return (ret*36.0);
	    else if(fromUnit=="yd"&&toUnit=="ft")
	    return (ret*3.0);
	    else if(fromUnit=="in"&&toUnit=="ft")
	    return (ret/12.0);
	    else if(fromUnit=="in"&&toUnit=="yd")
	    return (ret/36.0);
	    else if(fromUnit=="mi"&&toUnit=="ft")//
	    return (ret*3.0*1760.0);
	    else if(fromUnit=="mi"&&toUnit=="in")//
	    return (ret*3.0*1760.0*12.0);
	    else if(fromUnit=="mi"&&toUnit=="yd")
	    return (ret*1760.0);
	    else if(fromUnit=="ft"&&toUnit=="mi")//
	    return (ret/(3.0*1760.0));
	    else if(fromUnit=="in"&&toUnit=="mi")//
	    return (ret/(3.0*1760.0*12.0));
	    else if(fromUnit=="yd"&&toUnit=="mi")
	    return (ret/1760.0);
        else return ret;//
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	LengthUnitCalculator objectLengthUnitCalculator;

	//test case0
	int param00 = 1;
	string param01 = "mi";
	string param02 = "ft";
	double ret0 = objectLengthUnitCalculator.calc(param00,param01,param02);
	double need0 = 5280.0;
	assert_eq(0,ret0,need0);

	//test case1
	int param10 = 1;
	string param11 = "ft";
	string param12 = "mi";
	double ret1 = objectLengthUnitCalculator.calc(param10,param11,param12);
	double need1 = 1.893939393939394E-4;
	assert_eq(1,ret1,need1);

	//test case2
	int param20 = 123;
	string param21 = "ft";
	string param22 = "yd";
	double ret2 = objectLengthUnitCalculator.calc(param20,param21,param22);
	double need2 = 41.0;
	assert_eq(2,ret2,need2);

	//test case3
	int param30 = 1000;
	string param31 = "mi";
	string param32 = "in";
	double ret3 = objectLengthUnitCalculator.calc(param30,param31,param32);
	double need3 = 6.336E7;
	assert_eq(3,ret3,need3);

	//test case4
	int param40 = 1;
	string param41 = "in";
	string param42 = "mi";
	double ret4 = objectLengthUnitCalculator.calc(param40,param41,param42);
	double need4 = 1.5782828282828283E-5;
	assert_eq(4,ret4,need4);

	//test case5
	int param50 = 47;
	string param51 = "mi";
	string param52 = "mi";
	double ret5 = objectLengthUnitCalculator.calc(param50,param51,param52);
	double need5 = 47.0;
	assert_eq(5,ret5,need5);

}
