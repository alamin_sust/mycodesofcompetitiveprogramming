#include<stdio.h>
#include<math.h>
#include<iostream>
using namespace std;
int RMQ(int *arr,int from,int to)
{
    int min=9999999,i;
    for(i=from; i<=to; i++)
    {
        if(arr[i]<min)
            min=arr[i];
    }
    return min;
}

main()
{
    int p,t,from,to,min,arr[100010],n,i,j,chunk,query,st_chunk,end_chunk,rmq[1010],indst,indend;
    scanf(" %d",&t);
    for(p=1; p<=t; p++)
    {
        scanf(" %d %d",&n,&query);
        chunk=sqrt(n);
        if(chunk*chunk!=n)
            chunk++;
        for(i=1; i<=n; i++)
            scanf(" %d",&arr[i]);
        for(j=1,i=1; i<=chunk; i++,j+=chunk)
        {
            if((j+chunk-1)<=n)
                rmq[i]=RMQ(arr,j,j+chunk-1);
            else
                rmq[i]=RMQ(arr,j,n-1);
        }
        //for(i=1;i<=chunk;i++)
        //printf(" .%d\n",rmq[i]);
        printf("Case %d:\n",p);
        for(i=1; i<=query; i++)
        {
            scanf(" %d %d",&from,&to);
            if((from-1)%chunk==0)
                st_chunk=(from-1)/chunk+1;
            else
                st_chunk=(from-1)/chunk+2;
            end_chunk=to/chunk;
            indst=chunk*(st_chunk-1)+1;
            indend=chunk*end_chunk;
            min=9999999;
            //printf("%d %d %d %d\n",st_chunk,end_chunk,indst,indend);
            if(st_chunk>end_chunk)
                for(j=from; j<=to; j++)
                {
                    if(arr[j]<min)
                        min=arr[j];
                }
            else
            {
                for(j=from; j<indst; j++)
                {
                    if(arr[j]<min)
                        min=arr[j];
                }
                for(j=st_chunk; j<=end_chunk; j++)
                {
                    if(rmq[j]<min)
                        min=rmq[j];
                }
                for(j=to; j>indend; j--)
                {
                    if(arr[j]<min)
                        min=arr[j];
                }
            }
            printf("%d\n",min);
        }
    }
    return 0;
}

