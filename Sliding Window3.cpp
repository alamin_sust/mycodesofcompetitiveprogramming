/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node
{
    int ind,val;
};

bool operator<(node a,node b)
{
    return a.val>b.val;
}

struct node2
{
    int ind,val;
};

bool operator<(node2 a,node2 b)
{
    return a.val<b.val;
}


priority_queue<node>pq_min;
priority_queue<node2>pq_max;

node det;
node2 det2;
int n,k,i,j,in,res_min[1000010],res_max[1000010];

int main()
{
    cin>>n>>k;
    for(i=1; i<=k; i++)
    {
        scanf(" %d",&det.val);
        det.ind=i;
        det2.val=det.val;
        det2.ind=det.ind;
        pq_min.push(det);
        pq_max.push(det2);
    }
    res_min[1]=pq_min.top().val;
    res_max[1]=pq_max.top().val;
    j=1;
    for(i=k+1; i<=n; i++)
    {
        scanf(" %d",&det.val);
        det.ind=i;
        det2.val=det.val;
        det2.ind=det.ind;
        pq_min.push(det);
        pq_max.push(det2);
        while(pq_min.top().ind+k<=i)
            pq_min.pop();
        while(pq_max.top().ind+k<=i)
            pq_max.pop();
        res_min[++j]=pq_min.top().val;
        res_max[j]=pq_max.top().val;
    }
    for(i=1; i<=j; i++)
    {
        printf("%d",res_min[i]);
        if(i==j)
            printf("\n");
        else
            printf(" ");
    }
    for(i=1; i<=j; i++)
    {
        printf("%d",res_max[i]);
        if(i==j)
            printf("\n");
        else
            printf(" ");
    }
    return 0;
}
