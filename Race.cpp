#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(a,b) memset(a,b,sizeof(a))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};


int dp[1010],det[12],n;

void init(void)
{
    int a=1023;
    for(int i=10;i>=0;i--)
    {
        det[i]=a;
        a>>=1;
    }
    return;
}

int rec(int rem,int mask)
{
    if(rem==0)
        if(det[__builtin_popcount(mask)]==mask)
        return 1;
        else
        return 0;
    int &ret=dp[rem][mask];
    if(ret!=-1)
        return ret;
    ret=0;
    for(int i=1;i<=n;i++)
        ret=(ret+rec(rem-i))%10056;
    return ret;
}

main()
{
    int t,p;
    init();
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n;
        ms(dp,-1);
        printf("Case %d: %d\n",p,rec(n,0));
    }
    return 0;
}

