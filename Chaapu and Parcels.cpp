#include<stdio.h>
#include<math.h>
#include<string.h>
#include<vector>
#include<map>
#include<stack>
#include<queue>
#include<algorithm>
#include<iostream>
#define ll long long int
using namespace std;

ll a[110],b[110],arr[110][110],n,m,res,k;

ll chk(ll mxv)
{
    ll i,j,flag;
    ll ret=0LL;
    memset(a,0LL,sizeof(a));
    memset(b,0LL,sizeof(b));
    for(i=1LL;i<=n;i++)
    {
        flag=0LL;
        for(j=1LL;j<=m;j++)
        {
            if(arr[i][j]<=mxv)
            {
               // printf("%lld..%lld\n",arr[j][i],mxv);
                flag=1LL;
                break;
            }
        }
        a[i]+=flag;
    }
    for(i=1LL;i<=m;i++)
    {
        flag=0LL;
        for(j=1LL;j<=n;j++)
        {
            if(arr[j][i]<=mxv)
            {
             //   printf("%lld..%lld\n",arr[j][i],mxv);
                flag=1LL;
                break;
            }
        }
        b[i]+=flag;
    }
    ll ret1=0LL;
    for(i=1LL;i<=n;i++)
    {
        if(a[i]==1LL)
            ret1++;
    }
    for(j=1LL;j<=m;j++)
        {
            if(b[j]==1LL)
            ret++;
        }
    ret=min(ret,ret1);
    //printf("%lld--\n",mxv);
    if(ret>=k)
    if(res>mxv)
    res=mxv;
    return (ret>=k);
}


int main()
{
    ll i,j,mx=0LL,low,high,mid;
    scanf(" %lld %lld %lld",&n,&m,&k);
    for(i=1LL;i<=n;i++)
    {
        for(j=1LL;j<=m;j++)
        {
            scanf(" %lld",&arr[i][j]);
            mx=max(mx,arr[i][j]);
        }
    }
    res=999999999999999LL;

    low=1LL;
    high=mx;

    while(low<=high)
    {
       mid=(low+high)/2LL;
       if(chk(mid))
       {
           high=mid-1LL;
       }
       else
       {
           low=mid+1LL;
       }
    }
    printf("%lld\n",res);
    return 0;
}
