#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

map<int,int>mpp;

class PeacefulLine
{
public:
	string makeLine(vector <int> x)
	{
	    mpp.clear();
	    int a=x.size()/2+(x.size()%2);
	    for(int i=0;i<x.size();i++)
        {
            mpp[x[i]]++;
        }
        for(int i=0;i<x.size();i++)
        {
            if(mpp[x[i]]>a)
                return "impossible";
        }
        return "possible";
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	PeacefulLine objectPeacefulLine;

	//test case0
	vector <int> param00;
	param00.push_back(1);
	param00.push_back(2);
	param00.push_back(3);
	param00.push_back(4);
	string ret0 = objectPeacefulLine.makeLine(param00);
	string need0 = "possible";
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(1);
	param10.push_back(1);
	param10.push_back(1);
	param10.push_back(2);
	string ret1 = objectPeacefulLine.makeLine(param10);
	string need1 = "impossible";
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(1);
	param20.push_back(1);
	param20.push_back(2);
	param20.push_back(2);
	param20.push_back(3);
	param20.push_back(3);
	param20.push_back(4);
	param20.push_back(4);
	string ret2 = objectPeacefulLine.makeLine(param20);
	string need2 = "possible";
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(3);
	param30.push_back(3);
	param30.push_back(3);
	param30.push_back(3);
	param30.push_back(13);
	param30.push_back(13);
	param30.push_back(13);
	param30.push_back(13);
	string ret3 = objectPeacefulLine.makeLine(param30);
	string need3 = "possible";
	assert_eq(3,ret3,need3);

	//test case4
	vector <int> param40;
	param40.push_back(3);
	param40.push_back(7);
	param40.push_back(7);
	param40.push_back(7);
	param40.push_back(3);
	param40.push_back(7);
	param40.push_back(7);
	param40.push_back(7);
	param40.push_back(3);
	string ret4 = objectPeacefulLine.makeLine(param40);
	string need4 = "impossible";
	assert_eq(4,ret4,need4);

	//test case5
	vector <int> param50;
	param50.push_back(25);
	param50.push_back(12);
	param50.push_back(3);
	param50.push_back(25);
	param50.push_back(25);
	param50.push_back(12);
	param50.push_back(12);
	param50.push_back(12);
	param50.push_back(12);
	param50.push_back(3);
	param50.push_back(25);
	string ret5 = objectPeacefulLine.makeLine(param50);
	string need5 = "possible";
	assert_eq(5,ret5,need5);

	//test case6
	vector <int> param60;
	param60.push_back(3);
	param60.push_back(3);
	param60.push_back(3);
	param60.push_back(3);
	param60.push_back(13);
	param60.push_back(13);
	param60.push_back(13);
	param60.push_back(13);
	param60.push_back(3);
	string ret6 = objectPeacefulLine.makeLine(param60);
	string need6 = "possible";
	assert_eq(6,ret6,need6);

}
