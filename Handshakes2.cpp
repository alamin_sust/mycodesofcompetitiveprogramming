/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node
{
    int sk,pos,flag;
};

node det,arr1[200010],arr2[200010],arr0[200010];

int i,n,now,res[200010],low,high,mid,x,l0,l1,l2,curr,tp,mod;

int comp(node a,node b)
{
    return a.sk<b.sk;
}

int bs0(int val)
{
    low=0;
    high=l0-1;
    int ret=-1;
    while(low<=high)
    {
        mid=(low+high)/2;
        if(arr0[mid].sk<=val)
        {
            if(arr0[mid].flag==0)
            {
                if(ret<arr0[mid].sk)
                {
                ret=arr0[mid].sk;
                curr=arr0[mid].pos;
                x=mid;
                }
            }
            low=mid+1;
        }
        else
        {
            high=mid-1;
        }
    }
    return ret;
}

int bs1(int val)
{
    low=0;
    high=l1-1;
    int ret=-1;
    while(low<=high)
    {
        mid=(low+high)/2;
        if(arr1[mid].sk<=val)
        {
            if(arr1[mid].flag==0)
            {
                if(ret<arr1[mid].sk)
                {
                ret=arr1[mid].sk;
                curr=arr1[mid].pos;
                x=mid;
                }
            }
            low=mid+1;
        }
        else
        {
            high=mid-1;
        }
    }
    return ret;
}

int bs2(int val)
{
    low=0;
    high=l2-1;
    int ret=-1;
    while(low<=high)
    {
        mid=(low+high)/2;
        if(arr2[mid].sk<=val)
        {
            if(arr2[mid].flag==0)
            {
                if(ret<arr2[mid].sk)
                {
                ret=arr2[mid].sk;
                curr=arr2[mid].pos;
                x=mid;
                }
            }
            low=mid+1;
        }
        else
        {
            high=mid-1;
        }
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d",&n);

    for(i=1; i<=n; i++)
    {
        scanf(" %d",&det.sk);
        det.pos=i;
        if((det.sk%3)==0)
            arr0[l0++]=det;
        if((det.sk%3)==1)
            arr1[l1++]=det;
        if((det.sk%3)==2)
            arr2[l2++]=det;
    }
    sort(arr0,arr0+l0,comp);
    sort(arr1,arr1+l1,comp);
    sort(arr2,arr2+l2,comp);


    now=0;
    mod=0;
    for(i=1; i<=n; i++)
    {
        if(mod==0)
        {
            tp=bs0(now);
            if(tp<0)
            {
                printf("Impossible\n");
                return 0;
            }
            arr0[x].flag=1;
        }
        if(mod==1)
        {
            tp=bs1(now);
            if(tp<0)
            {
                printf("Impossible\n");
                return 0;
            }
            arr1[x].flag=1;
        }
        if(mod==2)
        {
            tp=bs2(now);
            if(tp<0)
            {
                printf("Impossible\n");
                return 0;
            }
            arr2[x].flag=1;
        }
        res[i]=curr;
        now=tp+1;

        mod++;
        if(mod>2)
            mod=0;
    }
    printf("Possible\n");
    for(i=1; i<=n; i++)
    {
        printf("%d ",res[i]);
    }
    printf("\n");
    return 0;
}






