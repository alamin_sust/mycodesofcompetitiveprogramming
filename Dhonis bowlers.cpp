/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll res,n,M,x,arr[100010],t,p,i;

ll bs2(ll ind,ll val)
{
    ll low,mid,high,ret;

    low=ind+1LL;
    high=n;
    ret=0;

    while(low<=high)
    {
        mid=(low+high)/2LL;
        if(arr[mid]>=val)
        {
            ret=max(ret,n-mid+1LL);
            high=mid-1LL;
        }
        else
            low=mid+1LL;
    }
    return ret;
}

ll bs1(ll ind,ll val)
{
    ll low,mid,high,ret;
    low=1LL;
    high=ind-1LL;
    ret=0LL;
    while(low<=high)
    {
        mid=(low+high)/2LL;
        if(arr[mid]<=val)
        {
            ret=max(ret,mid);
            low=mid+1LL;
        }
        else
            high=mid-1LL;
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %lld %lld %lld",&n,&M,&x);
        for(i=1;i<=n;i++)
        {
            scanf(" %lld",&arr[i]);
            arr[i]%=M;
        }
        sort(arr+1,arr+n+1);
        res=0LL;
        for(i=1;i<=n;i++)
        {
            ll a,b;
            if(x>=arr[i])
            {
            a=bs1(i,x-arr[i]);
            b=bs2(i,M-x+arr[i]);
            }
            else
            {
                b=bs2(i,M+x-arr[i]);
            }
            printf("%lld: %lld..%lld\n",i,a,b);
            res+=a+b;
            if((arr[i]+arr[i])%M<=x)
                res++;
        }
        printf("%lld\n",res);
    }
    return 0;
}




