#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll dp[105][105][3],n,k,d;

ll rec(ll lev,ll taken,ll flag)
{
    if(taken==n&&flag==1)
        return 1;
    if(taken>n)
        return 0;
    ll &ret=dp[lev][taken][flag];
    if(ret!=-1)
        return ret;
    ret=0;
    for(int i=1;i<=k;i++)
    {
        ret=(ret+rec(lev+1,taken+i,flag==1?1:(d<=i?1:0)))%1000000007LL;
    }
    return ret;
}

main()
{
    cin>>n>>k>>d;
    memset(dp,-1,sizeof(dp));
    cout<<rec(0,0,0)<<endl;
    return 0;
}

