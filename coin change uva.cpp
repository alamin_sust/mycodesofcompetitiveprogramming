#include<stdio.h>
#include<string.h>
#include<iostream>
using namespace std;

long long int dp[8][10010],coin[]={50,25,10,5,1};

long long int coin_change(long long int i,long long int amount)
{
    if(i>=5)
    {if(amount==0)
    return 1;
    else
    return 0;}
    if(dp[i][amount]!=-1)
    return dp[i][amount];
    long long int res1=0,res2;
    if(amount-coin[i]>=0)
    res1=coin_change(i,amount-coin[i]);
    res2=coin_change(i+1,amount);
    return dp[i][amount]=res1+res2;
}

main()
{

    long long int n;
    memset(dp,-1,sizeof(dp));
    while(scanf("%lld",&n)!=EOF)
    {
         printf("%d\n",coin_change(0,n));
    }
    return 0;
}
