#include<stdio.h>

int arr[1010];

void merge(int left,int mid,int right)
{
     int i,temparr[right-left+1];
     int pos=0,lpos=left,rpos=mid+1;

     while(lpos<=mid&&rpos<=right)
     {
         if(arr[lpos]<arr[rpos])
         temparr[pos++]=arr[lpos++];
         else
         temparr[pos++]=arr[rpos++];
     }
     while(lpos<=mid)
     temparr[pos++]=arr[lpos++];
     while(rpos<=right)
     temparr[pos++]=arr[rpos++];
     for(i=0;i<pos;i++)
     arr[left+i]=temparr[i];
     return;

}


void mergesort(int left,int right)
{
    int mid=(left+right)/2;

    if(left<right)
    {
        mergesort(left,mid);
        mergesort(mid+1,right);
        merge(left,mid,right);
    }
    return;
}

main()
{
    int i,n;
    while(1)
    {scanf(" %d",&n);
    if(n==0)
    break;
    for(i=0;i<n;i++)
    scanf(" %d",&arr[i]);
    mergesort(0,n-1);
    for(i=0;i<n;i++)
    printf("%d\n",arr[i]);
    }
    return 0;
}
