#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
using namespace std;

struct node
{
    int from,to,weight;
};

node MST[100010];

int comp(node a,node b)
{
    return a.weight<b.weight;
}

main()
{
    int n,e,i,a,b,x,y,k,parent[10010];
    long long int res;
    scanf(" %d %d",&n,&e);
    for(i=0;i<e;i++)
    {
        scanf(" %d %d %d",&MST[i].from,&MST[i].to,&MST[i].weight);
    }
    sort(MST,MST+e,comp);
    for(i=1;i<=n;i++)
    {
        parent[i]=i;
    }
    printf("\n");
    for(k=0,res=0,i=0;k<n-1&&i<e;i++)
    {
        a=MST[i].from;
        b=MST[i].to;
        while(1)
        {
            if(parent[a]==a)
            {
                x=a;
                break;
            }
            a=parent[a];
        }
        while(1)
        {
            if(parent[b]==b)
            {
                y=b;
                break;
            }
            b=parent[b];
        }
        if(x!=y)
            {
            printf("%d %d %d\n",MST[i].from,MST[i].to,MST[i].weight);
            parent[y]=x;
            k++;
            res+=MST[i].weight;
            }
    }
    printf("Length: %lld\n",res);
    return 0;
}
/*
12 17
1 2 2
2 3 1
3 4 3
1 5 6
2 6 2
3 7 2
4 8 5
5 6 4
6 7 1
7 8 3
5 9 5
6 10 2
7 11 4
8 12 4
9 10 2
10 11 3
11 12 6
*/
