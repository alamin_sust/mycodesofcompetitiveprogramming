#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

priority_queue<int>pq[110];

class TravellingSalesmanEasy
{
public:
	int getMaxProfit(int M, vector <int> profit, vector <int> city, vector <int> visit)
	{
	    for(int i=0;i<=M;i++)
        {
            while(!pq[i].empty())
                pq[i].pop();
        }

        for(int i=0;i<profit.size()&&i<city.size();i++)
        {
            pq[city[i]].push(profit[i]);
        }
        int res=0;
	    for(int i=0;i<visit.size();i++)
        {
            if(!pq[visit[i]].empty())
            {
                res+=pq[visit[i]].top();
                pq[visit[i]].pop();
            }
        }
        return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	TravellingSalesmanEasy objectTravellingSalesmanEasy;

	//test case0
	int param00 = 2;
	vector <int> param01;
	param01.push_back(3);
	param01.push_back(10);
	vector <int> param02;
	param02.push_back(1);
	param02.push_back(1);
	vector <int> param03;
	param03.push_back(2);
	param03.push_back(1);
	int ret0 = objectTravellingSalesmanEasy.getMaxProfit(param00,param01,param02,param03);
	int need0 = 10;
	assert_eq(0,ret0,need0);

	//test case1
	int param10 = 1;
	vector <int> param11;
	param11.push_back(3);
	param11.push_back(5);
	param11.push_back(2);
	param11.push_back(6);
	param11.push_back(4);
	vector <int> param12;
	param12.push_back(1);
	param12.push_back(1);
	param12.push_back(1);
	param12.push_back(1);
	param12.push_back(1);
	vector <int> param13;
	param13.push_back(1);
	param13.push_back(1);
	param13.push_back(1);
	int ret1 = objectTravellingSalesmanEasy.getMaxProfit(param10,param11,param12,param13);
	int need1 = 15;
	assert_eq(1,ret1,need1);

	//test case2
	int param20 = 6;
	vector <int> param21;
	param21.push_back(77);
	param21.push_back(33);
	param21.push_back(10);
	param21.push_back(68);
	param21.push_back(71);
	param21.push_back(50);
	param21.push_back(89);
	vector <int> param22;
	param22.push_back(4);
	param22.push_back(1);
	param22.push_back(5);
	param22.push_back(6);
	param22.push_back(2);
	param22.push_back(2);
	param22.push_back(1);
	vector <int> param23;
	param23.push_back(6);
	param23.push_back(5);
	param23.push_back(5);
	param23.push_back(6);
	param23.push_back(4);
	int ret2 = objectTravellingSalesmanEasy.getMaxProfit(param20,param21,param22,param23);
	int need2 = 155;
	assert_eq(2,ret2,need2);

	//test case3
	int param30 = 7;
	vector <int> param31;
	param31.push_back(22);
	param31.push_back(91);
	param31.push_back(53);
	param31.push_back(7);
	param31.push_back(80);
	param31.push_back(100);
	param31.push_back(36);
	param31.push_back(65);
	param31.push_back(92);
	param31.push_back(93);
	param31.push_back(19);
	param31.push_back(92);
	param31.push_back(95);
	param31.push_back(3);
	param31.push_back(60);
	param31.push_back(50);
	param31.push_back(39);
	param31.push_back(36);
	param31.push_back(2);
	param31.push_back(30);
	param31.push_back(63);
	param31.push_back(84);
	param31.push_back(2);
	vector <int> param32;
	param32.push_back(5);
	param32.push_back(3);
	param32.push_back(4);
	param32.push_back(3);
	param32.push_back(6);
	param32.push_back(5);
	param32.push_back(6);
	param32.push_back(6);
	param32.push_back(5);
	param32.push_back(2);
	param32.push_back(7);
	param32.push_back(6);
	param32.push_back(3);
	param32.push_back(2);
	param32.push_back(6);
	param32.push_back(1);
	param32.push_back(7);
	param32.push_back(4);
	param32.push_back(6);
	param32.push_back(3);
	param32.push_back(7);
	param32.push_back(2);
	param32.push_back(5);
	vector <int> param33;
	param33.push_back(5);
	param33.push_back(7);
	param33.push_back(1);
	param33.push_back(3);
	param33.push_back(6);
	param33.push_back(2);
	param33.push_back(5);
	param33.push_back(7);
	param33.push_back(3);
	param33.push_back(6);
	param33.push_back(3);
	param33.push_back(2);
	param33.push_back(7);
	param33.push_back(3);
	param33.push_back(1);
	param33.push_back(3);
	param33.push_back(1);
	param33.push_back(7);
	param33.push_back(2);
	param33.push_back(3);
	param33.push_back(1);
	param33.push_back(1);
	param33.push_back(3);
	param33.push_back(1);
	param33.push_back(6);
	param33.push_back(1);
	int ret3 = objectTravellingSalesmanEasy.getMaxProfit(param30,param31,param32,param33);
	int need3 = 1003;
	assert_eq(3,ret3,need3);

	//test case4
	int param40 = 85;
	vector <int> param41;
	param41.push_back(94);
	param41.push_back(21);
	param41.push_back(99);
	param41.push_back(27);
	param41.push_back(91);
	param41.push_back(1);
	param41.push_back(64);
	param41.push_back(96);
	param41.push_back(32);
	param41.push_back(39);
	param41.push_back(84);
	param41.push_back(71);
	param41.push_back(97);
	param41.push_back(53);
	param41.push_back(73);
	param41.push_back(20);
	param41.push_back(7);
	param41.push_back(13);
	param41.push_back(33);
	param41.push_back(45);
	param41.push_back(5);
	param41.push_back(85);
	param41.push_back(7);
	param41.push_back(87);
	param41.push_back(94);
	param41.push_back(37);
	param41.push_back(48);
	param41.push_back(30);
	param41.push_back(5);
	param41.push_back(85);
	param41.push_back(47);
	param41.push_back(62);
	param41.push_back(91);
	param41.push_back(18);
	param41.push_back(71);
	param41.push_back(37);
	param41.push_back(7);
	param41.push_back(25);
	param41.push_back(75);
	param41.push_back(17);
	param41.push_back(40);
	param41.push_back(19);
	param41.push_back(89);
	param41.push_back(85);
	param41.push_back(86);
	param41.push_back(87);
	param41.push_back(45);
	param41.push_back(12);
	param41.push_back(61);
	param41.push_back(71);
	param41.push_back(32);
	param41.push_back(73);
	param41.push_back(63);
	param41.push_back(89);
	param41.push_back(25);
	param41.push_back(51);
	param41.push_back(60);
	param41.push_back(76);
	param41.push_back(32);
	param41.push_back(2);
	param41.push_back(69);
	param41.push_back(78);
	param41.push_back(28);
	param41.push_back(32);
	param41.push_back(74);
	param41.push_back(44);
	param41.push_back(47);
	param41.push_back(11);
	param41.push_back(82);
	param41.push_back(5);
	param41.push_back(2);
	param41.push_back(28);
	param41.push_back(54);
	param41.push_back(35);
	param41.push_back(67);
	param41.push_back(44);
	param41.push_back(35);
	param41.push_back(6);
	param41.push_back(70);
	param41.push_back(66);
	param41.push_back(77);
	param41.push_back(7);
	param41.push_back(60);
	param41.push_back(67);
	param41.push_back(33);
	param41.push_back(66);
	param41.push_back(21);
	param41.push_back(91);
	param41.push_back(76);
	param41.push_back(75);
	param41.push_back(16);
	param41.push_back(79);
	param41.push_back(20);
	param41.push_back(24);
	param41.push_back(91);
	param41.push_back(31);
	param41.push_back(2);
	param41.push_back(50);
	param41.push_back(11);
	param41.push_back(19);
	param41.push_back(93);
	param41.push_back(49);
	param41.push_back(4);
	param41.push_back(7);
	param41.push_back(55);
	param41.push_back(9);
	param41.push_back(95);
	param41.push_back(39);
	param41.push_back(54);
	param41.push_back(12);
	param41.push_back(48);
	param41.push_back(38);
	param41.push_back(73);
	param41.push_back(100);
	param41.push_back(57);
	param41.push_back(97);
	param41.push_back(44);
	param41.push_back(2);
	param41.push_back(2);
	param41.push_back(51);
	param41.push_back(40);
	param41.push_back(4);
	param41.push_back(51);
	param41.push_back(3);
	param41.push_back(95);
	param41.push_back(93);
	param41.push_back(56);
	param41.push_back(88);
	param41.push_back(60);
	param41.push_back(98);
	param41.push_back(67);
	param41.push_back(7);
	param41.push_back(99);
	param41.push_back(46);
	param41.push_back(71);
	param41.push_back(75);
	param41.push_back(24);
	param41.push_back(82);
	param41.push_back(87);
	param41.push_back(29);
	param41.push_back(92);
	param41.push_back(92);
	param41.push_back(81);
	param41.push_back(87);
	param41.push_back(34);
	param41.push_back(83);
	param41.push_back(58);
	param41.push_back(46);
	param41.push_back(79);
	param41.push_back(53);
	param41.push_back(38);
	param41.push_back(32);
	param41.push_back(97);
	param41.push_back(41);
	param41.push_back(65);
	param41.push_back(10);
	param41.push_back(54);
	param41.push_back(81);
	param41.push_back(42);
	param41.push_back(37);
	param41.push_back(76);
	param41.push_back(28);
	param41.push_back(11);
	param41.push_back(50);
	param41.push_back(13);
	param41.push_back(29);
	param41.push_back(15);
	param41.push_back(99);
	param41.push_back(73);
	param41.push_back(72);
	param41.push_back(2);
	param41.push_back(81);
	param41.push_back(39);
	param41.push_back(75);
	param41.push_back(1);
	param41.push_back(54);
	vector <int> param42;
	param42.push_back(72);
	param42.push_back(69);
	param42.push_back(19);
	param42.push_back(25);
	param42.push_back(3);
	param42.push_back(65);
	param42.push_back(10);
	param42.push_back(42);
	param42.push_back(37);
	param42.push_back(76);
	param42.push_back(29);
	param42.push_back(34);
	param42.push_back(41);
	param42.push_back(14);
	param42.push_back(46);
	param42.push_back(46);
	param42.push_back(37);
	param42.push_back(55);
	param42.push_back(30);
	param42.push_back(32);
	param42.push_back(84);
	param42.push_back(57);
	param42.push_back(74);
	param42.push_back(16);
	param42.push_back(10);
	param42.push_back(48);
	param42.push_back(67);
	param42.push_back(31);
	param42.push_back(44);
	param42.push_back(84);
	param42.push_back(11);
	param42.push_back(59);
	param42.push_back(67);
	param42.push_back(63);
	param42.push_back(5);
	param42.push_back(31);
	param42.push_back(28);
	param42.push_back(71);
	param42.push_back(3);
	param42.push_back(21);
	param42.push_back(42);
	param42.push_back(21);
	param42.push_back(61);
	param42.push_back(50);
	param42.push_back(5);
	param42.push_back(79);
	param42.push_back(79);
	param42.push_back(27);
	param42.push_back(69);
	param42.push_back(33);
	param42.push_back(47);
	param42.push_back(70);
	param42.push_back(76);
	param42.push_back(70);
	param42.push_back(17);
	param42.push_back(73);
	param42.push_back(28);
	param42.push_back(64);
	param42.push_back(77);
	param42.push_back(84);
	param42.push_back(9);
	param42.push_back(6);
	param42.push_back(63);
	param42.push_back(71);
	param42.push_back(17);
	param42.push_back(71);
	param42.push_back(40);
	param42.push_back(9);
	param42.push_back(8);
	param42.push_back(16);
	param42.push_back(76);
	param42.push_back(76);
	param42.push_back(6);
	param42.push_back(53);
	param42.push_back(47);
	param42.push_back(10);
	param42.push_back(45);
	param42.push_back(31);
	param42.push_back(78);
	param42.push_back(55);
	param42.push_back(13);
	param42.push_back(35);
	param42.push_back(50);
	param42.push_back(43);
	param42.push_back(32);
	param42.push_back(78);
	param42.push_back(78);
	param42.push_back(44);
	param42.push_back(20);
	param42.push_back(56);
	param42.push_back(24);
	param42.push_back(43);
	param42.push_back(80);
	param42.push_back(62);
	param42.push_back(72);
	param42.push_back(16);
	param42.push_back(5);
	param42.push_back(72);
	param42.push_back(67);
	param42.push_back(29);
	param42.push_back(11);
	param42.push_back(51);
	param42.push_back(64);
	param42.push_back(27);
	param42.push_back(7);
	param42.push_back(44);
	param42.push_back(59);
	param42.push_back(1);
	param42.push_back(40);
	param42.push_back(71);
	param42.push_back(64);
	param42.push_back(63);
	param42.push_back(67);
	param42.push_back(81);
	param42.push_back(72);
	param42.push_back(22);
	param42.push_back(73);
	param42.push_back(59);
	param42.push_back(21);
	param42.push_back(44);
	param42.push_back(3);
	param42.push_back(18);
	param42.push_back(9);
	param42.push_back(75);
	param42.push_back(72);
	param42.push_back(43);
	param42.push_back(13);
	param42.push_back(44);
	param42.push_back(79);
	param42.push_back(42);
	param42.push_back(58);
	param42.push_back(49);
	param42.push_back(81);
	param42.push_back(44);
	param42.push_back(42);
	param42.push_back(41);
	param42.push_back(35);
	param42.push_back(81);
	param42.push_back(63);
	param42.push_back(74);
	param42.push_back(42);
	param42.push_back(79);
	param42.push_back(42);
	param42.push_back(39);
	param42.push_back(45);
	param42.push_back(49);
	param42.push_back(18);
	param42.push_back(73);
	param42.push_back(53);
	param42.push_back(36);
	param42.push_back(80);
	param42.push_back(34);
	param42.push_back(75);
	param42.push_back(57);
	param42.push_back(10);
	param42.push_back(79);
	param42.push_back(79);
	param42.push_back(33);
	param42.push_back(48);
	param42.push_back(18);
	param42.push_back(81);
	param42.push_back(3);
	param42.push_back(69);
	param42.push_back(36);
	param42.push_back(37);
	param42.push_back(49);
	param42.push_back(54);
	param42.push_back(29);
	param42.push_back(17);
	param42.push_back(81);
	param42.push_back(83);
	param42.push_back(13);
	param42.push_back(8);
	param42.push_back(69);
	param42.push_back(5);
	param42.push_back(84);
	vector <int> param43;
	param43.push_back(39);
	param43.push_back(29);
	param43.push_back(15);
	param43.push_back(5);
	param43.push_back(3);
	param43.push_back(65);
	param43.push_back(29);
	param43.push_back(64);
	param43.push_back(60);
	param43.push_back(21);
	param43.push_back(13);
	param43.push_back(10);
	param43.push_back(73);
	param43.push_back(75);
	param43.push_back(44);
	param43.push_back(84);
	param43.push_back(15);
	param43.push_back(61);
	param43.push_back(26);
	param43.push_back(49);
	param43.push_back(31);
	param43.push_back(27);
	param43.push_back(83);
	param43.push_back(24);
	param43.push_back(16);
	param43.push_back(55);
	param43.push_back(60);
	param43.push_back(74);
	param43.push_back(71);
	param43.push_back(53);
	param43.push_back(68);
	param43.push_back(15);
	param43.push_back(75);
	param43.push_back(15);
	param43.push_back(36);
	param43.push_back(4);
	param43.push_back(47);
	param43.push_back(9);
	param43.push_back(77);
	param43.push_back(45);
	param43.push_back(63);
	param43.push_back(32);
	param43.push_back(77);
	param43.push_back(84);
	param43.push_back(8);
	param43.push_back(68);
	param43.push_back(11);
	param43.push_back(5);
	param43.push_back(18);
	param43.push_back(80);
	param43.push_back(36);
	param43.push_back(52);
	param43.push_back(42);
	param43.push_back(59);
	param43.push_back(79);
	param43.push_back(83);
	param43.push_back(81);
	param43.push_back(29);
	param43.push_back(43);
	param43.push_back(70);
	param43.push_back(29);
	param43.push_back(19);
	param43.push_back(68);
	param43.push_back(5);
	param43.push_back(83);
	param43.push_back(60);
	param43.push_back(71);
	param43.push_back(66);
	param43.push_back(62);
	param43.push_back(81);
	param43.push_back(85);
	param43.push_back(39);
	param43.push_back(42);
	param43.push_back(40);
	param43.push_back(69);
	param43.push_back(60);
	param43.push_back(34);
	param43.push_back(12);
	param43.push_back(2);
	param43.push_back(4);
	param43.push_back(31);
	param43.push_back(36);
	param43.push_back(81);
	param43.push_back(33);
	param43.push_back(71);
	param43.push_back(32);
	param43.push_back(68);
	param43.push_back(5);
	param43.push_back(30);
	param43.push_back(59);
	param43.push_back(61);
	param43.push_back(10);
	param43.push_back(71);
	param43.push_back(49);
	param43.push_back(63);
	param43.push_back(30);
	param43.push_back(62);
	param43.push_back(83);
	param43.push_back(35);
	param43.push_back(56);
	param43.push_back(82);
	param43.push_back(2);
	param43.push_back(14);
	param43.push_back(59);
	param43.push_back(68);
	param43.push_back(74);
	param43.push_back(32);
	param43.push_back(31);
	param43.push_back(3);
	param43.push_back(28);
	param43.push_back(38);
	param43.push_back(54);
	param43.push_back(38);
	int ret4 = objectTravellingSalesmanEasy.getMaxProfit(param40,param41,param42,param43);
	int need4 = 4369;
	assert_eq(4,ret4,need4);

}

