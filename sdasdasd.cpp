#include <GL/glut.h>
#include <stdlib.h>
#include <math.h>


void init (void)
{
    glClearColor (0.0, 0.0, 0.0, 0.0); // set display-window color to black
    glMatrixMode (GL_PROJECTION); // set projection parameters
    gluOrtho2D (0.0, 600.0, 0.0, 600.0); // set the x-coordinate from 0 to 600 and y-coordinate from 0 to 600
}

void setPixel (int x, int y)
{
    glColor3f (1.0, 1.0, 1.0); // activate the pixel by setting the point color to white
    glBegin (GL_POINTS);
    glVertex2i (x, y);        // set the point
    glEnd ();
    glFlush (); // process all openGL routines as quickly as possible
}

// bresenham line drawing algorithm
void bresenhamAlg (int x0, int y0, int x1, int y1)
{
    int dx = abs (x1 - x0);
    int dy = abs (y1 - y0);
    int x, y;

    if (dx >= dy)  //slope |m| <= 1.0;
    {
        int d = 2*dy - dx;
        int ds = 2*dy;
        int dt = 2*(dy - dx);
        if (x0 < x1)   // set the left point to the starting point
        {
            x = x0;
            y = y0;
        }
        else   // exchange the right point to the starting point and left point to the end point
        {
            x = x1;
            y = y1;
            x1 = x0;
            y1 = y0;
        }
        setPixel (x, y); // activate the starting point
        while (x < x1)
        {
            if (d < 0)
                d += ds;
            else
            {
                if (y < y1)
                {
                    y++;
                    d += dt;
                }
                else
                {
                    y--;
                    d += dt;
                }
            }
            x++;
            setPixel (x, y);
        }
    }
    else   //slope |m| > 1.0;
    {
        int d = 2*dx  - dy;
        int inc1 = 2*dx;
        int inc2 = 2*(dx - dy);
        if (y0 < y1)   // set the left point to the starting point
        {
            x = x0;
            y = y0;
        }
        else   // exchange the right point to the starting point and left point to the end point
        {
            x = x1;
            y = y1;
            y1 = y0;
            x1 = x0;
        }
        setPixel (x, y); // activate the starting point
        while (y < y1)
        {
            if (d < 0)
                d += inc1;
            else
            {
                if (x > x1)
                {
                    x--;
                    d += inc2;
                }
                else
                {
                    x++;
                    d += inc2;
                }
            }
            y++;
            setPixel (x, y);
        }
    }
}


// draw a line in a specific x and y coordinates and fixed dash length
void drawSingleLine (void)
{
    bresenhamAlg(50, 50, 200, 200);
}

int main (int argc, char** argv)
{
    glutInit (&argc, argv); // initialize GLUT
    glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB); // set display mode
    glutInitWindowPosition (0, 0); // set top-left display-window position
    glutInitWindowSize (600, 600); // set display-window sidth and height
    glutCreateWindow ("Bresenhum"); // create display window

    init(); // execute initialization procedure
    glutDisplayFunc(drawSingleLine); // send graphics to display window
    glutMainLoop (); // display everything and wait

    return 0;
}
