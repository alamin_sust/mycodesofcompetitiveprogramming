#include<stdio.h>
#define infinity 999999
#define R 1
#define G 2
#define max 20010
long int flag,adj[max][max],distance,nodes;
typedef struct info
{
   long int predecessor;
   long int distance;
   long int mark;
}info;

void dijkstra(long int from,long int to,long int vertices)
{
    distance=0;
    info pathinfo[max];
    int mindist,i,j,sourcenode,temppath[max];
    for(i=1;i<=vertices;i++)
    {
        pathinfo[i].predecessor=0;
        pathinfo[i].distance=infinity;
        pathinfo[i].mark=G;
    }
    pathinfo[from].predecessor=0;
    pathinfo[from].distance=0;
    pathinfo[from].mark=R;
    sourcenode=from;
    do
    {
        for(i=1;i<=vertices;i++)
        {
            if(adj[sourcenode][i]>0&&pathinfo[i].mark==G)
            {
            if(pathinfo[i].distance>(pathinfo[sourcenode].distance+adj[sourcenode][i]))
                {
                pathinfo[i].predecessor=sourcenode;
                pathinfo[i].distance=pathinfo[sourcenode].distance+adj[sourcenode][i];
                }
            }
        }
        mindist=infinity;
        sourcenode=0;
        for(i=1;i<=vertices;i++)
        {
            if((pathinfo[i].distance<mindist)&&(pathinfo[i].mark==G))
            {
                mindist=pathinfo[i].distance;
                sourcenode=i;
            }
        }
        if(sourcenode==0)
        {flag=1;
        return;}
        pathinfo[sourcenode].mark=R;
    }while(sourcenode!=to);

    temppath[1]=sourcenode;
    sourcenode=pathinfo[sourcenode].predecessor;
    for(i=2;sourcenode!=0;i++)
    {
        temppath[i]=sourcenode;
        sourcenode=pathinfo[sourcenode].predecessor;
    }
    nodes=i-1;
    for(i=1;i<nodes;i++)
    distance+=adj[temppath[i]][temppath[i+1]];
    return;
}

main()
{
    long int cities,p,t,edges,n,i,j,weight,from,to,S,T;
    scanf(" %ld",&t);
    for(p=1;p<=t;p++)
    {
    scanf(" %ld %ld %ld %ld",&n,&edges,&S,&T);
    if(S==T)
    {printf("Case #%ld: 0\n",p);
    continue;}
    else if(edges==0)
    {
        printf("Case #%ld: unreachable\n",p);
        continue;
    }
    for(i=1;i<=n;i++)
    {
        for(j=1;j<n;j++)
        adj[i][j]=0;
    }
    for(i=0;i<edges;i++)
    {
        scanf(" %ld %ld %ld",&from,&to,&weight);
        adj[from+1][to+1]=weight;
        adj[to+1][from+1]=weight;
    }
    flag=0;
    dijkstra(S+1,T+1,n);
    if(cities==0||flag==1)
    printf("Case #%ld: unreachable\n",p);
    else
    printf("Case #%ld: %ld\n",p,distance);
    }
    return 0;
}

