#include<stdio.h>
#include<math.h>
#include<string.h>

struct node{
int loc_1,loc_2,loc_3,loc_4,loc_5,loc_6;
};

struct node arr[50000010];

void gen_mesh(int rows,int columns){
//freopen("mesh_matrix.txt","w",stdout);              // write in file
printf("\nRows: %d  ",rows);
printf("Columns: %d\n\n",columns);
int lower_tri=(rows-1)/2;                           // lower triangles per column
int total_tri=(rows-1)*(columns-1)/2;               // total triangle regions in the matrix
int i=0,box_col=0;
for(i=1;i<=total_tri;i++)
{
    box_col=(i-1)/(lower_tri*2)+1;
    int tri=(i%(lower_tri*2));
    printf("%d\n",box_col);
    if(tri%2)                                       // for lower triangles
    {
        arr[i].loc_1=i+((rows+1)*(box_col-1));      // the position of global 1 is assigned to local 1 (loc_1)
        arr[i].loc_2=arr[i].loc_1+(rows*2);         // the position of global 2 is assigned to local 2 (loc_2)
        arr[i].loc_3=arr[i].loc_1+2;                // the position of global 3 is assigned to local 3 (loc_3)
        arr[i].loc_4=arr[i].loc_1+rows;             // the position of global 4 is assigned to local 4 (loc_4)
        arr[i].loc_5=arr[i].loc_4+1;                // the position of global 5 is assigned to local 5 (loc_5)
        arr[i].loc_6=arr[i].loc_1+1;                // the position of global 6 is assigned to local 6 (loc_6)
    }
    else                                            // for upper triangles
    {
        arr[i].loc_1=arr[i-1].loc_2+2;              // the position of global 1 is assigned to local 1 (loc_1)
        arr[i].loc_2=arr[i-1].loc_3;                // the position of global 2 is assigned to local 2 (loc_2)
        arr[i].loc_3=arr[i-1].loc_2;                // the position of global 3 is assigned to local 3 (loc_3)
        arr[i].loc_4=arr[i-1].loc_4+2;              // the position of global 4 is assigned to local 4 (loc_4)
        arr[i].loc_5=arr[i-1].loc_5;                // the position of global 5 is assigned to local 5 (loc_5)
        arr[i].loc_6=arr[i-1].loc_2+1;              // the position of global 6 is assigned to local 6 (loc_6)
    }
    printf("Triangle %d:\n",i);
    printf("Local           %9d %9d %9d %9d %9d %9d\n",1,2,3,4,5,6);
    printf("Global          %9d %9d %9d %9d %9d %9d\n",arr[i].loc_1,arr[i].loc_2,arr[i].loc_3,arr[i].loc_4,arr[i].loc_5,arr[i].loc_6);
}

return;
}


int main()
{
    int rows=7,columns=9;
    gen_mesh(rows,columns);                         // rows and columns will be an odd number(atleast 3)
    return 0;
}
