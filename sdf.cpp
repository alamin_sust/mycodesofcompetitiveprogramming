#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class PathGameDiv2
{
public:
	int calc(vector <string> board)
	{
	    int ret=0,k,j,p=0,i,len=board[1].length();
	    for(i=0;i<len;i++)
        {
            if(board[0][i]=='.')
                p++;
            if(board[1][i]=='.')
                p++;
        }
	    if(board[0][0]=='.')
        {
            k=0,j=0;
            for(i=0;i<len;)
            {
                if(i==len-1)
                {
                    k++;
                    break;
                }
                k++;
                if(board[j][i+1]!='.')
                {
                    if(j==0)
                        j=1;
                    else
                        j=0;
                }
                else i++;
            }
            ret=max(ret,p-k);
        }
        if(board[1][0]=='.')
        {
            k=0,j=1;
            for(i=0;i<len;)
            {
                if(i==len-1)
                {
                    k++;
                    break;
                }
                k++;
                if(board[j][i+1]!='.')
                {
                    if(j==0)
                        j=1;
                    else
                        j=0;
                }
                else i++;
            }
            ret=max(ret,p-k);
        }
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	PathGameDiv2 objectPathGameDiv2;

	//test case0
	vector <string> param00;
	param00.push_back("#....");
	param00.push_back("...#.");
	int ret0 = objectPathGameDiv2.calc(param00);
	int need0 = 2;
	assert_eq(0,ret0,need0);

	//test case1
	vector <string> param10;
	param10.push_back("#");
	param10.push_back(".");
	int ret1 = objectPathGameDiv2.calc(param10);
	int need1 = 0;
	assert_eq(1,ret1,need1);

	//test case2
	vector <string> param20;
	param20.push_back(".");
	param20.push_back(".");
	int ret2 = objectPathGameDiv2.calc(param20);
	int need2 = 1;
	assert_eq(2,ret2,need2);

	//test case3
	vector <string> param30;
	param30.push_back("....#.##.....#...........");
	param30.push_back("..#......#.......#..#....");
	int ret3 = objectPathGameDiv2.calc(param30);
	int need3 = 13;
	assert_eq(3,ret3,need3);

}
