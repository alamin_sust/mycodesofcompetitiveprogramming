/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) ( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


ll g1,g2,g3,x1,yy1,x2,y2,x3,y3,nx1,ny1,nx2,ny2,nx3,ny3,mx1,my1,mx2,my2,mx3,my3;

ll gcd(ll a,ll b)
{
    if(b==0LL)
        return a;
    return gcd(b,a%b);
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    scanf(" %I64d %I64d",&x1,&yy1);
    scanf(" %I64d %I64d",&x2,&y2);
    scanf(" %I64d %I64d",&x3,&y3);

    x1+=10000000010LL;
    yy1+=10000000010LL;
    x2+=10000000010LL;
    y2+=10000000010LL;
    x3+=10000000010LL;
    y3+=10000000010LL;

    mx1=x1-x1;
    mx2=x2-x1;
    mx3=x3-x1;
    my1=yy1-yy1;
    my2=y2-yy1;
    my3=y3-yy1;

   // g1=gcd(mx1,my1);
    g2=gcd(mx2,my2);
    g3=gcd(mx3,my3);

   // nx1=mx1/g1;
   // ny1=my1/g1;

    nx2=mx2/g2;
    ny2=my2/g2;

    nx3=mx3/g3;
    ny3=my3/g3;

    if(nx2==nx3&&ny2==ny3)
    {
        printf("1\n");
        return 0;
    }

    if(x1==x2&&(dist(x1,yy1,x3,y3)>(dist(x1,yy1,x2,y2)+dist(x2,y2,x3,y3))||dist(x2,y2,x3,y3)>(dist(x1,yy1,x2,y2)+dist(x1,yy1,x3,y3))))
    {

        printf("2\n");
        return 0;
    }

    if(x1==x3&&(dist(x1,yy1,x2,y2)>(dist(x1,yy1,x3,y3)+dist(x2,y2,x3,y3))||dist(x2,y2,x3,y3)>(dist(x1,yy1,x2,y2)+dist(x1,yy1,x3,y3))))
    {
        printf("2\n");
        return 0;

    }
    if(x2==x3&&(dist(x1,yy1,x3,y3)>(dist(x1,yy1,x2,y2)+dist(x2,y2,x3,y3))||dist(x2,y2,x1,yy1)>(dist(x3,y3,x2,y2)+dist(x1,yy1,x3,y3))))
    {
        printf("2\n");
        return 0;
    }
    if(yy1==y2&&(dist(x1,yy1,x3,y3)>(dist(x1,yy1,x2,y2)+dist(x2,y2,x3,y3))||dist(x2,y2,x3,y3)>(dist(x1,yy1,x2,y2)+dist(x1,yy1,x3,y3))))
    {
        printf("2\n");
        return 0;
    }
    if(y2==y3&&(dist(x1,yy1,x3,y3)>(dist(x1,yy1,x2,y2)+dist(x2,y2,x3,y3))||dist(x2,y2,x1,yy1)>(dist(x3,y3,x2,y2)+dist(x1,yy1,x3,y3))))
    {
        printf("2\n");
        return 0;
    }
    if(y3==yy1&&(dist(x1,yy1,x2,y2)>(dist(x1,yy1,x3,y3)+dist(x2,y2,x3,y3))||dist(x2,y2,x3,y3)>(dist(x1,yy1,x2,y2)+dist(x1,yy1,x3,y3))))
    {
        printf("2\n");
        return 0;
    }


    if(x1==x2&&y2==y3)
    {
        printf("2\n");
        return 0;
    }

    if(x1==x3&&y3==y2)
    {
        printf("2\n");
        return 0;
    }

    if(x2==x1&&yy1==y3)
    {
        printf("2\n");
        return 0;
    }

    if(x2==x3&&y3==yy1)
    {
        printf("2\n");
        return 0;
    }

    if(x3==x1&&yy1==y2)
    {
        printf("2\n");
        return 0;
    }

    if(x3==x2&&y2==yy1)
    {
        printf("2\n");
        return 0;
    }

    printf("3\n");


    return 0;
}


