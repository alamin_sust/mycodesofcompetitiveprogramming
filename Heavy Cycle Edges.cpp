#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll a,c,b,k,i,res[25010],m,n,par[1010];

struct node{
    ll x,y,val;
};

node det;

bool operator<(node a,node b)
{
    return a.val>b.val;
}

priority_queue<node>pq;

main()
{
    while(cin>>n>>m)
    {
        if(n==0&&m==0)
            break;
        for(i=1; i<=n; i++)
            par[i]=i;
        k=0;
        for(i=1; i<=m; i++)
        {
            scanf(" %lld %lld %lld",&det.x,&det.y,&det.val);
            det.x++;
            det.y++;
            pq.push(det);
        }
        while(!pq.empty())
        {
            a=pq.top().x;
            b=pq.top().y;
            c=pq.top().val;
            pq.pop();
            while(a!=par[a])
                a=par[a];
            while(b!=par[b])
                b=par[b];
            if(a==b)
                res[k++]=c;
            else
                par[a]=b;
        }
        sort(res,res+k);
        if(k)
        {
            for(i=0; i<k; i++)
            {
                printf("%lld",res[i]);
                if(i!=k-1)
                    printf(" ");
            }
        }
        else
            printf("forest");
        printf("\n");
    }
    return 0;
}

