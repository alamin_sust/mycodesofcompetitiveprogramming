/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll n,m,l,r,M=1000000007LL,GCD[1010][1010];
map<pair<ll,ll>,ll>mpp;


ll rec(ll pos,ll gcd)
{
    if(gcd!=0LL&&gcd<l)
        return 0LL;
    if(pos==n)
    {
        if(gcd<=r)
            return 1LL;
        return 0LL;
    }
    pair<ll,ll>pii;
    pii.first=pos;
    pii.second=gcd;
    ll &ret=mpp[pii];
    if(ret!=0LL)
        return ret;
    ret=M;
    for(ll i=1LL;i<=m;i++)
    {
        ret=(ret+rec(pos+1,GCD[gcd][i]))%M+M;
    }
    return ret;
}

int main()
{
    ll p,t;
    for(ll i=0LL;i<=1000LL;i++)
        for(ll j=i;j<=1000LL;j++)
    {
        GCD[i][j]=GCD[j][i]=__gcd(i,j);
    }
    cin>>t;
    for(p=1LL;p<=t;p++)
    {
         cin>>n>>m>>l>>r;
         mpp.clear();
         //printf("%lld\n",__gcd(0LL,9LL));
         printf("%lld\n",rec(0LL,0LL)%M);
    }
    return 0;
}
