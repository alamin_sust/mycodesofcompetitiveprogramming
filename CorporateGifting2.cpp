/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node{
int x,val;
};

node det,u;
queue<node>q;

int stat[200010];

vector<int>adj[200010];

int bfs(int src)
{
    int res1,res2;
    memset(stat,0,sizeof(stat));
    stat[src]=1;
    while(!q.empty())
        q.pop();
    det.x=src;
    det.val=0;
    q.push(det);
    res1=0;
    res2=0;
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        if((u.val%2)==0)
            res1++;
        else
            res2++;
        det.val=u.val+1;
        for(int i=0;i<adj[u.x].size();i++)
        {
            det.x=adj[u.x][i];
            if(stat[det.x]==0)
            {
                stat[det.x]=1;
                q.push(det);
            }
        }
    }
    return (min(res1,res2)*2+max(res1,res2));
}

int main()
{
    int t,p,i,n,in;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        for(i=0;i<=n;i++)
            adj[i].clear();
        for(i=1;i<=n;i++)
        {
            scanf(" %d",&in);
            adj[in].push_back(i);
        }
        printf("Case #%d: %d\n",p,bfs(1));
    }
    return 0;
}




/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

map<string,int>mpp;
int p,t,i,j,res,n,l;
char arr[1000010];
string str;

int main()
{
    freopen("autocomplete.txt","r",stdin);
    freopen("autocomplete_output.txt","w",stdout);
    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        mpp.clear();
        res=0;
        scanf(" %d",&n);
        for(i=1;i<=n;i++)
        {
            scanf(" %s",arr);
            l=strlen(arr);
            str="";
            for(j=0;j<l;j++)
            {
                str+=arr[j];
                res++;
                if(mpp[str]==0)
                {
                    mpp[str]=1;
                    j++;
                    while(j<l)
                    {
                        str+=arr[j];
                        mpp[str]=1;
                        j++;
                    }
                    break;
                }
            }
        }
        printf("Case #%d: %d\n",p,res);
    }
    return 0;
}




