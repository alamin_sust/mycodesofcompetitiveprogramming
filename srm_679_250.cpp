#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class ListeningSongs
{
public:
	int listen(vector <int> durations1, vector <int> durations2, int minutes, int T)
	{
	    sort(durations1.begin(),durations1.end());
	    sort(durations2.begin(),durations2.end());
	    int secs=0,cnt=0;
	    for(int i=0;i<durations1.size()&&i<T;i++)
        {
            secs+=durations1[i];
            cnt++;
        }
        for(int i=0;i<durations2.size()&&i<T;i++)
        {
            secs+=durations2[i];
            cnt++;
        }
        if(cnt<(2*T)||(minutes*60)<secs)
            return -1;
        vector<int>v;
        v.clear();
        for(int i=T;i<durations1.size();i++)
        {
            v.push_back(durations1[i]);
        }
        for(int i=T;i<durations2.size();i++)
        {
            v.push_back(durations2[i]);
        }
        sort(v.begin(),v.end());
        for(int i=0;i<v.size();i++)
        {
            if((secs+v[i])<=(minutes*60))
                cnt++,secs+=v[i];
            else
                break;
        }
        return cnt;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	ListeningSongs objectListeningSongs;

	//test case0
	vector <int> param00;
	param00.push_back(300);
	param00.push_back(200);
	param00.push_back(100);
	vector <int> param01;
	param01.push_back(400);
	param01.push_back(500);
	param01.push_back(600);
	int param02 = 17;
	int param03 = 1;
	int ret0 = objectListeningSongs.listen(param00,param01,param02,param03);
	int need0 = 4;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(300);
	param10.push_back(200);
	param10.push_back(100);
	vector <int> param11;
	param11.push_back(400);
	param11.push_back(500);
	param11.push_back(600);
	int param12 = 10;
	int param13 = 1;
	int ret1 = objectListeningSongs.listen(param10,param11,param12,param13);
	int need1 = 2;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(60);
	param20.push_back(60);
	param20.push_back(60);
	vector <int> param21;
	param21.push_back(60);
	param21.push_back(60);
	param21.push_back(60);
	int param22 = 5;
	int param23 = 2;
	int ret2 = objectListeningSongs.listen(param20,param21,param22,param23);
	int need2 = 5;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(120);
	param30.push_back(120);
	param30.push_back(120);
	param30.push_back(120);
	param30.push_back(120);
	vector <int> param31;
	param31.push_back(60);
	param31.push_back(60);
	param31.push_back(60);
	param31.push_back(60);
	param31.push_back(60);
	param31.push_back(60);
	int param32 = 10;
	int param33 = 3;
	int ret3 = objectListeningSongs.listen(param30,param31,param32,param33);
	int need3 = 7;
	assert_eq(3,ret3,need3);

	//test case4
	vector <int> param40;
	param40.push_back(196);
	param40.push_back(147);
	param40.push_back(201);
	param40.push_back(106);
	param40.push_back(239);
	param40.push_back(332);
	param40.push_back(165);
	param40.push_back(130);
	param40.push_back(205);
	param40.push_back(221);
	param40.push_back(248);
	param40.push_back(108);
	param40.push_back(60);
	vector <int> param41;
	param41.push_back(280);
	param41.push_back(164);
	param41.push_back(206);
	param41.push_back(95);
	param41.push_back(81);
	param41.push_back(383);
	param41.push_back(96);
	param41.push_back(255);
	param41.push_back(260);
	param41.push_back(244);
	param41.push_back(60);
	param41.push_back(313);
	param41.push_back(101);
	int param42 = 60;
	int param43 = 3;
	int ret4 = objectListeningSongs.listen(param40,param41,param42,param43);
	int need4 = 22;
	assert_eq(4,ret4,need4);

	//test case5
	vector <int> param50;
	param50.push_back(100);
	param50.push_back(200);
	param50.push_back(300);
	vector <int> param51;
	param51.push_back(100);
	param51.push_back(200);
	param51.push_back(300);
	int param52 = 2;
	int param53 = 1;
	int ret5 = objectListeningSongs.listen(param50,param51,param52,param53);
	int need5 = -1;
	assert_eq(5,ret5,need5);

	//test case6
	vector <int> param60;
	param60.push_back(100);
	param60.push_back(200);
	param60.push_back(300);
	param60.push_back(400);
	param60.push_back(500);
	param60.push_back(600);
	vector <int> param61;
	param61.push_back(100);
	param61.push_back(200);
	int param62 = 1000;
	int param63 = 3;
	int ret6 = objectListeningSongs.listen(param60,param61,param62,param63);
	int need6 = -1;
	assert_eq(6,ret6,need6);

}
