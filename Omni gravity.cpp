/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

map<int,int>mpp;
string str,arr[10],tp[10];
int res,newpos,flag[5];

int check_if_new(void)
{
    int ret=0;
    flag[0]=flag[1]=flag[2]=flag[3]=0;
    for(int i=0;i<8;i++)
    {
        for(int j=0;j<8;j++)
        {
            if(arr[i][j]=='A'&&flag[0]==0)
                ret+=i*8+j,flag[0]=1;
            else if(arr[i][j]=='B'&&flag[1]==0)
                ret+=(i*8+j)*100,flag[1]=1;
            else if(arr[i][j]=='C'&&flag[2]==0)
                ret+=(i*8+j)*10000,flag[2]=1;
            else if(arr[i][j]=='D'&&flag[3]==0)
                ret+=(i*8+j)*1000000,flag[3]=1;
        }
    }
    if(mpp[ret]==0)
    {
        res++;
        mpp[ret]=1;
    }
    return ret;
}

void printarr(void)
{
    for(int i=0;i<8;i++)
        cout<<arr[i]<<"\n";
}

void rightshift(void)
{
   // printf("r");
    for(int j=7;j>=0;j--)
    {
        for(int i=0;i<8;i++)
        {
            if(arr[i][j]>='A'&&arr[i][j]<='D'&&i<7&&arr[i+1][j]==arr[i][j]&&j>0&&arr[i][j-1]==arr[i][j])
            {
                newpos=0;
                for(int k=j+1;k<8;k++)
                {
                    if(arr[i][k]=='.'&&arr[i+1][k]=='.')
                        newpos++;
                    else
                        break;
                }
                swap(arr[i][j],arr[i][j+newpos]);
                swap(arr[i+1][j],arr[i+1][j+newpos]);
                swap(arr[i][j-1],arr[i][j+newpos-1]);
                swap(arr[i+1][j-1],arr[i+1][j+newpos-1]);
            }
        }
    }
    check_if_new();
    return;
}

void leftshift(void)
{

   //printf("l..");
    for(int j=0;j<8;j++)
    {
        for(int i=0;i<8;i++)
        {
            if(arr[i][j]>='A'&&arr[i][j]<='D'&&i<7&&arr[i+1][j]==arr[i][j]&&j<7&&arr[i][j+1]==arr[i][j])
            {
                newpos=0;
                for(int k=j-1;k>=0;k--)
                {
                    if(arr[i][k]=='.'&&arr[i+1][k]=='.')
                        newpos++;
                    else
                        break;
                }
                swap(arr[i][j],arr[i][j-newpos]);
                swap(arr[i+1][j],arr[i+1][j-newpos]);
                swap(arr[i][j+1],arr[i][j-newpos+1]);
                swap(arr[i+1][j+1],arr[i+1][j-newpos+1]);
            }
        }
    }
    check_if_new();
    return;
}

void downshift(void)
{
   // printf("d");
    for(int i=7;i>=0;i--)
    {
        for(int j=0;j<8;j++)
        {
            if(arr[i][j]>='A'&&arr[i][j]<='D'&&j<7&&arr[i][j+1]==arr[i][j]&&i>0&&arr[i-1][j]==arr[i][j])
            {
                newpos=0;
                for(int k=i+1;k<8;k++)
                {
                    if(arr[k][j]=='.'&&arr[k][j+1]=='.')
                        newpos++;
                    else
                        break;
                }
                swap(arr[i][j],arr[i+newpos][j]);
                swap(arr[i][j+1],arr[i+newpos][j+1]);
                swap(arr[i-1][j],arr[i+newpos-1][j]);
                swap(arr[i-1][j+1],arr[i+newpos-1][j+1]);
            }
        }
    }
    check_if_new();
    return;
}

void upshift(void)
{
   // printf("u");
    for(int i=0;i<8;i++)
    {
        for(int j=0;j<8;j++)
        {
            if(arr[i][j]>='A'&&arr[i][j]<='D'&&j<7&&arr[i][j+1]==arr[i][j]&&i<7&&arr[i+1][j]==arr[i][j])
            {
                newpos=0;
                for(int k=i-1;k>=0;k--)
                {
                    if(arr[k][j]=='.'&&arr[k][j+1]=='.')
                        newpos++;
                    else
                        break;
                }
                swap(arr[i][j],arr[i-newpos][j]);
                swap(arr[i][j+1],arr[i-newpos][j+1]);
                swap(arr[i+1][j],arr[i-newpos+1][j]);
                swap(arr[i+1][j+1],arr[i-newpos+1][j+1]);
            }
        }
    }
    check_if_new();
    return;
}

int main()
{
    int t,p,i,j,k,now;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    cin>>t;
    for(p=1;p<=t;p++)
    {
        mpp.clear();
        for(i=0;i<8;i++)
        {
            cin>>arr[i];
            tp[i]=arr[i];
         }
        res=0;
        leftshift();
        mpp[check_if_new()]=1;
        res++;
        q.push(arr);
        /*k=1<<20;
        for(i=0;i<k;i++)
        {
            for(j=0;j<8;j++)
                arr[j]=tp[j];
            for(j=0;j<20;j+=2)
            {
                now=((i>>j)&1)*2+((i>>(j+1))&1);
                //printarr();
                if(now==0)
                    leftshift();
                else if(now==1)
                    rightshift();
                else if(now==2)
                    upshift();
                else
                    downshift();
              // printf("%d\n",now);
              // printarr();

              // getchar();
            }
        }*/
        printf("Case %d: %d\n",p,res);
    }
    return 0;
}




