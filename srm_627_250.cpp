#include <sstream>
#include<string.h>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class ManySquares
{
public:
	int howManySquares(vector <int> sticks)
	{
	    int i,arr[1010],ret=0;
	    memset(arr,0,sizeof(arr));
	    for(i=0;i<sticks.size();i++)
        {
            arr[sticks[i]]++;
            if(arr[sticks[i]]==4)
                {ret++;
                arr[sticks[i]]=0;}

        }
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	ManySquares objectManySquares;

	//test case0
	vector <int> param00;
	param00.push_back(1);
	param00.push_back(1);
	param00.push_back(2);
	param00.push_back(2);
	param00.push_back(1);
	param00.push_back(1);
	param00.push_back(2);
	int ret0 = objectManySquares.howManySquares(param00);
	int need0 = 1;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(3);
	param10.push_back(1);
	param10.push_back(4);
	param10.push_back(4);
	param10.push_back(4);
	param10.push_back(10);
	param10.push_back(10);
	param10.push_back(10);
	param10.push_back(10);
	int ret1 = objectManySquares.howManySquares(param10);
	int need1 = 1;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(1);
	param20.push_back(2);
	param20.push_back(3);
	param20.push_back(4);
	param20.push_back(1);
	param20.push_back(2);
	param20.push_back(3);
	param20.push_back(4);
	param20.push_back(1);
	param20.push_back(2);
	param20.push_back(3);
	param20.push_back(1);
	param20.push_back(2);
	param20.push_back(3);
	param20.push_back(4);
	param20.push_back(1);
	param20.push_back(2);
	param20.push_back(3);
	param20.push_back(3);
	param20.push_back(3);
	int ret2 = objectManySquares.howManySquares(param20);
	int need2 = 3;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(1);
	param30.push_back(1);
	param30.push_back(1);
	param30.push_back(2);
	param30.push_back(2);
	param30.push_back(2);
	param30.push_back(3);
	param30.push_back(3);
	param30.push_back(3);
	param30.push_back(4);
	param30.push_back(4);
	param30.push_back(4);
	int ret3 = objectManySquares.howManySquares(param30);
	int need3 = 0;
	assert_eq(3,ret3,need3);

	//test case4
	vector <int> param40;
	param40.push_back(1);
	param40.push_back(1);
	param40.push_back(1);
	param40.push_back(2);
	param40.push_back(1);
	param40.push_back(1);
	param40.push_back(1);
	param40.push_back(3);
	param40.push_back(1);
	param40.push_back(1);
	param40.push_back(1);
	int ret4 = objectManySquares.howManySquares(param40);
	int need4 = 2;
	assert_eq(4,ret4,need4);

	//test case5
	vector <int> param50;
	param50.push_back(2);
	param50.push_back(2);
	param50.push_back(4);
	param50.push_back(4);
	param50.push_back(8);
	param50.push_back(8);
	int ret5 = objectManySquares.howManySquares(param50);
	int need5 = 0;
	assert_eq(5,ret5,need5);
    return 0;
}
