#include<bits/stdc++.h>
#define inf 100000010
using namespace std;

struct node
{
    string word;
    int substring_pos,substrings;
};

node res[100010];
string arr[100010];
int n,cflag[210];

int comp(node a,node b)  // needed for builtin sort()
{
    if(a.substring_pos==b.substring_pos&&a.substrings==b.substrings)
        return a.word<b.word;
    if(a.substring_pos==b.substring_pos)
        return a.substrings>b.substrings;
    return a.substring_pos<b.substring_pos;
}

void sort_manually(void) // manually sort
{
    for(int i=0;i<n;i++)
    {
        for(int j=1;j<n;j++)
        {
            if(res[j].substring_pos==res[j-1].substring_pos&&res[j].substrings==res[j-1].substrings&&res[j].word<res[j-1].word)
            {
                swap(res[j],res[j-1]);
            }
            else if(res[j].substring_pos==res[j-1].substring_pos&&res[j].substrings>res[j-1].substrings)
            {
                swap(res[j],res[j-1]);
            }
            else if(res[j].substring_pos<res[j-1].substring_pos)
            {
                swap(res[j],res[j-1]);
            }
        }
    }
    return;
}

void generateSuggestions(string prefix)
{
    for(int i=0; i<n; i++)
    {
        node temp;
        temp.word=arr[i];
        temp.substrings=0;
        temp.substring_pos=inf;
        for(int j=0; j<(arr[i].length()-prefix.length()+1); j++)  // check if substring matches
        {
            int flag=0;
            for(int k=0,l=j; k<prefix.length(); l++,k++)
            {
                if(arr[i][l]!=prefix[k])
                {
                    flag=1;
                    break;
                }
            }
            if(flag==0)
            {
                if(temp.substring_pos==inf)
                temp.substring_pos=j;
                temp.substrings++;
            }
        }
        res[i]=temp;
    }

    sort(res,res+n,comp); // u can use this builtin sort
                              // or,
    sort_manually();    // sort manually (anyone will do);

    return;
}

int main()
{
    string prefix;
    int m;

    //freopen("words.txt","r",stdin);
    //freopen("suggest.txt","w",stdout);


    cout<<"How many words? ";
    cin>>n; //total number of words
    cout<<"Enter all words:\n";

    for(int i=0; i<n; i++) //input all words
    {
        cin>>arr[i];
    }

    cout<<"Maximum Suggestions for each word: ";
    cin>>m;

    cout<<"Enter prefixes:\n";
    while(cin>>prefix) // scan prefix
    {
        generateSuggestions(prefix);
        cout<<"Suggestions for '"<<prefix<<"':\n";
        for(int i=0; i<min(n,m); i++)
        {
            if(res[i].substring_pos==inf)
                break;
            cout<<res[i].word;
            cout<<" (substring matched from position "<<res[i].substring_pos<<",";
            cout<<" total substring matched: "<<res[i].substrings<<")\n";
        }
        cout<<"\n";
    }

    return 0;
}

/*
20
hate
worldclass
beautiful
boss
dog
diarrhoea
amoeba
maintenance
lietenant
spellingbee
iamnotagirl
saddamisspartacus
weloveyou
leafysoft
nothingbuttrue
entiglment
googlegesture
testing
cat
jubayer
15
girl
boy
*/

