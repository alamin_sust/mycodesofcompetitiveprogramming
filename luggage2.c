#include<stdio.h>
#include<string.h>

int dp[22][205][205],arr[22],n;

int knap(int i,int k1,int k2)
{
    if(i==n)
    {
        if(k1==k2)
        return 1;
        else return 0;
    }
    if(dp[i][k1][k2]!=-1)
    return dp[i][k1][k2];
    int ret1=0,ret2=0;
    if(k1<=k2)
    ret1=knap(i+1,k1+arr[i],k2-arr[i]);
    ret2=knap(i+1,k1,k2);
    return ret1|ret2;
}

main()
{
    int res,t,p,i,w2;
    char ch;
    scanf(" %d",&t);
    getchar();
    for(p=1;p<=t;p++)
    {
        memset(dp,-1,sizeof(dp));
        for(i=0,w2=0;scanf("%d",&arr[i]);)
        {
            scanf("%c",&ch);
            w2+=arr[i];
            i++;
            if(ch=='\n')
            break;
        }
        n=i;
        res=knap(0,0,w2);
        if(res==1)
        printf("YES\n");
        else
        printf("NO\n");
    }
    return 0;
}
