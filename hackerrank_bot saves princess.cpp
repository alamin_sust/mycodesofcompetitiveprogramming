#include <stdio.h>
#include <iostream>
#include <vector>
#include <queue>
#include <string.h>
using namespace std;
/* Head ends here */

struct node{
int x,y;
};

void displayPathtoPrincess(int n, vector <string> grid){
    //your logic here
    queue<node>q;
    node det;
    int flag=0,status[110][110],val[110][110];
    memset(status,0,sizeof(status));
    memset(val,-1,sizeof(val));
    for(int i=0;i<n&&flag==0;i++)
    {
        for(int j=0;j<n;j++)
        {
            if(grid[i][j]=='p')
            {det.x=i;
             det.y=j;
             flag=1;
            break;}
        }
    }
    q.push(det);
    status[det.x][det.y]=1;
    val[det.x][det.y]=0;
    while(1)
    {
       det=q.front();
        if(grid[det.x][det.y]=='m')
            break;
        q.pop();
        if(status[det.x-1][det.y]==0&&det.x>0)
        {
            status[det.x-1][det.y]=1;
            val[det.x-1][det.y]=val[det.x][det.y]+1;
            det.x--;
            q.push(det);
            det.x++;
        }
        if(status[det.x][det.y-1]==0&&det.y>0)
        {
            status[det.x][det.y-1]=1;
            val[det.x][det.y-1]=val[det.x][det.y]+1;
            det.y--;
            q.push(det);
            det.y++;
        }
        if(status[det.x][det.y+1]==0&&det.y<(n-1))
        {
            status[det.x][det.y+1]=1;
            val[det.x][det.y+1]=val[det.x][det.y]+1;
            det.y++;
            q.push(det);
            det.y--;
        }
        if(status[det.x+1][det.y]==0&&det.x<(n-1))
        {
            status[det.x+1][det.y]=1;
            val[det.x+1][det.y]=val[det.x][det.y]+1;
            det.x++;
            q.push(det);
            det.x--;
        }
    }
    while(val[det.x][det.y])
    {
        if(val[det.x-1][det.y]+1==val[det.x][det.y]&&det.x>0)
        {
            printf("UP\n");
            det.x--;
        }
        else if(val[det.x][det.y-1]+1==val[det.x][det.y]&&det.y>0)
        {
            printf("LEFT\n");
            det.y--;
        }
        else if(val[det.x+1][det.y]+1==val[det.x][det.y]&&det.x<n-1)
        {
            printf("DOWN\n");
            det.x++;
        }
        else if(val[det.x][det.y+1]+1==val[det.x][det.y]&&det.y<n-1)
        {
            printf("RIGHT\n");
            det.y++;
        }
    }
    return;
}
/* Tail starts here */
int main(void) {

    int m;
    vector <string> grid;

    cin >> m;

    for(int i=0; i<m; i++) {
        string s; cin >> s;
        grid.push_back(s);
    }

    displayPathtoPrincess(m,grid);

    return 0;
}
