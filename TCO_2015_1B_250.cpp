#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class TheNicePair
{
public:
	int solve(vector <int> A)
	{
	    int ret=0;
	    int flag=0;
	    int cnt;
	    for(int i=0;i<A.size();i++)
        {
            if(A[i]>1)
                flag=1;
            for(int j=i;j<A.size();j++)
            {
                for(int k=i;k<=j;k++)
                {
                    if(A[k]<=1)
                        continue;
                    cnt=0;
                    for(int l=i;l<=j;l++)
                    {
                        if((A[l]%A[k])==0)
                        {
                            cnt++;
                        }
                    }
                    if((cnt*2)>=(j-i+1))
                    {
                        ret=max(ret,j-i);
                    }
                }
            }
        }
        if(flag==0)
            return -1;
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	TheNicePair objectTheNicePair;

	//test case0
	vector <int> param00;
	param00.push_back(5);
	param00.push_back(5);
	param00.push_back(5);
	param00.push_back(5);
	param00.push_back(5);
	int ret0 = objectTheNicePair.solve(param00);
	int need0 = 4;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(1);
	param10.push_back(1);
	param10.push_back(1);
	param10.push_back(1);
	int ret1 = objectTheNicePair.solve(param10);
	int need1 = -1;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(2);
	param20.push_back(3);
	param20.push_back(5);
	param20.push_back(7);
	int ret2 = objectTheNicePair.solve(param20);
	int need2 = 1;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(8);
	param30.push_back(8);
	param30.push_back(5);
	param30.push_back(5);
	param30.push_back(5);
	int ret3 = objectTheNicePair.solve(param30);
	int need3 = 4;
	assert_eq(3,ret3,need3);

	//test case4
	vector <int> param40;
	param40.push_back(1);
	param40.push_back(1000);
	param40.push_back(1000);
	param40.push_back(1);
	param40.push_back(1000);
	param40.push_back(1);
	param40.push_back(999);
	int ret4 = objectTheNicePair.solve(param40);
	int need4 = 5;
	assert_eq(4,ret4,need4);

	//test case5
	vector <int> param50;
	param50.push_back(1000);
	param50.push_back(1);
	param50.push_back(1);
	param50.push_back(1000);
	int ret5 = objectTheNicePair.solve(param50);
	int need5 = 3;
	assert_eq(5,ret5,need5);

	//test case6
	vector <int> param60;
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(953);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(953);
	param60.push_back(1);
	param60.push_back(953);
	param60.push_back(953);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(953);
	param60.push_back(953);
	param60.push_back(953);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(953);
	param60.push_back(953);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(1);
	param60.push_back(953);
	param60.push_back(953);
	param60.push_back(953);
	param60.push_back(1);
	int ret6 = objectTheNicePair.solve(param60);
	int need6 = 15;
	assert_eq(6,ret6,need6);

}
