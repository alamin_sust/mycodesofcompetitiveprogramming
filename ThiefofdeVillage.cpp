/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};
int mx,arr[105][105],i,j,n;
int main()
{
    freopen("InputFile-2.txt","r",stdin);
    freopen("ThiefofdeVillageOutput.txt","w",stdout);
    while(scanf("%d",&n)==1)
    {
        memset(arr,0,sizeof(arr));
       for(i=1;i<=n;i++)
       {
           for(j=1;j<=i;j++)
            scanf(" %d",&arr[i][j]);
       }

       for(i=2;i<=n;i++)
       {
           for(j=1;j<=i;j++)
           arr[i][j]+=max(arr[i-1][j],arr[i-1][j-1]);
       }
       mx=-99999999;
       for(i=1;i<=n;i++)
       {
           mx=max(arr[n][i],mx);
       }
       printf("Output: %d\n",mx);
    }
    return 0;
}


/*
4
3
21 23
05 13 15
02 18 09 04

5
92
31 72
85 31 21
25 90 22 08
30 50 54 86 88
*/

