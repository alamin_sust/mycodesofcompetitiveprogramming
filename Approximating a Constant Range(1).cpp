/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int n,i,res,now,k,hand,mn,mx,arr[100010];
vector<int>v,val;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d",&n);

    for(i=1;i<=n;i++)
    {
        scanf(" %d",&arr[i]);
    }

    for(i=1;i<=n;)
    {
        now=arr[i];
        k=0;
        while(i<=n&&now==arr[i])
        {
            i++;
            k++;
        }
        v.push_back(k);
        val.push_back(now);
    }
    res=v[0];
    mn=mx=val[0];
    hand=v[0];
    for(i=1;i<v.size();i++)
    {
        mn=min(mn,val[i]);
        mx=max(mx,val[i]);
        if((mx-mn)>1)
        {
            res=max(res,hand);
            hand=v[i]+v[i-1];
            mn=min(val[i],val[i-1]);
            mx=max(val[i],val[i-1]);
        }
        else
        {
            hand+=v[i];
        }
    }
    res=max(res,hand);

    printf("%d\n",res);

    return 0;
}

