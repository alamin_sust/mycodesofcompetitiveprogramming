#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <string.h>

using namespace std;

int rr[]= {1,1,2,2,-1,-1,-2,-2};
int cc[]= {2,-2,1,-1,2,-2,-1,1};

struct node
{
    int x,y;
};

node det,det2;
queue<node>q;
int val[120],arr[12][12],ans[12][12],stat[12][12],num;

void bfs(int row,int col,int x,int y)
{
    memset(stat,0,sizeof(stat));
    stat[x][y]=1;
    det.x=x;
    det.y=y;
    q.push(det);
    arr[det.x][det.y]=0;
    while(!q.empty())
    {
        det=q.front();
        q.pop();
        for(int i=0; i<8; i++)
        {
            det2.x=det.x+rr[i];
            det2.y=det.y+cc[i];
            if(det2.x>=0&&det2.x<row&&det2.y>=0&&det2.y<col&&stat[det2.x][det2.y]==0)
            {
                arr[det2.x][det2.y]=arr[det.x][det.y]+1;
                q.push(det2);
                stat[det2.x][det2.y]=1;
            }
        }
    }
    return;
}

class CollectingRiders
{
public:
    int minimalMoves(vector <string> board)
    {
        string str[12];
        int i,j,col=board[0].length(),ii,jj,res=99999999;
        for(i=0; i<board.size(); i++)
        {
            str[i]=board[i];
            //cout<<str[i]<<endl;
        }
        int row=i;
        //printf("%d %d\n",row,col);
        memset(ans,0,sizeof(ans));
        num=0;
        for(i=0; i<row; i++)
        {
            for(j=0; j<col; j++)
            {
                if(str[i][j]!='.')
                {
                    for(ii=0; ii<12; ii++)
                        for(jj=0; jj<12; jj++)
                            arr[ii][jj]=99999999;
                    val[num]=str[i][j]-'0',bfs(row,col,i,j);
                    //printf("ll");
                    for(ii=0; ii<row; ii++)
                    {
                        for(jj=0; jj<col; jj++)
                        {
                       //     printf("%d %d %d\n",ii,arr[ii][jj],val[num]);
                            ans[ii][jj]+=(arr[ii][jj]/val[num])+((arr[ii][jj]%val[num])==0?0:1);
                            //tp=max(tp,(arr[k][i][j]/val[k])+((arr[k][i][j]%val[k])==0?0:1));
                        }
                    }
                    num++;
                     //printf("pp");
                }
            }
        }
        //for(k=0;k<num;k++)
        /*for(i=0;i<row;i++)
        {
            for(j=0;j<col;j++)
            {
                printf("%d",arr[k][i][j]);
            }
            printf("\n");
        }*/
        for(i=0; i<row; i++)
        {
            for(j=0; j<col; j++)
            {
                res=min(res,ans[i][j]);//tp=max(tp,(arr[k][i][j]/val[k])+((arr[k][i][j]%val[k])==0?0:1));
            }
        }
        return res>=9999999?-1:res;
    }
};


template<typename T> void print( T a )
{
    cerr << a;
}

void print( long long a )
{
    cerr << a << "L";
}

void print( string a )
{
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a )
{
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ )
    {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need )
{
    if ( have == need )
    {
        cerr << "Case " << n << " passed." << endl;
    }
    else
    {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need )
{
    if( have.size() != need.size() )
    {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ )
    {
        if( have[i] != need[i] )
        {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need )
{
    if ( have == need )
    {
        cerr << "Case " << n << " passed." << endl;
    }
    else
    {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

    CollectingRiders objectCollectingRiders;

    //test case0
    vector <string> param00;
    param00.push_back("...1");
    param00.push_back("....");
    param00.push_back("2...");
    int ret0 = objectCollectingRiders.minimalMoves(param00);
    int need0 = 2;
    assert_eq(0,ret0,need0);

    //test case1
    vector <string> param10;
    param10.push_back("........");
    param10.push_back(".1......");
    param10.push_back("........");
    param10.push_back("....3...");
    param10.push_back("........");
    param10.push_back("........");
    param10.push_back(".7......");
    param10.push_back("........");
    int ret1 = objectCollectingRiders.minimalMoves(param10);
    int need1 = 2;
    assert_eq(1,ret1,need1);

    //test case2
    vector <string> param20;
    param20.push_back("..");
    param20.push_back("2.");
    param20.push_back("..");
    int ret2 = objectCollectingRiders.minimalMoves(param20);
    int need2 = 0;
    assert_eq(2,ret2,need2);

    //test case3
    vector <string> param30;
    param30.push_back(".1....1.");
    int ret3 = objectCollectingRiders.minimalMoves(param30);
    int need3 = -1;
    assert_eq(3,ret3,need3);

    //test case4
    vector <string> param40;
    param40.push_back("9133632343");
    param40.push_back("5286698232");
    param40.push_back("8329333369");
    param40.push_back("5425579782");
    param40.push_back("4465864375");
    param40.push_back("8192124686");
    param40.push_back("3191624314");
    param40.push_back("5198496853");
    param40.push_back("1638163997");
    param40.push_back("6457337215");
    int ret4 = objectCollectingRiders.minimalMoves(param40);
    int need4 = 121;
    assert_eq(4,ret4,need4);
}
