/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll arr[500010],t,p,i,j,k,n,n2,ans,res[500010],d,c,M=1000000007LL;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %lld",&t);

    for(p=1LL; p<=t; p++)
    {
        scanf(" %lld %lld",&n,&d);
        c=0LL;
        for(j=0LL; j<n; j++)
        {
            arr[j]=((c+d*d)%10);
            c=(c+d*d)/10LL;
        }
        while(c)
        {
            arr[j]=(c%10LL);
            j++;
            c/=10LL;
        }
        //for(i=0;i<j;i++)
        //    printf("--%lld--\n",arr[i]);
        n2=j;
        c=0LL;
        for(i=0LL; i<n; i++)
        {
            res[i]=c;
            for(j=0LL; j<=i; j++)
            {
              res[i]+=arr[j];
            }
            c=res[i]/10LL;
            res[i]=(res[i]%10LL);
        }
        if(d<=3LL)
        {
            for(i=1LL;i<=n-1LL;i++)
            {
                res[i+n-1LL]=c;
                for(j=i;j<=n-1LL;j++)
                {
                 res[i+n-1LL]+=arr[j];

                }
                c= res[i+n-1LL]/10LL;
                 res[i+n-1LL]=( res[i+n-1LL]%10LL);
            }
            while(c)
            {
                res[i+n-1LL]=c%10LL;
                c/=10LL;
                i++;
            }
        }
        else
        {
            for(i=1LL;i<=n;i++)
            {
                res[i+n-1LL]=c;
                for(j=i;j<=n;j++)
                {
                 res[i+n-1LL]+=arr[j];

                }
                c=res[i+n-1LL]/10LL;
                 res[i+n-1LL]=( res[i+n-1LL]%10LL);
            }
            while(c)
            {
                res[i+n-1LL]=c%10LL;
                c/=10LL;
                i++;
            }
        }
        n=i+n-1LL;;
        ans=0LL;
        for(j=1LL,i=n-1LL;i>=0;i--)
        {
            //printf("%lld.",res[i]);
            ans=(ans+(res[i]*j)%M)%M;
            j=(j*23LL)%M;
        }
        printf("%lld\n",ans);
    }

    return 0;
}


