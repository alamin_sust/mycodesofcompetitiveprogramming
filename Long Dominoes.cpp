/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll dp[30LL][9LL][(1LL<<9LL)+2LL][(1LL<<9LL)+2LL],n,m;

ll rec(ll row,ll col,ll next,ll nextnext)
{

    if(row>=n||col>=m)
    {
        return 1LL;
    }
     // printf("%lld %lld..\n",row,col);
    ll &ret=dp[row][col][next][nextnext];
    if(ret!=-1LL)
        return ret;
    ret=0LL;
    if(!((1LL<<col)&next))
    {
        if((col+2LL)<m&&(!((1LL<<(col+1LL))&next))&&(!((1LL<<(col+2LL))&next)))
        {
             ret+=rec((col+3LL)>=m?row+1LL:row,(col+3LL)>=m?0LL:col+3LL,((1LL<<col)|(1LL<<(col+1LL))|(1LL<<(col+2LL)))^(((1LL<<col)|(1LL<<(col+1LL))|(1LL<<(col+2LL)))|next),((1LL<<col)|(1LL<<(col+1LL))|(1LL<<(col+2LL)))^(((1LL<<col)|(1LL<<(col+1LL))|(1LL<<(col+2LL)))|nextnext));
        }
        if((row+2LL)<n)
        {
            //printf("%lld %lld %lld %lld==\n",row,col,(col+1LL)>=m?row+1LL:row,(col+1LL)>=m?0LL:col+1LL);
            ret+=rec((col+1LL)>=m?row+1LL:row,(col+1LL)>=m?0LL:col+1LL,next|(1LL<<col),nextnext|(1LL<<col));
        }
    }
    else
    {
        if(((1LL<<col)&nextnext))
        ret+=rec((col+1LL)>=m?row+1LL:row,(col+1LL)>=m?0LL:col+1LL,next,(1LL<<col)^((1LL<<col)|nextnext));
        else
        ret+=rec((col+1LL)>=m?row+1LL:row,(col+1LL)>=m?0LL:col+1LL,(1LL<<col)^((1LL<<col)|next),(1LL<<col)^((1LL<<col)|nextnext));
    }
    return ret;
}


int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    while(cin>>m>>n&&m&&n)
    {
        memset(dp,-1LL,sizeof(dp));
        printf("%lld\n",rec(0LL,0LL,0LL,0LL));
    }

    return 0;
}





