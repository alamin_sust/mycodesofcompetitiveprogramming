/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll dp[18],n,arr[20],p,t,k;

ll rec(ll button, ll val)
{
    printf("%lld[[\n",val);
    if(button==n)
        return val>=k?val:9999999999999999LL;
    ll &ret=dp[button];
    if(ret!=-1LL)
        return ret;
    ret=99999999999999LL;
    for(ll i=0LL;i<32LL;i++)
    {
        ll tp=rec(button+1,(i*arr[button])+val);
       printf("%lld.......\n",tp);
         //if(tp>=k)
            ret=min(ret,tp);
         //else ret=min(ret,9999999999999999LL);
    }
    return ret;
}

bool comp(ll a,ll b)
{
    return a>b;
}

int main()
{
    ll i;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %I64d %I64d",&n,&k);
        for(i=0;i<n;i++)
            scanf(" %I64d",&arr[i]);
        sort(arr,arr+n,comp);
        memset(dp,-1,sizeof(dp));

        printf("%lld\n",rec(0LL,0LL));
    }
    return 0;
}


