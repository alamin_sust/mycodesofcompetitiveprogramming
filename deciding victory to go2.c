#include<stdio.h>

char arr[30][30];
int flag;

void floodfill1(int i,int j)
{
    arr[i][j]='O';
    if(arr[i][j-1]=='o')
        floodfill1(i,j-1);
    if(arr[i][j+1]=='o')
        floodfill1(i,j+1);
    if(arr[i-1][j]=='o')
        floodfill1(i-1,j);
    if(arr[i+1][j]=='o')
        floodfill1(i+1,j);
    return;
}

void floodfill2(int i,int j)
{
    arr[i][j]='X';
    if(arr[i][j-1]=='x')
        floodfill2(i,j-1);
    if(arr[i][j+1]=='x')
        floodfill2(i,j+1);
    if(arr[i-1][j]=='x')
        floodfill2(i-1,j);
    if(arr[i+1][j]=='x')
        floodfill2(i+1,j);
    return;
}

void fill1(int i,int j)
{
    arr[i][j]='o';
    if(arr[i][j-1]=='X'||arr[i][j+1]=='X'||arr[i-1][j]=='X'||arr[i+1][j]=='X')
    {
        flag=1;
        return;
    }
    if(arr[i][j-1]=='.')
        fill1(i,j-1);
    if(arr[i][j+1]=='.')
        fill1(i,j+1);
    if(arr[i-1][j]=='.')
        fill1(i-1,j);
    if(arr[i+1][j]=='.')
        fill1(i+1,j);
    return;
}

void fill2(int i,int j)
{
    arr[i][j]='x';
    if(arr[i][j-1]=='O'||arr[i][j+1]=='O'||arr[i-1][j]=='O'||arr[i+1][j]=='O')
    {
        flag=1;
        return;
    }
    if(arr[i][j-1]=='.')
        fill2(i,j-1);
    if(arr[i][j+1]=='.')
        fill2(i,j+1);
    if(arr[i-1][j]=='.')
        fill2(i-1,j);
    if(arr[i+1][j]=='.')
        fill2(i+1,j);
    return;
}

main()
{
    int n,i,j,p,black,white;
    char ch;
    scanf(" %d%c",&n,&ch);
    for(i=0; i<11; i++)
    {
        for(j=0; j<11; j++)
            arr[i][j]='#';
    }
    for(p=0; p<n; p++)
    {
        for(i=1; i<=9; i++)
        {
            for(j=1; j<=9; j++)
            {
                scanf(" %c",&arr[i][j]);
            }
            scanf("%c",&ch);
        }
        for(i=1; i<=9; i++)
        {
            for(j=1; j<=9; j++)
            {
                if(arr[i][j]=='O')
                {
                    flag=0;
                    if(arr[i-1][j]=='.')
                    {
                        fill1(i-1,j);
                        if(flag==0)
                            floodfill1(i-1,j);
                    }
                    else if(arr[i][j-1]=='.')
                    {
                        fill1(i,j-1);
                        if(flag==0)
                            floodfill1(i,j-1);
                    }
                    else if(arr[i+1][j]=='.')
                    {
                        fill1(i+1,j);
                        if(flag==0)
                            floodfill1(i+1,j);
                    }
                    else if(arr[i][j+1]=='.')
                    {
                        fill1(i,j+1);
                        if(flag==0)
                            floodfill1(i,j+1);
                    }

                }
                else if(arr[i][j]=='X')
                {
                    flag=0;
                    if(arr[i-1][j]=='.')
                    {
                        fill2(i-1,j);
                        if(flag==0)
                            floodfill2(i-1,j);
                    }
                    else if(arr[i][j-1]=='.')
                    {
                        fill2(i,j-1);
                        if(flag==0)
                            floodfill2(i,j-1);
                    }
                    else if(arr[i+1][j]=='.')
                    {
                        fill2(i+1,j);
                        if(flag==0)
                            floodfill2(i+1,j);
                    }
                    else if(arr[i][j+1]=='.')
                    {
                        fill2(i,j+1);
                        if(flag==0)
                            floodfill2(i,j+1);
                    }
                }
            }
        }
        for(black=0,white=0,i=1; i<=9; i++)
        {
            for(j=1; j<=9; j++)
            {
                if(arr[i][j]=='X')
                    black++;
                else if(arr[i][j]=='O')
                    white++;
            }
        }
        printf("Black %d White %d\n",black,white);
    }
    return 0;
}
