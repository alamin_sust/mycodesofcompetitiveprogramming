#include<stdio.h>
#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

int n,arr[1010][1010],res[1010][1010],dir;

pair<int,int> func(int mx,int my,int nx,int ny)
{
    pair<int,int> ret,ret2,ret3,retl,retr;
    //if(dir==0)
    {
        if(mx+1==nx)
        {
            retl.first=mx+1;
            retl.second=my+1;
        }
        else if(my+1==ny)
        {
            retl.first=mx-1;
            retl.second=my+1;
        }
        else if(nx+1==mx)
        {
            retl.first=mx-1;
            retl.second=my-1;
        }
        else
        {
            retl.first=mx+1;
            retl.second=my-1;
        }
    }
    //else
    {
        if(my+1==ny)
        {
            retr.first=mx+1;
            retr.second=my+1;
        }
        else if(mx+1==nx)
        {
            retr.first=mx+1;
            retr.second=my-1;
        }
        else if(ny+1==my)
        {
            retr.first=mx-1;
            retr.second=my-1;
        }
        else
        {
            retr.first=mx-1;
            retr.second=my+1;
        }
    }

    if(my+1==ny)
    {
        ret2.first=mx;
        ret2.second=my+2;
        ret3.first=mx;
        ret3.second=my+3;
    }
    else if(mx+1==nx)
    {
        ret2.first=mx+2;
        ret2.second=my;
        ret3.first=mx+3;
        ret3.second=my;
    }
    else if(ny+1==my)
    {
        ret2.first=mx;
        ret2.second=my-2;
        ret3.first=mx;
        ret3.second=my-3;
    }
    else
    {
        ret2.first=mx-2;
        ret2.second=my;
        ret3.first=mx-3;
        ret3.second=my;
    }

    printf("dir: %d nowpos: %d %d -> %d %d, turnleft: %d %d turnright: %d %d shoja1ghor: %d %d shoja2ghor: %d %d\n",dir,mx,my,nx,ny,retl.first,retl.second,retr.first,retr.second,ret2.first,ret2.second,ret3.first,ret3.second);

    if(dir==0&&arr[ret2.first][ret2.second]==0&&(ret3.first>n||ret3.second>n||ret3.first<1||ret3.second<1)&&(retl.first>1&&retl.second>1&&retl.first<n&&retl.second<n))
    {
        //printf("%d %d %d %d..\n",mx,my,nx,ny);
        ret=ret2;
    }
    else if(dir==1&&arr[ret2.first][ret2.second]==0&&(ret3.first>n||ret3.second>n||ret3.first<1||ret3.second<1)&&(retr.first>1&&retr.second>1&&retr.first<n&&retr.second<n))
    {
       // printf("%d %d %d %d..%d %d\n",mx,my,nx,ny,retr.first,retr.second);
        ret=ret2;
    }
    else
    {
        if(dir==0)
        {
            if(arr[retl.first][retl.second]==0)
            {
                ret=retl;
            }
            else if(arr[retr.first][retr.second]==0)
            {
                printf("OOOOOOOOOOOOOOOO");
                ret=retr;
                dir=1;
            }
            else
            {
                ret=ret2;
            }
        }
        else
        {
            if(arr[retr.first][retr.second]==0)
            {
                ret=retr;
            }
            else if(arr[retl.first][retl.second]==0)
            {
                ret=retl;
                dir=0;
            }
            else
            {
                ret=ret2;
            }
        }
    }
    return ret;
}


int main()
{
    int t,p,i,j,mx,my,ny,nx,l;
    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);

        for(i=0;i<=n+1;i++)
        {
            for(j=0;j<=n+1;j++)
            {

                arr[i][j]=0;
                if(i>n||j>n||i<1||j<1)
                {
                    arr[i][j]=1;
                }
            }
        }
        arr[1][1]=arr[1][2]=1;

        mx=1;
        my=2;
        nx=ny=1;
        dir=0;
        i=2;
        l=n*n;
        res[1][1]=2;
        res[1][2]=1;
        for(;i<l;i++)
        {
            pair<int,int>tmp=func(mx,my,nx,ny);
            mx=nx;
            my=ny;
            nx=tmp.first;
            ny=tmp.second;
            res[nx][ny]=i+1;
            arr[nx][ny]=1;
           // printf("%d %d..\n",nx,ny);
        }

        for(i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
            {
                printf("%d",res[i][j]);
                if(j==n)
                    printf("\n");
                else printf(" ");
            }
        }

    }
    return 0;
}
