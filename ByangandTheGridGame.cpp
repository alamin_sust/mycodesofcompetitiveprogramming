/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll p,k,x[100010],y[100010],dx[1010],dy[1010],ddx,ddy,i,j,fg,flag;


int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %lld %lld %lld %lld",&p,&k,&x[0],&y[0]);

    for(i=1; i<=p; i++)
    {
        scanf(" %lld %lld",&x[i],&y[i]);
    }
    flag=0;
    for(i=1; i<=k; i++)
    {
        scanf(" %lld %lld",&dx[i],&dy[i]);

    }

    for(i=1; i<=p; i++)
    {
        fg=0;
        ddx=x[i]-x[i-1];
        ddy=y[i]-y[i-1];
        for(j=1; j<=k; j++)
        {
            if(dx[j]==0&&dy[j]==0)
                continue;
            if(dx[j]==0)
            {
                if(dy[j]>0&&ddy>0)
                {
                    if(((ddy)%(dy[j]))==0)
                    {
                        fg=1;
                        break;
                    }
                }
                else if(dy[j]<0&&ddy<0)
                {
                    if(((-ddy)%(-dy[j]))==0)
                    {
                        fg=1;
                        break;
                    }

                }
            }
            else if(dy[j]==0)
            {
                if(dx[j]>0&&ddx>0)
                {
                    if(((ddx)%(dx[j]))==0)
                    {
                        fg=1;
                        break;
                    }
                }
                else if(dx[j]<0&&ddx<0)
                {
                    if(((-ddx)%(-dx[j]))==0)
                    {
                        fg=1;
                        break;
                    }
                }

            }
            else
            {
                if(dx[j]>0&&ddx>0)
                {
                    if(dy[j]>0&&ddy>0)
                    {
                        if(((ddx)%(dx[j]))==0&&((ddy)%(dy[j]))==0&&((ddx)/(dx[j]))==((ddy)/(dy[j])))
                        {
                            fg=1;
                            break;
                        }
                    }
                    else if(dy[j]<0&&ddy<0)
                    {
                        if(((ddx)%(dx[j]))==0&&((-ddy)%(-dy[j]))==0&&((ddx)/(dx[j]))==((-ddy)/(-dy[j])))
                        {
                            fg=1;
                            break;
                        }
                    }
                }
                else if(dx[j]<0&&ddx<0)
                {
                    if(dy[j]>0&&ddy>0)
                    {
                        if(((-ddx)%(-dx[j]))==0&&((ddy)%(dy[j]))==0&&((-ddx)/(-dx[j]))==((ddy)/(dy[j])))
                        {
                            fg=1;
                            break;
                        }
                    }
                    else if(dy[j]<0&&ddy<0)
                    {
                        if(((-ddx)%(-dx[j]))==0&&((-ddy)%(-dy[j]))==0&&((-ddx)/(-dx[j]))==((-ddy)/(-dy[j])))
                        {
                            fg=1;
                            break;
                        }
                    }
                }

            }
        }
        if(fg==0)
        {
            flag=1;
            break;
        }
    }

    if(flag==0)
        printf("Yes\n");
    else
        printf("No\n");

    return 0;
}

