#include<stdio.h>
#include<math.h>
#include<string.h>

long long int gcd(long long int a,long long int b)
{
    if(b==0) return a;
    else gcd(b,a%b);
}

main()
{
    long long int n,m,j,i,a,b,in,res;
    scanf(" %lld",&n);
    for(i=1;i<=n;i++)
    {
        scanf(" %lld",&m);
        scanf(" %lld",&in);
        a=1;
        b=in;
        for(j=2;j<=m;j++)
        {
            scanf(" %lld",&in);
            a*=in;
            a+=b;
            b*=in;
        }
        b*=m;
        res=gcd(a,b);
        a/=res;
        b/=res;
        printf("Case %lld: %lld/%lld\n",i,b,a);
    }
    return 0;
}
