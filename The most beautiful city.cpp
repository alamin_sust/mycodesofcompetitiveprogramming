#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int divs[1000010],flag[1000010];

void init(void)
{
    for(int i=2;i<=1000000;i++)
    {
        for(int j=i;j<=1000000;j+=i)
        {
            divs[j]++;
        }
    }
    return;
}


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    init();
    int n,k,in;
    cin>>n>>k;
    for(int i=1;i<=k;i++)
    {
        cin>>in;
        flag[in]++;
    }
    for(int i=1;i<=n;i++)
    {
        if(flag[i]>0&&(divs[i]&1)==0)
        {
            printf("%d %d\n",i,flag[i]);
        }
    }
    return 0;
}

