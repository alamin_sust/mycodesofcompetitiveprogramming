#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

int matf[35][5][5],matg[35][5][5],m,res1[5][5],res2[5][5];

void big_mod(long long int x)
{
    long long int i,j,k;
    for(i=1; i<=3; i++)
        for(j=1; j<=3; j++)
        {
            matf[x][i][j]=matg[x][i][j]=0;
            for(k=1; k<=3; k++)
            {
                matf[x][i][j]+=(matf[x-1][i][k]*matf[x-1][k][j])%m;
                matg[x][i][j]+=(matg[x-1][i][k]*matg[x-1][k][j])%m;
            }
            matf[x][i][j]%=m;
            matg[x][i][j]%=m;
        }
        return;
}

void mat_mul1(long long int x)
{
    long long int temp[5][5];
    for(long long int i=1; i<=3; i++)
    {
        for(long long int j=1; j<=3; j++)
        {
            temp[i][j]=res1[i][j];
        }
    }
    for(long long int i=1; i<=3; i++)
    {
        for(long long int j=1; j<=3; j++)
        {
            res1[i][j]=0;
            for(long long int k=1; k<=3; k++)
                res1[i][j]+=(temp[i][k]*matf[x][k][j])%m;
            res1[i][j]%=m;
        }
    }
    return;
}

void mat_mul2(long long int x)
{
    long long int temp[5][5];
    for(long long int i=1; i<=3; i++)
    {
        for(long long int j=1; j<=3; j++)
        {
            temp[i][j]=res2[i][j];
        }
    }
    for(long long int i=1; i<=3; i++)
    {
        for(long long int j=1; j<=3; j++)
        {
            res2[i][j]=0;
            for(long long int k=1; k<=3; k++)
                res2[i][j]+=(temp[i][k]*matg[x][k][j])%m;
            res2[i][j]%=m;
        }
    }
    return;
}

main()
{
    long long int i,n,p,t,a[4],b[4],k,c[4],q,f[4],g[4],ff,gg;
    cin>>t;
    for(p=1; p<=t; p++)
    {
        cin>>a[1]>>b[1]>>c[1];
        cin>>a[2]>>b[2]>>c[2];
        cin>>f[0]>>f[1]>>f[2];
        cin>>g[0]>>g[1]>>g[2];
        cin>>m;
        matf[0][1][1]=a[1]%m;
        matf[0][1][2]=b[1]%m;
        matf[0][1][3]=c[1]%m;
        matg[0][1][1]=a[2]%m;
        matg[0][1][2]=b[2]%m;
        matg[0][1][3]=c[2]%m;
        matf[0][2][1]=1;
        matf[0][3][2]=1;
        matg[0][2][1]=1;
        matg[0][3][2]=1;
        for(i=1; i<=32; i++)
            big_mod(i);
        cin>>q;
        printf("Case %lld:\n",p);
        for(i=1; i<=q; i++)
        {
            cin>>n;
            if(n<=2)
            {
                ff=f[n]%m;
                gg=g[n]%m;
            }
            else
            {
                n-=2;
                k=0;
                res1[1][1]=res1[2][2]=res1[3][3]=1;
                res1[2][1]=res1[1][2]=res1[1][3]=0;
                res1[2][3]=res1[3][1]=res1[3][2]=0;
                res2[1][1]=res2[2][2]=res2[3][3]=1;
                res2[2][1]=res2[1][2]=res2[1][3]=0;
                res2[2][3]=res2[3][1]=res2[3][2]=0;
                while((n>>k)|0!=0)
                {
                    if((n>>k)&1==1)
                    {
                        mat_mul1(k);
                        mat_mul2(k);
                    }
                    k++;
                }
                //printf("%d %d %d..\n",res1[1][1],res1[1][2],res1[1][3]);
                ff=((res1[1][1]*f[2]))+((res1[1][2]*f[1]))+((res1[1][3]*g[0]));
                gg=((res2[1][1]*g[2]))+((res2[1][2]*g[1]))+((res2[1][3]*f[0]));
                ff%=m;
                gg%=m;
            }
            printf("%lld %lld\n",ff,gg);
        }
    }
    return 0;
}
