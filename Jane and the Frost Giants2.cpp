#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

int det[210][210],row,col,adj1[210][210],adj2[210][210];
char arr[210][210];

int r[]={0,0,-1,1};
int c[]={-1,1,0,0};

struct node{
int x,y;
};

node que[40010];

void rec1(int i,int j,int val)
{
    int front=1,rear=1,xx,yy;
    que[front].x=i;
    que[front].y=j;
    while(front<=rear)
    {
        xx=que[front].x;
        yy=que[front++].y;
        //val=adj1[xx][yy]+1;
        for(int k=0;k<4;k++)
        {
            if(arr[xx+r[k]][yy+c[k]]!='#'&&adj1[xx+r[k]][yy+c[k]]==inf)
            {
                que[++rear].x=xx+r[k];
                que[rear].y=yy+c[k];
                adj1[xx+r[k]][yy+c[k]]=adj1[xx][yy]+1;
            }
        }
        //val++;
    }
    return;
}

void rec2(int i,int j,int val)
{
    int front=1,rear=1,xx,yy;
    que[front].x=i;
    que[front].y=j;
    memset(det,0,sizeof(det));
    det[i][j]=1;
    while(front<=rear)
    {
        xx=que[front].x;
        yy=que[front++].y;
        val=adj2[xx][yy]+1;
        for(int k=0;k<4;k++)
        {
            if(adj2[xx+r[k]][yy+c[k]]>val&&arr[xx+r[k]][yy+c[k]]!='#')//&&det[xx+r[k]][yy+c[k]]==0)
            {
                det[xx+r[k]][yy+c[k]]=1;
                que[++rear].x=xx+r[k];
                que[rear].y=yy+c[k];
                adj2[xx+r[k]][yy+c[k]]=val;
            }
        }
        //val++;
    }
    return;
}


main()
{
    int mn,i,j,p,t,jane_i,jane_j;
    cin>>t;
    for(p=1; p<=t; p++)
    {
        cin>>row>>col;
        getchar();
        for(i=0;i<=row+1;i++)
            for(j=0;j<=col+1;j++)
            arr[i][j]='#';
        for(i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
            {
                scanf("%c",&arr[i][j]);
                if(arr[i][j]=='J')
                {
                    jane_i=i;
                    jane_j=j;
                }
                //if(arr[i][j]=='F')
                // {
                //   frost_i=i;
                //    frost_j=j;
                // }
                adj1[i][j]=inf;
                adj2[i][j]=inf;
            }
            getchar();
        }
        adj1[jane_i][jane_j]=0;
        rec1(jane_i,jane_j,1);
        //getchar();
        for(i=1; i<=row; i++)
            for(j=1; j<=col; j++)
            {
                if(arr[i][j]=='F')
                {
                    adj2[i][j]=0;
                   rec2(i,j,1);
                }
            }
     /*  for(i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
            {
                if(adj1[i][j]==inf)
                    printf("0");
                else
                    printf("%d",adj1[i][j]);
            }
            cout<<endl;
        }
        cout<<endl;
        for(i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
            {
                if(adj2[i][j]==inf)
                    printf("0");
                else
                    printf("%d",adj2[i][j]);
            }
            cout<<endl;
        }*/
        for(mn=inf,i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
            {
                if(i==1||j==1||i==row||j==col)
                {
                    if(adj1[i][j]<adj2[i][j])
                    {
                        mn=min(mn,adj1[i][j]);
                    }
                }
            }

        }
        if(mn==inf)
        {
            printf("Case %d: IMPOSSIBLE\n",p);
        }
        else
            printf("Case %d: %d\n",p,mn+1);
    }
    return 0;
}

/*
2
4 5
##.##
#JF.#
#...#
#...#
3 3
###
#J.
#.F
*/

