/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int l,i,j,k,p,tp[110],cnt;
char arr[110];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %s",arr);

    l=strlen(arr);

    for(i=0;i<l;i++)
    {
        if(arr[i]=='0')
        {
            printf("YES\n");
            printf("0\n");
            return 0;
        }
    }

    for(i=8;i<1000;i+=8)
    {
        k=0;
        j=i;
        while(j)
        {
            tp[k++]=j%10;
            j/=10;
        }
        cnt=0;
        p=l-1;
        for(j=0;j<k;j++)
        {
            while(p>=0)
            {
                if((arr[p]-'0')==tp[j])
                {

                    cnt++;
                    p--;
                    break;
                }
                p--;
            }
        }
        if(cnt==k)
        {
            printf("YES\n");
            printf("%d\n",i);
            return 0;
        }
    }

    printf("NO\n");

    return 0;
}


