#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int arr[22],side,dp[4][1<<20],n;

int calc(int mask,int k)
{
    int ret=0;
    for(int i=0;i<n;i++)
    {
        if(((1<<i)&mask)!=0)
        ret+=arr[i];
    }
    return ret-(k*side);
}

int rec(int lev,int mask)
{
    int l=calc(mask,lev);
    if(l==side)
        return rec(lev+1,mask);
    if(lev==4)
        return 1;
    int &ret=dp[lev][mask];
    if(ret!=-1)
        return ret;
    ret=0;
    for(int i=0;ret==0&&i<n;i++)
    {
        if(((1<<i)&mask)==0&&(l+arr[i])<=side)
        {
                ret=max(ret,rec(lev,(1<<i)|mask));
        }
    }
    return ret;
}

main()
{
    int p,t,i,aggre;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n;
        aggre=0;
        for(i=0;i<n;i++)
        {
            scanf(" %d",&arr[i]);
            aggre+=arr[i];
        }
        if(aggre%4==0)
        {
            int k=(1<<n);
            int j;
            side=aggre/4;
            for(i=0;i<4;i++)
            {
                for(j=0;j<k;j++)
                    dp[i][j]=-1;
            }
            //memset(dp,-1,sizeof(dp));
            if(rec(0,0))
                printf("yes\n");
            else
                printf("no\n");
        }
        else
        {
            printf("no\n");
        }
    }
    return 0;
}

