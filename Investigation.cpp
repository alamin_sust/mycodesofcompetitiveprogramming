#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll flag[12][83][83][2],dp[12][83][83][2],p,t,n,k,len,cnt;
char num[12];

ll solve(ll pos,ll divmod,ll summod,ll border)
{
    if(pos==len)
    return (!divmod)&&(!summod);
    ll &ret=dp[pos][divmod][summod][border];
    if(flag[pos][divmod][summod][border]==cnt)
        return ret;
    flag[pos][divmod][summod][border]=cnt;
    ret=0;
    ll now=num[pos]-'0';
    for(ll i=0;i<=9;i++)
    {
        if(i>now&&border==0)
            break;
        ll nmod=(divmod*10+i)%k;
        ll smod=(summod+i)%k;
        ll bdr=i<now?1:border;
        ret+=solve(pos+1,nmod,smod,bdr);
    }
    return ret;
}

ll get(ll n)
{
    ll ret;
    sprintf(num,"%lld",n);
    len=strlen(num);
    cnt++;
    ret=solve(0,0,0,0);
    return ret;
}

main()
{
    ll a,b;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>a>>b>>k;
        if(k>82)
        printf("Case %lld: 0\n",p);
        else
        printf("Case %lld: %lld\n",p,get(b)-get(a-1));
    }
    return 0;
}
