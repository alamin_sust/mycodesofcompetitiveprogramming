#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
ll dp[43][2][43],c,p,a,b,res;
ll rec(ll pos,ll flag,ll ones)
{
    if(pos==-1)
        return ones;
    ll &ret=dp[pos][flag][ones];
    if(ret!=-1)
        return ret;
    ret=0;
    if(flag==1)
    {
        ret=rec(pos-1,flag,ones+1)+rec(pos-1,flag,ones);
    }
    else
    {

        if((c>>pos)&1LL==1)
        {
            ret=rec(pos-1,1,ones)+rec(pos-1,0,ones+1);
        }
        else
            ret=rec(pos-1,0,ones);
    }
    return ret;
}

main()
{
    p=1;
    while(cin>>a>>b)
    {
        if(a==0&&b==0)
            break;
        c=b;
        memset(dp,-1,sizeof(dp));
        res=rec(40,0,0);
        c=a-1;
        memset(dp,-1,sizeof(dp));
        if(c>=0)
        res-=rec(40,0,0);
        printf("Case %lld: %lld\n",p++,res);
    }
    return 0;
}

