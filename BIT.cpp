/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define SZ 16
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int T[17],arr[17];

void BIT_set(int i,int val)
{
    while(i<=SZ)
    {
        T[i]+=val;
        i=i+(i&(-i));
    }
    return;
}

int BIT_get(int i)
{
    int ret=0;
    while(i>0)
    {
        ret+=T[i];
        i=(i-(i&(-i)));
    }
    return ret;
}

main()
{
    int n,q,x,y,i;
    char ch;
    cin>>n>>q;  // array length=n, query=q
    for(i=1;i<=n;i++)
    {
        scanf(" %d",&arr[i]);
        BIT_set(i,arr[i]);
    }
    for(i=1;i<=q;i++)
    {
        scanf(" %c %d",&ch,&x);
        if(ch=='S')
            scanf(" %d",&y),BIT_set(x,y-arr[x]);
        else
            cout<<BIT_get(x)<<endl;
    }
    return 0;
}
