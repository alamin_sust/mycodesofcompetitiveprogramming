#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int t,p,i,x,y,node[510],n,m,fr,to,flag;
main()
{
    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %d %d",&n,&m);
        memset(node,0,sizeof(node));
        for(i=1;i<=m;i++)
        {
            scanf(" %d %d",&fr,&to);
            node[fr]++;
            node[to]++;
        }
        flag=0;
        if(m!=(n-1))
            {printf("Case %d: NotValid\n",p);
            continue;}
        x=0;
        y=0;
        for(i=1;i<=n;i++)
        {
            if(node[i]==0||node[i]>4)
            {
                flag=1;
                break;
            }
            if(node[i]<=2)
                continue;
            if(node[i]==3)
                y++;
            if(node[i]==4)
                x++;
        }
        if(x>1||y>1)
            flag=1;
        if((x+y)>1)
            flag=1;
        if(flag==1)
            printf("Case %d: NotValid\n",p);
        else if(x==1)
            printf("Case %d: X\n",p);
        else if(y==1)
            printf("Case %d: Y\n",p);
        else printf("Case %d: I\n",p);

    }
    return 0;
}

