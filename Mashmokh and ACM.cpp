#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll n,k,dp[2010][2010],res,modulo;

ll solve(ll taken,ll value)
{
    if(taken==k)
        return 1LL;
    ll &ret=dp[taken][value];
    if(ret!=-1)
        return ret%modulo;
    ret=0;
    ll call=value;
    for(ll i=1;i*call<=n;i++)
    {
        ret+=solve(taken+1,i*call);
    }
    return ret%modulo;
}

main()
{
    modulo=1000000007LL;
    cin>>n>>k;
    memset(dp,-1,sizeof(dp));
    res=solve(0,1);
    cout<<res<<endl;
    return 0;
}
