#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define PB push_back
using namespace std;

int dp[20][1<<20],m;

int rec(int pos,int mask)  //Valid Signal Elements calculation
{
    if(pos==m)
    {
        for(int i=0;i<m-1;i++)
        {
            if(i==0)
            {
                if((mask&(1<<i))==0&&(mask&(1<<(i+1)))==0)
                {
                    return 0;
                }
            }
            else if(i==(m-2))
            {
                if((mask&(1<<i-2))==0&&(mask&(1<<(i-1)))==0)
                {
                    return 0;
                }
            }
            else if((mask&(1<<i))==0&&(mask&(1<<(i+1)))==0&&(mask&(1<<(i+2)))==0)
            return 0;
        }
        return 1;
    }
    int &ret=dp[pos][mask];
    if(ret!=-1)
        return ret;
    ret=0;
    ret=rec(pos+1,mask|(1<<pos))+rec(pos+1,mask);
    return ret;
}

int get_power(int a,int b)  //Calculates a^b (a to the power b)
{
    if(b==0)
        return 1;
    return a*get_power(a,b-1);
}

int main()
{
    while(cin>>m)
    {
    memset(dp,-1,sizeof(dp)); // The code calculates mBnB;
    printf("Total Available Signal Elements in %dB: %d\n",m,get_power(2,m));
    printf("Total Valid Signal Elements in %dB: %d\n",m,rec(0,0));
    }
    return 0;
}


