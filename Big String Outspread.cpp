/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back


using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

stack<int>stk;
char arr[10000010],l,st,en,tp[10000010],newtp[10000010],arr2[10000010];

int getparenthesis(void)
{
    while(!stk.empty())
            stk.pop();
    for(int i=0;i<l;i++)
        {
            if(arr[i]=='(')
                stk.push(i);
            if(arr[i]==')')
            {
                st=stk.top();
                stk.pop();
                en=i;
                return 1;
            }
        }
    return 0;
}

void mem(void)
{
    for(int i=0;arr[i]!='\0';i++)
        arr[i]='\0';
    return;
}
void memtp(void)
{
    for(int i=0;tp[i]!='\0';i++)
        tp[i]='\0';
    return;
}

void memnewtp(void)
{
    for(int i=0;newtp[i]!='\0';i++)
        newtp[i]='\0';
    return;
}

int main()
{
    int j,t,p,x,num,y,k,i;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    scanf(" %d",&t);
    getchar();
    for(p=1;p<=t;p++)
    {
        gets(arr);
        l=strlen(arr);
        for(i=0,j=0;i<l;i++)
        {
            if(arr[i]==' ')
                continue;
            arr[j++]=arr[i];
        }
        l=j;
        arr[l+2]=')';
        for(i=l+1;i>=2;i--)
            arr[i]=arr[i-2];
        arr[0]='1';
        arr[1]='(';
        l+=3;
        for(i=0;i<l;i++)
            arr2[i]=arr[i];
        //printf("..%s..\n",arr);
        for(i=2,j=2;i<l;i++)
        {
            if(arr2[i]=='('&&(!(arr2[i-1]>='0'&&arr2[i-1]<='9')))
            {
                arr[j++]='1';
            }
            arr[j++]=arr2[i];
        }
        l=j;
        //cout<<arr<<endl;
        while(getparenthesis())
        {
            //printf("st-%d en-%d\n",st,en);
            x=0;
            memtp();
            for(j=st+1;j<en;j++)
            {
                if(arr[j]>='0'&&arr[j]<='9')
                {
                    num=0;
                    for(;;j++)
                    {
                        if(!(arr[j]>='0'&&arr[j]<='9'))
                        {
                            for(y=0;y<num;x++,y++)
                                tp[x]=arr[j];
                            break;
                        }
                        num=num*10+arr[j]-'0';
                    }
                }
                else
                    tp[x++]=arr[j];
            }
            j=st-1;
            memnewtp();
            num=0;
            k=1;
            while((j+1)&&arr[j]>='0'&&arr[j]<='9')
            {
                num+=(k*(arr[j]-'0'));
                k*=10;
                j--;
            }
            for(i=0;i<=j;i++)
                newtp[i]=arr[i];
            k=j+1;
            for(i=0;i<num;i++)
            {
                for(j=0;j<x;j++)
                    newtp[k++]=tp[j];
            }
            for(i=en+1;i<l;i++)
                newtp[k++]=arr[i];
            l=k;
            mem();
            for(i=0;i<k;i++)
                arr[i]=newtp[i];
        }
        printf("%s\n",arr);
    }
    return 0;
}




