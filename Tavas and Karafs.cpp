/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll low,high,mid,i,s[2000010],cum[2000010],A,B,t,p,l,m,n,res;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    cin>>A>>B>>n;

    for(ll i=1LL;i<=2000000LL;i++)
    {
        s[i]=A+(i-1LL)*B;
        cum[i]=cum[i-1]+s[i];
    }

    for(p=1LL;p<=n;p++)
    {
        cin>>l>>t>>m;
        if(s[l]>t)
        {
            printf("-1\n");
            continue;
        }
        low=l;
        high=2000000LL;
        res=l;
        while(low<=high)
        {
            mid=(low+high)/2LL;
            if((cum[mid]-cum[l-1])<=(t*m)&&s[mid]<=t)
            {
                res=max(res,mid);
                low=mid+1LL;
            }
            else
                high=mid-1LL;
        }
        printf("%I64d\n",res);
    }

    return 0;
}





