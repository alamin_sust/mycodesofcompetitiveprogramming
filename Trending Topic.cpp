/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node{
string arr;
int cnt;
};

node det;

bool operator<(node a,node b)
{
    if(a.cnt==b.cnt)
        return a.arr>b.arr;
    return a.cnt<b.cnt;
}

map<string,int>mpp;
priority_queue<node>pq;
queue<node>q;

char arr2[110];
string arr;
int cnt,topp;

int main()
{
    while(scanf("%s",arr2)!=EOF)
    {
        if(strlen(arr2)==6)
        {
            while((!q.empty())&&(q.front().cnt+7)<=cnt)
            mpp[q.front().arr]--,q.pop();
            while(cin>>arr)
            {
                if(arr=="</text>")
                    break;
                if(arr.size()<4)
                    continue;
                det.cnt=cnt;
                det.arr=arr;
                q.push(det);
                mpp[arr]++;
            }
            cnt++;
        }
        else
        {
            scanf(" %d %s",&topp,arr2);
            while(!pq.empty())
                pq.pop();
            for(map<string,int> :: iterator it=mpp.begin();it!=mpp.end();it++)
            {
                det.arr=it->first;
                det.cnt=it->second;
                pq.push(det);
            }
            printf("<top %d>\n",topp);
            for(int i=0;(!pq.empty())&&i<topp;)
            {
                int tmp=pq.top().cnt;
                while((!pq.empty())&&pq.top().cnt==tmp)
                {
                    cout<<pq.top().arr;
                    printf(" %d\n",pq.top().cnt);
                    pq.pop();
                    i++;
                }
            }
            printf("</top>\n");
        }
    }
    return 0;
}

