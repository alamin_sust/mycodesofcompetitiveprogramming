/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


char arr[10010][1010];
int fg,l[10010],i,k,t,p,flag,A,B,C,D,E,val[10010];


int main(int argc, const char **argv)
{
    //ios::sync_with_stdio(false);
    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);
    //double st=clock(),en;
    //en=clock();
    //cerr<<(double)(en-st)/CLOCKS_PER_SEC<<endl;
    cin>>t;
    for(p=1; p<=t; p++)
    {
        memset(l,0,sizeof(l));
        memset(val,0,sizeof(val));
        flag=0;
        for(k=0; !flag; k++)
        {
            val[k]=0;
            fg=0;
            for(; fg==0&& scanf("%s",&arr[k]);)
            {
                if(arr[k][0]==',')
                    break;
                if(arr[k][0]=='.')
                {
                    flag=1;
                    break;
                }
                l[k]=strlen(arr[k]);
                if(arr[k][l[k]-1]!=','&&arr[k][l[k]-1]!='.')
                {
                    arr[k][l[k]]=',';
                    l[k]++;
                }
                else fg=1;

                int tp=0;

                if(l[k]-1==6)
                    for(i=1; i<l[k]-1; i++)
                    {
                        if((arr[k][i-1]=='N'||arr[k][i-1]=='n')&&(arr[k][i]=='G'||arr[k][i]=='g'))
                        {
                            tp=2;
                            break;
                        }
                        if((arr[k][i-1]=='N'||arr[k][i-1]=='n')&&(arr[k][i]=='Y'||arr[k][i]=='y'))
                        {
                            tp=2;
                            break;
                        }
                    }

                if(l[k]-1==3&&(arr[k][0]=='a'||arr[k][0]=='e'||arr[k][0]=='i'||arr[k][0]=='o'||arr[k][0]=='u'||arr[k][0]=='A'||arr[k][0]=='E'||arr[k][0]=='I'||arr[k][0]=='O'||arr[k][0]=='U'))
                    tp=2;

                if(tp==0&&l[k]-1>=2)
                {
                    if(l[k]-1<=3)
                        tp=1;
                    else if(l[k]-1<=5)
                        tp=2;
                    else
                        tp=3;
                }
                //printf("%d\n",tp);
                val[k]+=tp;
                if(arr[k][l[k]-1]=='.')
                {
                    flag=1;
                    break;
                }
            }
        }
        A=B=C=D=E=0;
        for(i=0; i<4&&i<k; i++)
        {
            if(val[i]<=12&&val[i]>=8)
                A+=10;
        }
        for(i=2; i<4&&i<k; i++)
        {
            if(l[i]-3>=0&&l[i-2]-3>=0&&arr[i][l[i]-3]==arr[i-2][l[i-2]-3]&&arr[i][l[i]-2]==arr[i-2][l[i-2]-2])
                B+=20;
            if(val[i]==val[i-2])
                C+=10;
        }
        D=(k-4)*10;
        if(D<0)
            D=0;
        printf("Case #%d: %d %d %d %d %d\n",p,A,B,C,D,A+B+C-D);
    }
    return 0;
}

