/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll i,temp,a,c,b,res,l,ex,len;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %I64d %I64d %I64d %I64d",&a,&b,&c,&l);
    res=0LL;

    for(i=a;i<=(a+l);i++)
    {
        ex=0LL;
        if((b+c)<=i)
        {
            ex=i-(b+c)+1LL;
        }
        temp=0LL;
        len=l-(i-a);
        if(ex>=len)
        {
            temp=(ex+1LL);
        }
        len=l-(i-a)-ex;
        if(len>0)
        {
            if(temp==0LL&&ex==0LL)
                temp=1LL;
            temp*=(len+1LL)*(len+1LL);
        }
        res+=temp;
        printf("%lld\n",temp);
    }

    printf("%I64d\n",res);

    return 0;
}


