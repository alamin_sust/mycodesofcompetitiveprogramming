#include<stdio.h>
#include<iostream>
using namespace std;

int arr[10010];

int partition_(int f,int t)
{
    int j,i=f;
    for(j=f;j<t;j++)
    {
        if(arr[j]<arr[t])
        {
            swap(arr[i],arr[j]);
            i++;
        }
    }
    swap(arr[i],arr[j]);
    return i;
}

void quick_sort(int f,int t)
{
    if(f<t)
    {
        int p=partition_(f,t);
        quick_sort(f,p-1);
        quick_sort(p+1,t);
    }
    return;
}

main()
{
    int n,i;
    cin>>n;
    for(i=1;i<=n;i++)
    {
        cin>>arr[i];
    }
    quick_sort(1,n);
    for(i=1;i<=n;i++)
    {
        cout<<arr[i]<<" ";
    }
    return 0;
}
