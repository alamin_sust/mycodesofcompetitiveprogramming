/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
int rr[]= {0,0,-1,1};
int cc[]= {-1,1,0,0};
using namespace std;


struct node
{
    int x,y,val;
};
priority_queue<node>pq;

bool operator<(node a,node b)
{
    return a.val<b.val;
}


node nd,td;
int col[1010][1010],arr[1010][1010],t,p,i,j,n,m;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {
        scanf(" %d %d",&n,&m);

        while(!pq.empty())pq.pop();

        for(i=1; i<=n; i++)
            for(j=1; j<=m; j++)
                col[i][j]=0;

        for(i=1; i<=n; i++)
        {
            for(j=1; j<=m; j++)
            {
                scanf(" %d",&arr[i][j]);
                if(arr[i][j]>0)
                {
                    node tp;
                    tp.x=i;
                    tp.y=j;
                    tp.val=arr[i][j];
                    pq.push(tp);
                }
            }
        }

        while(!pq.empty())
        {
            node nd = pq.top();
            pq.pop();
            col[nd.x][nd.y]=1;

            //printf("%d %d %d\n",now.x,now.y,now.val);

            for(i=0; i<4; i++)
            {
                node td;
                if((nd.x+rr[i])>=1&&(nd.x+rr[i])<=n&&(nd.y+cc[i])>=1&&(nd.y+cc[i])<=m && col[nd.x+rr[i]][nd.y+cc[i]]==0 && arr[nd.x+rr[i]][nd.y+cc[i]]!=-1)
                {
                    col[nd.x+rr[i]][nd.y+cc[i]]=1;
                    td.x=nd.x+rr[i];
                    td.y=nd.y+cc[i];
                    td.val=nd.val-1;
                    if(td.val>0)
                        pq.push(td);
                }
            }
        }


        for(i=1; i<=n; i++)
        {
            for(j=1; j<=m; j++)
            {
                if(col[i][j]) printf("Y");
                else
                {
                    if(arr[i][j]==-1)
                    {
                        printf("B");
                    }
                    else printf("N");

                }

            }
            printf("\n");
        }

    }


    return 0;
}

