/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int m1,m2,m3,m4,m5,w1,w2,w3,w4,w5,res,sh,uh;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d %d %d %d %d",&m1,&m2,&m3,&m4,&m5);
    scanf(" %d %d %d %d %d",&w1,&w2,&w3,&w4,&w5);
    scanf(" %d %d",&sh,&uh);
    res=max(500*3/10,500-(m1*500)/250-50*w1);
    res+=max(1000*3/10,1000-(m2*1000)/250-50*w2);
    res+=max(1500*3/10,1500-(m3*1500)/250-50*w3);
    res+=max(2000*3/10,2000-(m4*2000)/250-50*w4);
    res+=max(2500*3/10,2500-(m5*2500)/250-50*w5);
    res+=100*sh;
    res-=50*uh;
    printf("%d\n",res);
    return 0;
}

