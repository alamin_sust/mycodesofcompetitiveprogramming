#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node
{
    int x,val;
};

node det;
vector<node>adj[510];
int d[510][3];

struct node2
{
    int parity,x,val;
};

bool operator<(node2 a,node2 b)
{
    return a.val>b.val;
}

priority_queue<node2>pq;

int dijkstra(int source,int dest)
{
    node2 det2,det3;
    for(int i=0;i<=505;i++)
        d[i][0]=d[i][1]=inf;
    while(!pq.empty())
        pq.pop();
    det2.parity=0;
    det2.x=source;
    det2.val=0;
    pq.push(det2);
    d[source][0]=0;
    while(!pq.empty())
    {

        det2=pq.top();
        pq.pop();
        for(int i=0;i<adj[det2.x].size();i++)
        {
            det3.parity=!det2.parity;
            det3.x=adj[det2.x][i].x;
            det3.val=d[det2.x][det2.parity]+adj[det2.x][i].val;
            if(d[det3.x][det3.parity]>det3.val)
                d[det3.x][det3.parity]=det3.val,pq.push(det3);
        }
    }
    return d[dest][0];
}

main()
{
    int n,m,p=1,i,a,b,c,res;
    while(cin>>n>>m)
    {
        for(i=0;i<=505;i++)
            adj[i].clear();
        for(i=1;i<=m;i++)
        {
            scanf(" %d %d %d",&a,&b,&c);
            det.x=++a;
            det.val=c;
            adj[++b].push_back(det);
            det.x=b;
            adj[a].push_back(det);
        }
        res=dijkstra(1,n);
        printf("Set #%d\n",p++);
        if(res==inf)
            printf("?\n");
        else
            printf("%d\n",res);
    }
    return 0;
}

