/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int n,m,adj[500][500],i,j,k;
char ch;

main()
{
    cin>>n>>m;
    getchar();
    for(i=1;i<=n;i++)
    {
        scanf("%c",&ch);
        if(ch=='>')
        {
            for(j=1;j<m;j++)
                adj[j+(m*(i-1))][j+1+(m*(i-1))]=1;
        }
        else
        {
            for(j=1;j<m;j++)
                adj[j+1+(m*(i-1))][j+(m*(i-1))]=1;
        }
    }
    getchar();

    for(i=1;i<=m;i++)
    {
        scanf("%c",&ch);
        if(ch=='v')
        {
            for(j=i;(j+m)<=m*n;j+=m)
                adj[j][j+m]=1;
        }
        else
        {
            for(j=i;(j+m)<=m*n;j+=m)
                adj[j+m][j]=1;
        }
    }
    getchar();
    n*=m;
    for(k=1;k<=n;k++)
    {
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
            {
                adj[i][j]|=adj[i][k]&adj[k][j];
            }
        }
    }
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
        {
            if(i==j)
                continue;
            if(adj[i][j]==0)
            {
                printf("NO\n");
                return 0;
            }
        }
    }
    printf("YES\n");
    return 0;
}

