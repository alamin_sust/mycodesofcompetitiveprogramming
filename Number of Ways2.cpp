/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll dp[500010][3],arr[500010],n;

ll rec(ll pos,ll state)
{
    if(pos==n)
    {
        if(state==2)
            return 1;
        return 0;
    }
    ll &ret=dp[pos][state];
    if(ret!=-1)
        return ret;
    ret=0;
    if(state==0)
    {
        if(arr[pos]*3LL==arr[n])
            ret+=rec(pos+1,1);
            ret+=rec(pos+1,0);
    }
    else if(state==1)
    {
        if(((arr[pos]/2)*3)==arr[n]&&(arr[pos]%2)==0)
            ret+=rec(pos+1,2);
            ret+=rec(pos+1,1);
    }
    else ret+=rec(n,2);
    return ret;


}

main()
{
    int i;
    cin>>n;
    for(i=1; i<=n; i++)
        scanf(" %I64d",&arr[i]),arr[i]+=arr[i-1];
    memset(dp,-1,sizeof(dp));
    cout<<rec(1,0)<<endl;
    return 0;
}

