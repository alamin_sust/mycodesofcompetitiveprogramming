/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll a,b,res,q,from,to,ch[1000010],ce[1000010],cf[1000010],hc[1000010],he[1000010],hf[1000010],ec[1000010],eh[1000010],ef[1000010],fc[1000010],fh[1000010],fe[1000010],ff,cc,hh,ee,f[1000010],c[1000010],h[1000010],e[1000010],i,l;
char arr[1000010],st,en;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    scanf(" %s",arr);
    l=strlen(arr);
    c[l]=h[l]=e[l]=f[l]=0LL;
    for(i=l-1LL;i>=0LL;i--)
    {
        if(arr[i]=='c')
            c[i]++;
        else if(arr[i]=='h')
            h[i]++;
        else if(arr[i]=='e')
            e[i]++;
        else if(arr[i]=='f')
            f[i]++;
        c[i]+=c[i+1];
        h[i]+=h[i+1];
        e[i]+=e[i+1];
        f[i]+=f[i+1];
    }

    for(i=l-1LL;i>=0LL;i--)
    {
        if(arr[i]=='c')
            cc++;
        else if(arr[i]=='h')
            hh++;
        else if(arr[i]=='e')
            ee++;
        else if(arr[i]=='f')
            ff++;
        if(arr[i]=='c')
        {
            ch[i]=hh;
            ce[i]=ee;
            cf[i]=ff;
        }
        else if(arr[i]=='h')
        {
            hc[i]=cc;
            he[i]=ee;
            hf[i]=ff;
        }
        else if(arr[i]=='e')
        {
            ec[i]=cc;
            eh[i]=hh;
            ef[i]=ff;
        }
        else if(arr[i]=='f')
        {
            fc[i]=cc;
            fh[i]=hh;
            fe[i]=ee;
        }

        ch[i]+=ch[i+1];
        ce[i]+=ce[i+1];
        cf[i]+=cf[i+1];

        hc[i]+=hc[i+1];
            he[i]+=he[i+1];
            hf[i]+=hf[i+1];

        ec[i]+=ec[i+1];
            eh[i]+=eh[i+1];
            ef[i]+=ef[i+1];

        fc[i]+=fc[i+1];
            fh[i]+=fh[i+1];
            fe[i]+=fe[i+1];
    }

    cin>>q;

    for(i=0LL;i<q;i++)
    {
        getchar();
        scanf(" %c %c %lld %lld",&st,&en,&from,&to);
        from--;
        to--;
        //printf("%c %c...\n",st,en);
        if(st=='c'&&en=='h')
        {
            a=c[from]-c[to+1];
            b=h[to+1];
            res=ch[from]-ch[to+1]-a*b;
        }
        else if(st=='c'&&en=='e')
        {
            a=c[from]-c[to+1];
            b=e[to+1];
            res=ce[from]-ce[to+1]-a*b;
        }
        else if(st=='c'&&en=='f')
        {
            a=c[from]-c[to+1];
            b=f[to+1];
            res=cf[from]-cf[to+1]-a*b;
        }
        else if(st=='h'&&en=='c')
        {
            a=h[from]-h[to+1];
            b=c[to+1];
            res=hc[from]-hc[to+1]-a*b;
        }
        else if(st=='h'&&en=='e')
        {
            a=h[from]-h[to+1];
            b=e[to+1];
            res=he[from]-he[to+1]-a*b;
        }
        else if(st=='h'&&en=='f')
        {
            a=h[from]-h[to+1];
            b=f[to+1];
            res=hf[from]-hf[to+1]-a*b;
        }
        else if(st=='e'&&en=='c')
        {
            a=e[from]-e[to+1];
            b=c[to+1];
            res=ec[from]-ec[to+1]-a*b;
        }
        else if(st=='e'&&en=='h')
        {
            a=e[from]-e[to+1];
            b=h[to+1];
            res=eh[from]-eh[to+1]-a*b;
        }
        else if(st=='e'&&en=='f')
        {
            a=e[from]-e[to+1];
            b=f[to+1];
            res=ef[from]-ef[to+1]-a*b;
        }
        else if(st=='f'&&en=='c')
        {
            a=f[from]-f[to+1];
            b=c[to+1];
            res=fc[from]-fc[to+1]-a*b;
        }
        else if(st=='f'&&en=='h')
        {
            a=f[from]-f[to+1];
            b=h[to+1];
            res=fh[from]-fh[to+1]-a*b;
        }
        else if(st=='f'&&en=='e')
        {
            a=f[from]-f[to+1];
            b=e[to+1];
            res=fe[from]-fe[to+1]-a*b;
        }

        printf("%lld\n",res);
    }

    return 0;
}




