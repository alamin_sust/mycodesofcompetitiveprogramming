#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class DevuAndPlantingTrees
{
public:
    int maximumTreesDevuCanGrow(vector <string> garden)
    {
        char arr[10][55];
        for(int i=0; i<9; i++)
        {
            for(int j=0; j<54; j++)
            {
                arr[i][j]='.';
            }
        }
        int n=garden[0].size(),ret=0;
        for(int i=0; i<2; i++)
        {
            for(int j=0; j<garden[i].size(); j++)
            {
                if(garden[i][j]=='*')
                    ret++;
                arr[i+1][j+1]=garden[i][j];
            }
        }
        for(int i=1; i<=2; i++)
        {
            for(int j=1; j<=n; j++)
            {
                if(arr[i][j]!='*'&&arr[i-1][j-1]!='*'&&arr[i-1][j+1]!='*'&&arr[i+1][j-1]!='*'&&arr[i+1][j+1]!='*'&&arr[i][j-1]!='*'&&arr[i-1][j]!='*'&&arr[i+1][j]!='*'&&arr[i][j+1]!='*')
                {
                    arr[i][j]='*';
                    ret++;
                }
            }
        }
        return ret;
    }
};


template<typename T> void print( T a )
{
    cerr << a;
}

void print( long long a )
{
    cerr << a << "L";
}

void print( string a )
{
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a )
{
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ )
    {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need )
{
    if ( have == need )
    {
        cerr << "Case " << n << " passed." << endl;
    }
    else
    {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need )
{
    if( have.size() != need.size() )
    {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ )
    {
        if( have[i] != need[i] )
        {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need )
{
    if ( have == need )
    {
        cerr << "Case " << n << " passed." << endl;
    }
    else
    {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

    DevuAndPlantingTrees objectDevuAndPlantingTrees;

    //test case0
    vector <string> param00;
    param00.push_back("..");
    param00.push_back("..");
    int ret0 = objectDevuAndPlantingTrees.maximumTreesDevuCanGrow(param00);
    int need0 = 1;
    assert_eq(0,ret0,need0);

    //test case1
    vector <string> param10;
    param10.push_back("..");
    param10.push_back(".*");
    int ret1 = objectDevuAndPlantingTrees.maximumTreesDevuCanGrow(param10);
    int need1 = 1;
    assert_eq(1,ret1,need1);

    //test case2
    vector <string> param20;
    param20.push_back("...");
    param20.push_back("..*");
    int ret2 = objectDevuAndPlantingTrees.maximumTreesDevuCanGrow(param20);
    int need2 = 2;
    assert_eq(2,ret2,need2);

    //test case3
    vector <string> param30;
    param30.push_back(".....*..........");
    param30.push_back(".*.......*.*..*.");
    int ret3 = objectDevuAndPlantingTrees.maximumTreesDevuCanGrow(param30);
    int need3 = 7;
    assert_eq(3,ret3,need3);

    //test case4
    vector <string> param40;
    param40.push_back("....*.*.*...........*........");
    param40.push_back("*..........*..*.*.*....*...*.");
    int ret4 = objectDevuAndPlantingTrees.maximumTreesDevuCanGrow(param40);
    int need4 = 13;
    assert_eq(4,ret4,need4);

    //test case5
    vector <string> param50;
    param50.push_back(".....*..*..........*............................*");
    param50.push_back("*..*.............*...*.*.*.*..*.....*.*...*...*..");
    int ret5 = objectDevuAndPlantingTrees.maximumTreesDevuCanGrow(param50);
    int need5 = 23;
    assert_eq(5,ret5,need5);

}
