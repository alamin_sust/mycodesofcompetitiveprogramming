#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class ChessFloor
{
public:
    int minimumChanges(vector <string> floor)
    {
        int res=9999999;
        for(int p='a'; p<='z'; p++)
        {
            for(int q='a'; q<='z'; q++)
            {
                if(p==q)
                    continue;
                int tp=0,flag=0;
                for(int i=0; i<floor.size(); i++)
                {
                    for(int j=0;j<floor.size();j++)
                    {
                        if(flag==0)
                        {
                            if(floor[i][j]!=p)
                                tp++;
                        }
                        else
                        {
                            if(floor[i][j]!=q)
                                tp++;
                        }
                        flag^=1;
                    }
                    if((floor.size()%2)==0)
                        flag^=1;
                }
                res=min(res,tp);
            }
        }
        return res;
    }
};


template<typename T> void print( T a )
{
    cerr << a;
}

void print( long long a )
{
    cerr << a << "L";
}

void print( string a )
{
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a )
{
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ )
    {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need )
{
    if ( have == need )
    {
        cerr << "Case " << n << " passed." << endl;
    }
    else
    {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need )
{
    if( have.size() != need.size() )
    {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ )
    {
        if( have[i] != need[i] )
        {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need )
{
    if ( have == need )
    {
        cerr << "Case " << n << " passed." << endl;
    }
    else
    {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

    ChessFloor objectChessFloor;

    //test case0
    vector <string> param00;
    param00.push_back("aba");
    param00.push_back("bbb");
    param00.push_back("aba");
    int ret0 = objectChessFloor.minimumChanges(param00);
    int need0 = 1;
    assert_eq(0,ret0,need0);

    //test case1
    vector <string> param10;
    param10.push_back("wbwbwbwb");
    param10.push_back("bwbwbwbw");
    param10.push_back("wbwbwbwb");
    param10.push_back("bwbwbwbw");
    param10.push_back("wbwbwbwb");
    param10.push_back("bwbwbwbw");
    param10.push_back("wbwbwbwb");
    param10.push_back("bwbwbwbw");
    int ret1 = objectChessFloor.minimumChanges(param10);
    int need1 = 0;
    assert_eq(1,ret1,need1);

    //test case2
    vector <string> param20;
    param20.push_back("zz");
    param20.push_back("zz");
    int ret2 = objectChessFloor.minimumChanges(param20);
    int need2 = 2;
    assert_eq(2,ret2,need2);

    //test case3
    vector <string> param30;
    param30.push_back("helloand");
    param30.push_back("welcomet");
    param30.push_back("osingler");
    param30.push_back("oundmatc");
    param30.push_back("hsixhund");
    param30.push_back("redandsi");
    param30.push_back("xtythree");
    param30.push_back("goodluck");
    int ret3 = objectChessFloor.minimumChanges(param30);
    int need3 = 56;
    assert_eq(3,ret3,need3);

    //test case4
    vector <string> param40;
    param40.push_back("jecjxsengslsmeijrmcx");
    param40.push_back("tcfyhumjcvgkafhhffed");
    param40.push_back("icmgxrlalmhnwwdhqerc");
    param40.push_back("xzrhzbgjgabanfxgabed");
    param40.push_back("fpcooilmwqixfagfojjq");
    param40.push_back("xzrzztidmchxrvrsszii");
    param40.push_back("swnwnrchxujxsknuqdkg");
    param40.push_back("rnvzvcxrukeidojlakcy");
    param40.push_back("kbagitjdrxawtnykrivw");
    param40.push_back("towgkjctgelhpomvywyb");
    param40.push_back("ucgqrhqntqvncargnhhv");
    param40.push_back("mhvwsgvfqgfxktzobetn");
    param40.push_back("fabxcmzbbyblxxmjcaib");
    param40.push_back("wpiwnrdqdixharhjeqwt");
    param40.push_back("xfgulejzvfgvkkuyngdn");
    param40.push_back("kedsalkounuaudmyqggb");
    param40.push_back("gvleogefcsxfkyiraabn");
    param40.push_back("tssjsmhzozbcsqqbebqw");
    param40.push_back("ksbfjoirwlmnoyyqpbvm");
    param40.push_back("phzsdodppzfjjmzocnge");
    int ret4 = objectChessFloor.minimumChanges(param40);
    int need4 = 376;
    assert_eq(4,ret4,need4);

}
