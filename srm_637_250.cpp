#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class GreaterGameDiv2
{
public:
	int calc(vector <int> snuke, vector <int> sothe)
	{
	    int ret=0;
	    for(int i=0;i<snuke.size()&&i<sothe.size();i++)
        {
            if(snuke[i]>sothe[i])
                ret++;
        }
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	GreaterGameDiv2 objectGreaterGameDiv2;

	//test case0
	vector <int> param00;
	param00.push_back(1);
	param00.push_back(3);
	vector <int> param01;
	param01.push_back(4);
	param01.push_back(2);
	int ret0 = objectGreaterGameDiv2.calc(param00,param01);
	int need0 = 1;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(1);
	param10.push_back(3);
	param10.push_back(5);
	param10.push_back(7);
	param10.push_back(9);
	vector <int> param11;
	param11.push_back(2);
	param11.push_back(4);
	param11.push_back(6);
	param11.push_back(8);
	param11.push_back(10);
	int ret1 = objectGreaterGameDiv2.calc(param10,param11);
	int need1 = 0;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(2);
	vector <int> param21;
	param21.push_back(1);
	int ret2 = objectGreaterGameDiv2.calc(param20,param21);
	int need2 = 1;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(3);
	param30.push_back(5);
	param30.push_back(9);
	param30.push_back(16);
	param30.push_back(14);
	param30.push_back(20);
	param30.push_back(15);
	param30.push_back(17);
	param30.push_back(13);
	param30.push_back(2);
	vector <int> param31;
	param31.push_back(6);
	param31.push_back(18);
	param31.push_back(1);
	param31.push_back(8);
	param31.push_back(7);
	param31.push_back(10);
	param31.push_back(11);
	param31.push_back(19);
	param31.push_back(12);
	param31.push_back(4);
	int ret3 = objectGreaterGameDiv2.calc(param30,param31);
	int need3 = 6;
	assert_eq(3,ret3,need3);

}
