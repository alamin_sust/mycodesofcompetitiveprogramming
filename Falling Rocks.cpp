/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

queue<int>q,q2;
int m,n,i,fg[1010];
char arr[1010][1010];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d %d",&m,&n);

    for(i=0; i<n; i++)
    {
        scanf(" %s",&arr[i]);
    }

    for(i=0; i<m; i++)
    {
        if(arr[0][i]=='Y')
        {
            q.push(i);
            break;
        }
    }
    for(i=1; i<n; i++)
    {
        while(!q2.empty())
            q2.pop();
        q2=q;
        while(!q.empty())
            q.pop();
        while(!q2.empty())
        {
            int x=q2.front();
            q2.pop();
            if(arr[i][x]!='R'&&arr[i-1][x]!='R'&&fg[x]!=i)
            {
                fg[x]=i;
                q.push(x);
              //  printf("%d..",x);
            }
            if((x-1)>=0&&arr[i][x-1]!='R'&&arr[i-1][x-1]!='R'&&fg[x-1]!=i)
            {
                fg[x-1]=i;
                q.push(x-1);
              //  printf("%d..",x-1);
            }
            if((x+1)<m&&arr[i][x+1]!='R'&&arr[i-1][x+1]!='R'&&fg[x+1]!=i)
            {
                fg[x+1]=i;
                q.push(x+1);
               // printf("%d..",x+1);
            }
        }
       // printf("\n");

    }
    if(q.empty())
    {
        printf("NO\n");
    }
    else
        printf("YES\n");
    return 0;
}


