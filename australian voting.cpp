#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define MAX_KAND 20

char imena[MAX_KAND][100];
int glasovi[1000][MAX_KAND];
int v_igri[MAX_KAND];
int glasov[MAX_KAND];

void prestej(int num_v, int st_kand) {
  int i, j;
  for (i=0; i<st_kand; i++) glasov[i] = 0;
  for (i=0; i<num_v; i++) {
    j = 0;
    while (v_igri[glasovi[i][j]] == 0) j++;
    glasov[glasovi[i][j]]++;
  }
  return;
}

int main(void) {
  int kdo, cases, st_kand, ostali, i, n, count, max, maxn, min, first = 1;
  char niz[200], *p;
  scanf("%d\n\n", &cases);
  while (cases > 0) {
    scanf("%d\n", &st_kand);
    for (i=0; i<st_kand; i++) {
      gets(imena[i]);
      v_igri[i] = 1;
    }
    count = 0;
    do {
      gets(niz);
      if (strlen(niz) < 1) break;
      p = niz;
      for (i=0; i<st_kand; i++) {
         sscanf(p, "%d", &n);
        while (isdigit(p[0])) p++;
        while (isspace(p[0])) p++;
        glasovi[count][i] = n-1;
      }
      count++;
      if (feof(stdin)) break;
    } while (1);
    ostali = st_kand;
    do {
      prestej(count, st_kand);
      max = 0;
      kdo = 0;
      maxn = 0;
      min = count;
      for (i=0; i<st_kand; i++)
      if (v_igri[i]) {
        if (glasov[i] > max) {
          max = glasov[i];
          kdo = i;
          maxn = 1;
        } else if (glasov[i] == max) maxn++;
        if (glasov[i] < min) min = glasov[i];
      }
      if (maxn == 1 && (1.0 * max / count) > 0.5) {
         if (!first) putchar('\n');
         printf("%s\n", imena[kdo]);
         first = 0;
         break;
      } else if (max == min) {
         if (!first) putchar('\n');
        for (i=0; i<st_kand; i++)
        if (v_igri[i]) printf("%s\n", imena[i]);
        first = 0;
        break;
      } else {
        for (i=0; i<st_kand; i++)
          if (v_igri[i]) {
            if (glasov[i] == min) {
              v_igri[i] = 0;
              ostali--;
            }
          }
      }
    } while (1);
    cases--;
  }
  return(0);
}
