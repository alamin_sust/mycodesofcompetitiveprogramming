/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


int n,m,arr[1200010],k,l,i,lft,rht,indx;
char str[1200010],bam[1200010],dan[1200010],in[10];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d %d",&n,&m);

    scanf(" %s",str);

    l=strlen(str);

    k=1;
    while(k<l)
    {
        k<<=1;
    }
    l=k;

    for(i=0,k=l; i<l; i++,k++)
    {
        if(str[i]=='.')
        {
            arr[k]=0;

            bam[k]=dan[k]='.';
        }
        else
        {
            arr[k]=0;
            bam[k]=dan[k]='a';
        }
    }

    for(k=l-1; k>=1; k--)
    {
        lft=k*2;
        rht=k*2+1;
        if(dan[lft]=='.'&&bam[rht]=='.')
        {
            arr[k]=arr[lft]+arr[rht]+1;
            bam[k]=bam[lft];
            dan[k]=dan[rht];
        }
        else
        {
            arr[k]=arr[lft]+arr[rht];
            bam[k]=bam[lft];
            dan[k]=dan[rht];
        }
    }


    //for(i=1;i<l+l;i++)
    //{
    //     printf("%d ",arr[i]);
    //}

    for(i=0; i<m; i++)
    {
        scanf(" %d %s",&indx,in);
        str[indx-1]=in[0];

        k=l+indx-1;

        if(str[indx-1]=='.')
        {
            arr[k]=0;
            bam[k]=dan[k]='.';
        }
        else
        {
            arr[k]=0;
            bam[k]=dan[k]='a';
        }
        k>>=1;
        while(k)
        {

            lft=k*2;
            rht=k*2+1;
            if(dan[lft]=='.'&&bam[rht]=='.')
            {
                arr[k]=arr[lft]+arr[rht]+1;
                bam[k]=bam[lft];
                dan[k]=dan[rht];
            }
            else
            {
                arr[k]=arr[lft]+arr[rht];
                bam[k]=bam[lft];
                dan[k]=dan[rht];
            }

            k>>=1;
        }

        //printf("%s\n",str);

        printf("%d\n",arr[1]);



    }



    return 0;
}


