#include<stdio.h>
int fres,hash,chess[11][11],input[11][11],colset[11],l_diag[20],r_diag[20],res[11],row,col;
void backTrack(int r)
{
   int i,c,j,m,n,tmax;
   if(r==9)
   {
      for(i=1;i<=8;i++)
      {
         if(i==col && res[i]==row)
         {
            hash++;
            printf("%2ld",hash);
            priintf("     ");
            for(j=1;j<=8;j++)
            {
               printf(" %d",res[j]);
            }
            printf("\n");
            break;
         }
      }
      if(fres<tmax)
      {
         fres=tmax;
      }
      return;
   }
   for(c=1;c<=8;c++)
   {
      if(chess[r][c]==0 && colset[c]==0 && l_diag[r+c]==0 && r_diag[r-c+8]==0)
      {
         chess[r][c]=1;
         colset[c]=1;
         l_diag[r+c]=1;
         r_diag[r-c+8]=1;
         res[r]=c;
         backTrack(r+1);
         chess[r][c]=0;
         colset[c]=0;
         l_diag[r+c]=0;
         r_diag[r-c+8]=0;
      }
   }
   return;
}
int main()
{
   int a,x,i,j;
   scanf(" %d",&a);
      fres=0;
      for(x=1;x<=a;x++)
      {
         scanf(" %d %d",&row,&col);
         for(i=0;i<10;i++)
         {
            for(j=0;j<10;j++)
            {
               chess[i][j]=0;
            }
            colset[i]=0;
         }
         for(i=0;i<=20;i++)
         {
            l_diag[i]=0;
            r_diag[i]=0;
         }
         printf("SOLN       COLUMN\n");
         printf(" #      1 2 3 4 5 6 7 8\n\n");
         hash=0;
         backTrack(1);
         if(x!=a)
         printf("\n");
      }
   return 0;
}
