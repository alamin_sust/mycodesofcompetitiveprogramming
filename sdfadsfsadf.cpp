#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define eps 0.00000001


using namespace std;

class TrianglesContainOriginEasy
{
public:
	int count(vector <int> x, vector <int> y)
	{
	    int i,j,k,ret=0;
	    double a,b,c,s,area,area2;
	    for(i=0;i<x.size();i++)
        {
            for(j=i+1;j<x.size();j++)
            {
                for(k=j+1;k<x.size();k++)
                {
                     a=dist((double)x[i],(double)y[i],(double)x[j],(double)y[j]);
                     b=dist((double)x[j],(double)y[j],(double)x[k],(double)y[k]);
                     c=dist((double)x[k],(double)y[k],(double)x[i],(double)y[i]);
                     s=(a+b+c)/2.0;
                     area=sqrt( s*(s-a)*(s-b)*(s-c) );

                     a=dist((double)x[i],(double)y[i],(double)x[j],(double)y[j]);
                     b=dist((double)x[j],(double)y[j],0.0,0.0);
                     c=dist(0.0,0.0,(double)x[i],(double)y[i]);
                     s=(a+b+c)/2.0;
                     area2=sqrt( s*(s-a)*(s-b)*(s-c) );

                     a=dist((double)x[i],(double)y[i],(double)x[k],(double)y[k]);
                     b=dist((double)x[k],(double)y[k],0.0,0.0);
                     c=dist(0.0,0.0,(double)x[i],(double)y[i]);
                     s=(a+b+c)/2.0;
                     area2+=sqrt( s*(s-a)*(s-b)*(s-c) );

                     a=dist((double)x[k],(double)y[k],(double)x[j],(double)y[j]);
                     b=dist((double)x[j],(double)y[j],0.0,0.0);
                     c=dist(0.0,0.0,(double)x[k],(double)y[k]);
                     s=(a+b+c)/2.0;
                     area2+=sqrt( s*(s-a)*(s-b)*(s-c) );

                     if((area2+eps)>area&&(area2-eps)<area)
                        ret++;

                }
            }
        }
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	TrianglesContainOriginEasy objectTrianglesContainOriginEasy;

	//test case0
	vector <int> param00;
	param00.push_back(-1);
	param00.push_back(-1);
	param00.push_back(1);
	vector <int> param01;
	param01.push_back(1);
	param01.push_back(-1);
	param01.push_back(0);
	int ret0 = objectTrianglesContainOriginEasy.count(param00,param01);
	int need0 = 1;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(-1);
	param10.push_back(-1);
	param10.push_back(1);
	param10.push_back(2);
	vector <int> param11;
	param11.push_back(1);
	param11.push_back(-1);
	param11.push_back(2);
	param11.push_back(-1);
	int ret1 = objectTrianglesContainOriginEasy.count(param10,param11);
	int need1 = 2;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(-1);
	param20.push_back(-2);
	param20.push_back(3);
	param20.push_back(3);
	param20.push_back(2);
	param20.push_back(1);
	vector <int> param21;
	param21.push_back(-2);
	param21.push_back(-1);
	param21.push_back(1);
	param21.push_back(2);
	param21.push_back(3);
	param21.push_back(3);
	int ret2 = objectTrianglesContainOriginEasy.count(param20,param21);
	int need2 = 8;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(1);
	param30.push_back(5);
	param30.push_back(10);
	param30.push_back(5);
	param30.push_back(-5);
	param30.push_back(7);
	param30.push_back(-9);
	param30.push_back(-6);
	param30.push_back(-3);
	param30.push_back(0);
	param30.push_back(8);
	param30.push_back(8);
	param30.push_back(1);
	param30.push_back(-4);
	param30.push_back(7);
	param30.push_back(-3);
	param30.push_back(10);
	param30.push_back(9);
	param30.push_back(-6);
	vector <int> param31;
	param31.push_back(5);
	param31.push_back(-6);
	param31.push_back(-3);
	param31.push_back(4);
	param31.push_back(-2);
	param31.push_back(-8);
	param31.push_back(-7);
	param31.push_back(2);
	param31.push_back(7);
	param31.push_back(4);
	param31.push_back(2);
	param31.push_back(0);
	param31.push_back(-4);
	param31.push_back(-8);
	param31.push_back(7);
	param31.push_back(5);
	param31.push_back(-5);
	param31.push_back(-2);
	param31.push_back(-9);
	int ret3 = objectTrianglesContainOriginEasy.count(param30,param31);
	int need3 = 256;
	assert_eq(3,ret3,need3);

}
