#include<stdio.h>
#include<math.h>
#include<vector>
#include<iostream>
#include<queue>
#include<stack>
#define inf 9999999
using namespace std;

vector <int> adj_list[110];
vector< int > :: iterator x;
int source,n,w[110][110];

struct adjacent
{
    int name,dist,par;
    bool operator<(const adjacent& other) const
    {
        return dist > other.dist;
    }
};

adjacent vertice[110];
priority_queue <adjacent> q;

void initialize_single_source(void)
{
    cin>>source;
    vertice[source].dist=0;
}

void relax(int u,int v)
{
    if(vertice[v].dist>(vertice[u].dist+w[u][v]))
    {
        vertice[v].dist=vertice[u].dist+w[u][v];
        vertice[v].par=u;
    }
    return;
}

void dijkstra(void)
{
    adjacent u;
    initialize_single_source();
    for(int i=1; i<=n; i++)
    {
        q.push(vertice[i]);
    }
    while(q.empty()==0)
    {

        u=q.top();
        q.pop();
        for(x=adj_list[u.name].begin(); x<adj_list[u.name].end(); x++)
        {
            relax(u.name,*x);
        }
    }
    return;
}

main()
{
    int i,m,j,in,weight;
    cin>>n;
    for(i=1; i<=n; i++)
    {
        vertice[i].name=i;
        vertice[i].dist=inf;
        vertice[i].par=0;
        cin>>m;
        for(j=1; j<=m; j++)
        {
            cin>>in>>weight;
            w[i][in]=weight;
            adj_list[i].push_back(in);
        }
    }
    dijkstra();
    int dest;
    cin>>dest;
    stack<int> st;
    printf("%d\n",vertice[dest].dist);
    printf("%d",source);
    for(i=dest;vertice[i].par!=0;i=vertice[i].par)
        st.push(vertice[i].name);
    while(st.empty()==0)
        {printf("->%d",st.top());
        st.pop();}
    return 0;
}
/*test case:
5
2
2 3
4 5
2
3 6
4 2
1
5 2
3
2 1
3 4
5 6
2
1 3
3 7*/
