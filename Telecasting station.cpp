#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.0000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int i,n;
double pos[100010],val[100010],low,mid,high;

double calc(double ps)
{
    double res=0.0;
    for(i=1;i<=n;i++)
    {
        if(pos[i]>ps)
        res+=(pos[i]-ps)*val[i];
        else
        res+=(ps-pos[i])*val[i];
    }
    return res;
}

main()
{
    cin>>n;
    for(i=1;i<=n;i++)
    {
        scanf(" %lf %lf",&pos[i],&val[i]);
    }
    low=0.0;
    high=50001.0;
    mid=(low+high)/2.0;
    int p=200;
    while(p--)
    {
        //printf("%lf\n",calc(mid));
        if(calc(mid)>calc(mid+eps))
        {
            low=mid;
        }
        else
        {
            high=mid;
        }
        mid=(low+high)/2.0;
    }
    printf("%.5lf\n",mid);
    return 0;
}

