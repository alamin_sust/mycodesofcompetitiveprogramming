#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

int stat[28][28],adj[28][28][28][28],par[28][28][3];

bool bfs(int x1,int y1,int x2,int y2)
{
    queue<int>x;
    queue<int>y;
    x.push(x1);
    y.push(y1);
    stat[x1][y1]=1;
    par[x1][y1][0]=x1;
    par[x1][y1][1]=y1;
    while(!x.empty())
    {
      int p1=x.front();
      int p2=y.front();
      x.pop();
      y.pop();
      for(int i=0;i<26;i++)
      {
          for(int j=0;j<26;j++)
          {
              if(adj[p1][p2][i][j]==1&&stat[i][j]==0)
              {
                  stat[i][j]=1;
                  par[i][j][0]=p1;
                  par[i][j][1]=p2;
                  if(i==x2&&j==y2)
                    return true;
                  x.push(i);
                  y.push(j);
              }
          }
      }
    }
    return false;
}

void print(int x2,int y2)
{
    if(par[x2][y2][0]==x2&&par[x2][y2][1]==y2);
        //printf("%c%c %c%c\n",x1+'A',y1+'A',x2+'A',y2+'A');
    else
    {
        print(par[x2][y2][0],par[x2][y2][1]);
        printf("%c%c %c%c\n",par[x2][y2][0]+'A',par[x2][y2][1]+'A',x2+'A',y2+'A');
    }
    return;
}

main()
{
    int t,p=1,n,i;
    char in1[10],in2[10];
    while(cin>>n)
    {
        if(p++!=1)
            printf("\n");
        memset(adj,0,sizeof(adj));
        memset(par,0,sizeof(par));
        memset(stat,0,sizeof(stat));
        for(i=1;i<=n;i++)
        {
            scanf(" %s %s",in1,in2);
            adj[in1[0]-'A'][in1[1]-'A'][in2[0]-'A'][in2[1]-'A']=1;
            adj[in2[0]-'A'][in2[1]-'A'][in1[0]-'A'][in1[1]-'A']=1;
        }
        scanf(" %s %s",in1,in2);
        if(bfs(in1[0]-'A',in1[1]-'A',in2[0]-'A',in2[1]-'A'))
        {
            print(in2[0]-'A',in2[1]-'A');
        }
        else
        printf("No route\n");

    }
    return 0;
}

