#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include<string.h>

using namespace std;

//vector<int>vc;
//int vrt;
//int dp[55][55];

/*int rec(int pos,int low,int high,int koyta)
{
    if((high-low)>=vrt)
        return koyta+1;
    if(pos==vc.size())
        return 9999999;
    int &ret=dp[pos][koyta];
    if(ret!=-1&&koyta>=ret)
        return ret;
    if(ret==-1)
    ret=9999999;
    ret=min(ret,rec(pos+1,min(low,vc[pos+1]),max(high,vc[pos+1]),koyta+1));
    ret=min(ret,rec(pos+2,min(low,vc[pos+2]),max(high,vc[pos+2]),koyta+1));
    return ret;
}
*/
class ProblemsToSolve
{
public:
	int minNumber(vector <int> pleasantness, int variety)
	{
	/*    vrt=variety;
	    vc=pleasantness;
	    vc.push_back(9999999);
	    vc.push_back(9999999);
	    memset(dp,-1,sizeof(dp));
	    return rec(0,0,0,0);*/
	    int res=9999999;
	    for(int i=0;i<pleasantness.size()-1;i++)
        {
            for(int j=i+1;j<pleasantness.size();j++)
            {
                if(abs(pleasantness[j]-pleasantness[i])>=variety)
                {
                    int tp=1;
                    for(int k=0;k<j;k+=2)
                        tp++;
                    for(int k=j+1;k<pleasantness.size();k+=2)
                        tp++;
                    res=min(res,tp);
                }
            }
        }
        return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	ProblemsToSolve objectProblemsToSolve;

	//test case0
	vector <int> param00;
	param00.push_back(1);
	param00.push_back(2);
	param00.push_back(3);
	int param01 = 2;
	int ret0 = objectProblemsToSolve.minNumber(param00,param01);
	int need0 = 2;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(1);
	param10.push_back(2);
	param10.push_back(3);
	param10.push_back(4);
	param10.push_back(5);
	int param11 = 4;
	int ret1 = objectProblemsToSolve.minNumber(param10,param11);
	int need1 = 3;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(10);
	param20.push_back(1);
	param20.push_back(12);
	param20.push_back(101);
	int param21 = 100;
	int ret2 = objectProblemsToSolve.minNumber(param20,param21);
	int need2 = 3;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(10);
	param30.push_back(1);
	int param31 = 9;
	int ret3 = objectProblemsToSolve.minNumber(param30,param31);
	int need3 = 2;
	assert_eq(3,ret3,need3);

	//test case4
	vector <int> param40;
	param40.push_back(6);
	param40.push_back(2);
	param40.push_back(6);
	param40.push_back(2);
	param40.push_back(6);
	param40.push_back(3);
	param40.push_back(3);
	param40.push_back(3);
	param40.push_back(7);
	int param41 = 4;
	int ret4 = objectProblemsToSolve.minNumber(param40,param41);
	int need4 = 2;
	assert_eq(4,ret4,need4);

}
