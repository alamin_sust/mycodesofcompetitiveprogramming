/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};



int M,n,k,tp,cnt,arr2[1000010],flag[110];


struct node{
int ind,val;
};
node arr[1000010];

void init(void)
{
    arr[1].val=1;
    arr[1].ind=1;
    arr[2].val=2;
    arr[2].ind=2;
    cnt=2;
    if(k>2)
    {arr[++cnt].val=3;
    arr[cnt].ind=3;}
    arr2[1]=1;
    arr2[2]=2;
    arr2[3]=3;
    for(int i=4;i<=n;i++)
    {
        arr2[i]=(arr2[i-1]+arr2[i-2]+arr2[i-3])%M+1;
        if(arr2[i]<=k)
        {
            arr[++cnt].val=arr2[i];
            arr[cnt].ind=i;
        }
    }
    return;
}

int main()
{
    int i,p,j,t,res,now;
    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %d %d %d",&n,&M,&k);
        memset(flag,0,sizeof(flag));
        init();
        res=99999999;
        now=0;
       // for(i=1;i<=cnt;i++)
       // {
       //     printf("%d...%d\n",arr[i].ind,arr[i].val);
       // }
        for(j=1,i=0;i<cnt;)
        {
            while(now<k&&i<cnt)
            {
                i++;
                if(flag[arr[i].val]==0)
                    now++;
                flag[arr[i].val]++;
            }
            while(now>=k&&j<=i)
            {
                res=min(res,arr[i].ind-arr[j].ind+1);
                //printf("%d--\n",arr[i].ind-arr[j].ind+1);
                //if(flag[arr[i].val]>0)
                flag[arr[j].val]--;
                if(flag[arr[j].val]==0)
                    now--;
                j++;
            }
        }
        if(res<99999999)
        printf("Case %d: %d\n",p,res);
        else
        printf("Case %d: sequence nai\n",p);
    }
    return 0;
}

