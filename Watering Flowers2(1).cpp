/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) ( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll res,x[2010],y[2010],n,j,i;
vector<ll>v;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    scanf(" %I64d",&n);

    for(i=1;i<=n+2;i++)
    {
        scanf(" %I64d %I64d",&x[i],&y[i]);
        if(i!=2)
        {
            v.push_back(dist(x[i],y[i],x[1],y[1]));
        }
    }

    res=2000000000000000010LL;

    for(i=0;i<v.size();i++)
    {
        ll tp=0;
        for(j=3;j<=n+2;j++)
        {
            if(dist(x[j],y[j],x[1],y[1])>v[i])
            {
                tp=max(tp,dist(x[j],y[j],x[2],y[2]));
            }
        }
        res=min(res,tp+v[i]);
    }

    printf("%I64d\n",res);

    return 0;
}
