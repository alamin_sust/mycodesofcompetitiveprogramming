/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) ( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int nodes;

struct pnt
{
    int x;
    int y;
};

pnt arr[3010];

pnt tmp[3010],need;

int ori(pnt a, pnt b, pnt c)
{
    int val = (b.y - a.y) * (c.x - b.x) -
              (b.x - a.x) * (c.y - b.y);

    if (val == 0)
        return 0;
    return (val>0)?1:2;
}
int convexhull(int n)
{
    if (n < 3)
        return 0;
    int nxt[n+2];
    for (int i = 0; i < n; i++)
        nxt[i] = -1;
    int lft = 0;
    for (int i = 1; i < n; i++)
        if (arr[i].x < arr[lft].x)
            lft = i;
    int p = lft, q;
    do
    {
        q = (p+1)%n;
        for (int i = 0; i < n; i++)
            {
                if(ori(arr[p],arr[i],arr[q])==0)
                nodes;
                if(ori(arr[p],arr[i],arr[q])==2)
                q = i;
            }

        nxt[p] = q;
        p = q;
    }
    while (p != lft);

    int ret=0,hh=0,fs=-1;
    pnt bef;

    for (int i = 0; i < n; i++)
    {
        if (nxt[i] != -1)
            {
                nodes++;
                if(fs==-1)
                   fs=i;

                if(arr[i].x==need.x&&arr[i].y==need.y)
                    ret=1;
               // if(hh>0&&ori(arr[i],bef,need)==0&&(dist(bef.x,bef.y,need.x,need.y)+dist(arr[i].x,arr[i].y,need.x,need.y))==dist(bef.x,bef.y,arr[i].x,arr[i].y))
              //  {
               //      printf("....");
              //        ret=1;
              //   }
                 bef.x=arr[i].x;
                 bef.y=arr[i].y;
                 hh++;
             //   cout << "(" << arr[i].x << ", " << arr[i].y << ")\n";
            }
    }
   // if(hh>0&&ori(arr[fs],bef,need)==0&&(dist(bef.x,bef.y,need.x,need.y)+dist(arr[fs].x,arr[fs].y,need.x,need.y))==dist(bef.x,bef.y,arr[fs].x,arr[fs].y))
    //             {
    //                  ret=1;
     //            }
    return ret;
}

int main()
{
    int n=7,p,t,i,j,res[3010],cnt,ct,k;

    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        for(i=0;i<n;i++)
            scanf(" %d %d",&tmp[i].x,&tmp[i].y);
        printf("Case #%d:\n",p);
        if(n<=3)
        {
            for(i=0;i<n;i++)
            {
                printf("0\n");
            }
            continue;
        }

        for(i=0;i<n;i++)
        {
            res[i]=99999999;
            need.x=tmp[i].x;
            need.y=tmp[i].y;
            for(j=0;j<(1<<n);j++)
            {
                cnt=0;
                ct=0;
                k=j;
                while(k)
                {
                    if(cnt==i||((1<<cnt)&j))
                    {
                        arr[ct].x=tmp[cnt].x;
                        arr[ct].y=tmp[cnt].y;
                        ct++;
                    }
                    k>>=1;
                    cnt++;
                }
                //printf("%d\n",ct);
                nodes=0;
                if(convexhull(ct))
                res[i]=min(res[i],n-nodes);
            }
            printf("%d\n",res[i]);
        }
    }

    return 0;
}
