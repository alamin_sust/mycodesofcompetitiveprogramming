#include<stdio.h>
#include<math.h>
#include<string.h>

main()
{
    char input[100010],first[10010],second[10010],sign,arr1[10010],arr2[10010],rev1[10010],rev2[10010],arr[10010];
    long long int ll,carry,k1,k2,fn,sn,res,l,l2,s1,s2,i,j,p,len,extra;
    long int t1=2147483647,t2=-2147483648;
    while(gets(input))
    {
        for(j=0;j<10010;j++)
                {arr1[j]='\0';
                arr2[j]='\0';
                arr[j]='\0';
                rev1[j]='\0';
                rev2[j]='\0';
                first[j]='\0';
                second[j]='\0';}
        fn=0;
        sn=0;
        res=0;
        s2=0;
        s1=0;
        ll=strlen(input);
        for(j=0,i=0;i<ll;i++)
        {
            if(input[i]=='+'||input[i]=='*')
            {sign=input[i];
            i++;
                break;}
            if(input[i]!=' ')
            first[j++]=input[i];
        }
        for(j=0;i<ll;i++)
        {
            if(input[i]!=' ')
            second[j++]=input[i];
        }
        k1=atoi(first);
        k2=atoi(second);
        l=strlen(first);
        if(first[0]=='-')
        {
            if(l>11)
                fn=1;
            else if(l==11&&first[1]>'2')
                fn=1;
            else if(k1>=0)
                fn=1;
        }
        else
        {
            if(l>10)
                fn=1;
            else if(l==10&&first[0]>'2')
                fn=1;
            else if(k1<0)
                fn=1;
        }
        l2=strlen(second);
        if(second[0]=='-')
        {
            if(l2>11)
                sn=1;
            else if(l2==11&&second[1]>'2')
                sn=1;
            else if(k2>=0)
                sn=1;
        }
        else
        {
            if(l2>10)
                sn=1;
            else if(l2==10&&second[0]>'2')
                sn=1;
            else if(k2<0)
                sn=1;
        }
        if(((k1==-1)&&(strcmp(second,"2147483648")==0))||((k2==-1)&&(strcmp(first,"2147483648")==0)))
            res=0;
        else if(first[0]=='0'||second[0]=='0')
            res=0;
        else if(l+l2>12&&sign=='*')
            res=1;
        else if(sn==1&&fn==1&&sign=='*')
            res=1;
        else if(sn==1&&sign=='*'&&first[0]!='0')
            res=1;
        else if(fn==1&&sign=='*'&&second[0]!='0')
            res=1;
        else if(fn==0&&sn==0&&sign=='*')
        {
            if((k1*k2)>=0)
            {
                if(((k1*k2)>t1))
                    res=1;
            }
            else
            {
                if(((k1*k2)<t2))
                    res=1;
            }
        }
        else if(sign=='+')
        {
            if(((fn+sn)>0&&first[0]=='-'&&second[0]=='-')||((fn+sn)>0&&first[0]!='-'&&second[0]!='-'))
                res=1;
            else
            {
                if(first[0]=='-')
                    s1=1;
                else
                    s1=0;
                if(second[0]=='-')
                    s2=1;
                else
                    s2=0;
                i=0;
                if(s1==1)
                    i++;
                if(l>l2)
                    len=l;
                else
                    len=l2;
                for(j=0;j<len;j++)
                {
                    arr1[j]='0';
                    arr2[j]='0';
                }
                for(p=0,j=l-i; i<l; i++)
                {
                    arr1[j--]=first[i];
                    rev1[p++]=first[i];
                }
                i=0;
                if(s2==1)
                    i++;
                for(p=0,j=l2-i; i<l2; i++)
                {
                    arr2[j--]=second[i];
                    rev2[p++]=second[i];
                }
                if((l-s1)>(l2-s2))
                    len=l-s1;
                else
                    len=l2-s2;
                if(s1==0&&s2==0)
                {
                    if(fn==1||sn==1)
                        res=1;
                    else if((k1+k2)>t1)
                        res=1;
                }
                else if(s1==1&&s2==1)
                {
                    if(fn==1||sn==1)
                        res=1;
                    else if((k1+k2)<t2)
                        res=1;
                }
                else
                {
                    if(strcmp(rev1,rev2)>0)
                    {
                        for(carry=0,j=len-1,i=0; i<len;j--,i++)
                        {
                            if((arr1[i]-arr2[i]-carry+'0')>='0')
                            {
                                arr[j]=(arr1[i]-arr2[i]-carry+'0');
                                carry=0;
                            }
                            else
                            {
                                arr[j]=arr1[i]-arr2[i]-carry+'0'+10;
                                carry=1;
                            }
                        }
                        extra=0;
                        for(i=0;arr[i]=='0';i++)
                            extra++;
                        if(s1==1)
                        {
                            if((strlen(arr)-extra)>10)
                            res=1;
                            else if((strlen(arr)-extra)==10&&arr[0]>'2')
                            res=1;
                            else if(atoi(arr)<0||atoi(arr)==t1+1)
                            res=1;
                        }
                        else
                        {
                            if((strlen(arr)-extra)>10)
                            res=1;
                            else if((strlen(arr)-extra)==10&&arr[0]>'2')
                            res=1;
                            else if(atoi(arr)<0)
                            res=1;
                        }
                    }
                    else
                    {
                        for(carry=0,j=len-1,i=0; i<len;j--,i++)
                        {
                            if((arr2[i]-arr1[i]-carry+'0')>='0')
                            {
                                arr[j]=(arr2[i]-arr1[i]-carry+'0');
                                carry=0;
                            }
                            else
                            {
                                arr[j]=arr2[i]-arr1[i]-carry+'0'+10;
                                carry=1;
                            }
                        }
                        extra=0;
                        for(i=0;arr[i]=='0';i++)
                            extra++;
                        if(s1==1)
                        {
                            if((strlen(arr)-extra)>10)
                            res=1;
                            else if((strlen(arr)-extra)==10&&arr[0]>'2')
                            res=1;
                            else if(atoi(arr)>=0||atoi(arr)==t1+1)
                            res=1;
                        }
                        else
                        {
                            if((strlen(arr)-extra)>10)
                            res=1;
                            else if((strlen(arr)-extra)==10&&arr[0]>'2')
                            res=1;
                            else if(atoi(arr)<0)
                            res=1;
                        }
                    }
                    }
            }
        }
        printf("%s (%s %c %s %s)\n",input,first,sign,second,arr);
        if(fn==1)
            printf("first number too big\n");
        if(sn==1)
            printf("second number too big\n");
        if(res==1)
            printf("result too big\n");
    }
    return 0;
}
