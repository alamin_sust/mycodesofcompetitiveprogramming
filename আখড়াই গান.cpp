#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.0000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

double calc(double ax,double ay,double bx,double by,double cx,double cy)
{
    double ab=sqrt( (ax-bx)*(ax-bx) + (ay-by)*(ay-by) );
    double ac=sqrt( (ax-cx)*(ax-cx) + (ay-cy)*(ay-cy) );
    double bc=sqrt( (cx-bx)*(cx-bx) + (cy-by)*(cy-by) );
    double P=(ab+ac+bc)/2.0;
    double ret=sqrt(P*(P-ab)*(P-bc)*(P-ac));
    if((ret-eps)<0.0)
        ret=99999999999.0;
    return ret;
}

main()
{
    double ax,ay,bx,by,cx,cy,px,py,A,area;
    while(cin>>ax>>ay>>bx>>by>>cx>>cy>>px>>py)
    {
        A=calc(ax,ay,bx,by,cx,cy);
        area=0.0;
        area+=calc(ax,ay,bx,by,px,py);
        area+=calc(ax,ay,cx,cy,px,py);
        area+=calc(cx,cy,bx,by,px,py);
        if((area+eps)>A&&(area-eps)<A)
        {
            printf("YES\n");
        }
        else
            printf("NO\n");
    }
    return 0;
}
