/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

string arr[1010],old,nw;
int i,k,n;
map<string,string>mpp;
map<string,int>mpp2;

int main()
{
    cin>>n;
    k=0;
    for(i=0;i<n;i++)
    {
        cin>>old>>nw;
        if(mpp2[old]==0)
        {
            arr[k++]=old;
        }
        mpp2[old]=mpp2[nw]=1;
        mpp[old]=nw;
    }
    printf("%d\n",k);
    for(i=0;i<k;i++)
    {
        cout<<arr[i]<<" ";
        while(mpp[arr[i]]!="")
            arr[i]=mpp[arr[i]];
        cout<<arr[i]<<endl;
    }
    return 0;
}





