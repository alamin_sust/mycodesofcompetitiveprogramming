/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll a2[40],a3[40],MOD;

void construct_arr(void)
{
    a2[0]=2LL;
    a3[0]=3LL;
    for(ll i=1;i<=37;i++)
    {
        a2[i]=(a2[i-1]*a2[i-1])%MOD;
        a3[i]=(a3[i-1]*a3[i-1])%MOD;
    }
    return;
}


ll pow2(ll val)
{
    //printf("%lld..\n",val);
    ll ret=1LL;
    for(ll i=0LL;val>0LL;i++)
    {
        if((val%2LL)!=0LL)
        ret=(ret*a2[i])%MOD;
        val/=2LL;
    }
    //printf("%lld--\n",ret);
    return ret;
}

ll pow3(ll val)
{
    //printf(".%lld\n",val);
    ll ret=1LL;
    for(ll i=0LL;val>0LL;i++)
    {
        if((val%2LL)!=0LL)
        ret=(ret*a3[i])%MOD;
        val/=2LL;
    }
    return ret;
}

main()
{
    ll t,n,m,p,res;
    MOD=1000000007LL;
    construct_arr();
    scanf(" %lld",&t);
    for(p=1LL;p<=t;p++)
    {
        res=0LL;
        scanf(" %lld %lld",&n,&m);
        if(m==1LL&&n>1LL)
            res=0LL;
        else if(m<=2LL)
        {
            res=(m*pow2((n-1LL)/2LL))%MOD;
        }
        else
        {
            res=(2LL*pow2((n-1LL)/2LL))%MOD;
            //printf("%lld\n",res);
            m-=2LL;
            res=(res+(m*(pow2(n/2LL)*pow3((n-1LL)/2LL))%MOD)%MOD)%MOD;
        }
        printf("%lld\n",res);
    }
    return 0;
}

