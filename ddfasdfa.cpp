#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>

using namespace std;
#define inf 0x3f3f3f3f

int main()
{
    int n,a;
    int p[100005],sum1,sum2;
    while(~scanf("%d%d",&n,&a))
    {
        for(int i=0;i<n;i++)
            scanf("%d",&p[i]);
        sort(p,p+n);
        if(a<p[1])
            sum1=p[n-1]-a;
        else if(a>p[n-1])
            sum1=a-p[1];
        else
        {
            sum1=(p[n-1]-p[1])+min(p[n-1]-a,a-p[1]);
        }

        if(a<p[0])
            sum2=p[n-2]-a;
        else if(a>p[n-2])
            sum2=a-p[0];
        else
        {
            sum2=(p[n-2]-p[0])+min(p[n-2]-a,a-p[0]);
        }
        int ans=min(sum1,sum2);
        printf("%d\n",ans);
    }
}
