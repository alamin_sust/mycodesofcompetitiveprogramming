/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int p,t,l,i,tp;
double C=12.01,H=1.008,O=16.0,N=14.01,res;
char arr[110];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %s",arr);
        l=strlen(arr);
        res=0.0;
        for(i=0;i<l;)
        {
            if(arr[i]=='C')
            {
                i++;
                tp=0;
                while(i<l&&arr[i]>='1'&&arr[i]<='9')
                {
                    tp=tp*10+arr[i]-'0';
                    i++;
                }
                tp=max(1,tp);
                res+=(double)tp*C;
            }
            else if(arr[i]=='H')
            {
                i++;
                tp=0;
                while(i<l&&arr[i]>='1'&&arr[i]<='9')
                {
                    tp=tp*10+arr[i]-'0';
                    i++;
                }
                tp=max(1,tp);
                res+=(double)tp*H;
            }
            else if(arr[i]=='O')
            {
                i++;
                tp=0;
                while(i<l&&arr[i]>='1'&&arr[i]<='9')
                {
                    tp=tp*10+arr[i]-'0';
                    i++;
                }
                tp=max(1,tp);
                res+=(double)tp*O;
            }
            else if(arr[i]=='N')
            {
                i++;
                tp=0;
                while(i<l&&arr[i]>='1'&&arr[i]<='9')
                {
                    tp=tp*10+arr[i]-'0';
                    i++;
                }
                tp=max(1,tp);
                res+=(double)tp*N;
            }
        }
       printf("%.3lf\n",res);
    }
    return 0;
}




