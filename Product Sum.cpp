/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll dp[23][2][2],k,arr[23],M=1000000007;

ll rec(ll pos,ll flag,ll st,ll product)
{
    printf("%lld------\n",product);
    if(pos==k)
        return product;
    ll &ret=dp[pos][flag][st];
    if(ret!=-1LL)
        return ret;
    ret=0LL;
    for(ll i=0; i<arr[pos]; i++)
    {
        ll pd;
        if(st==0LL&&i>0LL)
            pd=i;
        else
            pd=i*product;

        ret=(ret+rec(pos+1LL,1LL,st|(i>0LL),pd))%M;
    }
    if(flag==0)
    {
        ll pd;
        if(st==0LL&&arr[pos]>0LL)
            pd=arr[pos];
        else
            pd=arr[pos]*product;
        ret=(ret+rec(pos+1LL,0LL,(st|(arr[pos]>0LL)),pd))%M;
    }
    else
    {

        for(ll i=arr[pos]; i<=9LL; i++)
        {
            ll pd;
            if(st==0LL&&i>0LL)
                pd=i;
            else
                pd=i*product;
            ret=(ret+rec(pos+1LL,1LL,st|(i>0LL),pd))%M;
        }
    }
    return ret;
}


int main()
{
    ll t,p,res,aa,a,i;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    cin>>t;
    for(p=1LL; p<=t; p++)
    {
        scanf(" %lld",&a);
        a--;
        memset(dp,-1LL,sizeof(dp));
        aa=a;
        k=0LL;
        while(aa>0LL)
            aa/=10LL,k++;
        aa=a;
        for(i=k-1LL; i>=0LL; i--)
        {
            arr[i]=aa%10LL;
            aa/=10LL;
        }
        res=rec(0LL,0LL,0LL,0LL);

        printf("%lld...\n",res);

        scanf(" %lld",&a);
        memset(dp,-1LL,sizeof(dp));
        aa=a;
        k=0LL;
        while(aa>0LL)
            aa/=10LL,k++;
        aa=a;
        for(i=k-1LL; i>=0LL; i--)
        {
            arr[i]=aa%10LL;
            aa/=10LL;
        }
        res=rec(0LL,0LL,0LL,0LL)-res;
        res=(res+M)%M;
        printf("Case %lld: %lld\n",p,res);

    }
    return 0;
}




