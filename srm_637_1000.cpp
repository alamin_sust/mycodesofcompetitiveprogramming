#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include<string.h>
#include <algorithm>

using namespace std;

int rr[]={1,-1,0,0};
int cc[]={0,0,1,-1};

struct node {
int x,y;
};

node u,v;
map<char,int>mpp;

queue<node>q;

int k=1,val[1010],col[110][110],n,m,adj[110][110];

int bfs(int x,int y,vector <string> board)
{
    int ret=1;
    col[x][y]=1;
    u.x=x;
    u.y=y;
    q.push(u);
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        //printf("..");
        for(int i=0;i<4;i++)
        {
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            if(v.x>=0&&v.y>=0&&v.x<n&&v.y<m&&col[v.x][v.y]==0&&board[u.x][u.y]==board[v.x][v.y])
            {
                col[v.x][v.y]=1;
                q.push(v);
                ret++;
            }
        }

    }
    return ret;
}

class ConnectingGameDiv2
{
public:
	int getmin(vector <string> board)
	{
	    int i,j;
	    n=board.size();
	    m=board[0].length();
	    mpp.clear();
	    k=1;
	    memset(col,0,sizeof(col));
	    memset(val,0,sizeof(val));
	    for(i=0;i<100;i++)
            for(j=0;j<100;j++)
            adj[i][j]=99999999;
	    for(i=0;i<n;i++)
        {
            for(j=0;j<m;j++)
            {
                if(mpp[board[i][j]]==0)
                {
                    mpp[board[i][j]]=k;
                    val[k]=bfs(i,j,board);
                    //printf("%d--\n",val[k]);
                    k++;
                }
            }
        }
     //   printf("....");
        for(i=0;i<n;i++)
        {
            for(j=0;j<m-1;j++)
            {
                if(board[i][j]!=board[i][j+1])
                {
                   //printf(" %c->%c = %d\n",board[i][j],board[i][j+1],val[mpp[board[i][j]]]);
                    adj[mpp[board[i][j]]][mpp[board[i][j+1]]]=val[mpp[board[i][j]]];
                }
                if(i>0&&board[i][j]!=board[i-1][j])
                    adj[mpp[board[i][j]]][mpp[board[i-1][j]]]=val[mpp[board[i][j]]];
                if(i+1<n&&board[i][j]!=board[i+1][j])
                    adj[mpp[board[i][j]]][mpp[board[i+1][j]]]=val[mpp[board[i][j]]];
                if(i>0&&j>0&&board[i][j]!=board[i-1][j-1])
                    adj[mpp[board[i][j]]][mpp[board[i-1][j-1]]]=val[mpp[board[i][j]]];
                if(i>0&&j+1<m&&board[i][j]!=board[i-1][j+1])
                    adj[mpp[board[i][j]]][mpp[board[i-1][j+1]]]=val[mpp[board[i][j]]];
                if(i+1<n&&j>0&&board[i][j]!=board[i+1][j-1])
                    adj[mpp[board[i][j]]][mpp[board[i+1][j-1]]]=val[mpp[board[i][j]]];
                if(i+1<n&&j+1<m&&board[i][j]!=board[i+1][j+1])
                    adj[mpp[board[i][j]]][mpp[board[i+1][j+1]]]=val[mpp[board[i][j]]];
            }
        }
        for(i=0;i<n;i++)
        {
            adj[0][mpp[board[i][0]]]=0;
            adj[mpp[board[i][m-1]]][k]=val[mpp[board[i][m-1]]];
            //printf("%c %d\n",board[i][m-1],val[mpp[board[i][m-1]]]);
        }
       /* printf("%d\n",k);
        for(i=0;i<=k;i++)
        {
            for(j=0;j<=k;j++)
            {
                printf("%d ",adj[i][j]);
            }
            printf("\n");
        }*/
        for(int K=0;K<=k;K++)
        {
            for(i=0;i<=k;i++)
            {
                for(j=0;j<=k;j++)
                {
                    if(adj[i][j]>(adj[i][K]+adj[K][j]))
                        adj[i][j]=adj[i][K]+adj[K][j];
                }
            }
        }
        return adj[0][k];
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	ConnectingGameDiv2 objectConnectingGameDiv2;

	//test case0
	vector <string> param00;
	param00.push_back("AA");
	param00.push_back("BC");
	int ret0 = objectConnectingGameDiv2.getmin(param00);
	int need0 = 2;
	assert_eq(0,ret0,need0);

	//test case1
	vector <string> param10;
	param10.push_back("AAB");
	param10.push_back("ACD");
	param10.push_back("CCD");
	int ret1 = objectConnectingGameDiv2.getmin(param10);
	int need1 = 4;
	assert_eq(1,ret1,need1);

	//test case2
	vector <string> param20;
	param20.push_back("iii");
	param20.push_back("iwi");
	param20.push_back("iii");
	int ret2 = objectConnectingGameDiv2.getmin(param20);
	int need2 = 8;
	assert_eq(2,ret2,need2);

	//test case3
	vector <string> param30;
	param30.push_back("rng58");
	param30.push_back("Snuke");
	param30.push_back("Sothe");
	int ret3 = objectConnectingGameDiv2.getmin(param30);
	int need3 = 6;
	assert_eq(3,ret3,need3);

	//test case4
	vector <string> param40;
	param40.push_back("AAAAAAaaabcdefg");
	param40.push_back("AAAAAAhhDDDDDDD");
	param40.push_back("AAAAiAjDDDDDDDD");
	param40.push_back("AAAAiijDDDDDDDD");
	param40.push_back("AAAAAAAkDDDDDDD");
	param40.push_back("AAAAoAAAlDDDDDD");
	param40.push_back("AAApBnAAlDDDDDD");
	param40.push_back("srqBBBmmmmDDDDD");
	int ret4 = objectConnectingGameDiv2.getmin(param40);
	int need4 = 26;
	assert_eq(4,ret4,need4);

}

/*{
"AAAAAAaaabcdefg",
"AAAAAAhhDDDDDDD",
"AAAAiAjDDDDDDDD",
"AAAAiijDDDDDDDD",
"AAAAAAAkDDDDDDD",
"AAAAoAAAlDDDDDD",
"AAApBnAAlDDDDDD",
"srqBBBmmmmDDDDD"}
*/
