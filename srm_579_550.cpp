#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class UndoHistory
{
public:
	int minPresses(vector <string> lines)
	{

	    return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	UndoHistory objectUndoHistory;

	//test case0
	vector <string> param00;
	param00.push_back("tomorrow");
	param00.push_back("topcoder");
	int ret0 = objectUndoHistory.minPresses(param00);
	int need0 = 18;
	assert_eq(0,ret0,need0);

	//test case1
	vector <string> param10;
	param10.push_back("a");
	param10.push_back("b");
	int ret1 = objectUndoHistory.minPresses(param10);
	int need1 = 6;
	assert_eq(1,ret1,need1);

	//test case2
	vector <string> param20;
	param20.push_back("a");
	param20.push_back("ab");
	param20.push_back("abac");
	param20.push_back("abacus");
	int ret2 = objectUndoHistory.minPresses(param20);
	int need2 = 10;
	assert_eq(2,ret2,need2);

	//test case3
	vector <string> param30;
	param30.push_back("pyramid");
	param30.push_back("sphinx");
	param30.push_back("sphere");
	param30.push_back("python");
	param30.push_back("serpent");
	int ret3 = objectUndoHistory.minPresses(param30);
	int need3 = 39;
	assert_eq(3,ret3,need3);

	//test case4
	vector <string> param40;
	param40.push_back("ba");
	param40.push_back("a");
	param40.push_back("a");
	param40.push_back("b");
	param40.push_back("ba");
	int ret4 = objectUndoHistory.minPresses(param40);
	int need4 = 13;
	assert_eq(4,ret4,need4);

}

