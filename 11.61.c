#include<stdio.h>
extern int i=0;
typedef struct
{
    int month;
    int day;
    int year;
} date;
typedef struct
{
    char street[80];
    char city[80];
} addressname;

union address
{
    addressname office_address;
    addressname home_address;
};


typedef struct
{
    int offhom;
    char name[80];
    union address addressinput;
    int acct_no;
    char acct_type;
    float oldbalance;
    float newbalance;
    float payment;
    date lastpayment;
} record;
record readinput(int i);
void writeoutput(record customer);

main()
{
    int i,n;
    record customer[100];
    printf("CUSTOMER BILLING SYSTEM\n\n");
    printf("HOW MANY CUSTOMERS ARE THERE? ");
    scanf("%d", &n);
    for(i=0; i<n; i++)
    {
        customer[i]=readinput(i);

        if(customer[i].payment>0)
            customer[i].acct_type=(customer[i].payment<0.1*customer[i].oldbalance)? 'O' : 'C';

        else
            customer[i].acct_type = (customer[i].oldbalance > 0 )? 'D' :'C';

        customer[i].newbalance = customer[i].oldbalance - customer[i].payment;
    }

    for(i=0; i<n; ++i)
    {
        writeoutput(customer[i]);
    }
}

record readinput(int i)
{
    char flag;
    record customer;

    printf("\nCustomer no. %d\n",i+1);
    printf("  Name: ");
    scanf(" %[^\n]",customer.name);
    printf("which address You want to enter? \nenter h for home address or enter o for office address: ");
    scanf(" %c",&flag);
    if(flag=='o')
    {
        printf("OFFICE ADDRESS:\n");
        printf("  Street: ");
        scanf(" %[^\n]",customer.addressinput.office_address.street);
        printf("  City: ");
        scanf(" %[^\n]",customer.addressinput.office_address.city);
        i=1;
        customer.offhom=1;
    }
    else if(flag=='h')
    {
        printf("HOME ADDRESS:\n");
        printf("  Street: ");
        scanf(" %[^\n]",customer.addressinput.home_address.street);
        printf("  City: ");
        scanf(" %[^\n]",customer.addressinput.home_address.city);
        i=2;
        customer.offhom=2;
    }
    printf("  Account number: ");
    scanf("%d",&customer.acct_no);
    printf("  Previous balance: ");
    scanf("%f",&customer.oldbalance);
    printf("  Current payment: ");
    scanf("%f",&customer.payment);
    printf("  Payment date (MM/DD/YY): ");
    scanf("%d/%d/%d",&customer.lastpayment.month,
          &customer.lastpayment.day,
          &customer.lastpayment.year);
    return(customer);
}

void writeoutput(record customer)

{
    printf("\nName: %s",customer.name);
    printf("    Account number: %d\n",customer.acct_no);
    if(customer.offhom==1)
    {
        printf("\nOFFICE ADDRESS:\n");
        printf("Street: %s\n",customer.addressinput.office_address.street);
        printf("City: %s\n\n",customer.addressinput.office_address.city);
    }
    else if(customer.offhom==2)
    {
        printf("\nHOME ADDRESS:\n");
        printf("Street: %s\n",customer.addressinput.home_address.street);
        printf("City: %s\n\n",customer.addressinput.home_address.city);
    }
    printf("Oldbalance: %7.2f",customer.oldbalance);
    printf("  Current paymment: %7.2f",customer.payment);
    printf("  Newbalance: %7.2f\n\n",customer.newbalance);
    printf("Account Status: ");

    switch (customer.acct_type)
    {
    case 'C':
        printf("CURRENT\n\n");
        break;
    case 'O':
        printf("OVERDUE\n\n");
        break;
    case 'D':
        printf("DELINQUENT\n\n");
        break;
    default:
        printf("ERROR\n\n");
    }
    return;
}








