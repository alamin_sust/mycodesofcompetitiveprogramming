
/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node
{
    int a,b,c,d,e;
};

vector<node>v;
node det,arr[10010];

int t,p,i,j,aa,bb,cc,dd,ee,now,n,flag,flag2,kk,fg;

map<pair<int,int>,int >mpp,mpp2;

int comp(node a,node b)
{
    if(a.e==b.e&&a.a==b.a&&a.b==b.b&&a.c==b.c)
    {
        return a.d<b.d;
    }
    if(a.e==b.e&&a.a==b.a&&a.b==b.b)
    {
        return a.c<b.c;
    }
    if(a.e==b.e&&a.a==b.a)
    {
        return a.b<b.b;
    }
    if(a.e==b.e)
    {
        return a.a<b.a;
    }
    return a.e<b.e;
}

int main()
{
    freopen("C-small-attempt0.in","r",stdin);
    freopen("C-small-attempt0out.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {
        mpp.clear();
        mpp2.clear();
        scanf(" %d",&n);
        for(i=0; i<n; i++)
        {
            scanf(" %d.%d.%d.%d/%d",&det.a,&det.b,&det.c,&det.d,&det.e);
                        kk=32-det.e;
                        dd=det.d;
                        cc=det.c;
                        bb=det.b;
                        aa=det.a;
                        if(kk>=0)
                            dd=det.d>>(min(8,kk)),kk-=8;
                        if(kk>=0)
                            cc=det.c>>(min(8,kk)),kk-=8;
                        if(kk>=0)
                            bb=det.b>>(min(8,kk)),kk-=8;
                        if(kk>=0)
                            aa=det.a>>(min(8,kk)),kk-=8;
                        if(mpp[make_pair(((det.e<<16)+(aa<<8)+bb),((cc<<8)+dd))]==0)
            {
                mpp[make_pair(((det.e<<16)+(aa<<8)+bb),((cc<<8)+dd))]=1;
                arr[i].a=aa;
                arr[i].b=bb;
                arr[i].c=cc;
                arr[i].d=dd;
                arr[i].e=det.e;
            }
            else
            {
                n--;
                i--;
            }
        }
        sort(arr,arr+n,comp);
        now=0;
        v.clear();
        while(1)
        {
            fg=0;
            flag=0;
            for(i=0; i<n; i++)
            {
                if(arr[i].e==now)
                {
                    mpp2[make_pair(((arr[i].e<<16)+(arr[i].a<<8)+arr[i].b),((arr[i].c<<8)+arr[i].d))]=1;
                    v.push_back(arr[i]);
                    fg=1;
                }
            }
            if(fg==0)
            {
                now++;
                continue;
            }
            flag2=0;
            for(i=0; i<n; i++)
            {
                if(arr[i].e>now)
                {
                    flag=0;
                    for(j=0; j<v.size(); j++)
                    {
                        kk=32-v[j].e;
                        dd=v[j].d;
                        cc=v[j].c;
                        bb=v[j].b;
                        aa=v[j].a;
                        if(kk>=0)
                            dd=v[j].d>>(min(8,kk)),kk-=8;
                        if(kk>=0)
                            cc=v[j].c>>(min(8,kk)),kk-=8;
                        if(kk>=0)
                            bb=v[j].b>>(min(8,kk)),kk-=8;
                        if(kk>=0)
                            aa=v[j].a>>(min(8,kk)),kk-=8;
                        if(mpp2[make_pair(((v[j].e<<16)+(aa<<8)+bb),((cc<<8)+dd))]==1)
                        {
                            flag=1;
                            break;
                        }
                    }
                    if(flag==0)
                    {
                        flag2=1;
                    }
                }

            }
            if(flag2==0)
                break;
            now++;
        }
        printf("Case #%d:\n",p);
        for(i=0; i<v.size(); i++)
        {
            printf("%d.%d.%d.%d/%d\n",v[i].a,v[i].b,v[i].c,v[i].d,v[i].e);
        }
    }

    return 0;
}

