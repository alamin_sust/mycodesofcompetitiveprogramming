/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll x,cum[200010],M[200010],hh,k,i,j,arr[200010],n,m,res,h,mn;

ll f(ll ind, ll N)
{
    return cum[ind]-cum[ind-N];
}

ll func(ll ind)
{
    ll low=1;
    ll high=ind;
    ll mid=(low+high)/2LL;
    while(high>=low)
    {

        if(mid==ind&&f(ind,mid)<=k)
            break;
        if(f(ind,mid)>k&&f(ind,mid-1LL)<=k)
            break;
        if(f(ind,mid)>k)
            high=mid-1;
        else low=mid+1;
        mid=low + ((high - low) / 2LL);
    }
    return mid;
}


main()
{
    while(scanf("%I64d%I64d%I64d",&n,&m,&k)!=EOF)
    {
    for(i=1; i<=n; i++)
    {
        scanf(" %I64d",&arr[i]);
        cum[i]=cum[i-1]+arr[i];
    }
    for(i=1; i<=m; i++)
        scanf(" %I64d",&M[i]);
    i=1;
    hh=1;
    x=1;
    res=0;
    while(1)
    {
        if(i>n)
            break;
        if(hh<=m)
            mn=min(n-i+1,M[hh]-x),res+=mn,i+=mn,x+=mn;
        else
            res+=n-i+1,i=n+1;
        if(i<=n)
        {
            h=func(i-1);
            i-=h;
            res++;
            x++;
        }
        hh++;
    }
    printf("%I64d\n",res);
    }
    return 0;
}

