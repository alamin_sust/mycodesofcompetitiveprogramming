/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int stat[210],b,mx,val[210],flag[210];
queue<int>q;

struct node{
int x,y;
};

vector<int>adj[210];

node arr[210];

void dfs(int s)
{
    memset(stat,0,sizeof(stat));
    stat[s]=0;
    while(!q.empty())
        q.pop();
    q.push(s);
    b=s;
    val[s]=0;
    while(!q.empty())
    {
        int now=q.front();
        flag[now]=1;
        for(int i=0;i<adj[now].size();i++)
        {
            int v=adj[now][i];
            if(flag[v]==0)
            {
                val[v]=val[now]+1;
                flag[v]=1;
                if(val[v]>mx)
                mx=val[v],b=v;
                q.push(v);
            }
        }
        q.pop();
    }
    return;
}

main()
{
    int n,i,j,ans=0,res=0;
    cin>>n;
    for(i=1;i<n;i++)
    {
        scanf(" %d %d",&arr[i].x,&arr[i].y);
    }
    for(i=1;i<n;i++)
    {
        for(j=0;j<=n;j++)
            {adj[j].clear();
            flag[j]=0;}
        for(j=1;j<n;j++)
        {
            if(i==j)
                continue;
            adj[arr[j].x].push_back(arr[j].y);
            adj[arr[j].y].push_back(arr[j].x);
        }
        for(j=1;j<=n;j++)
        {
            if(flag[j]==0)
            {
                mx=0;
                dfs(j);
                memset(flag,0,sizeof(flag));
                dfs(b);
                break;
            }
        }
        res=mx;
        //printf("..%d\n",mx);
        for(j=1;j<=n;j++)
        {
            if(flag[j]==0)
            {
                mx=0;
                dfs(j);
                memset(flag,0,sizeof(flag));
                dfs(b);
                break;
            }
        }
        //printf("..%d\n",mx);
        res*=mx;
        if(res>ans)
            ans=res;
    }
    printf("%d\n",ans);
    return 0;
}


