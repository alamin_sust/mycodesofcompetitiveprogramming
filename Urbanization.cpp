/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll n,n1,n2,i,arr[100010];
double res,sum;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);


    scanf(" %I64d %I64d %I64d",&n,&n1,&n2);


    for(i=0LL;i<n;i++)
    {
        scanf(" %I64d",&arr[i]);
    }

    sort(arr,arr+n);

    reverse(arr,arr+n);

    res=0.0;
    sum=0.0;
    if(n1>n2)
    {
        for(i=0;i<n2;i++)
        {
            sum+=(double)arr[i];
        }
        res=sum/(double)n2;
        sum=0.0;
        for(i=n2;i<(n1+n2);i++)
        {
            sum+=(double)arr[i];
        }
        res+=sum/(double)n1;
    }
    else
    {
        for(i=0;i<n1;i++)
        {
            sum+=(double)arr[i];
        }
        res=sum/(double)n1;
        sum=0.0;
        for(i=n1;i<(n1+n2);i++)
        {
            sum+=(double)arr[i];
        }
        res+=sum/(double)n2;
    }

    printf("%.10lf\n",res);



    return 0;
}






