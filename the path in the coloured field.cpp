#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

int rr[]={0,0,-1,1};
int cc[]={1,-1,0,0};
int n,stat[1010][1010],adj[1010][1010],val[1010][1010];

int bfs(int x,int y)
{
    queue<int>xx;
    queue<int>yy;
    xx.push(x);
    yy.push(y);
    stat[x][y]=1;
    val[x][y]=0;
    while(!xx.empty())
    {
        int px=xx.front();
        int py=yy.front();
        xx.pop();
        yy.pop();
        for(int i=0;i<4;i++)
        {
            int xxx=px+rr[i];
            int yyy=py+cc[i];
            if(xxx>0&&xxx<=n&&yyy>0&&yyy<=n)
            if(stat[xxx][yyy]==0)
            {
                val[xxx][yyy]=val[px][py]+1;
                stat[xxx][yyy]=1;
                if(adj[xxx][yyy]==3)
                    return val[xxx][yyy];
                xx.push(xxx);
                yy.push(yyy);
            }
        }
    }
}

main()
{
    char ch;
    int i,res,j,k,l;
    while(cin>>n)
    {
        getchar();
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
            {
                scanf("%c",&ch);
                adj[i][j]=ch-'0';
            }
            getchar();
        }
        for(res=0,i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
            {
                if(adj[i][j]==1)
                {
                    for(k=1;k<=n;k++)
                        for(l=1;l<=n;l++)
                            stat[k][l]=0;
                    res=max(res,bfs(i,j));
                }
            }
        }
        printf("%d\n",res);
    }
    return 0;
}

