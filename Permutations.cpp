/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll n,m,p,q,mx,j,tp,f,arr[110];

ll fact(ll val)
{
    if(!val)
        return 1LL;
    return val*fact(val-1LL);
}

ll mn(ll from,ll to)
{
    ll ret=9999999LL;
    for(ll i=from;i<=to;i++)
        ret=min(ret,arr[i]);
    return ret;
}

int main()
{
    ll i;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    cin>>n>>m;
    for(i=1LL;i<=n;i++)
        arr[i]=i;
    f=fact(n);
    mx=0;
    for(p=0LL;p<f;p++)
    {
        tp=0LL;
        for(i=1LL;i<=n;i++)
        {
            for(j=i;j<=n;j++)
            tp+=mn(i,j);
        }
        //printf("%lld\n",tp);
        mx=max(tp,mx);
        next_permutation(arr+1LL,arr+n+1LL);
    }

    for(q=0,p=0LL;p<f;p++)
    {
        tp=0LL;
        for(i=1LL;i<=n;i++)
        {
            for(j=i;j<=n;j++)
            tp+=mn(i,j);
        }
        if(tp==mx)
            q++;
        if(q==m)
            break;
        next_permutation(arr+1LL,arr+n+1LL);
    }


    for(i=1LL;i<=n;i++)
        printf("%I64d ",arr[i]);

    return 0;
}





