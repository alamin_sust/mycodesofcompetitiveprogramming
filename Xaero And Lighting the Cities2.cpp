/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

vector<int>adj[500010];
queue<int>q,q2;
int n,arr[500010],stat[500010],val[500010];

int bfs3(int s)
{
    int ret1=0,ret2=1;


    while(!q.empty())
        q.pop();
    q.push(s);
    stat[s]=1;
    val[s]=0;
    while(!q.empty())
    {
        int u=q.front();
        q.pop();
        for(int i=0; i<adj[u].size(); i++)
        {
            int v=adj[u][i];
            if(stat[v]==0&&val[v]==1)
            {
                stat[v]=1;
                q.push(v);
                val[v]=val[u]+1;
                if(val[v]%2)
                    ret1++;
                else
                    ret2++;
            }
        }
    }

    return min(ret1,ret2);
}

int bfs(void)
{
    //printf("...");
    for(int i=1; i<=n; i++)
    {
        val[i]=0;
        stat[i]=0;
    }
    while(!q.empty())
        q.pop();
    while(!q2.empty())
        q2.pop();
    for(int i=1; i<=n; i++)
    {
        if(arr[i]==1)
        {
            q2.push(i);
            val[i]=0;
            stat[i]=1;
        }
    }
    int ret=0;
    while(!q2.empty())
    {
        //printf("ppp");
        q=q2;
        while(!q2.empty())q2.pop();
        while(!q.empty())
        {
            int u=q.front();
            q.pop();
            for(int i=0; i<adj[u].size(); i++)
            {
                int v=adj[u][i];
                if(stat[v]==0)
                {
                    stat[v]=1;
                    val[v]=val[u]+1;
                    if(val[v]<=1)
                        q.push(v);
                    else
                    {
                        //   printf("%d,,,,,\n",v);
                        val[v]=0;
                        q2.push(v);
                        ret++;
                    }

                }
            }
        }
    }
    //printf("%d...\n",ret);
    for(int i=1; i<=n; i++)
        stat[i]=0;
    //printf("%d..\n",val[i]);
    for(int i=1; i<=n; i++)
    {
        if(stat[i]==0&&val[i]==1)
        {
            ret+=bfs3(i);
        }
    }
    return ret;
}

void bfs2(void)
{
    for(int i=1; i<=n; i++)
    {
        val[i]=0;
        stat[i]=0;
    }
    while(!q.empty())
        q.pop();
    q.push(1);
    val[1]=0;
    stat[1]=1;
    while(!q.empty())
    {
        int u=q.front();
        q.pop();
        for(int i=0; i<adj[u].size(); i++)
        {
            int v=adj[u][i];
            if(stat[v]==0)
            {
                stat[v]=1;
                q.push(v);
                val[v]=val[u]+1;
            }
        }
    }
    return;
}


int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    int from,to,t,p,i,flag,res1,res2;

    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {
        flag=0;
        scanf(" %d",&n);
        for(i=1; i<=n; i++)
        {
            adj[i].clear();
            scanf(" %d",&arr[i]);
            if(arr[i])flag=1;
        }
        for(i=1; i<n; i++)
        {
            scanf(" %d %d",&from,&to);
            adj[from].push_back(to);
            adj[to].push_back(from);
        }
        if(flag)
        {
            printf("%d\n",bfs());
            continue;
        }
        bfs2();
        res1=res2=0;
        for(i=1; i<=n; i++)
        {
            if(val[i]%2)
            {
                if(arr[i]==0)
                    res1++;
            }
            else
            {
                if(arr[i]==0)
                    res2++;
            }
        }
        printf("%d\n",min(res1,res2));
    }


    return 0;
}


/*
8
8
0 0 0 1 1 0 0 0
1 2 2 3 3 4 4 5 5 6 6 7 7 8
*/
