#include<stdio.h>
#include<math.h>
#define inf 900000010

long long int maximum,rmaxq[100010],chunk,minimum,res,res2,to2,res3,res4,res1,from,to,st,en,mn=900000010,mx=-1,i,n,arr[100010],m,k,j,p,q,rmq[100010];
double result;

int range_minimum_query(void)
{
        st=from/chunk+1;
        en=to/chunk-1;
        res=inf;
        if(st<=en)
        {
            for(i=st;i<=en;i++)
            {
                res=min(res,rmq[i]);
            }
        for(i=from;i<(st*chunk);i++)
        res=min(res,arr[i]);
        for(i=to;i>=((en+1)*chunk);i--)
        res=min(res,arr[i]);
        }
        else
        {
            for(i=from;i<=to;i++)
            res=min(res,arr[i]);
        }
        return res;
}


int range_maximum_query(void)
{
    st=from/chunk+1;
        en=to/chunk-1;
        res=0;
        if(st<=en)
        {
            for(i=st;i<=en;i++)
            {
                res=max(res,rmaxq[i]);
            }
            for(i=from;i<(st*chunk);i++)
            res=max(res,arr[i]);
            for(i=to;i>=((en+1)*chunk);i--)
            res=max(res,arr[i]);
        }
        else
        {
            for(i=from;i<=to;i++)
            res=max(res,arr[i]);
        }
        return res;
}

main()
{
    scanf(" %lld",&n);
    for(i=0;i<n;i++)
    {scanf(" %lld",&arr[i]);
    if(arr[i]>mx)
    mx=arr[i];
    }
    chunk=sqrt(n);
    if(chunk*chunk!=n)
    chunk++;
    m=chunk*chunk;
    for(i=n;i<m;i++)
    arr[i]=inf;
    for(i=0,k=0;i<chunk;i++)
    {
        for(minimum=inf,maximum=0,j=0;j<chunk;k++,j++)
        {
            if(arr[k]<minimum)
            minimum=arr[k];
            if(arr[k]>maximum)
            maximum=arr[k];
        }
        rmq[i]=minimum;
        rmaxq[i]=maximum;
    }
    scanf(" %lld",&q);
    for(p=1;p<=q;p++)
    {
        res1=res2=res3=res4=0;
        scanf(" %lld %lld",&from,&to);
        to2=to;
        res1=range_minimum_query();
        res4=range_maximum_query();
        if(from!=0)
        {
        to=from-1;
        from=0;
        res2=range_maximum_query();}
        if(to2!=(n-1))
        {
            from=to2+1;
            to=n-1;
            res3=range_maximum_query();
        }
        res=max(res2,res3);
        result=(double)res1+(double)res;
        if((((double)res1+(double)res4)/2.0)>result)
        result=((double)res1+(double)res4)/2.0;
        printf("%.1lf\n",result);
    }
    return 0;
}

