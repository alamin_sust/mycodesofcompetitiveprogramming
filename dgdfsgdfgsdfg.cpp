/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


ll A[1010],dp[32][1010];
int n;
ll rec(int i,int j,int k)
{
if(i<=0 || j<=0) return k;
ll &ret=dp[i][j];
if(ret!=-1) return ret;
ret=k;
for(int ii=i-1; ii>=0; ii--)
{
for(int jj=j-1; jj>=0; jj--)
{
if(A[jj] & (1<<ii))
{
if(!(k & (1<<ii)))
ret=max(ret,rec(ii,jj,A[jj]^k));
}
}
}
return ret;
}
int main()
{
int t,pos,k;
ll ans;
cin>>t;
while(t--)
{
cin>>n>>k;
for(int i=0; i<n; i++)
{
cin>>A[i];
}
sort(A,A+n);
memset(dp,-1,sizeof(dp));
ans=k;
for(int i=30; i>=0; i--)
{
for(int j=n-1; j>=0; j--)
{
if(A[j] & (1<<i))
{
if(!(k & (1<<i)))
{
ans=max(ans,rec(i,j,A[j]^k));
}
}
}
}
cout<<ans<<endl;
}
return 0;
}
