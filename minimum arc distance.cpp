#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

main()
{
    double res,oa,ob,ab,ax,ay,bx,by,ox,oy,theta;
    int n,i;
    cin>>n;
    for(i=1;i<=n;i++)
    {
        cin>>ox>>oy>>ax>>ay>>bx>>by;
        ab=sqrt( ((ax-bx)*(ax-bx)) + ((ay-by)*(ay-by)) );
        oa=sqrt( ((ax-ox)*(ax-ox)) + ((ay-oy)*(ay-oy)) );
        ob=sqrt( ((bx-ox)*(bx-ox)) + ((by-oy)*(by-oy)) );
        theta=acos(((oa*oa)+(ob*ob)-(ab*ab))/(2.0*oa*ob));
        res=theta*oa;
        printf("Case %d: %.6lf\n",i,res);
    }
    return 0;
}
