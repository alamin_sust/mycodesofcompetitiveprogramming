#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll m,n,p,t,arr[1010];

bool possibility(ll cap)
{
    ll k=0,filled=0;
    for(ll i=1;i<=n;i++)
    {
        if(filled+arr[i]>cap)
            k++,filled=0,i--;
        else
            filled+=arr[i];
        if(k==m)
            return false;
    }
    return true;
}

main()
{
ll i,low,high,mid,k;
cin>>t;
for(p=1;p<=t;p++)
{
    cin>>n>>m;
    for(i=1;i<=n;i++)
    {
        scanf(" %lld",&arr[i]);
    }
    low=0LL;
    high=1000000001LL;
    mid=(low+high)/2;
    k=35;
    while(k--)
    {
        if(possibility(mid))
            {if(mid>0&&!possibility(mid-1))
            break;
            high=mid;}
        else
            low=mid;
        mid=(low+high)/2;
    }
    printf("Case %lld: %lld\n",p,mid);
}
return 0;
}
