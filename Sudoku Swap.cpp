/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

vector<pair<int,int> >cood;
vector<int>res;
int flag[11],col[10][10],arr[10][10],r[10],c[10],b[10];

void checkbox(void)
{
    int i,j,fg=0,k,l,cnt=1;
    for(i=1; i<=9; i+=3)
    {
        for(j=1; j<=9; j+=3)
        {
            fg=0;
            memset(flag,0,sizeof(flag));
            for(k=i; k<i+3; k++)
            {
                for(l=j; l<j+3; l++)
                {
                    if(flag[arr[k][l]])
                    {
                        fg=1;
                        break;
                    }
                    flag[arr[k][l]]=1;
                }
            }
            if(fg==1)
            {
                b[cnt]=1;
                for(k=i; k<i+3; k++)
                {
                    for(l=j; l<j+3; l++)
                    {
                        col[k][l]=1;
                    }
                }
            }
            cnt++;
        }
    }
    return;
}

void checkrow(void)
{
    int i,j,fg=0,cnt=1;
    for(i=1; i<=9; i++)
    {
        fg=0;
        memset(flag,0,sizeof(flag));
        for(j=1; j<=9; j++)
        {
            if(flag[arr[i][j]])
            {
                fg=1;
                break;
            }
            flag[arr[i][j]]=1;
        }
        if(fg)
        {
            r[cnt]=1;
            for(j=1; j<=9; j++)
            {
                col[i][j]=1;
            }
        }
        cnt++;
    }
    return;
}

void checkcol(void)
{
    int i,j,fg=0,cnt=1;
    for(i=1; i<=9; i++)
    {
        fg=0;
        memset(flag,0,sizeof(flag));
        for(j=1; j<=9; j++)
        {
            if(flag[arr[j][i]])
            {
                fg=1;
                break;
            }
            flag[arr[j][i]]=1;

        }
        if(fg)
        {
            c[cnt]=1;
            for(j=1; j<=9; j++)
            {
                col[j][i]=1;
            }
        }
        cnt++;
    }
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    int t,p,i,j,no,k,q,pp,qq,P;
    scanf(" %d",&t);

    for(P=1; P<=t; P++)
    {
        memset(col,0,sizeof(col));
        memset(r,0,sizeof(r));
        memset(c,0,sizeof(c));
        memset(b,0,sizeof(b));
        for(i=1; i<=9; i++)
            for(j=1; j<=9; j++)
                scanf(" %d",&arr[i][j]);

        res.clear();
        checkbox();
        checkrow();
        checkcol();
        cood.clear();
        for(i=1;i<=9;i++)
        {
            for(j=1;j<=9;j++)
            {
                if(col[i][j])
                {
                   // printf("%d %d\n",i,j);
                    cood.push_back(make_pair(i,j));
                }

            }
            //printf("\n");
        }
        printf("Case #%d:\n",P);
        if(cood.size()==0)
        {
            printf("Serendipity\n");
            continue;
        }


        for(i=0;i<cood.size();i++)
        {
            for(j=i+1;j<cood.size();j++)
            {
                int xx1=cood[i].first;
                int yy1=cood[i].second;
                int xx2=cood[j].first;
                int yy2=cood[j].second;
                swap(arr[xx1][yy1],arr[xx2][yy2]);
                p=1;
                q=1;
                no=0;
                for(k=1;no==0&&k<=9;k++)
                {
                    if(b[k])
                    {
                        //printf("%d\n",k);
                        memset(flag,0,sizeof(flag));
                        for(pp=p;pp<p+3;pp++)
                        {
                            for(qq=q;qq<q+3;qq++)
                            {
                                if(flag[arr[pp][qq]])
                                {
                                    no=1;
                                    break;
                                }
                                flag[arr[pp][qq]]=1;
                            }
                        }
                    }
                    q+=3;
                    if(q>9)
                    {
                        q=1;
                        p+=3;
                    }
                }
                for(k=1;no==0&&k<=9;k++)
                {
                    if(r[k])
                    {
                        memset(flag,0,sizeof(flag));
                        for(p=1;p<=9;p++)
                        {
                            if(flag[arr[k][p]])
                            {
                                no=1;
                                break;
                            }
                            flag[arr[k][p]]=1;
                        }
                    }
                }
                for(k=1;no==0&&k<=9;k++)
                {
                    if(c[k])
                    {
                        memset(flag,0,sizeof(flag));
                        for(p=1;p<=9;p++)
                        {
                            if(flag[arr[p][k]])
                            {
                                no=1;
                                break;
                            }
                            flag[arr[p][k]]=1;
                        }
                    }
                }

                memset(flag,0,sizeof(flag));
                for(k=1;k<=9;k++)
                {
                    if(flag[arr[xx1][k]])
                    {
                        no=1;
                        break;
                    }
                    flag[arr[xx1][k]]=1;
                }

                memset(flag,0,sizeof(flag));
                for(k=1;k<=9;k++)
                {
                    if(flag[arr[xx2][k]])
                    {
                        no=1;
                        break;
                    }
                    flag[arr[xx2][k]]=1;
                }

                memset(flag,0,sizeof(flag));
                for(k=1;k<=9;k++)
                {
                    if(flag[arr[k][yy1]])
                    {
                        no=1;
                        break;
                    }
                    flag[arr[k][yy1]]=1;
                }

                memset(flag,0,sizeof(flag));
                for(k=1;k<=9;k++)
                {
                    if(flag[arr[k][yy2]])
                    {
                        no=1;
                        break;
                    }
                    flag[arr[k][yy2]]=1;
                }

                if(no==0)
                {
                    res.push_back(xx1);
                    res.push_back(yy1);
                    res.push_back(xx2);
                    res.push_back(yy2);
                }
                swap(arr[xx1][yy1],arr[xx2][yy2]);
            }
        }
        for(i=0;i<res.size();i+=4)
        {
            printf("(%d,%d) <-> (%d,%d)\n",res[i],res[i+1],res[i+2],res[i+3]);
        }
    }


    return 0;
}


