#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define mod 1000007
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

priority_queue<ll>pq;
ll k,c,n,a[100010],i,ans,ca[100010],rca[100010],t,p,res[100010];

main()
{
    cin>>t;
    for(p=1LL;p<=t;p++)
    {
        cin>>k>>c>>n>>a[0];
        for(i=1;i<n;i++)
        {
            a[i]=(k*a[i-1]+c)%mod;
            pq.push(999999LL-a[i-1]);
        }
        pq.push(999999LL-a[n-1]);
        i=0LL;
        while(!pq.empty())
        {
            a[i++]=999999LL-pq.top();
            pq.pop();
        }
        //a[0]=1;a[1]=7;a[2]=11,a[3]=14,a[4]=21;
        ca[0]=a[0];
        for(i=1LL;i<n;i++)
        {
            ca[i]=ca[i-1]+a[i];
          //  printf("%lld..\n",ca[i]);
        }
        rca[n-1]=a[n-1];
        for(i=n-2LL;i>=0LL;i--)
        {
            rca[i]=rca[i+1]+a[i];
          //  printf("%lld..\n",rca[i]);
        }
        res[0]=rca[0]-(a[0]*n);
        ans=res[0];
        //cout<<res[0]<<endl;
        for(i=1;i<n;i++)
        {
            res[i]=res[i-1]-(((ca[i]-ca[i-1])-(rca[i-1]-rca[i]))*(n-(2LL*i)));
            ans+=res[i];
        }
        printf("Case %lld: %lld\n",p,ans/2LL);
    }
    return 0;
}
