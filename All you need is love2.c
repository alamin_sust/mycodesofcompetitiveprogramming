#include<stdio.h>
#include<string.h>
#include<math.h>
char arr1[10010],arr2[10010];
long long int i,j,l,n,res1,res2,k;

int gcd(int a,int b)
{
    if(b==0) return a;
    else gcd(b,a%b);
}

main()
{
     scanf(" %lld",&n);
     getchar();
     for(i=1;i<=n;i++)
     {
         gets(arr1);
         gets(arr2);
         l=strlen(arr1);
         for(res1=0,j=l-1,k=1;j>=0;j--,k*=2)
         {
             res1+=(arr1[j]-'0')*k;
         }
         l=strlen(arr2);
         for(res2=0,j=l-1,k=1;j>=0;j--,k*=2)
         {
             res2+=(arr2[j]-'0')*k;
         }
         if(gcd(res1,res2)!=1)
         {
             printf("Pair #%lld: All you need is love!\n",i);
         }
         else
         {
             printf("Pair #%lld: Love is not all you need!\n",i);
         }
     }
    return 0;
}
