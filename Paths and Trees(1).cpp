/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node{
ll from,to,val;
};

queue<pair<ll,ll> >q;
vector<ll>adj[300010],cost[300010];
vector<node>adj_list;
ll stat[300010],n,m;
map<pair<ll,ll>,ll>mpp,mpp2;

ll bfs(ll src,ll mxval)
{
  //  printf("%lld\n",mxval);
    while(!q.empty())
        q.pop();
    memset(stat,0,sizeof(stat));
    q.push(make_pair(src,0LL));
    stat[src]=1LL;
    ll cnt=1LL;
    while(!q.empty())
    {
        pair<ll,ll>u=q.front();
        q.pop();
        for(ll i=0LL;i<adj[u.first].size();i++)
        {
            pair<ll,ll>v=make_pair(adj[u.first][i],cost[u.first][i]+u.second);
          //  if(mxval==2)
            //    {
             //       printf("....%lld %lld %lld %lld\n",u.first,v.first,v.second,cnt);
              //  }
            if(stat[v.first]==0&&v.second<=mxval)
            {

                stat[v.first]=1;
                cnt++;
                q.push(v);
            }
        }
    }
    if(cnt==n)
    return 1;
    return 0;
}

ll construct(ll src,ll mxval)
{
    while(!q.empty())
        q.pop();
    memset(stat,0,sizeof(stat));
    q.push(make_pair(src,0LL));
    stat[src]=1LL;
    ll cnt=1LL;
    while(!q.empty())
    {
        pair<ll,ll>u=q.front();
        q.pop();
        for(ll i=0;i<adj[u.first].size();i++)
        {
            pair<ll,ll>v=make_pair(adj[u.first][i],cost[u.first][i]+u.second);
            if(mpp[make_pair(u.first,v.first)]==0&&mpp[make_pair(v.first,u.first)]==0&&v.second<=mxval)
            {
                node det;
                det.from=u.first;
                det.to=v.first;
                det.val=cost[u.first][i];
                mpp[make_pair(u.first,v.first)]=1;
                adj_list.push_back(det);

                stat[v.second]=1;
                cnt++;
                q.push(v);
            }
        }
    }
    if(cnt==n)
    return 1;
    return 0;
}


int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    ll from,to,val,mid,low,high,u,res;
    cin>>n>>m;

    for(ll i=1LL;i<=m;i++)
    {
        scanf(" %I64d %I64d %I64d",&from,&to,&val);
        mpp2[make_pair(from,to)]=i;
        mpp2[make_pair(to,from)]=i;
        adj[from].push_back(to);
        adj[to].push_back(from);
        cost[from].push_back(val);
        cost[to].push_back(val);
    }

    cin>>u;
    low=0LL;
    high=1000000000000000LL;
    res=high;
    while(low<=high)
    {
        mid=(low+high)/2LL;
        ll ret=bfs(u,mid);
        if(ret)
        {
            res=min(res,mid);
            high=mid-1;
        }
        else
        {
            low=mid+1;
        }
    }

    construct(u,res);
    //cout<<res<<endl;
    //printf("%d---\n",adj[4LL].size());

   // for(ll i=0;i<adj_list.size();i++)
   // {
   //      cout<<adj_list[i].from<<" "<<adj_list[i].to<<" "<<adj_list[i].val<<endl;
   // }




    return 0;
}


