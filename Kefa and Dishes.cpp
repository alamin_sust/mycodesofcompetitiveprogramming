/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll m,n,adj[110][110],arr[110],k,dp[(1<<18)+10][20];

ll rec(ll mask,ll now)
{
    if(__builtin_popcount(mask)==m)
        return 0LL;
    if(__builtin_popcount(mask)>m)
        return -999999999999LL;
    ll &ret=dp[mask][now];
    if(ret!=-1LL)
        return ret;
    ret=-999999999999LL;
    //ll taken=__builtin_popcount(mask);
    for(ll i=0LL;i<n;i++)
    {
        if(((1LL<<i)&mask)==0LL)
        {
            ret=max(ret,rec((1LL<<i)|mask,i+1LL)+adj[now][i+1]+arr[i+1]);
        }
    }
    return ret;
}

int main()
{
    ll i,a,c,b;

    scanf(" %I64d %I64d %I64d",&n,&m,&k);

    for(i=1LL;i<=n;i++)
    {
        scanf(" %I64d",&arr[i]);
    }

    for(i=1LL;i<=k;i++)
    {
        scanf(" %I64d %I64d %I64d",&a,&b,&c);
        adj[a][b]=c;
    }
    memset(dp,-1,sizeof(dp));
    printf("%I64d\n",rec(0LL,0LL));


    return 0;
}
