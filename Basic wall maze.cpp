#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int rr[]={1,-1,0,0};
int cc[]={0,0,1,-1};

struct node{
int x,y;
};

node s,e,from,to;
queue<node>que;
int stat[10][10],val[10][10],adj[10][10][10][10];

void bfs(void)
{
    memset(val,0,sizeof(val));
    memset(stat,0,sizeof(stat));
    que.push(e);
    stat[e.x][e.y]=1;
    val[e.x][e.y]=0;
    while(!que.empty())
    {
        node from;
        from.x=que.front().x;
        from.y=que.front().y;
        que.pop();
        for(int i=0;i<4;i++)
        {
            to.x=from.x+rr[i];
            to.y=from.y+cc[i];
            if(adj[from.x][from.y][to.x][to.y]==1&&stat[to.x][to.y]==0)
            {
                stat[to.x][to.y]=1;
                val[to.x][to.y]=val[from.x][from.y]+1;
                que.push(to);
            }
        }
    }
    return;
}

main()
{
    int i,j,k,a,b,c,d;
    while(cin>>s.y>>s.x&&s.x&&s.y)
    {
        cin>>e.y>>e.x;
        for(i=1;i<=6;i++)
        {
            for(j=1;j<=6;j++)
            {
               for(k=0;k<4;k++)
               {
                   if(i+rr[k]>0&&i+rr[k]<7&&j+cc[k]>0&&j+cc[k]<7)
                   {
                       adj[i][j][i+rr[k]][j+cc[k]]=1;
                       adj[i+rr[k]][j+cc[k]][i][j]=1;
                   }
               }
            }
        }
        for(i=0;i<3;i++)
        {
            cin>>b>>a>>d>>c;
            if(b==d)
            for(j=a+1;j<=c;j++)
            {
                for(k=b;k<=d;k++)
                {
                   adj[j][k][j][k+1]=0;
                   adj[j][k+1][j][k]=0;
                }
            }
            else
            for(j=b+1;j<=d;j++)
            {
                for(k=a;k<=c;k++)
                {
                   adj[k][j][k+1][j]=0;
                   adj[k+1][j][k][j]=0;
                }
            }
        }
        bfs();
        while(1)
        {
            if(val[s.x][s.y]==val[s.x-1][s.y]+1&&s.x>1)
            {
                printf("N");
                s.x--;
            }
            else if(val[s.x][s.y]==val[s.x+1][s.y]+1&&s.x<6)
            {
                printf("S");
                s.x++;
            }
            else if(val[s.x][s.y]==val[s.x][s.y-1]+1&&s.y>1)
            {
                printf("W");
                s.y--;
            }
            else if(val[s.x][s.y]==val[s.x][s.y+1]+1&&s.y<6)
            {
                printf("E");
                s.y++;
            }
            if(s.x==e.x&&s.y==e.y)
            {
                printf("\n");
                break;
            }
        }
    }
    return 0;
}

