/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<bits/stdc++.h>
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.0000000000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

map< , ll>mpp;
map< pair<double,double>, ll>mpp2;
ll res,i,n,p,t;
double a,b,c;

ll _gcd(ll a,ll b)
{
    if(b==0)
        return a;
    return _gcd(b,a%b);
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %lld",&t);

    for(p=1; p<=t; p++)
    {
        mpp.clear();
        mpp2.clear();
        scanf(" %lld",&n);
        for(i=1; i<=n; i++)
        {
            scanf(" %lf %lf %lf",&a,&b,&c);
            if(mpp2[make_pair(b/a,c/a)]==0)
            {
                mpp[b/a]++;
                mpp[b/a+eps]++;
                mpp[b/a-eps]++;
                mpp2[make_pair(b/a,c/a)]=1;
                mpp2[make_pair(b/a+eps,c/a+eps)]=1;
                mpp2[make_pair(b/a-eps,c/a+eps)]=1;
                mpp2[make_pair(b/a+eps,c/a-eps)]=1;
                mpp2[make_pair(b/a-eps,c/a-eps)]=1;
            }
        }
        res=0;
        for (auto x: mpp)
        {
            res=max(res,x.second);
        }
        printf("%lld\n",res);
    }

    return 0;
}
