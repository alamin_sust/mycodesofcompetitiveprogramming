#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class SwappingDigits
{
public:
	string minNumber(string num)
	{
	    string res;
	    int det,j,i,l;
	    l=num.size();
	    for(i=0;i<l;i++)
	    {
	        res[i]=num[i];
	    }
	    for(i=0;i<l;i++)
	    {
	        for(j=1;j<l;j++)
	        {
	            if(res[j]<res[j-1])
	            {
	                swap(res[j],res[j-1]);
	            }
	        }
	    }
	    if(res[0]=='0')
	    {
	        for(i=1;i<l;i++)
	        {
	            if(res[i]!='0')
	            {
	                det=i;
	                break;
	            }
	        }
	        swap(res[0],res[i]);
	    }
	    return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	SwappingDigits objectSwappingDigits;

	//test case0
	string param00 = "596";
	string ret0 = objectSwappingDigits.minNumber(param00);
	string need0 = "569";
	assert_eq(0,ret0,need0);

	//test case1
	string param10 = "93561";
	string ret1 = objectSwappingDigits.minNumber(param10);
	string need1 = "13569";
	assert_eq(1,ret1,need1);

	//test case2
	string param20 = "5491727514";
	string ret2 = objectSwappingDigits.minNumber(param20);
	string need2 = "1491727554";
	assert_eq(2,ret2,need2);

	//test case3
	string param30 = "10234";
	string ret3 = objectSwappingDigits.minNumber(param30);
	string need3 = "10234";
	assert_eq(3,ret3,need3);

	//test case4
	string param40 = "93218910471211292416";
	string ret4 = objectSwappingDigits.minNumber(param40);
	string need4 = "13218910471211292496";
	assert_eq(4,ret4,need4);

}
