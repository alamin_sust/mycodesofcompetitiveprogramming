#include <bits/stdc++.h>

pair<int, int> p;
p = make_pair(3, 4);

vector<int> v;
v = {1, 2, 5, 2};
for (auto i: v)
    cout << i << ' ';

 __builtin_ffs(10) = 2  // last 1 bit from right

 __builtin_clz(16) = 27  //32 � 5 = 27

 __builtin_ctz(16) = 4  //Number of trailing 0-bits is 4

__builtin_popcount(14) = 3

set<int> s = {8, 2, 3, 1};
for (set<int>::iterator it = s.begin(); it != s.end(); ++it)
    cout << *it << ' ';
// prints "1 2 3 8"

set<int> s = {8, 2, 3, 1};
for (auto it: s)
    cout << it << ' ';
// prints "1 2 3 8"

vector<int> v = {8, 2, 3, 1};
for (auto &it: v)
    it *= 2;
for (auto it: v)
    cout << it << ' ';
// prints "16 4 6 2"

for(i = 1; i <= n; i++)
    for(j = 1; j <= m; j++)
        cout << a[i][j] << " \n"[j == m];
