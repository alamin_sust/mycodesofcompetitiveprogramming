#include<cstdio>
#include<iostream>
#include<utility>
#include<algorithm>
using namespace std;

int print_longestlength(int arr[],int n)
{
int max=0,i,pos=1;
for(i=1;i<=n;i++)
{
if(arr[i]>max)
{
max=arr[i];
pos=i;
}
}
cout<<max<<endl;
return(pos);
}

void print_subsequence(int arr[],int pos,int index[])
{
int sequence[1005]={0};
int i=2;
sequence[1]=pos;
while(arr[pos]!=0)
{
sequence[i]=arr[pos];
i++;
pos=arr[pos];
}

for(int j=i-1;j>1;j--)
cout<<index[sequence[j]]<<endl;
cout<<index[sequence[1]];
}

void longest_sequence(int wt[],int iq[],int n,int index[])
{
int dp[1005]={0};
int predecessor[1005]={0};
int i,j,pos=0,max,position;
dp[1]=1;
for(i=2;i<n;i++)
{
max=0,pos=0;
for(j=i-1;j>0;j--)
{
if(wt[j]<wt[i] && iq[j]>iq[i])
{
if(max<dp[j])
{
pos=j;
max=dp[j];
}
}
}
dp[i]=max+1;
predecessor[i]=pos;
}
position=print_longestlength(dp,n);
print_subsequence(predecessor,position,index);
}

void swap(int *a,int *b)
{
int temp;
temp=*a;
*a=*b;
*b=temp;
}

void insertion_sort(int wt[],int iq[],int index[],int n)
{
int i,j;
for(i=2;i<n;i++)
{
j=i;
while((j>1) && (wt[j]<wt[j-1]))
{
swap(&wt[j],&wt[j-1]);
swap(&iq[j],&iq[j-1]);
swap(&index[j],&index[j-1]);
j-=1;
}
}
}
int main()
{
int i=1,k;
int weight[1005],iq[1005];
pair<int,int>data[1005];
int index[1005]={0};
while(cin>>weight[i]>>iq[i])
{
index[i]=i;
i++;
}
if(i==1)
cout<<"";
else
{
insertion_sort(weight,iq,index,i);

longest_sequence(weight,iq,i,index);
}
return 0;
}
