#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int dp[1<<13+5];

int rec(int mask)
{
    int &ret=dp[mask];
    if(ret!=-1)
     return ret;
    ret=0;
    for(int i=0;i<10;i++)
    {

        if(((1<<i)&mask)==0&&((1<<(i+1))&mask)!=0&&((1<<(i+2))&mask)!=0)
            {
                //printf("..");
                int tempmask=(1<<i)|mask;
                tempmask=(1<<(i+1))^tempmask;
                tempmask=(1<<(i+2))^tempmask;
                ret=max(ret,1+rec(tempmask));
            }
        if(((1<<i)&mask)!=0&&((1<<(i+1))&mask)!=0&&((1<<(i+2))&mask)==0)
            {
                int tempmask=(1<<(i+2))|mask;
                tempmask=(1<<i)^tempmask;
                tempmask=(1<<(i+1))^tempmask;
                ret=max(ret,1+rec(tempmask));
            }
    }
    return ret;
}

main()
{
    int n,i,mask,k,p;
    char arr[15];
    cin>>n;
    for(p=1;p<=n;p++)
    {
        scanf(" %s",&arr);
        k=0;
        memset(dp,-1,sizeof(dp));
        mask=0;
        for(i=0;i<12;i++)
        {
            mask<<=1;
            if(arr[i]=='o')
                {k++;
                mask++;
                }
        }
        //printf("%d\n",mask);
        printf("%d\n",k-rec(mask));
    }
    return 0;
}

/*
5
---oo-------
-o--o-oo----
-o----ooo---
oooooooooooo
oooooooooo-o
*/
