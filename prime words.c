#include<stdio.h>
#include<string.h>
#define MAX 12000
main()
{int i, j, sieve[MAX], in, l, res;
char arr[80];
for(i=0; i<MAX; i++){
        sieve[i]=1;
}
sieve[0]=sieve[1]=0;
for(i=2; i<MAX; i++){
        while(sieve[i]==0 && i<MAX)
        {i++;}
        for(j=i*i; j<MAX; j+=i)
        {sieve[j]=0;}
}
sieve[1]=1;
    while(gets(arr))
    {
        l=strlen(arr);
        for(res=0,i=0;i<l;i++)
        {
            if(arr[i]>='A'&&arr[i]<='Z')
            res+=arr[i]-'A'+27;
            if(arr[i]>='a'&&arr[i]<='z')
            res+=arr[i]-'a'+1;
        }
        if(sieve[res]==1)
        printf("It is a prime word.\n");
        else
        printf("It is not a prime word.\n");
    }
    return 0;
}
