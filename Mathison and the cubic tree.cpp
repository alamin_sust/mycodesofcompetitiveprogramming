/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
#define MAX 1000010
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int col[310],res,arr[310];

vector<pair<int,int> >v[310];
vector<int>adj[310];

int sieve[MAX+10],pc=0,prime[MAX+10],n;

void sieve_(void)
{
    sieve[0]=sieve[1]=1;
    for(int i=2; i<=MAX; i++)
    {
        if(sieve[i]==0)
        {
            prime[pc++]=i;
            for(int j=2; i*j<=MAX; j++)
                sieve[i*j]=1;
        }
    }
    return;
}

void rec(int node,vector<pair<int,int> > vv, int len)
{
    col[node]=1;
    if(vv.size()==0)res=max(res,len);

    for(int i=0; i<adj[node].size(); i++)
    {
        int to=adj[node][i];
        if(col[to])continue;

        vector<pair<int,int> >v2;
        v2.clear();
        vector<int>v3;
        v3.clear();

        map<int,int>mpp;
        mpp.clear();

        for(int j=0; j<v[to].size(); j++)
        {
            mpp[v[to][j].first]=v[to][j].second;
            v3.push_back(v[to][j].first);
        }

        for(int j=0; j<vv.size(); j++)
        {
            if(mpp[vv[j].first]==0)v3.push_back(vv[j].first);
            mpp[vv[j].first]+=vv[j].second;
        }


        for(int j=0; j<v3.size(); j++)
        {
            if(mpp[v3[j]]%3)
            v2.push_back(make_pair(v3[j],mpp[v3[j]]));
        }
        rec(to,v2,len+1);
    }
    return;
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    sieve_();

    int t;

    scanf(" %d",&t);

    for(int p=1; p<=t; p++)
    {
        scanf(" %d",&n);
        for(int i=1; i<=n; i++)
        {
            adj[i].clear();
            scanf(" %d",&arr[i]);
            v[i].clear();
            for(int j=0; arr[i]!=1&&j<pc; j++)
            {
                int cnt=0;
                while((arr[i]%prime[j])==0)
                {
                    cnt++;
                    arr[i]/=prime[j];
                }
                cnt%=3;
                if(cnt) v[i].push_back(make_pair(j,cnt));
            }
        }

        for(int i=1; i<n; i++)
        {
            int from,to;
            scanf(" %d %d",&from,&to);
            adj[from].push_back(to);
            adj[to].push_back(from);
        }

        res=-1;
        for(int i=1; i<=n; i++)
        {
            memset(col,0,sizeof(col));
            //for(int j=0;j<v[i].size();j++)printf("%d.%d ",prime[v[i][j].first],v[i][j].second);
            //printf("\n");
            rec(i,v[i],1);
        }
        printf("%d\n",res);
    }

    return 0;
}
























