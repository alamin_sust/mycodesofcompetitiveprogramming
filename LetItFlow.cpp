#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define LEFT 0
#define RIGHT 1
#define TOP 2
#define BOTTOM 3
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int dp[4][1003][5], M=1000000007, t, p, c;
char arr[8][1015];


int rec(int row, int col, int dir) {
    if(col == c) {
        if(dir == LEFT && row == 2) {
            return dp[row][col][dir] = 1;
        }
        return dp[row][col][dir] = 0;
    }
    if (arr[row][col] == '#') {
        return dp[row][col][dir] = 0;
    }
    int &ret = dp[row][col][dir];
    if(ret!=-1) return ret;
    ret = 0;

    if(row == 0) {
        if(dir==LEFT) ret = (ret + rec(row+1,col,TOP)) % M;
        else if(dir==BOTTOM) ret = (ret + rec(row,col+1,LEFT)) % M;
    } else if(row == 1) {
        if(dir==LEFT) {
            ret = (ret + rec(row-1,col,BOTTOM)) % M;
            ret = (ret + rec(row+1,col,TOP)) % M;
        }
        else if(dir==TOP) {
            ret = (ret + rec(row,col+1,LEFT)) % M;
        }
        else if(dir==BOTTOM) {
            ret = (ret + rec(row,col+1,LEFT)) % M;
        }
    } else {
        if(dir==LEFT) ret = (ret + rec(row-1,col,BOTTOM)) % M;
        else if(dir==TOP) ret = (ret + rec(row,col+1,LEFT)) % M;
    }

    return ret;
}

main()
{
    freopen("let_it_flow.txt","r",stdin);
    freopen("let_it_flow_output.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++) {
        scanf(" %d", &c);
        for(int i=0;i<3;i++) {
            scanf(" %s", arr[i]);
        }
        memset(dp,-1,sizeof(dp));
        printf("Case #%d: %d\n",p,rec(0,0,LEFT));
    }

    return 0;
}

