/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll x,y,n,i,segs;
char arr[300010];

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);


    scanf(" %I64d %I64d %I64d",&n,&x,&y);

    scanf(" %s",arr);

    ll segs=0LL;
    for(i=0LL;i<n;) {
        if(arr[i]=='0') {
            segs++;
            while(i<n && arr[i]=='0')
                i++;
        } else i++;
    }

    if(segs==0LL) {
        printf("0\n");
        return 0;
    }

    if(x<y) {
        printf("%I64d\n", (x*(segs-1LL))+y);
    } else {
        printf("%I64d\n", y*segs);
    }

    return 0;
}
