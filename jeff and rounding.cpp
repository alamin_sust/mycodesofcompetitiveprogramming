#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

double in;
int n,zero,nonzero,i,res,temp,mx,mn,k,now;

main()
{
    cin>>n;
    n*=2;
    for(res=0,zero=0,nonzero=0,i=1;i<=n;i++)
    {
        cin>>in;
        in=(in+eps)*1000;
        temp=int(in)%1000;
        res+=temp;
        if(temp==0)
            zero++;
        else
            nonzero++;
    }
    mx=min(nonzero,n/2);
    mn=(n-(zero*2))/2;
    now=inf;
    for(i=mn;i<=mx;i++)
    {
        k=i*1000-res;
        if(k<0)
            k=-k;
        now=min(now,k);
    }
    in=(double)now/1000.0;
    printf("%.3lf\n",in);
    return 0;
}

