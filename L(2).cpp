/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
vector<int>A;
vector<int>B;

main()
{
    int a,b,tmp;
    while(cin>>a>>b&&a+b)
    {
        A.clear();
        B.clear();
        for(int i=0; i<a; i++)
        {
            cin>>tmp;
            A.push_back(tmp);
        }
        for(int i=0; i<b; i++)
        {
            cin>>tmp;
            B.push_back(tmp);
        }
        sort(A.begin(),A.end());
        sort(B.begin(),B.end());
        if(A[0]>=B[1]||(A[0]>=B[0]&&A[0]>=B[1]))cout<<'N'<<endl;
        else cout<<'Y'<<endl;
    }
    return 0;
}
