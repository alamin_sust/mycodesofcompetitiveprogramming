/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

char arr[100010];
int m,i,p,k,tp,res,a,b,j,l;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d %d",&l,&m);
    scanf(" %s",arr);
    for(p=0;p<m;p++)
    {
        scanf(" %d %d",&a,&b);
        a--;
        b--;
        if(a>b)
            swap(a,b);
        a=max(0,a);
        b=min(l-1,b);
        res=tp=0;
       // for(i=0;i<l;i++)
        {
            i=a;
             tp=0,k=a;
             for(j=i;j<l;j++)
             {
                 if(arr[j]!=arr[k])
                 break;
                 if(k==b)
                    k=a,tp++;
                 else
                    k++;
             }
             res=max(res,tp);
        //
        }
        printf("%d\n",res);
    }

    return 0;
}


