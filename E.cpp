#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long int
#define inf 100000000000000010LL
using namespace std;

struct node{
ll from,to,type;
};
node arr[5010];

ll dp[5010][5],c,c1,c2,n,r;

ll comp(node a,node b)
{
    return a.to<b.to;
}


ll rec(ll pos,ll ant)
{
    if(pos==n)
        return 0LL;
    ll &ret=dp[pos][ant];
    if(ret!=-1LL)
        return ret;
    ret=inf;
    ll f1=0LL,f2=0LL;
    for(ll i=pos;i<n;i++)
    {
        if(arr[i].type==1LL)
            f2=1LL;
        if(arr[i].type==2LL)
            f1=1LL;
        if((arr[i].from)<=(r+arr[pos].to))
        {
            ret=min(ret,rec(i+1LL,3LL)+c);
            if(f2==0LL)
            {
                ret=min(ret,rec(i+1LL,2LL)+c2);
            }
            if(f1==0LL)
            {
                ret=min(ret,rec(i+1LL,1LL)+c1);
            }
        }
    }
    return ret;
}

int main()
{
    while(scanf("%lld%lld%lld%lld%lld",&n,&r,&c1,&c2,&c)!=EOF)
    {
        r*=2LL;
        if(n==0LL&&r==0LL&&c1==0LL&&c2==0LL&&c==0LL)
            break;
        for(ll i=0LL;i<n;i++)
        {
            scanf(" %lld %lld %lld",&arr[i].from,&arr[i].to,&arr[i].type);
        }
        sort(arr,arr+n,comp);
        memset(dp,-1LL,sizeof(dp));
        printf("%lld\n",rec(0LL,0LL));
    }
    return 0;
}
