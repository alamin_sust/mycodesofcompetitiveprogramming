#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
#define MAX 110000
long long int i, j, sieve[MAX], primecount=0, prime[MAX],r,c, in,n,k,det[510][510],arr[510][510];

long long int func1(long long int x)
{
    long long int ret=inf;
    for(long long int i=x;;i++)
    {
        if(sieve[i]==1)
            {ret=i-x;
            break;}
    }
    return ret;
}

main()
{
    for(i=0; i<MAX; i++)
    {
        sieve[i]=1;
    }
    sieve[0]=sieve[1]=0;
    for(i=2; i<MAX; i++)
    {
        while(sieve[i]==0 && i<MAX)
        {
            i++;
        }
        prime[primecount]=i;
        for(j=i*i; j<MAX; j+=i)
        {
            sieve[j]=0;
        }
        primecount++;
    }
    cin>>r>>c;
    for(i=1;i<=r;i++)
        for(j=1;j<=c;j++)
        cin>>arr[i][j];
    for(i=1;i<=r;i++)
    {
        for(j=1;j<=c;j++)
        {
            det[i][j]=func1(arr[i][j]);
           // cout<<det[i][j]<<" ";
        }
        //cout<<"\n";
    }
    long long res=inf;
    for(i=1;i<=r;i++)
    {
        long long k=0;
        for(j=1;j<=c;j++)
            k+=det[i][j];
        res=min(res,k);
    }
    for(i=1;i<=c;i++)
    {
        long long k=0;
        for(j=1;j<=r;j++)
            k+=det[j][i];
        res=min(res,k);
    }
    printf("%I64d\n",res);
    return 0;
}

