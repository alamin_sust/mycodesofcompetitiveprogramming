/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int n,m,stk[1010],arr[1010],pos[1010],res,j,tp_stkj,tp_stkj1,tp_pos11,tp_pos22,i,now,arr2[1010],flag[1010];

int main()
{
    cin>>n>>m;
    for(i=1;i<=n;i++)
    {
        scanf(" %d",&arr[i]);
    }
    now=0;
    for(i=1;i<=m;i++)
    {
        scanf(" %d",&arr2[i]);
        if(flag[arr2[i]]==0)
        {
            flag[arr2[i]]=1;
            stk[++now]=arr2[i];
            pos[arr2[i]]=now;
        }
    }
    res=0;
    for(i=1;i<=m;i++)
    {
         for(j=pos[arr2[i]]-1;j>=1;j--)
         {
             res+=arr[stk[j]];
             tp_stkj=stk[j];
             tp_stkj1=stk[j+1];

             tp_pos11=pos[stk[j]];
             tp_pos22=pos[stk[j+1]];

             stk[j]=tp_stkj1;
             stk[j+1]=tp_stkj;

             pos[tp_stkj]=tp_pos22;
             pos[tp_stkj1]=tp_pos11;
         }
    }
    printf("%d\n",res);
    return 0;
}
