/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node{
ll mn,mx;
};

node arr[10];

double dp[6][202],price[5];
ll l,n;

double rec(ll color,ll len)
{
    //printf("%d %d..\n",color,len);
    if(color==n)
    {
        if(len==l)
        return 0.0;
        return 10000000000.1;
    }
    if(len==l)
        return 10000000000.1;
    double &ret=dp[color][len];
    if(ret>=-0.5)
        return ret;
    ret=10000000000.1;
    for(ll i=arr[color].mn;i<=arr[color].mx&&(i+len)<=l;i++)
    {
        ret=min(ret,(price[color]*(double)i*(double)i)+rec(color+1LL,len+i));
    }
    return ret;
}

void mems(void)
{
    for(ll i=0LL;i<6LL;i++)
    {
        for(ll j=0LL;j<202LL;j++)
        {
            dp[i][j]=-1.0;
        }
    }
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    while(cin>>n>>l)
    {
        for(ll i=0LL;i<n;i++)
            scanf(" %lf",&price[i]);
        for(ll i=0LL;i<n;i++)
            scanf(" %lld %lld",&arr[i].mn,&arr[i].mx);

        double res=10000000000.1;

        do{
            mems();
            res=min(res,rec(0LL,0LL));
        }
        while(next_permutation(price,price+n));

        if(res>=10000000000.0)
        printf("Impossible\n");
        else
        printf("%.3lf\n",res);
    }
    return 0;
}





