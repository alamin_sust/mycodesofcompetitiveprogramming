#include<stdio.h>

int main()
{
       int N,i;

       while(scanf("%d",&N)!=EOF)
       {
               unsigned long long fact = 1;

               for( i=1; i<=N; i++)
               {
                       fact *= i;

                       while(fact%10==0)
                               fact /= 10;

                       fact %= 1000000;
               }
             printf("%5d -> %d\n",N,fact%10);
        }
       return 0;
}
