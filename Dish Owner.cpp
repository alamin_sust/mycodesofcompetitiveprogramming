/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int t,i,q,com,x,y,mx[10010],p,n,par[10010];

int parent_find(int x)
{
    if(par[x]==x)
        return x;
    return par[x]=parent_find(par[x]);
}

int join_set(int x,int y)
{
    return par[parent_find(x)]=parent_find(y);
}

main()
{
    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        for(i=1;i<=n;i++)
        {
            par[i]=i;
            scanf(" %d",&mx[i]);
        }
        scanf(" %d",&q);
        for(i=1;i<=q;i++)
        {
            scanf(" %d %d",&com,&x);
            x=parent_find(x);
            if(com==1)
            {
                printf("%d\n",x);
            }
            else
            {
                scanf(" %d",&y);
                y=parent_find(y);
                if(x==y)
                {
                    printf("Invalid query!\n");
                }
                else if(mx[y]>mx[x])
                {
                    join_set(x,y);
                    mx[x]=mx[y];
                }
                else if(mx[x]>mx[y])
                {
                    join_set(y,x);
                    mx[y]=mx[x];
                }
            }
        }
    }
    return 0;
}
