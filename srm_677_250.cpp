#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int sieve[1015],arr[1015];

void sieve_(void)
{
    sieve[0]=sieve[1]=1;
    for(int i=2;i<=1010;i++)
    {
        if(sieve[i]==0)
        {
            for(int j=2;i*j<=1010;j++)
                sieve[i*j]=1;
        }
    }
    return;
}


class PalindromePrime
{
public:
	int count(int L, int R)
	{
	    for(int i=0;i<=1010;i++)
            sieve[i]=0;
        sieve_();
        int res=0,k,num,fg;
        for(int i=L;i<=R;i++)
        {
            if(sieve[i]==0)
            {
                k=0;
                num=i;
                while(num)
                {
                    arr[k++]=num%10;
                    num/=10;
                }
                k--;
                fg=1;
                for(int j=0;j<k;j++,k--)
                {
                    if(arr[j]!=arr[k])
                    {
                     fg=0;
                     break;
                    }
                }
                res+=fg;
            }
        }
        return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	PalindromePrime objectPalindromePrime;

	//test case0
	int param00 = 100;
	int param01 = 150;
	int ret0 = objectPalindromePrime.count(param00,param01);
	int need0 = 2;
	assert_eq(0,ret0,need0);

	//test case1
	int param10 = 1;
	int param11 = 9;
	int ret1 = objectPalindromePrime.count(param10,param11);
	int need1 = 4;
	assert_eq(1,ret1,need1);

	//test case2
	int param20 = 929;
	int param21 = 929;
	int ret2 = objectPalindromePrime.count(param20,param21);
	int need2 = 1;
	assert_eq(2,ret2,need2);

	//test case3
	int param30 = 1;
	int param31 = 1;
	int ret3 = objectPalindromePrime.count(param30,param31);
	int need3 = 0;
	assert_eq(3,ret3,need3);

	//test case4
	int param40 = 1;
	int param41 = 1000;
	int ret4 = objectPalindromePrime.count(param40,param41);
	int need4 = 20;
	assert_eq(4,ret4,need4);

}

