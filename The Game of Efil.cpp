/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int rr[]={0,0,1,-1,1,-1,-1,1};
int cc[]={-1,1,0,0,1,-1,1,-1};

int n,m,res,arr[110][110];

int neigh(int x,int y)
{
    int ret=0;
    for(int i=0;i<8;i++)
    {
        int u=x+rr[i];
        int v=y+cc[i];
        if(arr[u][v])
            ret++;
    }
    return ret;

}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    int i,j,k,p=1,from,to;
    while(scanf("%d%d",&n,&m)!=EOF)
    {
        if(n==0&&m==0)
            break;
        memset(arr,0,sizeof(arr));
        scanf(" %d",&k);
        for(i=0;i<k;i++)
        {
             scanf(" %d %d",&from,&to);
             arr[from][to]=1;
        }
        for(i=0;i<n;i++)
        {
            for(j=0;j<m;j++)
            {
                arr[i][j+m]=arr[i][j];
                arr[i][j+2*m]=arr[i][j];
                arr[i+n][j]=arr[i][j];
                arr[i+n][j+m]=arr[i][j];
                arr[i+n][j+2*m]=arr[i][j];
                arr[i+2*n][j]=arr[i][j];
                arr[i+2*n][j+m]=arr[i][j];
                arr[i+2*n][j+2*m]=arr[i][j];
            }
        }
        res=0;
        for(i=0;i<n;i++)
        {
            for(j=0;j<m;j++)
            {
               if(arr[i+n][j+m]==1)
               continue;
               int ng=neigh(i+n,j+m);
               if(ng!=3)
                res++;
            }
        }
        if(res)
        printf("Case %d: %d possible ancestors.\n",p++,res);
        else
        printf("Case %d: Garden of Eden.\n",p++);
    }

    return 0;
}


