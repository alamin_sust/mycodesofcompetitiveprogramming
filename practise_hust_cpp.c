#include<stdio.h>
#include<vector>
#include<algorithm>
using namespace std;

int i,st,head,node,edge,y,x,test,kase,cur,tail;
int que[1000010];
bool flag[10010];

vector<int>adj[10001];
void BFS(int st)
{
   int i;
   head = tail = 0;

   que[tail++]=st;
   while(head!=tail)
   {
      cur = que[head++];
      int   l=adj[cur].size();
      for(i=0;i<l;i++)
      {
         if(!flag[adj[cur][i]] && adj[cur][i]!=st)
         {
            que[tail++] = adj[cur][i];
            flag[adj[cur][i]]=1;
         }
      }
   }

}

int main()
{
   scanf("%d",&test);
   while(test--)
   {

      scanf("%d %d",&node,&edge);
      for(i=0;i<=node;i++)
      {
         adj[i].clear();flag[i]=0;
      }

      for(i=1;i<=edge;i++)
      {
         scanf("%d %d",&x,&y);
         adj[x].push_back(y);
      }

      for(i=1;i<=node;i++)
         if(!flag[i])
            BFS(i);

      st = 0;
      for(i=1;i<=node;i++)if(!flag[i])st++;

      printf("Case %d: %d\n",++kase,st);

   }
   return 0;
}
