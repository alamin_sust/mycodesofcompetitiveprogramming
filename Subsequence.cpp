/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int arr[100010],n,k,i,j,now,res;

int main()
{
    while(scanf("%d%d",&n,&k)!=EOF)
    {
        res=0;
        for(i=1;i<=n;i++)
        {
            scanf(" %d",&arr[i]);
        }
        res=99999999;
        now=0;
        for(j=1,i=0;i<n;)
        {
            //printf("%d\n",j);
            while(now<k&&i<=n)
                {
                    i++;
                    now+=arr[i];
                    //printf("%d %d %d\n",now,i,j);
                }
            while(now>=k&&j<=i)
            {
                res=min(res,i-j+1);
                now-=arr[j];
              //  printf("%d\n",now);
                //getchar();
                j++;
            }
        }
        printf("%d\n",res==99999999?0:res);
    }
    return 0;
}




