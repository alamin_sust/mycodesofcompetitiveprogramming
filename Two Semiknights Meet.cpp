#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
string arr[10];
int res[10][10];
void dfs(int i,int j,int move_)
{
    if(i<0||j<0||i>7||j>7)
        return;
    //printf("%d..%d\n",i,j);
    res[i][j]=move_;
    if(i>1&&j>1&&res[i-2][j-2]==-1)
        dfs(i-2,j-2,move_+1);
    if(i>1&&j<6&&res[i-2][j+2]==-1)
        dfs(i-2,j+2,move_+1);
    if(j>1&&i<6&&res[i+2][j-2]==-1)
        dfs(i+2,j-2,move_+1);
    if(j<6&&i<6&&res[i+2][j+2]==-1)
        dfs(i+2,j+2,move_+1);
    return;
}

main()
{
    int t,p,i,j,x,y,flag;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        memset(res,-1,sizeof(res));
        for(flag=0,i=0;i<8;i++)
        {cin>>arr[i];}
        for(i=0;i<8;i++)
        for(j=0;j<8;j++)
            if(flag==0&&arr[i][j]=='K')
               x=i,y=j,dfs(x,y,0),flag=1;
            else if(flag==1&&arr[i][j]=='K')
               x=i,y=j;
        if(res[x][y]!=-1&&res[x][y]%2==0)
            printf("YES\n");
        else
            printf("NO\n");
    }
    return 0;
}

