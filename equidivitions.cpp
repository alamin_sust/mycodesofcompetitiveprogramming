#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define MAX 150
int n,field[MAX][MAX],flag[MAX][MAX],tot;
void cek(int i,int j,int k)
{
   if(field[i][j]==k)
   {
      if(flag[i][j]==0)
      {
         tot++; flag[i][j]=1;
      }
      if(i>0&&flag[i-1][j]==0)    cek(i-1,j,k);
      if(i<n-1&&flag[i+1][j]==0)    cek(i+1,j,k);
      if(j>0&&flag[i][j-1]==0)    cek(i,j-1,k);
      if(j<n-1&&flag[i][j+1]==0)    cek(i,j+1,k);
   }
}
int main()
{
   int i,j,k,jum[MAX],a[MAX],b[MAX],bad;
   char line[1000],*p;
   while(gets(line))
   {
      n=atoi(line);
      if(!n) break;
      for(i=0;i<n;i++)
         for(j=0;j<n;j++) { field[i][j]=flag[i][j]=0; }
      for(k=1;k<n;k++)
      {
         jum[k-1]=0;
         gets(line);
         p=strtok(line," "); i=atoi(p);
         p=strtok(NULL," "); j=atoi(p);
         a[k-1]=i-1;
         b[k-1]=j-1;
         if(i&&j) { field[i-1][j-1]=k; jum[k-1]++; }
         while((p=strtok(NULL," "))!=NULL)
         {
            i=atoi(p); p=strtok(NULL," "); j=atoi(p);
            if(i&&j) { field[i-1][j-1]=k; jum[k-1]++; }
         }
      }
      bad=0;
      for(i=0;i<n-1;i++)
      {
         tot=0;
         cek(a[i],b[i],i+1);
         if(tot!=jum[i]) { bad=1; break; }
      }
      if(bad) printf("wrong\n");
      else printf("good\n");
   }
   return 0;
}
