/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

double low,high,mid,res,w;
int k,i,n,arr[200010];

int func(double val)
{
    if(((double)arr[0])>=val&&((double)arr[n])>=(2.0*val))
    {
        return 1;
    }
    return 0;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d %lf",&n,&w);

    for(i=0;i<(n*2);i++)
    {
        scanf(" %d",&arr[i]);
    }

    sort(arr,arr+(n*2));

    low=0.0;
    high=w/3.0;
    res=0.0;
    k=0;
    while((low+0.0000000000000001)<=high&&k<300)
    {
        k++;
        mid=(low+high)/2.0;
        if(func(mid)&&(mid*3.0*n)<=w)
        {
            res=max(res,mid);
            low=mid;
        }
        else
            high=mid;
    }
    printf("%.10lf\n",res*3.0*n);
    return 0;
}


