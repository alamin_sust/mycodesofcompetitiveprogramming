/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


ll n,m,A[100010][4],B[100010][4],cum[100010],cum_T[100010],p,i,arr[100010],com,a,b,one,two,three,res,x,val;
char ch;

/*
void DEB(void)
{
    for(i=1; i<=n; i++)
    {
        printf("%lld ",A[i][1]);
    }
    printf("\n");
    for(i=1; i<=n; i++)
    {
        printf("%lld ",A[i][2]);
    }
    printf("\n");
    for(i=1; i<=n; i++)
    {
        printf("%lld ",A[i][3]);
    }
    printf("\n\n");

    for(i=1; i<=n; i++)
    {
        printf("%lld ",B[i][1]);
    }
    printf("\n");
    for(i=1; i<=n; i++)
    {
        printf("%lld ",B[i][2]);
    }
    printf("\n");
    for(i=1; i<=n; i++)
    {
        printf("%lld ",B[i][3]);
    }
    printf("\n\n");

    for(i=1; i<=n; i++)
        printf("%lld ",cum_T[i]);
    printf("\n\n");

    for(i=1; i<=n; i++)
        printf("%lld ",cum[i]);
    printf("\n\n");

    return;
}
*/

int main()
{
    scanf(" %lld %lld",&n,&m);
    getchar();
    for(i=1; i<=n; i++)
    {
        scanf("%c",&ch);
        arr[i]=(ch-'0')%3LL;
        if(arr[i]==0LL)
            arr[i]=3LL;
    }
    for(i=n; i>=1LL; i--)
    {
        cum[i]=(cum[i+1]+arr[i])%3LL;

        B[i][1]=B[i+1][1];
        B[i][2]=B[i+1][2];
        B[i][3]=B[i+1][3];
        if(cum[i]==0)
            B[i][3]++;
        else if(cum[i]==1)
            B[i][1]++;
        else
            B[i][2]++;


        if(arr[i]==1LL)
        {
            A[i][1]=1LL+A[i+1][3];
            A[i][2]=A[i+1][1];
            A[i][3]=A[i+1][2];
        }
        else if(arr[i]==2LL)
        {
            A[i][1]=A[i+1][2];
            A[i][2]=1LL+A[i+1][3];
            A[i][3]=A[i+1][1];
        }
        else
        {
            A[i][1]=A[i+1][1];
            A[i][2]=A[i+1][2];
            A[i][3]=1LL+A[i+1][3];
        }
        cum_T[i]=cum_T[i+1]+A[i][3];
    }
    //DEB();
    for(p=1LL; p<=m; p++)
    {
        scanf(" %lld",&com);
        if(com==1)
        {
            scanf(" %lld %lld",&x,&val);
            val%=3LL;
            if(val==0)
                val=3LL;
            if(arr[x]==val)
                continue;
            arr[x]=val;
            for(i=x; i>=1LL; i--)
            {
                cum[i]=(cum[i+1]+arr[i])%3LL;

                B[i][1]=B[i+1][1];
                B[i][2]=B[i+1][2];
                B[i][3]=B[i+1][3];
                if(cum[i]==0)
                    B[i][3]++;
                else if(cum[i]==1)
                    B[i][1]++;
                else
                    B[i][2]++;


                if(arr[i]==1LL)
                {
                    A[i][1]=1LL+A[i+1][3];
                    A[i][2]=A[i+1][1];
                    A[i][3]=A[i+1][2];
                }
                else if(arr[i]==2LL)
                {
                    A[i][1]=A[i+1][2];
                    A[i][2]=1LL+A[i+1][3];
                    A[i][3]=A[i+1][1];
                }
                else
                {
                    A[i][1]=A[i+1][1];
                    A[i][2]=A[i+1][2];
                    A[i][3]=1LL+A[i+1][3];
                }
                cum_T[i]=cum_T[i+1]+A[i][3];
            }

        }
        else
        {
            scanf(" %lld %lld",&a,&b);
            if(cum[b+1]==0LL)
            {
                one=B[a][1]-B[b+1][1];
                two=B[a][2]-B[b+1][2];
                three=B[a][3]-B[b+1][3];
            }
            else if(cum[b+1]==2LL)
            {
                one=B[a][3]-B[b+1][3];
                two=B[a][1]-B[b+1][1];
                three=B[a][2]-B[b+1][2];
            }
            else
            {
                one=B[a][2]-B[b+1][2];
                two=B[a][3]-B[b+1][3];
                three=B[a][1]-B[b+1][1];
            }
            res=cum_T[a]-cum_T[b+1]-(two*A[b+1][1]+one*A[b+1][2]+three*A[b+1][3]);
            printf("%lld\n",res);
        }
    }
    return 0;
}




