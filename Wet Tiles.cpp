/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int rr[]={0,0,1,-1};
int cc[]={-1,1,0,0};

int leaks,arr[1010][1010],r,c,val[1010][1010],stat[1010][1010],t;

struct node{
int x,y;
};

node det;
queue<node>q;

void bfs(void)
{
    while(!q.empty())
    {
        node u=q.front();
        q.pop();
        for(int i=0;i<4;i++)
        {
            node v;
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            if(v.x>=1&&v.y>=1&&v.x<=r&&v.y<=c&&stat[v.x][v.y]==0&&arr[v.x][v.y]==0)
            {
                stat[v.x][v.y]=1;
                val[v.x][v.y]=val[u.x][u.y]+1;
                leaks++;
                if(val[v.x][v.y]<t)
                {
                    q.push(v);
                }
            }
        }
    }
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    int l,w,i,j,k,ii,jj,xb,xe,yb,ye;

    while(scanf("%d",&r)!=EOF)
    {
        if(r==-1)
            break;
        memset(stat,0,sizeof(stat));
        memset(val,0,sizeof(val));
        memset(arr,0,sizeof(arr));
        leaks=0;
        while(!q.empty())q.pop();
        scanf(" %d %d %d %d",&c,&t,&l,&w);
        for(i=1;i<=l;i++)
        {
            scanf(" %d %d",&det.x,&det.y);
            leaks++;
            q.push(det);
            stat[det.x][det.y]=1;
            val[det.x][det.y]=1;
        }
        for(k=1;k<=w;k++)
        {
            scanf(" %d %d %d %d",&xb,&yb,&xe,&ye);
            ii=0;
            jj=0;
            if(xb<xe)
                ii=1;
            else if(xb>xe)
                ii=-1;
            if(yb<ye)
                jj=1;
            else if(yb>ye)
                jj=-1;

            for(i=xb,j=yb;;j+=jj,i+=ii)
            {
                arr[i][j]=1;
                if(i==xe&&j==ye)
                    break;
            }
        }

        bfs();
//        for(i=1;i<=r;i++)
//        {
//            for(j=1;j<=c;j++)
//            {
//                printf("%d",val[i][j]);
//            }
//            printf("\n");
//        }
        printf("%d\n",leaks);

    }

    return 0;
}


