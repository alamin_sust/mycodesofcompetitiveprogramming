#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
ll Max(ll u,ll v)
{
    if(__builtin_popcountll(u)>__builtin_popcountll(v))
        return u;
    else if(__builtin_popcountll(u)<__builtin_popcountll(v))
        return v;
    return min(u,v);
}

ll solve(ll l,ll r)
{
    ll res=0,cur=0;
    for(int i=62;i>=0;i--)
    {
        if((l&(1ll<<i))==(r&(1ll<<i))) cur+=(l&(1ll<<i));
        else
        {
            res=Max(res,cur+(1ll<<i)-1);
            cur+=(r&(1ll<<i));
        }
        res=Max(res,cur);
    }
    return Max(res,cur);
}

int main()
{
    int n;
    ll l,r;
    scanf(" %d",&n);
    for(int i=1;i<=n;i++)
    {
        scanf("%I64d %I64d",&l,&r);
        printf("%I64d\n",solve(l,r));
    }
    return 0;
}

