#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ull phi[5000100];
void phi_gen(void)
{
    ull i,j;
	for(i=2; i<=5000010; i++)
	{
	    if(phi[i]==0)
	    {
	        phi[i]=i-1;
			for(j=i+i; j<=5000010; j+=i)
			{
				if(phi[j]==0)
                phi[j]=j;
                phi[j]=(phi[j]/i)*(i-1);
			}
	    }
	}
	for(i=2;i<5000010;i++)
	{
        phi[i]*=phi[i];
        phi[i]+=phi[i-1];
	}
	return;
}

main()
{
    ull f,t,tc,p;
    phi_gen();
    scanf(" %llu",&tc);
    for(p=1;p<=tc;p++)
    {
        scanf(" %llu %llu",&f,&t);
        printf("Case %llu: %llu\n",p,phi[t]-phi[f-1]);
    }
    return 0;
}
