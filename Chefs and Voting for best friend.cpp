/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int t,p,n,i,tp,arr[110],res[110],stat[110],now,flag,j;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        memset(stat,0,sizeof(stat));
        scanf(" %d",&n);
        tp=0;
        for(i=1;i<=n;i++)
        {
            scanf(" %d",&arr[i]);
            tp+=arr[i];
        }
        if(tp!=n)
            {printf("-1\n");
            continue;}
        now=1;
        tp=0;
        while(tp<n)
        {
            flag=0;
            for(i=1;i<=n;i++)
            {
                if(i==now)
                    continue;
                if(arr[i]>0&&stat[i]==0)
                {
                    //printf("%d..%d\n",now,i);
                    stat[now]=1;
                    arr[i]--;
                    res[now]=i;
                    now=i;
                    flag=1;
                    break;
                }
            }
            if(flag==0)
                break;
            tp++;
        }
        if(flag==0)
        {
            for(i=1;i<=n;i++)
            {
                if(stat[i]==0)
                {
                    for(j=1;j<=n;j++)
                    {
                        if(j==i)
                            continue;
                        if(arr[j])
                        {
                            arr[j]--;
                            res[i]=j;
                            tp++;
                            break;
                        }
                    }
                }
            }
        }
        if(tp!=n)
            printf("-1\n");
        else
        {
            for(i=1;i<=n;i++)
            {
                printf("%d",res[i]);
                if(i==n)
                    printf("\n");
                else
                    printf(" ");
            }
        }

    }

    return 0;
}

