#include <limits.h>
int best[10];
int lis( int* a, int N ) {
   int i, j, max = INT_MIN;
   for ( i = 0; i < N; i++ ) best[i] = 1;

   for ( i = 1; i < N; i++ )
      for ( j = 0; j < i; j++ )
         if ( a[i] > a[j] && best[i] < best[j] + 1 ){
		    best[i] = best[j] + 1;
                if(max < best[i])
				max = best[i];
		 }

   //free( best );
   return max;
}
// Sample usage.
int main(){
  int b[] = { 1, 3, 2, 4, 3, 5, 4, 6 },len;
  // the longest increasing subsequence = 13456?
  // the length would be 5, as well lcs(b,8) will return.
  len=lis( b, 8 );
  printf("%d\n",  len);
}
