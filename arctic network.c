#include <stdio.h>
#include <math.h>
struct stacja
{
    int x,y;
} p[501];
int sat;
int max;
int zb[501];
int next[501];
int size[501];
int last[501];
double e[124750];
int w[124750];
int k[124750][2];
int swap(int *a, int *b)
{
    int i;
    i=*a;
    *a=*b;
    *b=i;
}
int make_set(int x)
{
    zb[x]=x;
    next[x]=0;
    size[x]=1;
    last[x]=x;
}
int join(int a, int b)
{
    int i,x,y;
    x=a;
    y=b;
    next[last[zb[x]]]=zb[y];
    last[zb[x]]=last[zb[y]];
    i=zb[y];
    size[zb[x]]+=size[zb[y]];
    while (i)
    {
        zb[i]=zb[x];
        i=next[i];
    }
}
int cmp(const void *a, const void *b)
{
    if (e[*(int *)a]<e[*(int *)b])
        return -1;
    else if (e[*(int *)a]>e[*(int *)b])
        return 1;
    else
        return 0;
}
main()
{
    int it,i,x,y,ie,j;
    int maxe;
    scanf("%d",&it);
    while (it)
    {
        it--;
        scanf("%d%d",&sat,&max);
        ie=max*(max-1)/2;
        for (i=1; i<=max; i++)
        {
            scanf("%d%d",&p[i].x,&p[i].y);
            make_set(i);
        }
        i=0;
        for (x=1; x<=max; x++)
            for (y=x+1; y<=max; y++)
            {
                e[i]=sqrt(((double)(p[x].x-p[y].x)*(double)(p[x].x-p[y].x))+((double)(p[x].y-p[y].y)*(double)(p[x].y-p[y].y)));
                k[i][0]=x;
                k[i++][1]=y;
            }
        for (i=0; i<ie; i++)
            w[i]=i;
        j=0;
        qsort(w,ie,sizeof(int),cmp);
        for (i=0; i<ie && j<max-sat; i++)
        {
            x=k[w[i]][0];
            y=k[w[i]][1];
            if (zb[x]==zb[y])
                continue;
            j++;
            join(x,y);
        }
        printf("%.2lf\n",e[w[i-1]]);
    }
    return 0;
}
