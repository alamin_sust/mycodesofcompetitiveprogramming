/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll n,i,j,x[100010],h[100010],res;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    scanf(" %I64d",&n);
    for(i=1LL; i<=n; i++)
    {
        scanf(" %I64d %I64d",&x[i],&h[i]);
    }
    if(n<=2LL)
    {
        printf("%I64d\n",n);
        return 0;
    }
    res=2LL;

    for(i=2LL,j=n-1LL; i<=j; i++,j--)
    {
        if(i==j)
        {
            if((x[i-1]+h[i])<x[i])
                res++;
            else if((x[i]+h[i])<x[i+1])
                res++;
            break;
        }

        if((x[i-1]+h[i])<x[i])
        {
            res++;
        }
        else if((x[i]+h[i])<x[i+1])
        {
            res++;
            x[i]+=h[i];

        }

        if((i+1LL)==j)
        {
            if((x[j-1]+h[j])<x[j])
            {
                res++;
                break;
            }
            else if((x[j]+h[j])<x[j+1])
            {
                res++;
                break;
            }
            break;
        }


        if((x[j]+h[j])<x[j+1])
        {
            res++;
        }
        else if((x[j-1]+h[j])<x[j])
        {
            res++;
            x[j]-=h[j];

        }

    }

    printf("%I64d\n",res);

    return 0;
}






