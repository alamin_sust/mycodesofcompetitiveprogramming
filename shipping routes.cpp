#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

int n,v[32],stat[32],adj[32][32];

bool bfs(int from,int to)
{
    queue<int>que;
    que.push(from);
    v[from]=0;
    while(!que.empty())
    {
       int pr=que.front();
        que.pop();
        stat[pr]=1;
        for(int i=1;i<=n;i++)
        {
            if(stat[i]==0&&adj[pr][i]==1)
            {
               v[i]=v[pr]+1;
               stat[i]=1;
               que.push(i);
               if(i==to)
                    return true;
            }
        }
    }
    return false;
}

main()
{
    char in[4],in2[4];
    int t,p,i,m,q,cost,val[28][28];
    cin>>t;
    printf("SHIPPING ROUTES OUTPUT\n\n");
    for(p=1;p<=t;p++)
    {
        printf("DATA SET  %d\n\n",p);
        memset(val,0,sizeof(val));
        memset(adj,0,sizeof(adj));
        cin>>n>>m>>q;
        for(i=1;i<=n;i++)
            {scanf(" %s",in);
            val[in[0]-'A'][in[1]-'A']=i;}
        for(i=1;i<=m;i++)
        {
            scanf(" %s %s",in,in2);
            adj[val[in[0]-'A'][in[1]-'A']][val[in2[0]-'A'][in2[1]-'A']]=1;
            adj[val[in2[0]-'A'][in2[1]-'A']][val[in[0]-'A'][in[1]-'A']]=1;
        }
        for(i=1;i<=q;i++)
        {
            memset(stat,0,sizeof(stat));
            cin>>cost;
            scanf(" %s %s",in,in2);
            if(bfs(val[in[0]-'A'][in[1]-'A'],val[in2[0]-'A'][in2[1]-'A']))
                printf("$%d\n",100*cost*v[val[in2[0]-'A'][in2[1]-'A']]);
            else
                printf("NO SHIPMENT POSSIBLE\n");
        }
        printf("\n");
    }
    printf("END OF OUTPUT\n");
    return 0;
}

