/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll res[1010],dp[1010][1010],t,p,n,MOD=1000000007LL;

ll rec(ll pos,ll mx)
{
    if(pos==n)
        return 1LL%MOD;
    ll &ret=dp[pos][mx];
    if(ret!=-1LL)
        return ret;
    ret=0LL;
    ret=(rec(pos+1LL,mx+1LL)+((mx*rec(pos+1LL,mx))%MOD))%MOD;
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    ll i,j;
    //memset(res,-1LL,sizeof(res));
    memset(dp,-1LL,sizeof(dp));
    n=1000;
    rec(0,0);
    scanf(" %lld",&t);
    for(p=1LL; p<=t; p++)
    {
        //memset(dp,-1LL,sizeof(dp));
        scanf(" %lld",&n);
        /*if(res[n]==-1)
        {
            for(i=0LL;i<=n+1LL;i++)
                for(j=0LL;j<=n+1LL;j++)
                dp[i][j]=-1LL;
            res[n]=rec(0LL,0LL);
        }
        printf("%lld %lld %lld\n",dp[1000][0],dp[999][0],dp[998][0]);*/
        printf("%lld\n",dp[1000-n][0]);
    }
    return 0;
}

/*
11
990
991
992
993
994
995
996
997
998
999
1000
*/


