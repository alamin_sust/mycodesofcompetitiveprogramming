
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

long long int tree[800010],upd[800010],lazy[800010],res;

void update(long long int node,long long int f,long long int t,long long int x,long long int y,long long int val)
{
    long long int mid=(f+t)/2;
    if(lazy[node]>0)
    {
        if(f!=t)
        {
        //tree[node*2]+=lazy[node]*(mid-f+1);
        //tree[node*2+1]+=lazy[node]*(t-mid);
        lazy[node*2]+=lazy[node];
        lazy[node*2+1]+=lazy[node];
        //lazy[node]=0;
        }
        tree[node]+=lazy[node]*(t-f+1);
        lazy[node]=0;
    }
    if(t<x||f>y)
        return;
    if(x<=f&&y>=t)
    {
        tree[node]+=val*(t-f+1);
        if(f!=t)
        {
            lazy[node*2]+=val;
            lazy[node*2+1]+=val;
        }
        return;
    }
    //if(x<=mid)
        update(node*2,f,mid,x,y,val);
    //if(y>mid)
        update(node*2+1,mid+1,t,x,y,val);
    tree[node]=tree[node*2]+tree[node*2+1];
    return;
}

long long int query(long long int node,long long int f,long long int t,long long int x,long long int y)
{
    long long int mid=(f+t)/2;
    if(lazy[node]>0)
    {
        if(f!=t)
        {
        //upd[node*2]=upd[node*2+1]=1;
        //tree[node*2]+=lazy[node]*(mid-f+1);
        //tree[node*2+1]+=lazy[node]*(t-mid);
        lazy[node*2]+=lazy[node];
        lazy[node*2+1]+=lazy[node];
        //lazy[node]=0;
        }
        tree[node]+=(t-f+1)*lazy[node];
        lazy[node]=0;
    }
    if(t<x||f>y)
        return 0;
    if(x<=f&&y>=t)
    {
        return tree[node];
    }
    long long int ret=0;
    //if(x<=mid)
        ret+=query(node*2,f,mid,x,y);
    //if(y>mid)
        ret+=query(node*2+1,mid+1,t,x,y);
    tree[node]=tree[node*2]+tree[node*2+1];
    return ret;
}

main()
{
    long long int t,p,n,q,i,x,y,v,com;
    scanf(" %lld",&t);
    for(p=1;p<=t;p++)
    {
        //memset(upd,0,sizeof(upd));
        memset(lazy,0,sizeof(lazy));
        memset(tree,0,sizeof(tree));
        scanf(" %lld %lld",&n,&q);
        printf("Case %lld:\n",p);
        for(i=1;i<=q;i++)
        {
            scanf(" %lld",&com);
            if(com==0)
            {
                scanf(" %lld %lld %lld",&x,&y,&v);
                update(1,1,n,x+1,y+1,v);
            }
            else
            {
                scanf(" %lld %lld",&x,&y);
                res=query(1,1,n,x+1,y+1);
                printf("%lld\n",res);
            }
        }
    }
    return 0;
}
