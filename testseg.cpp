/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
#define SZ 1000003
pair<int,int> treeMax[4*SZ];
int arr[SZ];
int arr2[SZ];
void initMax(int node,int b,int e)
{
    if(b==e)
    {
        treeMax[node].first=arr[b];
        treeMax[node].second=arr[b];
        return;
    }
    int left = node*2;
    int right = node*2+1;
    int mid = (b+e)/2;
    initMax(left,b,mid);
    initMax(right,mid+1,e);
    treeMax[node].first = max(treeMax[left].first,treeMax[right].first);
    treeMax[node].second = min(treeMax[left].second,treeMax[right].second);
}

pair<int,int> queryMax(int node,int b,int e,int i,int j)
{
    if(i>e||j<b)return make_pair(-2147483648,2147483647);
    if(b>=i&&e<=j) return treeMax[node];
    int left = node*2;
    int right = node*2+1;
    int mid = (b+e)/2;
    pair<int,int>retl;
    pair<int,int>retr;
    retl = queryMax(left,b,mid,i,j);
    retr = queryMax(right,mid+1,e,i,j);
    return make_pair(max(retl.first,retr.first),min(retl.second,retr.second));
}

int main()
{
    int n,k,i;
    scanf("%d %d",&n,&k);
    for(i = 1; i<=n; i++)
    {
        scanf("%d",&arr[i]);
    }
    initMax(1,1,n);
    k--;
    pair<int,int>ans;
    for(i=1; i+k<=n; i++)
    {
        ans=queryMax(1,1,n,i,i+k);
        arr2[i]=ans.first;
        arr[i]=ans.second;
    }
    for(i=1;i+k<=n;i++)
    {
        if(i!=1)printf(" %d",arr[i]);
        else printf("%d",arr[i]);
    }
    printf("\n");
    for(i=1;i+k<=n;i++)
    {
        if(i!=1)printf(" %d",arr2[i]);
        else printf("%d",arr2[i]);
    }
    printf("\n");
    return 0;
}

