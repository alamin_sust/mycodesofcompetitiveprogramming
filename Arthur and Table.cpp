/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node
{
    ll len,cost;
};

bool operator<(node a,node b)
{
    return a.cost<b.cost;
}

priority_queue<node>pqcost;
node arr[100010];
vector<node>vc;
ll n,i,tp,torcost,res,j,leglen[100010],legcost[100010],totcost;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %I64d",&n);

    for(i=0; i<n; i++)
    {
        scanf(" %I64d",&arr[i].len);
        leglen[arr[i].len]++;
    }
    for(i=0; i<n; i++)
    {
        scanf(" %I64d",&arr[i].cost);
        legcost[arr[i].len]+=arr[i].cost;
        totcost+=arr[i].cost;
        pqcost.push(arr[i]);
    }
    res=100000000000000LL;
    for(i=100000LL; i>=1LL; i--)
    {
        if(leglen[i])
        {
            tp=totcost-legcost[i];
            for(j=1; j<leglen[i];)
            {
                if(pqcost.empty())
                    break;
                if(pqcost.top().len>=i)
                {
                    pqcost.pop();
                    continue;
                }
                tp-=pqcost.top().cost;
                vc.push_back(pqcost.top());
                pqcost.pop();
                j++;
            }
            for(j=0;j<vc.size();j++)
            {
                pqcost.push(vc[j]);
            }
            vc.clear();
            res=min(res,tp);
        }
    }

    printf("%I64d\n",res);

    return 0;
}


