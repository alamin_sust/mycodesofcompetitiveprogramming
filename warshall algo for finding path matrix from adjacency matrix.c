#include<stdio.h>
#define max 20

main()
{
    int from,to,n,i,j,k,vertices=0,path[max][max],adj[max][max];
    printf("vertices: ");
    scanf(" %d",&vertices);
    printf("edges: ");
    scanf(" %d",&n);
    for(i=1;i<=vertices;i++)
    {
        for(j=1;j<=vertices;j++)
        adj[i][j]=0;
    }
    for(i=1;i<=n;i++)
    {
        scanf(" %d %d",&from,&to);
        adj[from][to]=1;
    }
    //adjacency matrix
    for(i=1;i<=vertices;i++)
    {
        for(j=1;j<=vertices;j++)
        printf(" %d",adj[i][j]);
        printf("\n");
    }
    printf("\n\n");

    //path matrix using warshall alg.
    for(i=1;i<=vertices;i++)
    {
        for(j=1;j<=vertices;j++)
        {
            path[i][j] = (adj[i][j] ? 1 : 0);
        }
    }

    for(k=1;k<=vertices;k++)
    {
     for(i=1;i<=vertices;i++)
       {
        for(j=1;j<=vertices;j++)
        {
            path[i][j] = path[i][j]|(path[i][k]&path[k][j]);
        }
        }
    }

    //path matrix print

    for(i=1;i<=vertices;i++)
    {
        for(j=1;j<=vertices;j++)
        printf(" %d",path[i][j]);
        printf("\n");
    }
    printf("\n");
return 0;
}
