#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

struct node{
int x,y,val;
};

node adj[55];
int xx,yy,t,p,n,i,m,j,flag,cost;
char from[55][55][55],par[110],to[55][55][55],arr[55][110][55];

int comp(node a,node b)
{
    return a.val<b.val;
}

main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>m;
        getchar();
        n=0;
        for(i=0;i<m;i++)
        {
            scanf(" %s %s %d",from[p][i],to[p][i],&cost);
            for(flag=0,j=0;j<n;j++)
            {
                if(strcmp(from[p][i],arr[p][j])==0)
                {
                    flag=1;
                    xx=j;
                    break;
                }
            }
            if(flag==0)
                    {
                        strcpy(arr[p][n],from[p][i]);
                        xx=n++;
                    }
            for(flag=0,j=0;j<n;j++)
            {
                if(strcmp(to[p][i],arr[p][j])==0)
                {
                    flag=1;
                    yy=j;
                    break;
                }
            }
            if(flag==0)
                    {
                    strcpy(arr[p][n],to[p][i]);
                    yy=n++;
                    //printf("%s\n",arr[n-1]);
                    }
                adj[i].x=xx;
                adj[i].y=yy;
                adj[i].val=cost;
                //strcpy(from,arr[108]);
                //strcpy(to,arr[108]);
        }
        sort(adj,adj+m,comp);
        for(i=0;i<n;i++)
            par[i]=i;
        int res;
        for(res=0,i=0,j=1;j<n&&i<m;i++)
        {
            int u=adj[i].x;
            int v=adj[i].y;
            //printf("%d %d\n",u,v);
            while(par[u]!=u)
                u=par[u];
            while(par[v]!=v)
                v=par[v];
            if(u!=v)
            {par[u]=v;
            j++;
                res+=adj[i].val;}
        }
        if(j<n)
            printf("Case %d: Impossible\n",p);
        else
        printf("Case %d: %d\n",p,res);
    }
    return 0;
}

