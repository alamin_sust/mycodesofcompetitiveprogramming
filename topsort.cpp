#include<stdio.h>
#include<vector>

using namespace std;

vector<int> lst[110], sorted;
int degree[110], colour[110];

int main()
{
    int u, v, i, j, k, l, n, m, f;
    while(scanf("%d %d" , &n, &m)!=EOF)
    {
        while(m--)
        {
            scanf("%d", &v);
            scanf("%d", &k);
            degree[v]=k;
            while(k--)
            {
                scanf("%d", &u);
                lst[u].push_back(v);
            }
        }
        while(sorted.size()!=n)
        {
            for(i=1;i<=n;i++)
            {
                if(!degree[i]&&!colour[i])
                {
                    sorted.push_back(i);
                    colour[i]=1;
                    for(j=0;j<lst[i].size();j++)
                        degree[lst[i][j]]--;
                    break;
                }
            }
        }
        for(i=0;i<sorted.size();i++)
        {
            lst[sorted[i]].clear();
            colour[sorted[i]]=0;
            if(i)
                printf(" ");
            printf("%d", sorted[i]);
        }
        printf("\n");
        sorted.clear();
    }
    return 0;
}
