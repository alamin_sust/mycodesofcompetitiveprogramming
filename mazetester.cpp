/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int rr[]={0,0,1,-1};
int cc[]={-1,1,0,0};

struct node{
int x,y;
};

node det,u,v;
queue<node>q;
int stat[33][33];
char arr[35][35];

int bfs(int xx,int yy)
{
    memset(stat,0,sizeof(stat));
    while(!q.empty())
        q.pop();
    det.x=xx;
    det.y=yy;
    stat[xx][yy]=1;
    q.push(det);
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(int i=0;i<4;i++)
        {
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            if(v.x>=0&&v.y>=0&&v.x<30&&v.y<30&&stat[v.x][v.y]==0&&arr[v.x][v.y]!='|'&&arr[v.x][v.y]!='-'&&arr[v.x][v.y]!='X')
            {
                if(arr[v.x][v.y]=='G')
                    return 1;
                stat[v.x][v.y]=1;
                q.push(v);
            }
        }
    }
    return 0;
}

int main()
{
    int i,j,x,y;
    freopen("InputFile-4.txt","r",stdin);
    freopen("mazetesterOutput.txt","w",stdout);
    while(scanf("%s",&arr[0])!=EOF)
    {
        for(i=1;i<30;i++)
            scanf(" %s",&arr[i]);
        for(i=0;i<30;i++)
        {
            for(j=0;j<30;j++)
            {
                if(arr[i][j]=='P')
                {
                    x=i;
                    y=j;
                    break;
                }
            }
        }
        if(bfs(x,y))
            printf("Possible\n");
        else
            printf("Impossible\n");
    }
    return 0;
}
