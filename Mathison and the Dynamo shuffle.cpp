/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define ull unsigned long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ull t,p,n,i,pos,k,pls;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %llu",&t);

    for(p=1LLU;p<=t;p++) {
        scanf(" %llu %llu",&n,&k);
        pos=0LLU;

        pls=(1LLU<<(n-1LLU));

        for(i=0LLU;i<n;i++) {
            if(i==(n-1LLU)) {
                pos+=k;
            } else {
                if(k%2LLU) {
                    pos+=pls;
                    k=(k-1LLU)/2LLU;
                } else {
                    k/=2LLU;
                }
            }
            pls>>=1LLU;
        }

        printf("%llu\n",pos);

    }

    return 0;
}

