/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int n,m,k,i,j,arr[110][110],res[110][110],flag,arr2[110][110],row[110],col[110];

int main()
{
    cin>>n>>m;
    for(i=1; i<=n; i++)
    {
        for(j=1; j<=m; j++)
        {
            scanf(" %d",&arr[i][j]);
        }
    }
    for(i=1;i<=n;i++)
    {
        row[i]=1;
        for(j=1;j<=m;j++)
            row[i]&=arr[i][j];
    }
    for(i=1;i<=m;i++)
    {
        col[i]=1;
        for(j=1;j<=n;j++)
            col[i]&=arr[j][i];
    }
    flag=0;
    for(i=1; i<=n; i++)
    {
        for(j=1; j<=m; j++)
        {
            if(row[i]&&col[j])
            {
                res[i][j]=1;
                for(k=1;k<=n;k++)
                    arr2[k][j]=1;
                for(k=1;k<=m;k++)
                    arr2[i][k]=1;
            }
        }
    }
    for(i=1;i<=n&&flag==0;i++)
    {
        for(j=1;j<=m;j++)
        {
            if(arr[i][j]!=arr2[i][j])
            {
                flag=1;
                break;
            }
        }
    }
    if(flag==0)
    {
        printf("YES\n");
        for(i=1; i<=n; i++)
        {
            for(j=1; j<=m; j++)
            {
                printf("%d ",res[i][j]);
            }
            printf("\n");
        }
    }
    else
        printf("NO\n");
    return 0;
}


