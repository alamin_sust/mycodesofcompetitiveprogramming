#include<stdio.h>
#include<math.h>

int main()
{
    long long n,sqrt_n,sqrt_n_1,up_diff,down_diff;
    while(scanf("%lld",&n)==1)
    {
        if(n == 0)
            break;

        sqrt_n=sqrt(n);
        sqrt_n_1=sqrt_n+1;

        if(sqrt_n*sqrt_n == n)
        {
            if(sqrt_n%2 == 1)
                printf("1 %lld\n",sqrt_n);
            else
                printf("%lld 1\n",sqrt_n);
        }

        else if(sqrt_n%2 == 1)
        {
            up_diff = sqrt_n_1*sqrt_n_1 - n + 1;
            if(up_diff > sqrt_n_1)
                up_diff = sqrt_n_1;

            down_diff = n - sqrt_n*sqrt_n;
            if(down_diff > sqrt_n_1)
                down_diff = sqrt_n_1;
            printf("%lld %lld\n",down_diff,up_diff);
        }

        else if(sqrt_n%2 == 0)
        {
            up_diff = sqrt_n_1*sqrt_n_1 - n + 1;
            if(up_diff > sqrt_n_1)
                up_diff = sqrt_n_1;

            down_diff = n - sqrt_n*sqrt_n;
            if(down_diff > sqrt_n_1)
                down_diff = sqrt_n_1;
            printf("%lld %lld\n",up_diff,down_diff);
        }
    }

    return 0;
}
