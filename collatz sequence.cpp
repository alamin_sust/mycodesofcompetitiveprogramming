/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

char arr[1010][55];
ll res[1010],n,i,j,hand;

main()
{
    //freopen("inputeuler.txt","r",stdin);
    cin>>n;
    getchar();
    for(i=0;i<n;i++)
    {
        gets(arr[i]);
        //printf("%s\n",arr[i]);
    }
    for(i=49;i>=0;i--)
    {
        for(j=0;j<n;j++)
        {
            res[i]+=arr[j][i]-'0';
        }
        res[i]+=hand;
        hand=res[i]/10;
        res[i]%=10;
    }
    if(hand>0)
    printf("%lld",hand);
    j=10;
    if(hand<9)
        j=9;
    else if(hand<99)
        j=8;
    else if(hand<999)
        j=7;
    else if(hand<9999)
        j=6;
    else if(hand<99999)
        j=5;
    else if(hand<999999)
        j=4;
    for(i=0;i<j;i++)
        printf("%lld",res[i]);
    return 0;
}


/*
5
37107287533902102798797998220837590246510135740250
46376937677490009712648124896970078050417018260538
74324986199524741059474233309513058123726617309629
91942213363574161572522430563301811072406154908250
23067588207539346171171980310421047513778063246676
*/
