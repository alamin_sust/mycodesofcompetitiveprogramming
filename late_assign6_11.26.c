#include<stdio.h>
#define N 3
typedef struct{
char *name;
int acct_no;
char acct_type;
float balance;
}record;

record *search(record table[], int acctn);

main()
{
  static record customer[N]={
  {"Smith",2222,'C',33.33},
  {"Jones",6666,'O',66.66},
  {"Brown",9999,'D',99.99}
  };

  int acctn;
  record *pt;
  printf("Customer account locator\n");
  printf("to END, enter 0 for the account number\n");
  printf("Account number: ");
  scanf("%d",&acctn);
  while(acctn!=0)
  {
      pt=search(customer,acctn);
      if(pt!=NULL)
      {
          printf("\nName: %s\n",pt->name);
          printf("Account no.: %d\n",pt->acct_no);
          printf("Account type: %c\n",pt->acct_type);
          printf("Balance: %.2f\n",pt->balance);
      }
      else
      printf("\nERROR - Please try again\n");

      printf("\nAccount no.: ");
      scanf("%d",&acctn);
  }
}

record *search(record table[N],int acctn)
{
    int count;

    for(count=0;count<N;++count)
        if(table[count].acct_no == acctn)
        return(&table[count]);

    return(NULL);

}
