#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

int arr[35][35][35],mat[35][35][35],n,m,result[35][35],res[35][35];

void build(int x)
{
    for(int i=1; i<=n; i++)
    {
        for(int j=1; j<=n; j++)
        {
            arr[x][i][j]=0;
            for(int k=1; k<=n; k++)
                arr[x][i][j]+=arr[x-1][i][k]*arr[1][k][j];
            arr[x][i][j]%=m;
        }
    }
}

void big_mod(int x)
{
    for(int i=1; i<=n; i++)
    {
        for(int j=1; j<=n; j++)
        {
            mat[x][i][j]=0;
            for(int k=1; k<=n; k++)
                mat[x][i][j]+=mat[x-1][i][k]*mat[x-1][k][j];
            mat[x][i][j]%=m;
        }
    }
    return;
}

void take_res(int x)
{
    for(int i=1; i<=n; i++)
    {
        for(int j=1; j<=n; j++)
        {
            res[i][j]=0;
            for(int k=1; k<=x; k++)
                res[i][j]+=arr[k][i][j];
            res[i][j]%=m;
        }
    }
    return;
}

void mat_mul(int x)
{
    int temp[35][35];
    for(int i=1; i<=n; i++)
        for(int j=1; j<=n; j++)
            temp[i][j]=result[i][j];
    for(int i=1; i<=n; i++)
    {
        for(int j=1; j<=n; j++)
        {
            result[i][j]=0;
            for(int k=1;k<=n;k++)
            {
                result[i][j]+=temp[i][k]*mat[x][k][j];
            }
            result[i][j]%=m;
        }
    }
    return;
}

main()
{
    int k,i,j;
    cin>>n>>k>>m;
    for(i=1; i<=n; i++)
    {
        for(j=1; j<=n; j++)
            cin>>arr[1][i][j];
    }
    for(i=2; i<=n; i++)
        build(i);
    for(i=1; i<=n; i++)
    {
        for(j=1; j<=n; j++)
        {
            if(i==j)
                result[i][j]=1;
            if(i>j)
                mat[0][i][j]=0;
            else
                mat[0][i][j]=1;
        }
    }
    for(i=1; i<=32; i++)
    {
        big_mod(i);
    }

    if(n>=k)
        take_res(n);
    else
    {
        k=k-n+1;
        j=0;
        //cout<<k;
        while((k>>j)!=0)
        {
            if(((k>>j)&1)==1)
            {
                mat_mul(j);
            }
            j++;
            //printf("%d %d\n",j,k);
        }
        int temp[35][35][35];
        //printf("ll");
        for(i=1; i<=n; i++)
        {
            for(j=1; j<=n; j++)
            {
            //printf("%d ",result[i][j]);
            for(k=1;k<=n;k++)
            {temp[i][j][k]=arr[n-i+1][j][k]*result[1][i];
            printf("%d ",arr[n-i+1][j][k]);
            }

            cout<<endl;
            }
            //cout<<endl;
        }
        for(i=1;i<=n;i++)
            for(j=1;j<=n;j++)
               for(k=1;k<=n;k++)
               res[i][j]+=temp[k][i][j];
        //res=(result[1][1]*9+result[1][2]*8+result[1][3]*7+result[1][4]*6+result[1][5]*5+result[1][6]*4+result[1][7]*3+result[1][8]*2+result[1][9])%m;
    }
    for(i=1; i<=n; i++)
    {
        for(j=1; j<=n; j++)
        {
            printf("%d",res[i][j]);
            if(j!=n)
                printf(" ");
            else
                printf("\n");
        }
    }
    return 0;
}
