#include<stdio.h>


void printpath(int *arr, int *path, int end)
{
    if(end> -1){
        printpath(arr,path,path[end]);
        printf("%d ",arr[end]);
    }
}
void longestincreasingsequence(int *arr, int n)
{
    int *s = new int[n];    //current max number of ints
    int *path = new int[n]; //previous number
    memset(path,0,n);

    int global_max = 1;        //at least one number
    int end_pos = 0;

    s[0]= 1;
    path[0]= -1;
    for(int i=1;i<n;i++){
        int local_prev = -1;
        s[i] = 1;
        for(int j=0;j<i;j++){
            if(arr[j]<arr[i] && s[i]<(s[j]+1)){
                s[i]= s[j]+1;
                local_prev = j;
            }
        }

        path[i] = local_prev;
        if(global_max < s[i]){
            global_max = s[i];
            end_pos = i;
        }
    }

    printf(" size of longest seq : %d \n",global_max);
    printpath(arr,path,end_pos);
    printf("\n");
    delete(path);
    delete(s);
}
