
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define inf 100000010
#define eps 0.00000001
#define ll long long
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define lim 20000000

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll arr[lim+10];

int sieve[lim+10],k,primes[lim+10],n,pw[lim+10];
int vec[lim+10][8];
int cnt[lim+10];

void sieve_(void)
{
    sieve[0]=sieve[1]=1;
    for(ll i=2;i<=lim;i++)
    {
        if(sieve[i]==0)
        {
            primes[k++]=i;
            for(ll j=2;i*j<=lim;j++)
                sieve[i*j]=1;
        }
    }
    return;
}

ll rec(int pos)
{
    if(pos==cnt[n])
        return 1;
    ll ret=0LL,rr=1;
    for(int i=0;i<=pw[pos];i++)
    {
        ret+=rr*rec(pos+1);
        rr*=vec[n][pos];
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    sieve_();

    for(int i=0;i<k;i++)
    {
        ll num=primes[i];
//        if(i<=10)
//            printf("%d...\n",num);
        while(num<=lim)
        {
            vec[num][cnt[num]++]=primes[i];
            num+=primes[i];
        }
    }

    for(int i=2;i<=lim;i++)
    {
        int ii=i;
        for(int j=0;j<cnt[i];j++)
        {
            pw[j]=0;
            int num=vec[i][j];
            while(ii%num==0)
            {
                pw[j]++;
                ii/=num;
            }
        }
        n=i;
        //arr[i]=rec(0);
    }
    for(int i=2;i<=lim;i++)
    arr[i]+=arr[i-1];

    while(scanf("%lld",&n)==1&&n)
    {
        printf("%lld\n",arr[n]);
    }

    return 0;
}


