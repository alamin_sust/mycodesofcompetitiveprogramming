#include<stdio.h>
#include<math.h>
main()
{
    int n,i;
    double pi=2.0*acos(0),r1,r2,r3,a,b,c,cA,cB,cC,angA,angB,angC,res,Area;
    scanf(" %d",&n);
    for(i=1;i<=n;i++)
    {
        scanf(" %lf %lf %lf",&r1,&r2,&r3);
        a=r2+r3;
        b=r1+r3;
        c=r1+r2;
        angA=acos((b*b + c*c - a*a)/(2.0*b*c));
        angA=angA*180.0/pi;
        angB=acos((a*a + c*c - b*b)/(2.0*a*c));
        angB=angB*180.0/pi;
        angC=acos((a*a + b*b - c*c)/(2.0*a*b));
        angC=angC*180.0/pi;
        Area = b*c*(sin(angA*pi/180.0)/2.0);
        cA= pi*r1*r1*angA/360.0;
        cB= pi*r2*r2*angB/360.0;
        cC= pi*r3*r3*angC/360.0;
        res=Area-(cA+cB+cC);
        printf("%.6lf\n",i,res);
    }
    return 0;
}
