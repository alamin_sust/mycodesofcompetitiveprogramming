/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll n,a[1000010],b[1000010],c[1000010],dp[2][2][1000010];

ll rec(ll ppre,ll pre,ll pos)
{
    //printf("%lld %lld %lld\n",ppre,pre,pos);
    if(pos==(n+1LL))
    {
        if(pre==0LL)
            return 0LL;
        if(ppre)
        {
            //printf("%lld\n",b[n]);
            return b[n];}
        return a[n];
    }
    ll &ret=dp[ppre][pre][pos];
    if(ret!=-1LL)
        return ret;
    ret=0LL;
    if(pre&&ppre)
    ret=max(c[pos-1LL]+rec(1LL,1LL,pos+1LL),b[pos-1LL]+rec(1LL,0LL,pos+1LL));
    else if(pre)
    ret=max(b[pos-1LL]+rec(1LL,1LL,pos+1LL),a[pos-1LL]+rec(1LL,0LL,pos+1LL));
    else if(ppre)
    ret=max(rec(0LL,1LL,pos+1LL),rec(0LL,0LL,pos+1LL));
    else
    ret=max(rec(0LL,1LL,pos+1LL),rec(0LL,0LL,pos+1LL));
    return ret;
}

int main()
{
    ll i;
    while(cin>>n)
    {
        for(i=1LL;i<=n;i++)
        {
            scanf(" %lld %lld %lld",&a[i],&b[i],&c[i]);
        }
        for(i=0LL;i<=n+2LL;i++)
            dp[0][0][i]=dp[0][1][i]=dp[1][0][i]=dp[1][1][i]=-1LL;
        printf("%lld\n",rec(0LL,0LL,1LL));
    }
    return 0;
}

