/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node{

ll x,y,lev,typ;

};

queue<node>q;
ll arr[3010][3010],nowlev,res,khali,r,c,levsum,levarr[3010];

void bfs()
{
    while(!q.empty())
    {
        node now=q.front();
        arr[now.x][now.y]=1LL;
        q.pop();
        if(now.lev==nowlev)
        {
            if(nowlev>1LL)
            levsum+=(r+c+1LL-((nowlev-1LL)*2LL))-levarr[nowlev-1];
            res+=(khali-levsum);
            printf("%lld\n",res);
            nowlev++;
        }
        if(now.typ==3LL)
        {
            node to;
            if((now.x+1)<=r&&(now.y+1)<=c&&arr[now.x+1][now.y+1]==0LL)
            {
                to.typ=3LL;
                to.x=now.x+1LL;
                to.y=now.y+1LL;
                to.lev=now.lev+1LL;
                khali--;
                levarr[min(to.x,to.y)]++;
                q.push(to);
            }
            if((now.x+1)<=r&&arr[now.x+1][now.y]==0LL)
            {
                to.typ=1LL;
                to.x=now.x+1LL;
                to.y=now.y;
                to.lev=now.lev+1LL;
                khali--;
                levarr[min(to.x,to.y)]++;
                q.push(to);
            }
            if((now.y+1)<=c&&arr[now.x][now.y+1]==0LL)
            {
                to.typ=2LL;
                to.x=now.x;
                to.y=now.y+1LL;
                to.lev=now.lev+1LL;
                khali--;
                levarr[min(to.x,to.y)]++;
                q.push(to);
            }
        }
        else if(now.typ==1)
        {
            node to;
            if((now.x+1)<=r&&arr[now.x+1][now.y]==0LL)
            {
                to.typ=1LL;
                to.x=now.x+1LL;
                to.y=now.y;
                to.lev=now.lev+1LL;
                khali--;
                levarr[min(to.x,to.y)]++;
                q.push(to);
            }
        }
        else
        {
            node to;
            if((now.y+1)<=c&&arr[now.x][now.y+1]==0LL)
            {
                to.typ=2LL;
                to.x=now.x;
                to.y=now.y+1LL;
                to.lev=now.lev+1LL;
                khali--;
                levarr[min(to.x,to.y)]++;
                q.push(to);
            }
        }
    }
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);


    ll t,p,k,i,x,y;

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++)
    {
        memset(arr,0LL,sizeof(arr));
        memset(levarr,0LL,sizeof(levarr));
        scanf(" %lld %lld %lld",&r,&c,&k);
        while(!q.empty())
            q.pop();
        khali=r*c;
        for(i=1LL;i<=k;i++)
        {
            scanf(" %lld %lld",&x,&y);
            arr[x+1][y+1]=1;
            node det;
            det.x=x+1LL;
            det.y=y+1LL;
            det.typ=3LL;
            det.lev=1LL;
            khali--;
            q.push(det);
            levarr[max(det.x,det.y)]++;
        }
        res=0LL;
        nowlev=1LL;
        levsum=0LL;
        bfs();
        printf("Case #%lld: %lld\n",p,res);
    }

    return 0;
}






