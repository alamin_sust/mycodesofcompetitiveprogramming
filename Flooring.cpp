/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};



ll pow(ll a, ll b, ll MOD) {
ll x = 1, y = a;
    while(b > 0) {
        if(b%2 == 1) {
            x=(x*y);
            if(x>MOD) x%=MOD;
        }
        y = (y*y);
        if(y>MOD) y%=MOD;
        b /= 2;
    }
    return x;
}

ll modInverse(ll a, ll m) {
    return pow(a,m-2,m);
}



ll sum(ll from, ll to, ll MOD)
{
    ll n=to;
    ll ret=((modInverse(5,MOD)*(n%MOD)*(n%MOD)*(n%MOD)*(n%MOD)*(n%MOD))%MOD + (modInverse(2,MOD)*(n%MOD)*(n%MOD)*(n%MOD)*(n%MOD))%MOD + (modInverse(3,MOD)*(n%MOD)*(n%MOD)*(n%MOD))%MOD - (modInverse(30,MOD)*(n%MOD))%MOD )%MOD;
    //n=from-1;
    //ret-=((modInverse(5,MOD)*(n%MOD)*(n%MOD)*(n%MOD)*(n%MOD)*(n%MOD))%MOD + (modInverse(2,MOD)*(n%MOD)*(n%MOD)*(n%MOD)*(n%MOD))%MOD + (modInverse(3,MOD)*(n%MOD)*(n%MOD)*(n%MOD))%MOD - (modInverse(30,MOD)*(n%MOD))%MOD )%MOD;
    return ret%MOD;
}

main()
{
    ll p,t,n,m,res,i;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n>>m;
        printf("%lld\n",sum(n,m, 10));
    }
    return 0;
}
/*
99
111111111111111111111111111111111111111111111111100000000000000000111111111111111111111111110000111
*/
