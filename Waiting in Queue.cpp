/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll t,p,k,l,i,m,n,res,next_enter_time,arr[100010];

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %lld", &t);

    for(p=1LL; p<=t; p++)
    {
        scanf(" %lld %lld %lld %lld",&n,&m,&k,&l);
        for(i=0; i<n; i++)
        {
            scanf(" %lld",&arr[i]);
        }
        sort(arr,arr+n);
        res = next_enter_time = l*(m+1LL);
        for(i=0LL; i<n; i++)
        {
            if(arr[i]<next_enter_time)
            {
                res = min(res,next_enter_time-arr[i]);
                next_enter_time+=l;
            }
            else
            {
                res = 0LL;
                break;
            }
        }
        printf("%lld\n",min(res,max(0LL,next_enter_time-k)));
    }

    return 0;
}
