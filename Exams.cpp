/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
int A,B;
};

int comp(node a,node b)
{
    if(a.A==b.A)
        return a.B<b.B;
    return a.A<b.A;
}

node arr[5010];
int i,n,res,tmp;

int main()
{
    cin>>n;
    for(i=0;i<n;i++)
    {
        scanf(" %d %d",&arr[i].A,&arr[i].B);
    }
    sort(arr,arr+n,comp);
    res=0;
    for(i=0;i<n;)
    {
        tmp=arr[i].A;
        while(i<n&&tmp==arr[i].A)
        {
            if(arr[i].B>=res&&arr[i].A>=res)
            res=max(res,min(arr[i].B,arr[i].A));
            else if(arr[i].B>=res)
            res=max(res,arr[i].B);
            else if(arr[i].A>=res)
            res=max(res,arr[i].A);
            i++;
        }
    }
    printf("%d\n",res);
    return 0;
}


