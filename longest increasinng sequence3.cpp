#include<stdio.h>
#include<string.h>
#include<iostream>
using namespace std;
int arr[100];

void printpath(int *arr, int *path, int end)
{
    if(end> -1){
        printpath(arr,path,path[end]);
        printf("%d ",arr[end]);
    }
}
void longestincreasingsequence(int n)
{
    int *s = new int[n];    //current max number of ints
    int *path = new int[n]; //previous number
    memset(path,0,n);

    int global_max = 1;        //at least one number
    int end_pos = 0;

    s[0]= 1;
    path[0]= -1;
    for(int i=1;i<n;i++){
        int local_prev = -1;
        s[i] = 1;
        for(int j=0;j<i;j++){
            if(arr[j]<arr[i] && s[i]<(s[j]+1)){
                s[i]= s[j]+1;
                local_prev = j;
            }
        }

        path[i] = local_prev;
        if(global_max < s[i]){
            global_max = s[i];
            end_pos = i;
        }
    }

    printf(" size of longest seq : %d \n",global_max);
    printpath(arr,path,end_pos);
    printf("\n");
   // delete(path);
   // delete(s);
}
int main()
{
    arr[0]=1;
    arr[1]=1;
    arr[2]=3;
    arr[3]=2;
    arr[4]=4;
    arr[5]=3;
    arr[6]=5;
    arr[7]=4;
    arr[8]=6;
   // 2,4,3,5,4,6};
    longestincreasingsequence(9);
}
