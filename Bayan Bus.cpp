/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

string arr[10];
int n;

main()
{
    arr[0]="+------------------------+";
    arr[1]="|#.#.#.#.#.#.#.#.#.#.#.|D|)";
    arr[2]="|#.#.#.#.#.#.#.#.#.#.#.|.|";
    arr[3]="|#.......................|";
    arr[4]="|#.#.#.#.#.#.#.#.#.#.#.|.|)";
    arr[5]="+------------------------+";
    cin>>n;
    if(n>=1)
    arr[1][1]='O';
    if(n>=2)
    arr[2][1]='O';
    if(n>=3)
    arr[3][1]='O';
    if(n>=4)
    arr[4][1]='O';

    if(n>=5)
    arr[1][3]='O';
    if(n>=6)
    arr[2][3]='O';
    if(n>=7)
    arr[4][3]='O';

    if(n>=8)
    arr[1][5]='O';
    if(n>=9)
    arr[2][5]='O';
    if(n>=10)
    arr[4][5]='O';

    if(n>=11)
    arr[1][7]='O';
    if(n>=12)
    arr[2][7]='O';
    if(n>=13)
    arr[4][7]='O';

    if(n>=14)
    arr[1][9]='O';
    if(n>=15)
    arr[2][9]='O';
    if(n>=16)
    arr[4][9]='O';

    if(n>=17)
    arr[1][11]='O';
    if(n>=18)
    arr[2][11]='O';
    if(n>=19)
    arr[4][11]='O';

    if(n>=20)
    arr[1][13]='O';
    if(n>=21)
    arr[2][13]='O';
    if(n>=22)
    arr[4][13]='O';

    if(n>=23)
    arr[1][15]='O';
    if(n>=24)
    arr[2][15]='O';
    if(n>=25)
    arr[4][15]='O';

    if(n>=26)
    arr[1][17]='O';
    if(n>=27)
    arr[2][17]='O';
    if(n>=28)
    arr[4][17]='O';

    if(n>=29)
    arr[1][19]='O';
    if(n>=30)
    arr[2][19]='O';
    if(n>=31)
    arr[4][19]='O';

    if(n>=32)
    arr[1][21]='O';
    if(n>=33)
    arr[2][21]='O';
    if(n>=34)
    arr[4][21]='O';

    cout<<arr[0]<<endl;
    cout<<arr[1]<<endl;
    cout<<arr[2]<<endl;
    cout<<arr[3]<<endl;
    cout<<arr[4]<<endl;
    cout<<arr[5]<<endl;

    return 0;
}
