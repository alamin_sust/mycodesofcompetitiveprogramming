/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int dp[2010],n,k,arr[2010];

int rec(int pos)
{
    if(pos==n)
        return 0;
    int &ret=dp[pos];
    if(ret!=-1)
        return ret;
    ret=99999999;
    for(int i=1;i<=k;i++)
    {
        if((i+pos)>n)
            break;
        ret=min(ret,(arr[pos+i]*2)+rec(pos+i));
    }
    return ret;
}

main()
{
    int i;
    cin>>n>>k;
    for(i=1;i<=n;i++)
        scanf(" %d",&arr[i]),arr[i]--;
    sort(arr+1,arr+1+n);
    memset(dp,-1,sizeof(dp));
    cout<<rec(0)<<endl;
    return 0;
}

