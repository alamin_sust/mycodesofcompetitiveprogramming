#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

int dp[53][53],arr[1010],n;

int dp_(int st,int en)
{
    int &ret=dp[st][en];
    if(st+1==en)
        return 0;
    if(ret!=-1)
        return ret;
    ret=0;
    int cost=inf;
    for(int i=st+1;i<en;i++)
    {
            cost=min(cost,dp_(st,i)+dp_(i,en)+arr[en]-arr[st]);
    }
    return ret=cost;
}

main()
{
    int res,len,i;
    while(1)
    {
      memset(dp,-1,sizeof(dp));
      scanf(" %d",&len);
      if(len==0)
        break;
      scanf(" %d",&n);
      for(i=1;i<=n;i++)
        scanf(" %d",&arr[i]);
        arr[0]=0;
        arr[n+1]=len;
      res=dp_(0,n+1);
      printf("The minimum cutting is %d.\n",res);
    }
    return 0;
}
