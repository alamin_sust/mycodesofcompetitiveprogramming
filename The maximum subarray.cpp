/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int mx,negs,res,p,t,csum,n,i,csu,val,cind,bestsum,bestsind,besteind,arr[100010],cum[100010];

int main()
{
    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        negs=0;
        res=0;
        mx=-10000000;
        scanf(" %d",&n);
        for(i=0;i<n;i++)
        {
            scanf(" %d",&arr[i]);
            mx=max(mx,arr[i]);
            if(arr[i]>=0)
                {res+=arr[i];
                negs=1;}
        }

        if(negs==0)
        {
            printf("%d %d\n",mx,mx);
            continue;
        }


        csum=0;
        cind=-1;
        bestsum=0;
        bestsind=-1;
        besteind=-1;
        for(i=0;i<n;i++)
        {
            val=csum+arr[i];
            if(val>0)
            {
                if(csum==0)
                    cind=i;
                csum=val;
            }
            else
            csum=0;

            if(csum>bestsum)
                {bestsum=csum;
            bestsind=cind;
            besteind=i;}
        }
        cum[0]=arr[0];
        for(i=1;i<n;i++)
            cum[i]=cum[i-1]+arr[i];
        printf("%d %d\n",cum[besteind]-cum[bestsind-1],res);
    }
    return 0;
}




