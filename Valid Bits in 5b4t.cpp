#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define PB push_back
using namespace std;

int dp[55][55][55],n;

int rec(int taken_positives,int taken_negatives,int taken_zeroes)  //Valid Signal Elements Calculation
{
    if((taken_positives+taken_negatives+taken_zeroes)==n)
    {
        if(taken_positives-taken_negatives==0||taken_positives-taken_negatives==1)
        {
            return 1;
        }
        return 0;
    }
    int &ret=dp[taken_positives][taken_negatives][taken_zeroes];
    if(ret!=-1)
        return ret;
    ret=0;
    ret+=rec(taken_positives+1,taken_negatives,taken_zeroes);
    ret+=rec(taken_positives,taken_negatives+1,taken_zeroes);
    ret+=rec(taken_positives,taken_negatives,taken_zeroes+1);
    return ret;
}

int get_power(int a,int b)  //Calculates a^b (a to the power b)
{
    if(b==0)
        return 1;
    return a*get_power(a,b-1);
}

int main()
{
    while(cin>>n)
    {
    memset(dp,-1,sizeof(dp)); // The code calculates mBnT;
    printf("Total Available Signal Elements in %dT: %d\n",n,get_power(3,n));
    printf("Total Valid Signal Elements in %dT: %d\n",n,rec(0,0,0));
    }
    return 0;
}

