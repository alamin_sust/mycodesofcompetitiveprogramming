/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll t,p,cum[2000010],in[2000010],n,q,i,j,li,ri;
vector<ll>v;

int main()
{
    freopen("D-small-attempt1.in","r",stdin);
    freopen("SumofSumsOut.txt","w",stdout);

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++)
    {
        v.clear();
        scanf(" %lld %lld",&n,&q);
        for(i=1LL;i<=n;i++)
        {
            scanf(" %lld",&in[i]);
            cum[i]=cum[i-1]+in[i];
        }
        v.push_back(0LL);
        for(i=1LL;i<=n;i++)
        {
            for(j=i;j<=n;j++)
            {
                v.push_back(cum[j]-cum[i-1]);
            }
        }
        sort(v.begin(),v.end());
        for(i=1LL;i<v.size();i++)
        {
            cum[i]=cum[i-1]+v[i];
        }
        printf("Case #%lld:\n",p);
        for(i=1LL;i<=q;i++)
        {
            scanf(" %lld %lld",&li,&ri);
            printf("%lld\n",cum[ri]-cum[li-1]);
        }
    }

    return 0;
}

