/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


stack<int>stk;
int stat[1010],u,v,par[1010],val[1010];
vector<int>adj[1010];

void dfs(int from,int to)
{
    while(!stk.empty())
        stk.pop();
    memset(stat,0,sizeof(stat));
    memset(par,0,sizeof(par));
    //memset(val,0,sizeof(val));
    //val[from]=0;
    stk.push(from);
    while(!stk.empty())
    {
        u=stk.top();
        stk.pop();
        for(int i=0;i<adj[u].size();i++)
        {
            v=adj[u][i];
            if(stat[v]==0)
            {
                stat[v]=1;
                par[v]=u;
                val[v]=val[u]+1;
                if(v==to)
                    return;
                stk.push(v);
            }
        }
    }
    return;
}


int main()
{
    int n,e,i,from,to;

    cin>>n>>e;

    for(i=0;i<e;i++)
    {
        scanf(" %d %d",&from,&to);
        adj[from].push_back(to);
      // adj[to].push_back(from);
    }

    scanf(" %d %d",&from,&to);

    dfs(from,to);

    //printf("%d\n",val[to]);
    while(par[to]!=0)
    {
        printf("%d ",to);
        to=par[to];
    }
    printf("%d\n",from);
    return 0;
}




