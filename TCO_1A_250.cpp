#include <sstream>
#include <string.h>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class Similars
{
public:
	int maxsim(int L, int R)
	{
	    int a,b,A[10],B[10],cnt;
	    int ret=0;
        for(int i=L;i<R;i++)
        {
            memset(A,0,sizeof(A));
            memset(B,0,sizeof(B));
            for(int k=0,j=i+1;k<=101&&j<=R;k++,j++)
            {
                a=i;
                b=j;
                while(a)
                {
                    A[a%10]=1;
                    a/=10;
                }
                while(b)
                {
                    B[b%10]=1;
                    b/=10;
                }
                cnt=0;
                for(int x=0;x<=9;x++)
                {
                    if(A[x]&&B[x])
                        cnt++;
                }
                ret=max(ret,cnt);
            }
        }
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	Similars objectSimilars;

	//test case0
	int param00 = 1;
	int param01 = 10;
	int ret0 = objectSimilars.maxsim(param00,param01);
	int need0 = 1;
	assert_eq(0,ret0,need0);

	//test case1
	int param10 = 1;
	int param11 = 99;
	int ret1 = objectSimilars.maxsim(param10,param11);
	int need1 = 2;
	assert_eq(1,ret1,need1);

	//test case2
	int param20 = 99;
	int param21 = 100;
	int ret2 = objectSimilars.maxsim(param20,param21);
	int need2 = 0;
	assert_eq(2,ret2,need2);

	//test case3
	int param30 = 1000;
	int param31 = 1010;
	int ret3 = objectSimilars.maxsim(param30,param31);
	int need3 = 2;
	assert_eq(3,ret3,need3);

	//test case4
	int param40 = 444;
	int param41 = 454;
	int ret4 = objectSimilars.maxsim(param40,param41);
	int need4 = 2;
	assert_eq(4,ret4,need4);
}
