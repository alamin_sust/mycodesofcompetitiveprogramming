#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

bool prime[1000020];
int tree[80010],arr[10010],res,lazy[80010],upd[80010];

void sieve(void)
{
    int i,j;
    prime[1]=prime[0]=1;
    for(i=4; i<=1000010; i+=2)
        prime[i]=1;
    for(i=3; i<=1010; i++)
    {
        if(prime[i]==0)
            for(j=i*i; j<=1000010; j+=i+i)
                prime[j]=1;

    }
    //printf("%d..\n",k);
    return;
}

void build(int node,int i,int j)
{
    if(i==j)
    {
        tree[node]=arr[j];
        return;
    }
    int mid=(i+j)/2;
    build(node*2,i,mid);
    build((node*2)+1,mid+1,j);
    tree[node]=tree[node*2]+tree[(node*2)+1];
    return;
}

int query(int node,int f,int t,int x,int y)
{
    if(upd[node])
    {
        int mid=(t+f)/2;
        if(f!=t)
        {
            tree[node*2]=lazy[node]*(mid-f+1);
            tree[(node*2)+1]=lazy[node]*(t-mid);
            upd[node*2]=upd[(node*2)+1]=1;
            lazy[node*2]=lazy[(node*2)+1]=lazy[node];
        }
        upd[node]=0;
    }
    if(x<=f&&y>=t)
    {
        return tree[node];
    }
    int sum=0;
    int mid=(f+t)/2;
    if(x<=mid)
        sum+=query(node*2,f,mid,x,y);
    if(y>mid)
        sum+=query((node*2)+1,mid+1,t,x,y);
    tree[node]=tree[node*2]+tree[(node*2)+1];
    return sum;
}

void update(int node,int f,int t,int x,int y,int val)
{
    if(x<=f&&y>=t)
    {
        if(f!=t)
        {
            upd[node]=1;
            lazy[node]=val;
        }
        tree[node]=val*(t-f+1);
        return;
    }
    if(upd[node])
    {
        int mid=(t+f)/2;
        if(t!=f)
        {
            tree[node*2]=lazy[node]*(mid-f+1);
            tree[(node*2)+1]=lazy[node]*(t-mid);
            upd[node*2]=upd[(node*2)+1]=1;
            lazy[node*2]=lazy[(node*2)+1]=lazy[node];
        }
        upd[node]=0;
    }
    int mid=(f+t)/2;
    if(x<=mid)
        update(node*2,f,mid,x,y,val);
    if(y>mid)
        update((node*2)+1,mid+1,t,x,y,val);
    tree[node]=tree[node*2]+tree[node*2+1];
    return;
}

main()
{
    int i,t,n,v,q,p,com,x,y;
    cin>>t;
    sieve();
    for(p=1; p<=t; p++)
    {
        memset(upd,0,sizeof(upd));
        memset(lazy,0,sizeof(lazy));
        scanf("%d %d",&n,&q);
        for(i=1; i<=n; i++)
        {
            cin>>arr[i];
            arr[i]=!prime[arr[i]];
        }
        build(1,1,n);
        //for(i=1;i<=10;i++)
        //  printf("%d ",tree[i]);
        printf("Case %d:\n",p);
        for(i=1; i<=q; i++)
        {
            scanf("%d",&com);
            if(com==0)
            {
                scanf("%d %d %d",&x,&y,&v);
                update(1,1,n,x,y,!prime[v]);
            }
            else
            {
                scanf("%d %d",&x,&y);
                res=query(1,1,n,x,y);
                printf("%d\n",res);
            }
        }
    }
    return 0;
}

