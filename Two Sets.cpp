/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll n,a,b,i,arr[100010],flag[100010],res[100010];
map<ll,ll>mpp;

main()
{
    cin>>n>>a>>b;
    for(i=1;i<=n;i++)
    {
        scanf(" %I64d",&arr[i]);
        mpp[arr[i]]=i;
    }
    for(i=1;i<=n;i++)
    {
        if(flag[i]==0)
        {
            if((a-arr[i])>=0&&mpp[a-arr[i]]>0)
            {
                flag[mpp[a-arr[i]]]=1;
                mpp[arr[i]]=0;
                mpp[a-arr[i]]=0;
                flag[i]=1;
            }
            else if((b-arr[i])>=0&&mpp[b-arr[i]]>0)
            {
                res[mpp[b-arr[i]]]=1;
                res[i]=1;
                flag[mpp[b-arr[i]]]=1;
                mpp[arr[i]]=0;
                mpp[b-arr[i]]=0;
                flag[i]=1;
            }
            else
            {
                printf("NO\n");
                return 0;
            }
        }
    }
    printf("YES\n");
    for(i=1;i<=n;i++)
        printf("%I64d ",res[i]);
    printf("\n");
    return 0;
}

