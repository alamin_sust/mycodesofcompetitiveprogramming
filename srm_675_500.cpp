#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

double dp[55][55][55];
double mn=99.0;
int K,n;
vector<string>D;

double rec(int pos,int now,int used,double nowval)
{
    if(now==1)
    mn=min(mn,dp[pos][now][used]);
    if(pos==n-1)
        return dp[pos][now][used];
    if(dp[pos][now][used]<=nowval)
    {
        if(now==1)
        mn=min(mn,dp[pos][now][used]);
        return dp[pos][now][used];
    }
    for(int i=0;i<n;i++)
    {
        if(i!=now)
        {
            dp[pos][now][used]=rec(pos+1,i,used,nowval+(double)(D[now][i]-'0'));
            if(used<K)
            dp[pos][now][used]=rec(pos+1,i,used+1,nowval+(double)(D[now][i]-'0')/2.0);
        }

    }
    return dp[pos][now][used];
}

class ShortestPathWithMagic
{
public:
	double getTime(vector <string> dist, int k)
	{
	    K=k;
	    n=dist.size();
	    D=dist;
        for(int i=0;i<53;i++)
        for(int j=0;j<53;j++)
        for(int p=0;p<53;p++)
            dp[i][j][p]=999.0;
        rec(0,0,0,0.0);
        return mn;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	ShortestPathWithMagic objectShortestPathWithMagic;

	//test case0
	vector <string> param00;
	param00.push_back("094");
	param00.push_back("904");
	param00.push_back("440");
	int param01 = 1;
	double ret0 = objectShortestPathWithMagic.getTime(param00,param01);
	double need0 = 4.5;
	assert_eq(0,ret0,need0);

	//test case1
	vector <string> param10;
	param10.push_back("094");
	param10.push_back("904");
	param10.push_back("440");
	int param11 = 2;
	double ret1 = objectShortestPathWithMagic.getTime(param10,param11);
	double need1 = 4.0;
	assert_eq(1,ret1,need1);

	//test case2
	vector <string> param20;
	param20.push_back("094");
	param20.push_back("904");
	param20.push_back("440");
	int param21 = 50;
	double ret2 = objectShortestPathWithMagic.getTime(param20,param21);
	double need2 = 4.0;
	assert_eq(2,ret2,need2);

	//test case3
	vector <string> param30;
	param30.push_back("094");
	param30.push_back("904");
	param30.push_back("440");
	int param31 = 0;
	double ret3 = objectShortestPathWithMagic.getTime(param30,param31);
	double need3 = 8.0;
	assert_eq(3,ret3,need3);

	//test case4
	vector <string> param40;
	param40.push_back("076237");
	param40.push_back("708937");
	param40.push_back("680641");
	param40.push_back("296059");
	param40.push_back("334508");
	param40.push_back("771980");
	int param41 = 1;
	double ret4 = objectShortestPathWithMagic.getTime(param40,param41);
	double need4 = 3.5;
	assert_eq(4,ret4,need4);

	//test case5
	vector <string> param50;
	param50.push_back("00");
	param50.push_back("00");
	int param51 = 50;
	double ret5 = objectShortestPathWithMagic.getTime(param50,param51);
	double need5 = 0.0;
	assert_eq(5,ret5,need5);

}
