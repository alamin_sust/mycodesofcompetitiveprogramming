/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int n,i,k,arr[100010];

main()
{
    cin>>n;
    for(i=1;i<=n;i++)
    {
        scanf(" %d",&arr[i]);
    }
    for(i=2;i<=n;i++)
    {
        if(arr[i]<arr[i-1])
            break;
    }
    if(i>n)
    {
        printf("0\n");
        return 0;
    }
    k=n-i+1;
    for(i++;i<=n;i++)
    {
        if(arr[i]<arr[i-1])
            break;
    }
    if(arr[n]>arr[1]||i<=n)
        printf("-1\n");
    else
        printf("%d\n",k);
    return 0;
}


