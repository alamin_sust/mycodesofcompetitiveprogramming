#include<stdio.h>
#include<string.h>

main()
{
    int same,temp,k3,k4,i,j,k,k1,k2,l1,l2,a1[10010],a2[10010],flag;
    char comp1[10010],comp2[10010],arr1[10010],arr2[10010];
    while(gets(arr1))
    {
        gets(arr2);
        l1=strlen(arr1);
        l2=strlen(arr2);
        for(k1=0,i=l1-1; i>=0; i--)
        {
            while(i>=0&&arr1[i]==' ')
                i--;
            a1[k1]=0;
            k=1;
            if(i>=0&&arr1[i]!=' ')
            {
                while(i>=0&&arr1[i]!=' ')
                {
                    a1[k1]+=(arr1[i]-'0')*k;
                    k*=10;
                    i--;
                }
                k1++;
            }
        }
        for(k2=0,i=l2-1; i>=0; i--)
        {
            while(i>=0&&arr2[i]==' ')
                i--;
            a2[k2]=0;
            k=1;
            if(i>=0&&arr2[i]!=' ')
            {
                while(i>=0&&arr2[i]!=' ')
                {
                    a2[k2]+=(arr2[i]-'0')*k;
                    k*=10;
                    i--;
                }
                k2++;
            }
        }
        for(j=0; j<k1; j++)
        {
            for(i=1; i<k1; i++)
            {
                if(a1[i]<a1[i-1])
                {
                    temp=a1[i];
                    a1[i]=a1[i-1];
                    a1[i-1]=temp;
                }
            }
        }
        for(j=0; j<k2; j++)
        {
            for(i=1; i<k2; i++)
            {
                if(a2[i]<a2[i-1])
                {
                    temp=a2[i];
                    a2[i]=a2[i-1];
                    a2[i-1]=temp;
                }
            }
        }
        for(k3=0,i=0; i<k1; i++)
        {
            comp1[k3]=a1[i];
            while(i+1<k1&&a1[i]==a1[i+1])
                i++;
            k3++;
        }
        for(k4=0,i=0; i<k2; i++)
        {
            comp2[k4]=a2[i];
            while(i+1<k2&&a2[i]==a2[i+1])
                i++;
            k4++;
        }
        same=0;
        if(k3==k4)
        {
            for(i=0; i<k3; i++)
            {
                if(comp1[i]!=comp2[i])
                {
                    same=1;
                    break;
                }
            }
            if(same==0)
            {
                printf("A equals B\n");
                continue;
            }
        }

        for(k=0,i=0,j=0; i<k3&&j<k4;)
        {
            if(comp1[i]==comp2[j])
            {
                i++;j++;k++;
            }
            else
            j++;
        }
        if(k==k3)
        {
            printf("A is a proper subset of B\n");
            continue;
        }
        for(k=0,i=0,j=0; i<k4&&j<k3;)
        {

            if(comp2[i]==comp1[j])
            {
                i++;j++;k++;
            }
            else
            j++;
        }
        if(k==k4)
        {
            printf("B is a proper subset of A\n");
            continue;
        }
        for(flag=0,i=0;flag==0&&i<k3;i++)
        {
            for(j=0;j<k4;j++)
            {
                if(comp1[i]==comp2[j])
                {flag=1;
                break;}
            }
        }
        if(flag==1)
        {printf("I'm confused!\n");
        continue;}
        printf("A and B are disjoint\n");
    }
    return 0;
}
