/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int arr[6],i,n,k,j,sum,sum2,res;

int main()
{
    while(cin>>n)
    {
        for(i=0; i<n; i++)
            scanf(" %d",&arr[i]);
        if(n<4)
        {
            printf("1024\n");
            continue;
        }
        res=0;
        if(n==4)
        {
            k=1<<4;
            for(i=0; i<k; i++)
            {
                if(__builtin_popcount(i)==3)
                {
                    sum=0;
                    for(j=0; j<4; j++)
                    {
                        if((1<<j)&i)
                            sum+=arr[j];
                    }
                    if((sum%1024)==0)
                    {
                        res=1024;
                        break;
                    }
                }
            }
            for(i=0; i<k; i++)
            {
                if(__builtin_popcount(i)==2)
                {
                    sum=0;
                    for(j=0; j<4; j++)
                    {
                        if((1<<j)&i)
                            sum+=arr[j];
                    }

                    if(sum>0&&(sum%1024)==0)
                            res=1024;
                    else res=max(res,sum%1024);
                        // if(sum2==0)
                        // printf("0\n");
                        // else
                        // printf("%d\n",(sum2%1024)==0?1024:(sum2%1024));

                }
            }
            printf("%d\n",res);
            continue;
        }
        k=1<<5;
        res=0;
        for(i=0; i<k; i++)
        {
            if(__builtin_popcount(i)==3)
            {
                sum=0;
                sum2=0;
                for(j=0; j<5; j++)
                {
                    if((1<<j)&i)
                        sum+=arr[j];
                    else
                        sum2+=arr[j];
                }
                if((sum%1024)==0)
                {
                    if(sum2>0&&(sum2%1024)==0)
                        res=1024;
                    else res=max(res,sum2%1024);
                    // if(sum2==0)
                    // printf("0\n");
                    // else
                    // printf("%d\n",(sum2%1024)==0?1024:(sum2%1024));

                }
            }
            if(i==k-1)
                printf("%d\n",res);
        }
    }
    return 0;
}

