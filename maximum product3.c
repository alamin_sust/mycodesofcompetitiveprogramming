#include<stdio.h>

main()
{
    long long int temp,ct=1,i,j,k,n,in,pos[30],neg[30],res;
    while(scanf("%lld",&n)!=EOF)
    {
        res=1;
        for(j=0,k=0,i=0;i<n;i++)
        {
            scanf(" %lld",&in);
            if(in>0)
            {pos[j]=in;
            j++;}
            else if(in<0)
            {neg[k]=in;
            k++;}
        }
        if(j==0&&k<=1)
        res=0;
        else
        {
            if(k%2==0)
            {
                for(res=1,i=0;i<j;i++)
                res*=pos[i];
                for(i=0;i<k;i++)
                res*=neg[i];
            }
            else
            {
                for(i=1;i<k;i++)
                {
                    if(neg[i]>neg[0])
                    {
                        temp=neg[0];
                        neg[0]=neg[i];
                        neg[i]=temp;
                    }
                }
                for(res=1,i=0;i<j;i++)
                res*=pos[i];
                for(i=1;i<k;i++)
                res*=neg[i];
            }
        }
        printf("Case #%lld: The maximum product is %lld.\n\n",ct,res);
        ct++;
    }
    return 0;
}
