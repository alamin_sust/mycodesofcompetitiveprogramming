#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
using namespace std;

main()
{
    int arr[510][510],i,j,row,col,query,res,p,x1,x2,y1,y2;
    while(1)
    {
        scanf(" %d %d %d",&row,&col,&query);
        if(row==0&&col==0&&query==0)
            break;
        memset(arr,0,sizeof(arr));
        for(p=0;p<query;p++)
        {
            scanf(" %d %d %d %d",&x1,&y1,&x2,&y2);
            if(x1>x2)
                swap(x1,x2);
            if(y1>y2)
                swap(y1,y2);
            for(i=x1;i<=x2;i++)
            {
                for(j=y1;j<=y2;j++)
                arr[i][j]=1;
            }
        }
        for(res=0,i=1;i<=row;i++)
        {
            for(j=1;j<=col;j++)
                res+=arr[i][j];
        }
        res=row*col-res;
        if(res==0)
        printf("There is no empty spots.\n");
        else if(res==1)
        printf("There is one empty spot.\n");
        else
        printf("There are %d empty spots.\n",res);
    }
    return 0;
}
