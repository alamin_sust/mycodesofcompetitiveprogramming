/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

main()
{
    int n,m;
    cin>>n>>m;
    if(n>0&&m>0)
    {
        if(n>m)
        {
        printf("%d %d\n",n,m);
        printf("0 0\n");
        printf("%d %d\n",n,m-1);
        if(m>3)
        printf("0 1\n");
        else
        printf("0 %d\n",m);
        }
        else if(m>n)
        {
        printf("%d %d\n",n,m);
        printf("0 0\n");
        printf("%d %d\n",n-1,m);
        if(n>3)
        printf("1 0\n");
        else
        printf("%d 0\n",n);
        }
        else
        {
        printf("%d %d\n",n,m);
        printf("0 0\n");
        printf("0 %d\n",m);
        printf("%d 0\n",n);
        }
    }
    else if(n>0)
    {
        printf("1 0\n");
        printf("%d 0\n",n);
        printf("0 0\n");
        printf("%d 0\n",n-1);
    }
    else if(m>0)
    {
        printf("0 1\n");
        printf("0 %d\n",m);
        printf("0 0\n");
        printf("0 %d\n",m-1);
    }
    return 0;
}


