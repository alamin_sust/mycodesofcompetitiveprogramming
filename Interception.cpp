/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int t,p,i,n,dummy;

int main()
{

    freopen("interception.txt","r",stdin);
    freopen("interception_output.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++) {
        scanf(" %d",&n);
        for(i=0;i<=n;i++) {
            scanf(" %d",&dummy);
        }
        if(n%2) {
            printf("Case #%d: 1\n0.0\n",p);
        } else {
            printf("Case #%d: 0\n",p);
        }
    }

    return 0;
}
