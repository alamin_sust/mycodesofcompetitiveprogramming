/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

char arr[1010][1010];
int mx,res,i,n,k,j,l[1010],cnt[1010],flag[1010][210],fg,x;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d",&n);

    for(i=1; i<=n; i++)
    {
        scanf(" %s",&arr[i]);
        l[i]=strlen(arr[i]);
        cnt[i]=0;
        for(j=0; j<l[i]; j++)
        {
            if(flag[i][arr[i][j]-'a']==0)
            {
                cnt[i]++;
                flag[i][arr[i][j]-'a']=1;
            }
        }
    }

    mx=0;
    for(i='a'; i<='z'; i++)
    {
        for(j='a'; j<='z'; j++)
        {
            res=0;
            for(k=1; k<=n; k++)
            {
                fg=0;
                if(cnt[k]<=2)
                {
                    for(x=0; x<26; x++)
                    {
                        if(x==(i-'a')||x==(j-'a'))
                            continue;
                        if(flag[k][x])
                        {
                            fg=1;
                            break;
                        }

                    }

                    if(fg==0)
                        res+=l[k];
                }
            }
            mx=max(res,mx);
        }
    }


    printf("%d\n",mx);


    return 0;
}


