#include <iostream>
#include <cstdio>
#define INT_MAX 2147483647
using namespace std;
int main ()
{
    int testCase;
    scanf ("%d", &testCase);
    while ( testCase-- ) {
        int bill;
        scanf ("%d", &bill);
        int coinNumber;
        scanf ("%d", &coinNumber);
        int coins [100 + 5];
        for ( int i = 0; i < coinNumber; i++ )
            scanf ("%d", &coins [i]);
        int dp [10000 + 10];
        for ( int i = 0; i < 10010; i++ )
            dp [i] = INT_MAX;
        dp [0] = 0;
        for ( int i = 0; i < coinNumber; i++ ) {
            for ( int j = 10000; j >= 0; j-- ) {
                if ( dp [j] != INT_MAX && j + coins [i] <= 10000 && dp [j + coins [i]] > dp [j] + 1 ) dp [j + coins [i]] = dp [j] + 1;
            }
        }
        for ( int i = bill; i <= 10000; i++ ) {
            if ( dp [i] != INT_MAX ) {
                printf ("%d %d\n", i, dp [i]);
                break;
            }
        }
    }
    return 0;
}

