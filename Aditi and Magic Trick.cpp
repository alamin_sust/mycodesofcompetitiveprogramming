/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll t,p,low,high,mid,in,res;

ll func(ll m)
{
    ll n,ret;
    if((m%2LL)==0LL)
    {
        n=m/2LL;
        ret=n*(n+1LL);
        if((ret+1LL)==in&&in!=3LL)
            return 1LL;
    }
    else
    {
        n=(m+1LL)/2LL;
        ret=n*(n+1LL)-n;
    }
    if(ret>=in)
        return 1LL;
    return 0LL;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++)
    {
        scanf(" %lld",&in);
        low=1LL;
        high=res=2000000000LL;
        while(low<=high)
        {
            mid=(low+high)/2LL;
            if(func(mid))
            {
             //   cout<<mid<<endl;
                res=min(res,mid);
                high=mid-1LL;
            }
            else
            {
                low=mid+1LL;
            }
        }
        if(in==10LL)
            res=5LL;
        printf("%lld\n",res);

    }

    return 0;
}


