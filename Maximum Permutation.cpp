/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int p,t,aa[26],ss[26],ls,la;
map<string,int>mpp;
char arr[200010],sub[20010];
vector<string>v;

void chk(int from,int to) {
    string s="";
    memset(aa,0,sizeof(aa));
    for(int i=from;i<to;i++)s+=arr[i],aa[arr[i]-'a']++;

    for(int i=0;i<26;i++) {
        if(aa[i]!=ss[i]) return;
    }
    mpp[s]++;
    if(mpp[s]==1)
    v.push_back(s);
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    int i;

    scanf(" %d",&t);

    for(p=1;p<=t;p++) {
        mpp.clear();
    v.clear();
        scanf(" %s",sub);
        scanf(" %s",arr);
        ls=strlen(sub);
        la=strlen(arr);

        memset(ss,0,sizeof(ss));
        for(i=0;i<ls;i++) ss[sub[i]-'a']++;

        for(i=0;i+ls<=la;i++) {
            chk(i,i+ls);
        }

        string res="-1";
        int rc=0;
        for(i=0;i<v.size();i++) {
            if(mpp[v[i]]>rc || (mpp[v[i]]==rc && res!="-1" && v[i]<res)) {
                res=v[i];
                rc=mpp[v[i]];
            }
        }
        cout<<res<<endl;

    }



    return 0;
}
