#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int sieve[2100];

void sieve_(void)
{
    for(int i=0; i<2010; i++)
    {
        sieve[i]=1;
    }
    sieve[0]=sieve[1]=0;
    for(int i=2; i<2010; i++)
    {
        while(sieve[i]==0 && i<2010)
        {
            i++;
        }
        for(int j=i*i; j<2010; j+=i)
        {
            sieve[j]=0;
        }
    }
}

main()
{
    while(getchar());
    char arr[10010];
    int det[200],n,p,l,i;
    sieve_();
    cin>>n;
    getchar();
    for(p=1;p<=n;p++)
    {
        memset(det,0,sizeof(det));
        gets(arr);
        l=strlen(arr);
        for(i=0;i<l;i++)
            det[arr[i]]++;
        printf("Case %d: ",p);
        int fg=0;
        for(i=0;i<200;i++)
        {
            if(sieve[det[i]]==1)
                printf("%c",i),fg=1;
        }
        if(fg==0)
            printf("empty");
        printf("\n");
    }
    return 0;
}

