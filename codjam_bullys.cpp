#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#define pi 2*acos(0)
#define eps 0.00000001
using namespace std;

main()
{
    FILE *read,*write;
    read=fopen("codejamround1ain.txt","r");
    write=fopen("codejamround1aout.txt","w");
    long long int i,r,t,T,j,res,p;
    fscanf(read," %lld",&T);
    for(i=1;i<=T;i++)
    {
        fscanf(read," %lld %lld",&r,&t);
        p=0;
        if(r%2==1)
        {
            for(res=0,j=r;j>0&&res<=t;j-=2)
            {
                res+=j*j;
                p++;
            }
            for(j=r+2;res<=t;j+=2)
            {
                res+=j*j;
                p++;
            }

        }
        else
        {
            for(res=0,j=r;j>0&&res<=t;j-=2)
            {
                res+=j*j;
                p++;
            }
            for(j=r+2;res<=t;j+=2)
            {
                res+=j*j;
                p++;
            }
        }
        fprintf(write,"Case #%lld: %lld\n",i,p-1);
    }
    fclose(read);
    fclose(write);
    return 0;
}

