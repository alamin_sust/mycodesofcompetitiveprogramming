#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ull m,E,p,t,high,low,mid,r,c;

ull get(ull n)
{
    m=n*2-1;
    ull ret=m*(m+1)*(2*m+1)/6;
    n--;
    ret-=2*n*(n+1)*(2*n+1)/3;
    ret-=E;
    return ret>c?0:1;
}

ull get2(ull n)
{
    n*=2;
    ull ret=n*(n+1)*(2*n+1)/6;
    n/=2;
    ret-=4*n*(n+1)*(2*n+1)/3;
    ret-=E;
    return ret>c?0:1;
}

main()
{
    //freopen("bullseye_in.txt","r",stdin);
    //freopen("bullseye_out.txt","w",stdout);
    ull mm=1000100000000000000LLU,rr=0LLU;
    for(p=1;mm>(pi*rr);p++)
        rr+=p*p;
    cout<<p;
    cin>>t;
    for(p=1; p<=t; p++)
    {
        cin>>r>>c;
        high=9084779LLU;
        //high/=2;
        low=0LLU;
        mid=(high+low)/2LLU;
        if((r%2)==1)
        {
            r/=2;
            if(r>=1)
            E=get(r);
            else
            E=0;
            while(1)
            {
                if(get(mid)==1)
                {
                    if(get(mid+1)==0)
                        break;
                    low=mid;
                }
                else
                    high=mid;
                mid=(low+high)/2LLU;
                printf("%lld\n",mid-r);
            }
        }
        else
        {
            m=r;
            E=m*(m+1)*(2*m+1)/3;
            while(1)
            {
                if(get2(mid))
                {
                    if(!get2(mid+1))
                        break;
                    low=mid;
                }
                else
                    high=mid;
                mid=(low+high)/2LLU;
                printf("%lld\n",mid);
            }
        }
        printf("Case #%llu: %llu",p,mid);
    }
    return 0;
}
