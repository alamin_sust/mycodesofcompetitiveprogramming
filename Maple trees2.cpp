/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};




double x[1010],y[1010],res,tres,high,low,tlow,thigh,tmid,tlres,trres,lres,rres,lowres,tlowres,highres,thighres,mid,tmidres,midres;
int n,cnt,tcnt,tlmid,trmid,lmid,rmid;

double func2(double X,double Y)
{
    double ret=0.0;
    for(int i=1;i<=n;i++)
    {
        ret=max(ret,sqrt((X-x[i])*(X-x[i])+(Y-y[i])*(Y-y[i])));
    }
    //cout<<ret<<"PP"<<endl;
    return ret;
}

double func(double val)
{
        tlow=-1000.0;
        thigh=1000.0;
        tcnt=30;
        tres=100000000000.0;
        while((tlow+eps)<=thigh)
        {
            tlmid=(thigh-tlow)/3.0+tlow;
            trmid=(thigh-tlow)*2.0/3.0+tlow;
            tmid=(tlow+thigh)/2.0;
            tlres=func2(val,tlmid);
            trres=func2(val,trmid);
            tlowres=func2(val,tlow);
            thighres=func2(val,thigh);
            tmidres=func2(val,tmid);
            tres=min(tlres,tres);
            tres=min(trres,tres);
            tres=min(tmidres,tres);
            if(tmidres<=tlres&&tmidres<=trres)
            {
                tlow=tlmid;
                thigh=trmid;
            }
            else if(tlres<trres&&tlowres<=thighres)
            {
                thigh=trmid;
            }
            else if(tlres>trres&&tlowres>=thighres)
            {
                tlow=tlmid;
            }
            else if(tlowres<=thighres)
            {
                thigh=trmid;
            }
            else
            {
                tlow=tlmid;
            }
        }
        return tres;
}





int main()
{
    int i;
    while(scanf("%d",&n)!=EOF&&n)
    {
        for(i=1;i<=n;i++)
        {
            scanf(" %lf %lf",&x[i],&y[i]);
        }

        low=-1000.0;
        high=1000.0;
        cnt=30;
        res=100000000000.0;
        while((low+eps)<=high)
        {
            lmid=(high-low)/3.0+low;
            rmid=(high-low)*2.0/3.0+low;
            mid=(low+high)/2.0;
            lres=func(lmid);
            rres=func(rmid);
            lowres=func(low);
            highres=func(high);
            midres=func(mid);
            //cout<<"low="<<low<<" high="<<high<<" lmid="<<lmid<<" rmid="<<rmid<<" lres="<<lres<<" rres="<<rres<<endl;

            res=min(lres,res);
            res=min(rres,res);
            res=min(midres,res);
            if(midres<=lres&&midres<=rres)
            {
                low=lmid;
                high=rmid;
            }
            else if(lres<rres&&lowres<=highres)
            {
                high=rmid;
            }
            else if(lres>rres&&lowres>=highres)
            {
                low=lmid;
            }
            else if(lowres<=highres)
            {
                high=rmid;
            }
            else
            {
                low=lmid;
            }
        }
        if(n==1)
        printf("0.50\n");
        else
        printf("%.2lf\n",res+0.5);

    }

    return 0;
}

