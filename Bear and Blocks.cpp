/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 1000000010
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int arr[100010],tree[400010];

void build(int l,int r,int id)
{
    if(l==r)
    {
        tree[id]=arr[l];
        return;
    }
    int mid=(l+r)/2;
    build(l,mid,id*2);
    build(mid+1,r,id*2+1);
    tree[id]=min(tree[id*2],tree[id*2+1]);
    return;
}

int query(int l,int r,int ql,int qr,int id)
{
    if((l<ql&&r<ql)||(l>qr&&r>qr))
        return inf;
    if(ql<=l&&qr>=r)
    {
        return tree[id];
    }
    int mid=(l+r)/2;
    int a1=query(l,mid,ql,qr,id*2);
    int a2=query(mid+1,r,ql,qr,id*2+1);
    return min(a1,a2);
}

int main()
{
    int n,i,res,res2,mid,low,high,k,resmid;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d",&n);

    for(i=1;i<=n;i++)
    {
        scanf(" %d",&arr[i]);
    }
    build(1,n,1);
    res=0;

    for(i=1;i<=n;i++)
    {
        high=res2=arr[i];
        low=0;
        k=40;
        while(low<=high&&k>0)
        {
            mid=(low+high)/2;
            resmid=min(i,1+query(1,n,max(1,i-mid+1),i-1,1));
            resmid=min(resmid,min(n-i+1,1+query(1,n,i+1,min(n,i+mid-1),1)));

            if(resmid<=mid)
            {
                res2=min(res2,mid);
                high=mid-1;
            }
            else
            {
                low=mid+1;
            }
            k--;
        }
        res=max(res,res2);
    }

    printf("%d\n",res);

    return 0;
}


