/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

double dp[3010][3010];
int n;
ll a,b;

double rec(int icpc,int acm)
{
    if(acm>b||icpc>a||(icpc+acm)>=n)
        return 0.0;
    double &ret=dp[icpc][acm];
    if(ret>-9)
        return ret;
    ret=(2+rec(icpc+1,acm)*(a-icpc)*2+rec(icpc,acm+1)*(b-acm))/((a-icpc+1)*2+b-acm);
    return ret;
}

double rec2(int icpc,int acm)
{
    if(acm>b||icpc>a||(icpc+acm)>=n)
        return 0.0;
    double &ret=dp[icpc][acm];
    if(ret>-9)
        return ret;
    ret=(1+rec2(icpc+1,acm)*(a-icpc)*2+rec2(icpc,acm+1)*(b-acm))/((a-icpc)*2+b-acm+1);
    return ret;
}

int main()
{
    while(cin>>n>>a>>b)
    {
        for(int i=1;i<=2;i++)
        {
            for(int j=0;j<n;j++)
                for(int k=0;k<n;k++)
                    dp[j][k]=-99;
            if(i==1)
            printf("%.16lf\n",rec(0,0));
            else
            printf("%.16lf\n",rec2(0,0));
        }
    }
    return 0;
}

