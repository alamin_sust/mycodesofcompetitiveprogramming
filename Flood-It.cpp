/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int rr[]={0,0,1,-1};
int cc[]={-1,1,0,0};

int nums[10],n,arr[22][22],flag[10],col[22][22],pos,colored,t,p,mx,sum;
char ch;



struct node{
int x,y;
};

queue<node>q;

node det,u,v;

void bfs(int x,int y)
{
    det.x=x;
    det.y=y;
    col[x][y]=2;
    q.push(det);
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(int i=0;i<4;i++)
        {
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            if(v.x<=n&&v.y<=n&&v.x>=1&&v.y>=1&&col[v.x][v.y]==0)
            {
                if(col[u.x][u.y]==2)
                {
                    if(arr[v.x][v.y]==arr[u.x][u.y])
                    {
                        col[v.x][v.y]=2;
                        colored++;
                    }
                    else
                    {
                        col[v.x][v.y]=1;
                        flag[arr[v.x][v.y]]++;
                    }
                    q.push(v);
                }
                else
                {
                    if(arr[v.x][v.y]==arr[u.x][u.y])
                    {
                        col[v.x][v.y]=1;
                        flag[arr[v.x][v.y]]++;
                        q.push(v);
                    }
                }
            }
        }
    }
    return;
}

int main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        getchar();
        for(int i=1;i<=n;i++)
        {
            for(int j=1;j<=n;j++)
            {
                scanf("%c",&ch);
                arr[i][j]=ch-'0';
            }
            getchar();
        }
        colored=0;
        //printf("....");
        memset(nums,0,sizeof(nums));
        sum=0;
        while(1)
        {
            memset(col,0,sizeof(col));
            memset(flag,0,sizeof(flag));
            bfs(1,1);
            mx=0;
            for(int i=1;i<=6;i++)
            {
                if(flag[i]>mx)
                {
                    mx=flag[i];
                    pos=i;
                }
            }
            if(flag[pos]==0)
                break;
            nums[pos]++;
            sum++;
            //printf("%d\n",pos);
            for(int i=1;i<=n;i++)
            {
                for(int j=1;j<=n;j++)
                {
                    if(col[i][j]==2)
                        arr[i][j]=pos;
                    if(col[i][j]==1&&arr[i][j]==pos)
                    {
                        col[i][j]=2;
                        colored++;
                    }
                }
            }
           /* for(int i=1;i<=n;i++)
            {
                for(int j=1;j<=n;j++)
                {
                    printf("%d",arr[i][j]);
                }
                printf("\n");
            }*/
        }
        printf("%d\n",sum);
        printf("%d %d %d %d %d %d\n",nums[1],nums[2],nums[3],nums[4],nums[5],nums[6]);
    }
    return 0;
}


