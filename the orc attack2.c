#include<stdio.h>

long A[1100][1100],B[1100][1100],check[1100];
long node;
void init(void)
{
    long i,j;

    for(i=0; i<node; i++)
    {
        check[i]=0;
        for(j=0; j<node; j++)
        {
            if(i==j)
            {
                A[i][j]=0;
                B[i][j]=0;
            }
            else
            {
                A[i][j]=10000000;
                B[i][j]=10000000;
            }
        }
    }
    return;
}

void warshall(void)
{
    long k,i,j;

    for (k=0 ; k<node ; k++)
    {
        for (i=0 ; i<node ; i++)
        {
            for (j=0 ; j<node ; j++)
            {
                if(A[i][j]<A[i][k]+A[k][j])
                    A[i][j]=A[i][j];
                else
                    A[i][j]=A[i][k]+A[k][j];
            }
        }
    }
    return;
}

int main()
{
    long cost,flag,Flag,N,u,v,x,i,j,k,min,s,ii,testCase,dataSet,p,max,start,minCost,count,hand;

    scanf("%ld",&testCase);
    for(dataSet=1; dataSet<=testCase; dataSet++)
    {
        scanf("%ld %ld",&node,&N);

        init();

        count=0;
        for(i=0; i<N; i++)
        {
            scanf("%ld %ld %ld",&u,&v,&cost);
            u--;
            v--;
            if(cost<A[u][v])
            {
                A[u][v]=cost;
                A[v][u]=cost;

                if(u<node)
                {
                    if(check[u]==0)
                    {
                        check[u]=1;
                        count++;
                    }
                }
                if(v<node)
                {
                    if(check[v]==0)
                    {
                        check[v]=1;
                        count++;
                    }
                }
            }
        }

        if(count==node)
        {
            warshall();

            Flag=start=0;
            for(i=0; i<node; i++)
            {
                count=0;
                p=-1000000;
                if(A[i][0]<10000000)
                {
                    p=A[i][0];
                    count++;
                }
                for(j=1; j<5; j++)
                {
                    if(A[i][j]==p)
                        count++;
                }
                if(count==5)
                {
                    if(start==0)
                    {
                        if(i!=node-1)
                        {
                            if(A[i][node-1]<10000000)
                                min=A[i][node-1];
                            else
                            {
                                if(A[i][0]<min)
                                    min=A[i][0];
                            }
                        }
                        else
                        {
                            if(A[i][0]<min)
                                min=A[i][0];
                        }

                        start=1;
                    }
                    else
                    {
                        if(i!=node-1)
                        {
                            if(A[i][node-1]<min)
                                min=A[i][node-1];
                            else
                            {
                                if(A[i][0]<min)
                                    min=A[i][0];
                            }
                        }
                        else
                        {
                            if(A[i][0]<min)
                                min=A[i][0];
                        }
                    }
                    Flag=1;
                }
            }
            if(Flag==1)
                printf("Map %ld: %ld\n",dataSet,min);
            else
                printf("Map %ld: -1\n",dataSet);
        }
        else
            printf("Map %ld: -1\n",dataSet);
    }
    return 0;
}
