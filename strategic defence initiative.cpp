#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<iostream>
using namespace std;
int arr[10010],path[10010],s[10010];

void printpath(int end)
{
    if(end> -1){
        printpath(path[end]);
        printf("%d\n",arr[end]);
    }
    return;
}
void longestincreasingsequence(int n)
{
    memset(path,0,sizeof(path));

    int global_max = 1,i,j;
    int end_pos = 0;

    s[0]= 1;
    path[0]= -1;
    for(i=1;i<n;i++){
        int local_prev = -1;
        s[i] = 1;
        for(j=0;j<i;j++){
            if(arr[j]<arr[i] && s[i]<(s[j]+1)){
                s[i]= s[j]+1;
                local_prev = j;
            }
        }

        path[i] = local_prev;
        if(global_max < s[i]){
            global_max = s[i];
            end_pos = i;
        }
    }

    printf("Max hits: %d\n",global_max);
    printpath(end_pos);
    return;
}

int main()
{
    char ch[10];
    char in[110];
    int i,t,j;
    scanf(" %d%c",&t,&ch[1]);
    gets(ch);
    for(i=0;i<t;i++)
    {
        j=0;
        while(gets(in))
        {
        if(in[0]=='\0')
        break;
        arr[j++]=atoi(in);
        }
        longestincreasingsequence(j);
       if(i!=t-1)
       printf("\n");
    }
    return 0;
}
