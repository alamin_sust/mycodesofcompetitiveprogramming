#include<stdio.h>
#include<string.h>
main()
{
    char str1[110],str2[110];
    int test_case=1,m,n,i,j,c[110][110];
    while(1)
    {
        gets(str1);
        if(str1[0]=='#')
            break;
        gets(str2);
        m = strlen(str1);
        n = strlen(str2);

        for(i=0; i<=m+1; i++)
        {
            for(j=0; j<=m+1; j++)
                c[i][j]=0;
        }


        for(i=1; i<=m; ++i)
        {
            for(j=1; j<=n; ++j)
            {
                if( str1[i-1] == str2[j-1] )
                {
                    c[i][j] = c[i-1][j-1] + 1;
                }
                else if( c[i-1][j] > c[i][j-1] )
                {
                    c[i][j] = c[i-1][j];
                }
                else
                {
                    c[i][j] = c[i][j-1];
                }
            }
        }
        printf("Case #%d: you can visit at most %d cities.\n", test_case, c[m][n]);
        test_case++;
    }
    return 0;
}
