#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

int dist[1010],m,n,cycle[1010];
vector<int>adj_list[1010];
vector<int>wt[1010];

void bfs(int u)
{
    if(cycle[u]==1)
        return;
    cycle[u]=1;
    for(int i=0;i<adj_list[u].size();i++)
        bfs(adj_list[u][i]);
}

void bellman_ford(void)
{
    for(int i=0;i<n;i++)
        dist[i]=0;
    for(int i=0;i<n-1;i++)
    {
        for(int j=0;j<n;j++)
        for(int k=0;k<adj_list[j].size();k++)
        {
            if(dist[j]+wt[j][k]<dist[adj_list[j][k]])
                dist[adj_list[j][k]]=dist[j]+wt[j][k];
        }
    }
    for(int i=0;i<n-1;i++)
    {
        for(int j=0;j<n;j++)
        for(int k=0;k<adj_list[j].size();k++)
        {
            if(dist[j]+wt[j][k]<dist[adj_list[j][k]])
                bfs(j);
        }
    }
    return;
}

main()
{
    int t,p,i,a,c,b;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n>>m;
        for(i=0;i<n;i++)
        {adj_list[i].clear();
            wt[i].clear();}
        for(i=0;i<m;i++)
        {
            scanf(" %d %d %d",&b,&a,&c);
            adj_list[a].push_back(b);
            wt[a].push_back(c);
        }
        int flag=0;
        memset(cycle,0,sizeof(cycle));
        printf("Case %d:",p);
        bellman_ford();
        for(i=0;i<n;i++)
            if(cycle[i]==1)
            {flag=1;
            printf(" %d",i);}
        if(flag==0)
            printf(" impossible");
            printf("\n");
    }
    return 0;
}

