#include<iostream>
#include<string.h>
using namespace std;

int a[125][125];
int n;
int pos[125],top[125];
int i,from,to,p1,p2,t1,t2;
char str1[15],str2[15];

void display()
{
    for(int i=0; i<n; i++)
    {
        cout<<i<<":";
        for(int j=0; j<top[i]; j++)
        {
            cout<<" "<<a[i][j];
        }
        cout<<"\n";
    }
}

int main()
{
    cin>>n;
    for(i=0; i<n; i++)
    {
        pos[i]=i;
        a[i][0]=i;
        top[i]=1;
    }



    while(1)
    {

        cin>>str1;
        if(strcmp(str1,"quit")==0) break;
        cin>>from;
        cin>>str2;
        cin>>to;
        if(from<0 || from>=n || to<0 || to>=n) continue;

        p1=pos[from];
        p2=pos[to];
        if(p1==p2) continue;

        for(i=0; i<top[p1]; i++)
        {
            if(a[p1][i]==from)
            {
                t1=i;
                break;
            }
        }

        for(i=0; i<top[p2]; i++)
        {
            if(a[p2][i]==to)
            {
                t2=i;
                break;
            }
        }


        if(strcmp(str1,"move")==0 &&strcmp(str2,"onto")==0)
        {
            int temp1,temp2;
            for(i=t1+1; i<top[p1]; i++)
            {
                temp1=a[p1][i];
                a[temp1][0]=temp1;
                top[temp1]=1;
                pos[temp1]=temp1;
            }
            for(i=t2+1; i<top[p2]; i++)
            {
                temp2=a[p2][i];
                a[temp2][0]=temp2;
                top[temp2]=1;
                pos[temp2]=temp2;
            }
            top[p1]=t1;
            top[p2]=t2+1;
            a[p2][top[p2]]=from;
            top[p2]++;
            pos[from]=p2;
        }
        else if(strcmp(str1,"move")==0 &&strcmp(str2,"over")==0)
        {
            int temp1,temp2;
            for(i=t1+1; i<top[p1]; i++)
            {
                temp1=a[p1][i];
                a[temp1][0]=temp1;
                top[temp1]=1;
                pos[temp1]=temp1;
            }
            top[p1]=t1;
            a[p2][top[p2]]=from;
            top[p2]++;
            pos[from]=p2;
        }


        else if(strcmp(str1,"pile")==0 &&strcmp(str2,"onto")==0)
        {
            int temp1,temp2;
            for(i=t2+1; i<top[p2]; i++)
            {
                temp2=a[p2][i];
                a[temp2][0]=temp2;
                top[temp2]=1;
                pos[temp2]=temp2;
            }
            top[p2]=t2+1;

            for(i=t1; i<top[p1]; i++)
            {
                a[p2][top[p2]]=a[p1][i];
                top[p2]++;
                pos[a[p1][i]]=p2;
            }
            top[p1]=t1;

        }
        else if(strcmp(str1,"pile")==0 &&strcmp(str2,"over")==0)
        {
            int temp1,temp2;

            for(i=t1; i<top[p1]; i++)
            {
                a[p2][top[p2]]=a[p1][i];
                top[p2]++;
                pos[a[p1][i]]=p2;
            }
            top[p1]=t1;

        }


    }
    display();
    return 0;
}
