#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
ll arr[100010],cum[100010],cum2[100010],i,n,k,q,m,a,b;
main()
{
    cin>>n;
    for(i=1;i<=n;i++)
        scanf(" %I64d",&arr[i]),cum[i]=cum[i-1]+arr[i];
    sort(arr+1,arr+n+1);
    for(i=1;i<=n;i++)
        cum2[i]=cum2[i-1]+arr[i];
    cin>>q;
    for(i=1;i<=q;i++)
    {
        scanf(" %I64d %I64d %I64d",&m,&a,&b);
        if(m==1)
            printf("%I64d\n",cum[b]-cum[a-1]);
        else
            printf("%I64d\n",cum2[b]-cum2[a-1]);
    }
    return 0;
}

