#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int index[100010],ind,lowlink[100010];
stack<int>stk;
map<int,int>mpp;
vector<int>adj[100010];

void tarjan(int v)
{
    index[v]=ind;
    lowlink[v]=ind;
    ind++;
    mpp[v]=1;
    stk.push(v);
    for(int i=0;i<adj[v].size();i++)
    {
        int w=adj[v][i];
        if(index[w]==0)
        {
            tarjan(w);
            //printf("%d..%d--%d**%d\n",lowlink[v],lowlink[w],v,w);
            lowlink[v]=min(lowlink[v],lowlink[w]);

        }
        else if(mpp[w]==1)
        {
            //printf("%d.%d-%d*%d\n",lowlink[v],index[w],v,w);
            lowlink[v]=min(lowlink[v],index[w]);
        }
    }
    if(index[v]==lowlink[v])
    {
        while(!stk.empty())
        {
            cout<<stk.top()<<" ";
            if(stk.top()==v)
                {stk.pop();
                 break;}
            stk.pop();
        }
        cout<<endl;
    }
    return;
}

main()
{
    int n,e,i,v,w;
    cin>>n>>e;
    for(i=1;i<=n;i++)
    {
        lowlink[i]=inf;
        index[i]=0;
    }
    for(i=1;i<=e;i++)
        cin>>v>>w,adj[v].push_back(w);
    for(i=1;i<=n;i++)
    {
        if(index[i]==0)
            ind=i,mpp.clear(),tarjan(i);
    }
    return 0;
}
/*
8 14
1 2
2 3
3 1
4 2
4 3
4 5
5 4
5 6
6 7
6 3
7 6
8 7
8 8
8 5
*/

