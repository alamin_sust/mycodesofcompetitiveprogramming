/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define dist2(x1,y1,z1,x2,y2,z2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) + ((z2-z1)*(z2-z1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node
{
    int x;
    double distance;
};

struct node2
{
    double x,y,floor;
};

node2 point[210];


bool operator<(node a,node b)
{
    return a.distance>b.distance;
}

node det;

priority_queue<node>pq;
int m,q,p=0,n,src,dst,par[205];
double adj[205][205],d[210];

void print_path(int x)
{
    if(par[x]==-1)
            {printf("%d",x);
            return;}
    print_path(par[x]);
    printf(" %d",x);
    return;
}

void dijkstra(void)
{
    for(int i=0;i<n;i++)
       d[i]=999999999999.0;
    par[src]=-1;
    d[src]=0;
    det.x=src;
    det.distance=0.0;
    pq.push(det);
    while(!pq.empty())
    {
        int u=pq.top().x;
        pq.pop();
        for(int i=0;i<n;i++)
        {
            if(adj[u][i]>999999999998.0)
            continue;
            if(d[i]>=(adj[u][i]+d[u]))
            {
                d[i]=(adj[u][i]+d[u]);
                par[i]=u;
                det.x=i;
                det.distance=d[i];
                pq.push(det);
            }
        }
    }
    print_path(dst);
    printf("\n");
    return;
}


int main()
{
    int x,y;
    char arr[110];
    while(cin>>n>>m)
    {
        if(p)
            printf("\n");
        for(int i=0;i<n;i++)
            for(int j=0;j<n;j++)
            adj[i][j]=999999999999.0;
        for(int i=0;i<n;i++)
        {
            scanf(" %lf %lf %lf",&point[i].floor,&point[i].x,&point[i].y);
            point[i].floor*=5.0;
        }
        for(int i=0;i<m;i++)
        {
            scanf(" %d %d %s",&x,&y,&arr);
            if(arr[0]=='w')
            {
                adj[x][y]=adj[y][x]=dist2(point[x].floor,point[x].x,point[x].y,point[y].floor,point[y].x,point[y].y);
            }
            else if(arr[0]=='l')
            {
                adj[x][y]=adj[y][x]=1.0;
            }
            else if(arr[0]=='s')
            {
                adj[x][y]=adj[y][x]=dist2(point[x].floor,point[x].x,point[x].y,point[y].floor,point[y].x,point[y].y);
            }
            else if(arr[0]=='e')
            {
                adj[x][y]=1.0;
                adj[y][x]=3.0*dist2(point[x].floor,point[x].x,point[x].y,point[y].floor,point[y].x,point[y].y);
            }
        }
        //printf("%lf %lf %lf===\n",adj[1][2],adj[1][0],adj[0][2]);
        scanf(" %d",&q);
        for(int i=0;i<q;i++)
        {

            scanf(" %d %d",&src,&dst);
            dijkstra();
        }
        p++;
    }
    return 0;
}

