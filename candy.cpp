/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

vector<int>arr[100010];
int dp1[100010],n,m,dp[100010],opt[100010],i,j;

int rec(int pos)
{
    if(pos>m)
        return 0;
    int &ret=dp[pos];
    if(ret!=-1)
    {
        return ret;
    }
    ret=0;
    ret=max(rec(pos+2)+arr[i][pos],rec(pos+1));
    return ret;
}

int rec1(int pos)
{
    if(pos>n)
        return 0;
    int &ret=dp1[pos];
    if(ret!=-1)
    {
        return ret;
    }
    ret=0;
    ret=max(rec1(pos+2)+opt[pos],rec1(pos+1));
    return ret;
}

main()
{
    int in;
    while(1)
   {
       cin>>n>>m;
       if(n==0&&m==0)
        break;
       for(i=1;i<=n;i++)
       {
           arr[i].clear();
           arr[i].push_back(0);
           for(j=1;j<=m;j++)
           {
               scanf(" %d",&in);
               arr[i].push_back(in);
           }
           for(j=0;j<=m;j++)
            dp[j]=-1;
           opt[i]=rec(0);
       }
       for(i=0;i<=n;i++)
        dp1[i]=-1;
       printf("%d\n",rec1(0));
   }
    return 0;
}
















