#include<stdio.h>
#define infinity 99999999

long int adj[20010][20010],n;

void warshall()
{
    long int k,i,j;
    for(k=0;k<n;k++)
    {
        for(i=0;i<n;i++)
        {
            for(j=0;j<n;j++)
            {
                if(adj[i][j]>(adj[i][k]+adj[k][j]))
                adj[i][j]=adj[i][k]+adj[k][j];
            }
        }
    }
    return;
}

main()
{
    long int i,j,t,p,m,S,T,from,to,dist;
    scanf(" %ld",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %ld %ld %ld %ld",&n,&m,&S,&T);
        for(i=0;i<n;i++)
        {
            for(j=0;j<n;j++)
            adj[i][j]=infinity;
        }
        for(i=0;i<m;i++)
        {
            scanf(" %ld %ld %ld",&from,&to,&dist);
            adj[from][to]=dist;
            adj[to][from]=dist;
        }
        warshall();
        if(S==T)
        printf("Case #%ld: 0\n",p);
        else if(adj[S][T]==infinity)
        printf("Case #%ld: unreachable\n",p);
        else
        printf("Case #%ld: %ld\n",p,adj[S][T]);
    }
    return 0;
}
