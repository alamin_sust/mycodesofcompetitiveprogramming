#include<stdio.h>

char arr[20][20];

void black(int x,int y)
{
    if(arr[x][y]=='.')
        arr[x][y]='x';
    if(arr[x][y-1]=='.')
        black(x,y-1);
    if(arr[x-1][y]=='.')
        black(x-1,y);
    if(arr[x][y+1]=='.')
        black(x,y+1);
    if(arr[x+1][y]=='.')
        black(x+1,y);
    return;
}

void white(int x,int y)
{
    if(arr[x][y]=='.')
        arr[x][y]='o';
    if(arr[x][y]=='x')
        arr[x][y]='#';
    if(arr[x][y-1]=='.'||arr[x][y-1]=='x')
        white(x,y-1);
    if(arr[x-1][y]=='.'||arr[x-1][y]=='x')
        white(x-1,y);
    if(arr[x][y+1]=='.'||arr[x][y+1]=='x')
        white(x,y+1);
    if(arr[x+1][y]=='.'||arr[x+1][y]=='x')
        white(x+1,y);
    return;
}


int main()
{
    int i,j,b,w,n,p;
    for(i=0;i<11;i++)
        {
            for(j=0;j<11;j++)
                arr[i][j]='#';
        }
    scanf(" %d",&n);
    getchar();
    for(p=1;p<=n;p++)
    {
        for(i=1;i<=9;i++)
        {
            for(j=1;j<=9;j++)
                scanf("%c",&arr[i][j]);
            getchar();
        }
        for(i=1;i<=9;i++)
        {
            for(j=1;j<=9;j++)
            {
                if(arr[i][j]=='X')
                    black(i,j);
            }
        }
        for(i=1;i<=9;i++)
        {
            for(j=1;j<=9;j++)
            {
                if(arr[i][j]=='O')
                    white(i,j);
            }
        }
        for(b=0,w=0,i=1;i<=9;i++)
        {
            for(j=1;j<=9;j++)
            {
                if(arr[i][j]=='X'||arr[i][j]=='x')
                    b++;
                if(arr[i][j]=='O'||arr[i][j]=='o')
                    w++;
            }
        }
        printf("Black %d White %d\n",b,w);
    }
    return 0;
}


