/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node{
int shift,amnt,threat;
};

int cnt,l;
char word[10010][1010],arr[100010];
node cipher[110];
map<string,int>mpp1,mpp2;


int comp(node a,node b)
{
    return a.amnt>b.amnt;
}

void func(int shift)
{
    for(int i=0;i<cnt;i++)
    {
        int len=strlen(word[i]);
        for(int j=0;j<len;j++)
        {
            word[i][j]--;
            if(word[i][j]<'a')
                word[i][j]='z';
        }
    }
    return;
}

void calculate(int x)
{
    cipher[x].amnt=cipher[x].threat=0;
    cipher[x].shift=x;
    for(int i=0;i<cnt;i++)
    {
        if(mpp2[word[i]])
            cipher[x].amnt++,cipher[x].threat++;
        else if(mpp1[word[i]])
            cipher[x].amnt++;
    }
    return;
}

int main()
{
    int i,j,p,t,n,k;
    char in[100010];
    cin>>t;
    for(p=1;p<=t;p++)
    {
        mpp1.clear();
        mpp2.clear();
        scanf(" %d",&n);
        for(i=1;i<=n;i++)
        {
            scanf(" %s",&in);
            l=strlen(in);
            for(j=0;j<l;j++)
                in[j]=tolower(in[j]);
            mpp1[in]=1;
        }
        scanf(" %d",&n);
        for(i=1;i<=n;i++)
        {
            scanf(" %s",&in);
            l=strlen(in);
            for(j=0;j<l;j++)
                in[j]=tolower(in[j]);
            mpp2[in]=1;
        }
        getchar();
        gets(arr);
        l=strlen(arr);
        cnt=0;
        for(i=0;i<l;)
        {
            for(j=0;;j++)
            {
                if(word[cnt][j]=='\0')
                break;
                word[cnt][j]='\0';
            }
            k=0;
            while(i<l&&(!((arr[i]>='A'&&arr[i]<='Z')||(arr[i]>='a'&&arr[i]<='z'))))
                i++;
            while(i<l&&((arr[i]>='A'&&arr[i]<='Z')||(arr[i]>='a'&&arr[i]<='z')))
            {
                word[cnt][k++]=tolower(arr[i]);
                i++;
            }
            if(k)
            {
              cnt++;
            }
        }
        for(i=0;i<26;i++)
        {
            calculate(i);
            func(i);
        }
        sort(cipher,cipher+26,comp);
        if(cipher[0].amnt==cipher[1].amnt)
            printf("Unable to decipher\n");
        else
        {
            for(i=0;i<l;i++)
            {
                if(arr[i]>='A'&&arr[i]<='Z')
                {
                arr[i]-=cipher[0].shift;
                if(arr[i]<'A')
                arr[i]+=26;
                }
                if(arr[i]>='a'&&arr[i]<='z')
                {
                arr[i]-=cipher[0].shift;
                if(arr[i]<'a')
                arr[i]+=26;
                }
                printf("%c",arr[i]);
            }
            printf("\nShift: %d, Match: %.0lf%%, Threat: %.0lf%%\n",cipher[0].shift,(double)cipher[0].amnt*100.0/(double)cnt,(double)cipher[0].threat*100.0/(double)cnt);
        }
    }
    return 0;
}
/*
2
11
a
also
from
if
it
keep
must
to
want
you
yourself
2
hide
secret
Vs lbh jnag gb xrrc n frperg, lbh zhfg nyfb uvqr vg sebz lbhefrys.
12
a
brother
darkness
destruction
is
it
language
mind
of
the
thing
words
3
beautiful
freedom
truth
Hs'r z adztshetk sghmf, sgd cdrsqtbshnm ne vnqcr.
*/
