#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class BearPair
{
public:
	int bigDistance(string s)
	{
	    int ret=-1;
	    for(int i=0;i<s.length();i++)
        {
            for(int j=i+1;j<s.length();j++)
            {
                if(s[i]!=s[j])
                {
                    ret=max(ret,j-i);
                }
            }
        }
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	BearPair objectBearPair;

	//test case0
	string param00 = "bear";
	int ret0 = objectBearPair.bigDistance(param00);
	int need0 = 3;
	assert_eq(0,ret0,need0);

	//test case1
	string param10 = "abcba";
	int ret1 = objectBearPair.bigDistance(param10);
	int need1 = 3;
	assert_eq(1,ret1,need1);

	//test case2
	string param20 = "oooohyeahpotato";
	int ret2 = objectBearPair.bigDistance(param20);
	int need2 = 13;
	assert_eq(2,ret2,need2);

	//test case3
	string param30 = "zzzzzzzzzzzzzzzzzzzzz";
	int ret3 = objectBearPair.bigDistance(param30);
	int need3 = -1;
	assert_eq(3,ret3,need3);

	//test case4
	string param40 = "qw";
	int ret4 = objectBearPair.bigDistance(param40);
	int need4 = 1;
	assert_eq(4,ret4,need4);

	//test case5
	string param50 = "xxyyxyxyyyyyyxxxyxyxxxyxyxyyyyyxxxxxxyyyyyyyyxxxxx";
	int ret5 = objectBearPair.bigDistance(param50);
	int need5 = 47;
	assert_eq(5,ret5,need5);

}
