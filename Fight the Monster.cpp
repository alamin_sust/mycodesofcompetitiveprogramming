#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll Hy,Hm,Ay,Am,Dy,Dm,H,D,A;

ll func(ll ay,ll dy)
{
    ll ret=ay*A+dy*D;
    ll m=max(0LL,(Ay+ay)-Dm);
    ll y=max(0LL,Am-(Dy+dy));
    if(m>0LL&&y==0LL)
        return ret;
    if(m==0LL&&y>0LL)
        return 999999999999999LL;
    if(m==0LL&&y==0LL)
        return 999999999999999LL;
    ll S1=Hm/m;
    ll M1=Hm%m;
    if(M1)
        S1++;
    ll low=0LL;
    ll high=999999999999999LL;
    ll mid,k=70LL,rr=high;
    while(k--)
    {
        mid=(low+high)/2LL;
        if((y*S1)<(mid+Hy))
        {
            rr=mid;
            high=mid-1LL;
        }
        else
            low=mid+1LL;
    }
    return ret+rr*H;
}

int main()
{
    ll res=0LL,i,j;
    cin>>Hy>>Ay>>Dy;
    cin>>Hm>>Am>>Dm;
    cin>>H>>A>>D;
    res=999999999999999LL;
    for(i=0LL; i<=202LL; i++)
    {
        for(j=0LL; j<=202LL; j++)
        {
            res=min(res,func(i,j));
        }
    }
    printf("%I64d\n",res);

    return 0;
}
