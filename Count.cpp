/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int row,col,a,b;
vector<int>arr[10010];

int bsrch(int r,int val)
{
    int ret=col;
    int low,high,mid;
    low=0;
    high=col-1;
    while(low<=high)
    {
        mid=(low+high)/2;
        if(arr[r][mid]>=a)
        {
            ret=mid;
            high=mid-1;
        }
        else
        {
            low=mid+1;
        }
    }
    return ret;
}

int bsrch2(int r,int val)
{
    int ret=-1;
    int low,high,mid;
    low=0;
    high=col-1;
    while(low<=high)
    {
        mid=(low+high)/2;
        if(arr[r][mid]<=b)
        {
            ret=mid;
            low=mid+1;
        }
        else
        {
            high=mid-1;
        }
    }
    return ret;
}


bool get(int *a) {
    char *p = (char*)a;
    int q;
    q = getchar(); if(q<0) return false; *(p++)=q;
    q = getchar(); if(q<0) return false; *(p++)=q;
    q = getchar(); if(q<0) return false; *(p++)=q;
    q = getchar(); if(q<0) return false; *(p++)=q;
    return true;
    //return scanf("%c%c%c%c",p,p+1,p+2,p+3)!=EOF;
}

int main()
{
    int i,j,in,res=0,tp,tp2;
    get(&row);
    get(&col);
    for(i=0;i<row;i++)
    {
        for(j=0;j<col;j++)
        {
            get(&in);
            arr[i].push_back(in);
        }
    }
    while(get(&a))
    {
        get(&b);
        if(a>b)
            {printf("0\n");
            continue;}
        res=0;
        for(i=0;i<row;i++)
        {
            tp=col-bsrch(i,a);
            tp2=bsrch2(i,b)+1;
            res+=tp+tp2-col;
        }
        printf("%d\n",res);
    }
    return 0;
}

