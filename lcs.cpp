#include<stdio.h>

int dp[40], arr[40];

int main()
{
    int a, b, c,top, i, j, k;
    dp[0]=-12;
    while(scanf("%d", &a)!=EOF)
    {
        for(i=0;i<a;i++)
            scanf("%d", &arr[i]);
        top=0;
        for(i=0;i<a;i++)
        {
            if(arr[i]>dp[top])
                dp[++top]=arr[i];
            else
            {
                j=top;
                while(dp[j]>=arr[i])j--;
                dp[j+1]=arr[i];
            }
        }
        printf("%d\n", top);
    }
    return 0;
}
