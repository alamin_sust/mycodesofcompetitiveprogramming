#include<cstdio>
#include<string.h>
#include<algorithm>
#include<iostream>
using namespace std;

#define MAX 10000

long c[MAX][MAX], N;
long sorted[MAX], unsort[MAX];

long LCS_LENGTH()
{
   long i, j;

   for(i=0; i<=N; i++)
      c[i][0]=0;
   for(j=0; j<=N; j++)
      c[0][j]=0;

   for(i=1; i<=N; i++)
      for(j=1; j<=N; j++)
         if(sorted[i-1]==unsort[j-1])
         {
            c[i][j]=c[i-1][j-1]+1;
         }
         else if(c[i-1][j]>=c[i][j-1])
         {
            c[i][j]=c[i-1][j];
         }
         else
         {
            c[i][j]=c[i][j-1];
         }

   return c[N][N];
}

int fun_name(  const void *a, const void *b )
{
   long *p = (long *)a;
   long *q = (long *)b;

   return *q - *p ;
}

int main()
{
   long i,n, ii, height[100010], testcase=1;

   while(1)
   {
      scanf("%ld", &n);
      if(n<0)
         break;
      else
      {
         unsort[0]=n;
         sorted[0]=n;

         ii=1;
         while(1)
         {
            scanf("%ld", &n);
            if(n<0)
               break;
            else
            {
               sorted[ii]=n;
               unsort[ii]=n;
               ii++;
            }
         }
      }

      N=ii;

      qsort( sorted, N, sizeof(long), fun_name );
      height[testcase]=LCS_LENGTH();
      testcase++;
   }
   for(i=1;i<testcase;i++)
   {
    printf("Test #%ld:\n",i);
    printf("  maximum possible interceptions: %ld\n",height[i]);
   if(i!=testcase-1)
   printf("\n");}
   return 0;
}
