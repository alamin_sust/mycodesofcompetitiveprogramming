/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};
int col[10010],par[10010],u,v,i,n,gcol[10010],flag[10010];
vector<int>adj[10010];
queue<int>q;

int bfs() {
    int ret=0;

    u=1;
    q.push(u);
    par[u]=0;
    col[0]=0;

    while(!q.empty()) {

        u=q.front();
        q.pop();
        if(col[par[u]]!=gcol[u]) {
            ret++;
        }
        col[u]=gcol[u];
        flag[u]=1;

        for(i=0;i<adj[u].size();i++) {
            v=adj[u][i];
            if(flag[v]==0)
            q.push(v),par[v]=u;
        }
    }


    return ret;

}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);


    scanf(" %d",&n);

    for(i=2;i<=n;i++) {
        scanf(" %d",&v);
        adj[v].push_back(i);
        adj[i].push_back(v);
    }
    for(i=1;i<=n;i++) {
        scanf(" %d",&gcol[i]);
    }

    printf("%d\n",bfs());

    return 0;
}






