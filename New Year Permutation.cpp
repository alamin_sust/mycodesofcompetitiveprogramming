/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int n,i,j,k,arr[310],pos[310],adj[310][310],col[310];
char ch;

int main()
{
    cin>>n;
    for(i=1; i<=n; i++)
    {
        scanf(" %d",&arr[i]);
        pos[arr[i]]=i;
    }
    getchar();
    for(i=1; i<=n; i++)
    {
        for(j=1; j<=n; j++)
        {
            scanf("%c",&ch);
            if(ch=='1')
            {
                adj[i][j]=adj[j][i]=1;
            }
        }
        getchar();
    }

    for(k=1; k<=n; k++)
    {
        for(i=1; i<=n; i++)
        {
            for(j=1; j<=n; j++)
            {
                adj[i][j]|=(adj[i][k]&adj[k][j]);
            }
        }
    }

    for(i=1; i<=n; i++)
    {
        for(j=1; j<=n; j++)
        {
            if(col[j]==0)
            {
                if(pos[i]==j)
                {
                    col[j]=i;
                    break;
                }
                else if(adj[pos[i]][j]==1)
                {
                    pos[arr[j]]=pos[i];
                    arr[pos[i]]=arr[j];
                    col[j]=i;
                    pos[i]=j;
                    arr[j]=i;
                    break;
                }
            }
        }
    }
    for(i=1; i<=n; i++)
    {
        printf("%d",col[i]);
        if(i==n)
            printf("\n");
        else
            printf(" ");
    }
    return 0;
}
