#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.0000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll res,iter,xx[2600][2600],yy[2600][2600];

void path(ll x,ll y)
{
    iter++;
    ll i,k=0;
    if(x>y)
    {
        for(i=1;i<=y;i++)
        {
            res++;
            xx[iter][++k]=1;
            yy[iter][k]=i;
            //printf("(1,%I64d) ",i);
        }
        for(i=2;i<=x;i++)
        {
            res++;
            xx[iter][++k]=i;
            yy[iter][k]=y;
           // printf("(%I64d,%I64d) ",i,y);
        }
    }
    else
    {
        for(i=1;i<=x;i++)
        {
            res++;
            xx[iter][++k]=i;
            yy[iter][k]=1;
            //printf("(%I64d,1) ",i);
        }
        for(i=2;i<=y;i++)
        {
            res++;
            xx[iter][++k]=x;
            yy[iter][k]=i;
            //printf("(%I64d,%I64d) ",x,i);
        }
    }
    return;
}

main()
{
    ll r,c,k,g,mn,g1,g2,extra,a,b,i,j;
    double f;
    cin>>r>>c>>k;
    f=sqrt((double)k);
    g=(ll)(f+eps);
    mn=min(r,c);
        extra=k-(mn*mn);
        g1=g2=mn;
        a=b=0;
        for(j=1,i=1;j<=extra;j++,i++)
        {
            if(i%2==1)
            {
                a++;
                if((g1+1)<=r&&a<=c)
                path(g1+1,a);
                else
                {
                    j--;
                    a=0;
                    g1++;
                }
            }
            else
            {
                b++;
                if(b<=r&&(g2+1)<=c)
                path(b,g2+1);
                else
                {
                    j--;
                    b=0;
                    g2++;
                }
            }
        }
        i=mn;
        if(mn*mn>k)
            i=g;
  for(;i>=1;i--)
    {
        path(i,i);
        for(j=1;j<i;j++)
        {
            path(i,j);
            path(j,i);
        }
    }
    cout<<res<<endl;
    for(i=extra;i>=1;i--)
    {
        j=1;
        while(xx[i][j]>0)
        {
            printf("(%I64d,%I64d) ",xx[i][j],yy[i][j]);
            j++;
        }
        printf("\n");
    }
    i=max(1LL,extra+1);
    for(;i<=iter;i++)
    {
        j=1;
        while(xx[i][j]>0)
        {
            printf("(%I64d,%I64d) ",xx[i][j],yy[i][j]);
            j++;
        }
        printf("\n");
    }
    return 0;
}

