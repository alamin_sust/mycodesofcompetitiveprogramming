/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int dp[1<<16][10][6],k,x,y,p,t,flag[20][20],m,n;
char arr[12][12];

bool rec(int mask,int len,int food)
{
    //printf("%d %d %d\n",mask,len,food);
    if(len==(k+food-1))
        return 1;
    int &ret=dp[mask][len][food];
    if(ret!=-1)
        return ret;
    ret=0;
    int call[4]={0},tpmask=mask,shft=len,nowx=0,nowy=0;
    flag[x+nowx][y+nowy]=1;
    while(shft)
    {
        shft--;
        int direction=tpmask%4;
        tpmask/=4;
        if(direction==0)
            nowx++;
        else if(direction==1)
            nowx--;
        else if(direction==2)
            nowy++;
        else
            nowy--;
        if((x+nowx)>=n)
            nowx-=n;
        if((x+nowx)<0)
            nowx+=n;
        if((y+nowy)>=m)
            nowy-=m;
        if((y+nowy)<0)
            nowy+=m;
        flag[x+nowx][y+nowy]=1;
    }
    while((x+nowx+1)>=n)
        nowx-=n;
    if(flag[x+nowx+1][y+nowy]==0&&arr[x+nowx+1][y+nowy]!='#')
        {
            if(arr[x+nowx+1][y+nowy]!='o')
            call[0]=1;
            else
            call[0]=2;
        }
    while((x+nowx-1)<0)
        nowx+=n;
    if(flag[x+nowx-1][y+nowy]==0&&arr[x+nowx-1][y+nowy]!='#')
        {
            if(arr[x+nowx-1][y+nowy]!='o')
            call[1]=1;
            else
            call[1]=2;
        }
    while((y+nowy+1)>=m)
        nowy-=m;
    while((x+nowx)<0)
        nowx+=n;
    while((x+nowx)>=n)
        nowx-=n;
    if(flag[x+nowx][y+nowy+1]==0&&arr[x+nowx][y+nowy+1]!='#')
        {
            if(arr[x+nowx][y+nowy+1]!='o')
            call[2]=1;
            else
            call[2]=2;
        }
    while((y+nowy-1)<0)
        nowy+=m;
    if(flag[x+nowx][y+nowy-1]==0&&arr[x+nowx][y+nowy-1]!='#')
        {
            if(arr[x+nowx][y+nowy-1]!='o')
            call[3]=1;
            else
            call[3]=2;
        }
    tpmask=mask;
    int nox=0;
    int noy=0;
    flag[x+nox][y+noy]=0;
    shft=len;
    while(shft)
    {
        shft--;
        int direction=tpmask%4;
        tpmask/=4;
        if(direction==0)
            nox++;
        else if(direction==1)
            nox--;
        else if(direction==2)
            noy++;
        else
            noy--;
        if((x+nox)>=n)
            nox-=n;
        if((x+nox)<0)
            nox+=n;
        if((y+noy)>=m)
            noy-=m;
        if((y+noy)<0)
            noy+=m;
        flag[x+nox][y+noy]=0;
    }
    shft=len*2;
    if(call[0])
        ret|=rec(mask,len+1,food+call[0]-1);
    if(call[1])
        ret|=rec(mask+(1<<shft),len+1,food+call[1]-1);
    if(call[2])
        ret|=rec(mask+(2<<shft),len+1,food+call[2]-1);
    if(call[3])
        ret|=rec(mask+(3<<shft),len+1,food+call[3]-1);
    return ret;
}

main()
{
    int i,j;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %d %d %d",&n,&m,&k);
        getchar();
        for(i=0;i<n;i++)
        {
            for(j=0;j<m;j++)
            {
                scanf("%c",&arr[i][j]);
                if(arr[i][j]=='x')
                    x=i,y=j;
            }
            getchar();
        }
        memset(dp,-1,sizeof(dp));
        /*for(i=0;i<20;i++)
        {
            for(j=0;j<20;j++)
                printf("%d ",flag[i][j]);
            printf("\n");
        }*/
        if(rec(0,0,0))
            printf("Case #%d: Fits perfectly!\n",p);
        else
            printf("Case #%d: Oh no, snake's too fat!\n",p);
    }
    return 0;
}

