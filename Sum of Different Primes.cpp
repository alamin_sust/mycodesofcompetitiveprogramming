#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define MAX 1200
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll sieve[2010], primecount=0, prime[2010],dp[1125][16][200];
void sieve_(void)
{
    for(ll i=0; i<MAX; i++)
    {
        sieve[i]=1;
    }
    sieve[0]=sieve[1]=0;
    for(ll i=2; i<MAX; i++)
    {
        while(sieve[i]==0 && i<MAX)
        {
            i++;
        }
        prime[++primecount]=i;
        for(ll j=i*i; j<MAX; j+=i)
        {
            sieve[j]=0;
        }
    }
    return;
}

ll get(ll res,ll k,ll ind)
{
    if(k==0&&res==0)
        return 1;
    ll &ret=dp[res][k][ind];
    if(ret!=-1)
        return ret;
    ret=0;
    for(ll i=ind;res>=prime[i]&&k>0;i++)
    {
        ret+=get(res-prime[i],k-1,i+1);
    }
    return ret;
}

main()
{
    ll res,k;
    sieve_();
    memset(dp,-1,sizeof(dp));
    //printf("%lld\n",primecount);
    while(scanf("%lld%lld",&res,&k)!=EOF)
    {
        if(res==0&&k==0)
            break;

        printf("%lld\n",get(res,k,1));
    }
    return 0;
}

