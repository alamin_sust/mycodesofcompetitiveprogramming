#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int t,p,c,arr[110],la[110],ra[110],cum[110],dd[110],i,j,mx;
char res[110][110];

main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++) {
        scanf(" %d",&c);

        for(i=1;i<=c;i++) {
            scanf(" %d",&arr[i]);
        }
        printf("Case #%d:",p);
        if(arr[1]==0 || arr[c]==0) {
            printf(" IMPOSSIBLE\n");
            continue;
        }

        memset(cum,0,sizeof(cum));
        memset(la,0,sizeof(la));
        memset(ra,0,sizeof(ra));
        memset(dd,0,sizeof(dd));

        for(i=0;i<106;i++) {
            for(j=0;j<106;j++) {
                res[i][j]='.';
            }
        }

        mx=0;

        int curr=1;

        for(i=1;i<=c;i++) {
            if(arr[i]) {
               la[i]=curr;
               curr+=arr[i];
               ra[i]=la[i]+arr[i]-1;

               if(la[i]<i) {
                  int lt = i-la[i];
                  int ii=1;
                  int jj=la[i];

                  while(ii<=lt) {
                    res[ii++][jj++]='\\';
                  }
               }
               if(ra[i]>i) {
                    int rt = ra[i]-i;
                  int ii=1;
                  int jj=ra[i];

                  while(ii<=rt) {
                    res[ii++][jj--]='/';
                  }
               }

               mx=max(mx,abs(i-ra[i]));
               mx=max(mx,abs(i-la[i]));
            }
        }





        printf(" %d\n",mx+1);
        for(i=1;i<=mx+1;i++) {
            for(j=1;j<=c;j++) {
                printf("%c",res[i][j]);
            }
            printf("\n");
        }
    }

    return 0;
}

