/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
int rr[]= {1,-1,0,0};
int cc[]= {0,0,1,-1};

struct node
{
    int x,y;
};

queue<node>q;

node from,to;

int dx,dy,sx,sy,found,val[1010][1010],fg[1010][1010];
char arr[1010][1010];

int bfs(void)
{
    found=0;
    val[dx][dy]=0;
    from.x=dx;
    from.y=dy;
    if(sx==from.x&&sy==from.y)
        found=1;
    fg[dx][dy]=1;
    q.push(from);
    while(!q.empty())
    {
        from=q.front();
        q.pop();
        for(int i=0; i<4; i++)
        {
            to.x=from.x+rr[i];
            to.y=from.y+cc[i];
            if(fg[to.x][to.y]==0&&arr[to.x][to.y]=='*')
            {
                fg[to.x][to.y]=1;
                val[to.x][to.y]=val[from.x][from.y]+1;
                q.push(to);
                if(to.x==sx&&to.y==sy)
                    found=1;
            }
        }
    }
    return found;
}

void getpath(void)
{
    from.x=sx;
    from.y=sy;
    fg[from.x][from.y]=0;
    while(1)
    {
        //printf("..");
        arr[from.x][from.y]='.';
        if(from.x==dx&&from.y==dy)
            break;
        if(fg[from.x][from.y+1]==1&&val[from.x][from.y+1]==val[from.x][from.y]-1)
            from.y++,fg[from.x][from.y]=0;
        else if(fg[from.x+1][from.y]==1&&val[from.x+1][from.y]==val[from.x][from.y]-1)
            from.x++,fg[from.x][from.y]=0;
        else if(fg[from.x][from.y-1]==1&&val[from.x][from.y-1]==val[from.x][from.y]-1)
            from.y--,fg[from.x][from.y]=0;
        else if(fg[from.x-1][from.y]==1&&val[from.x-1][from.y]==val[from.x][from.y]-1)
            from.x--,fg[from.x][from.y]=0;
    }
    return;
}

int dir(int x,int y)
{
    int ret=0;
    arr[from.x][from.y]='#';
    while(1)
    {
        if(arr[from.x+x][from.y+y]=='.')
            from.x+=x,from.y+=y,ret++,arr[from.x][from.y]='#';
        else break;
    }
    return ret;
}

main()
{
    int p,t,i,j,row,col,D,befD,k;
    scanf(" %d",&t);
    for(p=1; p<=t; p++)
    {
        scanf(" %d %d",&col,&row);
        getchar();
        for(i=0; i<=row+1; i++)
        {
            gets(arr[i]);
            for(j=0; j<=col+1; j++)
            {
                val[i][j]=99999999;
                fg[i][j]=0;
            }
            //printf("+++++\n");
        }
        scanf(" %d %d %d %d",&sy,&sx,&dy,&dx);
        sy++;
        sx++;
        dy++;
        dx++;
        /*for(i=0;i<=row+1;i++)
        {
            printf("%s",arr[i]);
            printf("\n");
        }*/
        if(arr[sx][sy]!='*'||arr[dx][dy]!='*')
        {
            printf("No route found.\n\n");
            continue;
        }
        if(bfs()==0)
        {
            printf("No route found.\n\n");
            continue;
        }

        getpath();

       /* for(i=0;i<=row+1;i++)
        {
            printf("%s",arr[i]);
            printf("\n");
        }*/

        if(arr[sx][sy+1]=='.')
            printf("Turn to the east.\n");
        else if(arr[sx+1][sy]=='.')
            printf("Turn to the south.\n");
        else if(arr[sx][sy-1]=='.')
            printf("Turn to the west.\n");
        else if(arr[sx-1][sy]=='.')
            printf("Turn to the north.\n");
        from.x=sx;
        from.y=sy;
        D=befD=0;
        while(1)
        {
           // printf("..");
           //getchar();
            if(from.x==dx&&from.y==dy)
                break;
            if(arr[from.x][from.y+1]=='.')
            {
                D=1;
                k=dir(0,1);
            }
            else if(arr[from.x+1][from.y]=='.')
            {
                D=2;
                k=dir(1,0);
            }
            else if(arr[from.x][from.y-1]=='.')
            {
                D=3;
                k=dir(0,-1);
            }
            else if(arr[from.x-1][from.y]=='.')
            {
                D=4;
                k=dir(-1,0);
            }
            if(befD)
            {
                if((befD==1&&D==4)||(befD==2&&D==1)||(befD==3&&D==2)||(befD==4&&D==3))
                printf("Turn left.\n");
                if((befD==1&&D==2)||(befD==2&&D==3)||(befD==3&&D==4)||(befD==4&&D==1))
                printf("Turn right.\n");
            }
            printf("Continue %d km.\n",k);
            befD=D;
        }
        printf("You have reached your destination.\n\n");
        /*for(i=0;i<row+2;i++)
        {
            for(j=0;j<col+2;j++)
            {
                printf("%2d ",val[i][j]);
            }
            printf("\n");
        }*/



    }

    return 0;
}

/*
2
10 8
+----------+
|          |
|   *****  |
|   *   *  |
| ***   *  |
|       *  |
|     ***  |
|       *  |
|       *  |
+----------+
1 3 7 7
15 15
+---------------+
|***            |
|  *            |
|  *            |
|  *            |
|  ********     |
|      ****     |
|      *****    |
|      ****     |
|        ****   |
|           *   |
|          ***  |
|          * *  |
|          ***  |
|           *   |
|         ******|
+---------------+
0 0 14 14
*/


