#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

int par[110],t,p,i,m,res1,res2,n,k;

struct node
{
    int x,y,val;
};

node adj[100010];

int comp(node a,node b)
{
    return a.val<b.val;
}

int comp2(node a,node b)
{
    return a.val>b.val;
}


main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n;
        n++;
        m=0;
        for(i=0;;i++)
        {
            cin>>adj[m].x>>adj[m].y>>adj[m].val;
            if(adj[m].x==0&&adj[m].y==0&&adj[m].val==0)
                break;
            m++;
        }
        for(i=0;i<n;i++)
            par[i]=i;
        sort(adj,adj+m,comp);
        for(res1=0,i=0,k=1;k<n&&i<m;i++)
        {
            int u=adj[i].x;
            int v=adj[i].y;
            while(par[u]!=u)
                u=par[u];
            while(par[v]!=v)
                v=par[v];
            if(u!=v)
            {
                res1+=adj[i].val;
                par[u]=v;
                k++;
            }
        }
        for(i=0;i<n;i++)
            par[i]=i;
        sort(adj,adj+m,comp2);
        for(res2=0,i=0,k=1;k<n&&i<m;i++)
        {
            int u=adj[i].x;
            int v=adj[i].y;
            while(par[u]!=u)
                u=par[u];
            while(par[v]!=v)
                v=par[v];
            if(u!=v)
            {
                res2+=adj[i].val;
                par[u]=v;
                k++;
            }
        }
        res1+=res2;
        if(res1%2==0)
            printf("Case %d: %d\n",p,res1/2);
        else
            printf("Case %d: %d/2\n",p,res1);
    }
    return 0;
}

