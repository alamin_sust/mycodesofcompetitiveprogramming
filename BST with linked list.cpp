#include<iostream>
using namespace std;
struct node
{
    int val;
    node *left, *right;
};

struct binarytree
{
    node *root;
    binarytree(){root = NULL;}
    void insert(int);
    void preorder(node*);
    void postorder(node*);
    void inorder(node*);
    void search(node*, int);
    void delval(node*, int);
};

void binarytree::insert(int x)
{
    if(root==NULL)
    {
        node *temp = new node;
        temp->val = x;
        root = temp;
        return;
    }
    else
    {
        node *temp=root;
        while(temp!=NULL)
        {
            if(x>temp->val)
            temp=temp->right;
            else
            temp=temp->left;
        }
        temp->val=x;
    }
    return;
}

void binarytree::preorder(node *r)
{
    if(r!= NULL)
    {
        cout<<r->val<<' ';
        preorder(r->left);
        preorder(r->right);
        return;
    }
}
void binarytree::postorder(node *r)
{
    if(r!= NULL)
    {
        postorder(r->left);
        postorder(r->right);
        cout<<r->val<<' ';
        return;
    }
}
void binarytree::inorder(node *r)
{
    if(r!= NULL)
    {
        inorder(r->left);
        cout<<r->val<<' ';
        inorder(r->right);
        return;
    }
}
int main()
{
    binarytree t;
    int i;
    cin>> i;
    while(i != 0)
    {
        t.insert(i);
        cin>>i;
    }
    cout<<"Preorder : ";
    t.preorder(t.root);
    cout<<"\nPostorder : ";
    t.postorder(t.root);
    cout<<"\nInordre : ";
    t.inorder(t.root);
}

