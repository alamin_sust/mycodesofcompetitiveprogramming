/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

vector<int>adj[1010];
int par[1010],max_bw[1010],bandwidth[1010][1010];
queue<int>q;

void send_flow(int node,int src,int bw)
{
    while(node!=src)
    {
        bandwidth[par[node]][node]-=bw;
        //bandwidth[node][par[node]]-=bw;
        node=par[node];
    }
    return;
}

int bfs(int src,int dst)
{
    memset(par,0,sizeof(par));
   while(!q.empty())
    q.pop();
   q.push(src);
   par[src]=-1;
   max_bw[src]=99999999;
   while(!q.empty())
   {
       int u=q.front();
       for(int i=0;i<adj[u].size();i++)
       {
           int v=adj[u][i];
           if(bandwidth[u][v]>0&&par[v]==0)
            {
                par[v]=u;
                max_bw[v]=min(max_bw[u],bandwidth[u][v]);
                q.push(v);
                if(v==dst)
                {
                    send_flow(v,src,max_bw[v]);
                    return max_bw[v];
                }
            }
       }
       q.pop();
   }
   return 0;
}

main()
{
    int i,j,p,s,t,n,flow,maxflow,c,bw,from,to;
    for(p=1;cin>>n;p++)
    {
        if(n==0)
            break;
        for(i=1;i<=n;i++)
            {
                adj[i].clear();
                for(j=1;j<=n;j++)
                    bandwidth[i][j]=0;
            }

        cin>>s>>t>>c;
        for(i=1;i<=c;i++)
        {
            scanf(" %d %d %d",&from,&to,&bw);
            adj[from].push_back(to);
            adj[to].push_back(from);
            bandwidth[from][to]+=bw;
            bandwidth[to][from]+=bw;
        }
        maxflow=0;
        while(1)
        {
            flow=bfs(s,t);
            maxflow+=flow;
            if(flow==0)
                break;
        }
        printf("Network %d\n",p);
        printf("The bandwidth is %d.\n\n",maxflow);
    }
    return 0;
}
