/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node{
int s,t;
};
node det;
vector<node>vc;
int lst,i,n,fc,sc,fff,sss,fcum[100010],scum[100010],arr[100010],ss[100010],tc,a,b,ff[100010];

int comp(node a,node b)
{
    if(a.s==b.s)
        return a.t<b.t;
    return a.s<b.s;
}

int main()
{
    cin>>n;
    for(i=1; i<=n; i++)
    {
        scanf(" %d",&arr[i]);
        if(arr[i]==1)
        {
            fc++;
            ff[fc]=i;
        }
        else
        {
            sc++;
            ss[sc]=i;
        }
        fcum[i]=fc;
        scum[i]=sc;
    }
    //for(i=1;i<=10;i++)
    //{
    //    printf("%d %d\n",fc,sc);
    //}
    for(i=1;i<=n;i++)
    {
        tc=i;
        a=b=0;
        fff=sss=0;
        lst=0;
        while(1)
        {
            //printf("%d %d\n",a,b);
            //getchar();
            if(a==fc&&b==sc)
            {
                if((fff>sss&&lst==1)||(fff<sss&&lst==2))
                {det.s=max(fff,sss);
                det.t=tc;
                vc.push_back(det);}
                break;
            }
            if(ff[tc+a]==0&&ss[tc+b]==0)
            break;
            if((ff[tc+a]<ss[tc+b]&&ff[tc+a]!=0)||(ss[tc+b]==0))
            {
                int rrr=a;
                a=fcum[ff[tc+rrr]];
                b=scum[ff[tc+rrr]];
                lst=1;
                fff++;
            }
            else if((ff[tc+a]>ss[tc+b]&&ss[tc+b]!=0)||(ff[tc+a]==0))
            {
                int rrr=b;
                a=fcum[ss[tc+rrr]];
                b=scum[ss[tc+rrr]];
                lst=2;
                sss++;
            }
            else break;
        }
    }
    //sort(vc.begin(),vc.end(),comp);
    printf("%d\n",vc.size());
    for(i=vc.size()-1;i>=0;i--)
    {
        printf("%d %d\n",vc[i].s,vc[i].t);
    }
    return 0;
}

