/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int habi=0,arr[510][510],cnt,sum[510],sum2[510];


void reset(int x,int y)
{
    arr[x][y]=1;
    cnt++;
    if(arr[x+1][y]==0)
        reset(x+1,y);
    if(arr[x-1][y]==0)
        reset(x-1,y);
    if(arr[x][y+1]==0)
        reset(x,y+1);
    if(arr[x][y-1]==0)
        reset(x,y-1);
    return;
}

int func(int x,int y,int r,int c)
{
    int i,j,ret=0;
    for(i=x,j=y;; i+=r,j+=c)
    {
        if(arr[i][j]==0)
            ret++;
        else
            break;
    }
    return ret;
}

int func2(int x,int y,int r,int c)
{
    int i,j,ret=1;
    for(i=x+r,j=y+c;; i+=r,j+=c)
    {
        if(arr[i][j]==0)
        {
            if(arr[i-r][j]!=0&&arr[i][j-c]!=0)
                break;
            ret++;
        }
        else
            break;
    }
    return ret;
}

int compute(int x,int y)
{
    if(arr[x+1][y]!=0&&arr[x][y+1]!=0&&arr[x][y-1]!=0&&arr[x-1][y]!=0)
    {
        //habi++;
        reset(x,y);
        return 1;
    }
    if(arr[x][y+1]==0&&arr[x+1][y]==0)
    {
        if(func(x,y,1,0)==func(x,y,0,1))
        {
            int tmp=sum[func(x,y,1,0)];
            //printf("erwter\n");
            habi++;
            cnt=0;
            reset(x,y);
            if(tmp==cnt)
                return 1;
            //printf("0000000001   %d %d\n",cnt,tmp);
            return 0;
        }
        return 0;
    }
    if(arr[x+1][y]==0)
    {
        int tp=func(x,y,1,0);
        int newx=x+tp-1;
        int newy=y;
        //printf("%d>>>>>>>>>>>>>>%d\n",newx,newy);
        if(arr[newx][newy+1]==0&&arr[newx][newy-1]==0)
        {
            // printf("llllllll--%d\n",tp);
            int tmp=sum2[func(newx,newy,0,1)];
            if(tp==func(newx,newy,0,1)&&func(newx,newy,0,1)==func(newx,newy,0,-1))
            {
                cnt=0;
                habi++;
                reset(newx,newy);
                if(cnt==tmp)
                    return 1;
              //    printf("0000000002   %d %d\n",cnt,tmp);
                return 0;
            }
            return 0;
        }
        if(arr[newx][newy+1]!=0&&arr[newx][newy-1]!=0)
        {
            //printf(">>>>>>>>>>>>>>1\n");
            if(arr[newx-1][newy-1]==0)
            {
                tp=func2(newx,newy,-1,-1);
                newx=newx-tp+1;
                newy=newy-tp+1;
                int tmp=sum2[func2(newx,newy,-1,1)];
                if(tp==func2(newx,newy,-1,1))
                {
                    //printf(">>>>>>>>>>>>>>1\n");
                    habi++;
                    cnt=0;
                    reset(newx,newy);
                    if(cnt==tmp)
                        return 1;
                //        printf("0000000003   %d %d\n",cnt,tmp);
                    return 0;
                }
                return 0;
            }
            else if(arr[newx-1][newy+1]==0)
            {
                tp=func2(newx,newy,-1,1);
                newx=newx-tp+1;
                newy=newy+tp-1;
                //printf("...");
                int tmp=sum2[func2(newx,newy,-1,-1)];
                 //printf("%d %d\n",tp,tmp);
                if(tp==func2(newx,newy,-1,-1))
                {
                    //   printf(">>>>>>>>>>>>>>2\n");
                    habi++;
                    cnt=0;
                    reset(newx,newy);
                    if(cnt==tmp)
                        return 1;
                  //    printf("0000000004   %d %d\n",cnt,tmp);
                    return 0;
                }
                return 0;
            }
            return 0;
        }
        if(arr[newx][newy+1]==0)
        {
            int tmp=sum[func(newx,newy,0,1)];
            if(tp==func(newx,newy,0,1))
            {
                habi++;
                cnt=0;
                reset(newx,newy);
                if(cnt==tmp)
                    return 1;
                //printf("0000000005   %d %d\n",cnt,tmp);
                return 0;
            }
            return 0;
        }
        else if(arr[newx][newy-1]==0)
        {
            int tmp=sum[func(newx,newy,0,-1)];
            if(tp==func(newx,newy,0,-1))
            {
                habi++;
                cnt=0;
                reset(newx,newy);
                if(cnt==tmp)
                    return 1;
                //printf("0000000006   %d %d\n",cnt,tmp);
                return 0;
            }
            return 0;
        }
        return 0;
    }
    if(arr[x][y+1]==0)
    {
        int tp=func(x,y,0,1);
        int newx=x;
        int newy=y+tp-1;
        //printf("%d %d**************\n",newx,newy);
        if(arr[newx+1][newy]==0&&arr[newx-1][newy]==0)
        {
            return 0;
        }
        if(arr[newx+1][newy]!=0&&arr[newx-1][newy]!=0)
        {
            //  printf("maggie\n");
            tp=func2(newx,newy,1,-1);
            newx=newx+tp-1;
            newy=newy-tp+1;
            int tmp=sum2[func2(newx,newy,-1,-1)];
            if(tp==func2(newx,newy,-1,-1))
            {
                cnt=0;
                habi++;
                reset(newx,newy);
                if(cnt==tmp)
                    return 1;
                //printf("0000000007   %d %d %d %d %d\n",newx,newy,cnt,tmp,tp);
                return 0;
            }
            return 0;
        }
        if(arr[newx-1][newy]==0)
        {
            int tmp=sum[func(newx,newy,-1,0)];
            if(tp==func(newx,newy,-1,0))
            {
                cnt=0;
                habi++;
                reset(newx,newy);
                if(cnt==tmp)
                    return 1;
                //printf("0000000008   %d %d\n",cnt,tmp);
                return 0;
            }
            return 0;
        }
        if(arr[newx+1][newy]==0)
        {
            int tmp=sum[func(newx,newy,1,0)];
            if(tp==func(newx,newy,1,0))
            {
                cnt=0;
                habi++;
                reset(newx,newy);
                if(cnt==tmp)
                    return 1;
                //printf("0000000009   %d %d\n",cnt,tmp);
                return 0;
            }
            return 0;
        }
        return 0;
    }
    return 0;
}

void sumcalc(void)
{
    sum2[1]=1;
    int j=3;
    for(int i=2; i<=207; j+=2, i++)
    {
        sum2[i]=sum2[i-1]+j;
        //if(i<10)
        //printf("%d\n",sum[i]);
    }
    j=2;
    sum[1]=1;
    for(int i=2; i<=207; j++, i++)
    {
        sum[i]=sum[i-1]+j;
        //if(i<10)
        //printf("%d\n",sum[i]);
    }

}

main()
{
    int t,p,mr,mc,row,cut,k,res,col,i,j,x1,x2,y1,y2;
    sumcalc();
    cin>>t;
    for(p=1; p<=t; p++)
    {
        habi=0;
        scanf(" %d %d %d",&col,&row,&cut);
        mr=row*4+1;
        mc=col*4+1;
        for(i=0; i<=mr; i++)
        {
            for(j=0; j<=mc; j++)
            {
                if(i==1||i==mr||j==1||j==mc)
                    arr[i][j]=1;
                else
                    arr[i][j]=0;
            }
        }
        for(i=1; i<=cut; i++)
        {
            scanf(" %d %d %d %d",&y1,&x1,&y2,&x2);
            x1=(x1*4)+1;
            y1=(y1*4)+1;
            x2=(x2*4)+1;
            y2=(y2*4)+1;
            //printf("%d %d %d %d..\n",x1,y1,x2,y2);
            if(x1==x2)
            {
                if(y1>y2)
                    swap(y1,y2);
                for(j=y1; j<=y2; j++)
                {
                    arr[x1][j]=i;
                }
            }
            else if(y1==y2)
            {
                if(x1>x2)
                    swap(x1,x2);
                for(j=x1; j<=x2; j++)
                {
                    arr[j][y1]=i;
                }
            }
            else if(x1<x2)
            {
                if(y1>y2)
                {
                    for(j=x1,k=y1; j<=x2&& k>=y2; k--,j++)
                    {
                        arr[j][k]=i;
                    }
                }
                else
                {
                    for(j=x1,k=y1; j<=x2&& k<=y2; k++,j++)
                    {
                        //printf("%d %d---\n",j,k);
                        arr[j][k]=i;
                    }
                }
            }
            else
            {
                if(y1>y2)
                {
                    for(j=x1,k=y1; j>=x2&& k>=y2; k--,j--)
                    {
                        arr[j][k]=i;
                    }
                }
                else
                {
                    for(j=x1,k=y1; j>=x2&& k<=y2; k++,j--)
                    {
                        arr[j][k]=i;
                    }
                }
            }
        }
        //for(i=1; i<=mr; i++)
        //{
          //  for(j=1; j<=mc; j++)
           // {
            //    printf("%d ",arr[i][j]);
           // }
           // printf("\n");
        //}
        res=0;
        for(i=1; i<=mr; i++)
        {
            for(j=1; j<=mc; j++)
            {
                if(arr[i][j]==0)
                {
                    res+=compute(i,j);
                    reset(i,j);
                }
            }
        }
        //printf("====%d\n",habi);
        printf("%d",res);
        if(p!=t)
        printf("\n");
        printf("\n");
    }
    return 0;
}

