/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll fact[2000010],M=1000000007LL;

ll bigmod(ll a,ll b,ll m)
{
    ll ret=1LL;
    while(b>0LL)
    {
        if((b%2LL)==1LL)
        {
            ret=(ret*a)%m;
        }
        b/=2LL;
        a=(a*a)%m;
    }
    return ret;
}
ll inverse_mod(ll b,ll m)
{
   return bigmod(b,m-2LL,m);
}

void fct(void)
{
    fact[0]=1LL;
    for(ll i=1;i<=2000005LL;i++)
        fact[i]=(fact[i-1]*i)%M;
    return;
}

int main()
{
    ll t,p,i,n,k,res,tp,ttp;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    fct();
    scanf(" %lld",&t);

    for(p=1LL; p<=t; p++)
    {
        scanf(" %lld %lld",&n,&k);
        res=0LL;
        ttp=inverse_mod(fact[k],M);
        for(i=0LL; i<n; i++)
        {
            tp=(fact[k+i]%M);
            tp=(tp*inverse_mod(fact[i],M))%M;
            tp=(tp*ttp)%M;
            tp=(tp*(n-i))%M;
            if(i!=0LL)
                tp=(tp*2LL)%M;
            res=(res+tp)%M;
        }

        printf("%lld\n",res);

    }

    return 0;
}







