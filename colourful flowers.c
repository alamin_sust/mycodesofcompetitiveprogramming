#include <iostream>
#include <cstdio>
#include <cmath>
using namespace std;

#define PI 2*acos(0)

double triangle_area(double a,double b,double c)
{
    double s=(a+b+c)/2.0;
    return sqrt(s*(s-a)*(s-b)*(s-c));
}

int main()
{
    double a,b,c,area;

    while(scanf("%lf%lf%lf",&a,&b,&c)!=EOF)
    {
        area=triangle_area(a,b,c);

        printf("%.4lf ",PI*(a*b*c/4.0/area)*(a*b*c/4.0/area)-area);

        printf("%.4lf ",area-PI*(2.0*area/(a+b+c))*(2.0*area/(a+b+c)));

        printf("%.4lf\n",PI*(2.0*area/(a+b+c))*(2.0*area/(a+b+c)));

    }

    return 0;
}
