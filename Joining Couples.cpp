/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int flag[100010],lev[100010],par[100010],gpar[100010],p,n,in;

void root_is_the_cycle(int x)
{
    while(flag[x]==1)
    {
        lev[x]=1;
        gpar[x]=p;
        flag[x]=2;
        x=par[x];
    }
    p++;
    return;
}

void build(int x)
{
    while(flag[x]==0)
    {
        flag[x]=1;
        x=par[x];
    }
    if(flag[x]==1&&gpar[x]==0)
        root_is_the_cycle(x);
    return;
}

void level(int x)
{
    if(lev[x]!=0)
        return;
    level(par[x]);
    lev[x]=1+lev[par[x]];
    gpar[x]=gpar[par[x]];
    return;
}

main()
{
    int i,m,u,v,res;
    while(scanf("%d",&n)!=EOF)
    {
        for(i=1; i<=n; i++)
        {
            scanf(" %d",&in);
            par[i]=in;
            flag[i]=0;
            gpar[i]=0;
            lev[i]=0;
        }
        for(p=1,i=1; i<=n; i++)
        {
            if(flag[i]==0)
            {
                build(i);
                level(i);
            }
        }


        // for(i=1;i<=n;i++)
        //{
        //   printf("par=%d gpar=%d lev=%d\n",par[i],gpar[i],lev[i]);
        // }


        scanf(" %d",&m);
        for(i=1; i<=m; i++)
        {
            scanf(" %d %d",&u,&v);
            if(gpar[u]!=gpar[v])
            {
                printf("-1\n");
                continue;
            }
            res=0;
            if(lev[u]>lev[v])
            {
                while(lev[u]!=lev[v])
                {
                    u=par[u];
                    res++;
                }
            }
            if(lev[u]<lev[v])
            {
                while(lev[u]!=lev[v])
                {
                    v=par[v];
                    res++;
                }
            }
            while(u!=v)
            {
                if(flag[u]==2)
                {
                    int tp1=0,tp2=0,tpu=u;
                    //   printf("..%d %d\n",u,v);
                    while(u!=v)
                        u=par[u],tp1++;
                    u=tpu;
                    while(u!=v)
                        v=par[v],tp2++;
                    res+=min(tp1,tp2);
                    break;
                }
                res+=2;
                u=par[u];
                v=par[v];
            }
            printf("%d\n",res);
        }
    }
    return 0;
}
