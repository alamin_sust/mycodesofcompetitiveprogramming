#include<stdio.h>
#include<queue>
#include<math.h>
using namespace std;

main()
{
    long long test, num, i, sq, a, b, sum;
    queue< long long > q1, q2;
    scanf("%lld", &test);
    while( test -- )
    {
        scanf("%lld", &num);
        if( num <= 0 )
        {
            printf("0\n");
            continue;
        }
        sq = sqrt( num );
        q1.push( num );
        q2.push( 1 );
        for( i = 2; i <= sq; i ++ )
        {
            if( i != num / i )
            {
                q2.push( i );
            }
            q1.push( num / i );
        }
        sum = q1.front();
        a = q1.front();
        q1.pop();
        while( !q1.empty() )
        {
            sum += q1.front();
            b = q1.front();
            q1.pop();
            if( !q2.empty() )
            {
                sum += q2.front() * ( a - b );
                q2.pop();
            }
            a = b;
        }
        while( !q2.empty() )
        {
            sum += q2.front() * ( a - q2.front() );
            q2.pop();
        }
        printf("%lld\n", sum);
    }
    return 0;
}

