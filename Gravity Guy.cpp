/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int t,p,i,tp,res,st,l;
char arr[2][200010];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %s",&arr[0]);
        scanf(" %s",&arr[1]);
        l=strlen(arr[0]);
        st=0;
        res=inf;
        tp=0;
        if(arr[0][0]=='.')
        for(i=0;i<l;)
        {
            if((i+1<l)&&arr[0][i+1]=='#'&&arr[1][i+1]=='#')
                break;
            if(i==l-1)
            {
                res=tp;
                break;
            }
            if(arr[st][i+1]=='.')
            {
                i++;
            }
            else if(arr[st^1][i]=='.')
            {
                st^=1;
                tp++;
            }
            else if(arr[st^1][i+1]=='.')
            {
                st^=1;
                tp++;
                i++;
            }
            else
            {
                break;
            }
        }
        st=1;
        tp=0;
        if(arr[1][0]=='.')
        for(i=0;i<l;)
        {
            if((i+1<l)&&arr[0][i+1]=='#'&&arr[1][i+1]=='#')
                break;
            if(i==l-1)
            {
                res=min(res,tp);
                break;
            }
            if(arr[st][i+1]=='.')
            {
                i++;
            }
            else if(arr[st^1][i]=='.')
            {
                st^=1;
                tp++;
            }
            else if(arr[st^1][i+1]=='.')
            {
                st^=1;
                tp++;
                i++;
            }
            else
            {
                break;
            }
        }

        if(res==inf)
        printf("No\n");
        else
        printf("Yes\n%d\n",res);

    }

    return 0;
}


