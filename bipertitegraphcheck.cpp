#include<stdio.h>
#include<vector>

using namespace std;

vector<int>lst[2020];
int colour[2020], f;

void dfs(int n, int col);

int main()
{
    int n, m, i, j, k, a, b, c=1, cas;
    scanf("%d", &cas);
    while(cas--)
    {
        scanf("%d %d", &n, &m);
        while(m--)
        {
            scanf("%d %d", &a, &b);
            lst[a].push_back(b);
            lst[b].push_back(a);
        }
        f=0;
        for(i=1;i<=n;i++)
        {
            if(!colour[i])
                dfs(i, 1);
            if(f)
                break;
        }
        if(f)
            printf("Scenario #%d:\nSuspicious bugs found!\n", c++);
        else
            printf("Scenario #%d:\nNo suspicious bugs found!\n", c++);
        for(i=1;i<=n;i++)
        {
            colour[i]=0;
            lst[i].clear();
        }
    }
    return 0;
}

void dfs(int n, int col)
{
    if(f)
        return ;
    if(colour[n])
    {
        if(colour[n]!=col)
        {
            f=1;
            return ;
        }
        return ;
    }
    colour[n]=col;
    int i;
    for(i=0;i<lst[n].size();i++)
    {
        dfs(lst[n][i], col-!(col&1)+(col&1));
    }
    return ;
}
