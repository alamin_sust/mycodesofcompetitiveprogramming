/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

vector<int>v,itit;
int t,p,n,totcnt,g_iter,iter;

int gendiff(int baki,int val)
{
    int ret=0;
    int nowval=1;
    g_iter=0;
    while(nowval*val<=baki)
    {
        nowval*=val;
        g_iter++;
    }
    return ret=baki-nowval;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        //scanf(" %d",&n);

        for(int x=2;x<=100000;x++){
                n=x;
        totcnt=0;
        itit.clear();
        while(n>1)
        {
            int mx=1000010;
           int now=2;
            for(int i=2;i<=50;i++)
            {
               int diff=gendiff(n,i);
                if(diff<mx)
                {
                    mx=diff;
                    now=i;
                    iter=g_iter;
                }
            }
            n=mx;
            itit.push_back(iter);
            totcnt+=iter*now;
        }
        sort(itit.begin(),itit.end());


        int sum=0;
        for(int i=0;i<itit.size();i++)
            sum+=itit[i];

       totcnt+=itit[itit.size()-1]*itit.size()-sum;
       if(totcnt>98)
       printf("%d\n",totcnt);
        }
    }

    return 0;
}

