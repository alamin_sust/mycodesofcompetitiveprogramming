/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int MOD=1000000007,dp[3][802][102][102],tr,rr,br,rb,bb,tot;

int rec(int prev,int ntr,int nrr,int nbb)
{
    if(ntr>tr||nrr>rr||nbb>bb)
        return 0;
    if((ntr+nrr+nbb)==tot)
        return 1;
    int &ret=dp[prev][ntr][nrr][nbb];
    if(ret!=-1)
        return ret;
    ret=0;
    if(prev!=2)
    {
        if(prev==0)
        {
            ret=(ret+rec(0,ntr,nrr+1,nbb))%MOD;
            ret=(ret+rec(1,ntr+1,nrr,nbb))%MOD;
        }
        else
        {
            ret=(ret+rec(0,ntr+1,nrr,nbb))%MOD;
            ret=(ret+rec(1,ntr,nrr,nbb+1))%MOD;
        }
    }
    else
    {
        if((tr%2)==0)
        {
        ret=(ret+rec(0,ntr,nrr,nbb))%MOD;
        ret=(ret+rec(1,ntr,nrr,nbb))%MOD;
        }
        else if(rb>br)
        {
            ret=(ret+rec(0,ntr,nrr,nbb))%MOD;
        }
        else
        {
            ret=(ret+rec(1,ntr,nrr,nbb))%MOD;
        }
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    scanf(" %d %d %d %d",&rr,&rb,&bb,&br);

    tot=rr+rb+bb+br;
    tr=rb+br;
    memset(dp,-1,sizeof(dp));
//    int i,j,k,l;
//    for(i=0;i<3;i++)
//    {
//        for(j=0;j<1002;j++)
//        {
//            for(k=0;k<202;k++)
//            {
//                for(l=0;l<202;l++)
//                {
//                    dp[i][j][k][l]=-1;
//
//                }
//            }
//        }
//    }
    if(rr||rb||bb||br)
    printf("%d\n",rec(2,0,0,0));
    else
    printf("2\n");

    return 0;
}

