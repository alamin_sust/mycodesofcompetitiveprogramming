#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class TheConsecutiveIntegersDivTwo
{
public:
	int find(vector <int> numbers, int k)
	{
	    int ret=50000010;
	    if(k==1)
            return 0;
	    sort(numbers.begin(),numbers.end());
	    for(int i=1;i<numbers.size();i++)
        {
            if(numbers[i]==numbers[i-1])
                ret=min(ret,1);
            else
            ret=min(ret,numbers[i]-numbers[i-1]-1);
        }
	    return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	TheConsecutiveIntegersDivTwo objectTheConsecutiveIntegersDivTwo;

	//test case0
	vector <int> param00;
	param00.push_back(4);
	param00.push_back(47);
	param00.push_back(7);
	int param01 = 2;
	int ret0 = objectTheConsecutiveIntegersDivTwo.find(param00,param01);
	int need0 = 2;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(1);
	param10.push_back(100);
	int param11 = 1;
	int ret1 = objectTheConsecutiveIntegersDivTwo.find(param10,param11);
	int need1 = 0;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(-96);
	param20.push_back(-53);
	param20.push_back(82);
	param20.push_back(-24);
	param20.push_back(6);
	param20.push_back(-75);
	int param21 = 2;
	int ret2 = objectTheConsecutiveIntegersDivTwo.find(param20,param21);
	int need2 = 20;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(64);
	param30.push_back(-31);
	param30.push_back(-56);
	int param31 = 2;
	int ret3 = objectTheConsecutiveIntegersDivTwo.find(param30,param31);
	int need3 = 24;
	assert_eq(3,ret3,need3);

}


