#include<stdio.h>
#include<iostream>
using namespace std;

struct node
{
     int val;
     node *ptr;
};
struct singly{

     node *head,*tail;
     singly(){head=tail=NULL;}
     void insert(int x);
     void print();
     void delhead();
     void deltail();
     void delelement(int x);
     void insertinto(int x,int pos);
};
void singly::insert(int x)
{
    node *temp=new node;
    temp->val=x;
    temp->ptr=NULL;
    if(head==NULL)
    {head=tail=temp;return;}
    tail->ptr=temp;
    tail=temp;
}
void singly::print()
{
    node *temp=head;
    printf("Printing.....\n");
    while(temp!=NULL)
    {
        cout<<temp->val<<endl;
        temp=temp->ptr;
    }
}
void singly::delhead()
{
    node *temp=new node;
    if(head==NULL)
    {cout<<"underflow...."<<endl;
    return;}
    else
    {temp=head->ptr;
    head=temp;
    return;}

}
void singly::deltail()
{
    node *temp=head;
    if(head==NULL)
    {cout<<"no tail...."<<endl;
    return;}
    if(temp->ptr==NULL)
    {head=tail=NULL;return;}
    while(temp->ptr!=NULL)
    {
        tail=temp;
        temp=temp->ptr;
    }
    tail->ptr=NULL;
}
void singly::delelement(int x)
{
    node *temp=head;
    node *temp2=new node;
    if(head==NULL)
    {cout<<"are you kidding!...."<<endl;
    return;}
    if(head->val==x)
    {delhead();
    return;}
    while(temp->val!=x)
    {
        temp2=temp;
        temp=temp->ptr;
    }
    temp2->ptr=temp->ptr;
    delete temp;
    return;
}

void singly::insertinto(int x,int pos)
{
    int i;
    node *temp=new node;
    node *temp2=new node;
    temp=head;
    if(pos==1)
    {
        temp2->val=x;
        temp2->ptr=head;
        head=temp2;
        return;
    }
    i=2;
    while(i!=pos)
    {
        temp=temp->ptr;
          i++;
    }
    temp2->ptr=temp->ptr;
    temp2->val=x;
    temp->ptr=temp2;
}


main()
{
      singly a;
      a.insert(5);
      a.insert(6);
      a.insert(7);
      a.print();
      a.insert(8);
      a.insert(9);
      a.print();
      a.insert(1);
      a.insert(2);
      a.print();
      a.delhead();
      a.print();
      a.deltail();
      a.print();
      a.delelement(6);
      a.print();
      a.insertinto(2,3);
      a.print();
      return 0;
}

