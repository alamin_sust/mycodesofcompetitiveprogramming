/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll t,k,dp[1000010],MX=100000LL,MOD=1000000007LL,from,to;

ll rec(ll pos)
{
    if(pos>MX)
        return 0LL;
    if(pos==MX)
        return 1LL;
    ll &ret=dp[pos];
    if(ret!=-1)
        return ret;
    ret=0;
    ret=(ret+rec(pos+1))%MOD;
    ret=(ret+rec(pos+k))%MOD;
    return ret;
}

main()
{
    cin>>t>>k;
    memset(dp,-1,sizeof(dp));
    rec(0);
    for(ll i=MX-2LL;i>=0;i--)
        dp[i]=(dp[i]+dp[i+1])%MOD;
    dp[MX]=0;
    for(ll i=1;i<=t;i++)
    {
        scanf(" %I64d %I64d",&from,&to);
        printf("%I64d\n",(dp[MX-to]-dp[MX-from+1]+MOD)%MOD);
    }
    return 0;
}

