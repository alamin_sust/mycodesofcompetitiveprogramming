/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int MOD=1000000007,dp[3][102][102][102][102],rr,br,rb,bb,tot;

int rec(int prev,int nrr,int nrb,int nbb,int nbr)
{
    if(nrr>rr||nrb>rb||nbb>bb||nbr>br)
        return dp[prev][nrr][nrb][nbb][nbr]=0;
    if((nrr+nrb+nbb+nbr)==tot)
        return dp[prev][nrr][nrb][nbb][nbr]=1;
    int &ret=dp[prev][nrr][nrb][nbb][nbr];
    if(ret!=-1)
        return ret;
    ret=0;
    if(prev!=2)
    {
        if(prev==0)
        {
            ret=(ret+rec(0,nrr+1,nrb,nbb,nbr))%MOD;
            ret=(ret+rec(1,nrr,nrb+1,nbb,nbr))%MOD;
        }
        else
        {
            ret=(ret+rec(0,nrr,nrb,nbb,nbr+1))%MOD;
            ret=(ret+rec(1,nrr,nrb,nbb+1,nbr))%MOD;
        }
    }
    else
    {
        ret=(ret+rec(0,nrr,nrb,nbb,nbr))%MOD;
        ret=(ret+rec(1,nrr,nrb,nbb,nbr))%MOD;
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    scanf(" %d %d %d %d",&rr,&rb,&bb,&br);

    tot=rr+rb+bb+br;

    memset(dp,-1,sizeof(dp));

    printf("%d\n",rec(2,0,0,0,0));

    return 0;
}

