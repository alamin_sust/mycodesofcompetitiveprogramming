/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
#define MAX 100
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int sieve[MAX+10],primes[MAX+10],cnt=0,p,t,in;
bool arr[210];

void sieve_(void)
{
    sieve[0]=sieve[1]=1;
    for(int i=2;i<=MAX;i++)
    {
        if(sieve[i]==0)
        {
            primes[cnt++]=i;
            for(int j=2;i*j<=MAX;j++)
                sieve[i*j]=1;
        }
    }
    return;
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);
    sieve_();

    for(int i=0;i<cnt;i++) {
        for(int j=0;j<cnt;j++) {
            for(int k=0;k<cnt;k++) {
                for(int l=0;l<cnt;l++) {
                    if(i==j||k==l||(primes[i]*primes[j]+primes[k]*primes[l])>200) continue;
                    arr[primes[i]*primes[j]+primes[k]*primes[l]]=true;
                }
            }
        }
    }

    scanf(" %d",&t);

    for(p=1;p<=t;p++) {
        scanf(" %d",&in);
        if(arr[in]) {
            printf("YES\n");
        } else {
            printf("NO\n");
        }
    }

    return 0;
}
