#include <sstream>
#include <queue>
#include <stack>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <complex>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <bitset>
#include <list>
#include <string.h>
#include <assert.h>
#include <time.h>

#define LOCAL_TEST true
#define SZ(x) ((int)x.size())
#define clr(name,val) memset(name,(val),sizeof(name));
#define Assign(name,val) name.assign(val+1,vector<int>())
#define EPS .000000001
#define ll long long
#define SF scanf
#define PF printf
#define psb(b) push_back((b))
#define ppb() pop_back()
#define oo (1<<30)
#define mp make_pair
#define fs first
#define sc second
#define rep(var,s,n) for(var=(s);var<(n);(var)++)
#define rvp(var,s,n) for(var=(n-1);var>(s-1);(var)--)
#define read_ freopen("input.txt","r",stdin)
#define write_ freopen("outputtest.txt","w",stdout)

using namespace std;

int compare(string a,string b)
{
    if(a==b) return 0;
    if(SZ(a)==SZ(b))
    {
        for(int i=0; i<SZ(a); i++)
        {
            if(a[i]>b[i]) return 1;
            else if(a[i]<b[i]) return -1;
        }
        return -1;
    }
    if(SZ(a)>SZ(b)) return 1;
    return -1;
}
string string_add(string b,string a)
{
    if(SZ(a)>SZ(b)) swap(a,b);
    a=string((SZ(b)-SZ(a)),'0')+a;
    int carry=0,add;
    string res;
    for(int i=SZ(a)-1; i>-1; i--)
    {
        add=(int)(a[i]-'0')+(int)(b[i]-'0')+carry;
        carry=add/10;
        res+=(add%10)+'0';
    }
    if(carry) res+=carry+'0';
    reverse(res.begin(),res.end());
    return res;
}
string string_mul(string a,string b)
{
    if(SZ(a)>SZ(b)) swap(a,b);
    string res,tres;
    int l=0,carry,mul;
    for(int i=SZ(a)-1; i>-1; i--)
    {
        tres=string(l,'0');
        carry=0;
        for(int j=SZ(b)-1; j>-1; j--)
        {
            mul=(int)(a[i]-'0')*(int)(b[j]-'0')+carry;
            carry=mul/10;
            tres+=(mul%10)+'0';
        }
        if(carry) tres+=carry+'0';
        reverse(tres.begin(),tres.end());
        res=string_add(tres,res);
        l++;
    }
    return res;
}
void cut_leading_zero(string &res)
{
    int i;
    for(i=0; i<SZ(res); i++)
    {
        if(res[i]!='0') break;
    }
    res=res.substr(i);
    if(!SZ(res)) res="0";
    return;
}
ll big_mod(ll a,ll p,ll MOD)
{
    if(p==0) return 1L;
    ll ret=big_mod(a,p/2,MOD);
    ret=((ret*ret)%MOD+MOD)%MOD;
    if(p%2) ret=((ret*a)%MOD+MOD)%MOD;
    return ret;
}
ll power(ll a,ll p)
{
    if(p==0) return 1L;
    ll ret=power(a,p/2);
    ret=(ret*ret);
    if(p%2) ret=(ret*a);
    return ret;
}
int fastMax(int x, int y)
{
    return (((y-x)>>(32-1))&(x^y))^y;
}
int fastMin(int x, int y)
{
    return (((y-x)>>(32-1))&(x^y))^x;
}

#define MAX 100100

ll csum[MAX];



int main()
{
    int test,n,Q,l,r,cnt,cas=0;
    ll res,sum;
    double rr;
    cin>>test;
    while(test--)
    {
        cin>>n;
        for(int i=0;i<n;i++)
        {
            SF("%I64d",&csum[i]);
            if(i)
            {
                csum[i]+=csum[i-1];
            }
        }
        cin>>Q;
        PF("Case #%d:\n",++cas);
        while(Q--)
        {
            SF("%d %d",&l,&r);
            res=csum[r];

            if(l) res-=csum[l-1];
            sum=res;
            cnt=(r-l+1);
            rr=(res*1.0)/(cnt*1.0);
            res=(ll) round(rr);
            cout<<"res : "<<res<<endl;
            PF("%I64d\n",abs(sum-(ll)cnt*res));
        }
        puts("");
    }
    return 0;
}















