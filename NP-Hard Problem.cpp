/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int a,b,n,m,from[100010],to[100010],flag[100010];
queue<int>q;
vector<int>adj[100010];

void bfs(int u)
{
    while(!q.empty())
        q.pop();
    q.push(u);
    flag[u]=1;
    a++;
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(int i=0;i<adj[u].size();i++)
        {
            int v=adj[u][i];
            if(flag[v])
                continue;
            if(flag[u]==1)
                flag[v]=2,b++;
            else
                flag[v]=1,a++;
            q.push(v);
        }
    }
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    int i;

    scanf(" %d %d",&n,&m);

    for(i=1;i<=m;i++)
    {
        scanf(" %d %d",&from[i],&to[i]);
        adj[from[i]].push_back(to[i]);
        adj[to[i]].push_back(from[i]);
    }

    for(i=1;i<=n;i++)
    {
        if(flag[i]==0)
        {
            bfs(i);
        }
    }

    for(i=1;i<=m;i++)
    {
        if(flag[from[i]]==flag[to[i]])
        {
            printf("-1\n");
            return 0;
        }
    }

    printf("%d\n",a);
    for(i=1;i<=n;i++)
    {
        if(flag[i]==1)
            printf("%d ",i);
    }
    printf("\n");
    printf("%d\n",b);
    for(i=1;i<=n;i++)
    {
        if(flag[i]==2)
            printf("%d ",i);
    }
    printf("\n");

    return 0;
}






