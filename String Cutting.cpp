#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
int mid,left,right;
};

node arr[1010];
int t,p,n,i,l,res,temp[30],temp2[30],j,ff,tt,mn,mx,alph[10010][27];
char in[10010];

main()
{
    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        for(i=1;i<=n;i++)
        {
            scanf(" %d",&arr[i].mid);

        }
        scanf(" %s",in);
        l=strlen(in);
        mn=1;
        mx=l;
        for(i=1;i<=n;i++)
        {
            arr[i].left=mn;
            arr[i].right=mx;
            for(j=1;j<i;j++)
            {
                if(arr[j].mid<arr[i].mid)
                arr[i].left=max(arr[j].mid+1,arr[i].left);
                if(arr[j].mid>arr[i].mid)
                arr[i].right=min(arr[j].mid,arr[i].right);
            }
        }
        for(i=1;i<=l;i++)
        {
            for(j=0;j<26;j++)
                alph[i][j]=alph[i-1][j];
            alph[i][in[i-1]-'a']++;
        }
        res=0;
        for(i=1;i<=n;i++)
        {
            //printf("%d %d %d\n",arr[i].left,arr[i].mid,arr[i].right);
            ff=arr[i].left-1;
            tt=arr[i].mid;
            for(j=0;j<26;j++)
                temp[j]=alph[tt][j]-alph[ff][j];
            ff=arr[i].mid;
            tt=arr[i].right;
            for(j=0;j<26;j++)
                temp2[j]=alph[tt][j]-alph[ff][j];
            for(j=0;j<26;j++)
            {
                if(temp[j]==0&&temp2[j]>0)
                    res++;
                if(temp[j]>0&&temp2[j]==0)
                    res++;
            }
        }
        printf("%d\n",res);
    }
    return 0;
}

