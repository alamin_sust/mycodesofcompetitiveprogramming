#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

struct node
{
    long long int x,y,val;
};

node adj[1000010];
long long int t,i,j,k,res,flag[10010],par[10010],p,m,n,airport;

long long int comp(node a,node b)
{
    return a.val<b.val;
}

main()
{
    scanf(" %lld",&t);
    for(p=1;p<=t;p++)
    {
        scanf("%lld %lld %lld",&n,&m,&airport);
        for(i=0;i<m;i++)
        {
            scanf(" %lld %lld %lld",&adj[i].x,&adj[i].y,&adj[i].val);
        }
        sort(adj,adj+m,comp);
        for(i=1;i<=n;i++)
            {par[i]=i;
            flag[i]=0;}
        for(res=0,k=0,i=0;i<m;i++)
        {
            int u=adj[i].x;
            int v=adj[i].y;
            while(par[u]!=u)
                u=par[u];
            while(par[v]!=v)
                v=par[v];
            if(u!=v)
            {
                k++;
                par[v]=u;
                res+=adj[i].val;
            }
        }
        res=res+((n-k)*airport);
        printf("Case %lld: %lld %lld\n",p,res,(n-k));
    }
    return 0;
}
