/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int n,l,x,y,i,arr[100010],res;
bool flag,canx,cany;
map<int,bool>mpp;

int main()
{
    cin>>n>>l>>x>>y;

    for(i=0;i<n;i++)
    {
        scanf(" %d",&arr[i]);
        mpp[arr[i]]=true;
    }
    for(i=0;i<n;i++)
    {
        if(mpp[arr[i]+x]||mpp[arr[i]-x])
            canx=true;
        if(mpp[arr[i]+y]||mpp[arr[i]-y])
            cany=true;
    }
    if(canx&&cany)
    {
        printf("0\n");
        return 0;
    }
    if(canx==false&&cany==false)
    {
        flag=false;
        for(i=0;i<n;i++)
        {
            if(arr[i]-x>0)
            {
                if(mpp[arr[i]-x+y]||mpp[arr[i]-x-y])
                {
                    res=arr[i]-x;
                    flag=true;
                    break;
                }
            }
            if(arr[i]+x<l)
            {
                if(mpp[arr[i]+x+y]||mpp[arr[i]+x-y])
                {
                    res=arr[i]+x;
                    flag=true;
                    break;
                }
            }
        }

    if(flag==true)
            printf("1\n%d\n",res);
        else
            printf("2\n%d %d\n",x,y);
    }
    else if(canx==false)
    {
        printf("1\n%d\n",x);
    }
    else
    {
        printf("1\n%d\n",y);
    }
    return 0;
}


