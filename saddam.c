#include<stdio.h>
#include<math.h>
int reverse(int num);
int palindrome(int val);
int main()
{
    int N,count,i;
    unsigned long long val,originVal,sum;
    while(scanf("%d",&N)==1)
    {
        for(i=0; i<N; i++)
        {
            count=1;
            scanf("%llu",&originVal);
            sum=originVal+reverse(originVal);
            while(!palindrome(sum))
            {
                originVal=sum;
                sum=originVal+reverse(originVal);
                count++;
            }
            printf("%d %llu\n",count,sum);
        }
    }
    return 0;
}

int reverse(int num)
{
    int rev=0;
    while(num>0)
    {
        rev=rev*10;
        rev=rev+num%10;
        num=num/10;
    }
    return rev;
}

int palindrome(int val)
{
    if(val==reverse(val))   return 1;
    else    return 0;
}
