/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int cum[200010],arr[200010],i,j,x,n,res,st,en;

int main()
{
    cin>>n>>x;
    for(i=1;i<=n;i++)
    {
        scanf(" %d %d",&st,&en);
        for(j=st;j<=en;j++)
        {
            arr[j]=1;
        }
    }
    for(j=1,i=1;i<=100000;i++)
    {
        cum[i]=cum[i-1]+arr[i];
        if(arr[i])
        {
            arr[i]=j++;
        }
    }
   // for(i=100001;i<=200005;i++)
     //   cum[i]=99999999;
    j=0;
   // printf("%d..\n",arr[100000]);
    for(i=1,res=0;i<=100000;)
    {
        if(arr[i])
        {
            res++;
           // printf("%d\n",i);
            j=arr[i];
            i++;
            continue;
        }
        if(cum[i+x]<=(j+1))
        {
            //printf("%d\n",i);
            i+=x;
        }
        else
        {
            //printf("%d\n",i);
            res++;
            i++;
        }
    }
    printf("%d\n",res);
    return 0;
}

