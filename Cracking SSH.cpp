/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int dp[102][27],path[102][27],arr[102],adj[27][27],l,m;

void pathfind(int pos,int pre)
{
    if(pos==l)
    {cout<<"\n";
        return;}
    printf("%c",path[pos][pre]+'a');
    return pathfind(pos+1,path[pos][pre]);
}

int rec(int pos,int pre)
{
    if(pos==l)
        return 0;
    int &ret=dp[pos][pre];
    if(ret!=-1)
        return ret;
    ret=99999999;
    for(int i=0;i<m;i++)
    {
        int retnow;
        if(pos!=0)
        retnow=abs(arr[pos]-adj[pre][i])+rec(pos+1,i);
        else
        retnow=rec(pos+1,i);
        if(retnow<ret)
        {
            ret=retnow;
            path[pos][pre]=i;
        }
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    while(cin>>l>>m)
    {
        for(int i=1;i<l;i++)
            scanf(" %d",&arr[i]);
        for(int i=0;i<m;i++)
        {
            for(int j=0;j<m;j++)
            {
                scanf(" %d",&adj[i][j]);
            }
        }
        memset(dp,-1,sizeof(dp));
        rec(0,0);
        pathfind(0,0);
    }
    return 0;
}





