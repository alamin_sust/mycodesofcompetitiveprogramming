/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
string name;
int len,rs;
};
node arr[100010];

bool operator<(node a,node b)
{
    if(a.rs==b.rs)
    return a.len>b.len;
    return a.rs>b.rs;
}

priority_queue<node>pq[100010];

int L,R,n,i,j,PP=1,m;
string in1,in2;
map<string,int>mpp;

node det;

main()
{
    scanf(" %d",&n);
    for(i=1;i<=n;i++)
    {
        cin>>arr[i].name;
        arr[i].len=arr[i].name.size();
        arr[i].rs=0;
        for(j=0;j<arr[i].len;j++)
        {
            arr[i].name[j]=tolower(arr[i].name[j]);
            if(arr[i].name[j]=='r')
                arr[i].rs++;
        }
        //printf("%d\n",arr[i].rs);
    }
    scanf(" %d",&m);
    for(i=1;i<=m;i++)
    {
        cin>>in1>>in2;
        det.len=in2.size();
        det.rs=0;
        for(j=0;j<det.len;j++)
        {
            in2[j]=tolower(in2[j]);
            if(in2[j]=='r')
                det.rs++;
        }
        int l=in1.size();
        for(j=0;j<l;j++)
        {
            in1[j]=tolower(in1[j]);
        }
       // printf("%d...\n",det.rs);
        if(mpp[in1]==0)
        {
            mpp[in1]=PP++;
        }
        det.name=in2;
        pq[mpp[in1]].push(det);
        //cout<<in2;
        //printf("%d.........%d....\n",mpp[in1],pq[mpp[in1]].top().len);
    }
    R=0;
    L=0;
    for(i=1;i<=n;i++)
    {
        int RR=arr[i].rs;
        int LL=arr[i].len;
        while(mpp[arr[i].name]>0)
        {
            det=pq[mpp[arr[i].name]].top();
            //printf("%d.........%d\n",det.len,mpp[arr[i].name]);
            //printf("%d %d\n",det.rs,det.len);
            if(det.rs<arr[i].rs)
            {
                   R+=det.rs;
                   L+=det.len;
                   //printf("%d %d\n",det.rs,det.len);
            }
            else if(det.rs==arr[i].rs)
            {
                if(det.len<arr[i].len)
                {
                   R+=det.rs;
                   L+=det.len;
                }
                else
                {
                   R+=arr[i].rs;
                   L+=arr[i].len;
                }
            }
            else
            {
                R+=arr[i].rs;
                L+=arr[i].len;
            }
        }
        else
        {
            R+=arr[i].rs;
            L+=arr[i].len;
        }
    }
    printf("%d %d\n",R,L);
    return 0;
}

