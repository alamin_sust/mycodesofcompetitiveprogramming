/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

map<string,vector< pair<ll,ll> > >mpp;
map<string,pair<double,ll> >mpp2;
ll n,i,qty,j,pr,cnt;
string arr[10010];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);


    while(cin>>n)
    {
        for(i=1LL;i<=n;i++)
        {
            cin>>arr[i];
            scanf(" %lld %lld",&pr,&qty);
            mpp[arr[i]].push_back(make_pair(pr*qty,qty));
        }
        cnt=0;
       sort(arr+1,arr+n+1);
        for(i=1LL;i<=n;i++)
        {
            if( mpp[arr[i]].size()>0LL)
            {
                pr=0LL;
                qty=0LL;
                for(j=0LL;j<mpp[arr[i]].size();j++)
                {
                    pr+=mpp[arr[i]][j].first;
                    qty+=mpp[arr[i]][j].second;
                }
                mpp2[arr[i]].first=((double)pr)/((double)qty);
                mpp2[arr[i]].second=qty;
                mpp[arr[i]].clear();
                cnt++;
            }
        }
        printf("%lld\n",cnt);
        for(i=1;i<=n;)
        {
            cout<<arr[i];
            printf(" %.10lf %lld\n",mpp2[arr[i]].first,mpp2[arr[i]].second);
            i++;
            while(i<=n&&arr[i]==arr[i-1])
            {
               i++;
            }
        }
    }

    return 0;
}





