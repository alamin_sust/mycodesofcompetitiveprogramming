#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include<string.h>
#define inf 99999999
using namespace std;

int dp[53][1010];

int rec(int num,int now,int maximum,vector<int> vc)
{
    if(num==vc.size())
        return now;
    int &ret=dp[num][now];
    if(ret!=-1)
        return ret;
    ret=-2;
    if((vc[num]+now)<=maximum)
        ret=max(ret,rec(num+1,vc[num]+now,maximum,vc));
    if((now-vc[num])>=0)
        ret=max(ret,rec(num+1,now-vc[num],maximum,vc));
    return ret;
}

class ChangingSounds
{
public:
	int maxFinal(vector <int> changeIntervals, int beginLevel, int maxLevel)
	{
	    memset(dp,-1,sizeof(dp));
	    int res=rec(0,beginLevel,maxLevel,changeIntervals);
	    return res==-2?-1:res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	ChangingSounds objectChangingSounds;

	//test case0
	vector <int> param00;
	param00.push_back(5);
	param00.push_back(3);
	param00.push_back(7);
	int param01 = 5;
	int param02 = 10;
	int ret0 = objectChangingSounds.maxFinal(param00,param01,param02);
	int need0 = 10;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(15);
	param10.push_back(2);
	param10.push_back(9);
	param10.push_back(10);
	int param11 = 8;
	int param12 = 20;
	int ret1 = objectChangingSounds.maxFinal(param10,param11,param12);
	int need1 = -1;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(74);
	param20.push_back(39);
	param20.push_back(127);
	param20.push_back(95);
	param20.push_back(63);
	param20.push_back(140);
	param20.push_back(99);
	param20.push_back(96);
	param20.push_back(154);
	param20.push_back(18);
	param20.push_back(137);
	param20.push_back(162);
	param20.push_back(14);
	param20.push_back(88);
	int param21 = 40;
	int param22 = 243;
	int ret2 = objectChangingSounds.maxFinal(param20,param21,param22);
	int need2 = 238;
	assert_eq(2,ret2,need2);

}
