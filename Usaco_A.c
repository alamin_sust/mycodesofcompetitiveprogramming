#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

#define SZ 10003
ll treeMax[4*SZ];
ll treeMin[4*SZ];
ll arr[SZ];
pair<ll,ll>pll;

void initMax(ll node,ll b,ll e)
{
    if(b==e)
    {
        treeMax[node]=arr[b];
        return;
    }
    ll left = node*2;
    ll right = node*2+1;
    ll mid = (b+e)/2;
    initMax(left,b,mid);
    initMax(right,mid+1,e);
    treeMax[node] = max(treeMax[left],treeMax[right]);
}


ll queryMax(ll node,ll b,ll e,ll i,ll j)
{
    if(i>e||j<b)return -9999999999999LL;
    if(b>=i&&e<=j) return treeMax[node];
    ll left = node*2;
    ll right = node*2+1;
    ll mid = (b+e)/2;
    return max(queryMax(left,b,mid,i,j),queryMax(right,mid+1,e,i,j));
}

int main()
{
    ll n,k,i;
    scanf("%I64d %I64d",&n,&k);
    for(i = 1; i<=n; i++)
    {
        scanf(" %I64d",&arr[i]);
    }
    initMax(1,1,n);


    printf(" %I64d",queryMax(1,1,n,i,i+k));

    return 0;
}


