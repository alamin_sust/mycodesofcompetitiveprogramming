#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ull res=1;

ull get_digit(ull a)
{
    ull dg=0;
    while(a!=0)
        a/=10,res*=10,dg++;
    return dg;
}

ull pw(ull in)
{
    ull i,ret=1;
    for(i=0;i<in;i++)
        ret*=10;
    return ret;
}

main()
{
    ull w,m,k,tot_num,up,dig,rem;
    cin>>w>>m>>k;
    dig=get_digit(m);
    rem=res-m;
    up=rem*dig*k;
    tot_num=0;
    while(up<=w)
    {
        tot_num+=rem;
        dig++;
        rem=(pw(dig)-pw(dig-1));
        up+=(dig)*k*rem;
    }
    up-=(dig)*k*rem;
    tot_num+=(w-up)/(dig*k);
    cout<<tot_num;
    return 0;
}

