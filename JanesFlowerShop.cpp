/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

double val,arr[110];
ll n;

double fc(double r)
{
    double ret=0.0,onrr=(1.0+r);
    for(ll i=1LL;i<=n;i++)
    {
        double now=arr[i];
        for(ll j=0LL;j<i;j++)
        {
            now/=onrr;
        }
        if(onrr<0.0&&(i%2LL)==1LL)
        ret-=now;
        else
        ret+=now;
    }
    ret-=val;
    if(ret<0.0)
        ret*=-1.0;
    return ret;
}

int main()
{
    freopen("C-small-attempt2a.in","r",stdin);
    freopen("C-small-attempt2aOut.txt","w",stdout);

    ll t,p,cnt;
    double res,high,low,mid,mp,mm,mn,mx,epps;

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++)
    {
        scanf(" %lld %lf",&n,&val);
        for(ll i=1LL;i<=n;i++)
        {
            scanf(" %lf",&arr[i]);
        }
        res=-1.0;
        mx=1000000000000010.0;
        low=-0.0000000000040;
        high=0.9999999999996;
        epps=0.00000000000002;
        cnt=1000;
        while(cnt--)
        {
            mid=(low+high)/2.0;
            mp=fc(mid+epps);
            mm=fc(mid);
            mn=fc(mid-epps);
            if(mm<mx)
            {
                mx=mm;
                res=mid;
            }
            if(mp<mm)
            {
                low=mid;
            }
            else if(mn<mm)
            {
                high=mid;
            }
            else
            {
                break;
            }
        }
        if(mx>0.000001)
            res=-1.0;
        printf("Case #%lld: %.12lf\n",p,res);
    }

    return 0;
}






