/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


int bb,l,hs,flag,i,need,paint[100010],cum[100010],cum2[100010];
char arr[100010];

int main()
{
    scanf(" %s",&arr);
    l=strlen(arr);
    if(arr[0]=='(')
    cum[0]=1,cum2[0]=0;
    if(arr[0]==')')
    {
        printf("-1\n");
        return 0;
    }
    for(i=1;i<l;i++)
    {
        cum[i]=cum[i-1]+(arr[i]=='('?1:0);
        if(arr[i]==')')
        bb++;
        if(arr[i]=='#')
            hs++;
    }
    need=cum[l-1]-bb;
    if(need<hs)
    {
        printf("-1\n");
        return 0;
    }
    for(i=l-1;i>=0;i--)
    {
        if(flag==0&&arr[i]=='#')
        {
            paint[i]=need-hs+1;
            flag=1;
        }
        else if(arr[i]=='#')
        {
            paint[i]=1;
        }
    }
    cum2[0]=paint[0];
    for(i=1;i<l;i++)
    {
        cum2[i]=paint[i]+cum2[i-1]+(arr[i]==')'?1:0);
    }
    for(i=0;i<l;i++)
    {
        if(cum2[i]>cum[i])
        {
            printf("-1\n");
            return 0;
        }
    }
    for(i=0;i<l;i++)
    {
        if(paint[i]>0)
            printf("%d\n",paint[i]);
    }
    return 0;
}

