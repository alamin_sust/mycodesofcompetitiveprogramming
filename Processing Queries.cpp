/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node{
ll t,d,id;
};

node arr[200010];
queue<node>q;
ll res[200010];

ll comp(node a,node b)
{
    return a.t<b.t;
}

ll n,b,i,now,qs;


int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    while(scanf("%I64d%I64d",&n,&b)!=EOF)
    {
        for(i=0;i<n;i++)
        {
            scanf(" %I64d %I64d",&arr[i].t,&arr[i].d);
            arr[i].id=i;
        }
        sort(arr,arr+n,comp);
        now=0LL;
        while(!q.empty())
            q.pop();
        memset(res,0,sizeof(res));
        qs=0LL;
        arr[n].t=3000000000000000010LL;
        arr[n].d=0LL;
        arr[n].id=n;
        for(i=0;i<=n;i++)
        {
            while(qs>0LL&&max(now,q.front().t)<=arr[i].t)
            {
                now=max(now,q.front().t)+q.front().d;

                qs--;
                res[q.front().id]=now;
                q.pop();
            }
            if(qs<b)
            {
                q.push(arr[i]);
                qs++;
            }
            else
            {
                res[arr[i].id]=-1LL;
            }
        }
        for(i=0;i<n;i++)
        {
            printf("%I64d",res[i]);
            if(i==n-1LL)
                printf("\n");
            else
                printf(" ");
        }
    }

    return 0;
}
