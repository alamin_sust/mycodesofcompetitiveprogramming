/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

map<ll,ll>mpp;
ll p,t,i,pos,j,k,bit,n,arr[100010],side[20],sum;

int main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        mpp.clear();
        scanf(" %lld",&n);
        n=(1<<n);
        for(i=0;i<n;i++)
            {scanf(" %lld",&arr[i]);
            mpp[arr[i]]++;}
        sort(arr,arr+n);
        pos=1;
        //side[1]=arr[1];
        //side[2]=arr[2];
        mpp[0]--;
        //mpp[arr[1]]--;
        //mpp[arr[2]]--;
        for(pos=1,k=0,i=0;i<n;i++)
        {
            if(i==1||((i%2LL)==0&&__builtin_popcount(i)==1))
            {
                //printf("..");
                for(j=pos;;j++)
                {
                    if(mpp[arr[j]]>0)
                    {
                        side[k++]=arr[j];
                        pos=j;
                        //mpp[arr[j]]--;
                        break;
                    }
                }

            }
            for(j=0,sum=0,bit=i;bit>0;j++)
                {
                    if((bit%2))
                        sum+=side[j];
                    bit/=2;
                }
                mpp[sum]--;
        }
        for(i=0;i<k;i++)
        {
            printf("%lld",side[i]);
            if(i==k-1)
                printf("\n");
            else
                printf(" ");
        }
    }
    return 0;
}


