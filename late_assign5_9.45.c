#include<stdio.h>
main()
{
    int first,second;
    float result,arr[10]=
    {
        {1.0},  //US D.
        {0.65},  //B.P.
        {1.4},   //C.D.
        {1.7},   //D.G.
        {5.3},   //F.F.
        {1.5},   //G.M.
        {1570},  //I.L.
        {98},    //J.Y.
        {3.4},   //M.P.
        {1.3}    //S.F.
    };
    char arr2[10][20]=
    {
        {"US Dollar(s)"},
        {"British Pound(s)"},
        {"Canadian Dollar(s)"},
        {"Dutch Guilders(s)"},
        {"French Franc(s)"},
        {"German mark(s)"},
        {"Italian Lira(s)"},
        {"Japanese Yen(s)"},
        {"Mexican Peso(s)"},
        {"Swiss Franc(s)"}

    };
    printf("1.US Dollar\n");
    printf("2.British Pound\n");
    printf("3.Canadian Dollar\n");
    printf("4.Dutch Guilder\n");
    printf("5.French Franc\n");
    printf("6.German mark\n");
    printf("7.Italian Lira\n");
    printf("8.Japanese Yen\n");
    printf("9.Mexican Peso\n");
    printf("10.Swiss Franc\n");
    printf("enter two currencies.\nenter 0 to terminate\n");
    for(;;)
    {
        scanf(" %d %d",&first,&second);
        if(first==0||second==0)
            break;
        result=arr[second]/arr[first];
        printf("One %s = %.2f %s\n",arr2[first],result,arr2[second]);
    }
}
