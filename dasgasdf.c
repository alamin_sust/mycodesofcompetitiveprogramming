#include<stdio.h>
#include<string.h>
#define infinity 100010
int adj[1010][1010],i,j,k,t,p,e,from,to,n,cost;
main()
{
    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %d %d",&n,&e);
        memset(adj,100010,sizeof(adj));
        for(i=0;i<e;i++)
        {
            scanf(" %d %d %d",&from,&to,&cost);
            if(adj[from][to]!=infinity)
            {
                if(adj[from][to]>cost)
                    {adj[from][to]=cost;
                    adj[to][from]=cost;}
            }
            else
            {
            adj[from][to]=cost;
            adj[to][from]=cost;
            }
        }
        for(k=0;k<n;k++)
        {
            for(i=0;i<n;i++)
            {
                for(j=0;j<n;j++)
                {
                    if(adj[i][j]>adj[i][k]&&adj[i][j]>adj[k][j])
                        {
                            if(adj[i][k]>adj[k][j])
                            adj[i][j]=adj[i][k];
                            else
                            adj[i][j]=adj[k][j];
                        }
                }
            }
        }
        scanf(" %d",&from);
        printf("Case %d:\n",p);
        for(i=0;i<n;i++)
        {
            if(from==i)
                printf("0\n");
            else if(adj[from][i]==infinity)
                printf("Impossible\n");
            else
                printf("%d\n",adj[from][i]);
        }
    }
    return 0;
}

