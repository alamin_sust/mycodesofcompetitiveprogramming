/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll MOD=33554431LL,powpow2[110],pow2[100010],cumpow2[100010];

void setpowpow2(void)
{
    powpow2[0]=2LL;
    for(ll i=1LL;i<=102LL;i++)
    {
        powpow2[i]=(powpow2[i-1]*powpow2[i-1])%MOD;
    }
    return;
}

ll getpow2(ll v)
{
    ll ret=1LL,cnt=0LL;
    while(v)
    {
        if(v%2LL)
        ret=(ret*powpow2[cnt])%MOD;
        v/=2LL;
        cnt++;
    }
    return ret;
}


ll rec(ll n)
{
    if(n<=100000LL)
        return cumpow2[n];
    return (rec(n/2LL)*( (getpow2(n/2LL)*rec((n/2LL)+(n%2LL)))%MOD)  )%MOD;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    ll i,t,p,n;
    setpowpow2();
    for(i=0LL; i<=100002LL; i++)
    {
        pow2[i]=getpow2(i);
    }
    cumpow2[0]=pow2[0];
    for(i=1LL; i<=100002LL; i++)
    {
        cumpow2[i]=(cumpow2[i-1]+pow2[i])%MOD;
    }


    scanf(" %lld",&t);

    for(p=1LL; p<=t; p++)
    {
        scanf(" %lld",&n);
        printf("Case %lld: %lld\n",p,rec(n));
    }

    return 0;
}






