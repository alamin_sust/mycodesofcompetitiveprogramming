#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,yy1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-yy1)*(y2-yy1)) )
using namespace std;
//long long int rr[]={1,2,-1,-2,1,2,-1,-2};
//long long int rr[]={2,1,2,1,-2,-1,-2,-1};

long long int x[10],y[10],res,flag,x1[110],yy1[110],x2[110],y2[110],p,t,n,i,j;

long long int direction(long long int p1,long long int p2,long long int p3)
{
    return ((x[p2]-x[p1])*(y[p3]-y[p1]))-((x[p3]-x[p1])*(y[p2]-y[p1]));
}

bool on_segment(long long int p1,long long int p2,long long int p3)
{
    if((min(x[p1],x[p2])<=x[p3]&&max(x[p1],x[p2])>=x[p3])&&(min(y[p1],y[p2])<=y[p3]&&max(y[p1],y[p2])>=y[p3]))
        return true;
    else return false;
}

bool segment_intersect(long long int p1,long long int p2,long long int p3,long long int p4)
{
    long long int d1=direction(p3,p4,p1);
    long long int d2=direction(p3,p4,p2);
    long long int d3=direction(p1,p2,p3);
    long long int d4=direction(p1,p2,p4);
    // prlong long intf("%d  %d  %d  %d\n",d1,d2,d3,d4);
    if(((d1>0&&d2<0)||(d1<0&&d2>0))&&((d3>0&&d4<0)||(d3<0&&d4>0)))
        return true;
    else if(d1==0&&on_segment(p3,p4,p1))
        return true;
    else if(d2==0&&on_segment(p3,p4,p2))
        return true;
    else if(d3==0&&on_segment(p1,p2,p3))
        return true;
    else if(d4==0&&on_segment(p1,p2,p4))
        return true;
    else
        return false;
}

main()
{
    scanf(" %lld",&t);
    for(p=1;p<=t;p++)
    {
        res=0;
        scanf(" %lld",&n);
        for(i=1;i<=n;i++)
        scanf(" %lld %lld %lld %lld",&x1[i],&yy1[i],&x2[i],&y2[i]);
        for(i=1;i<=n;i++)
        {
            flag=1;
            for(j=1;j<=n;j++)
            {
                if(i!=j)
                {
                    x[1]=x1[i];
                    y[1]=yy1[i];
                    x[2]=x2[i];
                    y[2]=y2[i];
                    x[3]=x1[j];
                    y[3]=yy1[j];
                    x[4]=x2[j];
                    y[4]=y2[j];
                    if(segment_intersect(1,2,3,4))
                        {flag=0;
                        break;}
                }
            }
            res+=flag;
        }
        printf("%lld\n",res);
    }
    return 0;
}
