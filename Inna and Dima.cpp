#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll rr[]={0,0,1,-1};
ll cc[]={1,-1,0,0};

struct node{
ll x,y;
};
node det,to;
ll res,flag,val[1010][1010],dir[1010][1010],status[1010][1010],r,c;
char arr[1010][1010];

void bfs(ll i,ll j,ll k)
{
    det.x=i;
    det.y=j;
    queue<node>q;
    q.push(det);
    char ch;
    while(!q.empty())
    {
        det=q.front();
        val[det.x][det.y]=-1;
        q.pop();
        if(arr[det.x][det.y]=='D')
            ch='I';
        else if(arr[det.x][det.y]=='I')
            ch='M';
        else if(arr[det.x][det.y]=='M')
            ch='A';
        else ch='D';
        for(i=0;i<4;i++)
        {
            to.x=det.x+rr[i];
            to.y=det.y+cc[i];
            if(arr[to.x][to.y]==ch&&val[to.x][to.y]!=1)
            {
                q.push(to);
                if(ch=='A')
                    {
                        val[to.x][to.y]=++k;
                        res=max(res,k);
                    }
                else
                    val[to.x][to.y]=-1;
            }
        }
    }
    return;
}

queue<node>chk;
ll now=1;

void chk_inf(ll i,ll j)
{
    ll k=1000010;
    det.x=i;
    det.y=j;
    chk.push(det);
    status[det.x][det.y]=now;
    char ch;
    while((!chk.empty())&&k>0)
    {
        k--;
        det=chk.front();

        chk.pop();
        if(arr[det.x][det.y]=='D')
            ch='I';
        else if(arr[det.x][det.y]=='I')
            ch='M';
        else if(arr[det.x][det.y]=='M')
            ch='A';
        else ch='D';
        for(i=0;i<4;i++)
        {
            to.x=det.x+rr[i];
            to.y=det.y+cc[i];
            if(arr[to.x][to.y]==ch&&status[to.x][to.y]==now)
                {flag=1;
                printf("%lld..%lld %lld\n",now,det.x,det.y);
                return;}
            if(arr[to.x][to.y]==ch&&status[to.x][to.y]==0)
            {
                chk.push(to);
                    status[to.x][to.y]=now;
            }
        }
    }
    if(k==0)
        flag=1;
    return;
}

main()
{
    ll i,j;
    cin>>r>>c;
    getchar();
    for(i=1;i<=r;i++)
    {
        for(j=1;j<=c;j++)
        {
            scanf("%c",&arr[i][j]);
        }
        getchar();
    }
    for(i=1;i<=r;i++)
    {
        for(j=1;j<=c;j++)
        {
            if(arr[i][j]=='D'&&val[i][j]==0)
            {
                ll tp=max(max(val[i-1][j],val[i][j-1]),max(val[i+1][j],val[i][j+1]));
                if(tp<0)
                    tp=0;
                bfs(i,j,tp);
            }
        }
    }
    for(i=1;i<=r&&flag==0;i++)
    {
        for(j=1;j<=c;j++)
        {
            if(arr[i][j]=='D'&&status[i][j]==0)
            {
                now++;
                chk_inf(i,j);
                if(flag==1)
                    break;
            }
        }
    }
    if(flag==1)
        printf("Poor Inna!\n");
    else if(res==0)
        printf("Poor Dima!\n");
    else
        printf("%I64d\n",res);
    return 0;
}

