#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#define PI 3.141592653589793

main()
{
    double k1,k2,x1,y1,x2,y2,x3,y3,rad,cir1,g,f,centx,centy,k;
    while(scanf(" %lf %lf %lf %lf %lf %lf",&x1,&y1,&x2,&y2,&x3,&y3)==6)
    {
        k1=(x3-x1)*(x3-x2)+(y3-y1)*(y3-y2);
        k2=(x3-x1)*(y1-y2)+(y3-y1)*(x1-x2);
        if(k2!=0.0)
        {
            k=k1/k2;
            g=0.50000*(k*(y2-y1)-(x1+x2));
            f=0.50000*(k*(x1-x2)-(y1+y2));
            centx=-g;
            centy=-f;
            rad=sqrt(((centx-x1)*(centx-x1))+((centy-y1)*(centy-y1)));
            cir1=2.0*rad*PI;
            printf("%.2lf\n", cir1);
        }
        else printf("0.00\n");
    }
    return 0;
}
