/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int player[3][15],K,triangle[3],triple[3],arr[28];
map<int,int>mpp;

int rec(int mask)
{
    if(mpp[mask]!=0)
        return mpp[mask];
    int ret=0;
    for(int i=0; i<K; i++)
    {
        if((1<<i)&mask)
        {
            for(int j=i+1; j<K; j++)
            {
                if((1<<j)&mask)
                {
                    for(int k=j+1; k<K; k++)
                    {
                        if((1<<k)&mask)
                        {
                           if(arr[i]+arr[j]>arr[k])
                           ret=max(ret,1+rec(mask^((1<<i)|(1<<j)|(1<<k))));
                        }
                    }
                }
            }
            ret=max(ret,rec(mask^(1<<i)));
            break;
        }
    }
    return mpp[mask]=ret;
}


int main()
{
    int i,n,j,in;
    while(cin>>n&&n)
    {
        memset(player,0,sizeof(player));
        for(i=0; i<n; i++)
        {
            scanf(" %d",&in);
            player[i%2+1][in]++;
        }
        for(i=1; i<=2; i++)
        {
            triple[i]=0;
            for(j=1; j<=13; j++)
            {
                triple[i]+=player[i][j]/3;
                player[i][j]%=3;
            }
        }
        if(triple[1]>triple[2])
        {
            printf("1\n");
            continue;
        }
        if(triple[2]>triple[1])
        {
            printf("2\n");
            continue;
        }
        for(i=1; i<=2; i++)
        {
            for(K=0,j=1; j<=13; j++)
            {
                while(player[i][j]>0)
                    arr[K++]=j,player[i][j]--;
            }
            mpp.clear();
            triangle[i]=rec((1<<K)-1);
        }
        if(triangle[1]>triangle[2])
            printf("1\n");
        else if(triangle[2]>triangle[1])
            printf("2\n");
        else
            printf("0\n");
    }
    return 0;
}

