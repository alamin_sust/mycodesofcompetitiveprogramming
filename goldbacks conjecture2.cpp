#include <iostream>
#include <cstdio>
#include <cmath>
using namespace std;
long long pr[1000080],T;
bool isp[1000080];
void prime()
{
    long long i,j;
    T=0;
    for(i=2; i<1000000; i++)
    {
        if(isp[i]==0)
        {
            for(j=i*i; j<1000000; j=j+i)
            {
                isp[j]=1;
            }
            pr[T]=i;
            T++;
        }
    }
}
int main()
{
    prime();
    long long a,s,d,f,g,h,k,l;
    while(scanf("%lld",&a)==1)
    {
        if(a==0) break;
        else
        {
            s=1;
            for(d=T-1; d>=0; d--)
            {
                if(pr[d]>=a) continue;
                for(f=0; f<d; f++)
                {
                    if(a==pr[d]+pr[f])
                    {
                        s=0;
                        printf("%lld = %lld + %lld\n",a,pr[f],pr[d]);
                        break;
                    }
                }
                if(s==0) break;
            }
            if(s==1)
            {
                printf("Goldbach's conjecture is wrong.\n");
            }
        }
    }
    return 0;
}
