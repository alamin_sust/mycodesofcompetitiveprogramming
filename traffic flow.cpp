#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};
struct node
{
    int x,y,val;
};

bool operator<(node a,node b)
{
    return a.val<b.val;
}

priority_queue<node>adj;
node temp;

main()
{
    int t,p,i,res,n,m,par[110];
    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %d %d",&n,&m);
        for(i=0;i<n;i++)
            par[i]=i;
        for(i=1;i<=m;i++)
        {
                scanf(" %d %d %d",&temp.x,&temp.y,&temp.val);
                adj.push(temp);
        }
        res=inf;
        i=1;
        while(!adj.empty())
        {
            if(i>=n)
                {adj.pop();
                continue;}
            int u=adj.top().x;
            int v=adj.top().y;
            int cost=adj.top().val;
            adj.pop();
            while(par[u]!=u)
                u=par[u];
            while(par[v]!=v)
                v=par[v];
            if(u!=v)
            {
                i++;
                par[u]=v;
                res=min(res,cost);
            }
        }
        printf("Case #%d: %d\n",p,res);
    }
    return 0;
}

