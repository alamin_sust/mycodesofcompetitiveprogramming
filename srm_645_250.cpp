#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class BacteriesColony
{
public:
	vector <int> performTheExperiment(vector <int> colonies)
	{
	    int prev,flag=1;
	    while(flag)
        {
            flag=0;
            prev=colonies[0];
            for(int i=1;i<colonies.size()-1;i++)
            {
                if(colonies[i]<prev&&colonies[i]<colonies[i+1])
                    {
                    flag=1;
                    prev=colonies[i];
                    colonies[i]++;
                    }
                else if(colonies[i]>prev&&colonies[i]>colonies[i+1])
                    {
                    flag=1;
                    prev=colonies[i];
                    colonies[i]--;
                    }
                else
                    prev=colonies[i];
            }
        }
        return colonies;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	BacteriesColony objectBacteriesColony;

	//test case0
	vector <int> param00;
	param00.push_back(5);
	param00.push_back(3);
	param00.push_back(4);
	param00.push_back(6);
	param00.push_back(1);
	vector <int> ret0 = objectBacteriesColony.performTheExperiment(param00);
	vector <int> need0;
	need0.push_back(5);
	need0.push_back(4);
	need0.push_back(4);
	need0.push_back(4);
	need0.push_back(1);
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(1);
	param10.push_back(5);
	param10.push_back(4);
	param10.push_back(9);
	vector <int> ret1 = objectBacteriesColony.performTheExperiment(param10);
	vector <int> need1;
	need1.push_back(1);
	need1.push_back(4);
	need1.push_back(5);
	need1.push_back(9);
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(78);
	param20.push_back(34);
	param20.push_back(3);
	param20.push_back(54);
	param20.push_back(44);
	param20.push_back(99);
	vector <int> ret2 = objectBacteriesColony.performTheExperiment(param20);
	vector <int> need2;
	need2.push_back(78);
	need2.push_back(34);
	need2.push_back(34);
	need2.push_back(49);
	need2.push_back(49);
	need2.push_back(99);
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(32);
	param30.push_back(68);
	param30.push_back(50);
	param30.push_back(89);
	param30.push_back(34);
	param30.push_back(56);
	param30.push_back(47);
	param30.push_back(30);
	param30.push_back(82);
	param30.push_back(7);
	param30.push_back(21);
	param30.push_back(16);
	param30.push_back(82);
	param30.push_back(24);
	param30.push_back(91);
	vector <int> ret3 = objectBacteriesColony.performTheExperiment(param30);
	vector <int> need3;
	need3.push_back(32);
	need3.push_back(59);
	need3.push_back(59);
	need3.push_back(59);
	need3.push_back(47);
	need3.push_back(47);
	need3.push_back(47);
	need3.push_back(47);
	need3.push_back(47);
	need3.push_back(18);
	need3.push_back(18);
	need3.push_back(19);
	need3.push_back(53);
	need3.push_back(53);
	need3.push_back(91);
	assert_eq(3,ret3,need3);

}
