/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll dp[17][1<<17][2],n,k;

ll rec(ll pos,ll mask,ll flag)
{
    //printf("%lld %lld %lld\n",pos,mask,flag);
    if(pos==n)
        return flag;
    ll &ret=dp[pos][mask][flag];
    if(ret!=-1)
        return ret;
    ret=0;
    for(ll i=0;i<n;i++)
    {
        if(((1<<i)&mask)==0)
        ret+=rec(pos+1,(1<<i)|mask,pos>=k?flag:(i==pos?0:(1&flag)));
    }
    return ret;
}

int main()
{
    ll p,t,K,i,j,cs;
    scanf(" %lld",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %lld %lld %lld",&cs,&n,&k);
        K=(1<<n);
        for(i=0;i<n;i++)
            for(j=0;j<K;j++)
            dp[i][j][0]=dp[i][j][1]=-1;
        //memset(dp,-1,sizeof(dp));
        printf("%lld %lld\n",cs,rec(0,0,1));
    }

    return 0;
}


