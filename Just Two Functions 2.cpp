#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

int mat[35][8][8],res[8][8],m;

void big_mod(int x)
{
    for(int i=1;i<=6;i++)
    {
        for(int j=1;j<=6;j++)
        {
          //  mat[x][i][j]=0;
            for(int k=1;k<=6;k++)
                mat[x][i][j]+=mat[x-1][i][k]*mat[x-1][k][j];
            mat[x][i][j]%=m;
           // printf("%d ",mat[x-1][i][j]);
        }
        //printf("\n");
    }
    return;
}

void mat_mul(int x)
{
    //printf("%d..\n",x);
    int temp[8][8];
    for(int i=1; i<=6; i++)
    {
        for(int j=1; j<=6; j++)
        {
            temp[i][j]=res[i][j];
        }
    }
    for(int i=1; i<=6; i++)
    {
        for(int j=1; j<=6; j++)
        {
            res[i][j]=0;
            for(int k=1; k<=6; k++)
                res[i][j]+=(temp[i][k]*mat[x][k][j]);
            res[i][j]%=m;
        }
    }
    return;
}

main()
{
    int t,p,a[4],b[4],i,j,ff,n,k,gg,c[4],f[4],g[4],q;
    cin>>t;
    for(p=1; p<=t; p++)
    {
        cin>>a[1]>>b[1]>>c[1];
        cin>>a[2]>>b[2]>>c[2];
        cin>>f[0]>>f[1]>>f[2];
        cin>>g[0]>>g[1]>>g[2];
        cin>>m;
        memset(mat,0,sizeof(mat));
        mat[0][1][1]=a[1];
        mat[0][1][2]=b[1];
        mat[0][1][6]=c[1];
        mat[0][4][3]=c[2];
        mat[0][4][4]=a[2];
        mat[0][4][5]=b[2];
        mat[0][2][1]=1;
        mat[0][3][2]=1;
        mat[0][5][4]=1;
        mat[0][6][5]=1;
        for(i=1;i<=32;i++)
        big_mod(i);
        cin>>q;
        printf("Case %d:\n",p);
        for(i=1;i<=q;i++)
        {
            cin>>n;
            if(n<3)
            {
                ff=f[n]%m;
                gg=g[n]%m;
            }
            else
            {
                n-=2;
                memset(res,0,sizeof(res));
                for(j=1;j<=6;j++)
                    res[j][j]=1;
                k=0;
                while((n>>k)|0!=0)
                {
                    if((n>>k)&1==1)
                    {
                        mat_mul(k);
                    }
                    k++;
                }
                ff=(res[1][1]*f[2])+(res[1][2]*f[1])+(res[1][3]*f[0])+(res[1][4]*g[2])+(res[1][5]*g[1])+(res[1][6]*g[0]);
                gg=(res[4][1]*f[2])+(res[4][2]*f[1])+(res[4][3]*f[0])+(res[4][4]*g[2])+(res[4][5]*g[1])+(res[4][6]*g[0]);
                //gg=(res[4][3]*f[0])+(res[4][4]*g[2])+(res[4][5]*g[1]);
                ff%=m;
                gg%=m;
            }
            printf("%d %d\n",ff,gg);
        }
    }
    return 0;
}
