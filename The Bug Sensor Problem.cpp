#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) ( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
ll x,y,val;
};

node det;

bool operator<(node a,node b)
{
    return a.val>b.val;
}

priority_queue<node>pq;
ll t,p,n,k,i,j,a,b,c,x[100010],y[100010],par[100010];
main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>k;
        n=0;
        while(scanf("%lld",&a)&&a!=-1)
        {
            x[++n]=a;
            scanf(" %lld",&y[n]);
        }
        for(i=1;i<=n;i++)
        {
            par[i]=i;
            for(j=i+1;j<=n;j++)
            {
                det.x=i;
                det.y=j;
                det.val=dist(x[i],y[i],x[j],y[j]);
                pq.push(det);
                swap(det.x,det.y);
                pq.push(det);
            }
        }
        if(n==k)
        {
            printf("0\n");
            while(!pq.empty())
                pq.pop();
            continue;
        }
        while(!pq.empty())
        {
            a=pq.top().x;
            b=pq.top().y;
            c=pq.top().val;
            pq.pop();
            while(par[a]!=a)
                a=par[a];
            while(par[b]!=b)
                b=par[b];
            if(a!=b)
            {
                n--;
                par[a]=b;
            }
            if(n==k)
            {
                printf("%.0lf\n",ceil(sqrt((double)c)));
                break;
            }
        }
    }
    return 0;
}
