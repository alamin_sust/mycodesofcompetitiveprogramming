/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

map<ll,ll>mpp;
ll p,t,k,n,f,i,arr[1000010],res,j,K;

int main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        mpp.clear();
        cin>>n>>f;
        if(n==1)
            {printf("0\n");
            continue;}
        for(i=1;i<=n;i++)
        {
            scanf(" %lld",&arr[i]);
            mpp[arr[i]]++;
        }
        sort(arr+1,arr+n+1);
        res=0;
        for(i=1;i<=n;i++)
        {
            k=1;
            for(j=i+1;j<=n;j++)
            {
                if(arr[j]!=arr[i])
                {
                    break;}
                k++;
            }
            i=j-1;
            res+=k*(k-1)/2;
            K=arr[i]*f;
            while(K<=10000000000LL)
            {
                res+=mpp[K]*k;
                K*=f;
            }
        }
        printf("%lld\n",res);
    }
    return 0;
}


