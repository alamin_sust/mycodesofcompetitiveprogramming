#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
int x,y,val,id;
};

node det,data[10010];
int t,p,i,a,b,c,res1,res2,k,mx,n,m,par[1010],nn;

bool operator<(node a,node b)
{
    return a.val>b.val;
}

priority_queue<node>pq;
queue<int>forbidden;

main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n>>m;
        for(i=1;i<=n;i++)
            par[i]=i;
        for(i=1;i<=m;i++)
        {
            scanf(" %d %d %d",&det.x,&det.y,&det.val);
            det.id=i;
            pq.push(det);
        }
        res1=0;
        k=0;
        while(!pq.empty())
        {
            det=pq.top();
            data[++k].x=a=det.x;
            data[k].y=b=det.y;
            data[k].val=det.val;
            data[k].id=det.id;
            pq.pop();
            while(a!=par[a])
                a=par[a];
            while(b!=par[b])
                b=par[b];
            if(a!=b)
                par[a]=b,forbidden.push(det.id),res1+=det.val;
        }
        res2=99999999;
        while(!forbidden.empty())
        {
        mx=0;
        nn=1;
        for(i=1;i<=n;i++)
            par[i]=i;
        c=forbidden.front();
        forbidden.pop();
        for(i=1;i<=k;i++)
        {
            if(data[i].id==c)
                continue;
            a=data[i].x;
            b=data[i].y;
            while(a!=par[a])
                a=par[a];
            while(b!=par[b])
                b=par[b];
            if(a!=b)
                par[a]=b,mx+=data[i].val,nn++;
        }
        if(nn==n)
            res2=min(res2,mx);
        }
        printf("%d %d\n",res1,res2);
    }
    return 0;
}

