/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node
{
    int x,y,degree;
};

bool operator<(node a,node b)
{
    return a.degree>b.degree;
}

node det,adj[2010][2010];
char arr[2010][2010];
int res,m,n;
priority_queue<node>pq;

void fc(int i,int j)
{
    if(j>0)
    {
        adj[i][j-1].degree--;
        if(adj[i][j-1].degree==1)
            pq.push(adj[i][j-1]);
    }
    if(i>0)
    {
        adj[i-1][j].degree--;
        if(adj[i-1][j].degree==1)
            pq.push(adj[i-1][j]);
    }
    if(j<(m-1))
    {
        adj[i][j+1].degree--;
        if(adj[i][j+1].degree==1)
            pq.push(adj[i][j+1]);
    }
    if(i<(n-1))
    {
        adj[i+1][j].degree--;
        if(adj[i+1][j].degree==1)
            pq.push(adj[i+1][j]);
    }
    return;
}

void relax(void)
{
    while(!pq.empty())
    {
        int i=pq.top().x;
        int j=pq.top().y;
        //printf("%d %d..\n",i,j);
        if(j>0&&arr[i][j-1]=='.'&&arr[i][j]=='.')
        {
            arr[i][j-1]='<';
            arr[i][j]='>';
            adj[i][j].degree=0;
            adj[i][j-1].degree=0;
            fc(i,j-1);
            fc(i,j);

            res++;
        }
        else if(i>0&&arr[i-1][j]=='.'&&arr[i][j]=='.')
        {
            res++;
            arr[i-1][j]='^';
            arr[i][j]='v';
            adj[i][j].degree=0;
            adj[i-1][j].degree=0;
            fc(i,j);
            fc(i-1,j);
        }
        else if(j<(m-1)&&arr[i][j+1]=='.'&&arr[i][j]=='.')
        {
            res++;
            arr[i][j+1]='>';
            arr[i][j]='<';
            adj[i][j].degree=0;
            adj[i][j+1].degree=0;
            fc(i,j+1);
            fc(i,j);
        }
        else if(i<(n-1)&&arr[i+1][j]=='.'&&arr[i][j]=='.')
        {
            res++;
            arr[i+1][j]='v';
            arr[i][j]='^';
            adj[i][j].degree=0;
            adj[i+1][j].degree=0;
            fc(i+1,j);
            fc(i,j);
        }
        pq.pop();
    }
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    int i,j,place;
    cin>>n>>m;
    for(i=0; i<n; i++)
        scanf(" %s",arr[i]);
    place=0;
    res=0;
 //   for(int p=0; p<4; p++)
  //  {
        for(i=0; i<n; i++)
        {
            for(j=0; j<m; j++)
            {
                if(arr[i][j]=='.')
                {

                    place++;
                    adj[i][j].degree=0;
                    adj[i][j].x=i;
                    adj[i][j].y=j;
                    if(j>0&&arr[i][j-1]=='.')
                    {
                        adj[i][j].degree++;
                    }
                    if(i>0&&arr[i-1][j]=='.')
                    {
                        adj[i][j].degree++;
                    }
                    if(j<(m-1)&&arr[i][j+1]=='.')
                    {
                        adj[i][j].degree++;
                    }
                    if(i<(n-1)&&arr[i+1][j]=='.')
                    {
                        adj[i][j].degree++;
                    }
                }
                else
                    adj[i][j].degree=0;
            }
        }
        for(i=0; i<n; i++)
        {
            for(j=0; j<m; j++)
            {
                if(adj[i][j].degree==1)
                {
    //                printf("%d %d---\n",i,j);
                    pq.push(adj[i][j]);
                }
            }
        }
        //printf("\n");
        relax();


    if((res*2)==place)
    {
        for(i=0; i<n; i++)
        {
            for(j=0; j<m; j++)
                printf("%c",arr[i][j]);
            printf("\n");
        }
    }
    else
        printf("Not unique\n");
    //printf("%d %d\n",res,place);

    return 0;
}





