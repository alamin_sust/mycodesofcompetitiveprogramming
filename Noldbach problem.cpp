#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
#define MAX 1010
int i, j, sieve[MAX], primecount=0, prime[MAX], in,n,k;
main()
{
    for(i=0; i<MAX; i++)
    {
        sieve[i]=1;
    }
    sieve[0]=sieve[1]=0;
    for(i=2; i<MAX; i++)
    {
        while(sieve[i]==0 && i<MAX)
        {
            i++;
        }
        prime[primecount]=i;
        for(j=i*i; j<MAX; j+=i)
        {
            sieve[j]=0;
        }
        primecount++;
    }
    sieve[prime[0]]=sieve[prime[1]]=0;
    for(i=2;i<primecount;i++)
    {
        int flag=0;
        for(j=i;j>=2;j--)
        if(prime[i]==1+prime[j-1]+prime[j-2])
        {
            flag=1;
            break;
        }
        if(flag==0)
            sieve[prime[i]]=0;
    }
    cin>>n>>k;
    int res=0;
    for(i=1;i<=n;i++)
    {
        if(sieve[i]==1)
            res++;
    }
    if(res>=k)
        printf("YES");
    else
        printf("NO");
    return 0;
}

