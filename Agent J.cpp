#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2.0*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int n,i;
double aa,bb,cc,a,b,c,A,B,C,circle,triangle,s;

main()
{
    cin>>n;
    for(i=1;i<=n;i++)
    {
        circle=0;
        cin>>aa>>bb>>cc;
        c=aa+bb;
        a=bb+cc;
        b=cc+aa;
        A=acos((b*b+c*c-a*a)/(2.0*b*c));
        B=acos((a*a+c*c-b*b)/(2.0*a*c));
        C=acos((b*b+a*a-c*c)/(2.0*a*b));
        circle+=A/(2*pi)*pi*aa*aa;
        circle+=B/(2*pi)*pi*bb*bb;
        circle+=C/(2*pi)*pi*cc*cc;
        s=(a+b+c)/2.0;
        triangle=sqrt(s*(s-a)*(s-b)*(s-c));
        printf("Case %d: %.6lf\n",i,triangle-circle);
    }
    return 0;
}

