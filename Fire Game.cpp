/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
int rr[]={0,0,-1,1};
int cc[]={-1,1,0,0};

struct node{
int x,y;
};

queue<node>q;
node u,v;

int grass1,val[12][12],cnt=0,flag[12][12];
char arr[12][12];

int func(int x,int y)
{
    int ret=0;
    u.x=x;
    u.y=y;
    flag[x][y]=cnt+1;
    if(val[x][y]==-1)
    grass1++;
    val[x][y]=0;
    q.push(u);
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(int i=0;i<4;i++)
        {
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            if(flag[v.x][v.y]!=(cnt+1)&&arr[v.x][v.y]=='#')
            {
                q.push(v);
                flag[v.x][v.y]=cnt+1;
                if(val[v.x][v.y]==-1)
                    {grass1++;
                    val[v.x][v.y]=val[u.x][u.y]+1;}
                else
                    val[v.x][v.y]=min(val[v.x][v.y],val[u.x][u.y]+1);
                ret=max(ret,val[v.x][v.y]);
            }
        }
    }
    return ret;
}

int getval(int n,int m)
{
    int ret=0;
    for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=m;j++)
        {
            if(val[i][j]==-1)
                continue;
            ret=max(ret,val[i][j]);
        }
    }
    return ret;
}

int main()
{
    int p,t,grass,n,m,mn,res1,res2;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n>>m;
        getchar();
        grass=0;
        for(int i=1;i<=n;i++)
        {
            for(int j=1;j<=m;j++)
            {
                scanf("%c",&arr[i][j]);
                if(arr[i][j]=='#')
                    grass++;
            }
            getchar();
        }
       // printf("....");
        mn=99999999;
        for(int i=1;i<=n;i++)
        {
            for(int j=1;j<=m;j++)
            {
                for(int k=i;k<=n;k++)
                {
                    for(int l=1;l<=m;l++)
                    {
                        if(arr[i][j]==arr[k][l]&&arr[i][j]=='#')
                        {
                            memset(val,-1,sizeof(val));
                            //memset(flag,0,sizeof(flag));
                            grass1=0;
                            cnt++;
                            res1=func(i,j);
                            cnt++;
                            //memset(flag,0,sizeof(flag));
                            res2=func(k,l);
                            if(grass1==grass)
                            mn=min(mn,getval(n,m));
                        }
                    }
                }
            }
        }
        for(int i=1;i<=n;i++)
          for(int j=1;j<=m;j++)
            arr[i][j]='\0';

        printf("Case %d: %d\n",p,mn==99999999?-1:mn);
    }
    return 0;
}
/*
4
3 3
.#.
###
.#.
3 3
.#.
#.#
.#.
3 3
...
#.#
...
3 3
###
..#
#.#
*/

