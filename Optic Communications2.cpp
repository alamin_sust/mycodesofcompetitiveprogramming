/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

queue<int>q;
vector<string>arr[27];
char msg[110],com[110],from[110],to[110];
int stat[27],adj[27][27];

void bfs(int u)
{
    q.push(u);
    memset(stat,0,sizeof(stat));
    stat[u]=1;
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(int i=0;i<26;i++)
        {
            if(stat[i]==0&&adj[u][i]==1)
            {
                arr[i].push_back(msg);
                stat[i]=1;
                q.push(i);
            }
        }
    }
    return;
}

int main()
{
    int t,p,i,m,j;
    cin>>t;
    for(p=1;p<=t;p++)
    {

        memset(adj,0,sizeof(adj));
        for(i=0;i<26;i++)
            arr[i].clear();
        scanf(" %d",&m);
        for(i=1;i<=m;i++)
        {
            scanf(" %s",com);
            if(com[0]=='V')
            {
                scanf(" %s %s",from,to);
                adj[from[0]-'A'][to[0]-'A']=1;
                adj[to[0]-'A'][from[0]-'A']=1;
            }
            else if(com[0]=='W'||com[0]=='O')
            {
                scanf(" %s %s",from,to);
                adj[from[0]-'A'][to[0]-'A']=0;
                adj[to[0]-'A'][from[0]-'A']=0;
            }
            else
            {
                scanf(" %s",from);
                getchar();
                gets(msg);
                bfs(from[0]-'A');
            }
        }
        for(i=0;i<26;i++)
        {
            if(arr[i].size())
            {
                printf("%c: ",i+'A');
                printf("[");
                for(j=0;j<arr[i].size();j++)
                {
                    if(j)
                        printf(", ");
                    cout<<arr[i][j];
                }
                printf("]\n");
            }
        }
        if(p!=t)
            printf("\n");
    }
    return 0;
}
/*
2
6
VISIBLE B A
MESSAGE A "HELLO"
WEATHER A B
MESSAGE A "GOODBYE"
VISIBLE A B
MESSAGE A "WORLD"
11
VISIBLE B A
VISIBLE C A
VISIBLE D A
VISIBLE E A
VISIBLE F A
VISIBLE G H
MESSAGE A "YOU"
MESSAGE B "ARE"
OBSTACLE B A
MESSAGE C "NOT"
MESSAGE G "ALONE"
*/
