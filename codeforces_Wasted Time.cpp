#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
using namespace std;

double dist(double x1,double y1,double x2,double y2)
{
    return sqrt( ((x1-x2)*(x1-x2)) + ((y1-y2)*(y1-y2)) );
}

main()
{
    double k,x1,y1,x2,y2,res;
    int n,i;
    cin>>n>>k;
    cin>>x1>>y1;
    for(res=0,i=2;i<=n;i++)
    {
        cin>>x2>>y2;
        res+=dist(x1,y1,x2,y2);
        x1=x2;
        y1=y2;
    }
    res/=50;
    res*=k;
    printf("%.9lf\n",res);
    return 0;
}

