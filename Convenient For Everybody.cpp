/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int n,arr[100010],i,s,t,sum,res,d,x,tp,ans;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&n);

    for(i=1; i<=n; i++)
    {
        scanf(" %d",&arr[i]);
    }

    scanf(" %d %d",&s,&t);

    d=t-s;
    sum=0;
    for(i=1; i<d; i++)
    {
        sum+=arr[i];
    }

    res=-1;
    ans=1;

    for(i=1; i<=n; i++)
    {
        tp=i+d-1;
        if(tp>n)tp-=n;
        sum=(sum-arr[i-1])+arr[tp];

        if(sum>res)
        {
            res=sum;
            x=i;
            if(i<=s)
            {
                ans=s-(i-1);
            }
            else
            {
                ans=s-(i-1)+n;
            }
        }
        else if(sum==res)
        {
            if(i<=s)
            {
                if(ans>(s-(i-1)))
                {
                    ans=(s-(i-1));
                    res=sum;
                    x=i;
                }
            }
            else
            {
                if(ans>(s-(i-1)+n))
                {
                    ans=(s-(i-1)+n);
                    res=sum;
                    x=i;
                }
            }

        }
    }

    printf("%d\n",ans);

    return 0;
}
