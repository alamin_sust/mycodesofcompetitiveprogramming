/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

map<string,int>mpp,mpp2;
int t,p,i,n,cnt;
char arr1[110][25][25],arr2[110][25][25];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        mpp.clear();
        scanf(" %d",&n);
        cnt=0;
        for(i=0;i<n;i++)
        {
            scanf(" %s %s",&arr1[p][i],&arr2[p][i]);
            if(mpp[arr1[p][i]]==0)
            {
                mpp[arr1[p][i]]=1;
                cnt++;
            }
            if(mpp[arr2[p][i]]==0)
            {
                mpp[arr2[p][i]]=1;
                cnt++;
            }
        }
        int res=1000;
        for(i=0;i<(1<<n);i++)
        {
            int cnt2=0;
            int x=i;
            mpp2.clear();
            int kk=0;
            while(x)
            {
                if(x%2)
                {
                    if(mpp2[arr1[p][kk]]==0)
                    {
                        mpp2[arr1[p][kk]]=1;
                        cnt2++;
                    }
                    if(mpp2[arr2[p][kk]]==0)
                    {
                        mpp2[arr2[p][kk]]=1;
                        cnt2++;
                    }
                }
                x>>=1;
                kk++;
            }
            if(cnt2==cnt&&__builtin_popcount(i)<res)
            {
                res=__builtin_popcount(i);
            }
        }

        printf("Case #%d: %d\n",p,n-res);

    }

    return 0;
}

