/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


struct node{
int dif;
string name;
};

node arr[110];
int fg[210];
char tp[110];

int comp(node a,node b)
{
    if(a.dif==b.dif)
        return a.name<b.name;
    return a.dif>b.dif;
}

int fc(int k)
{
    memset(fg,0,sizeof(fg));
    int l=arr[k].name.length();
    for(int i=0;i<l;i++)
    {
        fg[arr[k].name[i]]=1;
    }
    int ret=0;
    for(int i=0;i<205;i++)
    {
        ret+=fg[i];
    }
    return ret;
}

int main()
{
    freopen("A-largea.in","r",stdin);
    freopen("A-largeaOut.txt","w",stdout);

    int t,p,i,n;
    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        getchar();
        for(i=0;i<n;i++)
        {
            gets(tp);
            arr[i].name=tp;
            arr[i].dif=fc(i);
        }
        sort(arr,arr+n,comp);
        printf("Case #%d: ",p);
        cout<<arr[0].name<<"\n";
    }

    return 0;
}






