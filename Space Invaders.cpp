#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <cstring>
#include <limits>
#include <vector>
#include <cstdlib>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <algorithm>

using namespace std;

const int INF = 2000000000;
const int MOD = 10007;

typedef long long Long;
typedef double DD;
typedef vector<int> VI;
typedef vector<VI > VVI;
typedef pair<int,int> PII;
typedef vector<PII> VPII;

#define sf scanf
#define pf printf
#define mem(a,b) memset(a,b,sizeof(a))
#define pb push_back
#define REP(i,a,b) for(int i=a; i<=b; ++i)
#define REPI(i,a,b,c) for(int i=a; i<=b; i+=c)
#define REPR(i,a,b) for(int i=b; i>=a; --i)
#define REPRD(i,a,b,c) for(int i=b; i>=a; i-=c)
#define REPB(i,a) for(int i=a; ;i++)
#define REPRB(i,a) for(int i=a; ; i--)
#define mp(a,b) make_pair(a,b)
#define fs first
#define sc second
#define SZ(s) ((int)s.size())
#define PI 3.141592653589793
#define VS vector<string>
#define VI vector<int>
#define VD vector<DD>
#define VL vector<Long>
#define VVL vector<VL >
#define lim 100007
#define tlim (1<<((int)ceil(log2(lim))+1))
#define unq(vec) stable_sort(vec.begin(),vec.end());\
vec.resize(distance(vec.begin(),unique(vec.begin(),vec.end())));
#define BE(a) a.begin(),a.end()
#define rev(a) reverse(BE(a));
#define sorta(a) stable_sort(BE(a))
#define sortc(a) sort(BE(a),comp)

int rr[]= {0,0,-1,1};
int cc[]= {1,-1,0,0};

struct node
{
    int x,y;
};
node u,v;
queue<node>q;

int n,m,k,col[1010][1010],flag,cell[1010],cell2[1010],in[10010];
char arr[1010][1010];

bool bfs1(int x,int y)
{
    while(!q.empty())
        q.pop();
    //printf(" %d %d\n",x,y);
    u.x=x;
    u.y=y;
    if(col[x][y]==1||x==n)
        return true;
    arr[x][y]='.';
    q.push(u);
    col[x][y]=2;
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(int i=0; i<4; i++)
        {
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            if(v.x>=1&&v.x<=n&&v.y>=1&&v.y<=m)
            {
                if(col[v.x][v.y]==1&&arr[v.x][v.y]=='.')
                    return true;
                else if(arr[v.x][v.y]=='.'&&v.x==n)
                    return true;
                if(col[v.x][v.y]==0&&arr[v.x][v.y]=='.')
                {
                    col[v.x][v.y]=2;
                    q.push(v);
                }
            }
        }
    }
    return false;
}



bool bfs2(int x,int y)
{
    while(!q.empty())
        q.pop();
    //printf(" %d %d\n",x,y);
    u.x=x;
    u.y=y;
    if(arr[x][y]==2||x==1)
        return true;
    arr[x][y]='.';
    q.push(u);
    col[x][y]=1;
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(int i=0; i<4; i++)
        {
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            if(v.x>=1&&v.x<=n&&v.y>=1&&v.y<=m)
            {
                if(col[v.x][v.y]==2&&arr[v.x][v.y]=='.')
                    return true;
                else if(arr[v.x][v.y]=='.'&&v.x==1)
                    return true;
                if(col[v.x][v.y]==0&&arr[v.x][v.y]=='.')
                {
                    col[v.x][v.y]=1;
                    q.push(v);
                }
            }
        }
    }
    return false;
}

void bfs3(int x,int y)
{
    while(!q.empty())
        q.pop();
    u.x=x;
    u.y=y;
    q.push(u);
    col[x][y]=2;

    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(int i=0; i<4; i++)
        {
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            if(v.x>=1&&v.x<=n&&v.y>=1&&v.y<=m)
                if(col[v.x][v.y]==0&&arr[v.x][v.y]=='.')
                {
                    col[v.x][v.y]=2;

                    q.push(v);
                }
        }
    }
    return;
}

void bfs(int x,int y)
{
    while(!q.empty())
        q.pop();
    u.x=x;
    u.y=y;
    q.push(u);
    col[x][y]=1;
    if(x==1)
    {
        flag=1;
        return;
    }
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(int i=0; i<4; i++)
        {
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            if(v.x>=1&&v.x<=n&&v.y>=1&&v.y<=m)
                if(col[v.x][v.y]==0&&arr[v.x][v.y]=='.')
                {
                    col[v.x][v.y]=1;
                    if(v.x==1)
                    {
                        flag=1;
                        return;
                    }
                    q.push(v);
                }
        }
    }
    return;
}

void nowrow(int x,int y)
{
    cell[y]=0;
    for(int i=x+1; i<=n; i++)
    {
        if(arr[i][y]=='#')
        {
            cell[y]=i;
            return;
        }
    }
    return;
}

void nowrow2(int x,int y)
{
    cell2[y]=0;
    for(int i=x-1; i>=1; i--)
    {
        if(arr[i][y]=='#')
        {
            cell2[y]=i;
            return;
        }
    }
    return;
}


void deb1(void)
{
    for(int i=1; i<=n; i++)
    {
        for(int j=1; j<=m; j++)
        {
            printf("%d ",col[i][j]);
        }
        printf("\n");
    }
}

int main(int argc, const char **argv)
{

    //ios::sync_with_stdio(false);
    //double st=clock(),en;
    // freopen("input.txt","r",stdin);
    // freopen("output.txt","w+",stdout);
    //en=clock();
    //cerr<<(double)(en-st)/CLOCKS_PER_SEC<<endl;
    int i,j;

    while(cin>>n>>m>>k)
    {
        getchar();
        for(i=1; i<=n; i++)
        {
            for(j=1; j<=m; j++)
            {
                scanf("%c",&arr[i][j]);
            }
            getchar();
        }
        for(i=1; i<=k; i++)
            scanf(" %d",&in[i]);
        //memset(col,0,sizeof(col));

        for(i=1; i<=n; i++)
            for(j=1; j<=m; j++)
                col[i][j]=0;


        flag=0;
        for(i=1; i<=m; i++)
        {
            if(arr[n][i]=='.'&&col[n][i]==0)
            {
                bfs(n,i);
            }
        }
        //deb1();
        for(i=1; i<=m; i++)
        {
            if(arr[1][i]=='.'&&col[1][i]==0)
            {
                bfs3(1,i);
            }
        }
        //deb1();
        //for(i=1; i<=n; i++)
        //  for(j=1; j<=m; j++)
        //     col[i][j]=0;

        if(flag==1)
        {
            printf("0\n");
            continue;
        }
        memset(cell,0,sizeof(cell));
        memset(cell2,0,sizeof(cell2));
        for(i=1; i<=m; i++)
        {
            for(j=1; j<=n; j++)
            {
                if(arr[j][i]=='#')
                {
                    cell[i]=j;
                    // printf("%d--%d\n",i,j);
                    break;
                }
            }
        }
        for(i=1; i<=m; i++)
        {
            for(j=n; j>=1; j--)
            {
                if(arr[j][i]=='#')
                {
                    cell2[i]=j;
                    //printf("%d-------%d\n",i,j);
                    break;
                }
            }
        }

        for(i=1; i<=k; i++)
        {
            int tt=0,pp=0;
            if(in[i]>0)
            {
                tt=cell[abs(in[i])];
                if(tt)
                {
                    pp=bfs1(cell[abs(in[i])],abs(in[i]));
                    nowrow(cell[abs(in[i])],abs(in[i]));
                }
            }
            else
            {
                tt=cell2[abs(in[i])];
                if(tt)
                {
                    pp=bfs2(cell2[abs(in[i])],abs(in[i]));
                    nowrow2(cell2[abs(in[i])],abs(in[i]));
                }
            }
            // printf("%d..\n",tt);
            //  deb1();
            if(tt&&pp)
            {
                flag=1;
                if(in[i]>0)
                    printf("%d\n",i);
                else
                    printf("%d\n",-i);
                break;
            }

            //   if(in[i]>0)

            //   else

            // deb1();
        }
        if(flag==0)
            printf("X\n");
    }
    return 0;
}
/*

####
#..#
#..#
###.
1 1 1 2 3 4 4

*/
