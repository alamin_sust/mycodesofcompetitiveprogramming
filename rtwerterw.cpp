#include <cstring>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <memory.h>
#include <cassert>
#include <queue>
//#include<bits/stdc++.h>

#define in freopen("input.txt", "r", stdin);
#define out freopen("output.txt", "w", stdout);
#define clr(arr, key) memset(arr, key, sizeof arr)
#define pb push_back
#define mp(a, b) make_pair(a, b)
#define PI acos(-1)
#define CF ios_base::sync_with_stdio(0);
#define all(v) v.begin(), v.end()
#define no_of_ones __builtin_popcount // count 1's in a numbers binary representation
#define SZ(v) (int)(v.size())
#define eps 10e-7
#define oo (1LL<<60)
#define N 10000010
#define mod 1000000007
#define re(i,a) for(int i=0; i<a; i++)
#define ll long long
#define llu unsigned long long int
#define VI  vector <int>
#define VVI  vector <VI>
#define PI  pair <ll, int>
#define VPI  vector <PI>
#define VVPI  vector <VPI>
#define VL  vector <ll>
#define VVL  vector <VL>
#define VS  vector <string>
#define VB  vector <bool>
#define VD  vector <long double>
#define VVD  vector <VD>

//int col[8] = {0, 1, 1, 1, 0, -1, -1, -1};
//int row[8] = {1, 1, 0, -1, -1, -1, 0, 1};
//int col[4] = {1, 0, -1, 0};
//int row[4] = {0, 1, 0, -1};
//int months[13] = {31,28,31,30,31,30,31,31,30,31,30,31};

using namespace std;
bool cmp(pair<int,int>p1,pair<int,int>p2)
{
    if(p1.first==p2.first) return p1.second<p2.second;
    return p1.first<p2.first;
}

int main()
{
#ifdef MANSUR
//        in;
//        out;
#endif

    int T,row,column,p,x,y,w;
    int col[105][105],ans;
    vector< pair<int,int> >save;
    scanf("%d", &T);
    for(int cs=1; cs<=T; cs++)
    {
        save.clear();
        memset(col,-1,sizeof col);
        cin>>row>>column>>p;
        for(int i=0; i<p; i++)
        {
            cin>>x>>y;
            col[x][y]=1; //interesting
            save.push_back(make_pair(x,y));
        }
        sort(save.begin(),save.end(),cmp);
        cin>>w;
        for(int i=0; i<w; i++)
        {
            cin>>x>>y;
            col[x][y]=2;//obstacle
        }
        ans=0;
        for(int i=0; i<save.size(); i++)
        {
            if(col[save[i].first][save[i].second]==1)
            {
                int cRowRight=0,cColumnUp=0,cColumnDown=0;
                bool flg=true;
                for(int j=save[i].second; j<=column && flg; j++) //right
                {
                    if(col[save[i].first][j]==2)
                    {
                        flg=false;
                    }
                    else if(col[save[i].first][j]==1)
                    {
                        cRowRight++;
                    }
                }
                flg=true;
                for(int j=save[i].first; j<=row && flg; j++) //down
                {
                    if(col[j][save[i].second]==2)
                    {
                        flg=false;
                    }
                    else if(col[j][save[i].second]==1)
                    {
                        cColumnDown++;
                    }
                }
                flg=true;
                for(int j=save[i].first; j>=1 && flg; j--) //up
                {
                    if(col[j][save[i].second]==2)
                    {
                        flg=false;
                    }
                    else if(col[j][save[i].second]==1)
                    {
                        cColumnUp++;
                    }
                }
                if(cRowRight>cColumnDown && cRowRight>cColumnUp)
                {
                    bool flg=true;
                    for(int j=save[i].second; j<=column && flg; j++)
                    {
                        if(col[save[i].first][j]==2)
                        {
                            flg=false;
                        }
                        else if(col[save[i].first][j]==1)
                        {
                            col[save[i].first][j]=-1;
                        }
                    }
                    ans++;
                }
                else if(cColumnUp>cColumnDown && cColumnUp>cRowRight)
                {
                    flg=true;
                    for(int j=save[i].first; j>=1 && flg; j--) //up
                    {
                        if(col[j][save[i].second]==2)
                        {
                            flg=false;
                        }
                        else if(col[j][save[i].second]==1)
                        {
                            col[j][save[i].second]=-1;
                        }
                    }
                    ans++;
                }
                else
                {
                    flg=true;
                    for(int j=save[i].first; j<=row && flg; j++) //down
                    {
                        if(col[j][save[i].second]==2)
                        {
                            flg=false;
                        }
                        else if(col[j][save[i].second]==1)
                        {
                            col[j][save[i].second]=-1;
                        }
                    }
                    ans++;
                }
            }
        }
        cout<<ans<<endl;
    }
    return 0;
}
/*
1

7 7
46
1 1 1 2 1 3 1 4 1 5 1 6
2 1 2 2 2 3 2 4 2 5
3 1 3 2 3 3 3 4 3 5 3 6 3 7
4 1 4 2 4 3 4 4 4 5 4 6 4 7
5 1 5 2 5 3 5 4 5 5 5 6 5 7
6 1 6 2 6 3 6 4 6 5 6 6 6 7
7 1 7 2 7 3 7 4 7 5 7 6 7 7
3
1 7 2 6 2 7


1

3 4
9
1 1 1 2 1 3
2 1 2 2
3 1 3 2 3 3 3 4
3
1 4 2 3 2 4


1

2 3
5
1 1 1 2
2 1 2 2 2 3
1
1 3
*/
