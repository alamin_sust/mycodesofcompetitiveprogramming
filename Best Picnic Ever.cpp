#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

int arr[1010],det[1010],n,stk[1010],stat[1010],adj[1010][1010],p;

void dfs(int u)
{
    int top=1;
    stk[top]=u;
    while(top>0)
    {
    u=stk[top--];
    stat[u]=1;
    det[u]++;
    for(int i=1;i<=n;i++)
    {
        if(stat[i]==0&&adj[u][i]==p)
        {
            stat[i]=1;
            stk[++top]=i;
        }
    }
    }
    return;
}

main()
{
    int t,k,m,i,j,from,to,res;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        memset(det,0,sizeof(det));
        cin>>k>>n>>m;
        for(i=1;i<=n;i++)
            for(j=1;j<=n;j++)
            adj[i][j]=0;
        for(i=1;i<=k;i++)
        {
            cin>>arr[i];
         //   det[arr[i]]=1;
        }
        for(i=1;i<=m;i++)
        {
            scanf(" %d %d",&from,&to);
            adj[from][to]=p;
        }
        for(i=1;i<=k;i++)
        {
            memset(stat,0,sizeof(stat));
            dfs(arr[i]);
        }
        for(res=0,i=1;i<=n;i++)
        {
         //   printf("%d ",det[i]);
            if(det[i]==k)
                res++;
        }
        printf("Case %d: %d\n",p,res);
    }
    return 0;
}

