#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstring>
#include <cmath>

using namespace std;


int n, m, s;
vector< vector<char> > grid;
int xo, yo;
char ori;
int sti;

void rotate(char orient){
    switch (ori){
            case 'N':
            if (orient == 'D') ori = 'L';
            if (orient == 'E') ori = 'O';
            return;
            break;
            case 'L':
            if (orient == 'D') ori = 'S';
            if (orient == 'E') ori = 'N';
            return;
            break;
            case 'S':
            if (orient == 'D') ori = 'O';
            if (orient == 'E') ori = 'L';
            return;
            break;
            case 'O':
            if (orient == 'D') ori = 'N';
            if (orient == 'E') ori = 'S';
            return;
            break;
    }
}

void move(){
    switch (ori){
            case 'N':
            if (yo > 0){
                if (grid[yo-1][xo] != '#'){
                    yo--;
                    if (grid[yo][xo] == '*') {sti++; grid[yo][xo] = '.';}
                }
            }
            return;
            break;
            case 'L':
            if (xo < n-1){
                if (grid[yo][xo+1] != '#'){
                    xo++;
                    if (grid[yo][xo] == '*') {sti++; grid[yo][xo] = '.';}
                }
            }
            return;
            break;
            case 'S':
            if (yo < n-1){
                if (grid[yo+1][xo] != '#'){
                    yo++;
                    if (grid[yo][xo] == '*') {sti++; grid[yo][xo] = '.';}
                }
            }
            return;
            break;
            case 'O':
            if (xo > 0){
                if (grid[yo][xo-1] != '#'){
                    xo--;
                    if (grid[yo][xo] == '*') {sti++; grid[yo][xo] = '.';}
                }
            }
            return;
            break;
    }
}

int main()
{
    while (1){
        sti=0;
        scanf("%d %d %d\n", &n, &m, &s);
        grid.assign(n, vector<char>());
        for (int i = 0; i<n; i++) grid[i].assign(m, '.');
        if (n == 0 && m == 0 && s == 0) break;
        string line;
        for (int i = 0; i<n; i++){
            getline(cin,line);
            for (int j = 0; j<m; j++){
                grid[i][j] = line[j];
                if (line[j] == 'N' || line[j] == 'S' || line[j] == 'L' || line[j] == 'O' ){
                    xo = j;
                    yo = i;
                    ori = line[j];
                }
            }
        }
        string inst;
        getline(cin,inst);
        for (int i = 0; i<inst.size(); i++){
            if (inst[i] == 'D' || inst[i] == 'E') rotate(inst[i]);
            else if (inst[i] == 'F') move();
        }

        printf("%d\n", sti);

    }
    return 0;
}
