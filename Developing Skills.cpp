/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

struct node{
int first,second;
};

priority_queue<node>pq;

bool operator<(node a,node b)
{
    return a.second>b.second;
}

node now;
int n,k,in,res,i,tp;

int main()
{

    scanf(" %d %d",&n,&k);
    res=0;
    for(i=1;i<=n;i++)
    {
        scanf(" %d",&in);
        res+=(in/10);
        if(in==100)
        {
            now.first=in;
            now.second=100000010;
            pq.push(now);
        }
        else
        {
            tp=10-(in%10);
            now.first=in;
            now.second=tp;
            pq.push(now);
        }
    }



    while(k>0)
    {
        now=pq.top();
        pq.pop();
        //printf("%d..%d\n",now.first,now.second);
        if(now.second>k)
            break;
        k-=now.second;
        now.first+=now.second;
        res++;
        if(now.first==100)
            now.second=100000010;
        else
            now.second=10;
        pq.push(now);
    }



    printf("%d\n",res);


    return 0;
}
