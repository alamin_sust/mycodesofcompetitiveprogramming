
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
int nn[]={0,0,0,0,1,-1};
int rr[]={0,1,0,-1,0,0};
int cc[]={1,0,-1,0,0,0};

struct node
{
    int x,y,z;
};

node det,tp;
int status[33][33][33],val[33][33][33],n,row,col,res,i,j,k;
char arr[33][33][33];

void bfs(void)
{
     memset(status,0,sizeof(status));
     queue<node>q;
     while(!q.empty())
        q.pop();
     q.push(det);
     status[det.x][det.y][det.z]=1;
     val[det.x][det.y][det.z]=0;
     res=inf;
     while(!q.empty())
     {
         det=q.front();
         q.pop();
         for(i=0;i<6;i++)
         {
             if(det.x+nn[i]>0&&det.x+nn[i]<=n&&det.y+rr[i]>0&&det.y+rr[i]<=row&&det.z+cc[i]>0&&det.z+cc[i]<=col)
             if(status[det.x+nn[i]][det.y+rr[i]][det.z+cc[i]]==0)
                if(arr[det.x+nn[i]][det.y+rr[i]][det.z+cc[i]]=='.')
                {
                    val[det.x+nn[i]][det.y+rr[i]][det.z+cc[i]]=val[det.x][det.y][det.z]+1;
                    //printf("--");
                    tp.x=det.x+nn[i];
                    tp.y=det.y+rr[i];
                    tp.z=det.z+cc[i];
                    q.push(tp);
                    status[tp.x][tp.y][tp.z]=1;
                }
                else if(arr[det.x+nn[i]][det.y+rr[i]][det.z+cc[i]]=='E')
                {
                    res=val[det.x][det.y][det.z]+1;
                    return;
                }
         }
     }
     return;
}

main()
{
    while(cin>>n>>row>>col&&n&&row&&col)
    {
        getchar();
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=row;j++)
            {
                for(k=1;k<=col;k++)
                {
                    scanf("%c",&arr[i][j][k]);
                    if(arr[i][j][k]=='S')
                        det.x=i,det.y=j,det.z=k;
                }
                getchar();
            }
            //getchar();
            getchar();
        }
        bfs();
        if(res==inf)
            printf("Trapped!\n");
        else
            printf("Escaped in %d minute(s).\n",res);
    }
    return 0;
}
