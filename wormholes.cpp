#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

int dist[1010],m,n;

struct node{
int from,to,val;
};
node adj_list[2010];

bool bellman_ford(void)
{
    for(int i=0;i<n;i++)
        dist[i]=inf;
    dist[0]=0;
    for(int i=0;i<n-1;i++)
    {
        for(int j=0;j<m;j++)
        {
            if(dist[adj_list[j].from]>dist[adj_list[j].to]+adj_list[j].val)
                dist[adj_list[j].from]=dist[adj_list[j].to]+adj_list[j].val;
        }
    }
    for(int i=0;i<n-1;i++)
    {
        for(int j=0;j<m;j++)
        {
            if(dist[adj_list[j].from]>dist[adj_list[j].to]+adj_list[j].val)
                return false;
        }
    }
    return true;
}

main()
{
    int t,p,i;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n>>m;
        for(i=0;i<m;i++)
        {
            scanf(" %d %d %d",&adj_list[i].from,&adj_list[i].to,&adj_list[i].val);
        }
        if(bellman_ford())
            printf("not possible\n");
        else
            printf("possible\n");
    }
    return 0;
}

