#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
int x,y,val;
};

node det;

bool operator<(node a,node b)
{
    return a.val>b.val;
}

priority_queue<node>pq;

main()
{
    int a,b,c,i,j,n,par[30],t,p,arr[30][30];
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        for(i=1;i<=n;i++)
            par[i]=i;
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
            {
                scanf(" %d",&arr[i][j]);
                if(j!=n)
                    getchar();
            }
        }
        for(i=1;i<=n;i++)
        {
            for(j=i+1;j<=n;j++)
            {
                if(arr[i][j])
                {det.x=i;
                det.y=j;
                det.val=arr[i][j];
                pq.push(det);}
            }
        }
        printf("Case %d:\n",p);
        while(!pq.empty())
        {
            a=pq.top().x;
            b=pq.top().y;
            c=pq.top().val;
            while(par[a]!=a)
                a=par[a];
            while(par[b]!=b)
                b=par[b];
            if(a!=b)
            {
                par[a]=b;
                printf("%c-%c %d\n",'A'+pq.top().x-1,'A'+pq.top().y-1,c);
            }
            pq.pop();
        }
    }
    return 0;
}

