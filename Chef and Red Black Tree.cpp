/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int t,p,bits_max,bits_min,i,bits_a,bits_b,stat,a,b,tp,red,black;
char arr[5];

int main()
{
    cin>>t;
    stat=0;
    for(p=1;p<=t;p++)
    {
        black=red=0;
        scanf(" %s",&arr);
        if(arr[1]=='i')
        {
            stat^=1;
        }
        else if(arr[1]=='b')
        {
            scanf(" %d %d",&a,&b);
            bits_a=bits_b=0;
            tp=a;
            while(tp)
            {
                tp>>=1;
                bits_a++;
            }
            tp=b;
            while(tp)
            {
                tp>>=1;
                bits_b++;
            }
            bits_max=max(bits_a,bits_b);
            bits_min=min(bits_a,bits_b);
            for(i=bits_a;i>bits_min;i--)
            {
                if(i%2)
                    black++;
                else
                    red++;
                a>>=1;
            }
            for(i=bits_b;i>bits_min;i--)
            {
                if(i%2)
                    black++;
                else
                    red++;
                b>>=1;
            }
            for(i=bits_min;;i--)
            {
                if(a==b)
                {
                    if(i%2)
                    black++;
                else
                    red++;
                        break;
                }
                if(i%2)
                    black+=2;
                else
                    red+=2;
                a>>=1;
                b>>=1;
            }
            if(stat)
                swap(red,black);
            printf("%d\n",black);
        }
        else
        {
            scanf(" %d %d",&a,&b);
            bits_a=bits_b=0;
            tp=a;
            while(tp)
            {
                tp>>=1;
                bits_a++;
            }
            tp=b;
            while(tp)
            {
                tp>>=1;
                bits_b++;
            }
            bits_max=max(bits_a,bits_b);
            bits_min=min(bits_a,bits_b);
            for(i=bits_a;i>bits_min;i--)
            {
                if(i%2)
                    black++;
                else
                    red++;
                a>>=1;
            }
            for(i=bits_b;i>bits_min;i--)
            {
                if(i%2)
                    black++;
                else
                    red++;
                b>>=1;
            }
            for(i=bits_min;;i--)
            {
                if(a==b)
                {
                    if(i%2)
                    black++;
                else
                    red++;
                        break;
                }
                if(i%2)
                    black+=2;
                else
                    red+=2;
                a>>=1;
                b>>=1;
            }
            if(stat)
                swap(red,black);
            printf("%d\n",red);
        }
    }
    return 0;
}


