/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll flag,n,m,i,j,arr3[200010],arr1[200010],arr2[200010],res,k;

main()
{
    cin>>n>>m;
    for(i=1; i<=n; i++)
    {
        scanf(" %I64d",&arr1[i]);

    }
    for(i=1; i<=m; i++)
    {
        scanf(" %I64d",&arr2[i]);
        if(i>1)
        {
            if(arr1[i]!=arr1[i-1])
                flag=1;
        }
    }
    if(m==1)
        flag=1LL;
    if(flag==0)
    {
        arr3[1]=1LL;
        for(i=2; i<=n; i++)
        {
            if(arr1[i]==arr1[i-1])
                arr3[i]=arr3[i-1]+1LL;
            else
                arr3[i]=1LL;
        }
        arr3[n+1]=-999999999999999LL;
        for(i=n; i>=1; i--)
        {
            if(arr3[i]>arr3[i+1]&&arr3[i]>m)
            {
                res+=(arr3[i]-m+1)-(arr3[i]/m);
            }
        }
    }
    k=1LL;
    j=999999999999999LL;
    for(i=1; i<=n; i++)
    {
        if((arr1[i]+j)==arr2[k])
        {
            k++;
        }
        else
        {
            k=1;
            j=arr2[k]-arr1[i];
            k++;
        }
        if(k==(m+1))
        {
            res++;
            k=1LL;
            j=999999999999999LL;
        }
    }
    printf("%I64d\n",res);
    return 0;
}

