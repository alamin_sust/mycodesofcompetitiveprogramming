/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll n,i,dst,arr[100010],col[100010],cdst[100010],ind,res,u,v;
vector<ll>adj[100010],val[100010];
queue<ll>q;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);


    scanf(" %I64d",&n);

    for(i=1;i<=n;i++)
    {
        scanf(" %I64d",&arr[i]);
    }

    for(i=2;i<=n;i++)
    {
        scanf(" %I64d %I64d",&ind,&dst);
        adj[i].push_back(ind);
        adj[ind].push_back(i);
        val[i].push_back(dst);
        val[ind].push_back(dst);
    }

    memset(col,0,sizeof(col));

    col[1]=1;
    q.push(1);
    res=1;
    cdst[1]=0LL;
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(i=0;i<adj[u].size();i++)
        {
            dst=val[u][i];
            v=adj[u][i];
            if(col[v])
                continue;
            col[v]=1;
            cdst[v]=max(0LL,cdst[u]+dst);
            if(arr[v]>=cdst[v])
                q.push(v),res++;
        }
    }

    printf("%I64d\n",n-res);

    return 0;
}





