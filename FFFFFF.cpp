/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define MAX 100005
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

pair<int,int>pii[100010];
int sieve[100010],flag[100010];
vector<int>fact[100010];


void calculate(void)
{
    sieve[0]=sieve[1]=1;
    for(int i=2;i<=MAX;i++)
    {
        if(sieve[i]==0)
        {
            fact[i].push_back(i);
            for(int j=2;i*j<=MAX;j++)
                {sieve[i*j]=1;
                //det.cnt=j/i;
                fact[i*j].push_back(i);}
        }
    }
    for(int i=1;i<=100000;i++)
    {
        pii[i].first=pii[i-1].first;
        pii[i].second=pii[i-1].second;
        int k=i;
        for(int j=0;j<fact[i].size();j++)
        {
            while((k%fact[i][j])==0)
            {
                k/=fact[i][j];
                pii[i].second++;
            }
            if(flag[fact[i][j]]==0)
            {
                pii[i].first++;
                flag[fact[i][j]]=1;
            }
        }
    }
    return;
}

int main()
{
    int t,a;
    calculate();
    cin>>t;
    for(int p=1;p<=t;p++)
    {
        cin>>a;
        printf("%d %d\n",pii[a].first,pii[a].second);
    }
    return 0;
}


