#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

int dp[110][1010],n,arr[1010],coin[1010];

int rec(int i,int amt)
{
    if(i>n)
        if(amt==0)
        return 1;
        else
        return 0;
    if(amt==0)
        return 1;
    if(amt<0)
        return 0;
    if(dp[i][amt]!=-1)
        return dp[i][amt];
    int ret1=0;
    for(int j=0;j<=coin[i];j++)
    {
            ret1=(ret1+rec(i+1,amt-(arr[i]*j)))%100000007;
    }
    return dp[i][amt]=ret1;
}

main()
{
    int t,p,i,j,amt;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        memset(dp,-1,sizeof(dp));
        cin>>n>>amt;
        for(i=1;i<=n;i++)
        {
            cin>>arr[i];
        }
        for(i=1;i<=n;i++)
        {
            cin>>coin[i];
        }
        cout<<"Case "<<p<<": "<<rec(1,amt)<<endl;
    }
    return 0;
}

