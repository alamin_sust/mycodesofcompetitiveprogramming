/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int dim,dsq,arr[200][200];


int floodfill(int len,int wid,int sx,int sy,int val)
{
    int ix=-1,iy=-1;
    if(sx==1)
        ix=1;
    if(sy==1)
        iy=1;
    while(sx>=1&&sx<=dsq&&sy>=1&&sy<=dsq&&arr[sx][sy]!=0)
        sx+=ix;
    int ty=sy,i,j;

    for(i=0; i<len; i++,sx+=ix)
    {
        for(j=0,sy=ty; j<wid; j++,sy+=iy)
        {
            if(sx<1||sy<1||sx>dsq||sy>dsq)
                return 0;
            if(arr[sx][sy]!=0)
                return 0;
            arr[sx][sy]=val;
        }
    }

    return 1;

}


int main()
{

    int i,j,k,i1,j1,k1,l[10],w[10],x,y,xx,yy;

    scanf(" %d %d %d %d %d %d",&l[1],&w[1],&l[2],&w[2],&l[3],&w[3]);

    dim=l[1]*w[1];
    dim+=l[2]*w[2];
    dim+=l[3]*w[3];

    dsq=sqrt(dim);
    if((dsq*dsq)!=dim)
    {
        printf("-1\n");
        return 0;
    }

    for(i=0; i<=1; i++)
        for(i1=0; i1<4; i1++)
            for(j=0; j<=1; j++)
                for(j1=0; j1<4; j1++)
                    for(k=0; k<=1; k++)
                        for(k1=0; k1<4; k1++)
                        {
                            memset(arr,0,sizeof(arr));

                            if(i==0)
                                x=l[1],y=w[1];
                            else
                                x=w[1],y=l[1];
                            if(i1==0)
                                xx=1,yy=1;
                            else if(i1==1)
                                xx=1,yy=dsq;
                            else if(i1==2)
                                xx=dsq,yy=1;
                            else if(i1==3)
                                xx=dsq,yy=dsq;
                            if(floodfill(x,y,xx,yy,1)==0)
                                continue;

                            if(j==0)
                                x=l[2],y=w[2];
                            else
                                x=w[2],y=l[2];
                            if(j1==0)
                                xx=1,yy=1;
                            else if(j1==1)
                                xx=1,yy=dsq;
                            else if(j1==2)
                                xx=dsq,yy=1;
                            else if(j1==3)
                                xx=dsq,yy=dsq;
                            if(floodfill(x,y,xx,yy,2)==0)
                                continue;

                            if(k==0)
                                x=l[3],y=w[3];
                            else
                                x=w[3],y=l[3];
                            if(k1==0)
                                xx=1,yy=1;
                            else if(k1==1)
                                xx=1,yy=dsq;
                            else if(k1==2)
                                xx=dsq,yy=1;
                            else if(k1==3)
                                xx=dsq,yy=dsq;
                            if(floodfill(x,y,xx,yy,3)==0)
                                continue;
                            printf("%d\n",dsq);
                            for(int ii=1; ii<=dsq; ii++)
                            {
                                for(int jj=1; jj<=dsq; jj++)
                                {
                                    if(arr[ii][jj]==1)
                                        printf("A");
                                    else if(arr[ii][jj]==2)
                                        printf("B");
                                    else if(arr[ii][jj]==3)
                                        printf("C");
                                }
                                printf("\n");
                            }
                            return 0;

                        }


    printf("-1\n");


    return 0;
}
