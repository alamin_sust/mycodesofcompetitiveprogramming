#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class ChristmasTreeDecorationDiv2
{
public:
	int solve(vector <int> col, vector <int> x, vector <int> y)
	{
	    int ret=0;
	    for(int i=0;i<x.size();i++)
        {
            if(col[x[i]-1]!=col[y[i]-1])
            {
                ret++;
            }
        }
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	ChristmasTreeDecorationDiv2 objectChristmasTreeDecorationDiv2;

	//test case0
	vector <int> param00;
	param00.push_back(1);
	param00.push_back(2);
	param00.push_back(3);
	param00.push_back(3);
	vector <int> param01;
	param01.push_back(1);
	param01.push_back(2);
	param01.push_back(3);
	vector <int> param02;
	param02.push_back(2);
	param02.push_back(3);
	param02.push_back(4);
	int ret0 = objectChristmasTreeDecorationDiv2.solve(param00,param01,param02);
	int need0 = 2;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(1);
	param10.push_back(3);
	param10.push_back(5);
	vector <int> param11;
	param11.push_back(1);
	param11.push_back(3);
	vector <int> param12;
	param12.push_back(2);
	param12.push_back(2);
	int ret1 = objectChristmasTreeDecorationDiv2.solve(param10,param11,param12);
	int need1 = 2;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(1);
	param20.push_back(1);
	param20.push_back(3);
	param20.push_back(3);
	vector <int> param21;
	param21.push_back(1);
	param21.push_back(3);
	param21.push_back(2);
	vector <int> param22;
	param22.push_back(2);
	param22.push_back(1);
	param22.push_back(4);
	int ret2 = objectChristmasTreeDecorationDiv2.solve(param20,param21,param22);
	int need2 = 2;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(5);
	param30.push_back(5);
	param30.push_back(5);
	param30.push_back(5);
	vector <int> param31;
	param31.push_back(1);
	param31.push_back(2);
	param31.push_back(3);
	vector <int> param32;
	param32.push_back(2);
	param32.push_back(3);
	param32.push_back(4);
	int ret3 = objectChristmasTreeDecorationDiv2.solve(param30,param31,param32);
	int need3 = 0;
	assert_eq(3,ret3,need3);

	//test case4
	vector <int> param40;
	param40.push_back(9);
	param40.push_back(1);
	param40.push_back(1);
	vector <int> param41;
	param41.push_back(3);
	param41.push_back(2);
	vector <int> param42;
	param42.push_back(2);
	param42.push_back(1);
	int ret4 = objectChristmasTreeDecorationDiv2.solve(param40,param41,param42);
	int need4 = 1;
	assert_eq(4,ret4,need4);
}
