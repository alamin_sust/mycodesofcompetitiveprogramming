/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int dp[30010],dp2[30010],a,b,l;
string arr;

int fc(int pos)
{
    if(dp2[pos]!=-1) return dp2[pos];
    int ret=0;
    int low=0,high=pos;
    while(low<=high)
    {
        int mid=(low+high)/2;
        if((pos+mid)<=l&&arr.substr(0,pos).find(arr.substr(pos,mid))!=-1)
        {
            low=mid+1;
            ret=max(ret,mid);
        }
        else
        {
            high=mid-1;
        }
    }
    return dp2[pos]=ret;
}

int rec(int pos)
{
    if(pos>=l)
        return 0;
    int &ret=dp[pos];
    if(ret!=-1)
        return ret;
    ret=1000000010;
    ret=min(ret,a+rec(pos+1));

    int cnt=fc(pos);

    if(cnt)
    {
        ret=min(ret,b+rec(pos+cnt));
    }

    /*int k=1,bef=0;
    while((pos+k)<=l&&(bef=arr.substr(bef,pos-bef).find(arr.substr(pos,k)))!=-1)
    {
     ret=min(ret,b+rec(pos+k));
     k++;
    }*/
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    int t,p;
    cin>>t;

    for(p=1;p<=t;p++)
    {
        cin>>l>>a>>b;
        cin>>arr;
        memset(dp,-1,sizeof(dp));
        memset(dp2,-1,sizeof(dp2));
        cout<<rec(0)<<"\n";
    }


    return 0;
}

