#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
int rr[]={0,0,-1,1};
int cc[]={1,-1,0,0};

int arr[45][45],k,ii,jj,row,col,mn,mx,res[45][45][42*42],status[45][45];

struct node{
int x,y,val,mn,mx;
};

bool operator<(node a,node b)
{
    return a.val>b.val;
}

node det,det2;

priority_queue<node>pq;

void bfs(int xx,int yy)
{
    int i,j;
    memset(status,0,sizeof(status));
    while(!pq.empty())
    pq.pop();
    det.x=xx;
    det.y=yy;
    det.val=0;
    det.mn=det.mx=arr[xx][yy];
    k=0;
    pq.push(det);
    status[det.x][det.y]=1;
    while(!pq.empty())
    {
       det=pq.top();
       pq.pop();
       //if(status[det.x][det.y]==1)
       //continue;
       //status[det.x][det.y]=1;
       //det.mn=min(det.mn,arr[det.x][det.y]);
       //det.mx=max(det.mx,arr[det.x][det.y]);
       res[xx][yy][++k]=det.val;
       //printf("%d ",res[xx][yy][k]);
       //abs(arr[xx][yy]-arr[det.x][det.y]);
       for(i=0;i<4;i++)
       {
           ii=det.x+rr[i];
           jj=det.y+cc[i];
           if(status[ii][jj]==0)
           if(ii>0&&ii<=row&&jj>0&&jj<=col)
           {
               status[ii][jj]=1;
               det2.x=ii;
               det2.y=jj;
              det2.mx=max(det.mx,arr[ii][jj]);
               det2.mn=min(det.mn,arr[ii][jj]);
               //det2.val=det2.mx-det2.mn;
               det2.val=det2.mx-det2.mn;
           //    printf("%d..%d-%d\n",det2.val,ii,jj);
               pq.push(det2);
           }
       }
    }
    return;
}

main()
{
    int i,j,in,ans,n,p;
    memset(res,-1,sizeof(res));
    cin>>row>>col;
    for(i=1;i<=row;i++)
    {
        for(j=1;j<=col;j++)
        {
            scanf(" %d",&arr[i][j]);
        }
    }
    for(i=1;i<=row;i++)
    {
        for(j=1;j<=col;j++)
        {
            bfs(i,j);
         //   getchar();
        }
    }
    cin>>n;
    for(p=1;p<=n;p++)
    {
        scanf(" %d",&in);
        ans=inf;
        for(i=1;i<=row;i++)
            for(j=1;j<=col;j++)
            if(res[i][j][in]!=-1)
                ans=min(ans,res[i][j][in]);
            //printf("%d ",res[i][j][in]);
        printf("%d\n",ans);
    }
    return 0;
}

