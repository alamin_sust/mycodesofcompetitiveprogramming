#include<stdio.h>

typedef struct {
int month;
int day;
int year;
} date;

typedef struct {
char name[80];
char street[80];
char city[80];
int acct_no;
char acct_type;
float oldbalance;
float newbalance;
float payment;
date lastpayment;} record;
record readinput(int i);
void writeoutput(record customer);

main()
{
    int i,n;
    record customer[100];
    printf("CUSTOMER BILLING SYSTEM\n\n");
    printf("HOW MANY CUSTOMERS ARE THERE? ");
    scanf("%d", &n);
    for(i=0;i<n;i++)
    {
        customer[i]=readinput(i);

        if(customer[i].payment>0)
        customer[i].acct_type=(customer[i].payment<0.1*customer[i].oldbalance)? 'O' : 'C';

        else
        customer[i].acct_type = (customer[i].oldbalance > 0 )? 'D' :'C';

        customer[i].newbalance = customer[i].oldbalance - customer[i].payment;
    }

    for(i=0;i<n;++i)
    {
        writeoutput(customer[i]);
    }
}

record readinput(int i)
{
    FILE *custbill;
    record customer;
    custbill=fopen("billingdata.txt","w");
    printf("\nCustomer no. %d\n",i+1);
    fprintf(custbill,"\nCustomer no. %d\n",i+1);
    printf("  Name: ");
    fprintf(custbill,"  Name: ");
    scanf(" %[^\n]",customer.name);
    fprintf(custbill,customer.name);
    printf("  Street: ");
    fprintf(custbill,"\n  Street: ");
    scanf(" %[^\n]",customer.street);
    fprintf(custbill,customer.street);
    printf("  City: ");
    fprintf(custbill,"\n  City: ");
    scanf(" %[^\n]",customer.city);
    fprintf(custbill,customer.city);
    printf("  Account number: ");
    fprintf(custbill,"\n  Account number: ");
    scanf("%d",&customer.acct_no);
    fprintf(custbill,"%d",customer.acct_no);
    printf("  Previous balance: ");
    fprintf(custbill,"\n  Previous balance: ");
    scanf("%f",&customer.oldbalance);
    fprintf(custbill,"%f",customer.oldbalance);
    printf("  Current payment: ");
    fprintf(custbill,"\n  Current payment: ");
    scanf("%f",&customer.payment);
    fprintf(custbill,"%f",customer.payment);
    printf("  Payment date (MM/DD/YY): ");
    fprintf(custbill,"\n  Payment date (MM/DD/YY): ");
    scanf("%d/ %d/%d",&customer.lastpayment.month,
                     &customer.lastpayment.day,
                     &customer.lastpayment.year);
    fprintf(custbill,"%d/",customer.lastpayment.month);
    fprintf(custbill,"%d/",customer.lastpayment.month);
    fprintf(custbill,"%d",customer.lastpayment.month);
    fclose(custbill);
    return(customer);
}

void writeoutput(record customer)

{
    FILE *stat;
    stat=fopen("billstat.txt","w");
    printf("\nName: %s",customer.name);
    fprintf(stat,"\nName: %s",customer.name);
    printf("    Account number: %d\n",customer.acct_no);
    fprintf(stat,"    Account number: %d\n",customer.acct_no);
    printf("Street: %s\n",customer.street);
    fprintf(stat,"Street: %s\n",customer.street);
    printf("City: %s\n\n",customer.city);
    fprintf(stat,"City: %s\n\n",customer.city);
    printf("Oldbalance: %7.2f",customer.oldbalance);
    fprintf(stat,"Oldbalance: %7.2f",customer.oldbalance);
    printf("  Current paymment: %7.2f",customer.payment);
    fprintf(stat,"  Current paymment: %7.2f",customer.payment);
    printf("  Newbalance: %7.2f\n\n",customer.newbalance);
    fprintf(stat,"  Newbalance: %7.2f\n\n",customer.newbalance);
    printf("Account Status: ");
    fprintf(stat,"Account Status: ");

    switch (customer.acct_type)
    {
        case 'C':
            printf("CURRENT\n\n");
            fprintf(stat,"CURRENT\n\n");
            break;
        case 'O':
            printf("OVERDUE\n\n");
            fprintf(stat,"OVERDUE\n\n");
            break;
        case 'D':
            printf("DELINQUENT\n\n");
            fprintf(stat,"DELINQUENT\n\n");
            break;
        default:
            printf("ERROR\n\n");
            fprintf(stat,"ERROR\n\n");
    }
    fclose(stat);
    return;
}








