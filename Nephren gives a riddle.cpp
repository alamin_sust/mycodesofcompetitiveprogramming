/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

char arr[110] = "What are you doing at the end of the world? Are you busy? Will you save us?";
char arr_l[110] = "What are you doing while sending \"";
char arr_m[110] = "\"? Are you busy? Will you send \"";
char arr_r[110] = "\"?";
char a1[1010] = "What are you doing while sending \"What are you doing at the end of the world? Are you busy? Will you save us?\"? Are you busy? Will you send \"What are you doing at the end of the world? Are you busy? Will you save us?\"?";

ll tp,len,len_l,n,len_m,len_r,cum[100010],q,k;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    len = strlen(arr);
    len_l = strlen(arr_l);
    len_m = strlen(arr_m);
    len_r = strlen(arr_r);

    //printf("%I64d %I64d %I64d %I64d\n",len,len_l,len_m,len_r);

    scanf(" %I64d",&q);

    ll extra=  len_l+len_m+len_r;

    cum[0]=len;
    for(ll i=1LL;; i++) {
        tp=cum[i-1]*2LL+extra;
        if(tp<=1000000000000000000LL) {
        cum[i]=tp;
        }
        else
        {
            break;
        }
    }

    for(ll i=0LL;i<q;i++) {
        scanf(" %I64d %I64d",&n, &k);
        if(n<=52&&cum[n]<k) {
            printf(".");
            continue;
        }

        k--;
        if(n>53LL) {
            while(n>53LL && (k-len_l)>=0LL) {
                k-=len_l;
                n--;
            }
        }
        if(n==0LL) {
            printf("%c",arr[k]);
            continue;
        }
        if(n==1LL) {
            printf("%c",a1[k]);
            continue;
        }
        for(ll j=n;j>1LL;j--) {
            if(k<len_l) {
                break;
            }
            if(k>=len_l && k<(cum[j-1]+len_l)) {
                k-=len_l;
                continue;
            }
            if(k>=(cum[j-1]+len_l) && k<(cum[j-1]+len_l+len_m)) {
                k-=cum[j-1];
                k+=len;
                break;
            }
            if(k>=(cum[j-1]+len_l+len_m) && k<(cum[j-1]*2LL+len_l+len_m)) {
                k-=(cum[j-1]+len_l+len_m);
                continue;
            }
            if(k>=(cum[j-1]*2LL+len_l+len_m)) {
                k-=(cum[j-1]*2LL);
                k+=(len*2LL);
                break;
            }

        }
        printf("%c",a1[k]);
    }
    printf("\n");
    return 0;
}

