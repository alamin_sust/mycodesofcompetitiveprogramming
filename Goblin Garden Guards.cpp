/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
int rr[]={0,0,1,-1,-1,-1,1,1};
int cc[]={1,-1,0,0,-1,1,-1,1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

bool arr[10005][10005];
ll res,X[20010],Y[20010],r[20010],x[100010],y[100010];

struct node{
ll x,y,ind;
};

node u,v,det;
queue<node>q;

void bfs(void)
{
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(ll i=0LL;i<8LL;i++)
        {
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            v.ind=u.ind;
            if(dist((double)v.x,(double)v.y,(double)X[u.ind],(double)Y[u.ind])<=(double)r[u.ind])
            {
                arr[v.x][v.y]=true;

            q.push(v);
            }
        }
    }
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    ll ng,ns,i;

    scanf(" %I64d",&ng);
    res=ng;
    for(i=0LL;i<ng;i++)
    {
        scanf(" %I64d %I64d",&x[i],&y[i]);
    }

    scanf(" %I64d",&ns);
    for(i=0LL;i<ns;i++)
    {
        scanf(" %I64d %I64d %I64d",&X[i],&Y[i],&r[i]);
        det.x=X[i];
        det.y=Y[i];
        det.ind=i;
        arr[det.x][det.y]=true;
        q.push(det);
    }
printf("..");
    bfs();
    printf("..");
    for(i=0LL;i<ng;i++)
    {
        if(arr[x[i]][y[i]])
            res--;
    }

    printf("%I64d\n",res);

    return 0;
}


