/*Author : Md. Al- Amin
           Reg. No.: 2011331055
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int i,j,n,arrival_time[1010],turn_around_time[1010],burst_time[1010],waiting_time[1010],total_turn_around_time,total_waiting_time;
char name[1010][110];

main()
{
    printf("Enter The Number of Processes: ");
    cin>>n;
    for(i=1;i<=n;i++)
    {
        printf("Process %d Information:\n",i);
        printf("Name: ");
        scanf(" %s",&name[i]);
        printf("Arrival Time: ");
        scanf(" %d",&arrival_time[i]);
        printf("Burst Time: ");
        scanf(" %d",&burst_time[i]);
    }
    for(i=1;i<=n;i++)
    {
        for(j=2;j<=n;j++)
        {
            if(burst_time[j]<burst_time[j-1])
            {
                swap(name[j],name[j-1]);
                swap(burst_time[j],burst_time[j-1]);
                swap(arrival_time[j],arrival_time[j-1]);
            }
        }
    }
    //if(n>=1)
      //  turn_around_time[1]=burst_time[1];
    for(i=1;i<=n;i++)
    {
        waiting_time[i]=waiting_time[i-1]+burst_time[i-1]-arrival_time[i];
        turn_around_time[i]=waiting_time[i]+burst_time[i];
        total_waiting_time+=waiting_time[i];
        total_turn_around_time+=turn_around_time[i];
    }
    printf("\n      NAME    ARRIVAL TIME      BURST TIME    WAITING TIME     TURN AROUND TIME\n");
    for(i=1;i<=n;i++)
    {
        printf("%10s %15d %15d %15d %15d\n",name[i],arrival_time[i],burst_time[i],waiting_time[i],turn_around_time[i]);
    }
    printf("\nTotal Waiting Time: %d\n",total_waiting_time);
    printf("Average Waiting Time: %lf\n",(double)total_waiting_time/(double)n);
    printf("\nTotal Turn Around Time Time: %d\n",total_turn_around_time);
    printf("Average Turn Around Time Time: %lf\n",(double)total_turn_around_time/(double)n);
    return 0;
}
