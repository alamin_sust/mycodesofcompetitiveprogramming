/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node{
ll val,type;
};

node det;
priority_queue<node>pq;

ll arr[100010],i,kk,n,mpp[100010],now,v,tot,inc,q,x,y;
pair<ll,ll>brr[100010];

bool operator<(node a,node b)
{
    if(a.val==b.val)
    return a.type>b.type;
    return a.val>b.val;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    scanf(" %lld",&n);
    for(i=1LL;i<=n;i++)
    {
        scanf(" %lld %lld",&brr[i].first,&brr[i].second);
        det.val=brr[i].first;
        mpp[brr[i].first]++;
        det.type=1LL;
        pq.push(det);
        det.val=brr[i].second+1LL;
        mpp[brr[i].second+1LL]++;
        det.type=2LL;
        pq.push(det);
    }
    now=1LL;
    v=0LL;
    while(!pq.empty())
    {
        kk=pq.top().val;
        for(i=now;i<kk;i++)
            arr[i]=v,tot+=v;
        inc=0LL;
        for(i=0LL;i<mpp[kk];i++)
        {
            det=pq.top();
            pq.pop();
            if(det.type==1LL)
                inc++;
            else
                inc--;
        }
        v+=inc;
        now=kk;
    }
    for(i=now;i<=n;i++)
        arr[i]+=v,tot+=v;
    scanf(" %lld",&q);

    for(i=1LL;i<=q;i++)
    {
        scanf(" %lld %lld",&x,&y);
        kk=tot-(brr[x].second-brr[x].first+1LL)-arr[y];
        if(y>=brr[x].first&&y<=brr[x].second)
        kk++;
        if(kk%2LL)
            printf("O\n");
        else
            printf("E\n");
    }
    return 0;
}
