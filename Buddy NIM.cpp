/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int arr[110],brr[110],i,t,p,n,m,flag,j,k,in;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {
        scanf(" %d %d",&n,&m);
        flag=1;
        for(i=0,j=0; i<n; i++)
        {
            scanf(" %d",&in);
            if(in) arr[j++]=in;
        }
        for(i=0,k=0; i<m; i++)
        {
            scanf(" %d",&in);
            if(in) brr[k++]=in;
        }
        n=j;
        m=k;
        if(n==m)
        {
            sort(arr,arr+n);
            sort(brr,brr+m);
            flag=0;
            for(i=0; i<n; i++)
            {
                if(arr[i]!=brr[i])
                {
                    flag=1;
                    break;
                }
            }
        }
        if(flag)
            printf("Alice\n");
        else
            printf("Bob\n");
    }

    return 0;
}
