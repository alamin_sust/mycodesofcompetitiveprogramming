#include<stdio.h>
#include<math.h>
main()
{
    int n,p;
    double sm_area,x,y,z,a,b,c,rat,ratio,up,down,mid,s,area,trap,res;
    scanf(" %d",&n);
    for(p=1;p<=n;p++)
    {
        scanf(" %lf %lf %lf %lf",&c,&b,&a,&ratio);
        up=a;
        down=0.0;
        mid=a/2.0;
        s=(a+b+c)/2.0;
        area=sqrt(s*(s-a)*(s-b)*(s-c));
        while(1)
        {
            x=mid/a*c;
            y=mid/a*b;
            z=mid;
            s=(x+y+z)/2.0;
            sm_area=sqrt(s*(s-x)*(s-y)*(s-z));
            rat=sm_area/(area-sm_area);
            if((rat+0.0000001)>ratio&&(rat-0.0000001)<ratio)
            {
                res=mid/a*c;
                break;
            }
            else if(rat>ratio)
            {
                up=mid-0.0000001;
            }
            else if(rat<ratio)
            {
                down=mid+0.0000001;
            }
            mid=(up+down)/2.0;
        }
        printf("Case %d: %.8lf\n",p,res);
    }
    return 0;
}
