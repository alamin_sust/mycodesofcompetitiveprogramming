/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll t,p,k;
double h,s,high,low,mid,dif,b,A,B,C,res;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++)
    {
        scanf(" %lf %lf",&h,&s);
        s*=2.0;
        low=0.000000000000001;
        high=sqrt(s);
        k=100LL;
        h*=h;
        res=99999999999999.0;
        while(k--)
        {
            mid=(low+high)/2.0;
            b=s/mid;
            dif=mid*mid+b*b-h;
            if(dif<0)
                dif*=-1.0;
            if(dif<res)
            {
                res=dif;
                A=mid;
                B=b;
                C=sqrt(mid*mid+b*b);
            }
            if((mid*mid+b*b)>h)
            {
                low=mid;
            }
            else
            {
                high=mid;
            }
        }
        if(res<=0.01)
        printf("%.10lf %.10lf %.10lf\n",A,B,C);
        else
        printf("-1\n");
    }

    return 0;
}






