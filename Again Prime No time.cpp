#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ull primecount,sieve[11000],prime[1250],am[1250],an[1250];

void sieve_(void)
{
    ull i,j;
    for(i=0; i<10100; i++)
    {
    sieve[i]=1;
    }
    sieve[0]=sieve[1]=0;
    for(i=2; i<10100; i++)
    {
        while(sieve[i]==0 && i<10100)
        {
            i++;
        }
        prime[++primecount]=i;
        for(j=i*i; j<10100; j+=i)
        {
            sieve[j]=0;
        }
    }
    //cout<<primecount<<endl;
    return;
}

void get_pow(ull m)
{
    ull i;
    for(i=1;prime[i]<=m;i++)
    {
        while((m%prime[i])==0)
        {
            am[i]++;
            m/=prime[i];
        }
    }
    return;
}

void get_fact(ull n)
{
    ull i,m;
    for(i=1;prime[i]<=n;i++)
    {
        m=n;
        while(m)
        {
            m/=prime[i];
            an[i]+=m;
        }
    }
    return;
}

main()
{
    ull t,p,m,n,i,res,flag;
    sieve_();
    scanf(" %llu",&t);
    for(p=1;p<=t;p++)
    {
        memset(am,0,sizeof(am));
        memset(an,0,sizeof(an));
        scanf(" %llu %llu",&m,&n);
        get_pow(m);
        get_fact(n);
        flag=0;
        res=100000000000000000LL;
        for(i=1;i<=primecount;i++)
        {
            if(am[i]||an[i])
            {
                if(am[i]>an[i])
                {
                    flag=1;
                    break;
                }
                if(am[i])
                res=min(res,an[i]/am[i]);
            }
        }
        printf("Case %llu:\n",p);
        if(flag==1)
        {
            printf("Impossible to divide\n");
        }
        else
            printf("%llu\n",res);
    }
    return 0;
}

