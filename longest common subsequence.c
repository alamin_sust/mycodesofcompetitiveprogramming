#include<stdio.h>
#include<string.h>
#define maxn 100100
int max(int a,int b)
{
        return a>b?a:b;
}
int LongestCommonSubsequence(char arr1[],char arr2[])
{
        int Slength = strlen(arr1);
        int Tlength = strlen(arr2);
        int iter,jter;
        for(iter=Slength;iter>=1;iter--)
        {
                arr1[iter] = arr1[iter-1];
        }
        for(iter=Tlength;iter>=1;iter--)
        {
                arr2[iter] = arr2[iter-1];
        }
        int common[Slength+1][Tlength+1];
        for(iter=0;iter<=Tlength;iter++)
        {
                common[0][iter]=0;
        }
        for(iter=0;iter<=Slength;iter++)
        {
                common[iter][0]=0;
        }
        for(iter=1;iter<=Slength;iter++)
        {
                for(jter=1;jter<=Tlength;jter++)
                {
                        if(arr1[iter] == arr2[jter] )
                        {
                                common[iter][jter] = common[iter-1][jter-1] + 1;
                        }
                        else
                        {
                                common[iter][jter] = max(common[iter][jter-1],common[iter-1][jter]);
                        }

                }
        }
        return common[Slength][Tlength];

}
int main()
{
        char arr1[maxn],arr2[maxn];
        while(gets(arr1))
        {
        gets(arr2);
        printf("%d\n",LongestCommonSubsequence(arr1,arr2));
        }
return 0;
}
