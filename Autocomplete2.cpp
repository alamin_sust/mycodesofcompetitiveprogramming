/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


int res,det[1000010][27],i,j,k,l,n,x,y,t,fg;
char arr[1000010];

main()
{
    freopen("autocomplete.txt","r",stdin);
    freopen("autocomplete_output.txt","w",stdout);
    cin>>t;
    for(x=1; x<=t; x++)
    {
        cin>>n;
        memset(det,0,sizeof(det));
        res=0;
        for(k=0,y=1; y<=n; y++)
        {
            scanf(" %s",arr);
            l=strlen(arr);
            j=0;
            fg=0;
            for(i=0; i<l; i++)
            {
                if(fg==0)
                    res++;
                if(det[j][arr[i]-'a']==0)
                {
                    det[j][arr[i]-'a']=++k;
                    j=k;
                    fg=1;
                }
                else
                {
                    j=det[j][arr[i]-'a'];
                }
            }
        }
        printf("Case #%d: %d\n",x,res);
    }
    return 0;
}
