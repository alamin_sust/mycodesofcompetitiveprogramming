#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

ll cnt(ll n,ll d)
{
    ll res=0;
    while(n)
    {
        n/=d;
        res+=n;
    }
    return res;
}

ll cntf(ll n,ll d)
{
    ll res=0;
    while(n%d==0)
    {
        n/=d;
        res++;
    }
    return res;
}


main()
{
    ll t,i,n,r,p,q,two,five;
    scanf(" %lld",&t);
    for(i=1;i<=t;i++)
    {
        two=five=0;
        scanf(" %lld %lld %lld %lld",&n,&r,&p,&q);
        two+=cnt(n,2);
        two-=cnt(n-r,2);
        two-=cnt(r,2);
        two+=cntf(p,2)*q;
        five+=cnt(n,5);
        five-=cnt(n-r,5);
        five-=cnt(r,5);
        five+=cntf(p,5)*q;
        printf("Case %lld: %lld\n",i,min(two,five));
    }
    return 0;
}
