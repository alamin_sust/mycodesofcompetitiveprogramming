#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;


struct DATA{
    int x, y;
};


DATA places[MAXX];
int number_of_dust;
ll dp[1<<MAXX];



bool sameTriangle(int i, int j, int k)
{
    return (places[j].y - places[i].y) * (places[k].x - places[i].x) == (places[j].x - places[i].x) * (places[k].y - places[i].y);
}





ll rec(int used)
{
    /*
    cout<<"calling ";
    loop(i, number_of_dust)
    {
        if(check(used, i) == 0) cout<<0;
        else cout<<1;
    }
    cout<<endl;
    */

    ll &ret = dp[used];

    if(ret != -1) return ret;

    int cnt = 0;

    loop(i, number_of_dust)
    {
        if(check(used, i) == 0) cnt++;
    }
    if(cnt == 1) return 1;


    ret = INF;

    loop(i, number_of_dust)
    {
        if(check(used, i) == 0)
        {

            int temp = (used | (1<<i));

            FOR(j, i+1, number_of_dust)
            {
                int bt = (used | (1<<i));
                if(check(temp, j) == 0)
                {
                    temp = temp | (1<<j);
                    bt = bt | (1<<j);


                    loop(k, number_of_dust)
                    {
                        if(check(temp, k) == 0 && sameTriangle(i, j, k))
                        {
                            temp = temp | (1<<k);
                            bt = bt | (1<<k);
                        }
                    }
                    ret = min(ret, 1 + rec(bt));
                }
            }
            break;
        }
    }
    return ret;
}


int main()
{
    int kases, kaseno = 0;
    getint(kases);

    while(kases--)
    {
        getint(number_of_dust);
        loop(i, number_of_dust)
        {
            getint(places[i].x); getint(places[i].y);
        }

        mem(dp, -1);
        dp[ (1<<number_of_dust) - 1 ] = 0;

        pf("Case %d: %lld\n", ++kaseno, rec(0));

    }

}
