/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};


int flag=0,nx,ny,nx2,ny2,row,col,i,j;
char arr[1010][1010];

void dfs(int x,int y)
{
    if(x<0||x>(row+1)||y<0||y>(col+1))
        return;
    if(arr[x][y]=='O')
        return;
    if(arr[x][y]=='X')
        flag=1;
    arr[x][y]='+';
    dfs(x+1,y);
    dfs(x-1,y);
    dfs(x,y+1);
    dfs(x,y-1);
    return;
}

void func1(int x,int y)
{
    while(1)
    {
        if(x>row||y>col)
            break;
        arr[x][y]='O';
        nx2=x;
        ny2=y;
        if(arr[x+1][y-1]!='X'&&arr[x+1][y]=='X')
            x++;
        else if(arr[x+1][y-1]!='X'&&arr[x+1][y]!='X'&&arr[x][y+1]=='X')
            y++;
        else
            break;
    }
}

void func2(int x,int y)
{
    while(1)
    {
        if(x>row||y>col)
            break;
        arr[x][y]='O';
        nx=x;
        ny=y;
        if(arr[x-1][y+1]!='X'&&arr[x][y+1]=='X')
            y++;
        else if(arr[x-1][y+1]!='X'&&arr[x][y+1]!='X'&&arr[x+1][y]=='X')
            x++;
        else
            break;
    }
}


main()
{
    cin>>row>>col;
    getchar();
    for(i=1; i<=row; i++)
    {
        for(j=1; j<=col; j++)
            scanf("%c",&arr[i][j]);
        getchar();
    }
    for(i=1; i<=row; i++)
    {
        for(j=1; j<=col; j++)
        {
            if(arr[i][j]=='X')
            {
                func1(i,j);
                func2(i,j);
                goto xx;
            }
        }
    }
xx:
    if(nx!=nx2||ny!=ny2)
    {
        printf("-1\n");
        return 0;
    }
    dfs(0,0);
    flag=0;
    if(flag==1)
    {
        printf("-1\n");
        return 0;
    }
    for(i=1; i<=row; i++)
    {
        for(j=1; j<=col; j++)
        {
            if(arr[i][j]=='.')
            {
                printf("-1\n");
                return 0;
            }
            printf("%c",arr[i][j]);
            arr[i][j]='O';
        }
        printf("\n");
    }
    return 0;
}

