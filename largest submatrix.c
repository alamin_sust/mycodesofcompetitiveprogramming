#include<stdio.h>
#include<string.h>
long long int N,N2,sumMatrix[10010][30][30],a[30][30],p;
char arr[10010][30][30];
void preComputeMatrix(void)
{
    long long int i,j;
    for(i=0; i<N; i++)
    {
        for(j=0; j<N2; j++)
        {
            if(i==0 && j==0)
                sumMatrix[p][i][j] = a[i][j];
            else if(i==0)
                sumMatrix[p][i][j]+=sumMatrix[p][i][j-1] + a[i][j];
            else if(j==0)
                sumMatrix[p][i][j]+=sumMatrix[p][i-1][j] + a[i][j];
            else
                sumMatrix[p][i][j]+=sumMatrix[p][i-1][j]+sumMatrix[p][i][j-1]-sumMatrix[p][i-1][j-1] + a[i][j];
        }
    }
   return;
}
long long int computeSum(long long int i1,long long int i2,long long int j1,long long int j2)

{
        if(i1==0 && j1==0)
        return sumMatrix[p][i2][j2];
        else if(i1==0)
        return sumMatrix[p][i2][j2] - sumMatrix[p][i2][j1-1];
        else if(j1==0)
        return sumMatrix[p][i2][j2] - sumMatrix[p][i1-1][j2];
        else
        return sumMatrix[p][i2][j2] - sumMatrix[p][i2][j1-1]- sumMatrix[p][i1-1][j2] + sumMatrix[p][i1-1][j1-1];
}

long long int getMaxMatrix(void)
{
    long long int row1,row2,col1,col2;
    long long int maxSum = -10010;
    for(row1=0; row1<N; row1++)
    {
        for(row2=row1; row2<N; row2++)
        {
            for(col1=0; col1<N2; col1++)
            {
                for(col2=col1; col2<N2; col2++)
                {
                    if(maxSum<computeSum(row1,row2,col1,col2))
                    maxSum=computeSum(row1,row2,col1,col2);
                }
            }
        }
    }
    if(maxSum<0)
    maxSum=0;
    return maxSum;
}
main()
{
   long long int ch,l,in,i,j,t,res[10010] ;
    scanf(" %lld%c",&t,&ch);
    for(p=0;p<t;p++)
    {
    scanf("%c",&ch);
    gets(arr[p][0]);
    l=strlen(arr[p][0]);
    N=l;
    N2=N;
    for(i=1;i<N;i++)
    gets(arr[p][i]);
    for(i=0;i<N;i++)
     {
        for(j=0;j<N;j++)
        {
            if(arr[p][i][j]=='0')
            a[i][j]=-10010;
            else
            a[i][j]=1;
        }
     }
    preComputeMatrix();
    res[p]=getMaxMatrix();
    }
    for(i=0;i<t;i++)
    {
        printf("%lld\n",res[i]);
        if(i!=t-1)
        printf("\n");
    }
    return 0;
}
