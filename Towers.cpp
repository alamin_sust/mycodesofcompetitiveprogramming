/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node1{
int x,id;
};

struct node2{
int x,id;
};

bool operator<(node1 a,node1 b)
{
    return a.x>b.x;
}

bool operator<(node2 a,node2 b)
{
    return a.x<b.x;
}

node1 det1;
node2 det2;
priority_queue<node1>pq_min;
priority_queue<node2>pq_max;
int i,n,k,in,a[1010],b[1010],cnt;

int main()
{
    cin>>n>>k;
    for(i=1;i<=n;i++)
    {
        scanf(" %d",&in);
        det1.x=in;
        det1.id=i;
        det2.x=in;
        det2.id=i;
        pq_min.push(det1);
        pq_max.push(det2);
    }
    for(i=0;i<k;i++)
    {
        det1=pq_min.top();
        det2=pq_max.top();
        if(det2.x>(det1.x+1))
        {
            pq_min.pop();
            pq_max.pop();
            a[cnt]=det2.id;
            b[cnt]=det1.id;
            cnt++;
            det1.x++;
            det2.x--;
            pq_min.push(det1);
            pq_max.push(det2);
        }
        else break;
    }
    printf("%d %d\n",pq_max.top().x-pq_min.top().x,cnt);
    for(i=0;i<cnt;i++)
        printf("%d %d\n",a[i],b[i]);
    return 0;
}


