#include<stdio.h>
#define infinity 999999
#define R 1
#define G 2
#define max 1010
int adj[max][max],distance,shortestpath[max],nodes;
typedef struct info
{
    int predecessor;
    int distance;
    int mark;
}info;

int dijkstra(int from,int to,int vertices)
{
    distance=0;
    info pathinfo[max];
    int mindist,i,j,sourcenode,temppath[max];
    for(i=1;i<=vertices;i++)
    {
        pathinfo[i].predecessor=0;
        pathinfo[i].distance=infinity;
        pathinfo[i].mark=G;
    }
    pathinfo[from].predecessor=0;
    pathinfo[from].distance=0;
    pathinfo[from].mark=R;
    sourcenode=from;
    do
    {
        for(i=1;i<=vertices;i++)
        {
            if(adj[sourcenode][i]>0&&pathinfo[i].mark==G)
            {
            if(pathinfo[i].distance>(pathinfo[sourcenode].distance+adj[sourcenode][i]))
                {
                pathinfo[i].predecessor=sourcenode;
                pathinfo[i].distance=pathinfo[sourcenode].distance+adj[sourcenode][i];
                }
            }
        }
        mindist=infinity;
        sourcenode=0;
        for(i=1;i<=vertices;i++)
        {
            if((pathinfo[i].distance<mindist)&&(pathinfo[i].mark==G))
            {
                mindist=pathinfo[i].distance;
                sourcenode=i;
            }
        }
        if(sourcenode==0)
        return 0;
        pathinfo[sourcenode].mark=R;
    }while(sourcenode!=to);

    temppath[1]=sourcenode;
    sourcenode=pathinfo[sourcenode].predecessor;
    for(i=2;sourcenode!=0;i++)
    {
        temppath[i]=sourcenode;
        sourcenode=pathinfo[sourcenode].predecessor;
    }
    nodes=i-1;
    for(i=1,j=nodes;j>=1;j--,i++)
    {
        shortestpath[i]=temppath[j];
    }
    for(i=1;i<nodes;i++)
    distance+=adj[shortestpath[i]][shortestpath[i+1]];
    return nodes;
}

main()
{
    int cities,edges,n,i,j,weight,from,to;
    scanf(" %d",&n);
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
        adj[i][j]=0;
    }
    scanf(" %d",&edges);
    for(i=1;i<=edges;i++)
    {
        scanf(" %d %d %d",&from,&to,&weight);
        adj[from][to]=weight;
    }
    scanf(" %d %d",&from,&to);
    cities=dijkstra(from,to,n);
    if(cities==0)
    printf("No Path Available\n");
    else
    {
        printf("Go to %d",shortestpath[1]);
        for(i=2;i<=cities;i++)
        printf(" to %d",shortestpath[i]);
        printf("\n");
    }
    printf("distance: %d\n",distance);
    return 0;
}
