/*Author : Md. Al- Amin
20th batch
Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll n,m,k,M=1000000007LL;

struct node
{
    ll si,ei,sj,ej;
};
node edge[50010];

ll comp(node a,node b)
{
    return a.si<b.si;
}

ll func(ll val,ll pw)
{
    if(val<0)
        return 1LL;
    ll ret=1LL;
    ll now=pw;
    for(ll i=0LL; val>0&&i<50LL; i++)
    {
        if(val%2LL)
            ret=(ret*now)%M;
        now=(now*now)%M;
        val/=2LL;
    }
    return ret;
}

ll getnext(ll stpos,ll now)
{
// printf("%lld %lld\n",stpos,now);
    ll low=now+1LL;
    ll high=k,mid,ret=99999999LL;
    while(low<=high)
    {
        mid=(low+high)/2LL;
        if(edge[mid].si>=stpos)
        {
            ret=min(ret,mid);
            high=mid-1LL;
        }
        else
            low=mid+1LL;
    }
// printf("ret%lld\n",ret);
    return ret;
}

map<pair<ll,ll>,ll>dp;
ll gb;

ll rec(ll pos,ll tp)
{
  printf("%lld...%lld\n",pos,tp);
// getchar();
    if(pos>=k)
    {
        printf("%lld  (1)\n",tp);
        gb=(gb+tp)%M;
        return tp;
    }
    ll &ret=dp[make_pair(pos,tp)];
    if(ret!=0LL)
    {
        printf("%lld  (2)\n",ret);
        gb=(gb+ret)%M;
        return ret;
    }
    ret=0LL;
    ret=(ret+((rec(pos+1LL,(tp*func(min(edge[pos+1LL].si,n+1)-max(1LL,edge[pos].si),m))%M))%M))%M;
//ret=(ret+rec(pos+1LL,tp*m))%M;
    ll nxt=getnext(edge[pos].ei,pos);
//ret=(ret+((rec(nxt,(tp*func(min(edge[pos+1LL].si,n)-max(1LL,edge[pos].si),m))%M))%M))%M;
//printf("%lld;;;;\n",nxt);
    ret=(ret+((rec(nxt,(tp*func(min(edge[nxt].si,n)-max(0LL,edge[pos].ei),m))%M))%M))%M;
    return ret;
}

int main()
{
//freopen(".txt","r",stdin);
//freopen(".txt","w",stdout);
    while(scanf("%lld%lld%lld",&n,&m,&k)==3)
    {
        ll res=func(n,m);
        for(ll i=0LL; i<k; i++)
        {
            scanf(" %lld %lld %lld %lld",&edge[i].si,&edge[i].sj,&edge[i].ei,&edge[i].ej);
        }
        sort(edge,edge+k,comp);
        edge[k].si=n+1LL;
//for(ll i=0;i<=k;i++)
// printf("%lld %lld %lld %lld\n",edge[i].si,edge[i].sj,edge[i].ei,edge[i].ej);
        //memset(dp,-1LL,sizeof(dp));
        if(k)
            res=(rec(0LL,1LL*func(max(0LL,edge[0].si-1LL),m)))%M;
        res=gb;
        if(k==0)
            res=(func(n,m))%M;
        printf("%lld\n",res);
    }
    return 0;
}
