/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll n,arr[1000010],odd,sum,i;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %I64d",&n);

    for(i=0LL;i<n;i++) {
        scanf(" %I64d",&arr[i]);
        if(arr[i]%2LL) {
            odd++;
        }
        sum+=arr[i];
    }

    if((sum%2LL)==1LL||odd>0LL) {
        printf("First\n");
        return 0;
    }
    printf("Second\n");

    return 0;
}

