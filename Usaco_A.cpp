#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

#define SZ 10003
ll treeMax[4*SZ];
ll treeMin[4*SZ];
ll arr[SZ];
pair<ll,ll>pll;
ll dp[10005][1005],n,k;

void initMax(ll node,ll b,ll e)
{
    if(b==e)
    {
        treeMax[node]=arr[b];
        return;
    }
    ll left = node*2;
    ll right = node*2+1;
    ll mid = (b+e)/2;
    initMax(left,b,mid);
    initMax(right,mid+1,e);
    treeMax[node] = max(treeMax[left],treeMax[right]);
}


ll queryMax(ll node,ll b,ll e,ll i,ll j)
{
    if(i>e||j<b)return -9999999999999LL;
    if(b>=i&&e<=j) return treeMax[node];
    ll left = node*2;
    ll right = node*2+1;
    ll mid = (b+e)/2;
    return max(queryMax(left,b,mid,i,j),queryMax(right,mid+1,e,i,j));
}

ll rec(ll pos, ll taken) {
    if(pos>n) return 0LL;
    ll &ret = dp[pos][taken];
    if(ret!=-1LL) return ret;
    ret = 0LL;
    if((taken+1LL)==k) {
        ret = (taken+1LL)*queryMax(1LL,1LL,n,pos-taken,pos)+rec(pos+1LL,0LL);
    } else {
        ret = max((taken+1LL)*queryMax(1LL,1LL,n,pos-taken,pos)+rec(pos+1LL,0LL),rec(pos+1LL,taken+1LL));
    }
    return ret;
}

int main()
{
    freopen("teamwork.in","r",stdin);
    freopen("teamwork.out","w",stdout);

    ll i;
    scanf(" %I64d %I64d",&n,&k);
    for(i = 1LL; i<=n; i++)
    {
        scanf(" %I64d",&arr[i]);
    }
    initMax(1LL,1LL,n);

    memset(dp,-1LL,sizeof(dp));

    printf("%I64d\n",rec(1LL,0LL));

    return 0;
}


