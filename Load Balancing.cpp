/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int A,C,B,i,j,k,p,t,in,flag[10010],n,sum,tpsum;

int main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        memset(flag,0,sizeof(flag));
        scanf(" %d",&n);
        for(i=1;i<=n;i++)
        {
            scanf(" %d",&in);
            flag[in]++;
        }
        for(i=1;i<=160;i++)
        {
            flag[i]+=flag[i-1];
            //printf("%d..\n",flag[i]);
        }
        sum=100000010;
        for(i=0;i<=160;i++)
        {
            for(j=i+1;j<=160;j++)
            {
                for(k=j+1;k<=160;k++)
                {
                    tpsum=abs(flag[i]*4-n)+abs((flag[j]-flag[i])*4-n)+abs((flag[k]-flag[j])*4-n)+abs((flag[160]-flag[k])*4-n);
                    if(tpsum<sum)
                    {
                        sum=tpsum;
                        A=i;
                        B=j;
                        C=k;
                       // printf("%d %d %d %d\n",sum,A,B,C);
                    }
                }
            }
        }
        printf("Case %d: %d %d %d\n",p,A,B,C);
    }
    return 0;
}






