/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

char arr[110][110];
int row,col;

void fire(int x,int y,int xx,int yy)
{
    int i,j;
    for(i=x+xx,j=y+yy; i<=row&&i>=1&&j<=col&&j>=1; i+=xx,j+=yy)
    {
        if(arr[i][j]=='x')
        {
            arr[i][j]='&';
            return;
        }
        if(arr[i][j]=='/')
        {
            if(xx==0&&yy==1)
                xx=-1,yy=0;
            else if(xx==1&&yy==0)
                xx=0,yy=-1;
            else if(xx==-1&&yy==0)
                xx=0,yy=1;
            else if(xx==0&&yy==-1)
                xx=1,yy=0;
        }
        else if(arr[i][j]=='\\')
        {
            if(xx==0&&yy==1)
                xx=1,yy=0;
            else if(xx==1&&yy==0)
                xx=0,yy=1;
            else if(xx==-1&&yy==0)
                xx=0,yy=-1;
            else if(xx==0&&yy==-1)
                xx=-1,yy=0;
        }
    }
    return;
}

int main()
{
    int i,j,x,y,p=1;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    while(1)
    {
        scanf(" %d %d",&col,&row);
        getchar();
        if(row==0&&col==0)
            break;
        for(i=0; i<=row+1; i++)
            for(j=0; j<=col+1; j++)
                arr[i][j]='x';
        for(i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
            {
                scanf("%c",&arr[i][j]);
                if(arr[i][j]=='*')
                    x=i,y=j;
            }
            getchar();
        }

        if(arr[x][y+1]!='x')
            fire(x,y,0,1);
        else if(arr[x][y-1]!='x')
            fire(x,y,0,-1);
        else if(arr[x+1][y]!='x')
            fire(x,y,1,0);
        else if(arr[x-1][y]!='x')
            fire(x,y,-1,0);
        printf("HOUSE %d\n",p++);
        for(i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
                printf("%c",arr[i][j]);
            printf("\n");
        }
    }
    return 0;
}

/*

11 6
xxxxxxxxxxx
x../..\...x
x..../....x
*../......x
x.........x
xxxxxxxxxxx
5 5
xxxxx
*...x
x...x
x...x
xxxxx
5 5
xxxxx
x./\x
*./.x
x..\x
xxxxx
6 6
xxx*xx
x/...x
x....x
x/./.x
x\./.x
xxxxxx
10 10
xxxxxxxxxx
x.../\...x
x........x
x........x
x.../\..\x
*...\/../x
x........x
x........x
x...\/...x
xxxxxxxxxx
0 0
*/
