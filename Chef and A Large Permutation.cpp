/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll t,p,i,arr[100010],flag,sum,n,k,S;

int main()
{
    cin>>t;
    for(p=1LL;p<=t;p++)
    {
        scanf(" %lld %lld",&n,&k);
        flag=0LL;
        sum=0LL;
        for(i=0LL;i<k;i++)
        {
            scanf(" %lld",&arr[i]);
        }
        sort(arr,arr+k);
        for(i=0LL;i<k;i++)
        {
            sum+=arr[i];
            S=arr[i]*(arr[i]+1LL)/2LL-sum;
            if(S<arr[i])
            {
                flag=arr[i];
                break;
            }
        }
        if(flag==0LL)
        {
            flag=n*(n+1LL)/2LL-sum+1LL;
        }
        if(flag%2LL)
        {
            printf("Chef\n");
        }
        else
            printf("Mom\n");
    }
    return 0;
}

