/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int t,p,nowval,n,hand,now,k,i,j;
vector<int>v;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);


    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        v.clear();
        hand=0;
        nowval=100;
        while(n){
        k=0;
        now=1;
        while(now<=n)
        {
            now*=2;
            k++;
        }
        k--;
        now/=2;
        n-=now;
        hand=max(hand,k);
        nowval-=(hand-k);
        for(i=k,j=nowval+1;i<hand;j++,i++)
        {
            v.push_back(j);
        }
        for(i=nowval-(k*2)+1,j=0;j<k;j++,i+=2)
        {
            v.push_back(i+1);
            v.push_back(i);
        }
        nowval-=(k*2);
        }


        for(i=0;i<v.size();i++)
        {
            printf("%d",v[i]);
            if(i==v.size()-1)
                printf("\n");
            else
                printf("\n");
        }

    }



    return 0;
}

