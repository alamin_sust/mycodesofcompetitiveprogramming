#include <algorithm>
#include <bitset>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <list>
#include <map>
#include <queue>
#include <stack>
#include <set>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

struct node{
int mn,mx;
};

char arr[50100],str[50100];
node flag[10100];
int t,p,l,k,fg[10100];
deque<int>a,u,c,g;

void init(int pp)
{
    //memset(flag,0,sizeof(flag));
    for(int x=0;x<=pp;x++)
    {
        flag[x].mn=flag[x].mx=0;
    }
    while(!a.empty())
        a.pop_front();
    while(!u.empty())
        u.pop_front();
    while(!c.empty())
        c.pop_front();
    while(!g.empty())
        g.pop_front();
    return;
}

int main()
{
    int mx,i,aa,uu,cc,gg,tpcnt,res,cnt,nowmn,nowmx,j;
    scanf(" %d",&t);
    for(p=1; p<=t; p++)
    {
        scanf(" %s",arr);
        l=strlen(arr);
        k=0;

        scanf(" %d",&mx);
        for(i=0; i<l;)
        {
            if(arr[i]=='A'||arr[i]=='U'||arr[i]=='C'||arr[i]=='G')
            {
                int ii=i;
                i++;
                cnt=0;
                while(i<l&&arr[i]>='0'&&arr[i]<='9')
                {
                    cnt=(cnt*10)+arr[i]-'0';
                    i++;
                }
                for(j=0; j<cnt; j++)
                {
                    //printf("%d..%c\n",k,arr[i]);
                    str[k]=arr[ii];
                    if(arr[ii]=='A')
                        a.push_back(k);
                    else if(arr[ii]=='U')
                        u.push_back(k);
                    else if(arr[ii]=='C')
                        c.push_back(k);
                    else
                        g.push_back(k);
                    k++;
                }
            }
        }
       // printf("%s\n",str);
        while((!a.empty())&&(!u.empty()))
        {
            if(a.front()<u.back())
            {aa=a.front();
            uu=u.back();
          //  printf("%d %d\n",aa,uu);
            a.pop_front();
            u.pop_back();
            }
            else
            {
                aa=a.back();
            uu=u.front();
          //  printf("%d %d\n",aa,uu);
            a.pop_back();
            u.pop_front();
            }
            flag[aa].mn=min(aa,uu);
            flag[aa].mx=max(aa,uu);
            flag[uu].mn=min(aa,uu);
            flag[uu].mx=max(aa,uu);
        }
        tpcnt=0;
        while(!c.empty()&&!g.empty()&&tpcnt<mx)
        {
            tpcnt++;
            if(c.front()<g.back())
            {cc=c.front();
            gg=g.back();
            c.pop_front();
            g.pop_back();
            }
            else{
             cc=c.back();
            gg=g.front();
            c.pop_back();
            g.pop_front();


            }
            flag[cc].mn=min(cc,gg);
            flag[cc].mx=max(cc,gg);
            flag[gg].mn=min(cc,gg);
            flag[gg].mx=max(cc,gg);
        }
        res=0;
        int ttp=0;
        nowmn=nowmx=0;
        memset(fg,0,sizeof(fg));
        for(i=0;i<k;i++)
        {
             //if(fg[i]==1)
               // continue;
             if(fg[flag[i].mn]==1||fg[flag[i].mx]==1)
                continue;
             //printf("%d %d\n",flag[i].mn,flag[i].mx);

             fg[flag[i].mn]=1;
             fg[flag[i].mx]=1;
             if(flag[i].mn<nowmx)
             {
                 nowmx=max(flag[i].mx,nowmx);
                 //res=nowmx-nowmn+1;
                 ttp++;
                 res=max(res,ttp);
             }
             else
             {
                 nowmn=flag[i].mn;
                 nowmx=flag[i].mx;
                 ttp=1;
                 res=max(res,ttp);
             }
        }
        printf("Case %d: %d\n",p,res);
        if(p!=t)
            init(k+1);
    }
    return 0;
}
