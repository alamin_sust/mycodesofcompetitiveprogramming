/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
int x,y;
};

node det,det2;
queue<node>q;

char arr[15][15];
int p,t,n,m,k,i,j,val[15][15];

int bfs(int x,int y)
{
    int ret=1;
    arr[x][y]='#';
    det.x=x;
    det.y=y;
    val[x][y]=1;
    q.push(det);
    while(!q.empty())
    {
        det=q.front();
        q.pop();
        int x2=det.x+1;
        if(x2>n)
            x2-=n;
        if(arr[x2][det.y]!='#')
        {
            if(arr[x2][det.y]=='o')
                {
                val[x2][det.y]=val[det.x][det.y];
                arr[x2][det.y]='#';
                det2.x=x2;
                det2.y=det.y;
                q.push(det2);
                }
            else{
                val[x2][det.y]=val[det.x][det.y]+1;
                ret=max(ret,val[x2][det.y]);
                arr[x2][det.y]='#';
                det2.x=x2;
                det2.y=det.y;
                q.push(det2);
            }
        }
        int y2=det.y+1;
        if(y2>m)
            y2-=m;
        if(arr[det.x][y2]!='#')
        {
            if(arr[det.x][y2]=='o')
                {
                val[det.x][y2]=val[det.x][det.y];
                arr[det.x][y2]='#';
                det2.x=det.x;
                det2.y=y2;
                q.push(det2);
                }
            else{
                val[det.x][y2]=val[det.x][det.y]+1;
                ret=max(ret,val[det.x][y2]);
                arr[det.x][y2]='#';
                det2.x=det.x;
                det2.y=y2;
                q.push(det2);
            }
        }
        x2=det.x-1;
        if(x2<1)
            x2+=n;
        if(arr[x2][det.y]!='#')
        {
            if(arr[x2][det.y]=='o')
                {
                val[x2][det.y]=val[det.x][det.y];
                arr[x2][det.y]='#';
                det2.x=x2;
                det2.y=det.y;
                q.push(det2);
                }
            else{
                val[x2][det.y]=val[det.x][det.y]+1;
                ret=max(ret,val[x2][det.y]);
                arr[x2][det.y]='#';
                det2.x=x2;
                det2.y=det.y;
                q.push(det2);
            }
        }
        y2=det.y-1;
        if(y2<1)
            y2+=m;
        if(arr[det.x][y2]!='#')
        {
            if(arr[det.x][y2]=='o')
                {
                val[det.x][y2]=val[det.x][det.y];
                arr[det.x][y2]='#';
                det2.x=det.x;
                det2.y=y2;
                q.push(det2);
                }
            else{
                val[det.x][y2]=val[det.x][det.y]+1;
                ret=max(ret,val[det.x][y2]);
                arr[det.x][y2]='#';
                det2.x=det.x;
                det2.y=y2;
                q.push(det2);
            }
        }
    }
    return ret;
}

main()
{
    int x,y;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n>>m>>k;
        getchar();
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=m;j++)
            {
                scanf("%c",&arr[i][j]);
                if(arr[i][j]=='x')
                    x=i,y=j;
            }
            getchar();
        }
        int res=bfs(x,y);
        //printf("%d\n",res);
        if(res>=k)
            printf("Case #%d: Fits perfectly!\n",p);
        else printf("Case #%d: Oh no, snake's too fat!\n",p);
    }
    return 0;
}
