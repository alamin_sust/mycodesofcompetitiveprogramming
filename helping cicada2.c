#include<stdio.h>
long long int arr[1010],res,element,ans[1010],nn;

long long int gcd(long long int x,long long int y)
{
        long long int temp;
        while(x!=0)
        {temp=x;
        x=y%x;
        y=temp;}
        return y;
}

long long int lcm(long long int x,long long int y)
{
        return x/gcd(x, y)*y;
}

void backtrack(long long int in,long long int ans_in)
{
    if(in>element)
    {
       long long int tot_lcm,i;
       if(ans_in>1)
        {
        for(tot_lcm=ans[1],i=2;i<ans_in;i++)
        tot_lcm=lcm(tot_lcm,ans[i]);
        if(ans_in%2==0)
            res+=(nn/tot_lcm);
        else
            res-=(nn/tot_lcm);
        }
        return;
    }
    ans[ans_in]=arr[in];
    backtrack(in+1,ans_in+1);
    backtrack(in+1,ans_in);
}

main()
{
   long long int t,i,j;
    scanf(" %lld",&t);
    for(i=1;i<=t;i++)
    {
    res=0;
     scanf(" %lld %lld",&nn,&element);
     for(j=1;j<=element;j++)
        scanf(" %lld",&arr[j]);
     backtrack(1,1);
     printf("Case %lld: %lld\n",i,nn-res);
    }
   return 0;
}
