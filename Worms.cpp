/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int i,n,m,arr[100010],val,res;
main()
{
    cin>>n;
    for(i=1; i<=n; i++)
        scanf(" %d",&arr[i]),arr[i]+=arr[i-1];
    cin>>m;
    for(i=1; i<=m; i++)
    {
        res=0;
        scanf(" %d",&val);
        int low=1;
        int high=n;
        int mid;
        while(low<=high)
        {
            mid=(low+high)/2;
            if(arr[mid]>=val&&arr[mid-1]<val)
             res=mid;
            if(arr[mid]<val)
             low=mid+1;
            else
            high=mid-1;
        }
        printf("%d\n",res);
    }
    return 0;
}

