#include<stdio.h>
#include<math.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<string.h>
#include<vector>

using namespace std;

int t,n,m,res[510][30],adj[510][510][28],p,stat[510][510];

struct node
{
    int city,hr,tc;
};

node det;
queue<node>q;

void bfs(int shr)
{
    //memset(vis,0,sizeof(vis));
    int i,j;
    for(i=1; i<=n; i++)
    {

            if(i==1)
                res[i][shr]=0;
            else
                res[i][shr]=1000000010;

    }
    memset(stat,0,sizeof(stat));
    det.city=1;
    det.hr=shr;
    det.tc=0;
    q.push(det);
    while(!q.empty())
    {
        node u=q.front();

        q.pop();

        for(int x=1; x<=n; x++)
        {

            if(adj[u.city][x][0]>=1000000010||stat[u.city][x]>0)
                continue;
            stat[u.city][x]=1;
            for(i=0,j=u.hr; i<=23; j++,i++)
            {
                if(u.city==1&&i>0)
                    break;
                det.city=x;
                det.hr=(adj[u.city][x][j%24]+j)%24;
                det.tc=u.tc+i+adj[u.city][x][j%24];
                res[x][shr]=min(res[x][shr],det.tc);
                q.push(det);
            }

        }
    }
    return;
}


int main()
{

    freopen("A-small-practice(1).in","r",stdin);
    freopen("output.txt","w",stdout);
    int i,j,from,to,cost,s,d,k,K;
    scanf(" %d",&t);


    for(p=1; p<=t; p++)
    {
        scanf(" %d %d %d",&n,&m,&K);
        for(i=1; i<=n; i++)
        {
            for(j=1; j<=n; j++)
            {
                for(k=0; k<=24; k++)
                {
                    adj[i][j][k]=1000000010;
                }
            }
        }
        for(i=1; i<=m; i++)
        {
            scanf(" %d %d",&from,&to);
            for(j=0; j<24; j++)
            {
                scanf(" %d",&cost);
                adj[from][to][j]=min(adj[from][to][j],cost);
                adj[to][from][j]=min(adj[to][from][j],cost);
            }
        }
        memset(res,-1,sizeof(res));
        for(i=0; i<24; i++)
        {
            bfs(i);
        }
        printf("Case #%d:",p);
        for(i=1; i<=K; i++)
        {
            scanf(" %d %d",&s,&d);
            if(res[s][d]<1000000010)
            printf(" %d",res[s][d]);
            else
            printf(" -1");
        }
        printf("\n");
    }

    return 0;
}
