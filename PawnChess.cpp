/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 100000
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

char arr[110][110];
int a,b;

int getval(int x,int y,int dir)
{
    int ret=0;
    int i=x+dir;
    for(;i>=0&&i<8;i+=dir)
    {
        if(arr[i][y]!='.')
            return inf;
        ret++;

    }
    return max(ret,0);
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    int i,j;
    for(i=0;i<8;i++)
    scanf("%s",&arr[i]);
    a=b=inf;
    for(i=0;i<8;i++)
    {
        for(j=0;j<8;j++)
        {
            if(arr[i][j]=='W')
            {
                a=min(a,getval(i,j,-1));
            }
            else if(arr[i][j]=='B')
            {
                b=min(b,getval(i,j,1));
            }
        }
    }
   // printf("%d %d\n",a,b);
    if(a<=b)
    printf("A\n");
    else
    printf("B\n");

    return 0;
}


