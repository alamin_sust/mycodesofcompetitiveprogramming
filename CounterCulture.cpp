/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll val[110],arr[110],tp,t,p,n,noz,res,dig,pw;

ll rev(ll v)
{
    ll ret=0LL;
    while(v)
    {
        ret=(ret*10LL)+(v%10LL);
        v/=10LL;
    }
    return ret;
}

int main()
{
    //freopen("A-small-attempt0.in","r",stdin);
    //freopen("CounterCultureOut.txt","w",stdout);

   val[1]=1LL;
   val[2]=11LL;
   val[3]=101LL;
   val[4]=1001LL;
   val[5]=10001LL;
   val[6]=100001LL;
   val[7]=1000001LL;
   val[8]=10000001LL;
   val[9]=100000001LL;
   val[10]=1000000001LL;
   val[11]=10000000001LL;
   val[12]=100000000001LL;
   val[13]=1000000000001LL;
   val[14]=10000000000001LL;
   val[15]=100000000000001LL;


    arr[2]=11LL;
    arr[3]=11LL+19LL;
    arr[4]=11LL+19LL+100LL+10LL;
    arr[5]=11LL+19LL+1000LL+10LL;
    arr[6]=11LL+19LL+10000LL+10LL;
    arr[7]=11LL+19LL+100000LL+10LL;
    arr[8]=11LL+19LL+1000000LL+10LL;
    arr[9]=11LL+19LL+10000000LL+10LL;
    arr[10]=11LL+19LL+100000000LL+10LL;
    arr[11]=11LL+19LL+1000000000LL+10LL;
    arr[12]=11LL+19LL+10000000000LL+10LL;
    arr[13]=11LL+19LL+100000000000LL+10LL;
    arr[14]=11LL+19LL+1000000000000LL+10LL;
    arr[15]=11LL+19LL+10000000000000LL+10LL;
    arr[16]=11LL+19LL+100000000000000LL+10LL;


 //printf("%lld\n",rev(31326));
    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++)
    {
        scanf(" %lld",&n);
        printf("Case #%lld: ",p);
        res=n;

        if(n<20LL)
        {
            printf("%lld\n",n);
            continue;
        }
        if((n<100LL)&&(n%10LL)==0LL)
        {
            printf("%lld\n",n);
            continue;
        }
        dig=0LL;
        pw=1LL;
        tp=n;
        noz=0LL;
        while(tp)
        {
            if((tp%10LL)!=0LL)
                noz++;
            pw*=10LL;
            tp/=10LL;
            dig++;
        }
        pw/=10LL;

        if(n<100LL)
        {
            printf("%lld\n",11LL+max(n%10LL,n/pw)+min(n%10LL,n/pw)-1LL);
            continue;
        }

        if(noz==1LL&&(n/pw)==1LL)
        {
            printf("%lld\n",arr[dig]-1LL);
            continue;
        }

        if(noz==1LL)
        {
            printf("%lld\n",arr[dig]+n-val[dig]);
            continue;
        }

        if((n%10LL)>(n/pw)&&noz>2)
        {
            n=rev(n);
            printf("%lld\n",arr[dig]+max(n%10LL,n/pw)+n-(max(n%10LL,n/pw)*pw+1LL)+1LL);
        }
        else
        printf("%lld\n",arr[dig]+max(n%10LL,n/pw)+n-(max(n%10LL,n/pw)*pw+1LL));

    }

    return 0;
}




