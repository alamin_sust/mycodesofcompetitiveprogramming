/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll n,arr[200010],k;

ll func(ll mx)
{
    ll ret=0LL;
    for(ll i=1LL,j=n; i<=n&&i<j; j--)
    {
        if((arr[i]+arr[j])<=mx)
        {
            ret++;
            i++;
        }
    }
    if((ret+k)>=n)
        return 1LL;
    return 0LL;
}


int main()
{
    ll cum=0LL,i,res=0LL;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    scanf(" %I64d %I64d",&n,&k);

    for(i=1LL; i<=n; i++)
    {
        scanf(" %I64d",&arr[i]);
        cum+=arr[i];
    }

    ll low=arr[n];
    ll high=cum;
    ll cnt=100LL;
    res=high;
    while(low<=high&&cnt--)
    {
        ll mid=(low+high)/2LL;
        if(func(mid))
        {
            res=min(res,mid);
            high=mid-1LL;
        }
        else
        {
            low=mid+1LL;
        }
    }

    printf("%I64d\n",res);

    return 0;
}

