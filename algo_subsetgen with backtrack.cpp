#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int n,x[1010],arr[1010],taken[1010];

void process(int n)
{
    for(int i=0;i<n;i++)
    {
        printf("%d ",x[i]);
    }
    printf("\n");
    return;
}

void subset(int i)
{
    process(i);
    for(int j=0;j<n;j++)
    {
        if(taken[j]==0&&(i==0||j>=x[i-1]))
        {
            taken[j]=1;
            x[i]=arr[j];
            subset(i+1);
            taken[j]=0;
        }
    }
    return;
}

main()
{
    int i;
    cin>>n;
    for(i=0;i<n;i++)
        cin>>arr[i];
    sort(arr,arr+n);
    subset(0);
    return 0;
}

