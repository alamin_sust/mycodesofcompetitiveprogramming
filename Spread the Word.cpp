/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;
ll res,t,p,arr[100010],now,n,i,cum[100010];
int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++) {
        scanf(" %lld",&n);

        for(i=1LL;i<=n;i++) {
            scanf(" %lld",&arr[i]);
            cum[i]=arr[i]+cum[i-1];
        }

        now=1LL;
        res=0LL;

        for(;now<n;res++) {
            now+=cum[now];
        }
        printf("%lld\n",res);
    }

    return 0;
}
