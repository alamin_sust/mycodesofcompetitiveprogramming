#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <string.h>
#include <algorithm>

using namespace std;

pair<int,int>in[55];

priority_queue<pair<int,int> >pq;

bool operator<(pair<int,int> aa,pair<int,int> bb)
{
    return aa.second>bb.second;
}

class PrivateD2party
{
public:
	int getsz(vector <int> a)
	{
	    int ret=0,flag[55];
	    memset(flag,0,sizeof(flag));
	    for(int i=0;i<a.size();i++)
        in[i].second=0,in[i].first=i;
	    for(int i=0;i<a.size();i++)
        {
            if(a[i]!=i)
            {
                in[a[i]].second++;
                //printf("--%d--%d--\n",a[i],in[a[i]].second);
            }
        }
        for(int i=0;i<a.size();i++)
        {

            if(in[i].second==0)
            pq.push(in[i]);
        }
        while(!pq.empty())
        {
            printf("..........%d\n",pq.top().first);
            flag[pq.top().first]=1;
            ret++;
            in[a[pq.top().first]].second--;
            int tp=a[pq.top().first];
            pq.pop();
            if(flag[tp]==0&&in[tp].second==0)
            pq.push(make_pair(tp,0));

        }
        printf("%d....\n",ret);
        for(int i=0;i<a.size();i++)
        {
            if(flag[i]==0)
            {
               // printf(" %d..\n",i);
                int tp=0;
                int now=i;
                while(a[now]!=now&&flag[now]==0)
                {
                    tp++;
                    flag[now]=1;
                    now=a[now];
                }
                ret+=tp-1;
            }
        }
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	PrivateD2party objectPrivateD2party;

	//test case0
	vector <int> param00;
	param00.push_back(0);
	param00.push_back(1);
	int ret0 = objectPrivateD2party.getsz(param00);
	int need0 = 2;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(1);
	param10.push_back(0);
	int ret1 = objectPrivateD2party.getsz(param10);
	int need1 = 1;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(1);
	param20.push_back(0);
	param20.push_back(3);
	param20.push_back(2);
	int ret2 = objectPrivateD2party.getsz(param20);
	int need2 = 2;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(5);
	param30.push_back(2);
	param30.push_back(2);
	param30.push_back(4);
	param30.push_back(5);
	param30.push_back(0);
	int ret3 = objectPrivateD2party.getsz(param30);
	int need3 = 5;
	assert_eq(3,ret3,need3);

	//test case4
	vector <int> param40;
	param40.push_back(3);
	param40.push_back(2);
	param40.push_back(1);
	param40.push_back(0);
	param40.push_back(5);
	param40.push_back(4);
	int ret4 = objectPrivateD2party.getsz(param40);
	int need4 = 3;
	assert_eq(4,ret4,need4);

}
