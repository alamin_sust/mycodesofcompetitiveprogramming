/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

char arr[1000010];
int t,p,i,l,lov,c,h,e,f,j;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&t);


    for(p=1;p<=t;p++) {
        scanf(" %s",arr);

        l=strlen(arr);
        lov=0;

        for(i=0;i<l-3;i++) {

            c=0;
            for(j=0;j<4;j++) {
                if(arr[j+i]=='c') c=1;
            }
            h=0;
            for(j=0;j<4;j++) {
                if(arr[j+i]=='h') h=1;
            }
            e=0;
            for(j=0;j<4;j++) {
                if(arr[j+i]=='e') e=1;
            }
            f=0;
            for(j=0;j<4;j++) {
                if(arr[j+i]=='f') f=1;
            }

            if(c+h+e+f==4) {
                lov++;
            }

        }

        if(lov)
        printf("lovely %d\n",lov);
        else printf("normal\n");

    }

    return 0;
}
