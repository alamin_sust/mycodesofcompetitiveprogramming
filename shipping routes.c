#include<stdio.h>
#include<string.h>

long long int N,M,Q,p;
long long int front,rear,adj[20][50][50],status[20][50],orig[20][50],from,to,que[20][10010];

void enq(long long int x)
{
    que[p][rear]=x;
    return;
}

void bfs(long long int from,long long int to)
{
    long long int temp,i;
    front=1;
    rear=1;
    enq(from);
    status[p][from]=2;
    orig[p][from]=NULL;
    while(front<=rear)
    {
        temp=que[p][front];
        front++;
        for(i=1;i<=M;i++)
        {
            if(adj[p][temp][i]==1&&status[p][i]==1)
            {
                rear++;
                enq(i);
                orig[p][i]=temp;
                status[p][i]=2;
            }
        }
    }

    return;
}

main()
{
    char ch,port[20][50][10];
    long long int i,j,n,k,val;
    scanf(" %d",&n);
    for(p=1;p<=n;p++)
    {
        scanf(" %d %d %d%c",&M,&N,&Q,&ch);
        for(i=1;i<=M;i++)
        {
            scanf(" %s",&port[p][i]);
        }
        for(i=1;i<=M;i++)
        {
            for(j=1;j<=M;j++)
            adj[p][i][j]=0;
        }
        for(i=1;i<=N;i++)
        {
            char in1[10],in2[10];
            scanf(" %s %s",in1,in2);
            for(j=1;j<=M;j++)
            {
                if(strcmp(in1,port[p][j])==0)
                {from=j;
                break;}
            }
            for(j=1;j<=M;j++)
            {
                if(strcmp(in2,port[p][j])==0)
                {to=j;
                break;}
            }
            adj[p][from][to]=1;
            adj[p][to][from]=1;
        }
        for(i=1;i<=Q;i++)
        {
            char in1[10],in2[10];
            scanf(" %d %s %s",&val,in1,in2);
            for(j=1;j<=M;j++)
            {
                if(strcmp(in1,port[p][j])==0)
                {from=j;
                break;}
            }
            for(j=1;j<=M;j++)
            {
                if(strcmp(in2,port[p][j])==0)
                {to=j;
                break;}
            }
            for(j=1;j<=M;j++)
            status[p][j]=1;
            bfs(from,to);
            j=0;
            k=to;
            while(orig[p][k]!=NULL)
            {
                j++;
                k=orig[p][k];
            }
            if(p==1&&i==1)
            printf("SHIPPING ROUTES OUTPUT\n\n");
            if(i==1)
            printf("DATA SET  %d\n\n",p);
            if(j==0)
            printf("NO SHIPMENT POSSIBLE\n");
            else
            printf("$%d\n",j*val*100);
            if(i==Q)
            printf("\n");
        }
        if(p==n)
        printf("END OF OUTPUT\n");
    }
    return 0;
}
