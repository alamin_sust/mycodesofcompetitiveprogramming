#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

double dp[1000010];
ll t,n,base;
main()
{
    for(ll i=1;i<=1000000;i++)
        dp[i]=dp[i-1]+log(i);
    cin>>t;
    for(ll i=1;i<=t;i++)
    {
        scanf(" %lld %lld",&n,&base);
        printf("Case %lld: %lld\n",i,(ll)(dp[n]/(dp[base]-dp[base-1]))+1);
    }
    return 0;
}

