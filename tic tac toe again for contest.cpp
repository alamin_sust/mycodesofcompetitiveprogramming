/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int i,j,p,q,flag[10][10],flag2[10][10],x,z,xx,zz,n;
char arr[10][10];

main()
{
    cin>>n;
    for(i=1;i<=n;i++)
    {
        memset(flag,0,sizeof(flag));
        memset(flag2,0,sizeof(flag2));
        x=z=0;
        for(j=0;j<3;j++)
        {
            scanf(" %s",&arr[j]);
            if(arr[j][0]=='X')
                x++;
            else if(arr[j][0]=='O')
                z++;
            if(arr[j][1]=='X')
                x++;
            else if(arr[j][1]=='O')
                z++;
            if(arr[j][2]=='X')
                x++;
            else if(arr[j][2]=='O')
                z++;
        }
        if(x!=(z+1)&&x!=z)
        {
            printf("no\n");
            continue;
        }
        xx=zz=0;
        p=0;
        q=0;
        if(arr[p][q]=='X'&&arr[p][q+1]=='X'&&arr[p][q+2]=='X')
        {
            if(flag[p][q]==0&&flag[p][q+1]==0&&flag[p][q+2]==0)
            {
                xx++;
            }
            flag[p][q]=1;
            flag[p][q+1]=1;
            flag[p][q+2]=1;
        }
        p=1;
        q=0;
        if(arr[p][q]=='X'&&arr[p][q+1]=='X'&&arr[p][q+2]=='X')
        {
            if(flag[p][q]==0&&flag[p][q+1]==0&&flag[p][q+2]==0)
            {
                xx++;
            }
            flag[p][q]=1;
            flag[p][q+1]=1;
            flag[p][q+2]=1;
        }
        p=2;
        q=0;
        if(arr[p][q]=='X'&&arr[p][q+1]=='X'&&arr[p][q+2]=='X')
        {
            if(flag[p][q]==0&&flag[p][q+1]==0&&flag[p][q+2]==0)
            {
                xx++;
            }
            flag[p][q]=1;
            flag[p][q+1]=1;
            flag[p][q+2]=1;
        }
        p=0;
        q=0;
        if(arr[p][q]=='X'&&arr[p+1][q]=='X'&&arr[p+2][q]=='X')
        {
            if(flag[p][q]==0&&flag[p+1][q]==0&&flag[p+2][q]==0)
            {
                xx++;
            }
            flag[p][q]=1;
            flag[p+1][q]=1;
            flag[p+2][q]=1;
        }
        p=0;
        q=1;
        if(arr[p][q]=='X'&&arr[p+1][q]=='X'&&arr[p+2][q]=='X')
        {
            if(flag[p][q]==0&&flag[p+1][q]==0&&flag[p+2][q]==0)
            {
                xx++;
            }
            flag[p][q]=1;
            flag[p+1][q]=1;
            flag[p+2][q]=1;
        }

        p=0;
        q=2;
        if(arr[p][q]=='X'&&arr[p+1][q]=='X'&&arr[p+2][q]=='X')
        {
            if(flag[p][q]==0&&flag[p+1][q]==0&&flag[p+2][q]==0)
            {
                xx++;
            }
            flag[p][q]=1;
            flag[p+1][q]=1;
            flag[p+2][q]=1;
        }
        if(arr[0][0]=='X'&&arr[1][1]=='X'&&arr[2][2]=='X')
        {
            if(flag[0][0]==0&&flag[1][1]==0&&flag[2][2]==0)
            {
                xx++;
            }
            flag[0][0]=1;
            flag[1][1]=1;
            flag[2][2]=1;
        }
        if(arr[2][0]=='X'&&arr[1][1]=='X'&&arr[0][2]=='X')
        {
            if(flag[2][0]==0&&flag[1][1]==0&&flag[0][2]==0)
            {
                xx++;
            }
            flag[2][0]=1;
            flag[1][1]=1;
            flag[0][2]=1;
        }


        p=0;
        q=0;
        if(arr[p][q]=='O'&&arr[p][q+1]=='O'&&arr[p][q+2]=='O')
        {
            if(flag2[p][q]==0&&flag2[p][q+1]==0&&flag2[p][q+2]==0)
            {
                zz++;
            }
            flag2[p][q]=1;
            flag2[p][q+1]=1;
            flag2[p][q+2]=1;
        }
        p=1;
        q=0;
        if(arr[p][q]=='O'&&arr[p][q+1]=='O'&&arr[p][q+2]=='O')
        {
            if(flag2[p][q]==0&&flag2[p][q+1]==0&&flag2[p][q+2]==0)
            {
                zz++;
            }
            flag2[p][q]=1;
            flag2[p][q+1]=1;
            flag2[p][q+2]=1;
        }
        p=2;
        q=0;
        if(arr[p][q]=='O'&&arr[p][q+1]=='O'&&arr[p][q+2]=='O')
        {
            if(flag2[p][q]==0&&flag2[p][q+1]==0&&flag2[p][q+2]==0)
            {
                zz++;
            }
            flag2[p][q]=1;
            flag2[p][q+1]=1;
            flag2[p][q+2]=1;
        }
        p=0;
        q=0;
        if(arr[p][q]=='O'&&arr[p+1][q]=='O'&&arr[p+2][q]=='O')
        {
            if(flag2[p][q]==0&&flag2[p+1][q]==0&&flag2[p+2][q]==0)
            {
                zz++;
            }
            flag2[p][q]=1;
            flag2[p+1][q]=1;
            flag2[p+2][q]=1;
        }
        p=0;
        q=1;
        if(arr[p][q]=='O'&&arr[p+1][q]=='O'&&arr[p+2][q]=='O')
        {
            if(flag2[p][q]==0&&flag2[p+1][q]==0&&flag2[p+2][q]==0)
            {
                zz++;
            }
            flag2[p][q]=1;
            flag2[p+1][q]=1;
            flag2[p+2][q]=1;
        }

        p=0;
        q=2;
        if(arr[p][q]=='O'&&arr[p+1][q]=='O'&&arr[p+2][q]=='O')
        {
            if(flag2[p][q]==0&&flag2[p+1][q]==0&&flag2[p+2][q]==0)
            {
                zz++;
            }
            flag2[p][q]=1;
            flag2[p+1][q]=1;
            flag2[p+2][q]=1;
        }
        if(arr[0][0]=='O'&&arr[1][1]=='O'&&arr[2][2]=='O')
        {
            if(flag2[0][0]==0&&flag2[1][1]==0&&flag2[2][2]==0)
            {
                zz++;
            }
            flag2[0][0]=1;
            flag2[1][1]=1;
            flag2[2][2]=1;
        }
        if(arr[2][0]=='O'&&arr[1][1]=='O'&&arr[0][2]=='O')
        {
            if(flag2[2][0]==0&&flag2[1][1]==0&&flag2[0][2]==0)
           {
                zz++;
            }
            flag2[2][0]=1;
            flag2[1][1]=1;
            flag2[0][2]=1;
        }

        if((xx+zz)>1)
        {
            printf("no\n");
        }
        else if((xx==1&&x==z)||(zz==1&&x==(z+1)))
            printf("no\n");
        else
            printf("yes\n");

    }
    return 0;
}


