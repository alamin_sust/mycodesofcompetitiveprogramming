#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

unsigned long long MOD=pow(2,32);

struct node
{
    unsigned long long v1,v2;
};

node arr[40];

unsigned long long x=1,res[40][40],temp[40][40],mat[40][40][40];

void make_node(void)
{
    for(unsigned long long i=1;i<x;i++)
    {
        for(unsigned long long j=1;j<x;j++)
        {
              if((arr[i].v1<(arr[j].v1-1)||arr[i].v1>(arr[j].v1+1))&&(arr[i].v1<(arr[j].v2-1)||arr[i].v1>(arr[j].v2+1)))
                if((arr[i].v2<(arr[j].v1-1)||arr[i].v2>(arr[j].v1+1))&&(arr[i].v2<(arr[j].v2-1)||arr[i].v2>(arr[j].v2+1)))
              {
                  mat[1][i][j]=1;
              }
        }
    }
    return;
}

void make_adj_mat(void)
{
    for(unsigned long long i=1;i<9;i++)
    {
        for(unsigned long long j=i+2;j<11;j++)
        {
            //printf("..");
              arr[x].v1=i;
              arr[x].v2=j;
              //mat[0][i][j]=1;
              x++;
        }
    }
    make_node();
    return;
}

void calc(unsigned long long power)
{
    for(unsigned long long i=1;i<37;i++)
    {
        for(unsigned long long j=1;j<37;j++)
        {
            temp[i][j]=res[i][j];
        }
    }
    for(unsigned long long i=1;i<37;i++)
    {
        for(unsigned long long j=1;j<37;j++)
        {
            res[i][j]=0;
            for(unsigned long long k=1;k<37;k++)
            res[i][j]+=(temp[k][j]*mat[power][i][k]);
           res[i][j]%=MOD;
        }
    }
    return;
}

void make_bigmod(unsigned long long power)
{
    for(unsigned long long i=1;i<37;i++)
    {
        for(unsigned long long j=1;j<37;j++)
        {
            for(unsigned long long k=1;k<37;k++)
            mat[power][i][j]+=(mat[power-1][i][k]*mat[power-1][k][j]);
          // mat[power][i][j]%=MOD;
        }
    }
    return;
}

main()
{
    unsigned long long n,p,t,i,j,flag,result;
    //int ss=pow(36,4);
    //printf("%d\n",ss);
   // printf("%lld\n",result);
    cin>>t;
    make_adj_mat();
    //make_bigmod();
    for(i=2;i<32;i++)
        make_bigmod(i);
    //cout<<x<<endl;
    //for(i=1;i<37;i++)
      //  {for(j=1;j<37;j++)
        //printf("%lld ",mat[0][i][j]);
        //printf("\n");}
    for(p=1;p<=t;p++)
    {
        cin>>n;
        if(n==1)
        {printf("Case %llu: 36\n",p);
        continue;}
        //n--;
        flag=0;
        n=n*2-2;
        long long int k=0;
        while(((n>>k)|0)!=0)
        {

            if(((n>>k)&1)==1)
            {
            if(flag==0)
            for(flag=1,i=1;i<37;i++)
            for(j=1;j<37;j++)
                res[i][j]=mat[k][i][j];
            else
            //{
                //printf("%lld..\n",k);
                calc(k);
            //}
            }
            k++;
        }
        //printf("%lld..\n",k);
        for(result=0,i=1;i<37;i++)
        {
            for(j=1;j<37;j++)
                result+=res[i][j];
        }
        printf("Case %llu: %llu\n",p,result%MOD);
    }
    return 0;
}
