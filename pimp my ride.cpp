#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
#define inf 999999999
using namespace std;

int n,dp[16][1<<16],arr[16][16];

int rec(int row,int mask)
{
    if(row>=n)
    return 0;
    if(dp[row][mask]!=-1)
        return dp[row][mask];
    dp[row][mask]=inf;
    for(int i=0;i<n;i++)
    {
        if((mask&(1<<i))==0)
        {
        int cost=0;
        for(int j=0;j<n;j++)
                if(i!=j&&(mask&(1<<j))!=0)
                    cost+=arr[i][j];
        dp[row][mask]=min(dp[row][mask],rec(row+1,(mask|(1<<i)))+cost);
        }

    }
    return dp[row][mask];
}

main()
{
    int t,p,i,j,res;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        memset(dp,-1,sizeof(dp));
        cin>>n;
        for(res=0,i=0;i<n;i++)
            for(j=0;j<n;j++)
            {cin>>arr[i][j];
            if(i==j)
                res+=arr[i][j];}
        res+=rec(0,0);
        printf("Case %d: %d\n",p,res);
            }
    return 0;
}

