#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
using namespace std;

int n,i,xx,yy,j,det;
double slope[101010];

struct node
{
    int x,y;
};

node temp,res[600];
stack<node>stk;

node pt[10010],pt2[10010];

int angle(int x1,int y1,int x2,int y2,int x3,int y3)
{
    int  val=((y2-y1)*(x3-x2))-((x2-x1)*(y3-y2));
    if(val<0)
        return 0;
    else return 1;
}

int comp1(node a,node b)
{
    return a.y<b.y;
}

int comp2(node a,node b)
{
    if(a.y==b.y)
        return a.x<b.x;
    else
        return a.y<b.y;
}

main()
{
    int t;
    cin>>t;
    cout<<t<<endl;
    for(int p=1; p<=t; p++)
    {
        scanf(" %d",&n);
        n--;
        for(i=1; i<=n; i++)
        {
            scanf(" %d %d",&pt[i].x,&pt[i].y);
        }
        cin>>i>>i>>i;
        sort(pt+1,pt+n+1,comp1);
        sort(pt+1,pt+n+1,comp2);
        for(i=2; i<=n; i++)
        {
            if(pt[i].x==pt[1].x)
                slope[i]=0;
            else if((double)pt[i].x>(double)pt[1].x)
                slope[i]=(fabs((double)pt[i].y-(double)pt[1].y)/fabs((double)pt[i].x-(double)pt[1].x));
            else
                slope[i]=999999999.0-(fabs((double)pt[i].y-(double)pt[1].y)/fabs((double)pt[i].x-(double)pt[1].x));
        }
        //printf("llllll");
        for(i=2; i<=n; i++)
        {
            for(j=3; j<=n; j++)
            {
                if(slope[j]<slope[j-1])
                {
                    swap(slope[j],slope[j-1]);
                    swap(pt[j].x,pt[j-1].x);
                    swap(pt[j].y,pt[j-1].y);
                }
                else if((slope[j]+eps)>slope[j-1]&&(slope[j]-eps)<slope[j-1])
                {
                    if(pt[j].y>pt[j-1].y)
                    {
                        swap(pt[j].x,pt[j-1].x);
                        swap(pt[j].y,pt[j-1].y);
                    }
                }
            }
        }
        //printf("gg");
        pt[n+1].x=pt[1].x;
        pt[n+1].y=pt[1].y;
        n++;
        temp.x=pt[1].x;
        temp.y=pt[1].y;
        stk.push(temp);
        temp.x=pt[2].x;
        temp.y=pt[2].y;
        stk.push(temp);
        temp.x=pt[3].x;
        temp.y=pt[3].y;
        stk.push(temp);
        //printf("oo");
        for(i=4; i<=n; i++)
        {
            node temp2;
            temp2.x=stk.top().x;
            temp2.y=stk.top().y;
            stk.pop();
            temp.x=stk.top().x;
            temp.y=stk.top().y;
            stk.push(temp2);
            while(angle(temp.x,temp.y,temp2.x,temp2.y,pt[i].x,pt[i].y))
            {
                //printf("mm");
                stk.pop();
                temp2.x=stk.top().x;
                temp2.y=stk.top().y;
                stk.pop();
                temp.x=stk.top().x;
                temp.y=stk.top().y;
                stk.push(temp2);
            }
            temp.x=pt[i].x;
            temp.y=pt[i].y;
            //printf("llllll");
            stk.push(temp);
        }
        int k=0;
        while(!stk.empty())
        {
            res[k].x=stk.top().x;
            res[k++].y=stk.top().y;
            stk.pop();
        }
        printf("%d\n",k);
        for(i=k-1; i>=0; i--)
            printf("%d %d\n",res[i].x,res[i].y);
        printf("-1\n");
    }
    return 0;
}


