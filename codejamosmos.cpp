#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#define pi 2*acos(0)
#define eps 0.00000001
using namespace std;

main()
{
    int t,p,i,res,a,n,arr[110];
    FILE *osmin,*osmout;
    osmin=fopen("codejamosmin.txt","r");
    osmout=fopen("codejamosmout.txt","w");
    fscanf(osmin," %d",&t);
    for(p=1;p<=t;p++)
    {
        fscanf(osmin," %d %d",&a,&n);
        for(i=0;i<n;i++)
        {
            fscanf(osmin," %d",&arr[i]);
        }
        sort(arr,arr+n);
        res=0;
        for(i=0;i<n;i++)
        {
            if(arr[i]>=a)
            {
                if((a+a-1)<=arr[i])
                {
                    res++;
                }
                else
                {
                    res++;
                    a=a+a-1+arr[i];
                }
            }
            else
            a+=arr[i];
        }
        fprintf(osmout,"Case #%d: %d\n",p,res);
    }
    fclose(osmin);
    fclose(osmout);
    return 0;
}
