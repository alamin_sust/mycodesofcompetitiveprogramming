/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int dp[200010][20];
vector<int>adj[200010];

int rec(int pos,int val)
{
    if(adj[pos].size()==0)
        return 0;
    int &ret=dp[pos][val];
    if(ret!=-1)
        return ret;
    ret=0;
    int tp=0;
    for(int i=0; i<adj[pos].size(); i++)
    {
        tp=99999999;
        for(int j=1; j<=19; j++)
        {
            if(j==val)
                continue;
            tp=min(tp,j+rec(adj[pos][i],j));
        }
        ret+=tp;
    }
    return ret;
}

int main()
{
    int t,p,i,n,in;
    freopen("corporate_gifting.txt","r",stdin);
    freopen("corporate_gifting_output.txt","w",stdout);
    scanf(" %d",&t);
    for(p=1; p<=t; p++)
    {
        scanf(" %d",&n);
        for(i=1; i<=n; i++)
        {
            scanf(" %d",&in);
            adj[in].push_back(i);
        }
        memset(dp,-1,sizeof(dp));
        printf("Case #%d: %d\n",p,rec(0,0));
        if(p!=t)
        {
            for(i=0; i<=n; i++)
                adj[i].clear();
        }
    }
    return 0;
}
