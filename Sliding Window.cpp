/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define llu long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int i,n,k,arr[1000010];

struct node
{
    int ind,val;
};

bool operator<(node a,node b)
{
    return a.val>b.val;
}

priority_queue<node>pq;
stack<node>stk;
node det;
node window_min[1000010],window_max[1000010];
int tp,now,now_ind;

int main()
{
    cin>>n>>k;
    for(i=1;i<=n;i++)
    {
        scanf(" %d",&arr[i]);
        det.ind=i;
        det.val=arr[i];
        pq.push(det);
    }

    while(!pq.empty())
    {
        det=pq.top();
        pq.pop();
        stk.push(det);
        now=det.ind-window_min[det.ind].ind;
        now_ind=k-window_min[det.ind].ind;
        //printf("%d %d\n",det.ind,det.val);
        while(now>0&&now_ind>0)
        {
            if(window_min[now].ind==0)
            {
                window_min[now].ind=now_ind;
                window_min[now].val=det.val;
                now_ind--;
                now--;
            }
            else
            {
                break;
               // tp=window_min[now].ind;
               // now-=tp;
               // now_ind-=tp;
            }
        }
    }

    while(!stk.empty())
    {
        det=stk.top();
        stk.pop();
        now=det.ind-window_max[det.ind].ind;
        now_ind=k-window_max[det.ind].ind;
        //printf("%d %d\n",det.ind,det.val);
        while(now>0&&now_ind>0)
        {
            if(window_max[now].ind==0)
            {
                window_max[now].ind=now_ind;
                window_max[now].val=det.val;
                now_ind--;
                now--;
            }
            else
            {
                break;
                //tp=window_max[now].ind;
                //now-=tp;
                //now_ind-=tp;
            }
        }
    }

    for(i=1;(i+k-1)<=n;i++)
    {
        if(i!=1)
            printf(" ");
        printf("%d",window_min[i].val);
    }
    printf("\n");

    for(i=1;(i+k-1)<=n;i++)
    {
        if(i!=1)
            printf(" ");
        printf("%d",window_max[i].val);
    }
    printf("\n");
    return 0;
}

