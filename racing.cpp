#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
int x,y,val;
};

node det;

bool operator<(node a,node b)
{
    return a.val<b.val;
}

priority_queue<node>pq;

main()
{
    int t,p,n,m,i,par[10010],a,c,b,res;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n>>m;
        for(i=1;i<=n;i++)
            par[i]=i;
        for(i=1;i<=m;i++)
        {
            scanf(" %d %d %d",&det.x,&det.y,&det.val);
            pq.push(det);
        }
        res=0;
        while(!pq.empty())
        {
            a=pq.top().x;
            b=pq.top().y;
            c=pq.top().val;
            pq.pop();
            while(par[a]!=a)
                a=par[a];
            while(par[b]!=b)
                b=par[b];
            if(a==b)
                res+=c;
            else
               par[a]=b;
        }
        printf("%d\n",res);
    }
    scanf(" %d",&t);
    return 0;
}
