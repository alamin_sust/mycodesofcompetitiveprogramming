/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

map<pair<pair<ll,ll> , pair<ll,ll> > , ll> mpp;
map<pair<ll,ll> , ll> mppres;
ll t,p,i;

//int _gcd(int a,int b)
//{
//    if(b==0)
//        return a;
//    return _gcd(b,a%b);
//}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    ll a,b,A1,B1,C2,C1,A2,GCD1,GCD2,res,n,gun1,gun2,gun3,D1,c;

    scanf(" %lld",&t);

    for(p=1; p<=t; p++)
    {
        mpp.clear();
        mppres.clear();
        scanf(" %lld",&n);
        res=0;
        for(i=1; i<=n; i++)
        {
            scanf(" %lld %lld %lld",&a,&b,&c);
            if(a<0)
                gun1=-1;
            else gun1=1;
            if(gun1==-1)
            {
                gun1=1;
                if(b>0)
                    gun2=-1;
                else gun2=1;
                if(c>0)
                    gun3=-1;
                else gun3=1;
            }
            else
            {
                if(b<0)
                    gun2=-1;
                else gun2=1;
                if(c<0)
                    gun3=-1;
                else gun3=1;
            }
            a=abs(a);
            b=abs(b);
            c=abs(c);
            GCD1=__gcd(a,b);
            A1=a/GCD1;
            B1=b/GCD1;
            GCD2=__gcd(GCD1,c);
            C1=c/GCD2;
            D1=GCD1/GCD2;
            //gun1=gun2=gun3=1;
            if(mpp[make_pair(make_pair(gun1*A1,gun2*B1),make_pair(gun3*C1,D1))]==0)
            {
                mpp[make_pair(make_pair(gun1*A1,gun2*B1),make_pair(gun3*C1,D1))]=1;
                mppres[make_pair(gun1*A1,gun2*B1)]++;
                res=max(res,mppres[make_pair(gun1*A1,gun2*B1)]);
            }
        }
        printf("%lld\n",res);
    }

    return 0;
}

