#include <vector>
#include <queue>
#include <set>
#include <map>
#include <string>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
#define all(n) (n).begin(),(n).end()
#define rall(n) (n).rbegin(),(n).rend()
#define mp make_pair
#define pb push_back
#define sz size()
#define FS first
#define SS second
#define FO(i,x) for(int i=0;i<x;i++)

#define READ(s) freopen(s, "r", stdin)
#define WRITE(s) freopen(s, "w", stdout)

int a[500010];
int main(){
    int n;
	while(scanf("%d",&n) == 1){
		long long sum = 0;
		for(int i = 0;i < n;i++){
			scanf("%d",&a[i]);
			sum += a[i];
		}
		if(sum % 3 != 0){
			printf("0\n");
			continue;
		}
		long long t1 = sum / 3;
		long long t2 = t1*2;
		sum = 0;
		long long ans = 0;
		int cnt1 = 0, cnt2 = 0;
		for(int i = 0;i < n;i++){
			sum += a[i];
			if(i < n-1 && sum == t2){
				ans += cnt1;
			}
			if(sum == t1)cnt1++;
		}
		printf("%I64d\n",ans);
	}
    return 0;
}
