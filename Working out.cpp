#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int n,m,arr[1010][1010],upleft[1010][1010],upright[1010][1010],downleft[1010][1010],downright[1010][1010],i,j,mx;

main()
{
    cin>>n>>m;
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=m;j++)
        {
            scanf(" %d",&arr[i][j]);
        }
    }
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=m;j++)
        {
            upleft[i][j]=arr[i][j]+max(upleft[i-1][j],upleft[i][j-1]);
        }
    }
    for(i=n;i>=1;i--)
    {
        for(j=m;j>=1;j--)
        {
            downright[i][j]=arr[i][j]+max(downright[i+1][j],downright[i][j+1]);
        }
    }
    for(i=n;i>=1;i--)
    {
        for(j=1;j<=m;j++)
        {
            downleft[i][j]=arr[i][j]+max(downleft[i+1][j],downleft[i][j-1]);
        }
    }
    for(i=1;i<=n;i++)
    {
        for(j=m;j>=1;j--)
        {
            upright[i][j]=arr[i][j]+max(upright[i-1][j],upright[i][j+1]);
        }
    }
    int xx,yy;
    mx=0;
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=m;j++)
        {
            if(mx<upleft[i][j]+upright[i][j]+downleft[i][j]+downright[i][j]-(4*arr[i][j]))
            xx=i,yy=j,mx=max(mx,upleft[i][j]+upright[i][j]+downleft[i][j]+downright[i][j]-(4*arr[i][j]));
        }
    }
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=m;j++)
        {
            printf("%5d ",upright[i][j]);
        }
        printf("\n");
    }
    printf("%d %d%d\n",mx,xx,yy);
    return 0;
}
