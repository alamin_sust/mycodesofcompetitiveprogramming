/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int i,t,p,add,arr[100010],brr[100010],flag,n;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);


    scanf(" %d", &t);

    for(p=1;p<=t;p++) {
        scanf(" %d",&n);
        for(i=1;i<=n;i++) {
            scanf(" %d", &arr[i]);
        }
        for(i=1;i<=n;i++) {
            scanf(" %d", &brr[i]);
        }
        flag=0;
        for(i=1;i<=(n-2);i++) {
            if(arr[i]>brr[i]){
                flag=1;
                break;
            }
            add=brr[i]-arr[i];
            arr[i]+=add;
            arr[i+1]+=(add*2);
            arr[i+2]+=(add*3);
        }
        for(;flag==0&&i<=n;i++) {
            if(arr[i]!=brr[i]) flag=1;
        }
        if(flag) {
            printf("NIE\n");
        } else {
            printf("TAK\n");
        }
    }

    return 0;
}
