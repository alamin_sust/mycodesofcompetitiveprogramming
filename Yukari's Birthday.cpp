/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
int exp,num;
};

ll MX=1000000000000LL,kkkk=0;
map<ll,node>pows;
node det;

void init(void)
{
    for(int i=2;i<=1000000;i++)
    {
        ll now=i+1,pp=i;
        for(int j=1;now<=MX;j++)
        {
            kkkk++;
         //   XXX++;
            if(i==10)
                printf("%d %d %lld\n",i,j,now);
            det.exp=j;
            det.num=i;
            if(pows[now].num==0)
            {
                pows[now].exp=j;
                pows[now].num=i;
            }
            else if(pows[now].num*pows[now].exp>i*j)
            {
                pows[now].exp=j;
                pows[now].num=i;
            }
            else if(pows[now].num*pows[now].exp==i*j&&pows[now].exp<j)
            {
                pows[now].exp=j;
                pows[now].num=i;
            }
            now+=(pp*i);
            pp*=i;
        }
    }
    return;
}

int main()
{
    ll n;
    init();
    //printf("%lld\n",kkkk);
    while(cin>>n)
    {

        if(pows[n].num>0)
        printf("%d %d\n",pows[n].exp,pows[n].num);
        else
        {
           printf("1 %lld\n",n-1);
        }
    }
    return 0;
}



