#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include<string.h>

using namespace std;

long long dp[12][55][55][55],res;
long long fact[] = {1, 1, 2, 6, 24, 120, 720,
    5040, 40320, 362880, 3628800, 39916800, 479001600};

long long rec(int lev,int red,int green,int blue)
{
    if(red<0||green<0||blue<0)
    return 0;
    if(lev==0)
    return 1;
    long long &ret=dp[lev][red][green][blue];
    if(ret!=-1)
    return ret;
    ret=0;
    if(lev%3==0)
        ret+=fact[lev]/(fact[lev/3]*fact[lev/3]*fact[lev/3])*rec(lev-1,red-lev/3,green-lev/3,blue-lev/3);
    if(lev%2==0)
        ret+=fact[lev]/(fact[lev/2]*fact[lev/2])*rec(lev-1,red-lev/2,green-lev/2,blue);
    if(lev%2==0)
        ret+=fact[lev]/(fact[lev/2]*fact[lev/2])*rec(lev-1,red-lev/2,green,blue-lev/2);
    if(lev%2==0)
        ret+=fact[lev]/(fact[lev/2]*fact[lev/2])*rec(lev-1,red,green-lev/2,blue-lev/2);
    //if(red>=lev)
        ret+=rec(lev-1,red-lev,green,blue);
    //if(blue>=lev)
        ret+=rec(lev-1,red,green,blue-lev);
    //if(green>=lev)
        ret+=rec(lev-1,red,green-lev,blue);
    return ret;
}

class ChristmasTree
{
public:
	long long decorationWays(int N, int red, int green, int blue)
	{
	    memset(dp,-1,sizeof(dp));
        return rec(N,red,green,blue);
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	ChristmasTree objectChristmasTree;

	//test case0
	int param00 = 2;
	int param01 = 1;
	int param02 = 1;
	int param03 = 1;
	long long ret0 = objectChristmasTree.decorationWays(param00,param01,param02,param03);
	long long need0 = 6;
	assert_eq(0,ret0,need0);

	//test case1
	int param10 = 2;
	int param11 = 2;
	int param12 = 1;
	int param13 = 0;
	long long ret1 = objectChristmasTree.decorationWays(param10,param11,param12,param13);
	long long need1 = 3;
	assert_eq(1,ret1,need1);

	//test case2
	int param20 = 3;
	int param21 = 2;
	int param22 = 2;
	int param23 = 1;
	long long ret2 = objectChristmasTree.decorationWays(param20,param21,param22,param23);
	long long need2 = 0;
	assert_eq(2,ret2,need2);

	//test case3
	int param30 = 3;
	int param31 = 2;
	int param32 = 2;
	int param33 = 2;
	long long ret3 = objectChristmasTree.decorationWays(param30,param31,param32,param33);
	long long need3 = 36;
	assert_eq(3,ret3,need3);

	//test case4
	int param40 = 8;
	int param41 = 1;
	int param42 = 15;
	int param43 = 20;
	long long ret4 = objectChristmasTree.decorationWays(param40,param41,param42,param43);
	long long need4 = 197121;
	assert_eq(4,ret4,need4);
}
