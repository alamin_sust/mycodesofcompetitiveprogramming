#include<stdio.h>
#include<iostream>
using namespace std;
bool col[110];
long long int arr[110],n;
double res[110];
double gold(long long int pos)
{
    long long int i;
    if(pos >= n)
    return 0;
    if(pos + 1 == n)
    return arr[pos];
    if( col[pos]==1 )
    return res[pos];
    col[pos] = 1;
    double &ret = res[pos];
    double k;
    if(n - pos - 1 >= 6)
    k = 6;
    else
    k = n - pos - 1;
    ret = arr[pos];
    for(i=1;i<7;i++)
    ret = ret + gold(pos + i)/k;
    return ret;
}
main()
{
    double ans;
    long long int t,i,p;
    scanf(" %lld",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %lld",&n);
        for(i=0;i<110;i++)
        col[i] = 0;
        for(i=0; i<n; i++)
        scanf(" %lld",&arr[i]);
        ans=gold(0);
        printf("Case %lld: %.10lf\n",p,ans);
    }
    return 0;
}

