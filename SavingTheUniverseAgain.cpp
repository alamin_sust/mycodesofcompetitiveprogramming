/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

char arr[1010];

ll cum_power() {
    ll ret=0LL;
    ll l=strlen(arr);
    ll power=1LL;
    for(ll i=0LL;i<l;i++) {
        if(arr[i]=='C') {
            power*=2LL;
        } else {
            ret+=power;
        }
    }
    return ret;
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    ll i,t,p,d,flag,steps,tot;

    scanf(" %lld", &t);

    for(p=1LL;p<=t;p++) {
        scanf(" %lld %s",&d,&arr);
        ll l=strlen(arr);
        tot=cum_power();

        steps=0LL;
        flag=1LL;
        while(tot>d&&flag) {
            flag=0LL;
            for(i=l-1LL;i>=1LL;i--) {
                if(arr[i-1]=='C'&&arr[i]=='S') {
                    flag=1LL;
                    arr[i-1]='S';
                    arr[i]='C';
                    tot=cum_power();
                    steps++;
                    break;
                }
            }
        }
        printf("Case #%lld: ",p);
        if(tot<=d) {
            printf("%lld\n",steps);
        } else {
            printf("IMPOSSIBLE\n");
        }
    }

    return 0;
}
