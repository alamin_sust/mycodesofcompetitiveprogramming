/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int dp[1005][2][2][2][2],n;
char arr[3][1010];

int rec(int pos,int up,int down,int lup,int ldown)
{
    //if(val<=3)
    //printf("%d %d %d %d %d %d\n",pos,up,down,lup,ldown,val);
    if(pos==n)
        return 0;
    int &ret=dp[pos][up][down][lup][ldown];
    if(ret!=-1)
        return ret;
    ret=100000010;

    if(arr[0][pos]=='.'&&arr[1][pos]=='.')
    {
        if((arr[0][pos+1]=='.'||up==1)&&(arr[1][pos+1]=='.'||down==1))
            ret=min(ret,rec(pos+1,up,down,1,1));
        if(ldown==0||arr[1][pos+1]=='.')
        ret=min(ret,1+rec(pos+1,1,down,0,ldown));
        if(lup==0||arr[0][pos+1]=='.')
        ret=min(ret,1+rec(pos+1,up,1,lup,0));
        ret=min(ret,2+rec(pos+1,1,1,0,0));
    }
    else if(arr[0][pos]=='.'&&arr[1][pos]=='X')
    {
        if(arr[0][pos+1]=='.'||up==1)
            ret=min(ret,rec(pos+1,up,0,1,0));
        ret=min(ret,1+rec(pos+1,1,0,0,0));
    }
    else if(arr[0][pos]=='X'&&arr[1][pos]=='.')
    {
        if(arr[1][pos+1]=='.'||down==1)
            ret=min(ret,rec(pos+1,0,down,0,1));
        ret=min(ret,1+rec(pos+1,0,1,0,0));
    }
    else
    {
        ret=min(ret,rec(pos+1,0,0,0,0));
    }

    return ret;
}

int main()
{
    freopen("high_security.txt","r",stdin);
    freopen("high_security_output.txt","w",stdout);
    int t,p;
    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        scanf(" %s",&arr[0]);
        scanf(" %s",&arr[1]);
        arr[0][n]='X';
        arr[1][n]='X';
        memset(dp,-1,sizeof(dp));
        printf("Case #%d: %d\n",p,rec(0,0,0,0,0));
    }

    return 0;
}


