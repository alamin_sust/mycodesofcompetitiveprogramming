#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class JustifyText
{
public:
	vector <string> format(vector <string> text)
	{
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

void main( int argc, char* argv[] )
{

	JustifyText objectJustifyText;

	//test case0
	vector <string> param00;
	param00.push_back("BOB");
	param00.push_back("TOMMY");
	param00.push_back("JIM");
	vector <string> ret0 = objectJustifyText.format(param00);
	vector <string> need0;
	need0.push_back("  BOB");
	need0.push_back("TOMMY");
	need0.push_back("  JIM");
	assert_eq(0,ret0,need0);

	//test case1
	vector <string> param10;
	param10.push_back("JOHN");
	param10.push_back("JAKE");
	param10.push_back("ALAN");
	param10.push_back("BLUE");
	vector <string> ret1 = objectJustifyText.format(param10);
	vector <string> need1;
	need1.push_back("JOHN");
	need1.push_back("JAKE");
	need1.push_back("ALAN");
	need1.push_back("BLUE");
	assert_eq(1,ret1,need1);

	//test case2
	vector <string> param20;
	param20.push_back("LONGEST");
	param20.push_back("A");
	param20.push_back("LONGER");
	param20.push_back("SHORT");
	vector <string> ret2 = objectJustifyText.format(param20);
	vector <string> need2;
	need2.push_back("LONGEST");
	need2.push_back("      A");
	need2.push_back(" LONGER");
	need2.push_back("  SHORT");
	assert_eq(2,ret2,need2);
}
