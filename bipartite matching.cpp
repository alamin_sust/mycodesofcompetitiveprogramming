#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
using namespace std;

vector <int> adj[2010];
int n,m,i,j,f,t,color[2010],flag,p=1,tot,jj;


int bfs(int i)
{
    queue<int>q;
    q.push(i);
    color[i]=1;
    while(!q.empty())
    {
        int process=q.front();
        q.pop();
        for(int j=0;j<adj[process].size();j++)
        {
            int to=adj[process][j];
            if(color[to]==0)
                {
                    color[to]=3-color[process];
                    q.push(to);
                }
            else if(color[process]==color[to])
                return true;
        }
    }
    return false;
}

main()
{
    cin>>tot;
    for(p=1;p<=tot;p++)
    {
        cin>>n>>m;
        for(i=0;i<=n;i++)
            adj[i].clear();
        memset(color,0,sizeof(color));
        for(i=1;i<=m;i++)
            {scanf(" %d %d",&f,&t);
            adj[f].push_back(t);
            adj[t].push_back(f);}
            flag=0;
        for(i=1;i<=n;i++)
        {
            if(color[i]==0)
            {
                if(bfs(i))
                    {flag=1;
                    break;}
            }
        }
        printf("Scenario #%d:\n",p);
        if(flag==0)
            printf("No suspicious bugs found!\n");
        else
            printf("Suspicious bugs found!\n");
    }
    return 0;
}

