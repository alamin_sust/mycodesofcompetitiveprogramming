#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
#define ull long long unsigned
using namespace std;

ull res[4][4],mat[50][4][4];

void calc(ull power)
{
    ull temp[4][4];
    for(ull i=1;i<=2;i++)
    {
        for(ull j=1;j<=2;j++)
        {
             temp[i][j]=res[i][j];
        }
    }
    for(ull i=1;i<=2;i++)
    {
        for(ull j=1;j<=2;j++)
        {
            res[i][j]=0;
            for(ull k=1;k<=2;k++)
            {
                res[i][j]+=(temp[i][k]*mat[power][k][j]);

            }
        }
    }
    return;
}

void bigmod(ull power)
{
    for(ull i=1;i<=2;i++)
    {
        for(ull j=1;j<=2;j++)
        {
            mat[power][i][j]=0;
            for(ull k=1;k<=2;k++)
            {
                mat[power][i][j]+=(mat[power-1][i][k]*mat[power-1][k][j]);
                //mat[power][i][j]%=100000;
            }
        }
    }
    return;
}

main()
{
    ull i,x,p,t,q,a,b,n,m;
    //for(MOD=1,i=1;i<=63;i++)
      //  MOD*=2;
    //printf("%llu\n",MOD);
    cin>>t;
    //for(ull i=1;i<=2;i++)
      //  for(ull j=1;j<=2;j++)
        //printf("%llu ",mat[10][i][j]);
    for(x=1;x<=t;x++)
    {
        cin>>p>>q>>n;
        if(n==0)
        {
            printf("Case %llu: 2\n",x);
            continue;
        }
        if(n==1)
        {
            printf("Case %llu: %llu\n",x,p);
            continue;
        }
        if(p==0&&q==0)
        {
            printf("Case %llu: 0\n",x);
            continue;
        }
        if(p==0&&q==1)
        {
            if(n%2==0)
                printf("Case %llu: 2\n",x);
            if(n%2==1)
                printf("Case %llu: -2\n",x);
            continue;
        }
        mat[0][1][1]=p;
        mat[0][1][2]=-q;
        mat[0][2][1]=1;
        mat[0][2][2]=0;
        for(ull i=1;i<=32;i++)
        bigmod(i);
        //printf("%llu\n",MOD);
        ull k=0;
        ull fg=0;
        n--;
        while((n>>k)|0)
        {
            //printf(".");
            if(((n>>k)&1)==1)
                {
                    if(fg==0)
                    {
                        fg=1;
                        for(ull i=1;i<=2;i++)
                            for(ull j=1;j<=2;j++)
                                res[i][j]=mat[k][i][j];
                    }
                    else
                    calc(k);
                }
            k++;
        }
       ull result=(((res[1][1]*p))+((res[1][2]*2)));
        printf("Case %llu: %llu\n",x,result);
    }
    return 0;
}
