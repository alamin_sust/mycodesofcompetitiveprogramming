#include<stdio.h>
#include<string.h>
char arr[100];
void inorder(int n)
{
    if(arr[n]!='\0')
    {
        inorder(n*2);
        printf("%c ",arr[n]);
        inorder((n*2)+1);
    }
}

void preorder(int n)
{
    if(arr[n]!='\0')
    {

        printf("%c ",arr[n]);
        preorder(n*2);
        preorder((n*2)+1);
    }
}

void postorder(int n)
{
    if(arr[n]!='\0')
    {
        postorder(n*2);
        postorder((n*2)+1);
        printf("%c ",arr[n]);
    }
}

main()
{
    int n,i;
    memset(arr,'\0',sizeof(arr));
    gets(arr);
    n=strlen(arr);
    for(i=n;i>0;i--)
    {
        arr[i]=arr[i-1];
        if(arr[i]=='0')
        arr[i]='\0';
    }
    preorder(1);
    printf("\n");
    inorder(1);
    printf("\n");
    postorder(1);
    printf("\n");
    return 0;
}
