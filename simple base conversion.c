#include<stdio.h>
#include<string.h>
#include<math.h>
#include<stdlib.h>

char table[] = "0123456789ABCDEF";
long hex_to_dec(char num[])
{
   long number = 0;
   long i,j;
   long len;
   len = strlen(num);
   for(i = len-1; i>=0; i--)
   {
      for(j = 0;j<16;j++)
      {
         if(table[j]==num[i])
            break;
      }
      number +=j*pow(16,len-i-1);
   }
   return number;
}

char *dec_to_hex(long num)
{
   char number[20];
   long temp;
   long i,j,k;
   i = 0;
   while(num > 0)
   {
      temp = num%16;
      num /= 16;
      number[i++] = table[temp];
   }
   number[i] = '\0';
   char m[15];
   k = strlen(number);
   for(i = k-1,j=0;i>=0;i--,j++)
      m[j] = number[i];
   m[j] = '\0';
   return m;
}

int main()
{
   char *num = new char num[20];
   char temp[16];
   int i,j,k;
   long number;

   while(gets(num)!=NULL)
   {
      if(num[0]=='-')
         break;
      if(num[0]=='0'&&num[1]=='x')
      {
         k = strlen(num);
         for(i=2,j=0;i<k;i++,j++)
            temp[j] = num[i];
         temp[j] = '\0';
         number = hex_to_dec(temp);
         printf("%ld\n",number);
      }
      else
      {
         number = atol(num);
         num = dec_to_hex(number);
         printf("0x%s\n",num);
      }
   }
   return 0;
}
