#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ull arr[1000010];

ull lcm_gen(ull a,ull b)
{
    return a*b/__gcd(a,b);
}

ull lcm(ull f,ull t)
{
    if(f==t)
        return arr[f];
    ull m=(f+t)/2;
    return lcm_gen(lcm(f,m),lcm(m+1,t));
}

main()
{
    ull t,p,n,i;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %llu",&n);
        for(i=1;i<=n;i++)
        {
            scanf(" %llu",&arr[i]);
        }
        printf("Case %llu: %llu\n",p,lcm(1,n));
    }
    return 0;
}

