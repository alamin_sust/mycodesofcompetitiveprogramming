#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#define pi 2*acos(0)
#define inf INT_MAX
#define eps 0.00000001
using namespace std;
int dp[4][4010][4010],res,curr,sum,a[10],n;

int rec(int i,int amount,int e)
{
    if(i==3||amount>=n)
    {
        if(amount==n)
        return e;
        else
        return 0;
    }
    if(dp[i][amount][e]!=-1)
    return dp[i][amount][e];
    int ret1=0,ret2=0;
    if(amount+a[i]<=n)
    ret1=rec(i,amount+a[i],e+1);
    ret2=rec(i+1,amount,e);
    return dp[i][amount][e]=max(ret1,ret2);
}

main()
{
    memset(dp,-1,sizeof(dp));
    scanf(" %d %d %d %d",&n,&a[0],&a[1],&a[2]);
    sort(a,a+3);
    res=rec(0,0,0);
    printf("%d\n",res);
    return 0;
}
