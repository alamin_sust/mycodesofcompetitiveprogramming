/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

long long int prime[1000100], countPrime=0LL;
ll mark[1000000];

bool is_prime(ll a,ll b)
{
    long long int c;
    long long int i, j, n, cnt=0LL, limit;

    memset( mark, 0LL, sizeof(mark));
    n=sqrt(b)+1LL;
    limit=sqrt(n);
    prime[countPrime++]=2LL;
    for( i=4LL; i<=n; i+=2LL )
        mark[i]=1LL;

    for( i=3LL; i<=n; i+=2LL )
    {
        if( mark[i]==0LL )
        {
            prime[countPrime++]=i;
            if( i<=limit )
                for( j=i*i; j<=n; j+=(2LL*i) )
                    mark[j]=1LL;
        }
    }

    long long int sz=b-a+1LL;
    long long int arr[sz+100];
    long long int k, store=a, m=0LL;

    for( k=0LL; k<sz; k++ ) arr[k]=store++;
    for( i=0LL; i<countPrime; i++ )
    {
        long long int start=(ceil((double)a/prime[i]))*prime[i];
        long long int en=(floor((double)b/prime[i]))*prime[i];
        for( j=start; j<=en; j+=prime[i] )
            {
                if(arr[j-a]!=1LL&&prime[i]!=j)
                arr[j-a]=1LL;
            }
    }
    if(arr[0]!=1LL)
        return true;

    return false;
}


int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    ll n;

    while(scanf("%I64d",&n)==1){

    if(n<=2LL)
    {
        printf("1\n");
        //return 0;
    }
    else if((n%2LL)==0LL)
    {
        printf("2\n");
        //return 0;
    }

    else if(is_prime(n,n))
    {
        printf("1\n");
        //return 0;
    }
    else if(is_prime(n-2LL,n-2LL))
    {
        printf("2\n");
        //return 0;
    }
    else printf("3\n");

    }




    return 0;
}






