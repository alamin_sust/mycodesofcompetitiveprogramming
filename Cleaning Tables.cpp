/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node
{
    int x,val;
};

bool operator<(node a,node b)
{
    return a.val<b.val;
}

queue<int>q[410];
priority_queue<node>que;
node det;

int customer,h,n,k,flag[1010],arr[1010],i,res,p,t;

main()
{
    cin>>t;
    for(p=1; p<=t; p++)
    {
        res=0;
        cin>>n>>customer;
        for(i=1; i<=customer; i++)
        {
            scanf(" %d",&arr[i]);
            q[arr[i]].push(i);
        }
        for(i=1; i<=customer; i++)
        {
            det.x=arr[i];
            q[arr[i]].pop();
            if(!q[arr[i]].empty())
                det.val=q[arr[i]].front();
            else
                det.val=9999999;
            if(flag[arr[i]]==0)
            {
                flag[arr[i]]=1;
                if(que.size()==(n+h))
                    flag[que.top().x]=0,que.pop();
                res++;
                que.push(det);
            }
            else
            {
                h++;
                que.push(det);
            }

        }
        h=0;
        memset(flag,0,sizeof(flag));
        while(!que.empty())
            que.pop();
        for(i=1; i<=customer; i++)
        {
            while(!q[i].empty())
                q[i].pop();
        }
        printf("%d\n",res);
    }
    return 0;
}


