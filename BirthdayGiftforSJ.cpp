/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int cnt,k,arr[110],a,b,dp[35][1010],cum[1010];
map<int,int>mpp;

void rec(int pos,int val)
{
    if(pos==cnt)
        return;
    int &ret=dp[pos][val];
    if(ret!=-1)
        return;
    mpp[val]=1;
    ret=0;
    for(int i=0;(i+val)<=1000;i+=arr[pos])
    {
        rec(pos+1,i);
    }
    return;
}

int main()
{
    int p,i,j,t;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    cnt=0;
    for(i=2;i<=1000;i++)
    {
        for(j=2,k=i*i;k<=1000&&j<=1000;j++)
        {
            if(mpp[k]==0)
            mpp[k]=1,arr[cnt++]=k;
        }
    }
   // printf("%d\n",cnt);

    memset(dp,-1,sizeof(dp));
    rec(0,0);
    mpp[0]=0;
    for(i=1;i<=1000;i++)
    {
        cum[i]=cum[i-1]+mpp[i];
        if(mpp[i]==0)
        printf("%d ",i);
    }
    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
       scanf(" %d %d",&a,&b);
       printf("%d\n",(cum[b]-cum[a-1]));
    }
    return 0;
}





