/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int dp[104][104][104],n,arr[104],b,odd[104],even[104];

int rec(int pos,int cut,int cost)
{

    if(pos == n-1)
    {
        return dp[pos][cut][cost] = cut;
    }
    int &ret = dp[pos][cut][cost];
    if(ret!=-1)
        return ret;


    if(odd[pos]==even[pos])
    {
        if((cost+abs(arr[pos]-arr[pos+1]))<=b)
            ret = max(rec(pos+1,cut+1,(cost+abs(arr[pos]-arr[pos+1]))), rec(pos+1,cut,cost));
        else
            ret = rec(pos+1,cut,cost);
    }
    else
    {
        ret = rec(pos+1,cut,cost);
    }

    return ret;
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d %d",&n,&b);


    for(int i=0; i<n; i++)
    {
        scanf(" %d",&arr[i]);
        if(i>0)
        {
            odd[i] = odd[i-1] + (arr[i]%2);
            even[i] = even[i-1] + ((arr[i]%2)==0?1:0);
        }
        else
        {
            odd[0] = arr[i]%2;
            even[0] = (arr[i]%2)==0?1:0;
        }
    }

    memset(dp,-1,sizeof(dp));

    printf("%d\n",rec(0,0,0));

    return 0;
}
