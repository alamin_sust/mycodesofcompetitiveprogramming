/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll t,p,i,res,rem,n,k,sum,add,j,M=1000000007LL;

ll func(ll val) {
    return (val*val-val)%M;
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %lld", &t);

    for(p=1LL;p<=t;p++) {
        scanf(" %lld %lld",&n,&k);
        sum = ((k*(k+1LL))/2LL);
        if(sum > n) {
          printf("-1\n");
          continue;
        }
        add = (n-sum)/k;
        rem = (n-sum)%k;

        res=1LL;
        for(i=k,j=rem;i>=1LL;j--,i--) {
            res = (res*func(i+add+(j>0LL?1LL:0LL)))%M;
        }
        printf("%lld\n",res);
    }

    return 0;
}
