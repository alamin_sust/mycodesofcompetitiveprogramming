/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node
{
    int x,y,parx,pary;
};
node det,v;
int xb,yb,xe,ye,r,c,stat[110][110],vis,reachable,multi,code;
char arr[110][110];
queue<node>q;

int getcode(char ch)
{
    if(ch>='A')
        return ch-'A'+10;
    else return ch-'0';
}

void bfs(void)
{
    while(!q.empty())
        q.pop();
    memset(stat,0,sizeof(stat));
    det.x=xb;
    det.y=yb;
    det.parx=-1;
    det.pary=-1;
    stat[xb][yb]=1;
    q.push(det);
    vis=1;
    multi=0;
    reachable=0;

    while(!q.empty())
    {
        node u=q.front();
        q.pop();
        code=getcode(arr[u.x][u.y]);
        if(((1<<0)&code)==0)
        {
            v.x=u.x;
            v.y=u.y-1;
        //    printf("%d %d........\n",v.x,v.y);
            if(v.x>=0&&v.y>=0&&v.x<r&&v.y<c){
            if((!(v.x==u.parx&&v.y==u.pary))&& stat[v.x][v.y])
            {
                multi=1;
            }
            if(stat[v.x][v.y]==0)
            {
                stat[v.x][v.y]=1;
                v.parx=u.x;
                v.pary=u.y;
                q.push(v);
                if(v.x==xe&&v.y==ye)
                    reachable=1;
                vis++;
            }}
        }
        if(((1<<1)&code)==0)
        {
            v.x=u.x+1;
            v.y=u.y;
          //   printf("%d %d........\n",v.x,v.y);
            if(v.x>=0&&v.y>=0&&v.x<r&&v.y<c){
            if((!(v.x==u.parx&&v.y==u.pary))&& stat[v.x][v.y])
            {
                multi=1;
            }
            if(stat[v.x][v.y]==0)
            {
                stat[v.x][v.y]=1;
                v.parx=u.x;
                v.pary=u.y;
                q.push(v);
                if(v.x==xe&&v.y==ye)
                    reachable=1;
                vis++;
            }}
        }
        if(((1<<2)&code)==0)
        {
            v.x=u.x;
            v.y=u.y+1;
            //          printf("%d %d........\n",v.x,v.y);
            if(v.x>=0&&v.y>=0&&v.x<r&&v.y<c)
            {if((!(v.x==u.parx&&v.y==u.pary))&& stat[v.x][v.y])
            {
                multi=1;
            }
            if(stat[v.x][v.y]==0)
            {
                stat[v.x][v.y]=1;
                v.parx=u.x;
                v.pary=u.y;
                q.push(v);
                if(v.x==xe&&v.y==ye)
                    reachable=1;
                vis++;
            }}
        }
        if(((1<<3)&code)==0)
        {
            v.x=u.x-1;
            v.y=u.y;
                        //printf("%d %d........\n",v.x,v.y);
            if(v.x>=0&&v.y>=0&&v.x<r&&v.y<c)
            {if((!(v.x==u.parx&&v.y==u.pary))&& stat[v.x][v.y])
            {
                multi=1;
            }
            if(stat[v.x][v.y]==0)
            {
                stat[v.x][v.y]=1;
                v.parx=u.x;
                v.pary=u.y;
                q.push(v);
                if(v.x==xe&&v.y==ye)
                    reachable=1;
                vis++;
            }
            }
        }

    }
    if(r*c!=vis)
        vis=0;
    else vis=1;
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    int i,j;

    while(scanf("%d%d",&r,&c)==2&&(r+c))
    {
        xb=-1;
        for(i=0; i<r; i++)
        {
            scanf(" %s",&arr[i]);
            for(j=0; j<c; j++)
            {
                code=getcode(arr[i][j]);
                if((i==0&&((1<<3)&code)==0) || (i==r-1&&((1<<1)&code)==0) || (j==0&&((1<<0)&code)==0) || (j==c-1&&((1<<2)&code)==0))
                {
                    if(xb==-1)
                    {
                        xb=i;
                        yb=j;
                    }
                    else
                    {
                        xe=i;
                        ye=j;
                    }
                }
            }
        }
        //printf("%d %d..%d %d\n",xb,yb,xe,ye);

        bfs();

        if(!reachable)
            printf("NO SOLUTION\n");
        else if(!vis)
            printf("UNREACHABLE CELL\n");
        else if(multi)
            printf("MULTIPLE PATHS\n");
        else
            printf("MAZE OK\n");

    }

    return 0;
}


