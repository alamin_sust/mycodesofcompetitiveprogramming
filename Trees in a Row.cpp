#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

long long n,k,ii,i,mx,p,j,flag,arr[10010],fg[10010],res;

main()
{
    cin>>n>>k;
    for(i=1;i<=n;i++)
        scanf(" %I64d",&arr[i]);
    res=arr[1];
    mx=9999999;
    for(i=1;i<=n;i++)
    {
        ii=arr[i]-(k*(i-1));
        if(ii<=0)
            continue;
        for(j=1,p=ii;j<=n;p+=k,j++)
        {
            if(arr[j]!=p)
                fg[i]++;
        }
        if(fg[i]<mx)
        {
            mx=fg[i];
            res=ii;
        }
    }
    printf("%I64d\n",mx);
    for(j=res,i=1;i<=n;j+=k,i++)
    {
        if(arr[i]<j)
        {
            printf("+ %I64d %I64d\n",i,j-arr[i]);
        }
        else if(arr[i]>j)
        {
            printf("- %I64d %I64d\n",i,arr[i]-j);
        }

    }
    return 0;
}

