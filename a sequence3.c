#include<stdio.h>
#include<memory.h>

int main()

{
    int i,j,k,d,sum,a,b,c,rem,r,flag,ncase=1;
    int array[31000];
    int seque[31],duplicate[31],max;


    while(scanf("%d",&d)!=EOF)

    {
        memset(duplicate,0,sizeof(duplicate));
         flag=0;
        for(max=0,i=0; i<d; i++)
        {
            scanf(" %d",&seque[i]);
            if(duplicate[i]==1||seque[i]<max)
            flag=1;
            duplicate[i]=1;
            max=seque[i];

        }

       if(flag==0)
       {
        memset(array,0,sizeof(array));


        for(i=0; i<d-1; i++)

        {
            sum=seque[i];
            for(j=i+1; j<d; j++)

            {
                a=seque[i];
                b=seque[j];
                c=a+b;

                array[c]=1;

                sum=sum+b;
                array[sum]=1;

                for(k=i+1; k<j; k++)

                {
                    rem=seque[k];

                    array[sum-rem]=1;
                }

            }
        }
        for(i=1; i<d; i++)

        {
            r=seque[i];

            if(array[r]==1)
                flag=1;

        }
    }

        printf("Case #%d:",ncase);

        for(i=0; i<d; i++)
        {
            printf(" %d",seque[i]);

        }


        if(flag==1)
            printf("\nThis is not an A-sequence.\n");

        else

            printf("\nThis is an A-sequence.\n");

        ncase++;

    }
    return 0;
}
