/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int dp[1000010][7],n;

int rec(int pos,int num)
{
    if(pos==0)
        return 1;
    int &ret=dp[pos][num];
    if(ret!=-1)
        return ret;
    ret=0LL;
    for(int i=num;i<7;i++)
    {
        ret=(ret+rec(pos-1,i))%1000000007;
    }
    return ret%1000000007;
}

main()
{
    memset(dp,-1,sizeof(dp));
    cin>>n;
    if(n<=12)
    cout<<"0"<<endl;
    else
    cout<<rec((n-13)/2,0)<<endl;
    return 0;
}

