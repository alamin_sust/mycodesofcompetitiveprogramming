/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node{
int penalty,solve,contest;
char name[110];
};

node arr[110];
int k;
map<string,int>mpp;
char name[110];
int penalty,solve;

int comp(node a,node b)
{
    if(a.solve==b.solve&&a.penalty==b.penalty&&a.contest<b.contest)
        return strcmp(a.name,b.name);
    if(a.solve==b.solve&&a.penalty==b.penalty)
        return a.contest<b.contest;
    if(a.solve==b.solve)
        return a.penalty<b.penalty;
    return a.solve>b.solve;
}

int main()
{
    freopen("tfc_res.txt","r",stdin);
    freopen("tfc_rank.txt","w",stdout);
    k=1;

    while(scanf("%s",name)!=EOF)
    {
        if(mpp[name]==0)
        {
            mpp[name]=k++;
        }
        scanf(" %d %d",&solve,&penalty);
        strcpy(arr[mpp[name]].name,name);
        arr[mpp[name]].penalty+=penalty;
        arr[mpp[name]].solve+=solve;
        arr[mpp[name]].contest++;
    }

    sort(arr+1,arr+k,comp);
    printf("Combined Ranklist of Team Forming Contest-1,2,3\n\n");
    printf("rank            name  contests solve penalty\n");
    for(int i=1;i<k;i++)
    {
        printf("%3d. %15s %9d %5d %7d\n",i,arr[i].name,arr[i].contest,arr[i].solve,arr[i].penalty);
    }
    return 0;
}





