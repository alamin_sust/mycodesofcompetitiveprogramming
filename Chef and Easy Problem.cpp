/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int n,q,i,j,arr[100010],cum[100010][31],res,a,b;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d %d",&n, &q);

    for(i=1;i<=n;i++) {
        scanf(" %d",&arr[i]);
        for(j=0;j<31;j++) {
            cum[i][j]=cum[i-1][j]+(((1<<j)&arr[i])!=0);
        }
    }


    for(j=1;j<=q;j++) {
    scanf(" %d %d",&a,&b);
    res=0;
    for(i=30;i>=0;i--) {
        res<<=1;
        if(((cum[b][i]-cum[a-1][i])*2)<(b-a+1)) {
            res++;
        }
    }

    printf("%d\n",res);
    }

    return 0;
}
