/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

vector<ll>adj[100010];
ll cnt[100010],arr[100010],res;

ll dfs(ll node)
{
    if(adj[node].size()==0LL)
        return arr[node]=1LL;
    ll &ret=arr[node];
    if(ret)
    return ret;
    for(ll i=0LL;i<adj[node].size();i++)
    {
        ret=max(ret,1LL+dfs(adj[node][i]));
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    ll n,m,u,v,i;

    scanf(" %I64d %I64d",&n,&m);

    for(i=1LL;i<=m;i++)
    {
        scanf(" %I64d %I64d",&u,&v);
        if(u>v)
            adj[u].push_back(v);
        else
            adj[v].push_back(u);
        cnt[u]++;
        cnt[v]++;

    }

    for(i=n;i>=1LL;i--)
    {
        if(arr[i]==0LL)
        dfs(i);
    }

    for(i=1LL;i<=n;i++)
    {
        ll tp=arr[i]*cnt[i];
        if(tp>res)res=tp;
    }
    printf("%I64d\n",res);
    return 0;
}

