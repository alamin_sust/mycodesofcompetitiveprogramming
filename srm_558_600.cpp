#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <string.h>

using namespace std;

int kk=50,stamp,len,scost,pcost,dp[55][55],arr[55][55];
string str;

int get(int f,int t)
{
    if(arr[f][t]!=-1)
        return arr[f][t];
    arr[f][t]=0;
    char prev='*';
    int flag=0;
    for(int i=f;i<=t;i++)
    {
        if(prev!='*'&&prev!=str[i]&&str[i]!='*')
            {flag=1;
            break;}
        if(str[i]!='*')
            prev=str[i];
    }
    return arr[f][t]=flag;
}

int rec(int f,int t)
{
    if(t==len)
    {
     return 0;
    }
    int &ret=dp[f][t];
    if(ret!=-1)
        return ret;
    ret=99999999;
    int i=t+1;
    //for(int i=t+1;i<=len;i++)
    for(int j=i;(j+stamp-1)<=len;j++)
    if(get(i-1,j+stamp-2)==0)
    ret=min(ret,(rec(i,j+stamp-1)+pcost*((j+stamp-i)/stamp+(((j+stamp-i)%stamp)==0?0:1))));
    return ret;
}

class Stamp
{
public:
	int getMinimumCost(string desiredColor, int stampCost, int pushCost)
	{
	    int res=99999999;
	    len=desiredColor.length();
	    memset(arr,-1,sizeof(arr));
	    str=desiredColor;
	    //cout<<str<<endl;
	    scost=stampCost;
	    pcost=pushCost;
	    for(int i=1;i<=len;i++)
        {
            stamp=i;
            memset(dp,-1,sizeof(dp));
            res=min(res,rec(0,0)+i*stampCost);
        }
        return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	Stamp objectStamp;

	//test case0
	string param00 = "RRGGBB";
	int param01 = 1;
	int param02 = 1;
	int ret0 = objectStamp.getMinimumCost(param00,param01,param02);
	int need0 = 5;
	assert_eq(0,ret0,need0);

	//test case1
	string param10 = "R**GB*";
	int param11 = 1;
	int param12 = 1;
	int ret1 = objectStamp.getMinimumCost(param10,param11,param12);
	int need1 = 5;
	assert_eq(1,ret1,need1);

	//test case2
	string param20 = "BRRB";
	int param21 = 2;
	int param22 = 7;
	int ret2 = objectStamp.getMinimumCost(param20,param21,param22);
	int need2 = 30;
	assert_eq(2,ret2,need2);

	//test case3
	string param30 = "R*RR*GG";
	int param31 = 10;
	int param32 = 58;
	int ret3 = objectStamp.getMinimumCost(param30,param31,param32);
	int need3 = 204;
	assert_eq(3,ret3,need3);

	//test case4
	string param40 = "*B**B**B*BB*G*BBB**B**B*";
	int param41 = 5;
	int param42 = 2;
	int ret4 = objectStamp.getMinimumCost(param40,param41,param42);
	int need4 = 33;
	assert_eq(4,ret4,need4);

	//test case5
	string param50 = "*R*RG*G*GR*RGG*G*GGR***RR*GG";
	int param51 = 7;
	int param52 = 1;
	int ret5 = objectStamp.getMinimumCost(param50,param51,param52);
	int need5 = 30;
	assert_eq(5,ret5,need5);
}
