#include <iostream>
#include <cstring>
#include <string>

using namespace std;

char b[30][30];
string a;
int maxtmp;

void check(int i, int j)
{
   if (b[i][j] == '1')
   {
      b[i][j] = '2';
      for (int t = i - 1 ; t <= i + 1 ; t++)
         for (int k = j - 1 ; k <= j + 1 ; k++)
            if (b[t][k] == '1')
            {
               maxtmp++;
               check(t, k);
            }
   }
}

int main()
{
   int n, max;
   cin >> n;
   for (int i = 1 ; i <= n ; i++)
   {
      memset(b, 0, sizeof b);
      max = maxtmp = 0;
      cin >> a;
      for (int j = 1 ; j <= a.size() ; j++)
         b[1][j] = a[j - 1];
      for (int j = 2 ; j <= a.size() ; j++)
         for (int k = 1 ; k <= a.size() ; k++)
            cin >> b[j][k];
      for (int j = 1 ; j <= a.size() ; j++)
      {
         for (int k = 1 ; k <= a.size() ; k++)
         {
            if (b[j][k] == '1')
            {
               maxtmp = 1;
               check(j, k);
            }
            if (maxtmp > max && maxtmp != 1)
               max = maxtmp;
         }
      }
      cout << max << "\n";
      if (i != n)
         cout << "\n";
   }
   return 0;
}
