#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#include<set>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

int low[10010],d[10010],tme=0,col[10010],par[10010];
vector<int>adj[10010];
set<int>art_point;

void dfs(int u)
{
    int cnt=0;
    tme++;
    d[u]=low[u]=tme;
    col[u]=1;
    for(int i=0;i<adj[u].size();i++)
    {
       int v=adj[u][i];
        if(col[v]==0)
        {
            par[v]=u;
            //col[v]=1;
            cnt++;
            dfs(v);
            if(low[v]>=d[u]&&u!=1)
                art_point.insert(u);
            low[u]=min(low[u],low[v]);
        }
        else if(par[u]!=v)
        {
            low[u]=min(low[u],d[v]);
        }
    }
    if(cnt>1&&u==1)
        art_point.insert(u);
    return;
}

main()
{
    int t,p,n,m,i,from,to;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n>>m;
        for(i=0;i<=n;i++)
            adj[i].clear();
        memset(col,0,sizeof(col));
        memset(par,0,sizeof(par));
        memset(d,0,sizeof(d));
        memset(low,0,sizeof(low));
        for(i=1;i<=m;i++)
        {
            scanf(" %d %d",&from,&to);
            adj[from].push_back(to);
            adj[to].push_back(from);
        }
        tme=0;
        dfs(1);
        printf("Case %d: %d\n",p,art_point.size());
        art_point.clear();
    }
    return 0;
}

