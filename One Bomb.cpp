/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int n,m,row,col,k,kk,kk2,i,j,bx[1000010],by[1000010],rmp[1010],cmp[1010];
char arr[1010][1010];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    scanf(" %d %d",&n,&m);
    getchar();
    row=col=0;
    for(i=1; i<=n; i++)
    {
        for(j=1; j<=m; j++)
        {
            scanf("%c",&arr[i][j]);
            if(arr[i][j]=='*')
            {
                bx[k]=i;
                by[k]=j;
                k++;
                if(rmp[i])
                {
                    row=i;
                }
                if(cmp[j])
                {
                    col=j;
                }
                rmp[i]=1;
                cmp[j]=1;
            }
        }
        getchar();
    }

    if(k==0)
    {
    printf("YES\n");
    printf("1 1\n");
    return 0;
    }

    if(k==1)
    {
        printf("YES\n");
    printf("%d %d\n",bx[0],by[0]);
    return 0;

    }

    if(k==2&&row>0)
    {
        printf("YES\n");
    printf("%d 1\n",row);
    return 0;
    }
    if(k==2&&col>0)
    {
        printf("YES\n");
    printf("1 %d\n",col);
    return 0;
    }

    if(k==2)
    {
    printf("YES\n");
    printf("%d %d\n",bx[0],by[1]);
    return 0;
    }

    if(row==0&&col==0)
    {
        printf("NO\n");
        return 0;
    }

    if(row)
    {
        kk=0;
        for(i=1;i<=m;i++)
        {
            if(arr[row][i]=='*')
                kk++;
        }

        for(i=1;i<=m;i++)
        {
            kk2=0;
            if(arr[row][i]=='*')
            kk2=-1;
            for(j=1;j<=n;j++)
            {
                if(arr[j][i]=='*')
                    kk2++;
            }
            if((kk+kk2)==k){
                printf("YES\n");
                printf("%d %d\n",row,i);
                return 0;
            }
        }

    }

    if(col)
    {
        kk=0;
        for(i=1;i<=n;i++)
        {
            if(arr[i][col]=='*')
                kk++;
        }

        for(i=1;i<=n;i++)
        {
            kk2=0;
            if(arr[i][col]=='*')
            kk2=-1;
            for(j=1;j<=m;j++)
            {
                if(arr[i][j]=='*')
                    kk2++;
            }
            if((kk+kk2)==k){
                printf("YES\n");
                printf("%d %d\n",i,col);
                return 0;
            }
        }

    }

    printf("NO\n");



    return 0;
}






