/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int res,p,t,i,n,in,curr,mpp[1000010],ind,arr[1000010],q,a,b,tp;
map<int,int>mpp_tp;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&n);

    curr=1;
    for(i=1;i<=n;i++) {
        scanf(" %d",&in);
        if(mpp_tp[in]==0) {
            ind=curr++;
            mpp_tp[in]=ind;
        }
        ind=mpp_tp[in];
        arr[i]=ind;
    }

    scanf(" %d",&q);

    for(p=1;p<=q;p++) {

        scanf(" %d %d",&a,&b);
        memset(mpp,0,sizeof(mpp));
        res=0;
        for(i=a;i<=b;i++) {
            tp=mpp[arr[i]];
            if(tp==0) {
                res++;
                mpp[arr[i]]=1;
            } else if(tp==1) {
                res--;
                mpp[arr[i]]=2;
            }
        }
        printf("%d\n",res);
    }
    return 0;
}
