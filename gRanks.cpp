/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

vector<ll>res[10010];
ll t,p,P,cnt,i,prize[10010],m,n,w,j,rnk;
string arr,arr2[10010];
map<string,ll>mpp;

struct node{
ll s;
string nm;
};

node score[10010];

int comp(node a,node b)
{
    if(a.s==b.s)
        return a.nm<b.nm;
    return a.s>b.s;
}

int main()
{
    freopen("A-large.in","r",stdin);
    freopen("A-largeout.txt","w",stdout);

    scanf(" %lld",&t);

    for(P=1LL;P<=t;P++)
    {
        cnt=0LL;
        mpp.clear();
        scanf(" %lld",&p);
        for(i=1LL;i<=p;i++)
        {
            scanf(" %lld",&prize[i]);
        }
        scanf(" %lld",&n);

        for(i=1LL;i<=n;i++)
        {
            scanf(" %lld",&w);
            for(j=1LL;j<=p;j++)
            {
                cin>>arr;
                if(mpp[arr]==0LL)
                {
                    mpp[arr]=++cnt;
                    arr2[cnt]=arr;
                }
                res[mpp[arr]].push_back(w*prize[j]);
            }
        }
        scanf(" %lld",&m);
        for(i=1LL;i<=cnt;i++)
        {
            sort(res[i].begin(),res[i].end());
            reverse(res[i].begin(),res[i].end());
            score[i].s=0LL;
            score[i].nm=arr2[i];
            for(j=0LL;j<m&&j<res[i].size();j++)
            {
                score[i].s+=res[i][j];
            }
        }

        sort(score+1LL,score+cnt+1LL,comp);

        printf("Case #%lld:\n",P);
        rnk=1LL;
        for(i=1LL;i<=cnt;i++)
        {
            if(i==1LL)
                rnk=1LL;
            else
            {
                if(score[i].s<score[i-1].s)
                    rnk=i;
            }
            printf("%lld: ",rnk);
            cout<<score[i].nm<<"\n";
            res[i].clear();
        }

    }

    return 0;
}


