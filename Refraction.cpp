/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int t,p;
double aa,bb,cc,mu,a1,a2,b1,b2,c1,c2,W,H,xe,ye,x,alfa,theta1,theta2,hmin,h,low,high,C;

int main()
{
    cin>>t;
    for(p=1; p<=t; p++)
    {
        scanf(" %lf %lf %lf %lf %lf %lf",&W,&H,&x,&xe,&ye,&mu);
        if(ye<H&&xe>W)
            {printf("Impossible\n");
            continue;}
        if(xe<=W)
            {printf("0.0000\n");
            continue;}
        aa=xe-W;
        bb=ye-H;
        cc=sqrt((aa*aa)+(bb*bb));
        alfa=asin(bb/cc);
        hmin=99999999999.0;
        low=0.0;
        high=H;
        int yoyo=35;
        while(low<=high&&yoyo--)
        {
            h=(low+high)/2.0;
            b1=H-h;
            c1=b1/sin(alfa);
            a1=b1*sin(pi/2.0-alfa)/sin(alfa);

            theta1=pi/2.0-alfa;
            theta2=asin(sin(theta1)/mu);

            b2=h;
            a2=b2*sin(theta2)/sin(pi/2.0-theta2);
            C=W-a1-a2;
            if(C>x&&C<=W)
            {
                hmin=min(hmin,h);
                high=h;
            }
            else
            {
                low=h;
            }
        }
        if(hmin<99999999998.0)
            printf("%.4lf\n",hmin);
        else
            printf("Impossible\n");
    }
    return 0;
}

