#include<stdio.h>
#include<math.h>
#define infinity 999999
main()
{
    int flag,t,i,j,k,p,cities;
    double max,adj[110][110],x[110],y[110],dist;
    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %d",&cities);
        for(i=1;i<=cities;i++)
        scanf(" %lf %lf",&x[i],&y[i]);
        for(i=1;i<=cities;i++)
        {
            adj[i][i]=infinity;
            for(j=i+1;j<=cities;j++)
            {
                dist=sqrt( ((x[i]-x[j])*(x[i]-x[j])) + ((y[i]-y[j])*(y[i]-y[j])) );
                if(dist>10.0)
                adj[i][j]=adj[j][i]=infinity;
                else
                adj[i][j]=adj[j][i]=dist;
            }
        }

        for(k=1;k<=cities;k++)
        {
            for(i=1;i<=cities;i++)
            {
                for(j=1;j<=cities;j++)
                {
                    if(adj[i][j]>(adj[i][k]+adj[k][j]))
                    adj[i][j]=adj[i][k]+adj[k][j];
                }
            }
        }
        for(flag=0,max=0.0,i=1;i<cities;i++)
        {
            for(j=1;j<=cities;j++)
            {
                if(adj[i][j]==infinity)
                {flag=1;
                break;}
                if(adj[i][j]>max)
                max=adj[i][j];
            }
        }
        if(flag==0)
        printf("Case #%d:\n%.4lf\n",p,max);
        else
        printf("Case #%d:\nSend Kurdy\n",p);
        printf("\n");
    }
    return 0;
}
