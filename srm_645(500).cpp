#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#define ll long long
using namespace std;

struct node{
ll pos,len;
};

node arr[55];
ll n;

ll comp(node a,node b)
{
     return a.pos<b.pos;
}

ll func(ll now)
{
    ll i,ret=0LL;
    for(i=0LL;i<n;i++)
    {
        ret+=abs(now-arr[i].pos);
        now+=arr[i].len;
    }
    return ret;
}

class ConnectingCars
{
public:
	long long minimizeCost(vector <int> positions, vector <int> lengths)
	{
	    ll now,nowm,nowp,k,i,high,low,mid,ret;

	    n=(ll)positions.size();

	    for(i=0LL;i<n;i++)
        {
            arr[i].pos=(ll)positions[i];
            arr[i].len=(ll)lengths[i];
        }

	    sort(arr,arr+n,comp);

	    k=100LL;
	    low=arr[0].pos;
	    high=arr[n-1].pos;
	    ret=9999999999999999LL;

	    while(low<=high&&k--)
        {
            mid=(low+high)/2LL;
            now=func(mid);
            nowp=func(mid+1LL);
            nowm=func(mid-1LL);
            if(now<=nowm&&now<=nowp)
                {ret=now;
                break;}
            else if(now>=nowm)
            {
                high=mid-1LL;
            }
            else
                low=mid+1LL;
        }
	    return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	ConnectingCars objectConnectingCars;

	//test case0
	vector <int> param00;
	param00.push_back(1);
	param00.push_back(3);
	param00.push_back(10);
	param00.push_back(20);
	vector <int> param01;
	param01.push_back(2);
	param01.push_back(2);
	param01.push_back(5);
	param01.push_back(3);
	long long ret0 = objectConnectingCars.minimizeCost(param00,param01);
	long long need0 = 15;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(100);
	param10.push_back(50);
	param10.push_back(1);
	vector <int> param11;
	param11.push_back(10);
	param11.push_back(2);
	param11.push_back(1);
	long long ret1 = objectConnectingCars.minimizeCost(param10,param11);
	long long need1 = 96;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(4);
	param20.push_back(10);
	param20.push_back(100);
	param20.push_back(13);
	param20.push_back(80);
	vector <int> param21;
	param21.push_back(5);
	param21.push_back(3);
	param21.push_back(42);
	param21.push_back(40);
	param21.push_back(9);
	long long ret2 = objectConnectingCars.minimizeCost(param20,param21);
	long long need2 = 66;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(5606451);
	param30.push_back(63581020);
	param30.push_back(81615191);
	param30.push_back(190991272);
	param30.push_back(352848147);
	param30.push_back(413795385);
	param30.push_back(468408016);
	param30.push_back(615921162);
	param30.push_back(760622952);
	param30.push_back(791438427);
	vector <int> param31;
	param31.push_back(42643329);
	param31.push_back(9909484);
	param31.push_back(58137134);
	param31.push_back(99547272);
	param31.push_back(39849232);
	param31.push_back(15146704);
	param31.push_back(144630245);
	param31.push_back(604149);
	param31.push_back(15591965);
	param31.push_back(107856540);
	long long ret3 = objectConnectingCars.minimizeCost(param30,param31);
	long long need3 = 1009957100;
	assert_eq(3,ret3,need3);

}
