/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int cs,p,t;
double v1,v2,vf,d,low,high,mid,s1,s2;

int main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>cs>>d>>v1>>v2>>vf;
        low=0.0;
        high=99999999999.0;
        while((low+eps)<=high)
        {
            mid=(low+high)/2.0;
            s1=v1*mid;
            s2=v2*mid;
           // printf("%lf %lf\n",s1,mid);
            if((s1+s2)>d)
                high=mid;
            else
                low=mid;
        }
        printf("%d %.2lf\n",cs,vf*mid);
    }
    return 0;
}


