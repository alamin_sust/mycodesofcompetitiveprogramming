/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

queue<int>q;
vector<int>res1,res2,adj[100010];
int col[100010],s,t,mpp[30][30],n;
char arr1[100010],arr2[100010];

void bfs(int s) {


    while(!q.empty()) q.pop();

    col[s]=1;
    q.push(s);

    while(!q.empty()) {

        s=q.front();
        q.pop();

        for(int i=0;i<adj[s].size();i++) {
            t=adj[s][i];

            if(col[t])continue;

            res1.push_back(s);
            res2.push_back(t);
            q.push(t);
            col[t]=1;

        }


    }
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&n);


    scanf(" %s",arr1);
    scanf(" %s",arr2);

    for(int i=0;i<n;i++) {
        if(arr1[i]!=arr2[i] && mpp[arr1[i]-'a'][arr2[i]-'a']==0) {
            mpp[arr1[i]-'a'][arr2[i]-'a']=1;
            mpp[arr2[i]-'a'][arr1[i]-'a']=1;
            adj[arr2[i]-'a'].push_back(arr1[i]-'a');
            adj[arr1[i]-'a'].push_back(arr2[i]-'a');
        }
    }

    for(int i=0;i<26;i++) {

        if(col[i]==0&&adj[i].size()) {
            bfs(i);
        }

    }
    printf("%d\n",res1.size());

    for(int i=0;i<res1.size();i++) {

        printf("%c %c\n",res1[i]+'a',res2[i]+'a');
    }
    return 0;
}
