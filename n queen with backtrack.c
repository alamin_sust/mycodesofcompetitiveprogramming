#include<stdio.h>
int col[10010]={0},n,res=0,diagdiff[10010]={0},diagadd[10010]={0};

void backTrack(int row)
{
    if(row>n)
    {res++;
    return;}
    int i;
    for(i=1;i<=n;i++)
    {
        if(col[i]||diagdiff[row-i+n]||diagadd[row+i])
        continue;
        col[i]=1;
        //printf("%d ",i);
        diagdiff[row-i+n]=1;
        diagadd[row+i]=1;
        backTrack(row+1);
        col[i]=0;
        diagdiff[row-i+n]=0;
        diagadd[row+i]=0;
    }
    return;
}

main()
{
    scanf(" %d",&n);
    backTrack(1);
    printf("%d\n",res);
    return 0;
}
