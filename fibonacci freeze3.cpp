#include<iostream>
#include<cmath>
#include<vector>

using namespace std;

#define MAX 1500

class BigInt {
   string str;
   int sign;
   int len;
public:
   BigInt(BigInt &b) {
      str.resize(MAX);
      fill(str.begin(),str.end(),'0');
      str=b.str;
      sign=b.sign;
      len=b.len;
   }
   BigInt(string s) {
      sign='+';
      str.resize(MAX);
      fill(str.begin(),str.end(),'0');
      if(s[0]=='-') {
         sign='-';
         s[0]='0';
      }
      copy_backward(s.begin(),s.end(),str.end());
      len=s.length();
   }
   BigInt() {
      sign='+';
      str.resize(MAX);
      fill(str.begin(),str.end(),'0');
      len=0;
   }

   BigInt operator+(BigInt B) {
      BigInt sum;
      int big=(len>B.len?len : B.len);
      int stop=MAX-big;
      int carry=0;
      for(int i=MAX-1;i>=stop-1;i--) {
         sum.str[i]=carry + str[i]+B.str[i] - 2*'0';
         carry=sum.str[i]/10;
         sum.str[i]%=10;
         sum.str[i]+='0';
      }
      if(sum.str[stop-1]!='0')
         sum.len=big+1;
      else
         sum.len=big;
      return sum;

   }
   void calclen() {
      int i=0;
      for(i=0;str[i]=='0';i++);
      len=MAX-i;
   }
   BigInt operator-(BigInt B) {
      BigInt sum;
      for(int i=MAX-len;i<MAX;i++)
         B.str[i]='9'-B.str[i]+'0';
      B=B+BigInt("1");
      sum=*this+B;
      if(sum.len>this->len) {
         sum.str[MAX-sum.len]='0';
         sum.len--;
      }
      sum.calclen();
      return sum;

   }

   BigInt operator++() {
      (*this)=(*this)+BigInt("1");
      calclen();
   }


   BigInt operator*(BigInt B) {
      BigInt prod;
      for(int j=MAX-1;j>=MAX-len;j--)
         prod=prod+multiply(*this,B.str[j],MAX-j-1);
      prod.calclen();
      return prod;
   }
   BigInt multiply(BigInt B, char c, int shift) {
      BigInt term;
      int carry=0;
      for(int i=MAX-1;i>=MAX-B.len -1;i--) {
         term.str[i-shift]=((B.str[i]-'0')*(c-'0') + carry)%10 + '0';
         carry=((B.str[i]-'0')*(c-'0') + carry)/10;
      }
      term.calclen();
      return term;
   }
   friend ostream& operator<<(ostream &stream, BigInt B);
};

ostream& operator<<(ostream &stream, BigInt B) {
   if(B.len==0)
      stream<<'0';
   for(int i=B.str.size()-B.len;i<B.str.size();i++)
      stream<<B.str[i];
   return stream;
}

int main() {
   int n;
   while(true) {
      cin>>n;
      if(feof(stdin))
         break;
      BigInt last("1"),sLast("0");
      for(int i=0;i<n-1;i++) {
         BigInt temp=last;
         last=last+sLast;
         sLast=temp;
      }
      if(n==0)
         last=BigInt("0");
      cout<<"The Fibonacci number for "<<n<<" is "<<last<<"\n";
   }
   return 0;
}
