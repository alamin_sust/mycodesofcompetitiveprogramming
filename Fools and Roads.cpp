/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

pair<int,int>pii;
map<pair<int,int>,int>mpp;
queue<int>q;
vector<int>adj[100010];
int val[100010][22];
int par[100010],lev[100010];
int dp[100010][22];
int flag[100010];

void rec(int node,int num)
{
    if(node<1)
        return;
    if(num==0)
    {
        dp[node][num]=par[node];
        return;
    }
    int &ret=dp[node][num];
    if(ret!=-1)
        return;
    rec(node,num-1);
    rec(dp[node][num-1],num-1);
    ret=dp[dp[node][num-1]][num-1];
    return;
}

void build(int root)
{
    par[root]=-1;
    lev[root]=1;
    q.push(root);
    while(!q.empty())
    {
        int u=q.front();
        q.pop();
        for(int i=0; i<adj[u].size(); i++)
        {
            int v=adj[u][i];
            if(!lev[v])
                par[v]=u,lev[v]=lev[u]+1,q.push(v);
        }
    }
    return;
}

main()
{
    int n;
    cin>>n;
    for(int i=1; i<n; i++)
    {
        scanf(" %d %d",&pii.first,&pii.second);
        mpp[pii]=i;
        swap(pii.first,pii.second);
        mpp[pii]=i;
        adj[pii.first].push_back(pii.second);
        adj[pii.second].push_back(pii.first);
    }
    //int mx=0,rt=1;
    //for(int i=1;i<=n;i++)
    // {
    //    if(adj[i].size()>mx)
    //    {
    //        mx=adj[i].size();
    //        rt=i;
    //    }
    //}
    build(1);
    memset(dp,-1,sizeof(dp));
    for(int i=1; i<=n; i++)
    {
        if(adj[i].size()==1)
        {
            rec(i,20);
        }
    }
    //for(int i=1;i<=n;i++)
    //{
    //    printf("%d %d %d\n",dp[i][0],dp[i][1],dp[i][2]);
    //}
    int m,res[100010]= {0};
    cin>>m;
    for(int i=1; i<=m; i++)
    {
        int u,v;
        scanf(" %d %d",&u,&v);
        int pow=0;
        if(lev[u]>lev[v])
        {
            while(lev[u]!=lev[v])
            {
               // printf("..");
                if(dp[u][pow]>0&&lev[dp[u][pow]]>=lev[v])
                {
                    val[u][pow]++;
                    u=dp[u][pow];
                    pow++;
                }
                else
                    pow--;

            }
        }
        pow=0;
        if(lev[u]<lev[v])
        {
            while(lev[u]!=lev[v])
            {
                //printf("**");
                if(dp[v][pow]>0&&lev[dp[v][pow]]>=lev[u])
                {
                    val[v][pow]++;
                    v=dp[v][pow];
                    pow++;
                }
                else
                    pow--;
            }
        }
        pow=0;
        //printf("%d %d;;;;;;;;;;;\n",u,v);
        while(u!=v)
        {
            //printf("--");
            if(par[u]==par[v])
            {
                val[u][0]++;
                val[v][0]++;
                u=par[u];
                v=par[v];
            }
            else if(dp[u][pow]>0&&lev[dp[u][pow]]!=lev[dp[v][pow]])
            {
                val[u][pow]++;
                u=dp[u][pow];
                val[v][pow]++;
                v=dp[v][pow];
                pow++;
            }
            else
                pow--;
        }
       // for(int i=1; i<=n; i++)
       // {
       //     printf("%d %d %d %d\n",val[i][0],val[i][1],val[i][2],val[i][3]);
       // }
    }

    for(int i=1; i<=n; i++)
    {
        if(adj[i].size()==1)
        {
            int tp=i;
            while(flag[tp]==0)
            {
                for(int j=0;j<=20;j++)
                {
                    res[tp]+=val[tp][i];
                }
                tp=par[tp];

            }
        }
    }
    return 0;
}

