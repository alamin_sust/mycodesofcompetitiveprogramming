#include <stdio.h>

long long int getDevidersNumber(long long int a,long long int b)
{
    long long int i= 0;
    while ((a % b)==0)
    {
        a=a/b;
        i++;
    }
    return i;
}

int main()
{
    long long int i, j, N, m, n, pow;
    scanf("%lld", &N);
    for (i = 1; i <= N; i++)
    {
        scanf("%lld", &m);
        scanf("%lld", &n);
        printf("Case %lld:\n", i);
        j = m;
        pow = 0;
        while (j <= n)
        {
            pow += getDevidersNumber(j, m);
            j += m;
        }
        if (pow!=0)
        {
            printf("%d", pow);
        }
        else
        {
            printf("Impossible to divide");
        }
        printf("\n");
    }
    return 0;
}
