/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

map<ll,ll>mpp;

struct node
{
    ll st,en;
};

node arr[1000010];
ll X[1000010],Y[1000010],A,B,C1,M,C2,L1,R1,N;
vector<ll>v,vc[2000010];
ll res[2000010];
set<ll>stt;

node fc(ll ind)
{
    node ret;
    X[ind]=( A*X[ind-1LL] + B*Y[ind-1LL] + C1 ) % M;
    Y[ind]=( A*Y[ind-1LL] + B*X[ind-1LL] + C2 ) % M;
    ret.st=min(X[ind],Y[ind])*2LL;
    ret.en=max(X[ind],Y[ind])*2LL+1LL;
    if(mpp[ret.st]==0LL)
    {
        v.push_back(ret.st);
        mpp[ret.st]=1LL;
    }
    if(mpp[ret.en]==0LL)
    {
        v.push_back(ret.en);
        mpp[ret.en]=1LL;
    }
    return ret;
}

int main()
{
    freopen("new 1.txt","r",stdin);
    freopen("B-small-practiceOut.txt","w",stdout);
    ll p,t;
    scanf(" %lld",&t);

    for(p=1LL; p<=t; p++)
    {
        mpp.clear();
        memset(res,0LL,sizeof(res));
        v.clear();
        for(ll i=0LL; i<1000005LL; i++)
            vc[i].clear();
        stt.clear();
        scanf(" %lld %lld %lld %lld %lld %lld %lld %lld",&N,&L1,&R1,&A,&B,&C1,&C2,&M);
        arr[1LL].st=L1*2LL;
        mpp[L1*2LL]=1LL;
        v.push_back(L1*2LL);
        arr[1LL].en=R1*2LL+1LL;
        if(mpp[R1*2+1]==0LL)
            mpp[R1*2+1]=1LL,v.push_back(R1*2+1);
        X[1LL]=L1;
        Y[1LL]=R1;

        for(ll i=2LL; i<=N; i++)
        {
            arr[i]=fc(i);
            //printf("%lld %lld\n",arr[i].st,arr[i].en);
        }
        sort(v.begin(),v.end());
        for(ll i=0LL; i<v.size(); i++)
        {
            mpp[v[i]]=i+1LL;
        }


        for(ll i=1LL; i<=N; i++)
        {
            vc[mpp[arr[i].st]].push_back(i);
            vc[mpp[arr[i].en]].push_back(-i);
        }
        for(ll i=0LL; i<vc[1LL].size(); i++)
        {
            if(vc[1LL][i]>0LL)
                stt.insert(vc[1LL][i]);
            else
                stt.erase(vc[1LL][i]);
        }
        ll bfsz=0LL;
        ll tot=0LL;
        ll bi=0LL;
        ll bi2=0LL;
        for(ll i=2LL; i<=v.size(); i++)
        {
            ll now=-1LL;
            //printf("%lld..%lld......%d\n",v[i-2],v[i-1],stt.size());
            if(stt.size()==1LL&&(v[i-1LL]%2LL)==1LL)
            {
                now=*stt.begin();
                res[now]+=(v[i-1LL]-max(v[i-2LL]+1LL,bi2))/2LL;//-(bfsz==0LL?0LL:1LL));
                //printf("%lld %lld--::-\n",now,res[now]);
                bi2=v[i-1LL]+1LL;
            }
            if(stt.size()==1LL&&(v[i-1LL]%2LL)==0LL)
            {
                now=*stt.begin();
                res[now]+=(v[i-1LL]-max(v[i-2LL]+1LL,bi2))/2LL;//-(bfsz==0LL?0LL:1LL));
                //printf("%lld %lld---\n",now,res[now]);
                bi2=v[i-1LL]+2LL;
            }
            if(stt.size()>0LL)
            {
                tot+=(v[i-1LL]-max(v[i-2LL],bi)+1LL)/2LL;
                bi=v[i-1LL];
            }
            ll flag=0LL;
            for(ll j=0LL; j<vc[i].size(); j++)
            {
                if(vc[i][j]>0LL)
                    stt.insert(vc[i][j]),flag=1LL;
                else
                    stt.erase(-vc[i][j]);
            }


            //bfsz=stt.size();
            //if(flag==0LL&&now!=-1LL)
              //res[now]++;
            if(now!=-1LL){
                //printf("%lld  +++\n",i-1);
            if((v[i-1LL]%2LL)==1LL&&i<v.size()&&v[i]==(v[i-1]+1LL)){}
            else if((v[i-1LL]%2LL)==0LL&&i<v.size()&&v[i]==(v[i-1LL]+2LL)){}
            else res[now]++;//printf("%lld  +++\n",i-1);
            }
        }

        ll ans=0LL;

        for(ll i=1LL; i<=N; i++)
        {
            //printf("%lld  ooo\n",res[i]);
            ans=max(ans,res[i]);
        }
        //printf("%lld %lld\n",tot,ans);
        printf("Case #%lld: %lld\n",p,tot-ans);

    }


    return 0;
}






