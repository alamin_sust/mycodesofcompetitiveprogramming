/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll p,t,d1,d2,s1,s2,n1,n2,res,st,en,i,j;

int main()
{
    cin>>t;
    for(p=1; p<=t; p++)
    {
        scanf(" %lld %lld %lld",&n1,&s1,&d1);
        scanf(" %lld %lld %lld",&n2,&s2,&d2);
        en=min(d1*(n1-1LL)+s1,d2*(n2-1LL)+s2);

//        printf("%lld\n",en);
        for(i=s1,j=s2;;)
        {
            if(i==j)
            {
                st=i;
                break;
            }
            if(i<j)
                i+=d1;
            else
                j+=d2;
        }
        res=1LL;
        if(st<en)
            res+=(en-st)/((d1*d2)/(__gcd(d1,d2)));

        printf("%lld\n",res);

    }
    return 0;
}
