#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll prime[110];

void prime_gen(void)
{
    ll i,j,k,fg;
    k=1;
    for(i=2;i<=105;i++)
    {
        fg=0;
        for(j=2;j<i;j++)
        {
            if(i%j==0)
            {
                fg=1;
                break;
            }
        }
        if(fg==0)
            prime[k++]=i;
    }
    return;
}

main()
{
    ll t,p,k,in2,fg,in,res;
    prime_gen();
    scanf(" %lld",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %lld",&in);
        k=1;
        printf("Case %lld: %lld =",p,in);
        fg=0;
        while(prime[k]<=in)
        {
            if(fg==1)
                printf(" *");
            res=0;
            in2=in;
            while(in2>=prime[k])
            {
                in2/=prime[k];
                res+=in2;
            }
            printf(" %lld (%lld)",prime[k],res);
            k++;
            fg=1;
        }
        printf("\n");
    }
    return 0;
}

