/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};


ll calc(ll n)
{
    return n*(n+1)/2;
}

int main()
{
    ll n,i,sum1,sum2,sum,f,t,res,j;
    while(cin>>n&&n)
    {
        res=99999999;
        f=t=0;
        sum=calc(n);
        for(i=1,j=1;j<=n;)
        {
           sum1=(calc(j)-calc(i-1));
           sum2=sum-sum1;
          // printf("%d %d..\n",sum1,sum2);
            if(abs(sum1-sum2)<res)
            {
                res=abs(sum1-sum2);
                f=i;
                t=j;
            }
            else if(abs(sum1-sum2)==res&&i<f)
            {
                res=abs(sum1-sum2);
                f=i;
                t=j;
            }
            if(sum1>sum2)
            {
                i++;
            }
            else
            {
                j++;
            }
        }
        printf("%lld %lld\n",f,t);
    }
    return 0;
}


