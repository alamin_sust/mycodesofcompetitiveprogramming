/*Author : Md. Al- Amin
20th batch
Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define MAX 100510
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll sieve[MAX+10],cnt,prime[MAX+10],in[100010],flag[MAX+10],glob,res[MAX+10],M,N,Q,hagu1,hagu2,hagu3,hagu4,p,ret[30];

struct node
{
    ll val,cnt;
};
vector<node>arr[MAX+10];
node det;
map<ll,ll>mpp;

void sieve_(void)
{
    cnt=0LL;
    sieve[0]=sieve[1]=1LL;
    for(ll i=2LL; i<=MAX; i++)
    {
        if(sieve[i]==0LL)
        {
            prime[cnt++]=i;
            det.val=i;
            arr[i].push_back(det);
//printf("%lld\n",i);
            for(ll j=2LL; i*j<=MAX; j++)
            {
                sieve[i*j]=1LL;
                arr[i*j].push_back(det);
            }
        }
    }
    return;
}

void init_arr(void)
{
    for(ll i=1LL; i<=MAX; i++)
    {
        for(ll j=0LL; j<arr[i].size(); j++)
        {
            ll tp=i;
            det=arr[i][j];
            while((tp%det.val)==0)
            {
                arr[i][j].cnt++;
                tp/=det.val;
             //   hagu1++;
            }
        }
    }

}


ll calculate(ll num,ll pw)
{
//hagu++;
// printf("%lld %lld\n",num,pw);
// getchar();
    ll pw2=pw,res=1LL,j;
    ret[0]=num;
    for(ll j=1LL; pw2>0LL; j++,pw2>>=1LL)
    {
        ret[j]=(ret[j-1]*ret[j-1])%M;
    }
    for(j=0LL; pw>0LL; j++)
    {
        if(1LL&pw)
        {
            res=(res*ret[j])%M;
        }
        pw>>=1LL;
    }
    return res;
}

void relaxup(ll val,ll pw)
{
    if(sieve[val]==0LL)
    {
        glob=(glob*calculate(val,val))%M;
        return;
    }
    /*for(ll i=0LL;prime[i]<=val;i++)
    {
    if(sieve[val]==0LL)
    {
    flag[val]+=pw;
    break;
    }
    while((val%prime[i])==0LL)
    {
    flag[prime[i]]+=pw;
    val/=prime[i];
    }
    }*/
    for(ll i=0LL; i<arr[val].size(); i++)
    {
        flag[arr[val][i].val]+=(pw*arr[val][i].cnt);
       // hagu3++;
    }
    return;
}

void relaxdown(ll val,ll pw)
{
    /*for(ll i=0LL; prime[i]<=val; i++)
    {
        if(sieve[val]==0LL)
        {
            flag[val]-=pw;
            break;
        }
        while((val%prime[i])==0LL)
        {
            flag[prime[i]]-=pw;
            val/=prime[i];
        }
    }*/
    for(ll i=0LL; i<arr[val].size(); i++)
    {
        //hagu3++;
        flag[arr[val][i].val]-=(pw*arr[val][i].cnt);
    }
    return;
}

ll get_result(ll mx)
{
    ll ret=1LL;
    for(ll i=0LL; prime[i]<=mx; i++)
    {
        if(flag[prime[i]])
        {//hagu2++;
            ret=(ret*calculate(prime[i],flag[prime[i]]))%M;
        }
    }
    return ret;
}

void func(void)
{
    ll i,j;
    res[0]=1LL;
    glob=1LL;
    for(i=1LL,j=N; i<j; i++,j--)
    {
        relaxup(j,j);
        relaxdown(i,i);
        if(mpp[i]==p)
        res[i]=(glob*get_result(j-1LL))%M;
//printf("res[%lld]=%lld\n",i,res[i]);
    }
    return;
}

int main()
{
    ll t,i;
    sieve_();
    init_arr();
    cin>>t;
    for(p=1; p<=t; p++)
    {
        scanf(" %lld %lld %lld",&N,&M,&Q);
        for(i=0LL; prime[i]<=N; i++)
            flag[prime[i]]=0LL;

//printf("%lld %lld %lld %lld\n",hagu1,hagu2,hagu3,hagu4);
        for(i=1LL; i<=Q; i++)
        {
            scanf(" %lld",&in[i]);
            mpp[min(in[i],N-in[i])]=p;
        }
        func();
        for(i=1LL; i<=Q; i++)
        printf("%lld\n",res[min(in[i],N-in[i])]);

    }
    return 0;
}
