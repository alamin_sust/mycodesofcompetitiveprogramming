#include<stdio.h>
long long int arr[1010],res,element,ans[1010],nn;
long long int lcm(long long int n)
{
  long long int i,j,k,maximum,minimum,product;

  maximum=arr[1];
  for(i=1;i<=n;i++)
    if(ans[i]>=maximum)
      maximum=ans[i];
  minimum=arr[1];
  for(i=1;i<=n;i++)
    if(ans[i]<minimum)
      minimum=ans[i];

  for(i=1,product=1;i<=n;i++)
    product=product*ans[i];

  for(i=maximum;i<=product;i+=maximum)
  {

    k=0;
    for(j=1;j<=n;j++)
      if(i%ans[j]==0)
		 k+=1;
    if(k==n)
    {
      return i;
    }
  }
}

void backtrack(long long int in,long long int ans_in)
{
    if(in>element)
    {
       long long int i;
       if(ans_in>1)
        {
        if(ans_in%2==0)
            res+=(nn/lcm(ans_in-1));
        else
            res-=(nn/lcm(ans_in-1));
        }
        return;
    }
    ans[ans_in]=arr[in];
    backtrack(in+1,ans_in+1);
    backtrack(in+1,ans_in);
}

main()
{
   long long int t,i,j;
    scanf(" %lld",&t);
    for(i=1;i<=t;i++)
    {
    res=0;
     scanf(" %lld %lld",&nn,&element);
     for(j=1;j<=element;j++)
        scanf(" %lld",&arr[j]);
     backtrack(1,1);
     printf("Case %lld: %lld\n",i,nn-res);
    }
   return 0;
}
