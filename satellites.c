#include<stdio.h>
#include<math.h>
#define pi 2*acos(0)
main()
{
    double s,angle,rad,radius,ans1,ans2;
    char array[5];
    while(scanf("%lf%lf%s",&s,&angle,array)==3)
    {
        if(angle>180)
            angle=360-angle;
        if(array[0]=='m')
            angle=angle/60;
        rad=(pi/180)*angle;
        radius=s+6440;
        ans1=rad*radius;
        ans2=2*radius*sin(rad/2.0);
        printf("%0.6lf %0.6lf\n",ans1,ans2);
    }
    return 0;
}
