/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;
int t,p,i,n,even,odd,in;
int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++)  {
        scanf(" %d",&n);
        odd=even=0;
        for(i=0;i<n;i++) {
            scanf(" %d",&in);
            if(in%2) {
                odd++;
            } else even++;
        }
        even+=(odd/2);
        odd=(odd%2);
        if(even>0)even=1;
        printf("%d\n",even+odd);
    }

    return 0;
}
