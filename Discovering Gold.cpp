#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int n,visited[110],arr[1010];
double result[110];
double rec(int pos)
{
    if(pos>=n)
    {
        return 0;
    }
    if(pos+1==n)
        return arr[pos];

    if(visited[pos]==1)
      return result[pos];

    visited[pos]=1;
    //double &ret=result[pos];
    double by=(double)min(n-pos-1,6);
    result[pos]=arr[pos];
    for(int i=1;i<7;i++)
        result[pos]=result[pos]+rec(pos+i)/by;
    return result[pos];
}

main()
{
    int t,p,i;
    cin>>t;

    for(p=1;p<=t;p++)
    {
        memset(visited,0,sizeof(visited));
        //for(i=0;i<=105;i++)
         //   result[i]=0.0;
        cin>>n;
        for(i=0;i<n;i++)
            scanf(" %d",&arr[i]);
        printf("Case %d: %.10lf\n",p,rec(0));
    }
    return 0;
}

