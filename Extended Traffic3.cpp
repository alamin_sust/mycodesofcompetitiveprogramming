#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

int n,m,dist[210],dist2[210];

struct node
{
    int from,to,val;
};

node adj_list[100010];

void b_ford(void)
{
    for(int i=1;i<=n;i++)
        {dist[i]=inf;
        dist2[i]=inf;}
    dist[1]=dist2[1]=0;
    for(int i=1;i<n;i++)
    {
        for(int j=1;j<=m;j++)
        {
            if(dist[adj_list[j].from]!=inf&&dist[adj_list[j].to]>dist[adj_list[j].from]+adj_list[j].val)
                dist[adj_list[j].to]=dist2[adj_list[j].to]=dist[adj_list[j].from]+adj_list[j].val;
        }
    }
    for(int i=1;i<n;i++)
    {
        for(int j=1;j<=m;j++)
        {
            if(dist2[adj_list[j].from]!=inf&&dist2[adj_list[j].to]>dist2[adj_list[j].from]+adj_list[j].val)
                dist2[adj_list[j].to]=dist[adj_list[j].from]+adj_list[j].val;
        }
    }
    return;
}

main()
{
    int p,t,cost[210],i,j,in,q;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        for(i=1;i<=n;i++)
            scanf(" %d",&cost[i]);
        scanf(" %d",&m);
        for(i=1;i<=m;i++)
        {
            scanf(" %d %d",&adj_list[i].from,&adj_list[i].to);
            adj_list[i].val=(cost[adj_list[i].to]-cost[adj_list[i].from])*(cost[adj_list[i].to]-cost[adj_list[i].from])*(cost[adj_list[i].to]-cost[adj_list[i].from]);
        }
        printf("Case %d:\n",p);
        scanf(" %d",&q);
        b_ford();
        for(i=1;i<=q;i++)
        {
        scanf(" %d",&in);
        if(dist[in]==dist2[in]&&dist[in]<inf&&dist[in]>2)
        printf("%d\n",dist[in]);
        else
        printf("?\n");
        }
    }
    return 0;
}
