#include <stdio.h>
#include <math.h>
#include <algorithm>
using namespace std;

#define max_f 2000000
int fac[max_f+5],k;
void factor(int n){
   int i,sr;
   k=0;
   sr=sqrt(n)+1;
   for(i=1;i<=sr;i++){
      if(n%i==0){
         fac[k++]=i;
         if(i!=sr)
         fac[k++]=n/i;
      }
   }
}
int main(){
   int n,T,cas=1,i,r;
   scanf("%d",&T);
   while(T--){
      scanf("%d %d",&n,&r);
      printf("Case #%d:",cas++);
      if(n==r){printf(" 0\n");continue;}
      n-=r;
      factor(n);
      sort(fac,fac+k);
      for(i=0;i<k;i++)
         if(fac[i]>r){
            printf(" %d",fac[i]);
         }
      printf("\n");
   }
   return 0;
}
