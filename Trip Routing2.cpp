#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

string id[1010][1010],str[100010],nd[1000010];
int adj[1010][1010],p,pp;
char ar[1010];

struct node
{
    int dist,x;
};

node u,v;

bool operator<(node a,node b)
{
    return a.dist>b.dist;
}

int d[1010],status[1010],par[1010];
queue<int>res;
priority_queue<node>pq;
map<string,int>mpp;

void dijkstra(int from,int to)
{
    while(!pq.empty())
        pq.pop();
    while(!res.empty())
        res.pop();
    for(int i=1; i<=p; i++)
        d[i]=99999999,par[i]=i;
    memset(status,0,sizeof(status));
    u.dist=0;
    u.x=from;
    d[from]=0;
    par[from]=from;
    status[u.x]=1;
    pq.push(u);
    while(!pq.empty())
    {
        u.x=pq.top().x;
        u.dist=pq.top().dist;
        res.push(u.x);
        if(u.x==to)
            return;
        pq.pop();
        for(int i=1; i<=p; i++)
        {
            if(u.x!=i&&adj[u.x][i]!=-1)
            {
               // printf("%d %d %d\n",d[i],d[u.x],adj[u.x][i]);
                if(d[i]>(d[u.x]+adj[u.x][i]))
                {
                    //  printf("..");
                    d[i]=d[u.x]+adj[u.x][i];
                    status[i]=1;
                    v.x=i;
                    v.dist=d[i];
                    par[i]=u.x;
                    pq.push(v);
                }
            }
        }
    }
}

main()
{
    int l,i,j,k;
    memset(adj,-1,sizeof(adj));
    while(gets(ar))
    {
        int val=0;
        char from[25]= {'\0'},to[25]= {'\0'},ids[15]= {'\0'};
        l=strlen(ar);
        if(l==0)
            break;
        for(k=0,j=0,i=0; i<l; i++)
        {
            if(ar[i]==',')
                k++,j=0;
            else if(k==0)
            {
                from[j++]=ar[i];
            }
            else if(k==1)
            {
                to[j++]=ar[i];
            }
            else if(k==2)
            {
                ids[j++]=ar[i];
            }
            else
            {
                val=(val*10)+ar[i]-'0';
            }
        }
        //printf(" %s %s\n",from,to);
        if(mpp[from]==0)
        {
            mpp[from]=++p;
            nd[p]=from;
        }
        if(mpp[to]==0)
        {
            mpp[to]=++p;
            nd[p]=to;
        }
        if(adj[mpp[from]][mpp[to]]==-1||adj[mpp[from]][mpp[to]]>val)
        {
            adj[mpp[from]][mpp[to]]=adj[mpp[to]][mpp[from]]=val;
            //printf("%d-%d\n",mpp[from],mpp[to]);
            id[mpp[from]][mpp[to]]=id[mpp[to]][mpp[from]]=ids;
        }
    }
    while(gets(ar))
    {
        printf("\n\n");
        printf("From                 To                   Route      Miles\n");
        printf("-------------------- -------------------- ---------- -----\n");

        int kk=0;
        l=strlen(ar);
        char from[25]= {'\0'},to[25]= {'\0'};
        for(i=0; i<l; i++)
        {
            if(ar[i]==',')
                break;
            from[i]=ar[i];
        }
        int ff=mpp[from];
        for(i++,j=0; i<l; j++,i++)
        {
            if(ar[i]==',')
                break;
            to[j]=ar[i];
        }
        int tt=mpp[to];
        //printf("%s %s\n%d %d\n",from,to,ff,tt);
        dijkstra(ff,tt);
        while(par[tt]!=tt)
        {
            str[kk++]=nd[tt];
            tt=par[tt];
        }
        str[kk]=nd[ff];
        int l1;
        //printf("%d]]]\n",kk);
        //getchar();
        int ans=0;
        while(kk>=1)
        {
            int l1=21-str[kk].size();
            cout<<str[kk];
            while(l1>0)
                printf(" "),l1--;
            l1=21-str[kk-1].size();
            cout<<str[kk-1];
            while(l1>0)
                printf(" "),l1--;
            l1=11-id[mpp[str[kk]]][mpp[str[kk-1]]].size();
            cout<<id[mpp[str[kk]]][mpp[str[kk-1]]];
            while(l1>0)
                printf(" "),l1--;
            pp=adj[mpp[str[kk]]][mpp[str[kk-1]]];
            ans+=pp;
            l1=5;
            while(pp!=0)
                pp/=10,l1--;
            while(l1>0)
                printf(" "),l1--;
            cout<<adj[mpp[str[kk]]][mpp[str[kk-1]]];
            kk--;
            printf("\n");

        }
        l1=53;
        while(l1>0)
        printf(" "),l1--;
        printf("-----\n");
        l1=42;
        while(l1>0)
        printf(" "),l1--;
        printf("Total      ");
        pp=ans;
        l1=5;
        while(pp!=0)
                pp/=10,l1--;
            while(l1>0)
                printf(" "),l1--;
        printf("%d\n",ans);

    }
    return 0;
}
