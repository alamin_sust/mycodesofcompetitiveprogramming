/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

char arr[100010],a1[110],a2[110];
int l,l1,l2,i,k,j,a,b;

main()
{
    scanf("%s%s%s",&arr,&a1,&a2);
    l=strlen(arr);
    l1=strlen(a1);
    l2=strlen(a2);
    for(i=0;i<l;i++)
    {
        for(j=i,k=0;k<l1&&j<l;j++,k++)
        {
            if(arr[j]!=a1[k])
                break;
        }
        if(k==l1)
            {a=1;
            break;}
    }
    for(i=j;i<l;i++)
    {
        for(j=i,k=0;k<l2&&j<l;j++,k++)
        {
            if(arr[j]!=a2[k])
                break;
        }
        if(k==l2)
            {a++;
            break;}
    }
    for(i=l-1;i>=0;i--)
    {
        for(j=i,k=0;k<l1&&j>=0;j--,k++)
        {
            if(arr[j]!=a1[k])
                break;
        }
        if(k==l1)
            {b=1;
            break;}
    }
    for(i=j;i>=0;i--)
    {
        for(j=i,k=0;k<l2&&j>=0;j--,k++)
        {
            if(arr[j]!=a2[k])
                break;
        }
        if(k==l2)
            {b++;
            break;}
    }
    if(a==2&&b==2)
    {
        printf("both\n");
    }
    else if(a==2)
    {
        printf("forward\n");
    }
    else if(b==2)
    {
        printf("backward\n");
    }
    else
        printf("fantasy\n");
    return 0;
}


