/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

char ch1,ch2,arr[1000010];
int t,p,i,j,l,n,chr[30],flag[30];

int main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %s",arr);
        l=strlen(arr);

        for(i=0;i<26;i++)
        {
            chr[i]=(1<<i);
        }

        scanf(" %d",&n);
        getchar();
        for(i=0;i<n;i++)
        {
            scanf(" %c %c",&ch1,&ch2);
            //printf("%c %c\n",ch1,ch2);
            if(ch1!=ch2)
            {
            chr[ch1-'A']|=chr[ch2-'A'];
            chr[ch2-'A']=0;
            }
        }

        for(i=0;i<26;i++)
        {
            for(j=0;j<26;j++)
            {
                if((1<<j)&chr[i])
                flag[j]=i;
                //printf("%d %d\n",i,j);
            }
            //printf("%d\n",flag[i]);
        }

        for(i=0;i<l;i++)
        {
            if(arr[i]!='_')
            {
                arr[i]='A'+flag[arr[i]-'A'];
            }
        }
        printf("%s\n",arr);
    }
    return 0;
}

