/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll t,p,n,k,d1,d2;

bool check(ll a,ll b,ll c)
{
    ll mx=max(max(a,b),c);
    if((3*mx-a-b-c)<=(n-k))
        return true;
    return false;
}

main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %I64d %I64d %I64d %I64d",&n,&k,&d1,&d2);
        if(check(0,-d1,-(d1+d2))||check(0,-d1,-d1+d2)||check(-d1,0,-d2)||check(-d1,0,d2))
            printf("yes\n");
        else
            printf("no\n");
    }
    return 0;
}


/*
5
3 0 0 0
3 3 0 0
6 4 1 0
6 3 3 0
3 3 3 2
*/
