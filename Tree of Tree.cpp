/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

queue<ll>q;
vector<ll>adj[110];
ll par[110],val[110],id[110],arr[110];

void build(ll src)
{
    memset(par,0,sizeof(par));
    par[src]=-1;
    while(!q.empty())
        q.pop();
    q.push(src);
    id[src]=1;
    val[src]=arr[src];
    while(!q.empty())
    {
        int u=q.front();
        q.pop();
        for(ll i=0;i<adj[u].size();i++)
        {
            ll v=adj[u][i];
            if(par[v]==0)
            {
                par[v]=u;
                id[v]=id[u]+1;
                val[v]=val[u]+arr[v];
                q.push(v);
            }
        }
    }
    return;
}

main()
{
    ll n,k,i,j,mx,from,to;
    while(cin>>n>>k)
    {
        for(i=1;i<=n;i++)
            {scanf(" %lld",&arr[i]);
            adj[i].clear();}
        for(i=1;i<n;i++)
        {
            scanf(" %lld %lld",&from,&to);
            adj[from+1].push_back(to+1);
            adj[to+1].push_back(from+1);
        }
        mx=0;
        for(i=1;i<=n;i++)
        {
            build(i);
            for(j=1;j<=n;j++)
            {
                if(id[j]==k)
                {
                    mx=max(mx,val[j]);
                }
            }
        }
        printf("%lld\n",mx);
    }
    return 0;
}

