#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<cmath>
using namespace std;
long long int res=0,dp[210][110],n,arr[210][110];
long long int rec(long long int row,long long int col)
{
    if(row>n||col>n)
    return 0;
    return dp[row][col]=max(rec(row,col+1),rec(row+1,col))+arr[row][col];
}

main()
{
    long long int t,p,n,i,m,j;
    scanf(" %lld",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %lld",&n);
        m=n;
        for(i=0;i<=m+1;i++)
        {
            for(j=0;j<=n+1;j++)
            {
                dp[i][j]=-1;
                arr[i][j]=-1;
            }
        }
        for(i=1;i<=n;i++)
        {
        for(j=1;j<=n;j++)
        scanf(" %lld",&arr[i][j]);
        }
        rec(1,1);
        printf("%lld\n",dp[0][0]);
    }
    return 0;
}
