/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;


int n,k,i,j,arr[10002],cum[10002][8],cum2[102][10002],buc[25700],xr,bit,mn,gone,K;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d %d",&n,&K);

    for(i=1; i<=n; i++)
    {
        scanf(" %d",&arr[i]);
    }

    for(i=1; i<=n; i++)
    {
        for(j=0; j<7; j++)
        {
            if(((1<<j)&arr[i]))
                cum[i][j] = cum[i-1][j]+1;
            else
                cum[i][j] = cum[i-1][j];
        }
    }

    for(i=1;i<=100;i++) {
        for(j=1;j<=n;j++) {
            cum2[i][j] = cum2[i][j-1]+(arr[j]==i?1:0);
        }
    }

    for(i=1;i<=n;i++) {
        for(j=i;j<=n;j++) {
            xr=0;
            for(k=7;k>=0;k--) {
                bit=(cum[j][k]-cum[i-1][k])&1;
                xr=(xr<<1)+bit;
            }

            for(k=1;k<=100;k++) {
                if(cum2[k][j]!=cum2[k][i-1]) {
                    mn = k;
                    break;
                }
            }
            //printf("%d..\n",mn*xr);
            buc[mn*xr]++;
        }
    }

    gone=0;
    for(i=0;;i++) {
        gone+=buc[i];
        if(gone>=K)
        {
            printf("%d\n",i);
            break;
        }

    }

    return 0;
}

