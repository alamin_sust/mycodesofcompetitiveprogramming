/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int flag;
ll tp[10],col[10],pows[23],n,agg,k,arr[110];

int comp(ll a,ll b)
{
    return a>b;
}


int check_val(ll pos,string val)
{
    //printf("..%lld\n",val);
    int KK=pos-1LL;
    for(int i=1;i<=k;i++)
        col[i]=tp[i]=0LL;
    ll tttp;
    //memset(col,0LL,sizeof(col));
    while(KK>=0)
    {
        tttp=val[KK]-'0';
        col[tttp]=1LL;
        tp[tttp]+=arr[KK];
        KK--;
       // printf("%lld %lld\n",val%10LL,tp[val%10LL]);
        if(tp[tttp]>agg)
            return 0;
    }
   for(int i=1LL; i<=k; i++)
    {
        if(col[i]==0LL)
            return 1;
    }
    return 2;
}

void rec(ll pos,string val,vector<ll>vc)
{
   // printf("%lld %lld %lld\n",vc[1],vc[2],vc[3]);
   // cout<<val<<endl;
    if(flag==1)
        return;
    if(pos==n)
    {
        for(ll i=1LL;i<=k;i++)
        {
            if(vc[i]<0LL)
                return;
        }

        flag=1;
        return;
    }
    char j;
    for(ll i=1LL,j='1'; i<=k; j++,i++)
    {
        if(vc[i]+arr[pos]+(vc[i]==-1LL?1LL:0LL)<=agg)
        {
            ll tmp=vc[i];
            vc[i]+=arr[pos]+(vc[i]==-1LL?1LL:0LL);
            rec(pos+1,val+(char)j,vc);
            vc[i]=tmp;
        }
    }
    return;
}

int main()
{
    int t,p,i;
    //gen();
    cin>>t;
    for(p=1; p<=t; p++)
    {
        cin>>n>>k;
        agg=0LL;
        for(i=0; i<n; i++)
            scanf(" %lld",&arr[i]),agg+=arr[i];
        if((agg%k)!=0LL)
        {
            printf("no\n");
            continue;
        }
        sort(arr,arr+n,comp);
       // for(i=0;i<n;i++)
         // printf("%lld.. ",arr[i]);
        agg/=k;
        flag=0;
        vector<ll>vcc;
        for(i=0;i<=k;i++)
            vcc.push_back(-1LL);
        rec(0LL,"",vcc);
        if(flag)
            printf("yes\n");
        else
            printf("no\n");
    }
    return 0;
}

