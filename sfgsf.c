#include <iostream>
using namespace std;

void permute(char *a, int j = 0)
{
    int len = strlen(a);

    if(j == len)
    {
        cout << a << endl;
    }

    for(int i = j; i < len; i++)
    {
        char t = a[i];
        a[i] = a[j];
        a[j] = t;

        permute(a, j+1);
    }
}

int main()
{
    char a[] = "ABCD";
    permute(a);
    return 0;
}
