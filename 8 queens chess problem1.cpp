#include<stdio.h>
#include<iostream>
using namespace std;
int fres,hash,chess[11][11],input[11][11],colset[11],l_diag[20],r_diag[20],res[11],row,col;
void backTrack(int r)
{
   int c,m,n,tmax;
   if(r==9)
   {
      for(int i=1;i<=8;i++)
      {
         if(i==col && res[i]==row)
         {
            hash++;
            printf("%2ld",hash);
            cout<<"     ";
            for(int j=1;j<=8;j++)
            {
               cout<<" "<<res[j];
            }
            cout<<endl;
            break;
         }
      }
      if(fres<tmax)
      {
         fres=tmax;
      }
      return;
   }
   for(c=1;c<=8;c++)
   {
      if(chess[r][c]==0 && colset[c]==0 && l_diag[r+c]==0 && r_diag[r-c+8]==0)
      {
         chess[r][c]=1;
         colset[c]=1;
         l_diag[r+c]=1;
         r_diag[r-c+8]=1;
         res[r]=c;
         backTrack(r+1);
         chess[r][c]=0;
         colset[c]=0;
         l_diag[r+c]=0;
         r_diag[r-c+8]=0;
      }
   }
   return;
}
int main()
{
   int a;
   cin>>a;
      fres=0;
      for(int x=1;x<=a;x++)
      {
         cin>>row>>col;
         for(int i=0;i<10;i++)
         {
            for(int j=0;j<10;j++)
            {
               chess[i][j]=0;
            }
            colset[i]=0;
         }
         for(int i=0;i<=20;i++)
         {
            l_diag[i]=0;
            r_diag[i]=0;
         }
         cout<<"SOLN       COLUMN"<<endl;
         cout<<" #      1 2 3 4 5 6 7 8"<<endl<<endl;
         hash=0;
         backTrack(1);
         if(x!=a)
         {
            cout<<endl;
         }
      }
   return 0;
}
