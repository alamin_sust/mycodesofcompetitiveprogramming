/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))
#define MAX 1000002LL
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll sieve[MAX+10],dv,res,t,p,tp,n,k,m,kk,prime[MAX+10],cnt=0;

void sieve_(void)
{
    sieve[0]=sieve[1]=1;
    for(ll i=2;i<=MAX;i++)
    {
        if(sieve[i]==0)
        {
            prime[cnt++]=i;
            for(ll j=2;i*j<=MAX;j++)
                sieve[i*j]=1;
        }
    }
    return;
}


int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    ll i;
    sieve_();
    scanf(" %lld",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %lld %lld",&n,&k);
        kk=100LL;
        res=-1LL;
        m=n;
        while(kk--&&m>0)
        {
            n=m;
            dv=1;
            for(i=0;i<cnt;i++)
            {
                if((n%prime[i])==0)
                {
                tp=0;
                while(n>0&&(n%prime[i])==0)
                {
                    n/=prime[i];
                    tp++;
                }
                dv*=(tp+1LL);
                if(dv>k||(k%dv))
                    break;
                }
            }
            if(n>1)
            dv*=2LL;
            if(dv==k)
            {
                res=m;
                break;
            }
            m--;
        }
        printf("%lld\n",res);
    }

    return 0;
}

