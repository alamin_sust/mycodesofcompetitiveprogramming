#include <stdio.h>
#include <math.h>

int sieve[1001011]={0};

int main()
{
   unsigned long T, i, j;
   unsigned long KK=1,PRIM[100001],S=1,E,M;
      sieve[0]=1; sieve[1]=1;
        PRIM[0]=2;
      for(i=3 ; i<=1000000 ; i+=2)
      {
         sieve[i+1]=1;
            if(sieve[i]==0 && i<=1000)
            {
                  for(j=i*i ; j<=1001008 ; j+=2*i)
                  {
                        sieve[j]=1;
                  }
            }
                if(sieve[i]==0)
                {
                    PRIM[KK]=i;
                    KK++;
                }
      }
   scanf("%lu",&T);
   while(T--)
   {
      unsigned long L,U, I,key,k=0, ff,ll;
      unsigned long prim[100001];
      unsigned long dif[100001];

      scanf("%lu %lu",&L,&U);
      if(L%2==0) L+=1;

                key=L;
                S=1;
                E=KK;
                M=(int)((S+E)/2);
                while( (S<=E) && (PRIM[M]!=key) )
                {
                    if(PRIM[M]>key)
                        E=M-1;
                    else
                        S=M+1;
                    M=(int)((S+E)/2);
                }
                if(PRIM[M]==key)
                {
                    prim[k]=key;
                    k++;
                    ff=M;
                }
                else
                {
                    prim[k]=PRIM[M+1];
                    k++;
                    ff=M+1;
                }
      while(PRIM[ff]<=U)
      {
                prim[k]=PRIM[ff];
                k++;
                ff++;
      }

      if(k>3)
      {
         unsigned long v=0;
         for( I=0 ; I<k-1 ; I++)
         {
               v++;
               dif[I]=prim[I+1]-prim[I];
         }


            unsigned long cham,max=0,fag,temp;
            for( i=0 ; i<k-1 ; i++)
            {
                  temp=dif[i],fag=0;
                  for( j=0 ; j<k-1 ; j++)
                  {
                        if(temp==dif[j])
                           fag++;
                  }

                  if(fag>max)
                  {
                     max=fag;
                     cham=dif[i];
                  }
            }
            if(max>=2 && max!=v)
               printf("The jumping champion is %lu\n",cham);
            else
               printf("No jumping champion\n");

      }
      else
            printf("No jumping champion\n");

   }

   return 0;
}
