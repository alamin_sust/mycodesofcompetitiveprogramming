#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class FourStrings
{
public:
	int shortestLength(string a, string b, string c, string d)
	{
	    int i,j,k,l,l2,fg,res=100000010;
        string arr="abcd";
        string arr2[10];
        arr2[0]=a;
        arr2[1]=b;
        arr2[2]=c;
        arr2[3]=d;
        while(next_permutation(arr.begin(),arr.end()))
        {
            //cout<<arr<<endl;
            string str=arr2[arr[0]-'a'];
            l=str.length();
            l2=arr2[arr[1]-'a'].length();
           // flag=0;
            for(i=0;i<=l;i++)
            {
                fg=0;
                j=i;
                k=0;
                for(;k<l2&&j<l;j++,k++)
                {
                    if(str[j]!=arr2[arr[1]-'a'][k])
                    {
                        fg=1;
                        break;
                    }
                }
                if(fg==0)
                {
                    for(;k<l2;k++)
                    {
                        str+=arr2[arr[1]-'a'][k];
                    }
                    break;
                }
            }

            l=str.length();
            l2=arr2[arr[2]-'a'].length();
          //  flag=0;
            for(i=0;i<=l;i++)
            {
                fg=0;
                j=i;
                k=0;
                for(;k<l2&&j<l;j++,k++)
                {
                    if(str[j]!=arr2[arr[2]-'a'][k])
                    {
                        fg=1;
                        break;
                    }
                }
                if(fg==0)
                {
                    for(;k<l2;k++)
                    {
                        str+=arr2[arr[2]-'a'][k];
                    }
                    break;
                }
            }


            l=str.length();
            l2=arr2[arr[3]-'a'].length();
          //  flag=0;
            for(i=0;i<=l;i++)
            {
                fg=0;
                j=i;
                k=0;
                for(;k<l2&&j<l;j++,k++)
                {
                    if(str[j]!=arr2[arr[3]-'a'][k])
                    {
                        fg=1;
                        break;
                    }
                }
                if(fg==0)
                {
                    for(;k<l2;k++)
                    {
                        str+=arr2[arr[3]-'a'][k];
                    }
                    break;
                }
            }
            l=str.length();
            //cout<<str<<endl;
            res=min(res,l);
        }
        return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}
int main( int argc, char* argv[] )
{

	FourStrings objectFourStrings;

	//test case0
	string param00 = "abc";
	string param01 = "ab";
	string param02 = "bc";
	string param03 = "b";
	int ret0 = objectFourStrings.shortestLength(param00,param01,param02,param03);
	int need0 = 3;
	assert_eq(0,ret0,need0);

	//test case1
	string param10 = "a";
	string param11 = "bc";
	string param12 = "def";
	string param13 = "ghij";
	int ret1 = objectFourStrings.shortestLength(param10,param11,param12,param13);
	int need1 = 10;
	assert_eq(1,ret1,need1);

	//test case2
	string param20 = "top";
	string param21 = "coder";
	string param22 = "opco";
	string param23 = "pcode";
	int ret2 = objectFourStrings.shortestLength(param20,param21,param22,param23);
	int need2 = 8;
	assert_eq(2,ret2,need2);

	//test case3
	string param30 = "thereare";
	string param31 = "arelots";
	string param32 = "lotsof";
	string param33 = "ofcases";
	int ret3 = objectFourStrings.shortestLength(param30,param31,param32,param33);
	int need3 = 19;
	assert_eq(3,ret3,need3);

	//test case4
	string param40 = "aba";
	string param41 = "b";
	string param42 = "b";
	string param43 = "b";
	int ret4 = objectFourStrings.shortestLength(param40,param41,param42,param43);
	int need4 = 3;
	assert_eq(4,ret4,need4);

	//test case5
	string param50 = "x";
	string param51 = "x";
	string param52 = "x";
	string param53 = "x";
	int ret5 = objectFourStrings.shortestLength(param50,param51,param52,param53);
	int need5 = 1;
	assert_eq(5,ret5,need5);

}

