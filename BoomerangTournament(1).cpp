/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int now,n,b[110],w[110],arr[110][110];

void rec(string num,int round,int mask,int flag)
{
    b[now]=min(b[now],round);
    if((mask+1)==(1<<n)) return;
    int l=num.length();
    string str="";
    vector<int>v[18];
    for(int i=0; i<l; i++)
    {
        v[i].clear();
        for(int j=0; j<n; j++)
        {
            if((1<<j)&mask)
                continue;
            if(arr[num[i]-'0'][j])
                v[i].push_back(j);
        }
    }
    if(l==1)
    {
        for(int i=0; i<v[0].size(); i++)
        {
            int tpmask=mask;
            string tpstr="";

            tpstr+=num[0];
            tpstr+=(char)(v[0][i]+'0');
            tpmask=(tpmask|(1<<v[0][i]));

            rec(tpstr,(round==2&&flag==1)?1:(round/2+1),tpmask,1);
        }
    }
    else if(l==2)
    {
        for(int i=0; i<v[0].size(); i++)
        {
            for(int j=0; j<v[1].size(); j++)
            {
                int tpmask=mask;
                string tpstr="";

                tpstr+=num[0];
                tpstr+=(char)(v[0][i]+'0');
                tpmask=(tpmask|(1<<v[0][i]));

                tpstr+=num[1];
                tpstr+=(char)(v[1][j]+'0');
                tpmask=(tpmask|(1<<v[1][j]));

                rec(tpstr,(round==2&&flag==1)?1:(round/2+1),tpmask,1);
            }
        }
    }
    else if(l==4)
    {
        for(int i=0; i<v[0].size(); i++)
        {
            for(int j=0; j<v[1].size(); j++)
            {
                for(int x1=0; x1<v[2].size(); x1++)
                {
                    for(int x2=0; x2<v[3].size(); x2++)
                    {
                        int tpmask=mask;
                        string tpstr="";

                        tpstr+=num[0];
                        tpstr+=(char)(v[0][i]+'0');
                        tpmask=(tpmask|(1<<v[0][i]));

                        tpstr+=num[1];
                        tpstr+=(char)(v[1][j]+'0');
                        tpmask=(tpmask|(1<<v[1][j]));

                        tpstr+=num[2];
                        tpstr+=(char)(v[2][x1]+'0');
                        tpmask=(tpmask|(1<<v[2][x1]));

                        tpstr+=num[3];
                        tpstr+=(char)(v[3][x2]+'0');
                        tpmask=(tpmask|(1<<v[3][x2]));

                        rec(tpstr,(round==2&&flag==1)?1:(round/2+1),tpmask,1);
                    }
                }
            }
        }
    }
    else
    {
        for(int i=0; i<v[0].size(); i++)
        {
            for(int j=0; j<v[1].size(); j++)
            {
                for(int x1=0; x1<v[2].size(); x1++)
                {
                    for(int x2=0; x2<v[3].size(); x2++)
                    {
                        for(int x3=0; x3<v[4].size(); x3++)
                        {
                            for(int x4=0; x4<v[5].size(); x4++)
                            {
                                for(int x5=0; x5<v[6].size(); x5++)
                                {
                                    for(int x6=0; x6<v[7].size(); x6++)
                                    {
                                        int tpmask=mask;
                                        string tpstr="";

                                        tpstr+=num[0];
                                        tpstr+=(char)(v[0][i]+'0');
                                        tpmask=(tpmask|(1<<v[0][i]));

                                        tpstr+=num[1];
                                        tpstr+=(char)(v[1][j]+'0');
                                        tpmask=(tpmask|(1<<v[1][j]));

                                        tpstr+=num[2];
                                        tpstr+=(char)(v[2][x1]+'0');
                                        tpmask=(tpmask|(1<<v[2][x1]));

                                        tpstr+=num[3];
                                        tpstr+=(char)(v[3][x2]+'0');
                                        tpmask=(tpmask|(1<<v[3][x2]));

                                        tpstr+=num[4];
                                        tpstr+=(char)(v[4][x3]+'0');
                                        tpmask=(tpmask|(1<<v[4][x3]));

                                        tpstr+=num[5];
                                        tpstr+=(char)(v[5][x4]+'0');
                                        tpmask=(tpmask|(1<<v[5][x4]));

                                        tpstr+=num[6];
                                        tpstr+=(char)(v[6][x5]+'0');
                                        tpmask=(tpmask|(1<<v[6][x5]));

                                        tpstr+=num[7];
                                        tpstr+=(char)(v[7][x6]+'0');
                                        tpmask=(tpmask|(1<<v[7][x6]));

                                        rec(tpstr,(round==2&&flag==1)?1:(round/2+1),tpmask,1);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    int t,p,i,j;

    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {
        scanf(" %d",&n);

        for(i=0; i<n; i++)
        {
            for(j=0; j<n; j++)
            {
                scanf(" %d",&arr[i][j]);
            }
            w[i]=1;
            b[i]=n/2+1;
            for(j=0; j<n; j++)
            {
                if(i!=j&&arr[i][j]==0)
                {
                    w[i]=n/2+1;
                    break;
                }
            }
        }

        printf("Case #%d: \n",p);
        for(i=0; i<n; i++)
        {
            string temp="";
            temp+=(char)(i+'0');
            now=i;
            rec(temp,(n/2+1),(1<<i),0);
            printf("%d %d\n",b[i],w[i]);
        }
    }

    return 0;
}

