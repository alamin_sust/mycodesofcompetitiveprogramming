    #include<stdio.h>
    #include<math.h>


    long long int secureit(int n,int s);
    long long int arr_it(int n,int s);
    long long int secure[110][110];
    long long int arr[110][110];


    int main()
    {
       int n,s;
       long long int ans;
       while(1)
       {
          int i,j;
          scanf("%d %d",&n,&s);
          if(n<0 && s<0) break;
          for(i=0;i<100;i++)
             for(j=0;j<100;j++) secure[i][j]=-1;
          for(i=0;i<100;i++)
             for(j=0;j<100;j++) arr[i][j]=-1;
            ans=secureit(n,s);
          printf("%llu\n",ans);
       }
       return 0;
    }


    long long int arr_it(int n,int s)
    {
       if(s==n || s==n-1) return 1;
       if(n==2 && s==1) return 1;
       if(n==2 && (s==1 || s==2)) return 1;
       if(s>n) return 0;
       if(arr[n][s]!=-1) return arr[n][s];
       if(s==1)
       {
          arr[n][s]=(long long int) pow(2,n-2) - ((n-2)*(n-3))/2;
          return arr[n][s];
       }
       arr[n][s]=secureit(n-1,s-1);
       return arr[n][s];
    }

    long long int secureit(int n,int s)
    {

        if(s==n || s==n-1) return 1;
       if(n==2 && s==1) return 1;
       if(n==2 && (s==1 || s==2)) return 1;
       if(s>n) return 0;
       if(secure[n][s]!=-1) return secure[n][s];
       secure[n][s]=arr_it(n,s)+secureit(n-1,s)-arr_it(n-1,s)+arr_it(n-1,s+1);
       return secure[n][s];


    }
