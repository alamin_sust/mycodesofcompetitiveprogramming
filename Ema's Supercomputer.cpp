/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int rax,ray,rbx,cnt,n,m,rby,lax,lay,lbx,lby,uax,uay,ubx,uby,dax,day,dbx,dby,a,b,flag[16][16];
char arr[16][16];

int func(int xa,int ya,int xb,int yb)
{
    int ret=1;
    memset(flag,0,sizeof(flag));
    rax=lax=uax=dax=xa;
    ray=lay=uay=day=ya;
    cnt=1;
    a=1;
    b=1;
    while(1)
    {
        flag[rax][ray]=flag[lax][lay]=flag[uax][uay]=flag[dax][day]=1000000000;

        b=1;
        rbx=lbx=ubx=dbx=xb;
        rby=lby=uby=dby=yb;
        flag[rbx][rby]=flag[lbx][lby]=flag[ubx][uby]=flag[dbx][dby]=cnt;
        while(1)
        {
            ret=max(ret,a*b);
            flag[rbx][rby]=flag[lbx][lby]=flag[ubx][uby]=flag[dbx][dby]=cnt;
            rby++;
            lby--;
            ubx--;
            dbx++;
            if(rby>=m||lby<0||ubx<0||dbx>=n)
                break;
            if(arr[rbx][rby]!='G'||flag[rbx][rby]==1000000000)
                break;
            if(arr[lbx][lby]!='G'||flag[lbx][lby]==1000000000)
                break;
            if(arr[ubx][uby]!='G'||flag[ubx][uby]==1000000000)
                break;
            if(arr[dbx][dby]!='G'||flag[dbx][dby]==1000000000)
                break;
            b+=4;
        }

        ray++;
        lay--;
        uax--;
        dax++;
        if(ray>=m||lay<0||uax<0||dax>=n)
            break;
        if(arr[rax][ray]!='G'||flag[rax][ray]==cnt)
            break;
        if(arr[lax][lay]!='G'||flag[lax][lay]==cnt)
            break;
        if(arr[uax][uay]!='G'||flag[uax][uay]==cnt)
            break;
        if(arr[dax][day]!='G'||flag[dax][day]==cnt)
            break;
        cnt++;
        a+=4;
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    int i,j,k,l,res;

    scanf(" %d %d",&n,&m);

    for(i=0; i<n; i++)
    {
        scanf(" %s",&arr[i]);
    }

    res=0;

    for(i=0; i<n; i++)
    {
        for(j=0; j<m; j++)
        {
            for(k=0; k<n; k++)
            {
                for(l=0; l<m; l++)
                {
                    if(i==k&&j==l)
                        continue;
                    if(arr[i][j]=='G'&&arr[k][l]=='G')
                    {
                        res=max(res,func(i,j,k,l));
                    }
                }
            }
        }
    }

    printf("%d\n",res);

    return 0;
}
