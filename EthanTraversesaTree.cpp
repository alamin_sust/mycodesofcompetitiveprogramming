#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int pre[2010],post[2010],preCount,postCount,p,t,L[2010],R[2010],n,k,res[2010],col[2010];
queue<int>q;
vector<int>adj[2010];

void bfs(int u,int val) {
    if(adj[u].size() == 0) {
        res[u] = val;
        col[u]=1;
        return;
    }

    while(!q.empty()) {
        q.pop();
    }

    q.push(u);
    col[u]=1;
    res[u]=val;
    while(!q.empty()) {
        u=q.front();
        q.pop();
        for(int i=0; i<adj[u].size();i++) {
            int v = adj[u][i];
            if(col[v]==1) continue;
            q.push(v);
            col[v]=1;
            res[v]=val;
        }
    }
    return;
}


void preT(int node) {
    if(node<=n) {
        pre[preCount++] = node;
        if(L[node] != 0) {
            preT(L[node]);
        }
        if(R[node] != 0) {
            preT(R[node]);
        }
    }
    return;
}



void postT(int node) {
    if(node<=n) {
        if(L[node] != 0) {
            postT(L[node]);
        }
        if(R[node] != 0) {
            postT(R[node]);
        }
        post[postCount++] = node;
    }
    return;
}

main()
{
    freopen("ethan_traverses_a_tree.txt","r",stdin);
    freopen("ethan_traverses_a_tree_output.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++) {
        scanf(" %d %d",&n,&k);
        for(int i=1;i<=n;i++) {
            scanf(" %d %d",&L[i], &R[i]);
        }

        preCount = 1;
        postCount = 1;
        preT(1);
        postT(1);

        for(int i=0;i<=n;i++) {
            adj[i].clear();
        }

        for(int i=1;i<=n;i++) {
            if(pre[i]!=post[i]) {
                adj[pre[i]].push_back(post[i]);
                adj[post[i]].push_back(pre[i]);
            }
        }
        memset(col,0,sizeof(col));
        int now=0;
        for(int i=1;i<=n;i++) {
            if(col[i] == 1) continue;
            ++now;
            bfs(i,now);
        }
        printf("Case #%d:",p);
        if(now<k) {
            printf(" Impossible\n");
        } else {
            for(int i=1;i<=n;i++) {
                printf(" %d",min(res[i],k));
            }
            printf("\n");
        }
    }

    return 0;
}

