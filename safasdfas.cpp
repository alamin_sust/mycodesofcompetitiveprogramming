#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;

#define clrall(name,val) memset(name,(val),sizeof(name));
#define SF scanf
#define PF printf
#define fs first
#define sc second
#define mp make_pair
int dx[]={0,0,1,-1};
int dy[]={-1,1,0,0};


const int MAX=205;
int inf;
char mat[MAX][MAX];

int dist[MAX][MAX][2];
pair<int,int> star[MAX*MAX];
int idx,n,m;
queue<int> Q;

void init()
{
    clrall(dist,127);
    inf=dist[0][0][0];
    idx=0;
    while(!Q.empty()) Q.pop();
    return;
}

int bfs(int ex,int ey)
{
    int ux,uy,ust;
    int vx,vy,vst;
    bool flag;
    while(!Q.empty())
    {
        ux=Q.front();Q.pop();
        uy=Q.front();Q.pop();
        ust=Q.front();Q.pop();
        if(ux==ex&&uy==ey)return min(dist[ux][uy][0],dist[ux][uy][1]);
        flag=false;
        if(mat[ux][uy]=='*')
        {
            for(int i=0;i<idx;i++)
            {
                vx=star[i].fs;
                vy=star[i].sc;
                if(vx==ux&&vy==uy)flag=true;
                else
                {
                    if(dist[vx][vy][1]==inf)
                    {
                        dist[vx][vy][1]=dist[ux][uy][ust]+1;
                        Q.push(vx);
                        Q.push(vy);
                        Q.push(1);
                    }


                }
            }
            if(ust==1)
            {
                for(int i=0;i<4;i++)
                {
                    vx=dx[i]+ux;
                    vy=dy[i]+uy;
                    if(vx<0||vx==n||vy<0||vy==m||mat[vx][vy]=='#'||mat[vx][vy]=='*'||dist[vx][vy][1]!=inf) continue;
                    dist[vx][vy][1]=dist[ux][uy][ust]+1;
                    Q.push(vx);
                    Q.push(vy);
                    Q.push(1);
                }
            }
            idx=0;
            if(flag) star[idx++]=mp(ux,uy);
        }
        else
        {
            for(int i=0;i<4;i++)
            {
                vx=dx[i]+ux;
                vy=dy[i]+uy;
                if(vx<0||vx==n||vy<0||vy==m||mat[vx][vy]=='#'||dist[vx][vy][0]!=inf) continue;
                dist[vx][vy][0]=dist[ux][uy][ust]+1;
                Q.push(vx);
                Q.push(vy);
                Q.push(0);
            }
        }

    }
    return -1;
}



int main()
{
    int test,cas=0,ex,ey,res;
    SF("%d",&test);
    while(test--)
    {
        SF("%d %d",&n,&m);
        init();
        for(int i=0;i<n;i++)SF("%s",&mat[i]);
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<m;j++)
            {
                if(mat[i][j]=='*')star[idx++]=mp(i,j);
                else if(mat[i][j]=='P')Q.push(i),Q.push(j),Q.push(0),dist[i][j][0]=0;
                else if(mat[i][j]=='D') ex=i,ey=j;
            }
        }
        res=bfs(ex,ey);
        PF("Case %d: ",++cas);
        if(res==-1)
        {
            PF("impossible\n");
        }
        else
            PF("%d\n",res);
    }
    return 0;
}



