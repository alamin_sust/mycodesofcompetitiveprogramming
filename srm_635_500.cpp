#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class QuadraticLaw
{
public:
	long long getTime(long long d)
	{
	    long long low=0LL;
	    long long high=1000000000LL;
	    //high=(long long)(sqrt((double)high))-d;
	    long long mid,ret=0LL;
	    while(low<=high)
        {
            mid=(low+high)/2LL;
            if(mid*mid+mid<=d)
                ret=max(ret,mid);
            if(mid*mid+mid<=d)
                low=mid+1;
            else
                high=mid-1;
        }
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	QuadraticLaw objectQuadraticLaw;

	//test case0
	long long param00 = 1;
	long long ret0 = objectQuadraticLaw.getTime(param00);
	long long need0 = 0;
	assert_eq(0,ret0,need0);

	//test case1
	long long param10 = 2;
	long long ret1 = objectQuadraticLaw.getTime(param10);
	long long need1 = 1;
	assert_eq(1,ret1,need1);

	//test case2
	long long param20 = 5;
	long long ret2 = objectQuadraticLaw.getTime(param20);
	long long need2 = 1;
	assert_eq(2,ret2,need2);

	//test case3
	long long param30 = 6;
	long long ret3 = objectQuadraticLaw.getTime(param30);
	long long need3 = 2;
	assert_eq(3,ret3,need3);

	//test case4
	long long param40 = 7;
	long long ret4 = objectQuadraticLaw.getTime(param40);
	long long need4 = 2;
	assert_eq(4,ret4,need4);

	//test case5
	long long param50 = 1482;
	long long ret5 = objectQuadraticLaw.getTime(param50);
	long long need5 = 38;
	assert_eq(5,ret5,need5);

	//test case6
	long long param60 = 1000000000000000000;
	long long ret6 = objectQuadraticLaw.getTime(param60);
	long long need6 = 999999999;
	assert_eq(6,ret6,need6);

	//test case7
	long long param70 = 31958809614643170;
	long long ret7 = objectQuadraticLaw.getTime(param70);
	long long need7 = 178770270;
	assert_eq(7,ret7,need7);
}
