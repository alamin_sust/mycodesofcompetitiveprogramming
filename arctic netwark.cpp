#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) ( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
ll x,y,val;
};
node det;

bool operator<(node a,node b)
{
    return a.val>b.val;
}

priority_queue<node>pq;

main()
{
    ll t,p,i,n,k,x[1010],y[1010],j,par[1010];
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>k>>n;
        for(i=1;i<=n;i++)
            par[i]=i;
        for(i=1;i<=n;i++)
            scanf(" %lld %lld",&x[i],&y[i]);
        for(i=1;i<=n;i++)
        {
            for(j=i+1;j<=n;j++)
            {
                det.x=i;
                det.y=j;
                det.val=dist(x[i],y[i],x[j],y[j]);
                pq.push(det);
                swap(det.x,det.y);
                pq.push(det);
            }
        }
        while(1)
        {
            det=pq.top();
            pq.pop();
            while(par[det.x]!=det.x)
                det.x=par[det.x];
            while(par[det.y]!=det.y)
                det.y=par[det.y];
            if(det.x!=det.y)
                par[det.x]=det.y,n--;
            if(n==k)
            {
                printf("%.2lf\n",sqrt((double)det.val));
                break;
            }
        }
        while(!pq.empty())
            pq.pop();
    }
    return 0;
}

