
#include<iostream>

#define FOR(i, s, e) for(int i=s; i<e; i++)
#define loop(i, n) for(int i=0; i<n; i++)

#define getint(n) scanf("%d", &n)

#define MAXX 101
using namespace std;
double result[MAXX];
int input[MAXX];
int number_of_integers;
bool visited[MAXX];

double rec(int pos)
{
    if(pos >= number_of_integers)
    {
        return 0;
    }
    if(pos + 1 == number_of_integers)
    {
        return input[pos];
    }

    if( visited[pos] ) return result[pos];
    visited[pos] = true;
    double &ret = result[pos];
    double by;

    if(number_of_integers - pos - 1 >= 6)
    {
        by = 6;
    }
    else
    {
        by = number_of_integers - pos - 1;
    }


    ret = input[pos];

    FOR(i, 1, 7)

    {

        ret = ret + rec(pos + i)/by;

    }
    return ret;

}
int main()
{
    int kases, kaseno = 0;
    getint(kases);
    while(kases--)

    {
        getint(number_of_integers);
        loop(i, MAXX) visited[i] = false;

        loop(i, number_of_integers)

        {

            getint(input[i]);
        }
        printf("Case %d: %.6lf\n", ++kaseno, rec(0));
    }
    return 0;
}
