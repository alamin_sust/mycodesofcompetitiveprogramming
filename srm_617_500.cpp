#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include<string.h>

using namespace std;

class SlimeXSlimonadeTycoon
{
public:
	int sell(vector <int> morning, vector <int> customers, int stale_limit)
	{
	    int ret=0,prod[55];
	    memset(prod,0,sizeof(prod));
	    for(int i=0;i<morning.size();i++)
        {
            prod[i]+=morning[i];
            for(int j=max(0,i-stale_limit+1);j<=i;j++)
            {
                if(customers[i]>=prod[j])
                    ret+=prod[j],customers[i]-=prod[j],prod[j]=0;
                else
                    {ret+=customers[i],prod[j]-=customers[i];
                    break;}
            }
        }
	    return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	SlimeXSlimonadeTycoon objectSlimeXSlimonadeTycoon;

	//test case0
	vector <int> param00;
	param00.push_back(5);
	param00.push_back(1);
	param00.push_back(1);
	vector <int> param01;
	param01.push_back(1);
	param01.push_back(2);
	param01.push_back(3);
	int param02 = 2;
	int ret0 = objectSlimeXSlimonadeTycoon.sell(param00,param01,param02);
	int need0 = 5;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(10);
	param10.push_back(20);
	param10.push_back(30);
	vector <int> param11;
	param11.push_back(30);
	param11.push_back(20);
	param11.push_back(10);
	int param12 = 1;
	int ret1 = objectSlimeXSlimonadeTycoon.sell(param10,param11,param12);
	int need1 = 40;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(1);
	param20.push_back(2);
	param20.push_back(3);
	param20.push_back(4);
	param20.push_back(5);
	vector <int> param21;
	param21.push_back(5);
	param21.push_back(5);
	param21.push_back(5);
	param21.push_back(5);
	param21.push_back(5);
	int param22 = 5;
	int ret2 = objectSlimeXSlimonadeTycoon.sell(param20,param21,param22);
	int need2 = 15;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(10000);
	param30.push_back(0);
	param30.push_back(0);
	param30.push_back(0);
	param30.push_back(0);
	param30.push_back(0);
	param30.push_back(0);
	vector <int> param31;
	param31.push_back(1);
	param31.push_back(2);
	param31.push_back(4);
	param31.push_back(8);
	param31.push_back(16);
	param31.push_back(32);
	param31.push_back(64);
	int param32 = 4;
	int ret3 = objectSlimeXSlimonadeTycoon.sell(param30,param31,param32);
	int need3 = 15;
	assert_eq(3,ret3,need3);

}
