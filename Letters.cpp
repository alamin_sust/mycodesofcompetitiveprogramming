/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<string>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int rr[]= {0,0,1,-1};
int cc[]= {-1,1,0,0};

struct node
{
    int x,y;
};

char arr[110][110];
int val[110][110],stat[105][105],n;

queue<node>q;
node det,u,v;

int bfs(int mask)
{
    if(arr[1][1]>='a'&&arr[1][1]<='z')
    {
        if(((1<<(arr[1][1]-'a'))&mask)==0)
            return 99999999;
    }
    else if(arr[1][1]>='A'&&arr[1][1]<='Z')
    {
        if(((1<<(arr[1][1]-'A'))&mask)!=0)
            return 99999999;
    }
    while(!q.empty())
        q.pop();
    det.x=1;
    det.y=1;
    q.push(det);
    stat[1][1]=mask;
    val[1][1]=1;
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(int i=0; i<4; i++)
        {
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            if(v.x<1||v.y<1||v.x>n||v.y>n||stat[v.x][v.y]==mask)
                continue;
            if(arr[v.x][v.y]>='a'&&arr[v.x][v.y]<='z')
            {
                if(((1<<(arr[v.x][v.y]-'a'))&mask)!=0)
                {
                    val[v.x][v.y]=val[u.x][u.y]+1;
                    if(v.x==n&&v.y==n)
                        return val[v.x][v.y];
                    q.push(v);
                    stat[v.x][v.y]=mask;
                }
            }
            else if(arr[v.x][v.y]>='A'&&arr[v.x][v.y]<='Z')
            {
                if(((1<<(arr[v.x][v.y]-'A'))&mask)==0)
                {
                    val[v.x][v.y]=val[u.x][u.y]+1;
                    if(v.x==n&&v.y==n)
                        return val[v.x][v.y];
                    q.push(v);
                    stat[v.x][v.y]=mask;
                }
            }

        }
    }
    return 99999999;
}

int main()
{
    int i,j,l,res;
    while(scanf("%d",&n)==1)
    {
        getchar();
        for(i=1; i<=n; i++)
        {
            for(j=1; j<=n; j++)
            {
                scanf("%c",&arr[i][j]);
            }
            getchar();
        }
        l=(1<<10);
        res=99999999;
        memset(stat,-1,sizeof(stat));
        for(i=0; i<l; i++)
        {
            res=min(res,bfs(i));
        }
        if(res==99999999)
            printf("-1\n");
        else
            printf("%d\n",res);
    }
    return 0;
}


