#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int a,b,i,res=inf,dp[10][210];

int rec(int a,int b,int k,int val)
{
        if(dp[k][val]!=-1)
            return dp[k][val];
        if(a==b)
        return dp[k][val]=val;
        if(a>b&&a%k==0)
           {
           res=min(res,rec( a/k,b,2,val+1));
           res=min(res,rec( a/k,b,3,val+1));
           res=min(res,rec( a/k,b,5,val+1));
           }
        else if(b>a&&b%k==0)
           {
           res=min(res,rec( a,b/k,2,val+1));
           res=min(res,rec( a,b/k,3,val+1));
           res=min(res,rec( a,b/k,5,val+1));
           }
        return dp[k][val]=val;
}

main()
{
    cin>>a>>b;
    memset(dp,-1,sizeof(dp));
    res=min(res,rec(a,b,2,0));
    res=min(res,rec(a,b,3,0));
    res=min(res,rec(a,b,5,0));
    if(res!=inf)
        printf("%d\n",res);
    else
        printf("-1\n");
    return 0;
}

