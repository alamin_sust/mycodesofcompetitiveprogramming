/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll p,t,i,l,res,M=1000000007LL;
char arr[10010];

int main()
{
    freopen("A-large0.in","r",stdin);
    freopen("lazyout2.txt","w",stdout);

    scanf(" %lld",&t);
    for(p=1LL;p<=t;p++)
    {
        scanf(" %s",arr);
        l=strlen(arr);
        res=1LL;
        if(l==1LL)
        {
            printf("Case #%lld: 1\n",p);
            continue;
        }
        for(i=0LL;i<l;i++)
        {
            if(i==0LL)
            {
                if(arr[i]!=arr[i+1])
                    res=(res*2LL)%M;
            }
            else if(i==(l-1LL))
            {
                if(arr[i]!=arr[i-1])
                    res=(res*2LL)%M;
            }
            else
            {
                if(arr[i]!=arr[i+1]&&arr[i]!=arr[i-1]&&arr[i+1]!=arr[i-1])
                    res=(res*3LL)%M;
                else if(arr[i]==arr[i+1]&&arr[i+1]==arr[i-1])
                {
                    res=(res*1LL)%M;
                }
                else
                    res=(res*2LL)%M;
            }
        }
        printf("Case #%lld: %lld\n",p,res);
    }
    return 0;
}
