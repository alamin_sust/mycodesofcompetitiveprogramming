#include<stdio.h>
#include<math.h>
#include<map>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
#define inf 1000000
using namespace std;

priority_queue <int> que;
queue <int> res;
map<string,int>mp;

int n,m,i,arr[110][110],tot[110],a,j,p=1;
char from[60],to[60],name[100010][60];

main()
{
    while(cin>>n)
    {
        memset(arr,0,sizeof(arr));
        for(i=1; i<=n; i++)
            scanf(" %s",name[i]),mp[name[i]]=i,tot[i]=0;
        scanf(" %d",&m);
        for(i=1; i<=m; i++)
        {
            scanf(" %s %s",&to,&from);
            if(arr[mp[from]][mp[to]]==0)
            {arr[mp[from]][mp[to]]=1;
            tot[mp[from]]++;}
        }
        for(i=1; i<=n; i++)
        {
            if(tot[i]==0)
                que.push(inf-i);
        }
        while(!que.empty())
        {
            a=que.top();
            que.pop();
            a=inf-a;
            res.push(a);
            for(i=1; i<=n; i++)
            {
                if(arr[i][a]==1)
                {
                    arr[i][a]=0;
                    tot[i]--;
                    if(tot[i]==0)
                        que.push(inf-i);
                }
            }
        }
        printf("Case #%d: Dilbert should drink beverages in this order: ",p++);
        while(!res.empty())
        {
            printf("%s",name[res.front()]);
            res.pop();
            if(res.empty()==1)
                printf(".");
            else
                printf(" ");
        }
        printf("\n\n");
    }
    return 0;
}


