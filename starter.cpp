/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int n,arr[100010],i,res,k;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&n);


    for(i=1;i<=n;i++)
    {
        scanf(" %d",&arr[i]);

    }
    res=1;
    k=1;
    for(i=2;i<=n;i++)
    {
        if(arr[i]>=arr[i-1])
        {
            k++;
            res=max(res,k);
        }
        else
        {
            k=1;
        }
    }
    res=max(res,k);

    printf("%d\n",res);

    return 0;
}
