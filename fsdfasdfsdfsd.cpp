/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define FORN(n) for(int p=0;p<n;p++)
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

main()
{
    int k;
    int t=100;
    while(t--)
    {
        cin>>k;
        printf("+------------------------+\n");
        int x=(k<=4)?(k>0?1:0):(k-5)/3+2;
        int r=(k<=4)?(k>2?k-1:k):(k-5)%3+1;

        printf("|");
        FORN(x)
        printf("O.");
        FORN(11-x)
        printf("#.");
        printf("|D|)\n");

        printf("|");

        int z=x-1+((r>=2)?1:0);
        z=z>0?z:0;

        FORN(z)
        printf("O.");
        FORN(11-z)
        printf("#.");
        printf("|.|\n");

        printf("|%c............................|\n",(k>=3)?'O':'#');
        z=x-1+((r>=3)?1:0);
        z=z>0?z:0;
        printf("|");
        FORN(z)
        printf("O.");
        FORN(11-z)
        printf("#.");
        printf("|.|)\n");

    }

    return 0;
}

