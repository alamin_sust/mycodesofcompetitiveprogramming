#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int dp[1<<15],adj[15][15],n;

int rec(int mask)
{
    //cout<<mask;
    if(mask==((1<<n)-1))
        return 0;
    int &ret=dp[mask];
    if(ret!=-1)
        return ret;
    ret=1<<28;
    for(int i=0;i<n;i++)
    {
        if(((1<<i)&mask)==0)
        {
            int price=adj[i][i];
            for(int j=0;j<n;j++)
            {
                if(i!=j&&((1<<j)&mask))
                {
                    price+=adj[i][j];
              //      printf("v%d\n",price);
                }
            }
            ret=min(ret,price+rec((1<<i)|mask));
            //printf("..%d\n",1<<i|mask);
        }
    }
    return ret;
}

main()
{
    int t,p,i,j;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        memset(dp,-1,sizeof(dp));
        cin>>n;
        for(i=0;i<n;i++)
        {
            for(j=0;j<n;j++)
                cin>>adj[i][j];
        }
        printf("Case %d: %d\n",p,rec(0));
    }
    return 0;
}

