/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int k,dp[103][10][2][53],t,p,l,M=1000000007,res;
char arr[110];

int rec(int pos,int prev,int flag,int ek)
{
    //printf("%d %d %d\n",pos,prev,ek);
    if(pos==l)
        return ek;
    int &ret=dp[pos][prev][flag][ek];
    if(ret!=-1)
        return ret;
    ret=0;
    int i=0;
    for(i=0; i<arr[pos]-'0'; i++)
    {
        if(prev==2&&i==1)
            ret=(ret+rec(pos+1,i,1,ek+1))%M;
        else
            ret=(ret+rec(pos+1,i,1,ek))%M;
    }
    if(flag==0)
    {
        if(prev==2&&i==1)
            ret=(ret+rec(pos+1,i,0,ek+1))%M;
        else
            ret=(ret+rec(pos+1,i,0,ek))%M;
    }
    else
    {
        for(; i<=9; i++)
        {
            if(prev==2&&i==1)
                ret=(ret+rec(pos+1,i,1,ek+1))%M;
            else
                ret=(ret+rec(pos+1,i,1,ek))%M;
        }
    }
    return ret;
}

void mm(void)
{
    for(int i1=0;i1<=l;i1++)
    {
        for(int i2=0;i2<10;i2++)
        {
            for(int i3=0;i3<2;i3++)
            {
                for(int i4=0;i4<=(l>>1);i4++)
                    dp[i1][i2][i3][i4]=-1;
            }
        }
    }
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    cin>>t;

    for(p=1; p<=t; p++)
    {
        scanf(" %s",&arr);
        l=strlen(arr);
        k=0;
        for(int i=1;i<l;i++)
        {
            if(arr[i]=='1'&&arr[i-1]=='2')
                k++;
        }
        mm();
        //memset(dp,-1,sizeof(dp));
        res=rec(0,0,0,0);
        scanf(" %s",&arr);
        l=strlen(arr);
        mm();
        //memset(dp,-1,sizeof(dp));
        res=rec(0,0,0,0)-res;
        if(res<0)
            res=(res+M)%M;
        res=(res+k)%M;
        printf("%d\n",res);
    }
    return 0;
}




