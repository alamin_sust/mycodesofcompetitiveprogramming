#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll arr[510],k,m;
vector<ll>vc[510];
bool func(ll mid)
{
    ll i,cnt=0,cum=0;
    for(i=1;cnt<k&&i<=m;i++)
    {
        cum+=arr[i];
        if(cum>mid)
            cum=0,cnt++,i--;
    }
    if(cnt<=k&&i>m)
        return true;
    return false;
}

void prnt(ll mid)
{
    ll i,j,cum=0;
    for(i=0;i<=500;i++)
        vc[i].clear();
    for(j=k,i=m;i>=1;i--)
    {
        if(j<=i&&(cum+arr[i])<=mid)
            vc[j].push_back(arr[i]),cum+=arr[i];
        else
            i++,j--,cum=0;
    }
    return;
}

main()
{
    ll t,p,i,j,low,high,mid;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>m>>k;
        for(i=1;i<=m;i++)
        {
            scanf(" %lld",&arr[i]);
            //cum[i]=arr[i]+cum[i-1];
        }
        low=0;
        high=10000000LL*510LL;
        mid=(low+high)/2;
        while(1)
        {
            if(func(mid)==true&&func(mid-1)==false)
            {
                break;
            }
            if(func(mid)==true)
                high=mid;
            else
                low=mid;
            mid=(low+high)/2;
        }
        prnt(mid);
        for(i=1;i<=k;i++)
        {
            for(j=vc[i].size()-1;j>=0;j--)
            {
                if(i==k&&j==0)
                    printf("%lld\n",vc[i][j]);
                else
                    printf("%lld ",vc[i][j]);
            }
            if(i!=k)
                printf("/ ");
        }
        //printf(" %lld\n",mid);
    }
    return 0;
}
