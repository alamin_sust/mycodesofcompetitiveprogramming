#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#define ll long long int

using namespace std;

class AliceGameEasy
{
public:
	long long findMinimumValue(long long x, long long y)
	{
	    ll sum,flag=0LL,k=0,cnt=0LL,i,z;
	    z=x+y;
	    for(i=0LL;;i++)
        {
            sum=i*(i+1LL)/2LL;
            if(sum==z)
                {flag=1LL;
                k=i;
                break;}
            if(sum>z)
                break;
        }
        if(flag==0LL)
            return -1LL;
        for(i=k;x>0LL&&i>0LL;i--)
        {
            if(x>=i)
            {
                x-=i;
                cnt++;
            }
        }
        return cnt;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	AliceGameEasy objectAliceGameEasy;

	//test case0
	long long param00 = 7;
	long long param01 = 14;
	long long ret0 = objectAliceGameEasy.findMinimumValue(param00,param01);
	long long need0 = 2;
	assert_eq(0,ret0,need0);

	//test case1
	long long param10 = 10;
	long long param11 = 0;
	long long ret1 = objectAliceGameEasy.findMinimumValue(param10,param11);
	long long need1 = 4;
	assert_eq(1,ret1,need1);

	//test case2
	long long param20 = 932599670050;
	long long param21 = 67400241741;
	long long ret2 = objectAliceGameEasy.findMinimumValue(param20,param21);
	long long need2 = 1047062;
	assert_eq(2,ret2,need2);

	//test case3
	long long param30 = 7;
	long long param31 = 13;
	long long ret3 = objectAliceGameEasy.findMinimumValue(param30,param31);
	long long need3 = -1;
	assert_eq(3,ret3,need3);

	//test case4
	long long param40 = 0;
	long long param41 = 0;
	long long ret4 = objectAliceGameEasy.findMinimumValue(param40,param41);
	long long need4 = 0;
	assert_eq(4,ret4,need4);

	//test case5
	long long param50 = 100000;
	long long param51 = 400500;
	long long ret5 = objectAliceGameEasy.findMinimumValue(param50,param51);
	long long need5 = 106;
	assert_eq(5,ret5,need5);
     return 0;
}

