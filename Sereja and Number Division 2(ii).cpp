
/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll k2,j,rem[110],perm[10],t,l,p,i,res,res2,tp,num2,k,num[13],flag,n,val[110];
char arr[100010];


ll func(ll pos,ll number)
{
    if(pos==k2)
        return number%val[i];
    return (number*10LL+(arr[pos]-'0'))%val[i];
}


int main()
{
    scanf(" %lld",&t);
    for(p=1LL; p<=t; p++)
    {
        scanf(" %s",arr);
        l=strlen(arr);
        memset(num,0LL,sizeof(num));

        for(i=0LL; i<l; i++)
        {
            num[arr[i]-'0']++;
        }
        if(l<=7LL)
        {
            for(i=0LL; i<l; i++)
                perm[i]=arr[i]-'0',num[arr[i]-'0']--;
            k=l;
        }
        else
        {
            flag=k=0LL;
            while(k<7LL)
            {
                for(i=9LL;k<7LL&&i>=1LL; i--)
                {
                    if(num[i])
                    {
                        num[i]--;
                        perm[k++]=i;
                    }
                }
                //flag^=1LL;
            }
        }
        sort(perm,perm+k);
        scanf(" %lld",&n);
        for(i=1LL; i<=n; i++)
        {
            scanf(" %lld",&val[i]);
        }

        k2=0LL;
        for(k2=0LL,i=1LL;i<=9LL;i++)
        {
            while(num[i]>0LL)
            {
                num[i]--;
                arr[k2++]=i+'0';
            }
        }
        for(i=1LL;i<=n;i++)
        {
            rem[i]=func(0LL,0LL);
            for(j=0LL;j<k;j++)
                {
                    rem[i]*=10LL;
                }
            //printf("%lld...\n",rem[i]);
        }

        res=99999999999999LL;
        do
        {
            // for(i=0;i<k;i++)
            //    printf("%lld",perm[i]);
            // printf("\n");
            tp=0LL;
            num2=0LL;
            for(i=0LL;i<k;i++)
            {
                num2=(num2*10LL)+perm[i];
            }

            for(i=1LL;i<=n;i++)
            {
                //printf("%lld %lld--",rem[i],num2);
                tp+=(rem[i]+num2)%val[i];
            }
            if(res>tp)
            {
                res=tp;
                res2=num2;
            }
        }
        while(next_permutation(perm,perm+k));

        for(i=0LL;i<k2;i++)
            printf("%c",arr[i]);
        printf("%lld\n",res2);
    }
    return 0;
}



