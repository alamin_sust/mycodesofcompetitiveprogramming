/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll q,k,x,MOD=1000000007LL;
ll fib[55];


void gen_fib(void)
{
    fib[0]=1LL;
    fib[1]=2LL;
    for(ll i=2LL;i<=51LL;i++)
    {
        fib[i]=fib[i-1]+fib[i-2];
    }
    return;
}

ll rec(ll rem_val,ll pos,ll baki)
{
    if(baki==0)
    {
        if(rem_val==0)
        return 1LL;
        return 0LL;
    }
    if(pos>50)
        return 0;
    if((fib[pos]*baki)>rem_val)
        return 0;
    ll ret=0LL;
    ret=(ret+rec(rem_val,pos+1LL,baki))%MOD;
    ret=(ret+rec(rem_val-fib[pos],pos,baki-1LL))%MOD;
    //ret=(ret+rec(rem_val-fib[pos],pos+1LL,baki-1LL))%MOD;
   /* for(ll i=baki;i>=0LL;i--)
    {
        if((fib[pos]*(baki-i))<=rem_val)
        ret=(ret+rec(rem_val-(fib[pos]*(baki-i)),pos+1LL,i))%MOD;
        else
            break;
    }*/
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    gen_fib();



    ll t;
    scanf(" %lld",&t);
    for(ll p=1LL;p<=t;p++)
    {
        scanf(" %lld %lld",&x,&k);
        printf("%lld\n",rec(x,0LL,k));
    }

    return 0;
}

