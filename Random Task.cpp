#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(a,b) memset(a,b,sizeof(a))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
ll dp[65][65][3];
ll p,k,m;
ll digit_dp(ll pos,ll ones,ll flag)
{
    if(pos==-1)
    {
        if(ones==k)
            return 1;
        return 0;
    }
    ll &ret=dp[pos][ones][flag];
    if(ret!=-1)
        return ret;
    ret=0;
    if(flag==1)
    {
        ret+=digit_dp(pos-1,ones+1,flag);
        ret+=digit_dp(pos-1,ones,flag);
    }
    else
    {
        if((p>>pos)&1LL==1)
        {
            ret+=digit_dp(pos-1,ones+1,0);
            ret+=digit_dp(pos-1,ones,1);
        }
        else
        {
            ret+=digit_dp(pos-1,ones,0);
        }
    }
    return ret;
}

main()
{
    ll low,ans,high,mid,res;
    cin>>m>>k;
    low=0LL;
    high=1000000000000000001LL;

    while(1)
    {
        mid=(low+high)/2LL;
        p=2LL*mid;
        ms(dp,-1);
        res=digit_dp(63,0,0);
        p/=2LL;
        ms(dp,-1);
        res-=digit_dp(63,0,0);
        if(res==m)
        {
            ans=mid;
            break;
        }
        else if(res>m)
        {
            high=mid;
        }
        else
            low=mid;
    }
    if(m==1&&k==1)
        mid=1;
    cout<<mid<<endl;
    return 0;
}
