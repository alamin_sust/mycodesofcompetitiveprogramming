/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

queue<int>q;
vector<int>adj[30];
int n,m,from[30],to[30],cost[30],stat[30],fg[30][30];

int bfs(void)
{
    while(!q.empty())
        q.pop();

    q.push(1);
    stat[1]=1;

    while(!q.empty())
    {
        int u=q.front();
        q.pop();
        for(int i=0;i<adj[u].size();i++)
        {
            int v=adj[u][i];
            if(fg[u][v]==1)
                continue;
            if(stat[v])
                continue;
            stat[v]=1;
            q.push(v);
        }
    }

    int flag=1;
    for(int i=1;i<=n;i++)
    {
        if(stat[i]==0)
            flag=0;
        stat[i]=0;
    }
    return flag;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    int i,j,p=1;
    while(scanf("%d%d",&n,&m)!=EOF)
    {
        if(n==0&&m==0)
            break;
        for(i=0;i<m;i++)
        {
            scanf(" %d %d %d",&from[i],&to[i],&cost[i]);
        }
        int res=1000000010;
        for(i=0;i<(1<<m);i++)
        {
            if(__builtin_popcount(i)==n)
            {
                for(j=1;j<=n;j++)
                    adj[j].clear();
                int tp=0;
                for(j=0;j<m;j++)
                {
                    if((1<<j)&i)
                    {
                        tp+=cost[j];
                        adj[from[j]].push_back(to[j]);
                        adj[to[j]].push_back(from[j]);
                    }
                }
                int flag=0;
                for(j=0;j<m;j++)
                {
                    if((1<<j)&i)
                    {
                        fg[from[j]][to[j]]=fg[to[j]][from[j]]=1;
                        if(!bfs())
                        {
                            flag=1;
                            fg[from[j]][to[j]]=fg[to[j]][from[j]]=0;
                            break;
                        }
                        fg[from[j]][to[j]]=fg[to[j]][from[j]]=0;
                    }
                }
                if(flag==0)
                    res=min(res,tp);
            }
        }
        if(res<1000000010)
        printf("The minimal cost for test case %d is %d.\n",p++,res);
        else
        printf("There is no reliable net possible for test case %d.\n",p++);
    }

    return 0;
}


