#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class Cyclemin
{
public:
	string bestmod(string s, int k)
	{
	    string ret="",temp="";
	    for(int i=0;i<s.size();i++)
        {
            temp="";
            for(int cnt=0,p=0,j=i;p<s.size();p++,j++)
            {
                if(j>=s.size())
                    j=0;
                //printf("%d..\n",j);
                if(cnt<k&&s[j]>'a')
                {
                    cnt++;
                    temp+="a";
                }
                else
                {
                    temp+=s[j];
                }
            }
            if(ret.size()==0||ret>temp)
                ret=temp;
        }
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	Cyclemin objectCyclemin;

	//test case0
	string param00 = "aba";
	int param01 = 1;
	string ret0 = objectCyclemin.bestmod(param00,param01);
	string need0 = "aaa";
	assert_eq(0,ret0,need0);

	//test case1
	string param10 = "aba";
	int param11 = 0;
	string ret1 = objectCyclemin.bestmod(param10,param11);
	string need1 = "aab";
	assert_eq(1,ret1,need1);

	//test case2
	string param20 = "bbb";
	int param21 = 2;
	string ret2 = objectCyclemin.bestmod(param20,param21);
	string need2 = "aab";
	assert_eq(2,ret2,need2);

	//test case3
	string param30 = "sgsgaw";
	int param31 = 1;
	string ret3 = objectCyclemin.bestmod(param30,param31);
	string need3 = "aasgsg";
	assert_eq(3,ret3,need3);

	//test case4
	string param40 = "abacaba";
	int param41 = 1;
	string ret4 = objectCyclemin.bestmod(param40,param41);
	string need4 = "aaaabac";
	assert_eq(4,ret4,need4);

	//test case5
	string param50 = "isgbiao";
	int param51 = 2;
	string ret5 = objectCyclemin.bestmod(param50,param51);
	string need5 = "aaaisgb";
	assert_eq(5,ret5,need5);

}
