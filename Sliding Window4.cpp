/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node{
int ind,val;
};
node det;

struct node2{
int ind,val;
};
node2 det2;

bool operator<(node a,node b)
{
    return a.val>b.val;
}

bool operator<(node2 a,node2 b)
{
    return a.val<b.val;
}

priority_queue<node>mnpq;
priority_queue<node2>mxpq;

int mn[1000010],mx[1000010],arr[1000010];
int j,n,k,i;

int main()
{
    scanf(" %d %d",&n,&k);
    for(i=1;i<=n;i++)
    {
        scanf(" %d",&arr[i]);
    }
    for(i=1;i<=k;i++)
    {
        det.ind=i;
        det.val=arr[i];
        det2.ind=i;
        det2.val=arr[i];
        mnpq.push(det);
        mxpq.push(det2);
    }
    mn[1]=mnpq.top().val;
    mx[1]=mxpq.top().val;
    for(j=2,i=k+1;i<=n;j++,i++)
    {
        det.ind=i;
        det.val=arr[i];
        det2.ind=i;
        det2.val=arr[i];
        mnpq.push(det);
        mxpq.push(det2);
        while(mnpq.top().ind+k<=i)
            mnpq.pop();
        while(mxpq.top().ind+k<=i)
            mxpq.pop();
        mn[j]=mnpq.top().val;
        mx[j]=mxpq.top().val;
    }
    for(i=1;i<j;i++)
    {
        printf("%d",mn[i]);
        if(i==(j-1))
            printf("\n");
        else
            printf(" ");
    }
    for(i=1;i<j;i++)
    {
        printf("%d",mx[i]);
        if(i==(j-1))
            printf("\n");
        else
            printf(" ");
    }
    return 0;
}

