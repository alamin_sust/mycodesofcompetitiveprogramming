/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll p[110],P,t,nowe,nowb,st,en,n,flag,i,f[110];

int main()
{
    freopen("B-large.in","r",stdin);
    freopen("B-largeout.txt","w",stdout);

    scanf(" %lld",&t);

    for(P=1LL; P<=t; P++)
    {
        flag=0LL;
        scanf(" %lld",&n);
        nowb=0LL;
        nowe=100000000000000010LL;
        for(i=1LL; i<=n; i++)
        {
            scanf(" %lld %lld",&p[i],&f[i]);
            if(p[i])
            en=(100LL*f[i])/p[i];
            else
            en=nowe;
            if(p[i]!=100LL)
            {
                if(((100LL*f[i])%(p[i]+1LL))==0LL)
                    st=((100LL*f[i])/(p[i]+1LL))+1LL;
                else
                    st=((100LL*f[i])/(p[i]+1LL))+1LL;
                st=min(st,en);
            }
            else
            {
                st=en;
            }
            nowb=max(st,nowb);
            nowe=min(en,nowe);
            //printf("%lld %lld..\n",nowb,nowe);
            if(nowb>nowe)
            {
                flag=1LL;
            }
        }
        printf("Case #%lld: ",P);
        if(flag==1LL||nowb!=nowe)
        {
            printf("-1\n");
        }
        else
        {
            printf("%lld\n",nowb);
        }
    }


    return 0;
}


