/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

vector<int>adj[500010];
queue<int>q;
int n,arr[500010],stat[500010],val[500010];

void bfs(void)
{
    for(int i=1;i<=n;i++)
    {
        val[i]=0;
        stat[i]=0;
    }
    while(!q.empty())
        q.pop();
    q.push(1);
    val[1]=0;
    stat[1]=1;
    while(!q.empty())
    {
        int u=q.front();
        q.pop();
        for(int i=0;i<adj[u].size();i++)
        {
            int v=adj[u][i];
            if(stat[v]==0)
            {
                stat[v]=1;
                q.push(v);
                val[v]=val[u]+1;
            }
        }
    }
    return;
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    int from,to,t,p,i,res1,res2;

    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        for(i=1;i<=n;i++)
        {
            adj[i].clear();
            scanf(" %d",&arr[i]);
        }
        for(i=1;i<n;i++)
        {
            scanf(" %d %d",&from,&to);
            adj[from].push_back(to);
            adj[to].push_back(from);
        }
        bfs();
        res1=res2=0;
        for(i=1;i<=n;i++)
        {
            if(val[i]%2)
            {
                if(arr[i]==0)
                res1++;
            }
            else
            {
                if(arr[i]==0)
                    res2++;
            }
        }
        printf("%d\n",min(res1,res2));
    }


    return 0;
}

