/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
int rr[]={1,2,-1,-2,1,2,-1,-2};
int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int arr[1010][1010],r,c,m;

void fillg(int x,int y,int type)
{
    int i,j;
    if(type==1)
    {
        for(i=x+1,j=y-1;j>=1&&i<=r;j--,i++)
        {
                if(arr[i][j]==0||arr[i][j]==4)
                    arr[i][j]=4;
                else
                    break;

        }
        for(i=x-1,j=y+1;j<=c&&i>=1;j++,i--)
        {
                if(arr[i][j]==0||arr[i][j]==4)
                    arr[i][j]=4;
                else
                    break;

        }
        for(i=x-1,j=y-1;i>=1&&j>=1;i--,j--)
        {
                if(arr[i][j]==0||arr[i][j]==4)
                    arr[i][j]=4;
                else
                    break;
        }
        for(i=x+1,j=y+1;j<=c&&i<=r;j++,i++)
        {
                if(arr[i][j]==0||arr[i][j]==4)
                    arr[i][j]=4;
                else
                    break;
        }

        for(i=x,j=y-1;j>=1;j--)
        {
                if(arr[i][j]==0||arr[i][j]==4)
                    arr[i][j]=4;
                else
                    break;

        }
        for(i=x,j=y+1;j<=c;j++)
        {
                if(arr[i][j]==0||arr[i][j]==4)
                    arr[i][j]=4;
                else
                    break;

        }
        for(i=x-1,j=y;i>=1;i--)
        {
                if(arr[i][j]==0||arr[i][j]==4)
                    arr[i][j]=4;
                else
                    break;
        }
        for(i=x+1,j=y;i<=r;i++)
        {
                if(arr[i][j]==0||arr[i][j]==4)
                    arr[i][j]=4;
                else
                    break;
        }

    }
    else
    {

        for(i=0;i<8;i++)
        {
            if(arr[x+rr[i]][y+cc[i]]==0||arr[x+rr[i]][y+cc[i]]==4)
                arr[x+rr[i]][y+cc[i]]=4;
        }

    }
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    int i,j,res,x,y,p=1;
    while(scanf("%d%d",&r,&c)!=EOF)
    {
        if(r==0&&c==0)
            break;
        memset(arr,0,sizeof(arr));
        scanf(" %d",&m);
        for(i=1;i<=m;i++)
        {
            scanf(" %d %d",&x,&y);
            arr[x][y]=1;
        }
        scanf(" %d",&m);
        for(i=1;i<=m;i++)
        {
            scanf(" %d %d",&x,&y);
            arr[x][y]=2;
        }
        scanf(" %d",&m);
        for(i=1;i<=m;i++)
        {
            scanf(" %d %d",&x,&y);
            arr[x][y]=3;
        }
        for(i=1;i<=r;i++)
        {
            for(j=1;j<=c;j++)
            {
                  if(arr[i][j]==1)
                  fillg(i,j,1);
                  else if(arr[i][j]==2)
                  fillg(i,j,2);
            }
        }
        res=0;
        for(i=1;i<=r;i++)
        {
            for(j=1;j<=c;j++)
            {
                if(arr[i][j]==0)
                    res++;
            }
        }
        printf("Board %d has %d safe squares.\n",p++,res);
    }

    return 0;
}


