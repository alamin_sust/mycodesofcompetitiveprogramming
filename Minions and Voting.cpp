/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll res[100010],t,p,n,arr[100010],cum[100010];

void funcL(ll low, ll high) {
    ll ind = high+1LL;
    ll mid,ans=-1LL;
    while(low<=high) {
        mid=(low+high)/2LL;
        if(arr[ind]>=(cum[ind-1]-cum[mid-1])) {
            ans=mid;
            high=mid-1LL;
        } else low=mid+1LL;
    }

    if(ans==-1) {
        res[ind-1]++;
        res[ind]--;
        return;
    }

    if(ans>1LL) {
        ans--;
    }

    res[ans]++;
    res[ind]--;
    return;
}

void funcR(ll low, ll high) {
    ll ind = low-1LL;
    ll mid,ans=-1;
    while(low<=high) {
        mid=(low+high)/2LL;
        if(arr[ind]>=(cum[mid]-cum[ind])) {
            ans=mid;
            low=mid+1LL;
        } else high=mid-1LL;
    }

    if(ans==-1) {
        res[ind+1]++;
        res[ind+2]--;
        return;
    }


    if(ans<n) {
        ans++;
    }

    res[ans+1]--;
    res[ind+1]++;
    return;
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    ll i;
    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++) {
        scanf(" %lld",&n);

        for(i=1LL;i<=n;i++) {
            scanf(" %lld",&arr[i]);
            cum[i]=cum[i-1]+arr[i];
        }

        for(i=1LL;i<=n;i++) {
            if(i>1LL)
            funcL(1LL,i-1LL);
            if(i<n)
            funcR(i+1LL,n);
            //printf("d: %lld\n",i);
            //for(int j=1;j<=n;j++)printf("%lld ",res[j]);
            //printf("--\n");
        }

        ll val=0LL;
        for(i=1LL;i<=n;i++) {
            val+=res[i];
            res[i]=0LL;
            if(i>1LL)printf(" ");
            printf("%lld",val);
        }
        res[n+1]=0LL;
        printf("\n");
    }


    return 0;
}
