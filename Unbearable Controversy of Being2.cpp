/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll n,m,res,k,x,y,from,to,flag[3010];
vector<ll>adj[3010];
queue<ll>q,q2;

void bfs(ll u)
{
    for(ll i=0LL; i<adj[u].size(); i++)
        q.push(adj[u][i]);
    while(!q.empty())
    {
        ll v=q.front();
        for(ll i=0LL; i<adj[v].size(); i++)
        {
            if(adj[v][i]!=u)
            {
                if(flag[adj[v][i]]==0LL)
                {
                    q2.push(adj[v][i]);
                    flag[adj[v][i]]++;
                }
                else
                    flag[adj[v][i]]++;
            }
        }
        q.pop();
    }
    while(!q2.empty())
    {
        ll v=flag[q2.front()];
        flag[q2.front()]=0LL;
        res+=v*(v-1LL)/2LL;
        q2.pop();
    }
    return;
}

int main()
{
    ll i,j;
    scanf(" %I64d %I64d",&n,&m);
    for(i=1LL; i<=m; i++)
    {
        scanf(" %I64d %I64d",&from,&to);
        adj[from].push_back(to);
    }
    for(i=1LL; i<=n; i++)
    {
        bfs(i);
    }
    printf("%I64d\n",res);
    return 0;
}



