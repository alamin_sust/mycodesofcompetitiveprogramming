/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node
{
    int ind,val;
};

node det;
deque<node>pq;


int arr[1010][1010],row[1010][1010],col[1010][1010],cum[1010][1010],i,j,n,m,t,p,a,b,res;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    scanf(" %d %d",&n,&m);

    for(i=1; i<=n; i++)
    {
        for(j=1; j<=m; j++)
        {
            scanf(" %d",&arr[i][j]);
        }
    }

    for(i=1;i<=n;i++)
    {
        for(j=1;j<=m;j++)
        {
            cum[i][j]=((cum[i-1][j]+cum[i][j-1]-cum[i-1][j-1])+arr[i][j]);
        }
    }

    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {

        scanf(" %d %d",&a,&b);

        for(i=1; i<=m; i++)
        {
            pq.clear();
//            det.val=0;
//            for(j=1;j<a;j++)
//            {
//                if(det.val<=arr[j][i])
//                {
//                    det.ind=j;
//                    det.val=arr[j][i];
//                }
//            }
//            for(j=det.ind;j<a;j++)
//            {
//                det.ind=j;
//                det.val=arr[j][i];
//                pq.push_front(det);
//            }

            for(j=1;j<a;j++)
            {
                det.ind=j;
                det.val=arr[j][i];
                while((!pq.empty())&&pq.front().val<=det.val)
                {
                    pq.pop_front();
                }
                pq.push_front(det);
            }


            for(j=a; j<=n; j++)
            {
                det.ind=j;
                det.val=arr[j][i];

                while((!pq.empty())&&pq.front().val<=det.val)
                    pq.pop_front();

                while((!pq.empty())&&pq.back().ind<(j-a+1))
                    pq.pop_back();

                pq.push_front(det);

                col[j][i]=pq.back().val;

            }
        }




        for(i=1; i<=n; i++)
        {
            pq.clear();
//            det.val=0;
//            for(j=1;j<b;j++)
//            {
//                if(det.val<=col[i][j])
//                {
//                    det.ind=j;
//                    det.val=col[i][j];
//                }
//            }
//            for(j=det.ind;j<b;j++)
//            {
//                det.ind=j;
//                det.val=col[i][j];
//                pq.push_front(det);
//            }

            for(j=1;j<b;j++)
            {
                det.ind=j;
                det.val=col[i][j];
                while((!pq.empty())&&pq.front().val<=det.val)
                {
                    pq.pop_front();
                }
                pq.push_front(det);
            }

            for(j=b; j<=m; j++)
            {
                det.ind=j;
                det.val=col[i][j];
                while((!pq.empty())&&pq.front().val<=det.val)
                    pq.pop_front();

                while((!pq.empty())&&pq.back().ind<(j-b+1))
                    pq.pop_back();

                pq.push_front(det);

                row[i][j]=pq.back().val;

            }
        }

//        for(i=1;i<=n;i++)
//        {
//            for(j=1;j<=m;j++)
//           {
//                printf("%d ",row[i][j]);            }
//            printf("\n");
//        }

        res=2000000009;

        for(i=a;i<=n;i++)
        {
            for(j=b;j<=m;j++)
            {
                int sum=(cum[i][j]-cum[i-a][j])-cum[i][j-b]+cum[i-a][j-b];
                res=min(res,row[i][j]*a*b-sum);
            }
        }
        printf("%d\n",res);
    }

    return 0;
}


/*
6 6
5 1 2 4 3 5
8 5 7 2 3 1
9 2 9 5 2 4
1 3 4 0 6 6
4 2 1 4 8 2
3 9 4 5 2 1
1
3 3
*/






