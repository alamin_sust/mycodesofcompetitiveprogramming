#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
#define MAX 10010
int i, j, sieve[MAX], primecount=0, prime[MAX], in,n,k;
main()
{
    for(i=0; i<MAX; i++)
    {
        sieve[i]=1;
    }
    sieve[0]=sieve[1]=0;
    for(i=2; i<MAX; i++)
    {
        while(sieve[i]==0 && i<MAX)
        {
            i++;
        }
        prime[primecount]=i;
        for(j=i*i; j<MAX; j+=i)
        {
            sieve[j]=0;
        }
        primecount++;
    }
    cin>>n;
    int res=0;
    for(i=1;i<=n;i++)
    {
        int k=0;
        for(j=i-1;j>=0;j--)
        {
            if(sieve[j]==1&&i%j==0)
                k++;
        }
        //printf("%d\n",k);
        if(k==2)
            res++;
    }
    printf("%d\n",res);
    return 0;
}


