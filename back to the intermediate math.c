#include<stdio.h>
#include<math.h>
int main()
{
    int t,i;
    double d,u,v,td[10000],t1,t2;
    scanf("%d",&t);
    for(i=1; i<=t; i++)
    {
        scanf(" %lf %lf %lf",&d,&v,&u);
        if(u==0 || v>=u || v==0)
        {
            td[i]=-1;
        }
        else
        {
            t1=(1.0*d)/(u*1.0);
            t2=(1.0*d)/(sqrt((u*u)-(v*v))*1.0);
            td[i]=fabs(t2-t1);
        }

    }
    for(i=1;i<=t;i++)
     {
         if(td[i]==-1)
         printf("Case %d: can't determine\n",i);
      else
     printf("Case %d: %0.3lf\n",i,td[i]);}


    return 0;
}
