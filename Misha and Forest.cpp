/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


struct node{
int deg,x_or,v;
};

queue<node>q;

node arr[100010],det;

int n,i,m,resx[100010],resy[100010],tot_edges;

int main()
{
    cin>>n;
    for(i=0;i<n;i++)
    {
        scanf(" %d %d",&arr[i].deg,&arr[i].x_or);
        arr[i].v=i;
        tot_edges+=arr[i].deg;
        if(arr[i].deg==1)
        q.push(arr[i]);
    }
    tot_edges/=2;
    m=0;
    while(!q.empty())
    {
        det=q.front();
        q.pop();
        if(arr[det.v].deg<=0)
            continue;
        if(m==tot_edges)
            break;
        resx[m]=det.v;
        resy[m++]=det.x_or;
        arr[det.x_or].deg--;
        arr[det.v].deg--;
        arr[det.v].x_or=0;
        arr[det.x_or].x_or^=det.v;
        if(arr[det.x_or].deg==1)
        q.push(arr[det.x_or]);
    }
    printf("%d\n",m);
    for(i=0;i<m;i++)
    {
        printf("%d %d\n",resx[i],resy[i]);
    }
    return 0;
}



