/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll a,b,aa,bb,mna,ii,jj,n,mn,flag;

main()
{
    cin>>n>>a>>b;
    n*=6LL;
    if((a*b)>=(n))
    {
      printf("%I64d\n",a*b);
      printf("%I64d %I64d\n",a,b);
      return 0;
    }
    mna=min(a,b);
    aa=min(a,b);
    bb=max(a,b);
    aa=n/bb+(n%bb==0?0:1);
    ii=aa;
    jj=bb;
    mn=99999999999999999LL;
    while(1)
    {
       if((aa*bb-n)<mn&&min(aa,bb)>=mna)
       {
           mn=aa*bb-n;
           ii=aa;
           jj=bb;
       }
       if(flag==1&&(aa==mna))
            break;
       flag=1;
       if(bb<aa)
       {
           bb++;
           aa=n/bb+(n%bb==0?0:1);
       }
       else
       {
           aa--;
           bb=n/aa+(n%aa==0?0:1);
       }
    }
    if(a<b)
    {
      printf("%I64d\n",ii*jj);
      printf("%I64d %I64d\n",min(ii,jj),max(ii,jj));
    }
    else
    {
      printf("%I64d\n",ii*jj);
      printf("%I64d %I64d\n",max(ii,jj),min(ii,jj));
    }
    return 0;
}


