/********************************************************
* Md. Khairullah Gaurab                                 *
* Computer Science & Engineering                        *
* Shahjalal University of Science and Technology        *
* 20th Batch                                            *
* gaurab.cse.sust@gmail.com                             *
*********************************************************/

#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <cstring>
#include <limits>
#include <vector>
#include <cstdlib>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <algorithm>

using namespace std;

typedef long long Long;
typedef double DD;
typedef vector<int> VI;
typedef vector<VI > VVI;
typedef pair<int,int> PII;
typedef vector<PII> VPII;


const int INF = 2000000000;
const int MOD = 1000000007;


#define sf scanf
#define pf printf
#define mem(a,b)          memset(a,b,sizeof(a))
#define pb push_back
#define REP(i,a,b)        for(int i=a; i<=b; ++i)
#define REPI(i,a,b,c)     for(int i=a; i<=b; i+=c)
#define REPR(i,a,b)       for(int i=b; i>=a; --i)
#define REPRD(i,a,b,c)    for(int i=b; i>=a; i-=c)
#define REPB(i,a)         for(int i=a; ;i++)
#define REPRB(i,a)        for(int i=a; ; i--)
#define mp(a,b)   make_pair(a,b)
#define fs        first
#define sc        second
#define SZ(s)     ((int)s.size())
#define PI        3.141592653589793
#define VS        vector<string>
#define VI        vector<int>
#define VD        vector<DD>
#define VL        vector<Long>
#define VVL       vector<VL >
#define lim       65
#define tlim      (1<<((int)ceil(log2(lim))+1))
#define unq(vec)  stable_sort(vec.begin(),vec.end());\
                  vec.resize(distance(vec.begin(),unique(vec.begin(),vec.end())));
#define BE(a)     a.begin(),a.end()
#define rev(a)    reverse(BE(a));
#define sorta(a)  stable_sort(BE(a))
#define sortc(a, comp)  sort(BE(a),comp)

//int X[]={1,1,2,2,-1,-1,-2,-2},Y[]={2,-2,1,-1,2,-2,1,-1};//knight move
//int X[]={0,-1,-1,-1,0,1,1,1},Y[]={-1,-1,0,1,1,1,0,-1};//8 move
//int X[]={-1,0,1,0},Y[]={0,1,0,-1};//4 move

string inp;

struct node
{
	string str;
	int freq;
	node(string _, int __)
	{
		str = _;
		freq = __;
	}
	bool operator<(const node& z)const
	{
		if(z.freq==freq) return z.str<str;
		return z.freq>freq;
	}
};
map<string , int> foo;
priority_queue<node> boo;
vector<string> wordList[8];

string ARR[] = {"<top 1 />","<top 2 />","<top 3 />","<top 4 />","<top 5 />","<top 6 />","<top 7 />","<top 8 />","<top 9 />","<top 10 />",
				"<top 11 />","<top 12 />","<top 13 />","<top 14 />","<top 15 />","<top 16 />","<top 17 />","<top 18 />","<top 19 />","<top 20 />"};


int check(string test)
{
	REP(i,0,19)
	{
		if(test==ARR[i]) return (i+1);
	}
}
int main(int argc, const char **argv)
{
   // ios::sync_with_stdio(false);
    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);
    //double st=clock(),en;
    int daycnt = 0;
    //boo.clear();
    while(getline(cin,inp))
    {
    	if(SZ(inp)==0) continue;
    	if(inp=="<text>")
    	{
    		if(daycnt>=7)
    		{
    			int tmp = (daycnt%7);
    			REP(i,0,(SZ(wordList[tmp])-1))
    			{
    				foo[wordList[tmp][i]]--;
    			}
    			wordList[tmp].clear();
    		}
    		while(cin>>inp and inp!="</text>")
    		{
    			if(SZ(inp)<4) continue;
    			wordList[((daycnt%7))].pb(inp);
    			foo[inp]++;
    		}
    		daycnt++;
    	}
    	else
    	{
    		int TOPN = check(inp);
    		for(map<string, int> :: iterator itt = foo.begin(); itt!=foo.end(); itt++)
    		{
    			boo.push(node(itt->fs,itt->sc));
    		}
    		cout<<"<top "<<TOPN<<">"<<"\n";
    		int i = 0;
    		while(i<TOPN && (!boo.empty()) )
    		{
    			int freq = boo.top().freq;
    			while((!boo.empty()) and boo.top().freq==freq)
    			{
    				cout<<boo.top().str<<" "<<boo.top().freq<<"\n";
    				boo.pop();
    				i++;
    			}
    		}
    		cout<<"</top>\n";
    		while(!boo.empty()) boo.pop();
    	}
    }
    //en=clock();
    //cerr<<(double)(en-st)/CLOCKS_PER_SEC<<endl;
    return 0;
}

