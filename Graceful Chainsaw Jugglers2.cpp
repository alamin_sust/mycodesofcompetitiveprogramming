#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node {
    int red,blue;
};

vector<node>v;
int i,j;

int comp (node a, node b) {
    return (a.blue+a.red)<(b.blue+b.red);
}

int t,p,r,b,dp[2610][52][52][55];


int rec(int pos, int red,int blue, int res) {
    if(pos>=2600) return res;
    int &ret = dp[pos][red][blue][res];
    if(ret!=-1) return ret;
    ret = 0;
    if(v[pos].red<=red && v[pos].blue<=blue)
    {
        ret = max(rec(pos+1,red-v[pos].red,blue-v[pos].blue,res+1),rec(pos+1,red,blue,res));
    } else ret = rec(pos+1,red,blue,res);

    return ret;
}

main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    for(i=0;i<=50;i++) {
        for(j=0;j<=50;j++) {
                if(i==0&&j==0) continue;
                node tp;
        tp.red = i;
        tp.blue = j;
            v.push_back(tp);
        }
    }
    sort(v.begin(),v.end(),comp);

    //printf("%d\n",v.size());

    //for(i=0;i<10;i++) printf("%d %d\n",v[i].red,v[i].blue);

    scanf(" %d",&t);
    for(p=1;p<=t;p++) {
        scanf(" %d %d",&r,&b);

        memset(dp,-1,sizeof(dp));
        printf("Case #%d: %d\n",p,rec(0,r,b,0));
    }

    return 0;
}

