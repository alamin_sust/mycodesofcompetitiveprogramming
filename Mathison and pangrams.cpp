/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int t,p,i,l,res,fg[28],pr[28];
char arr[50010];

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++) {
        for(i=0;i<26;i++) {
        scanf(" %d",&pr[i]);
        }

        memset(fg,0,sizeof(fg));

        scanf(" %s",arr);
        l=strlen(arr);

        for(i=0;i<l;i++) {
            fg[arr[i]-'a']=1;
        }
        res=0;
        for(i=0;i<26;i++) {
            if(fg[i]==0) {
                res+=pr[i];
            }
        }
        printf("%d\n",res);

    }

    return 0;
}

