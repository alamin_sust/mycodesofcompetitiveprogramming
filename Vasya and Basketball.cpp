/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node
{
    ll val,id1,id2,agg;
};

bool operator<(node a,node b)
{
    return a.val>b.val;
}

node arr1[200010],arr2[200010],det;
priority_queue<node>pq;
ll N,M,n,m,i,nowa,nowb,resa,resb,ans,add1,add2,in;

int main()
{
    N=M=0LL;
    cin>>n;
    for(i=1LL; i<=n; i++)
    {
        scanf(" %I64d",&in);
        add1++;
        if(in==arr1[N].val)
        {
            arr1[N].agg++;
            continue;
        }
        arr1[++N].id1=1LL;
        arr1[N].id2=N;
        arr1[N].val=in;
        arr1[N].agg++;
    }
    cin>>m;
    for(i=1LL; i<=m; i++)
    {
        scanf(" %I64d",&in);
        add2++;
        if(in==arr2[M].val)
        {
            arr2[M].agg++;
            continue;
        }
        arr2[++M].id1=2LL;
        arr2[M].id2=M;
        arr2[M].val=in;
        arr2[M].agg++;
    }
    for(i=1LL; i<=N; i++)
    {
        det.val=arr1[i].val;
        det.agg=arr1[i].agg;
        det.id1=arr1[i].id1;
        det.id2=arr1[i].id2;
        pq.push(det);
        arr1[i].agg+=arr1[i-1].agg;
    }
    for(i=1LL; i<=M; i++)
    {
        det.val=arr2[i].val;
        det.agg=arr2[i].agg;
        det.id1=arr2[i].id1;
        det.id2=arr2[i].id2;
        pq.push(det);
        arr2[i].agg+=arr2[i-1].agg;
    }
    nowa=nowb=1LL;
    ans=-9999999999LL;
    while(!pq.empty())
    {
        det=pq.top();
        pq.pop();
        if(det.id1==1LL)
        {
            nowa=det.id2;
            if(arr2[nowb].val==arr1[nowa].val)
            {
            if(((add1-arr1[nowa-1LL].agg)*3LL+arr1[nowa-1LL].agg*2LL)-((add2-arr2[nowb-1LL].agg)*3LL+arr2[nowb-1LL].agg*2LL)>=ans)
            {
                resa=((add1-arr1[nowa-1LL].agg)*3LL+arr1[nowa-1LL].agg*2LL);
                resb=((add2-arr2[nowb-1LL].agg)*3LL+arr2[nowb-1LL].agg*2LL);
                ans=((add1-arr1[nowa-1LL].agg)*3LL+arr1[nowa-1LL].agg*2LL)-((add2-arr2[nowb-1LL].agg)*3LL+arr2[nowb-1LL].agg*2LL);
            }
            }
            else
            {
             if(((add1-arr1[nowa-1LL].agg)*3LL+arr1[nowa-1LL].agg*2LL)-((add2-arr2[nowb].agg)*3LL+arr2[nowb].agg*2LL)>=ans)
            {
                resa=((add1-arr1[nowa-1LL].agg)*3LL+arr1[nowa-1LL].agg*2LL);
                resb=((add2-arr2[nowb].agg)*3LL+arr2[nowb].agg*2LL);
                ans=((add1-arr1[nowa-1LL].agg)*3LL+arr1[nowa-1LL].agg*2LL)-((add2-arr2[nowb].agg)*3LL+arr2[nowb].agg*2LL);
            }
            }

        }
        else
        {
            nowb=det.id2;
            if(arr2[nowb].val==arr1[nowa].val)
            {
            if(((add1-arr1[nowa-1LL].agg)*3LL+arr1[nowa-1LL].agg*2LL)-((add2-arr2[nowb-1LL].agg)*3LL+arr2[nowb-1LL].agg*2LL)>=ans)
            {
                resa=((add1-arr1[nowa-1LL].agg)*3LL+arr1[nowa-1LL].agg*2LL);
                resb=((add2-arr2[nowb-1LL].agg)*3LL+arr2[nowb-1LL].agg*2LL);
                ans=((add1-arr1[nowa-1LL].agg)*3LL+arr1[nowa-1LL].agg*2LL)-((add2-arr2[nowb-1LL].agg)*3LL+arr2[nowb-1LL].agg*2LL);
            }
            }
            else
            {
             if(((add1-arr1[nowa].agg)*3LL+arr1[nowa].agg*2LL)-((add2-arr2[nowb-1LL].agg)*3LL+arr2[nowb-1LL].agg*2LL)>=ans)
            {
                resa=((add1-arr1[nowa].agg)*3LL+arr1[nowa].agg*2LL);
                resb=((add2-arr2[nowb-1LL].agg)*3LL+arr2[nowb-1LL].agg*2LL);
                ans=((add1-arr1[nowa].agg)*3LL+arr1[nowa].agg*2LL)-((add2-arr2[nowb-1LL].agg)*3LL+arr2[nowb-1LL].agg*2LL);
            }
            }
        }

    }
    if((add1*3LL-add2*3LL)>=ans)
    {
        resa=add1*3LL;
        resb=add2*3LL;
        ans=add1*3LL-add2*3LL;
    }
    cout<<resa<<":"<<resb<<endl;
    return 0;
}

