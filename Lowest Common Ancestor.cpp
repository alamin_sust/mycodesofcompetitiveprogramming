/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int lev[1010],par[1010],t,p,n,in,i,j,m;

void build(int node)
{
    if(par[node]==0)
        {lev[node]=0;
        return;}
    build(par[node]);
    lev[node]=1+lev[par[node]];
    return;
}

main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n;
        memset(par,0,sizeof(par));
        for(i=1;i<=n;i++)
        {
            scanf(" %d",&m);
            for(j=1;j<=m;j++)
            {
                scanf(" %d",&in);
                par[in]=i;
            }
        }
        memset(lev,-1,sizeof(lev));
        for(i=1;i<=n;i++)
        {
            if(lev[i]==-1)
                build(i);
        }
        //for(i=1;i<=n;i++)
          //  printf("%d %d\n",par[i],lev[i]);
        scanf(" %d",&m);
        printf("Case %d:\n",p);
        for(i=1;i<=m;i++)
        {
            int u,v;
            scanf(" %d %d",&u,&v);
            if(lev[u]>lev[v])
            {
                while(lev[u]!=lev[v])
                {
                   // printf(".");
                    u=par[u];
                }
            }
            else
            {
                while(lev[u]!=lev[v])
                {
                   // printf(".");
                    v=par[v];
                }
            }
            while(u!=v)
            {
                u=par[u];
                v=par[v];
            }
            printf("%d\n",u);
        }
    }
    return 0;
}

