/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node{
int tp,id;
};

int comp(node a,node b)
{
    return a.id>b.id;
}

vector<node>v;
int t,p,u,b,fg,n,i;
char str[1010];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        v.clear();
        for(i=1;i<=n;i++)
        {
            node det;
            scanf(" %d %s",&det.id,str);
            if(str[0]=='G')
                det.tp=1;
            else if(str[0]=='B')
                det.tp=2;
            else
                det.tp=3;
            v.push_back(det);
        }
        sort(v.begin(),v.end(),comp);
        b=0;
        u=0;
        int g=0;
        fg=0;
        for(i=0;i<v.size();i++)
        {
            if(v[i].tp==1)
            {
                g++;
                if(b<g||u<g)
                {
                    fg=1;
                }
            }
            else if(v[i].tp==2)
            {
                b++;
            }
            else
                u++;
        }
        if(fg==0&&g==b&&g==u)
             printf("Case %d: GBU\n",p);
        else
            printf("Case %d: Undefined\n",p);

    }
    return 0;
}

