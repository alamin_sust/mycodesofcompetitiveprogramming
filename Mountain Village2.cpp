#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
int rr[]={1,-1,0,0};
int cc[]={0,0,1,-1};

int arr[43][43],lower,upper,status[43][43],k,row,col;

void dfs(int x,int y)
{
    status[x][y]=1;
    k++;
    for(int i=0;i<4;i++)
    {
        if((x+rr[i])<=row&&(x+rr[i])>0&&(y+cc[i])<=col&&(y+cc[i])>0)
        if(status[x+rr[i]][y+cc[i]]==0&&arr[x+rr[i]][y+cc[i]]<=upper&&arr[x+rr[i]][y+cc[i]]>=lower)
            dfs(x+rr[i],y+cc[i]);
    }
    return;
}

bool get_result(int mn)
{
    memset(status,0,sizeof(status));
    for(int i=1; i<=row; i++)
        for(int j=1; j<=col; j++)
        {
            if(status[i][j]==0&&arr[i][j]>=lower&&arr[i][j]<=upper)
            {
                k=0;
                dfs(i,j);
                if(k>=mn)
                    return true;
            }
        }
    return false;
}

bool func(int lm,int n)
{
    bool flag=0;
    upper=lm,lower=0;
    while(flag==0&&upper<100)
    {
        flag=get_result(n);
        upper++;
        lower++;
    }
    return flag;
}

main()
{
    int i,j,from,to,mid,in,n;
    cin>>row>>col;
    for(i=1; i<=row; i++)
    {
        for(j=1; j<=col; j++)
            scanf(" %d",&arr[i][j]);
    }
    cin>>n;
    for(i=1; i<=n; i++)
    {
        scanf(" %d",&in);
        from=0;
        to=100;
        mid=50;
        while(1)
        {
            if(func(mid,in)==true)
                {if(mid==0||func(mid-1,in)==false)
                    break;
                to=mid;}
            else
                from=mid;
            mid=(from+to)/2;
        }
        printf("%d\n",mid);

    }
    return 0;
}

