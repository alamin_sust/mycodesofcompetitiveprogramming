/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll arr[100010],t,p,i,n,q,com,a,b,j;
vector<ll>v,iv;

struct node {
    ll ind,val;
};

node tree[400010];

bool operator<(node a,node b) {
    if(a.val==b.val) return a.ind>b.ind;
    return a.val<b.val;
}

node construct_tree(ll pos, ll l, ll r) {
    if(l==r){
        tree[pos].ind=l;
        tree[pos].val=arr[l];
        return tree[pos];
    }
    ll mid=(l+r)/2LL;
    return tree[pos] = max(construct_tree(pos*2LL,l,mid), construct_tree(pos*2LL+1LL,mid+1LL,r));
}

node query(ll pos,ll l,ll r,ll ql,ll qr) {
    if(r<ql||l>qr) {
        node nd;
    nd.ind=-1LL;
    nd.val=-1LL;
      return nd;
    }
    if(l>=ql && r<=qr) return tree[pos];
    ll mid = (l+r)/2LL;
    return max(query(pos*2LL,l,mid,ql,qr), query(pos*2LL+1LL,mid+1LL,r,ql,qr));
}

node update(ll pos,ll l, ll r, ll ind) {
    if(ind<l||ind>r) return tree[pos];
    if(l==r && l==ind){
        tree[pos].ind=ind;
        tree[pos].val=arr[ind];
        return tree[pos];
    }
    ll mid = (l+r)/2LL;
    return tree[pos] = max(update(pos*2LL,l,mid,ind),update(pos*2LL+1LL,mid+1LL,r,ind));
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %lld %lld",&n,&q);

    for(i=1LL;i<=n;i++) {
        scanf(" %lld",&arr[i]);
    }

    construct_tree(1LL,1LL,n);

    for(i=1LL;i<=q;i++) {
        scanf(" %lld %lld %lld",&com,&a,&b);
        if(com==1LL) {
            arr[a]=b;
            update(1LL,1LL,n,a);
        } else {
            v.clear();
            iv.clear();
            ll flag=0LL;
            for(j=0LL;j<(b-a+1);j++) {
                node res = query(1LL,1LL,n,a,b);
                iv.push_back(res.ind);
                v.push_back(res.val);
                arr[res.ind] = -1;
                update(1LL,1LL,n,res.ind);
                if(j>=2LL) {
                    if((v[j]+v[j-1])>v[j-2]) {
                        printf("%lld\n",v[j-2]+v[j-1]+v[j]);
                        flag=1LL;
                        break;
                    }
                }
            }
            if(flag==0LL) {
                printf("0\n");
            }
            for(j=v.size()-1LL;j>=0LL;j--) {
                arr[iv[j]]=v[j];
                update(1LL,1LL,n,iv[j]);
            }
        }
    }

    return 0;
}
