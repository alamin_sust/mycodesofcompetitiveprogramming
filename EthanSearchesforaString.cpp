/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

string arr,res;
int p,t,i,l;
bool flag;

int main()
{

    freopen("ethan_searches_for_a_string.txt","r",stdin);
    freopen("ethan_searches_for_a_string_output.txt","w",stdout);

    cin>>t;

    for(p=1;p<=t;p++) {
        cin>>arr;
        l=arr.size();
        flag=false;
        for(i=1;i<l;i++) {
            if(arr[0]==arr[i] && arr.substr(i,l) != arr.substr(0,l-i)) {
                res=arr.substr(0,i)+arr.substr(0,i)+arr.substr(i,l);
                flag=true;
                break;
            }
        }
        if(flag) {
        cout<<"Case #"<<p<<": "<<res<<endl;
        } else {
        cout<<"Case #"<<p<<": Impossible"<<endl;
        }
    }

    return 0;
}
