/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int t,p,b,w;
double per,x,y,pp,dd,deg,dst;

int main()
{
    freopen("progress_pie.txt","r",stdin);
    freopen("progress_pie_output.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        b=w=0;
        scanf(" %lf %lf %lf",&per,&x,&y);
        x-=50;
        y-=50;
        if(x>=0)
        {
            if(y>=0)
            {
                if(per>=25)
                {
                    b=1;
                }
                else if(per<=0)
                {
                    w=1;
                }
                else
                {
                    if(x==0&&y==0)
                    {
                        if(per==0)
                            w=1;
                        else
                            b=1;
                    }
                    else
                    {
                        deg=90-(y/(y+x))*90;
                        dd=deg/90;
                        pp=per/25;
                        if(dd>pp)
                            w=1;
                        else
                            b=1;
                    }
                }
            }
            else
            {
                y=-y;
                if(per>=50)
                {
                    b=1;
                }
                else if(per<=25)
                {
                    w=1;
                }
                else
                {
                    deg=(y/(y+x))*90;
                        dd=deg/90;
                        pp=(per-25)/25;
                        if(dd>pp)
                            w=1;
                        else
                            b=1;
                }
            }
        }
        else
        {
            x=-x;
            if(y<0)
            {
                y=-y;
                if(per>=75)
                {
                    b=1;
                }
                else if(per<=50)
                {
                    w=1;
                }
                else
                {
                    deg=90-(y/(y+x))*90;
                        dd=deg/90;
                        pp=(per-50)/25;
                        if(dd>pp)
                            w=1;
                        else
                            b=1;

                }
            }
            else
            {
                if(per>=100)
                {
                    b=1;
                }
                else if(per<=75)
                {
                    w=1;
                }
                else
                {
                    deg=(y/(y+x))*90;
                        dd=deg/90;
                        pp=(per-75)/25;
                        if(dd>pp)
                            w=1;
                        else
                            b=1;
                }
            }
        }


        dst=dist(0,0,x,y);
        if(dst>50)
        {
            printf("Case #%d: white\n",p);
        }
        else
        {
            if(b==1)
            {
                printf("Case #%d: black\n",p);
            }
            else
            {
                printf("Case #%d: white\n",p);
            }
        }
    }

    return 0;
}






