#include<stdio.h>
#include<math.h>

main()
{
    while(1)
    {
        int n,pages,i,k,p;
        scanf(" %d",&n);
        if(n==0)
        break;
        printf("Printing order for %d pages:\n",n);
        if(n==1)
        {
            printf("Sheet 1, front: Blank, 1\n");
            continue;
        }
        pages=n/4;
        if(pages*4!=n)
        pages++;
        for(p=1,k=pages*4,i=1;i<=pages;i++)
        {
            printf("Sheet %d, front:",i);
            if(k>n)
            printf(" Blank,");
            else
            printf(" %d,",k);
            k--;
            printf(" %d\n",p++);
            printf("Sheet %d, back :",i);
            printf(" %d,",p++);
            if(k>n)
            printf(" Blank\n");
            else
            printf(" %d\n",k);
            k--;
        }
    }
    return 0;
}
