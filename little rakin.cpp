#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
ll fn[1010],primecount=0,sieve[100010],t,arr[44][1010],prime[1010],n,a,b,p,i,mx;

void sieve_(void)
{
    for(ll i=0; i<=1000; i++)
    {
        sieve[i]=1;
    }
    sieve[0]=sieve[1]=0;
    for(ll i=2; i<=1000; i++)
    {
        while(sieve[i]==0 && i<=1000)
        {
            i++;
        }
        prime[primecount]=i;
        for(ll j=i*i; j<=1000; j+=i)
        {
            sieve[j]=0;
        }
        primecount++;
    }
    return;
}

void fact(ll x)
{
    for(ll i=0;i<primecount;i++)
    {
        while(fn[x]%prime[i]==0)
        {
            arr[x][prime[i]]++;
            fn[x]/=prime[i];
        }
    }
    return;
}

main()
{
    sieve_();
    SF(" %lld",&t);
    for(p=1;p<=t;p++)
    {
        memset(arr,0,sizeof(arr));
        SF(" %lld %lld %lld",&n,&a,&b);
        fn[0]=a;
        fact(0);
        fn[1]=b;
        fact(1);
        for(i=2;i<=n;i++)
        {
            for(ll j=0;j<primecount;j++)
            arr[i][prime[j]]+=arr[i-1][prime[j]]+arr[i-2][prime[j]];
        }
        for(i=0;i<primecount;i++)
        {
            if(arr[n][prime[i]]!=0)
                PF("%lld %lld\n",prime[i],arr[n][prime[i]]);
        }
        printf("\n");
    }
    return 0;
}

