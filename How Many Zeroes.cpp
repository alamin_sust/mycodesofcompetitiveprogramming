#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll c,dp[12][12][2][2],arr[12];

ll rec(ll pos,ll zeroes,ll flag,ll started)
{
    if(pos==0)
        return zeroes;
    ll &ret=dp[pos][zeroes][flag][started];
    if(ret!=-1)
        return ret;
    ret=0;
    for(ll i=0;i<=9;i++)
    {
        ll z=zeroes,st=1;
        if(i==0)
        {
            if(started==0)
                z=0,st=0;
            else
                z=zeroes+1;
        }
        if(flag==1)
        {
            ret+=rec(pos-1,z,flag,st);
        }
        else if(arr[pos]==i)
        {
            ret+=rec(pos-1,z,flag,st);
        }
        else if(arr[pos]>i)
        {
            ret+=rec(pos-1,z,1,st);
        }
    }
    return ret;
}

ll calc(ll x)
{
    ll ret=0;
    while(x>0)
    {
        arr[++ret]=x%10;
        x/=10;
    }
    return ret;
}

main()
{
    ll res,a,b,p,t;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %lld %lld",&a,&b);
        if(a==0&&b==0)
        {
            printf("Case %lld: 1\n",p);
            continue;
        }
        ms(dp,-1);
        c=b;
        res=rec(calc(c),0,0,0);
        ms(dp,-1);
        c=a-1;
        if(a==0)
            res++,c++;
        res-=rec(calc(c),0,0,0);
        printf("Case %lld: %lld\n",p,res);
    }
    return 0;
}
