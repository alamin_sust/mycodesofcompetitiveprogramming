#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define MAX 1000100
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
long long sieve[MAX+10], primecount=0, prime[MAX+10];
void sieve_(void)
{
    for(ll i=0; i<MAX; i++)
    {
        sieve[i]=1;
    }
    sieve[0]=sieve[1]=0;
    for(ll i=2; i<MAX; i++)
    {
        while(sieve[i]==0 && i<MAX)
        {
            i++;
        }
        prime[++primecount]=i;
        for(ll j=i*i; j<MAX; j+=i)
        {
            sieve[j]=0;
        }
    }
    return;
}

main()
{
    ll p,t,i,in,res;
    sieve_();
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %lld",&in);
        res=1;
        for(i=1;prime[i]<=sqrt(in);i++)
        {
            if(in%prime[i]==0)
            {
                ll k=1;
                while(in%prime[i]==0)
                {
                    in/=prime[i];
                    k++;
                }
                res*=k;
            }
        }
        if(in>=2)
            res*=2;
        res--;
        printf("Case %lld: %lld\n",p,res);
    }
    return 0;
}

