/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

stack<int>res;
queue<int>q;
int n,i,j,k,flag,arr[1000010];

main()
{
    scanf(" %d",&n);
    for(i=1;i<=n;i++)
        scanf(" %d",&arr[i]);
    flag=0;
    for(i=31;i>=0;i--)
    {
        for(j=1;j<=n;j++)
        {
            if((arr[j]>>i)&1)
            {
                flag++;
                q.push(arr[j]);
            }
        }
        if(flag>1)
        {printf("%d\n",i);
            break;}
        else
        {
            q.pop();
        }
    }
    for(i--;(!q.empty())&&i>=0;i--)
    {
        k=0;
        for(j=0;j<flag;j++)
        {
            int x=q.front();
            q.pop();
            if((x>>i)&1)
            {
                k++;
                res.push(x);
            }
            else
            {
                q.push(x);
            }
        }
        flag-=k;
    }
    if(res.size()>=2)
    {
        int x=res.top();
        res.pop();
        printf("%d\n",res.top()&x);
    }
    else
    {
        printf("%d\n",arr[1]&arr[2]);
    }
    return 0;
}

