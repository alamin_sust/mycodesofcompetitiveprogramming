#include<stdio.h>
#include<math.h>
#include<algorithm>
#include<iostream>
#include<map>
using namespace std;

map<long long,long long>mpp;
long long int t,p,i,j,k,tpgcd,P,Q,n1,n2,n3,m,flag,arr1[2010],arr2[2010],arr3[2010];

int main()
{

    freopen("B-large.in","r",stdin);
    freopen("output.txt","w",stdout);

    scanf(" %lld",&t);


    for(p=1LL; p<=t; p++)
    {
        scanf(" %lld %lld %lld",&n1,&n2,&n3);
        mpp.clear();
        for(i=1LL; i<=n1; i++)
        {
            scanf(" %lld",&arr1[i]);
        }
        for(i=1LL; i<=n2; i++)
        {
            scanf(" %lld",&arr2[i]);
        }
        for(i=1LL; i<=n3; i++)
        {
            scanf(" %lld",&arr3[i]);
        }

        for(i=1LL; i<=n2; i++)
        {
            for(j=1LL; j<=n3; j++)
            {

                tpgcd=__gcd(arr2[i],arr3[j]);
                if(mpp[(arr2[i]/tpgcd)*(arr3[j]/tpgcd)])
                    mpp[(arr2[i]/tpgcd)*(arr3[j]/tpgcd)]=1000000000000000010LL;
                else
                    mpp[(arr2[i]/tpgcd)*(arr3[j]/tpgcd)]=arr2[i];
            }
        }
        printf("Case #%lld:\n",p);
        scanf(" %lld",&m);

        for(i=1LL; i<=m; i++)
        {
            scanf(" %lld %lld",&P,&Q);
            flag=0LL;
            for(j=1LL; j<=n1&&flag==0LL; j++)
            {
                for(k=1LL; k<=n2; k++)
                {
                    long long tp=P*arr2[k];
                    long long tq=Q*arr1[j];
                    tpgcd=__gcd(tp,tq);
                    tp/=tpgcd;
                    tq/=tpgcd;
                    if(mpp[tp*tq]>0LL&&mpp[tp*tq]!=arr2[k])
                    {
                        flag=1LL;
                        printf("Yes\n");
                        break;
                    }
                }
            }
            if(flag==0LL)
                printf("No\n");
        }

    }

    return 0;
}
