//priority queue
#include<stdio.h>
#include<algorithm>
#include<iostream>
#include<queue>
#include<string>
using namespace std;

main()
{
    priority_queue<int>pq;
    pq.push(3);
    pq.push(1);
    pq.push(4);
    pq.push(2);
    while (!pq.empty()) {
       cout<<pq.top()<<endl;
       pq.pop();
    }
    return 0;
}
