/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int n,res,now,i,reg[110],hidden,mpp[1000010];
char arr[110][110];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d",&n);
    now=0;
    res=0;
    for(i=1;i<=n;i++)
    {
        scanf(" %s %d",&arr[i],&reg[i]);
        if(arr[i][0]=='-')
        {
            if(mpp[reg[i]]==0)
            now++;
            else
                mpp[reg[i]]=0;
        }
        else
        {
            mpp[reg[i]]=1;
        }

    }
    res=now;
    for(i=1;i<=n;i++)
    {
        if(arr[i][0]=='+')
        {
            now++;
            res=max(res,now);
        }
        else
        {
            now--;
            res=max(res,now);
        }
    }

    printf("%d\n",res);

    return 0;
}


