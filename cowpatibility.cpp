#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<set>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

string str[1000010];
ll arr[50010][8],n;
vector<ll>vec[1000010];
map<string,bool>mpp2;
map<string,ll>mpp;

string tostr(ll val)
{
    string ret = "";
    while(val)
    {
        ret+=(char)((val%10LL)+'0');
        val/=10LL;
    }
    return ret;
}

int main()
{
    freopen("cowpatibility.in","r",stdin);
    freopen("cowpatibility.out","w",stdout);

    for(ll i=1LL; i<=1000000LL; i++)
    {
        str[i] = tostr(i);
    }

    scanf(" %lld", &n);

    for(ll i=1LL; i<=n; i++)
    {
        for(ll j=1LL; j<=5LL; j++)
        {
            scanf(" %lld", &arr[i][j]);
            vec[arr[i][j]].push_back(i);
        }
        sort(arr[i]+1LL,arr[i]+6LL);
    }

    ll res = 0LL;

    for(ll i=1LL; i<=n; i++)
    {
        string cstr = "";
        set<ll> st;
        st.clear();
        for(ll j=1LL; j<=5LL; j++)
        {
            if(j>1LL)cstr+="-";
            cstr+=str[arr[i][j]];

        }
        if(!mpp2[cstr]) {
            mpp2[cstr]=true;
            for(ll j=1LL; j<=5LL; j++)
            {
                st.insert(vec[arr[i][j]].begin(),vec[arr[i][j]].end());
            }
            mpp[cstr]=st.size();
        }

        ll tp=mpp[cstr];

        res+=tp*(tp-1LL)/2LL;
        //printf("%lld\n",res);
    }

    res = n*(n-1LL)/2LL - (res/2LL);

    printf("%lld\n",res);

    return 0;
}
