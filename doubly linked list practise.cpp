#include<stdio.h>
#include<iostream>
using namespace std;

struct node{
   int val;
   node *prev;
   node *next;
};

struct doubly{

node *head,*tail;
doubly(){head=tail=NULL;}
void insert(int x);
void print(void);
void printrev(void);
void delhead(void);
void deltail(void);
void insertinto(int x,int pos);
void delelement(int x);
};

void doubly::insert(int x)
{
    node *temp=new node;
    temp->val=x;
    temp->prev=NULL;
    temp->next=NULL;
    if(head==NULL)
    {
        head=tail=temp;
    }
    else
    {
        tail->next=temp;
        temp->prev=tail;
        tail=temp;
    }
    return;
}

void doubly::print(void)
{
    node *temp=head;
    printf("printing.....\n");
    while(temp!=NULL)
    {
        cout<<temp->val<<endl;
        temp=temp->next;
    }
    delete temp;
    return;
}

void doubly::printrev(void)
{
    node *temp=new node;
    temp=tail;
    printf("Rev. printing\n");
    while(temp!=NULL)
    {
        printf("%d\n",temp->val);
        temp=temp->prev;
    }
    delete temp;
    return;
}

void doubly::delhead(void)
{
    head=head->next;
    head->prev=NULL;
    return;
}

void doubly::deltail(void)
{
    tail=tail->prev;
    tail->next=NULL;
    return;
}

void doubly::insertinto(int x,int pos)
{
    int i;
    node *temp=new node,*temp2=head;
    temp->val=x;
    if(pos==1)
    {
        head->prev=temp;
        temp->next=head;
        head=temp;
        return;
    }
    for(i=2;i<pos;i++)
    {
        temp2=temp2->next;
    }
    temp->next=temp2->next;
    temp2->next=temp;
    temp->prev=temp2;
    return;
}

void doubly::delelement(int x)
{
    node *temp=head,*temp2;
    if(temp->val==x)
    {
        head=head->next;
        head->prev=NULL;
        return;
    }
    while(temp->val!=x)
    {
        temp2=temp;
        temp=temp->next;
    }
    if(temp->next==NULL)
    {
        temp->prev->next=NULL;
        return;
    }
    temp2->next=temp->next;
    temp->next->prev=temp2;
    return;
}

main()
{
    doubly a;
    a.insert(2);
    a.insert(5);
    a.insert(3);
    a.insert(6);
    a.insert(8);
    a.insert(1);
    //a.print();
    //a.printrev();
    //a.delhead();
    //a.print();
    //a.deltail();
    a.print();
    a.insertinto(11,2);
    a.print();
    a.delelement(8);
    a.print();
    return 0;
}
