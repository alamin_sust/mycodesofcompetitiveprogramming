/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

map<int,int>mpp,mpp2;

int res[5010][5010],a[5010],arr[5010],i,j,k,n;
ll ans;


int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d",&n);

    for(i=1;i<=n;i++)
    {
        scanf(" %d",&arr[i]);
        a[i]=arr[i];
    }

    sort(arr+1,arr+n+1);
    k=0;
    for(i=1;i<=n;i++)
    {
        if(mpp[arr[i]]==0)
        {
            mpp[arr[i]]=++k;
            mpp2[k]=arr[i];
        }
    }

    for(i=1;i<=n;i++)
    {
        for(j=1;j<=k;j++)
        {
            res[i][j]=res[i-1][j];
            if(a[i]>mpp2[j])
            {
                res[i][j]++;
            }
        }
    }

    ans=0LL;

    for(i=1;i<=n;i++)
    {
        for(j=i+2;j<=n;j++)
        {
            if(a[i]>a[j])
            {
               // printf("%d %d %d\n",i,j,res[j-1][mpp[a[i]]]-res[i][mpp[a[i]]]);
                ans+=res[j-1][mpp[a[i]]]-res[i][mpp[a[i]]];
            }
        }
    }

    cout<<ans<<"\n";

    return 0;
}





