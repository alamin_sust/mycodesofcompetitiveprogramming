/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int arr[110],cum,sum,n,flag,cumu[110];

void rec(int pos,int taken,int sum)
{
    //printf("%d %d %d\n",pos,taken,sum);
    if(flag)
        return;
      //  printf("%d %d %d\n",pos,taken,sum);
    if((taken*2)==n)
    {
        if((sum*2)==cum)
            {flag=1;
             return;}
        return;
    }
   // printf("%d %d %d\n",pos,taken,sum);
    if(pos>=n)
        return;
   // printf("%d %d %d\n",pos,taken,sum);
    if((n-pos)<(n/2-taken))
        return;
  //  printf("%d %d %d\n",pos,taken,sum);
    if((sum*2)>cum||((cumu[n/2-taken]+sum)*2)>cum||((sum+cumu[n-pos]-cumu[n-pos-(n/2-taken)])*2)<cum)
        return;
   // printf("%d %d %d\n",pos,taken,sum);
    for(int i=pos+1;i<=n;i++)
    {
        rec(i,taken+1,sum+arr[i]);
    }
   // printf("%d %d %d\n",pos,taken,sum);
    return;
}

int main()
{
    int p,t,i;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {
        memset(arr,0,sizeof(arr));
        memset(cumu,0,sizeof(cumu));
        scanf(" %d",&n);
        cum=0;
        for(i=1; i<=n; i++)
        {
            scanf(" %d",&arr[i]);
            cum+=arr[i];
        }
        sort(arr+1,arr+n+1);
        for(i=1; i<=n; i++)
        {
            cumu[i]=cumu[i-1]+arr[i];
        }
        reverse(arr+1,arr+n+1);
        if(n%2)
        {
            printf("Case %d: No\n",p);
            continue;
        }
        flag=0;
        rec(0,0,0);
        if(flag)
            printf("Case %d: Yes\n",p);
        else
            printf("Case %d: No\n",p);

    }

    return 0;
}

/*
11
40
2 3 5 7 11 13 17 19 23 27 29 31
12
534
64
8678
52345
5987
25345
876
245
867
35
879
42
67
2
779
0257
78
245
678
87
22
78
3569
678572
2458
5468
6548
*/



