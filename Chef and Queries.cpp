/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll a,b,q,s[10000010],cnt1,cnt2,res,k,l,i,j,num,MOD=4294967296LL;
vector< pair<ll,ll> >adj1,adj2,pq1[429498],pq2[429498];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %lld %lld %lld %lld",&q,&s[1],&a,&b);
    num=s[1]/2;
    if(s[1]%2)
    pq1[num/10000].push_back(make_pair(num,1)),cnt1++;
    else
    pq2[num/10000].push_back(make_pair(num,1)),cnt2++;
    for(i=2;i<=q;i++)
    {
        s[i]=(a*s[i-1]+b)%MOD;
        num=s[i]/2;
        if(s[i]%2)
        {
             pq1[num/10000].push_back(make_pair(num,i));
             cnt1++;
        }
        else
        {
             pq2[num/10000].push_back(make_pair(num,i));
             cnt2++;
        }
    }
   // printf("--");
    k=0;
    while(cnt1)
    {
        l=pq1[k].size();
        sort(pq1[k].begin(),pq1[k].end());
        for(i=0;i<l;i++)
        {
            adj1.push_back(make_pair(pq1[k][i].first,pq1[k][i].second));

            cnt1--;
        }
        k++;
    }
    k=0;
    //printf("??");
    while(cnt2)
    {
        l=pq2[k].size();
        sort(pq2[k].begin(),pq2[k].end());
        for(i=0;i<l;i++)
        {
            adj2.push_back(make_pair(pq2[k][i].first,pq2[k][i].second));
//printf("%lld %lld",pq2[k][i].first,pq2[k][i].second);
            cnt2--;
        }
        k++;
    }
    //printf("..");
    res=0;
    for(i=0,j=0;i<adj1.size();)
    {
//        if(j==cnt2)
//        {
//          res+=adj1[i];
//        }
//        else if(adj1[i]<adj2[j])
//        {
//            res+=adj1[i];
//        }
        while(j<cnt2&&(adj2[j].first<adj1[i].first||(adj2[j].first==adj1[i].first&&adj2[j].second<adj1[i].second)))
        {
            j++;
        }
        if(j<cnt2&&adj2[j].first==adj1[i].first&&adj2[j].second>adj1[i].second)
        {
        }
        else
        {
            res+=adj1[i].first;
        }
       // printf("%lld %lld\n",i,j);
        num=adj1[i];
        while(i<cnt1&&adj1[i]==num)
            i++;
        num=adj2[j];
        while(j<cnt2&&adj2[j]==num)
            j++;

    }

    printf("%lld\n",res);

    return 0;
}

