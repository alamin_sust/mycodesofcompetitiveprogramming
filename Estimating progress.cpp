/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll l,r,res,n,prv,fg[50010][110],m,i,j,k,arr[500010],tp[500010];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    while(scanf("%lld",&n)!=EOF)
    {
        for(i=1LL;i<=n;i++)
        {
            scanf(" %lld",&arr[i]);
            for(j=0;j<=100;j++)
            {
                fg[i][j]=fg[i-1][j];
            }
            fg[i][arr[i]]++;
        }
        scanf(" %lld",&m);
        for(i=1LL;i<=m;i++)
        {
            scanf(" %lld %lld",&l,&r);
            for(j=0;j<=100;j++)
            {
                fg[n+1][j]=fg[r][j]-fg[l-1][j];
            }
            res=0LL;
            prv=-1;
            for(j=0;j<=100;j++)
            {
                if(fg[n+1][j]>0&&prv==-1)
                    prv=j;
                else if(fg[n+1][j])
                    res+=(j-prv)*(j-prv),prv=(j);
            }
            printf("%lld\n",res);
        }
    }

    return 0;
}


