/*Author : Md. Al- Amin
           Reg. No.: 2011331055
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#include <cmath>
#include <ctime>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define MAX 1000010
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll MOD=1000000000000LL,fact1[1000010],fact2[1000010],i,n,command,sieve[MAX], primecount1=0, primecount2=0, prime[MAX],arrival_time[1010],turn_around_time[1010],burst_time[1010],waiting_time[1010],total_turn_around_time,total_waiting_time;
char name[1010][110];

ll prime_generation(ll n) // it returns the total number of prime numbers between 1 to n
{
    for(ll i=2; i<=n; i++)
    {
        primecount1++;
        for(ll j=2; j<i; j++)
        {
            if(i%j==0)
            {
                primecount1--;
                break;
            }
        }
    }
    return primecount1;
}

ll prime_generation_sieve(ll n) // it returns the total number of prime numbers between 1 to n
{
    for(ll i=0; i<=n; i++)
        sieve[i]=1;
    sieve[0]=sieve[1]=0;
    for(ll i=2; i<=n; i++)
    {
        while(sieve[i]==0 && i<=n)
            i++;
        if(i>n)
            break;
        prime[primecount2]=i;
        for(ll j=i*i; j<=n; j+=i)
            sieve[j]=0;
        primecount2++;
    }
    return primecount2;
}

ll recursion_factorial(ll n) // it returns factorial n MOD 10^12
{
    if(n==1)
        return fact1[n]=1;
    return fact1[n]=((n%MOD)*(recursion_factorial(n-1)%MOD))%MOD;
}

ll loop_factorial(ll n) // it returns factorial n MOD 10^12
{
    fact2[0]=1;
    for(ll i=1; i<=n; i++)
    {
        fact2[i]=(fact2[i-1]*i)%MOD;
    }
    return fact2[n];
}

double exponential_function(ll n) // it returns e^2
{
    double res=1.0;
    for(ll i=1; i<=20; i++)
    {
        //printf("%lld--\n",fact2[i]);
        res+=(pow((double)n,(double)i)/(double)fact2[i]);
    }
    return res;
}

main()
{
    printf("If You want to get the results using burst times of (1)prime genaration function, (2)prime generation function(using sieve method) (3)recursion(factorial calculation), (4)loop(factorial calculation) and (5)exponential function e^x evaluation then press 1.\n\nor,\n\nIf You want to get results by putting the burst times manually then press 2\n");
    scanf(" %d",&command);
    if(command==1)
    {
        n=5;
        strcpy(name[1],"p1");
        name[2][0]='p';
        name[2][1]='2';
        name[3][0]='p';
        name[3][1]='3';
        name[4][0]='p';
        name[4][1]='4';
        name[5][0]='p';
        name[5][1]='5';
        clock_t start_time, end_time;
        start_time=clock();
        cout<<"p1: prime genaration function: total prime numbers between 1 and 1000 is "<<prime_generation(1000)<<endl;
        end_time=clock();
        burst_time[1]=(ll)(end_time-start_time)*1000;

        start_time=clock();
        cout<<"p2: prime generation function(using sieve method): total prime numbers between 1 and 1000 is "<<prime_generation_sieve(1000)<<endl;
        end_time=clock();
        burst_time[2]=(ll)(end_time-start_time)*1000;

        start_time=clock();
        cout<<"p3: recursion(factorial calculation): Factorial(1000)= "<<recursion_factorial(1000)<<endl;
        end_time=clock();
        burst_time[3]=(ll)(end_time-start_time)*1000;

        start_time=clock();
        cout<<"p4: loop(factorial calculation): Factorial(1000)= "<<loop_factorial(100000)<<endl;
        end_time=clock();
        burst_time[4]=(ll)(end_time-start_time)*1000;

        start_time=clock();
        cout<<"p5: exponential function e^x: e^2="<<exponential_function(2)<<endl;
        end_time=clock();
        burst_time[5]=(end_time-start_time);
        printf("%lldaaa\n",burst_time[5]);
    }
    else
    {
        printf("Enter The Number of Processes: ");
        cin>>n;
        for(i=1; i<=n; i++)
        {
            printf("Process %d Information:\n",i);
            printf("Name: ");
            scanf(" %s",&name[i]);
            printf("Arrival Time: ");
            scanf(" %d",&arrival_time[i]);
            printf("Burst Time: ");
            scanf(" %d",&burst_time[i]);
        }
    }
    //if(n>=1)
    //  turn_around_time[1]=burst_time[1];
    for(i=1; i<=n; i++)
    {
        waiting_time[i]=waiting_time[i-1]+burst_time[i-1]-arrival_time[i];
        turn_around_time[i]=waiting_time[i]+burst_time[i];
        total_waiting_time+=waiting_time[i];
        total_turn_around_time+=turn_around_time[i];
    }
    printf("      NAME    ARRIVAL TIME      BURST TIME    WAITING TIME     TURN AROUND TIME\n");
    for(i=1; i<=n; i++)
    {
        printf("%10s %15d %15d %15d %15d\n",name[i],arrival_time[i],burst_time[i],waiting_time[i],turn_around_time[i]);
    }
    printf("\nTotal Waiting Time: %d\n",total_waiting_time);
    printf("Average Waiting Time: %lf\n",(double)total_waiting_time/(double)n);
    printf("\nTotal Turn Around Time Time: %d\n",total_turn_around_time);
    printf("Average Turn Around Time Time: %lf\n",(double)total_turn_around_time/(double)n);
    return 0;
}
