#include <bits/stdc++.h>
#define ll long long int

using namespace std;

pair <ll,ll> lagao(ll n, ll a, ll b) {
	if(n<=a*b) {
		pair <ll, ll> p;
		p.first = a;
		p.second =b;
		return p;
	} else {
		pair <ll, ll> a1 = lagao(n,a+1,b);
		pair <ll, ll> a2 = lagao(n,a,b+1);
		if(a1.first*a1.second<a2.first*a2.second) {
			return a1;
		} else {
			return a2;
		}
	}
}

int main() {
	ll n, a, b;
	cin >> n >> a >> b;
	ll num = 6*n;
	pair <ll,ll> ans =  lagao(num,a,b);
	cout << ans.first * ans.second << endl;
	cout << ans.first << " " << ans.second << endl;
	return 0;
}
