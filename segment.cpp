/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
#define SZ 1000003
ll treeMax[4*SZ];
ll treeMin[4*SZ];
ll arr[SZ];
pair<ll,ll>pll;

void initMax(ll node,ll b,ll e)
{
    if(b==e)
    {
        treeMax[node]=arr[b];
        return;
    }
    ll left = node*2;
    ll right = node*2+1;
    ll mid = (b+e)/2;
    initMax(left,b,mid);
    initMax(right,mid+1,e);
    treeMax[node] = max(treeMax[left],treeMax[right]);
}


void initMin(ll node,ll b,ll e)
{
    if(b==e)
    {
        treeMin[node]=arr[b];
        return;
    }
    ll left = node*2;
    ll right = node*2+1;
    ll mid = (b+e)/2;
    initMin(left,b,mid);
    initMin(right,mid+1,e);
    treeMin[node] = min(treeMin[left],treeMin[right]);
}

ll queryMax(ll node,ll b,ll e,ll i,ll j)
{
    if(i>e||j<b)return -9999999999999LL;
    if(b>=i&&e<=j) return treeMax[node];
    ll left = node*2;
    ll right = node*2+1;
    ll mid = (b+e)/2;
    return max(queryMax(left,b,mid,i,j),queryMax(right,mid+1,e,i,j));
}

ll queryMin(ll node,ll b,ll e,ll i,ll j)
{
    if(i>e||j<b)return 9999999999999LL;
    if(b>=i&&e<=j) return treeMin[node];
    ll left = node*2;
    ll right = node*2+1;
    ll mid = (b+e)/2;
    return min(queryMin(left,b,mid,i,j),queryMin(right,mid+1,e,i,j));
}

int main()
{
    ll n,k,i;
    scanf("%I64d %I64d",&n,&k);
    for(i = 1; i<=n; i++)
    {
        scanf("%I64d",&arr[i]);
    }
    initMax(1,1,n);
    initMin(1,1,n);
    k--;
    for(i=1; i+k<=n; i++)
    {
        if(i!=1)printf(" %I64d",queryMin(1,1,n,i,i+k));
        else printf("%I64d",queryMin(1,1,n,i,i+k));
    }
    printf("\n");
    for(i=1; i+k<=n; i++)
    {
        if(i!=1)printf(" %I64d",queryMax(1,1,n,i,i+k));
        else printf("%I64d",queryMax(1,1,n,i,i+k));
    }
    printf("\n");
    return 0;
}

