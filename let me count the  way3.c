#include<stdio.h>
#define MAX 31000
int coin[5]= {50,25,10,5,1};
main()
{
    long long int a,i,j,total[MAX+1];

    total[0]=1;
    for(i=0; i<5; i++)
    {
        a=coin[i];
        for(j=a; j<=MAX; j++)
            total[j]+=total[j-a];
    }
    total[0]=0;

    int n;
    while (scanf("%d",&n)!=EOF)
    {
        if(n==0)
            printf("There is only 1 way to produce 0 cents change.\n");
        else if(total[n]!=1)
            printf("There are %lld ways to produce %d cents change.\n",total[n],n);
        else
            printf("There is only 1 way to produce %d cents change.\n",n);
    }
    return 0;
}

