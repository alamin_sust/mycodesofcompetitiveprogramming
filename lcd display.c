#include<iostream>
#include<cstdlib>
#include<cstring>
using namespace std;

void print(int i , int j , int size , string n , int c);

bool test(int num , int part);

int main()
{
    int size ;

    while(cin>>size&&size)
    {
        int c ;

        string n;

        cin>>n;

        for(int i=0;i<n.length();i++)
        {
            n[i]-=48;
        }

        c=n.length();

        for(int i=1;i<=(2*size+3);i++)
        {
            for(int j=0;j<c;j++)
            {
                print(i , j , size , n , c);

                if(j<c-1)cout<<" ";
            }

            cout<<endl;
        }

        cout<<endl;
    }

    return 0;
}

void print(int i , int j , int size , string n , int c)
{
    if(i==1)
    {
        cout<<" ";

        if(test(n[j] , 1))
        {
            for(int i=1;i<=size;i++)
            {
                cout<<"-";
            }
        }
        else
        {
            for(int i=1;i<=size;i++)
            {
                cout<<" ";
            }
        }

        cout<<" ";
    }
    else if(i<(size+2))
    {
        if(test(n[j] , 2))cout<<"|";
        else cout<<" ";

        for(int i=1;i<=size;i++)cout<<" ";

        if(test(n[j] , 3))cout<<"|";
        else cout<<" ";
    }
    else if(i==(size+2))
    {
        cout<<" ";

        if(test(n[j] , 4))
        {
            for(int i=1;i<=size;i++)
            {
                cout<<"-";
            }
        }
        else
        {
            for(int i=1;i<=size;i++)
            {
                cout<<" ";
            }
        }

        cout<<" ";
    }
    else if(i<(2*size+3))
    {
        if(test(n[j] , 5))cout<<"|";
        else cout<<" ";

        for(int i=1;i<=size;i++)cout<<" ";

        if(test(n[j] , 6))cout<<"|";
        else cout<<" ";
    }
    else
    {
        cout<<" ";

        if(test(n[j] , 7))
        {
            for(int i=1;i<=size;i++)
            {
                cout<<"-";
            }
        }
        else
        {
            for(int i=1;i<=size;i++)
            {
                cout<<" ";
            }
        }

        cout<<" ";
    }
}

bool test(int num , int part)
{
    switch(num)
    {
        case 0:
            switch(part)
            {
                case 1:
                    return 1;
                case 2:
                    return 1;
                case 3:
                    return 1;
                case 4:
                    return 0;
                case 5:
                    return 1;
                case 6:
                    return 1;
                case 7:
                    return 1;
            }
        case 1:
            switch(part)
            {
                case 1:
                    return 0;
                case 2:
                    return 0;
                case 3:
                    return 1;
                case 4:
                    return 0;
                case 5:
                    return 0;
                case 6:
                    return 1;
                case 7:
                    return 0;
            }
        case 2:
            switch(part)
            {
                case 1:
                    return 1;
                case 2:
                    return 0;
                case 3:
                    return 1;
                case 4:
                    return 1;
                case 5:
                    return 1;
                case 6:
                    return 0;
                case 7:
                    return 1;
            }
        case 3:
            switch(part)
            {
                case 1:
                    return 1;
                case 2:
                    return 0;
                case 3:
                    return 1;
                case 4:
                    return 1;
                case 5:
                    return 0;
                case 6:
                    return 1;
                case 7:
                    return 1;
            }
        case 4:
            switch(part)
            {
                case 1:
                    return 0;
                case 2:
                    return 1;
                case 3:
                    return 1;
                case 4:
                    return 1;
                case 5:
                    return 0;
                case 6:
                    return 1;
                case 7:
                    return 0;
            }
        case 5:
            switch(part)
            {
                case 1:
                    return 1;
                case 2:
                    return 1;
                case 3:
                    return 0;
                case 4:
                    return 1;
                case 5:
                    return 0;
                case 6:
                    return 1;
                case 7:
                    return 1;
            }
        case 6:
            switch(part)
            {
                case 1:
                    return 1;
                case 2:
                    return 1;
                case 3:
                    return 0;
                case 4:
                    return 1;
                case 5:
                    return 1;
                case 6:
                    return 1;
                case 7:
                    return 1;
            }
        case 7:
            switch(part)
            {
                case 1:
                    return 1;
                case 2:
                    return 0;
                case 3:
                    return 1;
                case 4:
                    return 0;
                case 5:
                    return 0;
                case 6:
                    return 1;
                case 7:
                    return 0;
            }
        case 8:
            switch(part)
            {
                case 1:
                    return 1;
                case 2:
                    return 1;
                case 3:
                    return 1;
                case 4:
                    return 1;
                case 5:
                    return 1;
                case 6:
                    return 1;
                case 7:
                    return 1;
            }
        case 9:
            switch(part)
            {
                case 1:
                    return 1;
                case 2:
                    return 1;
                case 3:
                    return 1;
                case 4:
                    return 1;
                case 5:
                    return 0;
                case 6:
                    return 1;
                case 7:
                    return 1;
            }
    }

    exit(1);
}
