#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class WakingUpEasy
{
public:
	int countAlarms(vector <int> volume, int S)
	{
	    int res=0,k=0;
	    for(int i=0;i<volume.size();i++)
        {
            k+=volume[i];
        }
        res+=volume.size()*(S/k);
        S%=k;
        for(int i=0;S>0&&i<volume.size();i++)
        {
            res++;
            S-=volume[i];
        }
	    return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	WakingUpEasy objectWakingUpEasy;

	//test case0
	vector <int> param00;
	param00.push_back(5);
	param00.push_back(2);
	param00.push_back(4);
	int param01 = 13;
	int ret0 = objectWakingUpEasy.countAlarms(param00,param01);
	int need0 = 4;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(5);
	param10.push_back(2);
	param10.push_back(4);
	int param11 = 3;
	int ret1 = objectWakingUpEasy.countAlarms(param10,param11);
	int need1 = 1;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(1);
	int param21 = 10000;
	int ret2 = objectWakingUpEasy.countAlarms(param20,param21);
	int need2 = 10000;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(42);
	param30.push_back(68);
	param30.push_back(35);
	param30.push_back(1);
	param30.push_back(70);
	param30.push_back(25);
	param30.push_back(79);
	param30.push_back(59);
	param30.push_back(63);
	param30.push_back(65);
	param30.push_back(6);
	param30.push_back(46);
	param30.push_back(82);
	param30.push_back(28);
	param30.push_back(62);
	param30.push_back(92);
	param30.push_back(96);
	param30.push_back(43);
	param30.push_back(28);
	param30.push_back(37);
	param30.push_back(92);
	param30.push_back(5);
	param30.push_back(3);
	param30.push_back(54);
	param30.push_back(93);
	param30.push_back(83);
	param30.push_back(22);
	param30.push_back(17);
	param30.push_back(19);
	param30.push_back(96);
	param30.push_back(48);
	param30.push_back(27);
	param30.push_back(72);
	param30.push_back(39);
	param30.push_back(70);
	param30.push_back(13);
	param30.push_back(68);
	param30.push_back(100);
	param30.push_back(36);
	param30.push_back(95);
	param30.push_back(4);
	param30.push_back(12);
	param30.push_back(23);
	param30.push_back(34);
	param30.push_back(74);
}
