#include<stdio.h>
#define max_(a,b) (a>b?a:b)

long long int a[10010],mark[10010][10010],store[10010][10010],n,t=0;

long long int dp(long long int i,long long int w)
{
   if(i==0 || w==0)
      return 0;
   else if(mark[i][w]==t)
      return store[i][w];
   long long int res;
   if(a[i]<=w)
   {
      res=max_(dp(i-1,w),dp(i-1,w-a[i])+a[i]);
   }
   else
      res=dp(i-1,w);
   mark[i][w]=t;
   store[i][w]=res;
   return res;
}
int main()
{
   long long int i,w,cs,tt,sum,x;
   scanf(" %lld",&tt);
   for(cs=1;cs<=tt;cs++)
   {
      t=1;
      scanf(" %lld",&n);
      sum=0;
      for(i=1;i<=n;i++)
      {
         scanf(" %lld",&a[i]);
         sum+=a[i];
      }
      w=sum/2;
      x=dp(n,w);
      printf("%lld\n",(sum-2*x));
   }
   return 0;
}
