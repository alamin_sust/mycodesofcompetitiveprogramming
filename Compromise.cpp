/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

map<string,int>mpp;
int fg=0,dir[110][110],mat[110][110],s1[110],s2[110],k1,k2;
char arr1[110][35],arr2[110][35],str[110][35];

void rec(int i,int j)
{
    if(i==0||j==0)
        return;
    if(dir[i][j]==1)
    {
        rec(i-1,j-1);
        if(fg)
            printf(" ");
        fg=1;
        printf("%s",str[s1[i]]);

    }
    else if(dir[i][j]==2)
        rec(i-1,j);
    else
        rec(i,j-1);
    return;
}

int main()
{
    int i,j,now;
    while(scanf("%s",&arr1[1])!=EOF)
    {
        k1=1;
        while(arr1[k1][0]!='#')
            scanf("%s",&arr1[++k1]);
        k2=1;
        scanf(" %s",&arr2[k2]);
        while(arr2[k2][0]!='#')
            scanf("%s",&arr2[++k2]);
        mpp.clear();
        fg=0;
        for(i=0; i<103; i++)
        {
            for(j=0; j<32; j++)
            {
                str[i][j]='\0';
            }
        }

        now=1;
        for(i=1; i<k1; i++)
        {
            if(mpp[arr1[i]]==0)
            {
                strcpy(str[now],arr1[i]);
                mpp[arr1[i]]=now++;
            }
            s1[i]=mpp[arr1[i]];
        }
        for(i=1; i<k2; i++)
        {
            if(mpp[arr2[i]]==0)
            {
                strcpy(str[now],arr2[i]);
                mpp[arr2[i]]=now++;
            }
            s2[i]=mpp[arr2[i]];
        }

        for(i=1; i<k1; i++)
        {
            for(j=1; j<k2; j++)
            {
                if(s1[i]==s2[j])
                {
                    mat[i][j]=mat[i-1][j-1]+1;
                    dir[i][j]=1;
                }
                else
                {
                    if(mat[i-1][j]>mat[i][j-1])
                    {
                        mat[i][j]=mat[i-1][j];
                        dir[i][j]=2;
                    }
                    else
                    {
                        mat[i][j]=mat[i][j-1];
                        dir[i][j]=3;
                    }
                }
            }
        }
        rec(k1-1,k2-1);
        printf("\n");
    }
    return 0;
}

