/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

queue<int>q;
int col[100010],val[100010],arr[100010];
vector<int>adj[100010];

int bfs(int src)
{
    int ret=0;
    q.push(src);
    col[src]=1;
    val[src]=1;
    int leaf=src;
    while(!q.empty())
    {
       int u=q.front();
       q.pop();
       for(int i=0;i<adj[u].size();i++)
       {
           int v=adj[u][i];
           if(col[v]==0&&arr[v]==1)
           {
               leaf=v;
               col[v]=1;
               q.push(v);
           }
       }
    }

    while(!q.empty())q.pop();
    memset(col,0,sizeof(col));

    src=leaf;
    q.push(src);
    col[src]=1;
    ret=val[src]=1;
    while(!q.empty())
    {
       int u=q.front();
       q.pop();
       for(int i=0;i<adj[u].size();i++)
       {
           int v=adj[u][i];
           if(col[v]==0&&arr[v]==1)
           {
               leaf=v;
               col[v]=1;
               q.push(v);
               val[v]=val[u]+1;
               ret=max(ret,val[v]);
           }
       }
    }

    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    int n,i,j,res;

    scanf(" %d",&n);

    for(i=1;i<=n;i++)
    {
        scanf(" %d",&arr[i]);
    }

    for(i=2;i<=n;i++)
    {
        scanf(" %d",&j);
        adj[i].push_back(j);
        adj[j].push_back(i);
    }
    res=0;
    for(i=1;i<=n;i++)
    {
        if(col[i]==0&&arr[i]==1)
        {
            res=max(res,bfs(i));
        }
    }

    printf("%d\n",res);


    return 0;
}
