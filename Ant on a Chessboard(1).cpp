/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll x,y,flag,now,n,k,ext;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    while(scanf("%lld",&n)==1&&n)
    {
        now=0LL,k=1LL,flag=0LL;
        while(now<n)
        {
           now+=k;
           k+=2LL;
           flag^=1LL;
        }
        //k-=2;
        now-=(k-2LL);
        ext=(n-now);
        k-=2LL;

        k=(k+1LL)/2LL;

        printf("%lld %lld %lld\n",k,now,flag);

        if((ext*2LL)<(k*2LL+1LL))
        {

            if(flag)
            {

                x=k;
                y=ext;
            }
            else
            {

                y=k;
                x=ext;
            }
        }
        else
        {
            if(flag)
            {
                 printf("..");
                y=k;
                x=(k*2LL-1LL)-(now)+1LL;
            }
            else
            {
                x=k;
                y=(k*2LL-1LL)-(now)+1LL;
            }

        }

        printf("%lld %lld\n",x,y);

    }

    return 0;
}

