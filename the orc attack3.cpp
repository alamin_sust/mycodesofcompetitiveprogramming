#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

main()
{
    int t,p,i,j,k,n,m,a,c,b,flag,mx,res,adj[110][110];
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n>>m;
        for(i=1;i<=n;i++)
            for(j=1;j<=n;j++)
                if(i==j)
                adj[i][i]=0;
                else
                adj[i][j]=inf;
        for(i=1;i<=m;i++)
        {
            scanf(" %d %d %d",&a,&b,&c);
            adj[a][b]=adj[b][a]=min(adj[b][a],c);
        }
        for(k=1;k<=n;k++)
        {
            for(i=1;i<=n;i++)
            {
                for(j=1;j<=n;j++)
                {
                    if(adj[i][j]>adj[i][k]+adj[k][j])
                        adj[i][j]=adj[i][k]+adj[k][j];
                }
            }
        }
        for(res=inf,i=1;i<=n;i++)
        {
            for(mx=-1,flag=0,j=1;j<=n;j++)
            {
                if(adj[i][j]==inf)
                    {flag=1;
                    break;}
                mx=max(mx,adj[i][j]);
            }
            if(flag==1)
                continue;
            for(flag=0,j=1;j<5;j++)
            {
                if(adj[i][j]!=adj[i][j+1])
                {flag=1;
                break;}
            }
            if(flag==1)
                continue;
            res=min(res,mx);
        }
        if(res==inf)
        printf("Map %d: -1\n",p);
        else
        printf("Map %d: %d\n",p,res);
    }
    return 0;
}

