/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int t,i,p,j,k,arr[100010],m,n,mn,cnt,seg,ans,st,en;
map<int,int>mpp;

struct node{
int l,r,jump;
};

vector<int>v,res[322];

node q[100010];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %d %d",&n,&m);
        mpp.clear();
        v.clear();
        //mpp2.clear();
        cnt=0;

        for(i=0;i<n;i++)
        {
            scanf(" %d",&arr[i]);
            v.push_back(arr[i]);
        }

        seg=sqrt(n);
        for(i=0;i<m;i++)
        {
            scanf("%d %d %d",&q[i].l,&q[i].r,&q[i].jump);
            v.push_back(q[i].jump);
        }

        sort(v.begin(),v.end());

        for(i=0;i<v.size();i++)
        {
            if(mpp[v[i]]==0)
            {
                //mpp2[++cnt]=arr[i];
                mpp[v[i]]=++cnt;
            }
        }

        for(i=0;i<=seg+1;i++)
        {
            res[i].clear();
            for(j=0;j<=cnt+3;j++)
                res[i].push_back(0);
        }
        res[0][1]++;
        //printf("..");
        //memset(res,0,sizeof(res));
        for(i=0,k=0;i<n;k++)
        {
            for(j=0;j<seg&&i<n;i++,j++)
            {
                //printf("%d..%d\n",k,mpp[arr[i]]);
                res[k][mpp[arr[i]]]++;
            }

            for(j=1;j<=cnt;j++)
            {
                res[k][j]+=res[k][j-1];
            }
        }
        printf("Case %d:\n",p);
        for(i=0;i<m;i++)
        {
            ans=0;
            st=q[i].l/seg+1;
            en=q[i].r/seg-1;
            //printf("%d %d %d\n",seg,st,en);
            for(j=st;j<=en;j++)
            {
                ans+=res[j][mpp[q[i].jump]];
            }

            if(st>en)
            {
                for(j=q[i].l;j<=q[i].r;j++)
                {
                    if(arr[j]<=q[i].jump)
                ans++;
                }
                printf("%d\n",ans);
                continue;
            }

            st=st*seg;
            for(j=q[i].l;j<st;j++)
            {
                if(arr[j]<=q[i].jump)
                ans++;
            }
            en=(en+1)*seg;
            for(j=en;j<=q[i].r;j++)
            {
                if(arr[j]<=q[i].jump)
                ans++;
            }
            printf("%d\n",ans);
        }
    }

    return 0;
}





