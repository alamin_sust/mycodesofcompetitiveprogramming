#include <stdio.h>
#include <math.h>
#include <string.h>

long long prime[1000000], countPrime=0;
char mark[10000000];
//int countAllPrimes( int n );
long long isPrime( long long ITEM );

int main()
{
    long long int a, b, c;
    long long int i, j, n, count=0, limit;
    double f=4.9, C=6.5;

    printf("Input the range\n");
    scanf("%lld %lld", &a, &b);         // a is lower, b is higher

    memset( mark, '0', sizeof(mark));       // Initialize the mark[] array with '0'

    // First generate the prime numbers upto sqrt(b);
    n=sqrt(b)+1;
    limit=sqrt(n);
    prime[countPrime++]=2;
    for( i=4; i<=n; i+=2 )  mark[i]='1';

    for( i=3; i<=n; i+=2 )
    {
        if( mark[i]=='0' )
        {
            prime[countPrime++]=i;
            if( i<=limit )
                for( j=i*i; j<=n; j+=2*i )
                    mark[j]='1';
        }
    }

    long long int size=b-a+1;
    long long int arr[size];
    long long k, store=a, m=0;

    // store the values from a to b to an array
    for( k=0; k<size; k++ ) arr[k]=store++;
    printf("total countPrime: %lld\n", countPrime);

    // Now compute the primes from a to b
    for( i=0; i<countPrime; i++ )
    {
        long long int start=(ceil((double)a/prime[i]))*prime[i];      // Start of the range from [a,b]
        long long int end=(floor((double)b/prime[i]))*prime[i];       // End of the range from [a,b]
        for( j=start; j<=end; j+=prime[i] )
            {//if((j-a)>=size||(j-a)<0)
            //{
            //    printf("j=%lld a=%lld j-a=%lld size=%lld\n",j,a,j-a,size);
            //    getchar();
            //}
            if(arr[j-a]!=1&&prime[i]!=j)
                arr[j-a]=1;
            }
    }


    // Count the primes into the range [a,b]
for( j=0; j<size; j++ )
    if( arr[j]!=1 )
            printf("--%lld\n",j),count++;

    printf("Total #of prime is %lld\n", count);
    return 0;
}


