/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int dp[1010][5][22][120],l;
char arr[10010];

int rec(int pos,int stat,int taken,int got)
{
    //printf("%d %d %d %d\n",pos,stat,taken,got);
    if(pos==l)
    {
        if(stat==0&&taken==0)
            return got;
        return 0;
    }
    int &ret=dp[pos][stat][taken][got];
    if(ret!=-1)
        return ret;
    ret=0;
    if(stat==0)
    {
        if(taken<15)
            ret=max(ret,rec(pos+1,stat,taken+1,got));
        if(taken>=2)
            ret=max(ret,rec(pos+1,stat+1,0,got));
    }
    if(stat==1)
    {
        if(taken==0&&(pos+1)<l&&arr[pos]=='a'&&arr[pos+1]=='t')
        {
            ret=max(ret,rec(pos+2,stat,taken+2,got));
        }
        if(taken!=0)
        {
            if(taken<(15+2))
                ret=max(ret,rec(pos+1,stat,taken+1,got));
            if(taken>=(2+2))
                ret=max(ret,rec(pos+1,stat+1,0,got));
        }
    }
    if(stat==2)
    {
        if(taken==0&&(pos+2)<l&&arr[pos]=='d'&&arr[pos+1]=='o'&&arr[pos+2]=='t')
        {
            ret=max(ret,rec(pos+3,stat,taken+3,got));
        }
        if(taken!=0)
        {
            if(taken<(15+3))
                ret=max(ret,rec(pos+1,stat,taken+1,got));
            if(taken>=(2+3))
            {
                ret=max(ret,rec(pos+1,stat+1,0,got));
                ret=max(ret,rec(pos+1,0,0,got+1));
            }
        }
    }
    if(stat==3)
    {
        if(taken==0&&(pos+2)<l&&arr[pos]=='d'&&arr[pos+1]=='o'&&arr[pos+2]=='t')
        {
            ret=max(ret,rec(pos+3,stat,taken+3,got));
        }
        if(taken!=0)
        {
            if(taken<(15+3))
                ret=max(ret,rec(pos+1,stat,taken+1,got));
            if(taken>=(1+3))
            {
                if(taken>=(2+3))
                    ret=max(ret,rec(pos+1,stat+1,0,got));
                ret=max(ret,rec(pos+1,0,0,got+1));
            }
        }
    }
    if(stat==4)
    {
        if(taken==0&&(pos+2)<l&&arr[pos]=='d'&&arr[pos+1]=='o'&&arr[pos+2]=='t')
        {
            ret=max(ret,rec(pos+3,stat,taken+3,got));
        }
        if(taken!=0)
        {
            if(taken<(1+3))
                ret=max(ret,rec(pos+1,stat,taken+1,got));
            if(taken==(1+3))
            {
                ret=max(ret,rec(pos+1,0,0,got+1));
            }
        }
    }
    return ret;
}

int main()
{
    int t,p,i,flag,res;
    cin>>t;
    getchar();
    for(p=1; p<=t; p++)
    {

        gets(arr);
        l=strlen(arr);
        flag=0;
        for(i=0; i<l; i++)
        {
            if(arr[i]==' ')
            {
                printf("$0.00\n");
                flag=1;
                break;
            }
        }
        if(flag)
            continue;
        memset(dp,-1,sizeof(dp));
        res=rec(0,0,0,0);
        //printf("%d\n",res);
        printf("$%.2lf\n",(double)res*1.28);
    }
    return 0;
}
/*
6
aaaaaaaameatmeedotcombbbbbbbbb
bbbbmattatdiedotdotnetpatdotdotherityatiinetdotnetaaaa
myemailathomedot com
myemailatabcdefghijklmnopqrstuvwxyzdotcom
helmattatdidotdotnet
helmatdotdotherityatiinetdotnet
*/
