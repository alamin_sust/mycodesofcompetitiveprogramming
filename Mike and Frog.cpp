/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,yy1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-yy1)*(y2-yy1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll n,x1,x2,yy1,y2,h1,h2,i,m,a1,a2,arr1[3000015],arr2[3000015],res1,res2,flag1,flag2;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %I64d",&m);
    scanf(" %I64d %I64d",&h1,&a1);
    scanf(" %I64d %I64d",&x1,&yy1);
    scanf(" %I64d %I64d",&h2,&a2);
    scanf(" %I64d %I64d",&x2,&y2);
    arr1[0]=h1;
    arr2[0]=h2;
    for(i=1LL;i<=3000010LL;i++)
    {
       // if(i<10)
       // printf("%I64d %I64d..\n",h1,h2);
        arr1[i]=(x1*h1+yy1)%m;
        arr2[i]=(x2*h2+y2)%m;
        h1=arr1[i];
        h2=arr2[i];
    }

    for(i=0LL;i<=3000010LL;i++)
    {
        if(arr1[i]==a1&&arr2[i]==a2)
        {
            printf("%I64d\n",i);
            return 0;
        }
    }
   // if(res1==-1||res2==-1)
    printf("-1\n");
   // else
   // printf("%I64d\n",max(res1,res2));
    return 0;
}


