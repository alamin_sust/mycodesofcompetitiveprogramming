/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

stack<int>stk[50100];
map<int,int>mpp;
int t,p,z,i,j,id[21000],fg[21000],hh,n,q,x,near;
char arr[110];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    scanf(" %d",&t);
    for(p=1; p<=t; p++)
    {
        for(i=0; i<=20050; i++)
        {
            fg[i]=0;
            id[i]=i;
            while(!stk[i].empty())
                stk[i].pop();
        }
        hh=10010;
        mpp.clear();

        scanf(" %d %d",&n,&q);
        printf("Case %d:\n",p);
        for(z=0; z<q; z++)
        {

            scanf(" %s",arr);
            if(strcmp(arr,"push")==0)
            {
                scanf(" %d %d",&i,&x);
                if(mpp[id[i]]==0)
                {
                    mpp[id[i]]=id[i];
                    if(fg[id[i]]==0)
                        stk[id[i]].push(x);
                    else
                    {
                        id[id[i]]=hh++;
                        stk[id[id[i]]].push(x);
                    }
                }
                else if(mpp[id[i]]==id[i])
                {
                    if(fg[id[i]]==0)
                        stk[id[i]].push(x);
                    else
                    {
                        id[id[i]]=hh++;
                        stk[id[id[i]]].push(x);
                    }
                }
                else
                {
                    while(mpp[id[i]]!=id[i])
                    {
                        i=mpp[id[i]];
                    }
                    if(fg[id[i]]==0)
                        stk[id[i]].push(x);
                    else
                    {
                        id[id[i]]=hh++;
                        stk[id[id[i]]].push(x);
                    }
                }
            }
            else if(strcmp(arr,"pop")==0)
            {
                scanf(" %d",&i);

                near=id[i];

                while(mpp[id[i]]!=id[i])
                {
                    i=mpp[id[i]];
                    if(!stk[id[i]].empty())
                        near=id[i];
                }

                stk[near].pop();
            }
            else if(strcmp(arr,"put")==0)
            {
                scanf(" %d %d",&i,&j);
                while(mpp[id[i]]!=id[i])
                    i=mpp[id[i]];
                mpp[id[i]]=id[j];
                fg[id[j]]=1;
            }
            else if(strcmp(arr,"top")==0)
            {
                scanf(" %d",&i);
                near=id[i];
                printf("%d..\n",mpp[id[i]]);
                while(mpp[id[i]]!=id[i])
                {
                    i=mpp[id[i]];
                    if(!stk[id[i]].empty())
                        near=id[i];
                }
                if(!stk[2].empty())
                printf("%d--\n",stk[2].top());
                if(stk[near].empty())
                    printf("Empty!\n");
                else
                    printf("%d\n",stk[near].top());
            }
        }
    }
    return 0;
}





