#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

long long int res[120][120],mat[35][120][120];

void make_node(long long int a,long long int b)
{
    //printf("%lld %lld\n",a,b);
    long long int flag[15];
    for(long long int i=1;i<11;i++)
        if((a<(i-1)||a>(i+1))&&(b<(i-1)||b>(i+1)))
        {flag[i]=0;
      //  printf("JJJ");
      }
        else
        {flag[i]=1;
        //printf("mm");
        }
    for(long long int i=1;i<9;i++)
    {
        for(long long int j=i+2;j<11;j++)
        {
              if(flag[i]==0&&flag[j]==0)
              {mat[0][a*10+b][i*10+j]++;
              //printf("%lld %lld %lld %lld\n",a,b,i,j);
              }
        }
    }
    return;
}

void make_adj_mat(void)
{
    for(long long int i=1;i<9;i++)
    {
        for(long long int j=i+2;j<11;j++)
        {
            //printf("..");
              make_node(i,j);
        }
    }
}

void calc(long long int power)
{
    for(long long int i=1;i<111;i++)
    {
        for(long long int j=1;j<111;j++)
        {
            for(long long int k=1;k<111;k++)
            res[i][j]=(res[i][j]+((mat[power][i][k]*mat[power][k][j]))%(1<<31))%(1<<31);
        }
    }
    return;
}

void make_bigmod(long long int power)
{
    for(long long int i=1;i<111;i++)
    {
        for(long long int j=1;j<111;j++)
        {
            for(long long int k=1;k<111;k++)
            mat[power][i][j]=(mat[power][i][j]+((mat[power-1][i][k]*mat[power-1][k][j]))%(1<<31))%(1<<31);
        }
    }
    return;
}

void prnt(void)
{

}

main()
{
    long long int n,p,t,i,j,flag,result;
    //printf("%d\n",1<<4);
    cin>>t;
    make_adj_mat();
    prnt();
    //make_bigmod();
    for(i=1;i<33;i++)
        make_bigmod(i);
    for(p=1;p<=t;p++)
    {
        cin>>n;
        if(n==1)
        {printf("Case %lld: 36\n",p);
        continue;}
        n--;
        flag=0;
        long long int k=0;
        while(((n>>k)|0)!=0)
        {
            if(((n>>k)&1)==1)
            {
            if(flag==0)
            for(flag=1,i=1;i<111;i++)
            for(j=1;j<111;j++)
                res[i][j]=mat[k][i][j];
            else
                calc(k);
            }
            k++;
        }
        for(result=0,i=1;i<111;i++)
        {
            for(j=1;j<111;j++)
                result+=res[i][j];
        }
        printf("Case %lld: %lld\n",p,result);
    }
    return 0;
}

