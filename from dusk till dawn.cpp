#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
map<string,int>mpp;
struct node{
int x,st,en;
};
node det;
vector<node>adj[1010];

struct node2{
int blood,x,arrival;
};
node2 det2,det3;
bool operator<(node2 a,node2 b)
{
    return a.blood>b.blood;
}
int n,stat[1010];
priority_queue<node2>pq;

int dijkstra(int source,int dest)
{
    while(!pq.empty())
        pq.pop();
    for(int i=0;i<=1005;i++)
    stat[i]=0;
    det2.blood=0;
    det2.x=source;
    det2.arrival=0;
    pq.push(det2);
    while(!pq.empty())
    {
        det2=pq.top();
        stat[det2.x]=1;
        pq.pop();
        if(det2.x==dest)
            return det2.blood;
        for(int i=0;i<adj[det2.x].size();i++)
        {
            if(stat[adj[det2.x][i].x]==1)
            continue;
            int bd=det2.blood;
            if(det2.arrival>adj[det2.x][i].st)
            bd++;
            det3.x=adj[det2.x][i].x;
            det3.blood=bd;
            det3.arrival=adj[det2.x][i].en;
            pq.push(det3);
        }
    }
    return -1;
}


main()
{
    int t,p,m,i,stt,dur,res;
    char a1[1010],a2[1010];
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>m;
        n=0;
        mpp.clear();
        for(i=0;i<=1005;i++)
            adj[i].clear();
        for(i=1;i<=m;i++)
        {
            scanf(" %s %s %d %d",a1,a2,&stt,&dur);
            if(mpp[a1]==0)
                mpp[a1]=++n;
            if(mpp[a2]==0)
                mpp[a2]=++n;
            det.x=mpp[a2];
            det.st=stt;
            if(det.st<=6)
                det.st+=24;
            det.en=det.st+dur;
            if(det.st>=18&&det.en<=30)
            adj[mpp[a1]].push_back(det);
        }
        scanf(" %s %s",a1,a2);
        printf("Test Case %d.\n",p);
        if(mpp[a1]==mpp[a2]&&mpp[a1]!=0)
        {
            printf("Vladimir needs 0 litre(s) of blood.\n");
            continue;
        }
        if(mpp[a1]==0||mpp[a2]==0)
        {
            printf("There is no route Vladimir can take.\n");
            continue;
        }
        res=dijkstra(mpp[a1],mpp[a2]);
        if(res==-1)
            printf("There is no route Vladimir can take.\n");
        else
            printf("Vladimir needs %d litre(s) of blood.\n",res);
    }
    return 0;
}

