#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int inc[1010];
vector<int>res,adj[1010];
queue<int>q;

main()
{
    int i,n,e,u,v;
    cin>>n>>e;
    for(i=1;i<=e;i++)
    {
        cin>>u>>v;
        inc[v]++;
        adj[u].push_back(v);
    }
    for(i=1;i<=n;i++)
    {
        if(inc[i]==0)
        {
            q.push(i);
        }
    }
    while(!q.empty())
    {
        u=q.front();
        res.push_back(u);
        q.pop();
        for(i=0;i<adj[u].size();i++)
        {
            inc[adj[u][i]]--;
            if(inc[adj[u][i]]==0)
                q.push(adj[u][i]);
        }
    }
    for(i=0;i<res.size();i++)
        cout<<res[i]<<" ";
    return 0;
}

/*
9 9
1 2
2 3
2 8
1 8
3 6
4 3
4 5
5 6
7 8
*/
