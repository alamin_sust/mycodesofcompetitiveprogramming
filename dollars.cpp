#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<string.h>
#define eps 0.00000001
using namespace std;

long long dp[12][6010],coin[]={1,2,4,10,20,40,100,200,400,1000,2000};

long long rec(long long i,long long amount)
{
    if(i>=11)
    {
        if(amount==0)
        return 1;
        else
        return 0;
    }
    if(dp[i][amount]!=-1)
    return dp[i][amount];
    long long ret1=0,ret2;
    if((amount-coin[i])>=0)
    ret1=rec(i,amount-coin[i]);
    ret2=rec(i+1,amount);
    return dp[i][amount]=ret1+ret2;
}

main()
{
    long long in,res;
    double n;
    memset(dp,-1,sizeof(dp));
    while(scanf("%lf",&n)!=EOF)
          {
              if(n-eps<0.0)
              break;
              n+=eps;
              n*=20;
              in=(long long)n;
              //printf(" %lld\n",in);
              res=rec(0,in);
              printf("%6.2lf%17lld\n",n/20,res);
          }
          return 0;
}
