/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


ll n,m,val[300010],par[300010],from,to,stat[300010];
string s;
vector<pair<ll,ll> >adj[300010];
priority_queue<pair<ll,ll> >pq;
map<pair<ll,ll>,ll>mpp,mpp2;

int comp(pair<ll,ll> a,pair<ll,ll> b)
{
    return a.second<b.second;
}

ll dijkstra(ll src)
{
    for(ll i=1LL; i<=n; i++)
        val[i]=999999999999999LL;
    memset(stat,0LL,sizeof(stat));

    while(!pq.empty())
    {
        pq.pop();
    }

    pq.push(make_pair(src,0LL));
    ll ret=0LL;
    val[src]=0LL;
    par[src]=-1LL;

    while(!pq.empty())
    {
        pair<ll,ll> tp;
        ll u=pq.top().first;
        ret+=pq.top().second;
        pq.pop();
        for(ll i=0LL; i<adj[u].size(); i++)
        {
            ll v=adj[u][i].first;

            if(val[v]>val[u]+adj[u][i].second)
            {
                val[v]=val[u]+adj[u][i].second;
                par[v]=u;
                pq.push(make_pair(v,val[v]));
            }
            else if (val[v] == val[u] + adj[u][i].second && adj[u][i].second < val[v] - val[par[v]])
                par[v] = u;
        }
    }

    return ret;
}

int main()
{
    ll in,i,cost,res;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    cin>>n>>m;

    for(i=1LL; i<=m; i++)
    {
        scanf(" %I64d %I64d %I64d",&from,&to,&cost);
        adj[from].push_back(make_pair(to,cost));
        adj[to].push_back(make_pair(from,cost));
        mpp[make_pair(min(from,to),max(from,to))]=i;
        mpp2[make_pair(min(from,to),max(from,to))]=cost;
    }
    cin>>in;
    dijkstra(in);
    res=0LL;
    vector<ll>vc;
    vc.clear();
    for(i=1LL; i<=n; i++)
    {
        if(in==i)
            continue;
        res+=mpp2[make_pair(min(i,par[i]),max(i,par[i]))];
        vc.push_back(mpp[make_pair(min(i,par[i]),max(i,par[i]))]);
    }
    printf("%I64d\n",res);
    for(i=0; i<vc.size(); i++)
        cout<<vc[i]<<" \n"[i==(vc.size()-1)];
    return 0;
}

