/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int flag[200010],l1,l2,i,res1,res2,mpp[1010];
char arr1[200010],arr2[200010];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    scanf(" %s",arr1);
    scanf(" %s",arr2);

    l1=strlen(arr1);
    l2=strlen(arr2);

    res1=res2=0;

    for(i=0; i<l2; i++)
    {
        mpp[arr2[i]]++;
    }

    for(i=0; i<l1; i++)
    {
        if(mpp[arr1[i]]>0)
        {
            mpp[arr1[i]]--;
            res1++;
            flag[i]=1;
        }
    }

    for(i=0; i<l1; i++)
    {
        if(arr1[i]>='a'&&arr1[i]<='z'&&flag[i]==0)
        {
            if(mpp[toupper(arr1[i])]>0)
            {
            mpp[toupper(arr1[i])]--;
                res2++;
                flag[i]=1;
            }
        }
        else if(arr1[i]>='A'&&arr1[i]<='Z'&&flag[i]==0)
        {
            if(mpp[tolower(arr1[i])]>0)
            {
            mpp[tolower(arr1[i])]--;
                res2++;
                flag[i]=1;
            }

        }
    }
    printf("%d %d\n",res1,res2);
    return 0;
}





