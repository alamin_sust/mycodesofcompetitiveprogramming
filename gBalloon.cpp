/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll x[1010],h[1010],n,m,q,t,p,v[1010];

ll rec(ll mxt)
{
    ll energy=0LL;

    for(ll i=0LL; i<n; i++)
    {
        ll mncost=100000010LL;
        if(x[i]==0LL)
            continue;
        if(x[i]<0LL)
        {
            for(ll j=0LL; j<m; j++)
            {
                if(v[j]<=0LL)
                    continue;
                if((v[j]*mxt)>=(-x[i]))
                {
                    mncost=min(mncost,abs(j-h[i]));
                }
            }
        }
        else
        {
            for(ll j=0LL; j<m; j++)
            {
                if(v[j]>=0LL)
                    continue;
                if(((-v[j])*mxt)>=(x[i]))
                {
                    mncost=min(mncost,abs(j-h[i]));
                }
            }

        }
        energy+=mncost;
     }
     if(energy<=q)
        return 1LL;
     else
        return 0LL;

}

int main()
{
    freopen("B-large222.in","r",stdin);
    freopen("B-large222out.txt","w",stdout);
    ll i,low,high,mid,cnt,res;
    scanf(" %lld",&t);

    for(p=1LL; p<=t; p++)
    {
        scanf(" %lld %lld %lld",&n,&m,&q);
        for(i=0LL; i<m; i++)
        {
            scanf(" %lld",&v[i]);
        }
        for(i=0LL; i<n; i++)
        {
            scanf(" %lld %lld",&x[i],&h[i]);
        }
        low=0LL;
        high=1000000000000010LL;
        cnt=100LL;
        res=high;
        while(low<=high&&cnt--)
        {
            mid=(low+high)/2LL;
            if(rec(mid))
            {
                res=min(res,mid);
                high=mid-1LL;
            }
            else
            {
                low=mid+1LL;
            }
        }

        printf("Case #%lld: ",p);
        if(res>=1000000000000010LL)
            printf("IMPOSSIBLE\n");
        else
            printf("%lld\n",res);
    }

    return 0;
}


