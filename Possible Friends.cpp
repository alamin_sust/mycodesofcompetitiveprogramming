/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int p,t,flag[55],l,i,j,k,mx,fg[55][55];
char arr[55][55];

int main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %s",&arr[0]);
        l=strlen(arr[0]);
        for(i=1;i<l;i++)
        {
            scanf(" %s",&arr[i]);
        }
        memset(flag,0,sizeof(flag));
        memset(fg,0,sizeof(fg));
        for(k=0;k<l;k++)
        {
            for(i=0;i<l;i++)
            {
                for(j=0;j<l;j++)
                {
                    if(i==j||j==k||k==i)
                        continue;
                    if(arr[k][i]=='Y'&&arr[i][j]=='Y'&&arr[k][j]=='N'&&fg[k][j]==0)
                    flag[k]++,fg[k][j]=1;
                }
            }
        }
        mx=0;
        k=0;
        for(i=0;i<l;i++)
        {
            if(flag[i]>mx)
            {
                mx=flag[i];
                k=i;
            }
        }
        printf("%d %d\n",k,mx);
    }
    return 0;
}










