/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

vector<int>adj[1010];
int pp=1,res[1010],col[1010],par[1010],flag[1010][1010];
queue<int>q;
set<int>stt,st[1010];

int bfs(int from,int to)
{
    while(!q.empty())
        q.pop();
    q.push(from);
    memset(col,0,sizeof(col));
    memset(par,0,sizeof(par));
    col[from]=1;
    while(!q.empty())
    {
        int u=q.front();
        q.pop();
        for(int i=0;i<adj[u].size();i++)
        {
            int v=adj[u][i];
            if(col[v])
                continue;
            col[v]=1;
            par[v]=u;
            if(v==to)
                break;
            q.push(v);
        }
    }
    int ret=0;
    //stt.clear();
    queue<int>qq,qq2;
    while(!qq.empty())
    qq.pop();
    for(int i=1;i<pp;i++)
        qq.push(i);
    while(!qq2.empty())
    qq2.pop();
    while(1)
    {
        while(!qq2.empty())
        qq2.pop();
        //ret=max(ret,res[to]);
        //res[to]++;

        //stt.insert(st[to].begin(),st[to].end());
        //st[to].insert(pp);
        while(!qq.empty())
        {
            int val=qq.front();
            qq.pop();
            if(flag[to][val]==0)
            {
                qq2.push(val);
            }
            else
                ret++;

        }
        qq=qq2;
        flag[to][pp]=1;
        if(to==from)
            break;
        to=par[to];
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);


    int n,m,i,u,v;

    scanf(" %d",&n);

    for(i=1;i<n;i++)
    {
        scanf(" %d %d",&u,&v);
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    scanf(" %d",&m);

    for(i=1;i<=m;i++)
    {
        scanf(" %d %d",&u,&v);
        printf("%d\n",bfs(u,v));
        pp++;
    }

    return 0;
}
