/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll p,t,num,i,j,in;
map<ll,ll>mpp;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);



    for(i=5000002LL;i>=2LL;i--)
    {
        for(j=i-1LL;j>=1LL;j--)
        {
            ll num=i*i-j*j;
            ll tp=sqrt(num);

            if((tp*tp)==num)
            {
                mpp[i]=1LL;
                //if(tp==3)
                //printf("%lld %lld %lld\n",i,j,tp);
            }

            if(tp>5000002LL)
                break;
        }
    }

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++)
    {
        scanf(" %lld",&in);
        if(mpp[in])
        printf("YES\n");
        else
        printf("NO\n");
    }

    return 0;
}


