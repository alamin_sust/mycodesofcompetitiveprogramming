#include<stdio.h>

int  st, mx,  fst, fen;

int main()
{
    int cas, c=1, num, sum, a, b, tot, i;
    scanf("%d", &cas);
    while(cas--)
    {
        scanf("%d", &tot);
        scanf("%d", &num);
        sum=num;
        mx=num;
        fst=st=1;
        fen=1;
        for(i=2;i<=tot;i++)
        {
            scanf("%d", &num);
            b=sum+num;
            a=num;
            if(a>b)
            {
                sum=a;
                st=i;
            }
            else
                sum=b;
            if(sum>mx)
            {
                mx=sum;
                fst=st;
                fen=i;
            }
        }
        printf("Case %d:\n%d %d %d\n", c++, mx, fst, fen);
        if(cas)
            printf("\n");
    }
    return 0;
}
