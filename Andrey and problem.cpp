#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
double res,m,arr[110];
int n,i,j,k,l;
main()
{
    cin>>n;
    for(i=1;i<=n;i++)
    {
        scanf(" %lf",&arr[i]);
        res=max(res,arr[i]);
    }
    for(i=1;i<=n;i++)
    {
        for(j=i+1;j<=n;j++)
        {
            m=arr[i]*(1.0-arr[j]);
            m+=arr[j]*(1.0-arr[i]);
            res=max(res,m);
        }
    }
    for(i=1;i<=n;i++)
    {
        for(j=i+1;j<=n;j++)
        {
            for(k=j+1;k<=n;k++)
            {
            m=arr[i]*(1.0-arr[j])*(1.0-arr[k]);
            m+=arr[j]*(1.0-arr[i])*(1.0-arr[k]);
            m+=arr[k]*(1.0-arr[j])*(1.0-arr[i]);
            res=max(res,m);
            }
        }
    }
    for(i=1;i<=n;i++)
    {
        for(j=i+1;j<=n;j++)
        {
            for(k=j+1;k<=n;k++)
            {
            for(l=k+1;l<=n;l++)
            {
            m=arr[i]*(1.0-arr[j])*(1.0-arr[k])*(1.0-arr[l]);
            m+=arr[j]*(1.0-arr[i])*(1.0-arr[k])*(1.0-arr[l]);
            m+=arr[k]*(1.0-arr[j])*(1.0-arr[i])*(1.0-arr[l]);
            m+=arr[l]*(1.0-arr[j])*(1.0-arr[i])*(1.0-arr[k]);
            res=max(res,m);
            }
            }
        }
    }
    printf("%.10lf\n",res);
    return 0;
}

