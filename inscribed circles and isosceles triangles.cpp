#include <iostream>
#include <fstream>
#include <cmath>
#include <iomanip>
#define pi 3.1415926
//#define LOCAL
#define fin cin
#define fout cout
using namespace std;
double circumference(double b,double h)
{
double r,l,sum=0;
r=h*b/(b+sqrt(4*h*h+b*b));
if(r>=0.000001)
{
sum=sum+2*pi*r;
b=b-2*r*b/h;
h=h-2*r;
return sum+circumference(b,h);
}
else return 0;
}
int main()
{
#ifdef LOCAL
ifstream fin("in.cpp");
ofstream fout("out.cpp");
#endif
int t;
fin>>t;
double h,b;
while(t--)
{
fin>>b>>h;
fout<<setiosflags(ios::fixed)<<setprecision(6)<<setw(13)<<circumference(b,h)<<endl;
if(t!=0) fout<<endl;
}
return 0;
