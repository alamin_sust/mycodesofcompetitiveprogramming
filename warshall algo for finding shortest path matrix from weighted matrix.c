#include<stdio.h>
#define max 20
#define infinity 999999
main()
{
    int pathTemp,from,to,n,i,j,k,vertices=0,weight,path[max][max],adj[max][max];
    printf("vertices: ");
    scanf(" %d",&vertices);
    printf("edges: ");
    scanf(" %d",&n);
    for(i=1;i<=vertices;i++)
    {
        for(j=1;j<=vertices;j++)
        adj[i][j]=0;
    }
    for(i=1;i<=n;i++)
    {
        scanf(" %d %d %d",&from,&to,&weight);
        adj[from][to]=weight;
    }
    //weighted matrix
    for(i=1;i<=vertices;i++)
    {
        for(j=1;j<=vertices;j++)
        printf(" %3d",adj[i][j]);
        printf("\n");
    }
    printf("\n\n");

    for(i=1;i<=vertices;i++)
    {
        for(j=1;j<=vertices;j++)
        {
            path[i][j]=(adj[i][j]?adj[i][j]:infinity);
        }
    }

    for(k=1;k<=vertices;k++)
    {
     for(i=1;i<=vertices;i++)
       {
        for(j=1;j<=vertices;j++)
        {
            pathTemp=(path[i][j]<(path[i][k]+path[k][j]));
            path[i][j] = pathTemp ? path[i][j] : (path[i][k]+path[k][j]);
        }
        }
    }

    //print shortest path matrix

    for(i=1;i<=vertices;i++)
    {
        for(j=1;j<=vertices;j++)
        {if(path[i][j]==infinity)
        path[i][j]=0;
        printf(" %3d",path[i][j]);}
        printf("\n");
    }
    printf("\n");
    return 0;
}
