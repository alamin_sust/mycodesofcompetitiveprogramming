#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
#define ull long long unsigned
using namespace std;

ull res[4][4],mat[50][4][4];

void calc(ull power)
{
    ull temp[4][4];
    for(ull i=1;i<=2;i++)
    {
        for(ull j=1;j<=2;j++)
        {
             temp[i][j]=res[i][j];
        }
    }
    for(ull i=1;i<=2;i++)
    {
        for(ull j=1;j<=2;j++)
        {
            res[i][j]=0;
            for(ull k=1;k<=2;k++)
            {
                res[i][j]+=(temp[i][k]*mat[power][k][j])%100000;

            }
            res[i][j]%=100000;
        }
    }
    return;
}

void bigmod(ull power)
{
    for(ull i=1;i<=2;i++)
    {
        for(ull j=1;j<=2;j++)
        {
            for(ull k=1;k<=2;k++)
            {
                mat[power][i][j]+=(mat[power-1][i][k]*mat[power-1][k][j])%100000;
                //mat[power][i][j]%=100000;
            }
            mat[power][i][j]%=100000;
        }
    }
    return;
}

main()
{
    ull t,p,a,b,n,m;
    mat[0][1][1]=mat[0][1][2]=1;
    mat[0][2][1]=1;
    mat[0][2][2]=0;
    cin>>t;
    for(ull i=1;i<=32;i++)
    bigmod(i);
    //for(ull i=1;i<=2;i++)
      //  for(ull j=1;j<=2;j++)
        //printf("%llu ",mat[10][i][j]);
    for(p=1;p<=t;p++)
    {
        cin>>a>>b>>n>>m;
        if(n==0)
        {
            printf("Case %llu: %llu\n",p,a);
            continue;
        }
        if(n==1)
        {
            printf("Case %llu: %llu\n",p,b);
            continue;
        }
        ull MOD=1,j=10;
        for(ull i=1;i<=m;i++)
            MOD*=j;
        n--;
        //printf("%llu\n",MOD);
        ull k=0;
        ull fg=0;
        while((n>>k)|0)
        {
            //printf(".");
            if(((n>>k)&1)==1)
                {
                    if(fg==0)
                    {
                        fg=1;
                        for(ull i=1;i<=2;i++)
                            for(ull j=1;j<=2;j++)
                                res[i][j]=mat[k][i][j];
                    }
                    else
                    calc(k);
                }
            k++;
        }
       ull result=(((res[1][1]*b))+((res[1][2]*a)))%MOD;
        printf("Case %llu: %llu\n",p,result);
    }
    return 0;
}
