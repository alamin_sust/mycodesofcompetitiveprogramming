/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

map<pair<int,int> , int>mpp;

int t,p,n,arr[510],cum[510];
char ch;

int rec(int pos,int x,int y,int dik)
{
    if(pos>n)
        return abs(x)+abs(y);
    if((cum[pos]+501)<(abs(x)+abs(y)))
        return 1000000007;
    if((abs(x)+abs(y))>1002)
        return 1000000007;
    int ret=1000000007;
    if(dik==0)
    ret = min(rec(pos+1,x,y+arr[pos],1),rec(pos+1,x,y-arr[pos],3));
    if(dik==1)
    ret = min(rec(pos+1,x+arr[pos],y,2),rec(pos+1,x-arr[pos],y,0));
    if(dik==2)
    ret = min(rec(pos+1,x,y-arr[pos],3),rec(pos+1,x,y+arr[pos],1));
    if(dik==3)
    ret = min(rec(pos+1,x-arr[pos],y,0),rec(pos+1,x+arr[pos],y,2));
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);


    scanf("%d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        scanf(" %d",&arr[0]);
        getchar();
        for(int i=1;i<=n;i++)
        {
            scanf(" %c",&ch);
            scanf(" %d",&arr[i]);
            getchar();
        }
        if(n<3)
        {
            printf("NO\n");
            continue;
        }
        cum[n+1]=0;
        for(int i=n;i>=0;i--)
        {
            cum[i]=cum[i+1]+arr[i];
        }
        //memset(dp,-1,sizeof(dp));
        printf("%d\n",rec(1,-arr[0],0,0));
    }
    return 0;
}






