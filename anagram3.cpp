#include<stdio.h>
#include<string.h>
#include<iostream>
using namespace std;

char arr[1010];

void swap(int x, int y){
    int temp = arr[x];
    arr[x]=arr[y];
    arr[y]=temp;

    return;
}

void printArray(int size){
    int i;

    for (i=0;i<size;i++)
        cout << arr[i];

    cout << endl;

    return;
}

void permute(int k,int size){
    int i;

    if (k==0)
        printArray(size);
    else{
        for (i=k-1;i>=0;i--){
            swap(i,k-1);
            permute(k-1,size);
            swap(i,k-1);
        }
    }

    return;
}

int main(){
     int i,j,k,n,l;
     char temp;
    scanf(" %d",&n);
    getchar();
    for(i=0;i<n;i++)
    {
     gets(arr);
     l=strlen(arr);
     for(j=0;j<l;j++)
     {
         for(k=1;k<l;k++)
         {
             if(arr[k]>arr[k-1])
             {
                 temp=arr[k];
                 arr[k]=arr[k-1];
                 arr[k-1]=temp;
             }
         }
     }
     permute(l,l);
    }
    return 0;
}
