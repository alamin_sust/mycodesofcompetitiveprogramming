/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int dp[8][10][2][8][2],num;
char arr[10];

int rec(int pos,int dig,int flag,int cnt,int is_started)
{
    if(pos==-1)
    {
        //printf("%d..\n",cnt);
        return cnt;
    }
    int &ret=dp[pos][dig][flag][cnt][is_started];
    if(ret!=-1)
        return ret;
    ret=0;
    if(flag==1)
    {
        for(int i=0; i<=9; i++)
            ret+=rec(pos-1,i,flag,(is_started|(i==0?0:1))==1?(i==num?cnt+1:cnt):0,is_started|(i==0?0:1));
    }
    else
    {
        ret+=rec(pos-1,arr[pos]-'0',flag,(is_started|(arr[pos]-'0'==0?0:1))==1?(arr[pos]-'0'==num?cnt+1:cnt):0,is_started|(arr[pos]-'0'==0?0:1));
        for(int i=0; i<arr[pos]-'0'; i++)
            ret+=rec(pos-1,i,1,is_started|(i==0?0:1)==1?(i==num?cnt+1:cnt):0,is_started|(i==0?0:1));
    }
    return ret;
}

main()
{
    int i,j,res,p,tp_from,tp_to,from,to;
    while(scanf("%d%d",&from,&to)!=EOF)
    {
        if(from==0&&to==0)
            break;
        if(from>to)
            swap(from,to);
        for(p=0; p<=9; p++)
        {
            num=p;
            tp_from=from-1;
            tp_to=to;
            memset(dp,-1,sizeof(dp));
            for(i=0; i<10; i++)
                arr[i]='0';
            for(i=0;tp_from>0;i++)
                arr[i]=(tp_from%10)+'0',tp_from/=10;
            //printf("..%s\n",arr);
            res=rec(7,0,0,0,0);
            //printf("%d\n",res);
            for(i=0; i<10; i++)
                arr[i]='0';
            for(i=0;tp_to>0;i++)
                arr[i]=(tp_to%10)+'0',tp_to/=10;
            memset(dp,-1,sizeof(dp));
            printf("%d",rec(7,0,0,0,0)-res);
            if(p==9)
                printf("\n");
            else
                printf(" ");
        }
    }
    return 0;
}

