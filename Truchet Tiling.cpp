/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int rr[]= {1,1,-1,-1};
int cc[]= {-1,1,1,-1};
int stat[210][210],row,col;
queue<pair<int,int> >q;
char arr[210][210];

double bfs(int x,int y)
{
    memset(stat,0,sizeof(stat));
    stat[x][y]=1;
    while(!q.empty())
        q.pop();
    q.push(make_pair(x,y));
    double ret=0.0;
    while(!q.empty())
    {
        int ux=q.front().first;
        int uy=q.front().second;
        //printf("%d %d..\n",ux,uy);
        q.pop();
        if((ux%2)==1&&(uy%2)==1)
        {
            ret+=4.0-pi/2;
            if(arr[ux/2][uy/2]=='0')
            {
                int vx=ux-1;
                int vy=uy+1;
                if(vx>=0&&vx<=(row*2)&&vy>=0&&vy<=(col*2)&&stat[vx][vy]==0)
                {
                    q.push(make_pair(vx,vy));
                    stat[vx][vy]=1;
                }
                vx=ux+1;
                vy=uy-1;
                if(vx>=0&&vx<=(row*2)&&vy>=0&&vy<=(col*2)&&stat[vx][vy]==0)
                {
                    q.push(make_pair(vx,vy));
                    stat[vx][vy]=1;
                }
            }
            else
            {
                int vx=ux+1;
                int vy=uy+1;
                if(vx>=0&&vx<=(row*2)&&vy>=0&&vy<=(col*2)&&stat[vx][vy]==0)
                {
                    q.push(make_pair(vx,vy));
                    stat[vx][vy]=1;
                }
                vx=ux-1;
                vy=uy-1;
                if(vx>=0&&vx<=(row*2)&&vy>=0&&vy<=(col*2)&&stat[vx][vy]==0)
                {
                    q.push(make_pair(vx,vy));
                    stat[vx][vy]=1;
                }
            }
        }
        else
        {
            int vx,vy;
            int tx=ux/2;
            int ty=uy/2;
            vx=ux+1;
            vy=uy+1;
            if(tx<row&&tx>=0&&ty<col&&ty>=0&&arr[tx][ty]=='1')
            {
                if(vx>=0&&vx<=(row*2)&&vy>=0&&vy<=(col*2)&&stat[vx][vy]==0)
                {
                    q.push(make_pair(vx,vy));
                    stat[vx][vy]=1;
                }
            }
            else if(vx>=0&&vx<=(row*2)&&vy>=0&&vy<=(col*2))
                ret+=pi/4.0;
            ty--;
            vx=ux+1;
                vy=uy-1;
            if(tx<row&&tx>=0&&ty<col&&ty>=0&&arr[tx][ty]=='0')
            {

                if(vx>=0&&vx<=(row*2)&&vy>=0&&vy<=(col*2)&&stat[vx][vy]==0)
                {
                    q.push(make_pair(vx,vy));
                    stat[vx][vy]=1;
                }
            }
            else if(vx>=0&&vx<=(row*2)&&vy>=0&&vy<=(col*2))
                ret+=pi/4.0;
            ty++;
            tx--;
              vx=ux-1;
                vy=uy+1;
            if(tx<row&&tx>=0&&ty<col&&ty>=0&&arr[tx][ty]=='0')
            {

                if(vx>=0&&vx<=(row*2)&&vy>=0&&vy<=(col*2)&&stat[vx][vy]==0)
                {
                    q.push(make_pair(vx,vy));
                    stat[vx][vy]=1;
                }
            }
            else if(vx>=0&&vx<=(row*2)&&vy>=0&&vy<=(col*2))
                ret+=pi/4.0;
            ty--;
            vx=ux-1;
                vy=uy-1;
            if(tx<row&&tx>=0&&ty<col&&ty>=0&&arr[tx][ty]=='1')
            {

                if(vx>=0&&vx<=(row*2)&&vy>=0&&vy<=(col*2)&&stat[vx][vy]==0)
                {
                    q.push(make_pair(vx,vy));
                    stat[vx][vy]=1;
                }
            }
            else if(vx>=0&&vx<=(row*2)&&vy>=0&&vy<=(col*2))
                ret+=pi/4.0;


        }
    }

    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    int t,p,i,j,q,x,y;
    cin>>t;

    for(p=1; p<=t; p++)
    {
        scanf(" %d %d",&row,&col);
        getchar();
        for(i=0; i<row; i++)
        {
            for(j=0; j<col; j++)
            {
                scanf("%c",&arr[i][j]);
            }
            getchar();
        }
        cin>>q;
        printf("Case %d:\n",p);
        for(i=1; i<=q; i++)
        {
            scanf(" %d %d",&x,&y);
            if((x%2)==(y%2))
                printf("%.4lf\n",bfs(x,y));
            else
                printf("0.0000\n");
        }
    }


    return 0;
}


