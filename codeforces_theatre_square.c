#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

main()
{
    long long int m,n,a1,a2,res1,res2;
    scanf(" %I64d %I64d %I64d",&a1,&a2,&n);
    if(a1<=n)
    res1=1;
    else
    {
        m=a1%n;
        res1=(a1-m)/n;
        if(m!=0)
        res1++;
    }
    if(a2<=n)
    res2=1;
    else
    {
        m=a2%n;
        res2=(a2-m)/n;
        if(m!=0)
        res2++;
    }
    printf("%I64d\n",res1*res2);
    return 0;
}
