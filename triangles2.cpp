/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807LL
#define long_min -9223372036854775808LL
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node
{
    double col;
    int x,y,z;
};

node tri[1010];
int dp[1010][5][1010],n;

int comp(node a,node b)
{
    return a.col<b.col;
}

int rec(int pos,int side,int taken)
{
    if(pos>=n)
        return taken;
    int &ret=dp[pos][side][taken];
    if(ret!=-1)
        return ret;
    ret=0;
    /*if(prev==0)
        ret=max(rec(pos+1,pos,0,taken+1),rec(pos+1,0,0,taken));
    else
    {
            if((tri[prev].col+eps)<tri[pos].col&&side!=1&&tri[prev].x==tri[pos].x)
                ret=max(ret,max(rec(pos+1,prev,side,taken),rec(pos+1,pos,1,taken+1)));
            else ret=max(ret,rec(pos+1,prev,side,taken));
            if((tri[prev].col+eps)<tri[pos].col&&side!=1&&tri[prev].x==tri[pos].y)
                ret=max(ret,max(rec(pos+1,prev,side,taken),rec(pos+1,pos,2,taken+1)));
            else ret=max(ret,rec(pos+1,prev,side,taken));
            if((tri[prev].col+eps)<tri[pos].col&&side!=1&&tri[prev].x==tri[pos].z)
                ret=max(ret,max(rec(pos+1,prev,side,taken),rec(pos+1,pos,3,taken+1)));
            else ret=max(ret,rec(pos+1,prev,side,taken));
            if((tri[prev].col+eps)<tri[pos].col&&side!=2&&tri[prev].y==tri[pos].x)
                ret=max(ret,max(rec(pos+1,prev,side,taken),rec(pos+1,pos,1,taken+1)));
            else ret=max(ret,rec(pos+1,prev,side,taken));
            if((tri[prev].col+eps)<tri[pos].col&&side!=2&&tri[prev].y==tri[pos].y)
                ret=max(ret,max(rec(pos+1,prev,side,taken),rec(pos+1,pos,2,taken+1)));
            else ret=max(ret,rec(pos+1,prev,side,taken));
            if((tri[prev].col+eps)<tri[pos].col&&side!=2&&tri[prev].y==tri[pos].z)
                ret=max(ret,max(rec(pos+1,prev,side,taken),rec(pos+1,pos,3,taken+1)));
            else ret=max(ret,rec(pos+1,prev,side,taken));
            if((tri[prev].col+eps)<tri[pos].col&&side!=3&&tri[prev].z==tri[pos].x)
                ret=max(ret,max(rec(pos+1,prev,side,taken),rec(pos+1,pos,1,taken+1)));
            else ret=max(ret,rec(pos+1,prev,side,taken));
            if((tri[prev].col+eps)<tri[pos].col&&side!=3&&tri[prev].z==tri[pos].y)
                ret=max(ret,max(rec(pos+1,prev,side,taken),rec(pos+1,pos,2,taken+1)));
            else ret=max(ret,rec(pos+1,prev,side,taken));
            if((tri[prev].col+eps)<tri[pos].col&&side!=3&&tri[prev].z==tri[pos].z)
                ret=max(ret,max(rec(pos+1,prev,side,taken),rec(pos+1,pos,3,taken+1)));
            else
            ret=max(ret,rec(pos+1,prev,side,taken));
    }*/
    int ps=pos;
    while(ps<=n&&(tri[ps].col-eps)<tri[pos].col)
        ps++;
    for(int i=ps;i<=n;i++)
    {
        if(pos==0)
        ret=max(ret,rec(i,0,taken+1));
        else
        {
            if(side!=1&&tri[pos].x==tri[i].x)
                ret=max(ret,rec(i,1,taken+1));
            if(side!=1&&tri[pos].x==tri[i].y)
                ret=max(ret,rec(i,2,taken+1));
            if(side!=1&&tri[pos].x==tri[i].z)
                ret=max(ret,rec(i,3,taken+1));
            if(side!=2&&tri[pos].y==tri[i].x)
                ret=max(ret,rec(i,1,taken+1));
            if(side!=2&&tri[pos].y==tri[i].y)
                ret=max(ret,rec(i,2,taken+1));
            if(side!=2&&tri[pos].y==tri[i].z)
                ret=max(ret,rec(i,3,taken+1));
            if(side!=3&&tri[pos].z==tri[i].x)
                ret=max(ret,rec(i,1,taken+1));
            if(side!=3&&tri[pos].z==tri[i].y)
                ret=max(ret,rec(i,2,taken+1));
            if(side!=3&&tri[pos].z==tri[i].z)
                ret=max(ret,rec(i,3,taken+1));
        }
    }
    ret=max(ret,rec(n+1,side,taken));
    return ret;
}


int main()
{

    while(cin>>n)
    {for(int i=1; i<=n; i++)
    {
        scanf(" %d %d %d %lf",&tri[i].x,&tri[i].y,&tri[i].z,&tri[i].col);
        if(max(tri[i].x,max(tri[i].y,tri[i].z))*2>=(tri[i].x+tri[i].y+tri[i].z))
        {
            i--;
            n--;
           // printf(".");
        }
    }
    sort(tri+1,tri+n+1,comp);
    tri[0].x=0;
    tri[0].z=0;
    tri[0].y=0;
    tri[0].col=-1.0;
    memset(dp,-1,sizeof(dp));
    printf("%d\n",rec(0,0,0));
    }
    return 0;
}


