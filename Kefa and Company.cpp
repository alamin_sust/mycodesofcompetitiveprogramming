/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

struct node
{
    ll price,f;
};

ll i,n,d,l,r,res,fact[100010];
node arr[100010];

int comp(node a,node b)
{
    return a.price<b.price;
}

int main()
{
    scanf(" %I64d %I64d",&n,&d);

    for(i=1LL; i<=n; i++)
    {
        scanf(" %I64d %I64d",&arr[i].price,&arr[i].f);
    }

    sort(arr+1LL,arr+n+1LL,comp);

    fact[1]=arr[1].f;
    for(i=2LL;i<=n;i++)
    {
        fact[i]+=fact[i-1]+arr[i].f;
    }

    l=1LL;
    r=1LL;
    res=0LL;
    while(r<=n)
    {
        if((arr[r].price-arr[l].price)<d)
        {
            res=max(res,fact[r]-fact[l-1]);
            r++;
        }
        else
        {
            l++;
        }
    }
    printf("%I64d\n",res);
    return 0;
}







