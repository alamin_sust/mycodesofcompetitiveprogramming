#include <sstream>
#include<string.h>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class HappyLetterDiv2
{
public:
	char getHappyLetter(string letters)
	{
	    int i,l=letters.size(),arr[300],k;
	    memset(arr,0,sizeof(arr));
	    k=l/2+1;
	    for(i=0;i<l;i++)
        {
            arr[letters[i]]++;
            if(arr[letters[i]]==k)
                return letters[i];
        }
        return '.';
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	HappyLetterDiv2 objectHappyLetterDiv2;

	//test case0
	string param00 = "aacaaa";
	char ret0 = objectHappyLetterDiv2.getHappyLetter(param00);
	char need0 = 'a';
	assert_eq(0,ret0,need0);

	//test case1
	string param10 = "dcdjx";
	char ret1 = objectHappyLetterDiv2.getHappyLetter(param10);
	char need1 = '.';
	assert_eq(1,ret1,need1);

	//test case2
	string param20 = "bcbbbbba";
	char ret2 = objectHappyLetterDiv2.getHappyLetter(param20);
	char need2 = 'b';
	assert_eq(2,ret2,need2);

	//test case3
	string param30 = "aabc";
	char ret3 = objectHappyLetterDiv2.getHappyLetter(param30);
	char need3 = '.';
	assert_eq(3,ret3,need3);
    return 0;
}
