#include<stdio.h>
#include<math.h>
main()
{
    int n,p,k=0;
    double A,B,C,sm_area,x,y,z,a,b,c,ratio,up,ext,rat,down,mid,s,area,trap,res,h;
    scanf(" %d",&n);
    for(p=1;p<=n;p++)
    {
        k=0;
        scanf(" %lf %lf %lf %lf",&c,&b,&a,&ratio);
        B=acos((c*c+a*a-b*b)/(2.0*c*a));
        C=acos((b*b+a*a-c*c)/(2.0*b*a));
        ext=tan(C)*tan(B)/(tan(B)+tan(C));
        up=a;
        down=0.0;
        mid=a/2.0;
        s=(a+b+c)/2.0;
        area=sqrt(s*(s-a)*(s-b)*(s-c));
        while(k<=100)
        {
            h=(a-mid)*ext;
            trap=0.5*(mid+a)*h;
            rat=(area-trap)/trap;
            if((rat+0.0000001)>ratio&&(rat-0.0000001)<ratio)
            {
                break;
            }
            else if(rat>ratio)
            {
                up=mid-0.0000001;
            }
            else if(rat<ratio)
            {
                down=mid+0.0000001;
            }
            mid=(up+down)/2.0;
            k++;
        }
        res=mid/a*c;
        printf("Case %d: %lf\n",p,res);
    }
    return 0;
}
