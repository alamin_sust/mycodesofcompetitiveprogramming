/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

double root2=sqrt(2.0);

double getres(int permute)
{
    int now;
    double ret=0.0;
    for(int i=0;permute;i++)
    {
        now=permute%10;
        permute/=10;
        if(i==0)
        {
            ret=h[i];
            corner_height[i]=h[i]/(2+root2);
            mxlen=h[i];
        }
        else
        {
            corner_height[i]=h[i]/(2+root2);
            k=0.0;
            if((h[i]-corner_height[i])<corner_height[i-1])
            {

            }
        }

    }
}


double rec(int mask,int permute)
{
     if((mask+1)==(1<<n))
     {
         return getres(permute);
     }
     double ret=1000000000000010.0;
     for(int i=0;i<n;i++)
     {
         if(((1<<i)&mask)==0)
         {
             ret=min(ret,rec(mask|(1<<i),permute*10+i))
         }
     }
     return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        mxh=0.0;
        mxw=8.0;
        for(i=0;i<n;i++)
        {
            scanf(" %lf",&h[i]);
            mxh=max(mxh,h[i]);
        }
        printf("Case %d: %.10lf\n",p,mxh*mxw*rec(0,""));
    }

    return 0;
}


