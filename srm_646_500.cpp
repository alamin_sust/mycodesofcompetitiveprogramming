#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <string.h>
using namespace std;

int rr[]={0,0,1,-1};
int cc[]={-1,1,0,0};

int stat[2010][2010],arr[2010][2010];

struct node{
int val,x,y;
};

queue<node>q;
node det,u;

int bfs(int k)
{
    memset(stat,0,sizeof(stat));
    while(!q.empty())
        q.pop();
    stat[1000][1000]=1;
    det.x=det.y=1000;
    det.val=0;
    int ret=0;
    q.push(det);
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        if(u.val>=k)
            continue;
        det.val=u.val+1;
        for(int i=0;i<4;i++)
        {
            if(stat[u.x+rr[i]][u.y+cc[i]]==0&&arr[u.x+rr[i]][u.y+cc[i]]==0)
            {
                stat[u.x+rr[i]][u.y+cc[i]]=1;
                det.x=u.x+rr[i];
                det.y=u.y+cc[i];

                ret=max(ret,det.x-1000);
                q.push(det);
            }
        }
    }
    return ret;
}

class TheGridDivTwo
{
public:
	int find(vector <int> x, vector <int> y, int k)
	{
	    memset(arr,0,sizeof(arr));
	    for(int i=0;i<x.size();i++)
        {
            arr[x[i]+1000][y[i]+1000]=1;
        }
        return bfs(k);
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	TheGridDivTwo objectTheGridDivTwo;

	//test case0
	vector <int> param00;
	param00.push_back(1);
	param00.push_back(1);
	param00.push_back(1);
	param00.push_back(1);
	vector <int> param01;
	param01.push_back(-2);
	param01.push_back(-1);
	param01.push_back(0);
	param01.push_back(1);
	int param02 = 4;
	int ret0 = objectTheGridDivTwo.find(param00,param01,param02);
	int need0 = 2;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(-1);
	param10.push_back(0);
	param10.push_back(0);
	param10.push_back(1);
	vector <int> param11;
	param11.push_back(0);
	param11.push_back(-1);
	param11.push_back(1);
	param11.push_back(0);
	int param12 = 9;
	int ret1 = objectTheGridDivTwo.find(param10,param11,param12);
	int need1 = 0;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	vector <int> param21;
	int param22 = 1000;
	int ret2 = objectTheGridDivTwo.find(param20,param21,param22);
	int need2 = 1000;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(1);
	param30.push_back(0);
	param30.push_back(0);
	param30.push_back(-1);
	param30.push_back(-1);
	param30.push_back(-2);
	param30.push_back(-2);
	param30.push_back(-3);
	param30.push_back(-3);
	param30.push_back(-4);
	param30.push_back(-4);
	vector <int> param31;
	param31.push_back(0);
	param31.push_back(-1);
	param31.push_back(1);
	param31.push_back(-2);
	param31.push_back(2);
	param31.push_back(-3);
	param31.push_back(3);
	param31.push_back(-4);
	param31.push_back(4);
	param31.push_back(-5);
	param31.push_back(5);
	int param32 = 47;
	int ret3 = objectTheGridDivTwo.find(param30,param31,param32);
	int need3 = 31;
	assert_eq(3,ret3,need3);

}
