#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

struct node
{
    int x;
    int value;
};
node det;
priority_queue<node>pq;
bool operator<(node a,node b)
{
    return a.value>b.value;
}


int d[510],stat[510],n,par[510],adj[510][510];

void dijkstra(int from)
{
    det.x=from;
    det.value=0;
    d[from]=0;
    pq.push(det);
    par[from]=from;
    while(!pq.empty())
    {
        int xx=pq.top().x;
        int vv=pq.top().value;
        pq.pop();
        stat[xx]=1;
        for(int i=1; i<=n; i++)
        {
            if(adj[xx][i]!=inf)
            {
                if(stat[i]==0)
                {
                    if(d[i]>(d[xx]+adj[xx][i]))
                    {
                        d[i]=d[xx]+adj[xx][i];
                        det.x=i;
                        det.value=d[i];
                        pq.push(det);
                        par[i]=xx;
                    }
                }
            }
        }
    }
    return;
}

main()
{
    int t,p=1,j,m,from,to,cost,i;
    while(1)
    {
        cin>>n;
        if(n==0)
            break;
        for(i=1; i<=n; i++)
        {
            for(j=1; j<=n; j++)
                adj[i][j]=inf;
            d[i]=inf;
            stat[i]=0;
            par[i]=0;
        }
        for(i=1; i<=n; i++)
        {
            cin>>m;
            for(j=1; j<=m; j++)
            {
                scanf(" %d %d",&to,&cost);
                adj[to][i]=cost;
            }
        }
        cin>>from>>to;
        dijkstra(to);
        printf("Case %d: Path =",p++);
        int temp;
        temp=from;
        while(1)
        {
            printf(" %d",temp);
            if(par[temp]==temp)
                break;
            temp=par[temp];
        }
        printf("; %d second delay\n",d[from]);
    }
    return 0;
}

