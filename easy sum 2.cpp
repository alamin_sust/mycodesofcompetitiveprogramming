#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll M=33554431LL,i,pw,pow_two[210],arr[210],k,t,p,n,res;
priority_queue<int>pq;

main()
{
    pow_two[0]=2;
    for(k=1,i=1; i<=32; i++,k<<=1)
    {
        pow_two[i]=(pow_two[i-1]*pow_two[i-1])%M;
    }
    arr[0]=2;
    for(i=1;i<=32;i++)
    {
        arr[i]=(arr[i-1]+((arr[i-1]*pow_two[i-1])%M))%M;
    }
    scanf(" %lld",&t);
    for(p=1; p<=t; p++)
    {
        scanf(" %lld",&n);
        for(i=0;n!=0;i++,n>>=1)
        {
            if(n&1)
                pq.push(i);
        }
        res=1LL;
        for(pw=1;!pq.empty();)
        {
             res=(res+(pw*arr[pq.top()]))%M;
             pw=(pw*pow_two[pq.top()])%M;
             pq.pop();
        }
        printf("Case %lld: %lld\n",p,res);
    }
    return 0;
}
