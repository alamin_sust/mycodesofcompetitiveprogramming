/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int dp[110][5],arr[110],n;

int rec(int pos,int prev)
{
    if(pos>=n)
        return 0;
    int &ret=dp[pos][prev];
    if(ret!=-1)
        return ret;
    ret=100000010;
    ret=min(ret,1+rec(pos+1,0));
    if(prev!=1&&(arr[pos]==1||arr[pos]==3))
    {
        ret=min(ret,rec(pos+1,1));
    }
    if(prev!=2&&(arr[pos]==2||arr[pos]==3))
    {
        ret=min(ret,rec(pos+1,2));
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    int i;

    scanf(" %d",&n);

    for(i=0;i<n;i++)
    {
        scanf(" %d",&arr[i]);
    }

    memset(dp,-1,sizeof(dp));

    printf("%d\n",rec(0,0));

    return 0;
}






