#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

int que[30],status[30],adj[30][30],par[1010],res[1010];

void bfs(int from,int to)
{
    int front=1,rear=1,process;
    que[front]=from;
    par[from]=from;
    while(front<=rear)
    {
        process=que[front++];
        status[process]=1;
        for(int i=0;i<26;i++)
        {
            if(adj[process][i]==1&&status[i]==0)
            {
                //printf("%c",i+'A');
                status[i]=1;
                que[++rear]=i;
                par[i]=process;
                if(to==i)
                    return;
            }
        }
    }
    return;
}

main()
{
    int j,k,t,i,u,p,m,q;
    char in1[110],in2[110];
    cin>>t;
    for(p=1;p<=t;p++)
    {
        memset(adj,0,sizeof(adj));
        if(p!=1)
            cout<<endl;
        cin>>m>>q;
        for(i=1;i<=m;i++)
        {scanf(" %s %s",in1,in2);
        adj[in1[0]-'A'][in2[0]-'A']=1;
        adj[in2[0]-'A'][in1[0]-'A']=1;}
        for(i=1;i<=q;i++)
        {
            memset(status,0,sizeof(status));
            //memset(par,0,sizeof(par));
            scanf(" %s %s",in1,in2);
            printf("%c",in1[0]);
            bfs(in1[0]-'A',in2[0]-'A');
            u=in2[0]-'A';
            k=0;
            while(par[u]!=u)
            {
                res[k++]=u;
                u=par[u];
            }
            for(j=k-1;j>=0;j--)
            {
                printf("%c",res[j]+'A');
            }
            cout<<endl;
        }
    }
    return 0;
}

