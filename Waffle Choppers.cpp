/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

char arr[110][110];
int t,p,r,c,v1,v2,flag;

bool func(int cut1,int cut2) {
    int i,j,ret1=0,ret2=0,ret3=0,ret4=0;
    for(i=0;i<cut1;i++) {
        for(j=0;j<cut2;j++) {
            if(arr[i][j]=='@') {
                ret1++;
            }
        }
    }
    for(i=0;i<r;i++) {
        for(j=0;j<cut2;j++) {
            if(arr[i][j]=='@') {
                ret2++;
            }
        }
    }
    ret2-=ret1;

    for(i=0;i<cut1;i++) {
        for(j=cut2;j<c;j++) {
            if(arr[i][j]=='@') {
                ret3++;
            }
        }
    }

    for(i=0;i<r;i++) {
        for(j=cut2;j<c;j++) {
            if(arr[i][j]=='@') {
                ret4++;
            }
        }
    }
    ret4-=ret3;

    return ret1==ret2&&ret2==ret3&&ret3==ret4;

}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    int i,j;

    scanf(" %d",&t);

    for(p=1;p<=t;p++) {

        scanf(" %d %d %d %d",&r,&c,&v1,&v2);
        getchar();
        for(i=0;i<r;i++) {
            for(j=0;j<c;j++) {
                scanf("%c",&arr[i][j]);
            }
            getchar();
        }

        int flag=0;
        for(i=1;i<r&&flag==0;i++) {
            for(j=1;j<c;j++) {
                if(func(i,j)) {
                    flag=1;
                    break;
                }
            }
        }

        if(flag) {
            printf("Case #%d: POSSIBLE\n",p);
        } else {
            printf("Case #%d: IMPOSSIBLE\n",p);
        }

    }

    return 0;
}
