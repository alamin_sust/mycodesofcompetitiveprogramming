/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int n,m,i,j,res,curr_cont_left,curr_pack_left;
pair<int,int> pack[2510],cont[2510];
char arr[1000010];
vector<int>v;

int comp(pair<int,int> a,pair<int,int> b) {
    return a.first<a.second;
}

bool can_inserted(double edge, double r) {

    return (r*r*2.0*2.0)>(2.0*edge*edge);

}


void compute_arr() {
    int curr=0;
    int len=strlen(arr);
    for(i=0;i<len;i++) {
        if(arr[i]==' ') {
            v.push_back(curr);
            curr=0;
            continue;
        }
        curr=(curr*2)+(arr[i]-'0');
        if((i+1)==len)v.push_back(curr);
    }
    return;
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    gets(arr);
    v.clear();
    compute_arr();
    n=v.size();
    for(i=0;i<n;i++) {
        pack[i].first=v[i];
    }

    gets(arr);
    v.clear();
    compute_arr();
    for(i=0;i<n;i++) {
        pack[i].second=v[i];
    }

    gets(arr);
    v.clear();
    compute_arr();
    m=v.size();
    for(i=0;i<n;i++) {
        cont[i].first=v[i];
    }

    gets(arr);
    v.clear();
    compute_arr();
    for(i=0;i<n;i++) {
        cont[i].second=v[i];
    }

    sort(pack,pack+n,comp);
    sort(cont,cont+m,comp);

    res=0;
    curr_pack_left=pack[0].second;
    curr_cont_left=cont[0].second;
    for(i=0,j=0;i<n&&j<m;) {
        if(!can_inserted((double)pack[i].first,(double)cont[j].first)) {
            curr_cont_left=cont[++j].second;
            continue;
        }
        if(curr_pack_left==0) {
            i++;
            if(i<n)
            curr_pack_left=pack[i].second;
            continue;
        }
        if(curr_cont_left==0) {
            j++;
            if(j<m)
            curr_cont_left=cont[j].second;
            continue;
        }

        if(curr_pack_left==curr_cont_left) {
            res+=curr_pack_left;
            curr_pack_left=curr_cont_left=0;
        } else if(curr_pack_left<curr_cont_left) {
            res+=curr_pack_left;
            curr_cont_left-=curr_pack_left;
        } else {
            res+=curr_cont_left;
            curr_pack_left-=curr_cont_left;
        }
    }

    printf("%d\n",res);

    return 0;
}
