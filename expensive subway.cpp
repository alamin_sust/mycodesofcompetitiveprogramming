#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

map<string,ll>mpp;
char in[1010],in2[1010];
ll i,par[1010],n,nn,k,m,a,b,res;

struct node{
ll x,y,val;
};

bool operator<(node a,node b)
{
    return a.val>b.val;
}

node det;
priority_queue<node>pq;

main()
{
    while(cin>>n>>m)
    {
        if(n==0&&m==0)
            break;
        mpp.clear();
        for(i=1; i<=n; i++)
            par[i]=i,scanf(" %s",in),mpp[in]=i;
        k=0;
        for(i=1; i<=m; i++)
        {
            scanf(" %s",in);
            scanf(" %s %lld",in2,&det.val);
            det.x=mpp[in];
            det.y=mpp[in2];
            pq.push(det);
        }
        scanf(" %s",in);
        res=0;
        nn=1;
        while(!pq.empty())
        {
            det=pq.top();
            a=det.x;
            b=det.y;
            pq.pop();
            while(a!=par[a])
                a=par[a];
            while(b!=par[b])
                b=par[b];
            if(a!=b)
                par[a]=b,res+=det.val,nn++;
        }
        if(nn!=n)
            printf("Impossible\n");
        else
            printf("%lld\n",res);
    }
    return 0;
}


