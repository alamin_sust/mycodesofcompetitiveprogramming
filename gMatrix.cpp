/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 1000000000000LL
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


ll t,p,n,k,c,x,A[3010],B[3010],arr[3010][3010],res,i,j,i1,jj1;


struct node{
ll x,y,val;
};

node det;
priority_queue<node>pq;
bool operator<(node a,node b)
{
    return a.val<b.val;
}


int main()
{
    freopen("D-large-practice.in","r",stdin);
    freopen("D-large-practiceout.txt","w",stdout);

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++)
    {
        scanf(" %lld %lld %lld %lld",&n,&k,&c,&x);
        for(i=1LL;i<=n;i++)
        {
            scanf(" %lld",&A[i]);
        }
        for(i=1LL;i<=n;i++)
        {
            scanf(" %lld",&B[i]);
        }

        for(i=1LL;i<=n;i++)
        {
            for(j=1LL;j<=n;j++)
            {
                arr[i][j]=(A[i]*i+B[j]*j+c)%x;
                //printf("%lld ",arr[i][j]);
            }
            //printf("\n");
        }

        res=0LL;
        for(i=1LL;(i+k-1LL)<=n;i++)
        {
            for(j=1LL;(j+k-1LL)<=n;j++)
            {
                if(j==1LL)
                {
                    while(!pq.empty())
                    pq.pop();
                    for(i1=i;i1<=k+i-1LL;i1++)
                    {
                        for(jj1=j;jj1<=k+j-1LL;jj1++)
                        {
                            det.x=i1;
                            det.y=jj1;
                            det.val=arr[i1][jj1];
                                pq.push(det);
                        }
                    }
                }
                while(1)
                {
                    if(pq.top().y<j)
                    pq.pop();
                    else
                    {
                        res+=pq.top().val;
                        break;
                    }
                }
              //  printf("%lld....\n",pq.top());
                if((j+k-1LL)<n)
                {
                    for(i1=i;i1<=(i+k-1LL);i1++)
                    {
                                det.x=i1;
                            det.y=j+k;
                            det.val=arr[i1][j+k];
                                pq.push(det);

                    }
                }
            }
        }

        printf("Case #%lld: %lld\n",p,res);

    }

    return 0;
}


