/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define lim 100010
#define sf scanf
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;


map<int, int> big, small;
map<int, int> par;
set<int>st;
int A[lim];

int main(int argc, const char **argv) {
	//ios::sync_with_stdio(false);
	//freopen("/Users/mkg/Desktop/Codeforces Round #353 (Div. 2)/D.cpp","r",stdin);
	//freopen("/Users/mkg/Desktop/Codeforces Round #353 (Div. 2)/D.cpp","w",stdout);
	//double st=clock(),en;
	int n, c;
	while (~sf("%d", &n)) {
		REP(i, 1, n) {
			sf("%d", &A[i]);
			c = A[i];
			if (i == 1) {
				st.insert(c);
			} else {
				set<int>::iterator it, PT, QT;
				st.insert(c);
				it = st.find(c);
				int P = -1, Q = -1;
				PT = it;
				QT = it;
				PT++;
				if (PT != st.end())P = *PT;
				if (it != st.begin()) {
					QT--;
					Q = *QT;
				}
				if (P > 0 and small[P] == 0) {
					par[c] = P;
					small[P] = 1;
				} else if (Q > 0 and big[Q] == 0) {
					par[c] = Q;
					big[Q] = 1;
				}
			}
		}

		REP(i, 2, n) {
			pf("%d ", par[A[i]]);
		}
		pf("\n");
	}
	//en=clock();
	//cerr<<(double)(en-st)/CLOCKS_PER_SEC<<endl;
	return 0;
}
