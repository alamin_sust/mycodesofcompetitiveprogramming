/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

vector<int>adj[110];
int mat[110],n,vis[110];

bool match(int u)
{
    for(int i=0;i<adj[u].size();i++)
    {
        int v=adj[u][i];
        if(vis[v]==0)
        {
            vis[v]=1;
            if(mat[v]<0||match(mat[v]))
            {
                mat[v]=u;
                return true;
            }
        }
    }
    return false;
}

int BPM(void)
{
    int ret=0,i;
    memset(mat,-1,sizeof(mat));
    for(i=1;i<=n;i++)
    {
        memset(vis,0,sizeof(vis));
        if(match(i))
            ret++;
    }
    return ret;
}

int main()
{
    int p,t,m,i,j,arr1[110],arr2[110];
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        for(i=1;i<=n;i++)
        {
            scanf(" %d",&arr1[i]);
        }
        scanf(" %d",&m);
        for(i=1;i<=m;i++)
        {
            scanf(" %d",&arr2[i]);
        }

        for(i=1;i<=n;i++)
        {
            adj[i].clear();
            for(j=1;j<=m;j++)
            {
                if(arr1[i]==0&&arr2[j]==0)
                    continue;
                if(arr1[i]!=0&&(arr2[j]%arr1[i])==0)
                    adj[i].push_back(j);
            }
        }
        printf("Case %d: %d\n",p,BPM());
    }
    return 0;
}



