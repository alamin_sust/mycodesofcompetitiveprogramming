/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int i,n,k,res,lightened[200010],arr[200010],b[200010],bef,flag,lg;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    scanf(" %d %d",&n,&k);

    for(i=1;i<=n;i++)
    {
        scanf(" %d",&arr[i]);
    }
    bef=-1000010;
    for(i=1;i<=n;i++)
    {
        if(arr[i]==1)
            bef=i;
        b[i]=bef;
    }
    for(i=n+1;i<=(2*n+1);i++)
        b[i]=bef;

    res=0;
    flag=0;
    for(i=1;i<=n;)
    {
        //printf("%d...\n",i);
        lg=i+k-1;
        if(b[lg]==-1||abs(b[lg]-i)>=k)
        {
            flag=1;
            break;
        }
        if(lightened[b[lg]]==0)
            lightened[b[lg]]=1,res++;
        i=max(i+1,b[lg]+k);
        //printf("%d--%d\n",i,lg);
    }
    if(flag==1)
        printf("-1\n");
    else
        printf("%d\n",res);

    return 0;
}






