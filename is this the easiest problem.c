#include<stdio.h>
main()
{
    long long int n,i,flag[22],a,b,c,max,other,equi[22],iso[22];
    scanf(" %lld",&n);
    for(i=0; i<n; i++)
    {
        scanf(" %lld %lld %lld",&a,&b,&c);
        if(a==b&&b==c)
            equi[i]=1;
        else
            equi[i]=0;
        if(a==b||b==c||a==c)
            iso[i]=1;
        else
            iso[i]=0;
        if(a>=b&&a>=c)
        {
            max=a;
            other=b+c;
        }
        else if(b>=a&&b>=c)
        {
            max=b;
            other=a+c;
        }
        else
        {
            max=c;
            other=a+b;
        }
        if(max<other)
            flag[i]=1;
        else
            flag[i]=0;

    }
    for(i=0; i<n; i++)
    {
        if(flag[i]!=1)
            printf("Case %lld: Invalid\n",i+1);
        else if(flag[i]==1&&equi[i]==1)
            printf("Case %lld: Equilateral\n",i+1);
        else if(flag[i]==1&&iso[i]==1)
            printf("Case %lld: Isosceles\n",i+1);
        else if(flag[i]==1)
            printf("Case %lld: Scalene\n",i+1);
    }
    return 0;
}

