#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
using namespace std;

vector <int> edge[110][1010],cost[110][1010];
int d[10010],p=0;

void bfs(int source)
{
    int i;
    for(i=0; i<=10005; i++)
        d[i]=10000000;
    d[source]=0;
    queue <int> q;
    q.push(source);
    while(q.empty()==false)
    {
        int u=q.front();
        q.pop();
        int ucost=d[u];
        for(i=0; i<edge[p][u].size(); i++)
        {
            int v=edge[p][u][i];
            int vcost=ucost+cost[p][u][i];
            if(d[v]>vcost)
            {
                d[v]=vcost;
                q.push(v);
            }
        }
    }
    return;
}

main()
{
    int n,e,i,from,ttl,c,to,k=1,res,flag[10010];
    while(1)
    {
        cin>>e;
        if(e==0)
            break;
        memset(flag,0,sizeof(flag));
        for(i=0; i<e; i++)
        {
            cin>>from>>to;
            flag[from]=flag[to]=1;
            edge[p][from].push_back(to);
            cost[p][from].push_back(1);
            edge[p][to].push_back(from);
            cost[p][to].push_back(1);
        }
        while(1)
        {
            cin>>from>>ttl;
            if(from==0&&ttl==0)
                break;
            bfs(from);
            for(i=0,res=0; i<10005; i++)
            {
                if(flag[i]==1)
                {
                    //if(i!=from)
                    //{
                    //printf("%d %d\n",i,d[i]);
                        if(d[i]>ttl)
                            res++;
                    //}
                }
            }
            printf("Case %d: %d nodes not reachable from node %d with TTL = %d.\n",k++,res,from,ttl);
        }
        p++;
    }
    return 0;
}
