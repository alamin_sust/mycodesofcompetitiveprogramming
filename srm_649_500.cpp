#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class CartInSupermarketEasy
{
public:
	int calc(int N, int K)
	{
	    int ret=0;
	    vector<int>vc,tp;
	    vc.clear();
	    tp.clear();
	    vc.push_back(N);
	    int i=0;
	    while(1)
        {
            sort(vc.begin(),vc.end());
            int n=vc.size();
            for(int j=n-1;j>=0;j--)
            {
                if(vc[j]>1&&i<K)
                {
                    i++;
                    tp.push_back(vc[j]/2);
                    tp.push_back((vc[j]/2)+(vc[j]%2));
                }
                else if(vc[j]>1)
                {
                    tp.push_back(vc[j]-1);
                }
            }
            ret++;
            if(tp.empty())
                break;
            vc.clear();
            vc=tp;
            tp.clear();
        }
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	CartInSupermarketEasy objectCartInSupermarketEasy;

	//test case0
	int param00 = 5;
	int param01 = 0;
	int ret0 = objectCartInSupermarketEasy.calc(param00,param01);
	int need0 = 5;
	assert_eq(0,ret0,need0);

	//test case1
	int param10 = 5;
	int param11 = 2;
	int ret1 = objectCartInSupermarketEasy.calc(param10,param11);
	int need1 = 4;
	assert_eq(1,ret1,need1);

	//test case2
	int param20 = 15;
	int param21 = 4;
	int ret2 = objectCartInSupermarketEasy.calc(param20,param21);
	int need2 = 6;
	assert_eq(2,ret2,need2);

	//test case3
	int param30 = 7;
	int param31 = 100;
	int ret3 = objectCartInSupermarketEasy.calc(param30,param31);
	int need3 = 4;
	assert_eq(3,ret3,need3);

	//test case4
	int param40 = 45;
	int param41 = 5;
	int ret4 = objectCartInSupermarketEasy.calc(param40,param41);
	int need4 = 11;
	assert_eq(4,ret4,need4);

	//test case5
	int param50 = 100;
	int param51 = 100;
	int ret5 = objectCartInSupermarketEasy.calc(param50,param51);
	int need5 = 8;
	assert_eq(5,ret5,need5);

}

