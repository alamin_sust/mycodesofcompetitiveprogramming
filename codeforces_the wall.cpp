#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
using namespace std;

long long int gcd(long long int a,long long int b)
{
    if(b==0) return a;
    else gcd(b,a%b);
}


main()
{
    long long int x,y,a,b,i,j,res,flag,f1[1010],lcm,def;
    cin>>x>>y>>a>>b;
    lcm=x*y/gcd(x,y);
    //cout<<lcm<<"\n";
    for(flag=0,res=0,j=0,i=a;i<=b;i++)
    {
        if(i%lcm==0)
        {
            f1[j++]=i;
            flag++;
            res++;
        }
        if(flag==2)
            break;
    }
    if(flag==2)
    {
        res=0;
        def=f1[1]-f1[0];
        //cout<<def<<endl;
        res+=b/def-((a-1)/def);
    }
    printf("%I64d\n",res);
    return 0;
}
