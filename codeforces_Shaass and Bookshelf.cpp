#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#define pi 2*acos(0)
#define inf INT_MAX
#define eps 0.00000001
using namespace std;

int dp[110][110][110],t[110],w[110],n;

int rec(int i,int thick,int width)
{
    if(i>=n)
    {
        if(thick>=width)
        return thick;
    }
    if(dp[i][thick][width]!=-1)
    return dp[i][thick][width];
    int ret1=99999999,ret2=99999999;
    if((thick+t[i])<=(width-w[i]))
    ret1=rec(i+1,t[i]+thick,width-w[i]);
    else
    ret2=rec(i+1,thick-t[i],width+w[i]);
    return dp[i][thick][width]=min(ret1,ret2);
}

main()
{
    int i,width;
    scanf(" %d",&n);
    for(i=0,width=0;i<n;i++)
    {
        scanf(" %d %d",&t[i],&w[i]);
        width+=w[i];
    }
    memset(dp,-1,sizeof(dp));
    printf("%d\n",rec(0,0,width));
    return 0;
}
