#include<stdio.h>
#include<ctype.h>

void sl(float val, int n);
void ddb(float val, int n);
void syd(float val, int n);
void writeoutput(int year,float depreciation,float value);

main()
{
    int n,choice = 0;
    float val;
    char answer1 = 'Y',answer2 = 'Y';

    while(toupper(answer1) != 'N')
    {
        if(toupper(answer2) != 'N')
        {
            printf("\nOriginal value : ");
            scanf("%f",&val);
            printf("No. of years : ");
            scanf("%d",&n);
        }
        printf("\nMethod: (1-SL 2-DDB 3-SYD)");
        scanf("%d",&choice);
        switch(choice)
        {
            case 1: /*straight line method*/
                printf("\nStraight line method\n\n");
                sl(val, n);
                break;
            case 2: /*double-declining balance method*/
                printf("\ndouble-declining balance method\n\n");
                ddb(val, n);
                break;
            case 3: /*sum-of-the-years'-digits method*/
                printf("\nsum-of-the-years'-digits method\n\n");
                syd(val, n);
                break;
        }
        printf("\n\nAnother calculation? (Y/N) ");
        scanf("%1s", &answer1);
        if(toupper(answer1) != 'N')
        {
            printf("enter a new set of data? (Y/N)");
            scanf("%1s", &answer2);
        }
    }
    printf("\nGoodbye, have a nice day!\n");
}
    void sl(float val, int n)
    {
        float deprec;
        int year;
        deprec = val/n;
        for(year = 1; year <= n; ++year)
        {
            val -= deprec;
            writeoutput(year, deprec, val);
        }
        return;
    }
    void ddb(float val, int n)
    {
        float deprec;
        int year;

        for(year = 1; year <=n ; ++year)
        {
            deprec = 2*val/n;
            val -= deprec;
            writeoutput(year, deprec, val);
        }
        return;
    }

    void syd(float val, int n)
    {
        float tag, deprec;
        int year;

        tag = val;
        for(year =1; year <= n; ++year)
        {
            deprec = (n-year+1)*tag / (n*(n+1)/2);
            val -= deprec;
            writeoutput(year, deprec, val);
        }
        return;
    }

    void writeoutput(int year, float depreciation, float value)
    {
        printf("End of the year %2d",year);
        printf(" Depreciation: %7.2f", depreciation);
        printf(" Current value: %8.2f\n", value);
        return;
    }


