#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

int rr[]={0,0,-1,1};
int cc[]={1,-1,0,0};
int r,c,val1[1010][1010],val2[1010][1010];
char adj[1010][1010];

void bfs1(int x,int y)
{
    queue<int>xx;
    queue<int>yy;
    xx.push(x);
    yy.push(y);
    val1[x][y]=0;
    while(!xx.empty())
    {
        int px=xx.front();
        int py=yy.front();
        xx.pop();
        yy.pop();
        for(int i=0;i<4;i++)
        {
            int xxx=px+rr[i];
            int yyy=py+cc[i];
            if(xxx>0&&xxx<=r&&yyy>0&&yyy<=c)
            if(adj[xxx][yyy]!='#')
            if(val1[xxx][yyy]==inf)
            {
                val1[xxx][yyy]=val1[px][py]+1;
                xx.push(xxx);
                yy.push(yyy);
            }
        }
    }
    return;
}

void bfs2(void)
{
    queue<int>xx;
    queue<int>yy;
    for(int i=1;i<=r;i++)
        for(int j=1;j<=c;j++)
    if(adj[i][j]=='F')
    {xx.push(i);
    yy.push(j);
    val2[i][j]=0;}
    while(!xx.empty())
    {
        int px=xx.front();
        int py=yy.front();
        xx.pop();
        yy.pop();
        for(int i=0;i<4;i++)
        {
            int xxx=px+rr[i];
            int yyy=py+cc[i];
            if(xxx>0&&xxx<=r&&yyy>0&&yyy<=c)
            if(adj[xxx][yyy]!='#')
            if(val2[xxx][yyy]==inf)
            {
                val2[xxx][yyy]=val2[px][py]+1;
                xx.push(xxx);
                yy.push(yyy);
            }
        }
    }
    return;
}

main()
{
    char ch;
    int i,res,j,k,l,t,p,jx,jy,flag;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        jx=0;jy=0;
        cin>>r>>c;
        getchar();
        for(flag=0,i=1;i<=r;i++)
        {
            for(j=1;j<=c;j++)
            {
                val1[i][j]=val2[i][j]=inf;
                scanf("%c",&adj[i][j]);
                if(adj[i][j]=='J')
                {
                    jx=i;
                    jy=j;
                }
                if(adj[i][j]=='F')
                {
                    flag=1;
                }
            }
            getchar();
        }
        if(jx!=0)
        bfs1(jx,jy);
        if(flag!=0)
        bfs2();
        for(res=inf,i=1;i<=r;i++)
        {
            for(j=1;j<=c;j++)
            {
                if(i==1||j==1||i==r||j==c)
                {
                    if(val1[i][j]<val2[i][j])
                        res=min(res,val1[i][j]);
                }
            }
        }
        if(res!=inf)
        printf("%d\n",res+1);
        else
        printf("IMPOSSIBLE\n");
    }
    return 0;
}


