/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

char home[110],away[110],yr[10],ha[10];
int jrs,i,n,mn,mpp[310];

int main()
{
    scanf(" %s",home);
    scanf(" %s",away);
    cin>>n;
    for(i=0;i<n;i++)
    {
        scanf(" %d %s %d %s",&mn,ha,&jrs,yr);
        if(ha[0]=='h')
        {
            if(yr[0]=='r'&&mpp[jrs]<2)
                printf("%s %d %d\n",home,jrs,mn),mpp[jrs]++;
            else if(mpp[jrs]==1)
                printf("%s %d %d\n",home,jrs,mn);
            mpp[jrs]++;
        }
        else
        {
            if(yr[0]=='r'&&mpp[100+jrs]<2)
                printf("%s %d %d\n",away,jrs,mn),mpp[100+jrs]++;
            else if(mpp[100+jrs]==1)
                printf("%s %d %d\n",away,jrs,mn);
            mpp[100+jrs]++;
        }
    }
    return 0;
}

