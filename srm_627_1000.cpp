#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

struct node{
int from,to,len;
};

node det;

bool operator<(node a,node b)
{
    return a.len<b.len;
}

priority_queue<node>pq;

class BubbleSortWithReversals
{
public:
	int getMinSwaps(vector <int> A, int K)
	{
	    int ret=0,beg=0,en=0,l,i,j,k;
	    l=A.size();
	    while(!pq.empty())
            pq.pop();
	    for(i=1;i<l;i++)
        {
            if(A[i]>A[i-1])
            {
                det.from=beg;
                det.to=i-1;
                det.len=i-beg;
                pq.push(det);
                beg=i;
                en=i;
            }
            else
            {
                en++;
            }
        }
        if(beg<en)
        {
            det.from=beg;
                det.to=i-1;
                det.len=i-beg;
                pq.push(det);
        }
        for(i=0;i<K&&(!pq.empty());i++)
        {
            det=pq.top();
            pq.pop();
            printf(" %d %d\n",det.from,det.to);
            for(j=det.from,k=det.to;j<=k;j++,k--)
            {
                swap(A[j],A[k]);
            }
        }
        ret=0;
        for(i=0;i<l;i++)
            printf("..%d\n",A[i]);
        for(i=0;i<(l-1);i++)
        {
            for(j=0;j<(l-2);j++)
            {
                if(A[j]>A[j+1])
                    {swap(A[j],A[j+1]);
                    ret++;}
            }
        }
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	BubbleSortWithReversals objectBubbleSortWithReversals;

	//test case0
	vector <int> param00;
	param00.push_back(6);
	param00.push_back(8);
	param00.push_back(8);
	param00.push_back(7);
	param00.push_back(7);
	int param01 = 1;
	int ret0 = objectBubbleSortWithReversals.getMinSwaps(param00,param01);
	int need0 = 0;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(7);
	param10.push_back(2);
	param10.push_back(2);
	param10.push_back(13);
	param10.push_back(5);
	param10.push_back(5);
	param10.push_back(2);
	int param11 = 2;
	int ret1 = objectBubbleSortWithReversals.getMinSwaps(param10,param11);
	int need1 = 3;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(12);
	param20.push_back(5);
	param20.push_back(1);
	param20.push_back(10);
	param20.push_back(12);
	param20.push_back(6);
	param20.push_back(6);
	param20.push_back(10);
	param20.push_back(6);
	param20.push_back(8);
	int param21 = 2;
	int ret2 = objectBubbleSortWithReversals.getMinSwaps(param20,param21);
	int need2 = 12;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(2);
	param30.push_back(3);
	param30.push_back(1);
	int param31 = 2;
	int ret3 = objectBubbleSortWithReversals.getMinSwaps(param30,param31);
	int need3 = 1;
	assert_eq(3,ret3,need3);

	//test case4
	vector <int> param40;
	param40.push_back(482);
	param40.push_back(619);
	param40.push_back(619);
	param40.push_back(601);
	param40.push_back(660);
	param40.push_back(660);
	param40.push_back(691);
	param40.push_back(691);
	param40.push_back(77);
	param40.push_back(77);
	param40.push_back(96);
	param40.push_back(77);
	int param41 = 9;
	int ret4 = objectBubbleSortWithReversals.getMinSwaps(param40,param41);
	int need4 = 22;
	assert_eq(4,ret4,need4);
	return 0;
}
