#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
#define MAXCANDIDATE 10
using namespace std;
int c[110];
void construct_candidates(int a[],int k,int c[],int *ncandidates,int input)
{
   bool used[10];
   memset(used,0,sizeof(used));
   for(int i=0;i<k;i++)
   {
       used[a[i]]=true;
   }
   *ncandidates=0;

   for(int i=1;i<=input;i++)
   {
       if(used[i]==false)
       {
           c[(*ncandidates)++]=i;
       }
   }
}

bool is_a_solution(int a[],int k,int input)
{
    if(k==input)
        return true;
    return false;
}

void process_solution(int a[],int k,int input)
{
    int i;
    for(i=1;i<=input;i++)
    {
        printf("%d ",a[i]);
    }
    printf("\n");
}

void backtracking(int a[],int k,int input)
{
    int c[MAXCANDIDATE];
    int ncandidates;
    int i;

    if(is_a_solution(a,k,input))
        {process_solution(a,k,input);
        return;}
    k++;
    construct_candidates(a,k,c,&ncandidates,input);
    for(i=0;i<ncandidates;i++)
    {
        a[k]=c[i];
        backtracking(a,k,input);
        if(k==input)
        {
            return;
        }
    }
}

main()
{
    int input;
    int a[1010];
    while(scanf("%d",&input))
    {
        for(int i=0;i<input;i++)
            cin>>c[i];
        backtracking(a,0,input);
    }
}
