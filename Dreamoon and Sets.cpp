/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int n,k,i,j,mx,a[10010],b[10010],c[10010],d[10010];

int main()
{
    cin>>n>>k;

    for(j=1,i=1;i<=n;j+=6,i++)
    {
        a[i]=j;
        b[i]=j+2;
        c[i]=j+4;
    }
    mx=j-2;
    for(j=2,i=1;i<=n;j+=2)
    {
        if(__gcd(a[i],j)==1&&__gcd(b[i],j)==1&&__gcd(c[i],j)==1)
        {
            d[i]=j;
            mx=max(mx,j);
            i++;
        }
    }
    printf("%d\n",mx*k);
    for(i=1;i<=n;i++)
        printf("%d %d %d %d\n",a[i]*k,b[i]*k,c[i]*k,d[i]*k);
    return 0;
}
