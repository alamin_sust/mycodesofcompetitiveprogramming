/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int mn,mx,res,i,n,a,arr[100010];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    scanf(" %d %d",&n,&a);

    for(i=0; i<n; i++)
    {
        scanf(" %d",&arr[i]);
    }

    if(n==1)
    {
        printf("0\n");
        return 0;
    }

    sort(arr,arr+n);

    res=1000000010;


    if(a<=arr[1])
    {
        res=min(res,arr[n-1]-a);
    }
    else if(a>=arr[n-1])
    {
        res=min(res,a-arr[1]);
    }
    else
    {
        mn=min(arr[n-1]-a,a-arr[1]);
        mx=max(arr[n-1]-a,a-arr[1]);
        res=min(res,(mn*2)+mx);
    }
    if(a<=arr[0])
    {
        res=min(res,arr[n-2]-a);
    }
    else if(a>=arr[n-2])
    {
        res=min(res,a-arr[0]);
    }
    else
    {
        mn=min(arr[n-2]-a,a-arr[0]);
        mx=max(arr[n-2]-a,a-arr[0]);
        res=min(res,(mn*2)+mx);
    }


    printf("%d\n",res);






    return 0;
}






