/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
int rr[]={-1,1};
int cc[]={-1,1};
using namespace std;

int arr[1010][1010],r,c,n,perm[10],res,xx[10],yy[10],t;

int rec(int nx,int ny,int sum,int pos) {
    if(pos==n) return sum;

    int ret=sum;

    for(int i=0;i<2;i++){
        for(int j=0;j<2;j++) {
            int nowx=nx+rr[i]*xx[perm[pos]];
            int nowy=ny+cc[j]*yy[perm[pos]];
            if(nowx>=0&&nowx<r&&nowy>=0&&nowy<c) {
                ret=max(ret,rec(nowx,nowy,sum+arr[nowx][nowy],pos+1));
            }
        }
    }
    return ret;
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    int sx,sy,i,j,p;

    scanf(" %d",&t);

    for(p=1;p<=t;p++) {
        scanf(" %d %d %d",&r,&c,&n);
        scanf(" %d %d",&sx,&sy);
        for(i=0;i<n;i++) scanf(" %d",&xx[i]);
        for(i=0;i<n;i++) scanf(" %d",&yy[i]);

        for(i=0;i<r;i++)
            for(j=0;j<c;j++)
            scanf(" %d",&arr[i][j]);

        for(i=0;i<n;i++) {
            perm[i]=i;
        }

        res=0;

        do{
        res=max(res,rec(sx,sy,arr[sx][sy],0));
        }while(next_permutation(perm,perm+n));
        printf("%d\n",res);
    }



    return 0;
}

