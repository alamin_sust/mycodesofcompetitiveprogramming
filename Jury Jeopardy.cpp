/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


int flag[110][210],t,p,i,j,minx,miny,maxx,maxy,gridx,gridy,dirx,diry,curx,cury,l;
char arr[100010];

int main()
{
    cin>>t;
    cout<<t<<endl;
    for(p=1;p<=t;p++)
    {
        scanf(" %s",&arr);
            l=strlen(arr);
        memset(flag,0,sizeof(flag));
        curx=1;
        cury=105;
        dirx=1;
        diry=0;
        flag[1][105]=1;
        minx=maxx=1;
        miny=maxy=105;
        for(i=0;i<l;i++)
        {
            if(arr[i]=='F')
            {
                curx+=dirx;
                cury+=diry;
                flag[curx][cury]=1;
            }
            else if(arr[i]=='B')
            {
                dirx*=-1;
                diry*=-1;
                curx+=dirx;
                cury+=diry;
                flag[curx][cury]=1;
            }
            else if(arr[i]=='R')
            {
                if(dirx==1&&diry==0)
                    dirx=0,diry=-1;
                else if(dirx==0&&diry==-1)
                    dirx=-1,diry=0;
                else if(dirx==-1&&diry==0)
                    dirx=0,diry=1;
                else if(dirx==0&&diry==1)
                    dirx=1,diry=0;
                curx+=dirx;
                cury+=diry;
                flag[curx][cury]=1;
            }
            else
            {
                if(dirx==0&&diry==-1)
                    dirx=1,diry=0;
                else if(dirx==1&&diry==0)
                    dirx=0,diry=1;
                else if(dirx==0&&diry==1)
                    dirx=-1,diry=0;
                else if(dirx==-1&&diry==0)
                    dirx=0,diry=-1;
                curx+=dirx;
                cury+=diry;
                flag[curx][cury]=1;
            }
            minx=min(minx,curx);
            miny=min(miny,cury);
            maxx=max(maxx,curx);
            maxy=max(maxy,cury);
        }
        gridx=maxx+1;
        gridy=maxy-miny+3;
        printf("%d %d\n",gridy,gridx);
        for(i=maxy+1;i>=miny-1;i--)
        {
            for(j=1;j<=maxx+1;j++)
            {
                if(flag[j][i])
                    printf(".");
                else
                    printf("#");
            }
            printf("\n");
        }
    }
    return 0;
}


