#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

int n,m,dist[210],dist2[210];

vector<int>adj_list[210];
vector<int>wt[210];

void b_ford(void)
{
    for(int i=1;i<=n;i++)
        {dist[i]=inf;
        dist2[i]=inf;}
    dist[1]=dist2[1]=0;
    for(int i=1;i<n;i++)
    {
        for(int k=1;k<=n;k++)
        for(int j=0;j<adj_list[k].size();j++)
        {
            if(dist[k]!=inf&&(dist[k]+wt[k][j])<dist[adj_list[k][j]])
                dist2[adj_list[k][j]]=dist[adj_list[k][j]]=dist[k]+wt[k][j];
        }
    }
    for(int i=1;i<n;i++)
    {
        for(int k=1;k<=n;k++)
        for(int j=0;j<adj_list[k].size();j++)
        {
            if(dist2[k]!=inf&&(dist2[k]+wt[k][j])<dist2[adj_list[k][j]])
                dist2[adj_list[k][j]]=dist2[k]+wt[k][j];
        }
    }
    return;
}

main()
{
    int p,t,cost[210],i,u,v,j,in,q;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        for(i=1;i<=n;i++)
            {adj_list[i].clear();
            wt[i].clear();
            scanf(" %d",&cost[i]);}
        scanf(" %d",&m);
        for(i=1;i<=m;i++)
        {
            scanf(" %d %d",&u,&v);
            adj_list[u].push_back(v);
            wt[u].push_back((cost[v]-cost[u])*(cost[v]-cost[u])*(cost[v]-cost[u]));
        }
        printf("Case %d:\n",p);
        scanf(" %d",&q);
        b_ford();
        for(i=1;i<=q;i++)
        {
        scanf(" %d",&in);
        if(dist[in]==dist2[in]&&dist[in]<inf&&dist[in]>2)
        printf("%d\n",dist[in]);
        else
        printf("?\n");
        }
    }
    return 0;
}

