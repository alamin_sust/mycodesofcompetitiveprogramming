#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

#define MAX 10100
long long i, j, sieve[MAX], primecount=0, n,prime[MAX], in,now,fg,flag[11000];
void sieve_(void)
{
    for(i=0; i<MAX; i++)
    {
        sieve[i]=1;
    }
    sieve[0]=sieve[1]=0;
    for(i=2; i<MAX; i++)
    {
        while(sieve[i]==0 && i<MAX)
        {
            i++;
        }
        prime[primecount]=i;
        for(j=i*i; j<MAX; j+=i)
        {
            sieve[j]=0;
        }
        primecount++;
    }
}

main()
{

    sieve_();
    cin>>n;
    for(i=n;i>0;i--)
    {
        now=i;
        for(j=0;prime[j]<=now;j++)
        {
            while((now%prime[j])==0)
            {
                now/=prime[j];
                flag[prime[j]]++;
            }
        }
    }
    fg=0;
    for(i=0;prime[i]<=n;i++)
    {
        if(fg==1)
            printf(" * ");
        fg=1;
        if(flag[prime[i]])
        printf("%lld^%lld",prime[i],flag[prime[i]]);
    }
    printf("\n");
    return 0;
}
