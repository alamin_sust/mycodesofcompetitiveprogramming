#include<stdio.h>
#include<string.h>
#include<math.h>
#define infinity 99999
int p,t,i,j,k,nodes,flag,adj[10100][10100],r,c,x[10100],y[10100],val;
char arr[110][110][110],ch;

main()
{

    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %d %d%c",&r,&c,&ch);
        for(i=2;i<=r+1;i++)
        {
            for(j=2;j<=c+1;j++)
            {
                scanf("%c",&arr[p][i][j]);
            }
            scanf("%c",&ch);
        }

        for(i=2;i<=r+1;i++)
        {
            for(j=2;j<=c+1;j++)
            {
                if(arr[p][i][j]=='Z')
                {
                    if(arr[p][i-2][j-1]=='.')
                    arr[p][i-2][j-1]='z';
                     if(arr[p][i-2][j+1]=='.')
                    arr[p][i-2][j+1]='z';
                     if(arr[p][i-1][j-2]=='.')
                    arr[p][i-1][j-2]='z';
                     if(arr[p][i-1][j+2]=='.')
                    arr[p][i-1][j+2]='z';
                     if(arr[p][i+1][j-2]=='.')
                    arr[p][i+1][j-2]='z';
                     if(arr[p][i+1][j+2]=='.')
                    arr[p][i+1][j+2]='z';
                     if(arr[p][i+2][j-1]=='.')
                    arr[p][i+2][j-1]='z';
                     if(arr[p][i+2][j+1]=='.')
                    arr[p][i+2][j+1]='z';
                }
            }
        }
       for(k=2,i=2;i<=r+1;i++)
        {
            for(j=2;j<=c+1;j++)
            {
                if(arr[p][i][j]=='.')
                {
                    x[k]=i;
                    y[k]=j;
                    k++;
                }
            }
        }

        flag=0;
        for(i=2;i<=r+1&&flag<2;i++)
        {
            for(j=2;j<=c+1;j++)
            {
                if(arr[p][i][j]=='A')
                {
                    x[1]=i;
                    y[1]=j;
                    flag++;
                }
                if(arr[p][i][j]=='B')
                {
                    x[k]=i;
                    y[k]=j;
                    flag++;
                    k++;
                }
            }
        }
        for(i=1;i<k;i++)
        {
            for(j=1;j<k;j++)
            {
                adj[i][j]=infinity;
            }
        }

        for(i=1;i<k;i++)
        {
            for(j=1;j<k;j++)
            {
                if(abs(x[i]-x[j])<=1&&abs(y[i]-y[j])<=1)
                {
                adj[i][j]=1;
                }
            }
        }
        nodes=k;
        for(k=1;k<nodes;k++)
        {
            for(i=1;i<nodes;i++)
            {
                for(j=1;j<nodes;j++)
                {
                    if(adj[i][j]>(adj[i][k]+adj[k][j]))
                    adj[i][j]=adj[i][k]+adj[k][j];
                }
            }
        }
        val=adj[1][nodes-1];
        if(val==infinity)
        printf("King Peter, you can't go now!\n");
        else
        printf("Minimal possible length of a trip is %d\n",val);
    }
    return 0;
}
