/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

queue<int>q;

int n,m,a,b,i,in[100010],par[100010],arr[100010],now,mpp[100010];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    scanf(" %d %d",&n,&m);

    for(i=0;i<m;i++)
    {
        scanf(" %d %d",&a,&b);
        par[b]=a;
        in[a]++;
    }

    for(i=1;i<=n;i++)
    {
        scanf(" %d",&arr[i]);
    }

    for(i=1;i<=n;i++)
    {
        if(mpp[arr[i]])
            continue;
        now=i;
        while(1)
        {
            if(now==arr[i])
                break;
            in[par[now]]--;
            mpp[now]=1;
            now=par[now];
        }
    }

    for(i=1;i<=n;i++)
    {
        if(mpp[arr[i]])
        {
            printf("-1\n");
            return 0;
        }
    }
    int cc=0;
    for(i=1;i<=n;i++)
    {
        if(mpp[i])
            continue;
        cc++;
        if(in[i]==0)
        {
            q.push(i);
        }
    }

    printf("%d\n",cc);
    while(!q.empty())
    {
        now=q.front();
        q.pop();
        printf("%d\n",now);
        in[par[now]]--;
        if(in[par[now]]==0)
            q.push(par[now]);
    }


    return 0;
}

