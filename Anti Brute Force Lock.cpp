#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
int x,y,val;
};

node det;

bool operator<(node a,node b)
{
    return a.val>b.val;
}

priority_queue<node>pq;
char arr[510][7];

int rolls(int a,int b)
{
    int ret=0,f,t;
    for(int i=0;i<4;i++)
    {
        f=arr[a][i]-'0';
        t=arr[b][i]-'0';
        ret+=min(abs(f-t),10-abs(f-t));
    }
    return ret;
}

main()
{
    int t,p,i,n,res,a,b,c,j,par[510];
    cin>>t;
    arr[0][0]='0';
    arr[0][1]='0';
    arr[0][2]='0';
    arr[0][3]='0';
    for(p=1;p<=t;p++)
    {
        cin>>n;
        //par[0]=0;
        for(i=1;i<=n;i++)
            scanf(" %s",&arr[i]),par[i]=i;
        for(i=1;i<=n;i++)
        {
            for(j=i+1;j<=n;j++)
            {
                det.x=i;
                det.y=j;
                det.val=rolls(i,j);
                pq.push(det);
                swap(det.x,det.y);
                pq.push(det);
            }
        }
        //res=0;
        res=99999999;
        for(i=1;i<=n;i++)
        {
            res=min(res,rolls(0,i));
        }
        while(!pq.empty())
        {
            a=pq.top().x;
            b=pq.top().y;
            c=pq.top().val;
            pq.pop();
            while(par[a]!=a)
                a=par[a];
            while(par[b]!=b)
                b=par[b];
            if(a!=b)
                par[a]=b,res+=c;
        }
        printf("%d\n",res);
    }
    return 0;
}
