/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node
{
   ll first,second;
};

bool operator<(node a,node b)
{
    if(a.first==b.first)
    return a.second>b.second;
    return a.first<b.first;
}

priority_queue<node>pq;
priority_queue<ll>pq2;
queue<ll>q;
ll res,t,p,i,n,m,l,d,arr[100010],h=40000000000000010LL;



int main()
{
    freopen("laundro_matt.txt","r",stdin);
    freopen("LaundroMattOut2.txt","w",stdout);

    scanf(" %lld",&t);

    for(p=1LL; p<=t; p++)
    {
        while(!pq.empty())
            pq.pop();
        while(!pq2.empty())
            pq2.pop();
        while(!q.empty())
            q.pop();
        scanf(" %lld %lld %lld %lld",&l,&n,&m,&d);
        for(i=1LL; i<=n; i++)
        {
            scanf(" %lld",&arr[i]);
            node det;
            det.first=h-arr[i];
            det.second=i;
            pq.push(det);
        }
        for(i=1LL; i<=l; i++)
        {
            node pii=pq.top();
            pq.pop();
            pq2.push(pii.first);
            pii.first-=arr[pii.second];
            pq.push(pii);
        }
        while(!pq.empty())
            pq.pop();

        res=0LL;

        if(l<=m)
        {
            while(!pq2.empty())
            {
                res=max(res,h-(pq2.top()-d));
                pq2.pop();
            }
        }
        else
        {
            for(i=1LL; i<=m; i++)
            {
                q.push(pq2.top()-d);
                pq2.pop();
            }

            for(;i<=l&&(!pq2.empty());i++)
            {
            ll tmp=pq2.top();
            pq2.pop();

            ll pii=q.front();
            q.pop();

            pii=min(pii,tmp)-d;
            res=max(res,h-pii);

            q.push(pii);
            }

        }
        printf("Case #%lld: %lld\n",p,res);
    }
    return 0;
}


