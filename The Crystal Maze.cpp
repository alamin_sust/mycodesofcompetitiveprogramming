#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

int det[510][510],res;
char arr[510][510];

void rec(int i,int j)
{
    if(arr[i][j]=='C')
        res++;
    arr[i][j]='*';
    if(arr[i-1][j]!='#'&&arr[i-1][j]!='*')
        rec(i-1,j);
    if(arr[i+1][j]!='#'&&arr[i+1][j]!='*')
        rec(i+1,j);
    if(arr[i][j-1]!='#'&&arr[i][j-1]!='*')
        rec(i,j-1);
    if(arr[i][j+1]!='#'&&arr[i][j+1]!='*')
        rec(i,j+1);
    return;
}

void floodfill(int i,int j)
{
    arr[i][j]='#';
    det[i][j]=res;
    if(arr[i-1][j]!='#')
        floodfill(i-1,j);
    if(arr[i+1][j]!='#')
        floodfill(i+1,j);
    if(arr[i][j-1]!='#')
        floodfill(i,j-1);
    if(arr[i][j+1]!='#')
        floodfill(i,j+1);
    return;
}

main()
{
    int x,y,p,t,row,col,q,i,j;
    cin>>t;
    for(p=1; p<=t; p++)
    {
        memset(det,0,sizeof(det));
        cin>>row>>col>>q;
        getchar();
        for(i=0; i<=row+1; i++)
        {
            for(j=0; j<=col+1; j++)
                arr[i][j]='#';
        }
        for(i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
                scanf("%c",&arr[i][j]);
            getchar();
        }
        for(i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
            {
                if(arr[i][j]=='C')
                {
                    res=0;
                    rec(i,j);
                    floodfill(i,j);
                }
            }
        }
        printf("Case %d:\n",p);
        for(i=1;i<=q;i++)
            {cin>>x>>y;
            printf("%d\n",det[x][y]);}
    }
    return 0;
}

/*
1
4 5 2
..#..
.C#C.
##..#
..C#C
1 1
4 1
*/
