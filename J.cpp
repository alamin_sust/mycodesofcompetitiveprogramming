#include <bits/stdc++.h>

using namespace std;

typedef long long Long;
#define lim 100007

set<pair<Long, int> > window;
vector<pair<Long, int> > DP;
int D[lim], P[lim];

int main()
{
    freopen("journey.in","r",stdin);
    freopen("journey.out","w",stdout);
    int N;
    Long T;
    while(cin>>N>>T)
    {
        T = T - N + 1;
        for (int i=1; i<N; ++i)
        {
            cin>>P[i];
        }
        for (int i=1; i<N-1; ++i)
        {
            cin>>D[i];
        }
        int lo = 1, hi = N;
        while(lo<hi)
        {
            int mid = (lo + hi)/2;
            window.clear();
            DP.clear();
            window.insert(pair<Long, int>(0,0));
            DP.push_back(pair<Long, int>(0,0));
            for (int i=1; i<N; ++i)
            {
                auto T = window.begin();
                pair<Long, int> pp = pair<Long, int>((D[i] + (*T).first), i);
                DP.push_back(pp);
                window.insert(pp);
                if(i>=mid)
                {
                    window.erase(DP[i-mid]);
                }
            }
            if(DP[N-1].first<=T)
            {
                hi = mid;
            }
            else
            {
                lo = mid + 1;
            }
        }
        int mmn = 100000000;
        //cout<<"Min Ticket: "<<hi<<endl;
        for (int i=hi; i<N; ++i)
        {
            mmn = min(mmn,P[i]);
        }
        cout<<mmn<<endl;
    }
    return 0;
}
