/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll p,t,i,j,k,n,v,st;
char arr[1010][25];
vector<ll>vec;

int main()
{

    freopen("tourist.txt","r",stdin);
    freopen("tourist_output.txt","w",stdout);

    scanf(" %I64d", &t);

    for(p=1LL;p<=t;p++) {
        vec.clear();
        scanf(" %I64d %I64d %I64d",&n,&k,&v);

        for(i=0LL;i<n;i++) {
            scanf(" %s", &arr[i]);
        }

        st = ((v-1LL)*k)%n;

        printf("Case #%I64d:",p);
        for(i=st,j=0LL;j<k;i++,j++) {
            if(i==n) {
                i=0LL;
            }
            vec.push_back(i);
        }
        sort(vec.begin(),vec.end());

        for(i=0LL;i<vec.size();i++) {
            printf(" %s",arr[vec[i]]);
        }
        printf("\n");
    }

    return 0;
}
