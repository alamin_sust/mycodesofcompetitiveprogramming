/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll t,p,n,k,b,arr[100010],res,i,now;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++) {
        scanf(" %lld %lld %lld",&n,&k,&b);
        for(i=0LL;i<n;i++) {
            scanf(" %lld",&arr[i]);
        }

//        if(k==0LL) {
//            printf("%lld\n",n);
//            continue;
//        }

        sort(arr,arr+n);

        res=1LL;
        now=arr[0]*k+b;
        for(i=1LL;i<n;i++) {
            if(arr[i]<now) continue;
            res++;
            now=arr[i]*k+b;
        }

        printf("%lld\n",res);
    }


    return 0;
}
