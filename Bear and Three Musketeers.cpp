/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 100000010
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


vector<int>adj[4010];
int n,m,i,j,k,u,v,w,from,res,to,arr[4010][4010],sz[4010];


int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d %d",&n,&m);

    for(i=1;i<=m;i++)
    {
        scanf(" %d %d",&from,&to);
        adj[from].push_back(to);
        adj[to].push_back(from);
        arr[from][to]=arr[to][from]=1;
    }
    for(i=1;i<=n;i++)
    {
        sort(adj[i].begin(),adj[i].end());
        sz[i]=adj[i].size();
    }

   // memset(dp,-1,sizeof(dp));
   // res=rec(1,0,0);
    res=inf;
    for(i=1;i<=n;i++)
    {
        u=i;
        for(j=0;j<sz[i];j++)
        {
            v=adj[i][j];
            for(k=0;k<sz[v];k++)
            {
                w=adj[v][k];
                if(arr[u][v]==1&&arr[v][w]==1&&arr[u][w]==1&&u!=v&&v!=w)
                {
                    res=min(res,sz[u]+sz[v]+sz[w]-6);
                }
            }
        }
    }


    if(res>=inf)
    printf("-1\n");
    else
    printf("%d\n",res);

    return 0;
}


