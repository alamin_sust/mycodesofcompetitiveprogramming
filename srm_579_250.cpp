#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class PrimalUnlicensedCreatures
{
public:
	int maxWins(int initialLevel, vector <int> grezPower)
	{
	    int res,i,arr[1010],k=0,temp=initialLevel,flag=0,fg[1010]={0};
	    while(grezPower.empty()==0)
        {
            arr[k++]=grezPower.back();
            printf("%d ",arr[k-1]);
            grezPower.pop_back();
        }
        sort(arr,arr+k);
        res=0;
        while(flag==0)
        {
            flag=1;
            for(i=k-1;i>=0;i--)
            {
                if(temp>arr[i]&&fg[i]==0)
                {
                    temp+=arr[i]/2;
                    fg[i]=1;
                    res++;
                    flag=0;
                    break;
                }
            }
        }
	    return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	PrimalUnlicensedCreatures objectPrimalUnlicensedCreatures;

	//test case0
	int param00 = 31;
	vector <int> param01;
	param01.push_back(10);
	param01.push_back(20);
	param01.push_back(30);
	int ret0 = objectPrimalUnlicensedCreatures.maxWins(param00,param01);
	int need0 = 3;
	assert_eq(0,ret0,need0);

	//test case1
	int param10 = 20;
	vector <int> param11;
	param11.push_back(24);
	param11.push_back(5);
	param11.push_back(6);
	param11.push_back(38);
	int ret1 = objectPrimalUnlicensedCreatures.maxWins(param10,param11);
	int need1 = 3;
	assert_eq(1,ret1,need1);

	//test case2
	int param20 = 20;
	vector <int> param21;
	param21.push_back(3);
	param21.push_back(3);
	param21.push_back(3);
	param21.push_back(3);
	param21.push_back(3);
	param21.push_back(1);
	param21.push_back(25);
	int ret2 = objectPrimalUnlicensedCreatures.maxWins(param20,param21);
	int need2 = 6;
	assert_eq(2,ret2,need2);

	//test case3
	int param30 = 4;
	vector <int> param31;
	param31.push_back(3);
	param31.push_back(13);
	param31.push_back(6);
	param31.push_back(4);
	param31.push_back(9);
	int ret3 = objectPrimalUnlicensedCreatures.maxWins(param30,param31);
	int need3 = 5;
	assert_eq(3,ret3,need3);

	//test case4
	int param40 = 7;
	vector <int> param41;
	param41.push_back(7);
	param41.push_back(8);
	param41.push_back(9);
	param41.push_back(10);
	int ret4 = objectPrimalUnlicensedCreatures.maxWins(param40,param41);
	int need4 = 0;
	assert_eq(4,ret4,need4);

}

