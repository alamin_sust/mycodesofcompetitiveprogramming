/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll p,t,i,j,n,m,l,r,expo;
double res,arr[1010];

int main()
{
    freopen("B-large-practice.in","r",stdin);
    freopen("B-large-practice.out","w",stdout);


    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++)
    {
        scanf(" %lld %lld",&n,&m);

        for(i=0LL;i<n;i++)
        {
            scanf(" %lf",&arr[i]);
            //pw[i][0]=arr[i];
           // for(j=1LL;j<=30LL;j++)
           // {
           //     pw[i][j]=sqrt(pw[i][j-1]);
           // }
        }
        printf("Case #%lld:\n",p);
        for(i=0LL;i<m;i++)
        {
            scanf(" %lld %lld",&l,&r);
            expo=r-l+1LL;
            res=1.0;
            for(j=l;j<=r;j++)
            {
//                ll tpex=expo,cnt=0LL;
//                while(tpex)
//                {
//                    if(tpex&1LL)
//                    res*=pw[j][cnt];
//                    cnt++;
//                    tpex>>=1LL;
//                }
              res*=pow(arr[j],1.0/expo);
            }
            printf("%.9lf\n",res);
        }

    }

    return 0;
}

