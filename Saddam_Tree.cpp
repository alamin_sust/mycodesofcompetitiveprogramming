/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<bits/stdc++.h>
using namespace std;

vector<int>arr[100010],roots;

void rec(int par,int node)      // DFS traverse
{
    if(par==-1)                    // par is the parent and node is the child
        printf("%d  (parent : none(it's a root))\n",node);
    else
        printf("%d  (parent : %d)\n",node,par);

    for(int i=0; i<arr[node].size(); i++)
        rec(node,arr[node][i]);
    return;
}


int main()
{
    int a,b,i;
    while(scanf("%d%d",&a,&b)==2)  // a child, b parent
    {
        if(b==-1)
            roots.push_back(a);
        else
            arr[b].push_back(a);
    }
    printf("\n");
    for(i=0; i<roots.size(); i++)
    {
        printf("<a new tree starts here>\n");
        rec(-1,roots[i]);             // call roots
        printf("<the tree ends here>\n\n");
    }

    return 0;
}

/* sample input
50 20
20 10
10 -1
60 30
90 60
210 -1
150 40
100 60
110 60
70 30
30 10
200 160
40 -1
300 160
160 40
*/





