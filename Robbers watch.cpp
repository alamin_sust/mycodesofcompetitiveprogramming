/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


ll dp[30][260][2][2],n,m,hh,mm,arrhh[110],arrmm[110];

ll rec(ll pos,ll mask,ll flag1,ll flag2)
{
    if(pos>=(hh+mm))
        return 1LL;
    ll &ret=dp[pos][mask][flag1][flag2];
    if(ret!=-1LL)
        return ret;
    ret=0LL;
    if(pos<hh)
    {
        if(flag1==0LL)
        {
            for(ll i=0LL;i<arrhh[pos];i++)
            {
                if(((1LL<<i)&mask)==0LL)
                ret+=rec(pos+1,((1LL<<i)|mask),1LL,flag2);
            }
            if(((1LL<<arrhh[pos])&mask)==0LL)
            ret+=rec(pos+1,((1LL<<arrhh[pos])|mask),0LL,flag2);
        }
        else
        {
            for(ll i=0LL;i<7LL;i++)
            {
                if(((1LL<<i)&mask)==0LL)
                ret+=rec(pos+1,((1LL<<i)|mask),1LL,flag2);
            }
        }

    }
    else
    {
        if(flag2==0LL)
        {
            for(ll i=0LL;i<arrmm[pos-hh];i++)
            {
                if(((1LL<<i)&mask)==0LL)
                ret+=rec(pos+1,((1LL<<i)|mask),flag1,1LL);
            }
            if(((1LL<<arrmm[pos-hh])&mask)==0LL)
            ret+=rec(pos+1,((1LL<<arrmm[pos-hh])|mask),flag1,0LL);
        }
        else
        {
            for(ll i=0LL;i<7LL;i++)
            {
                if(((1LL<<i)&mask)==0LL)
                ret+=rec(pos+1,((1LL<<i)|mask),flag1,1LL);
            }
        }
    }
    return ret;
}


int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    ll i,k;
    scanf(" %I64d %I64d",&n,&m);

    memset(dp,-1LL,sizeof(dp));

    hh=1LL;
    mm=1LL;
    for(i=0LL,k=1LL;;k*=7LL,i++)
    {
        if(n<=k)
        {
            hh=i;
            break;
        }
    }
    for(i=0,k=1LL;;k*=7LL,i++)
    {
        if(m<=k)
        {
            mm=i;
            break;
        }
    }
    if(hh<1LL)hh=1LL;
    if(mm<1LL)mm=1LL;

    ll tpn,tpm;
    n--;
    m--;
    tpn=n;
    tpm=m;

    for(i=hh-1LL;i>=0LL;i--)
    {
         arrhh[i]=tpn%7LL;
         tpn/=7LL;
    }
    for(i=mm-1LL;i>=0LL;i--)
    {
         arrmm[i]=tpm%7LL;
         tpm/=7LL;
    }




    printf("%I64d\n",rec(0LL,0LL,0LL,0LL));

    return 0;
}






