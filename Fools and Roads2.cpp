/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node
{
    int val,x;
};
node lev[100010];

int comp(node a,node b)
{
    return a.val>b.val;
}
queue<int>q;
pair<int,int>pii;
map<pair<int,int>,int>mpp;
vector<int>adj[100010];
int val[100010];
int par[100010];
int dp[100010][22],res[100010];

void rec(int node,int num)
{
    //printf("%d....%d\n",node,num);
    int &ret=dp[node][num];
    if(ret!=0)
        return;
    if(num==0)
    {
        dp[node][num]=par[node];
        return;
    }
    ret=0;
    rec(node,num-1);
    if(dp[node][num-1]>0)
        rec(dp[node][num-1],num-1);
    ret=dp[dp[node][num-1]][num-1];
    return;
}

void build(int u)
{
    lev[u].val=1;
    q.push(u);
    //for(int i=1;i<=17;i++)

    //  dp[u][i]=dp[dp[u][i-1]][i-1];
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(int i=0; i<adj[u].size(); i++)
        {
            int v=adj[u][i];
            if(lev[v].val==0)
            {
                lev[v].val=lev[u].val+1;
                par[v]=u;
                q.push(v);
            }
        }
    }
    return;
}

main()
{
    int n;
    cin>>n;
    for(int i=1; i<n; i++)
    {
        scanf(" %d %d",&pii.first,&pii.second);
        mpp[pii]=i;
        swap(pii.first,pii.second);
        mpp[pii]=i;
        adj[pii.first].push_back(pii.second);
        adj[pii.second].push_back(pii.first);
    }
    build(1);
    for(int i=1; i<=n; i++)
    {
        lev[i].x=i;
        rec(i,17);
    }
    int m;
    cin>>m;
    for(int i=1; i<=m; i++)
    {
        int u,v;
        scanf(" %d %d",&u,&v);
        val[u]++;
        val[v]++;
        if(lev[u].val<lev[v].val)
            swap(u,v);
        for(int j=17; j>=0; j--)
        {
            if(lev[dp[u][j]].val>=lev[v].val)
            {
                u=dp[u][j];
            }
        }
        if(u==v)
        {
            val[u]-=2;
            continue;
        }
        for(int j=17; j>=0; j--)
        {
            if(dp[u][j]!=dp[v][j])
            {
                u=dp[u][j];
                v=dp[v][j];
            }
        }
        val[dp[u][0]]-=2;

    }
    sort(lev+1,lev+1+n,comp);
    for(int i=1; i<n; i++)
    {
        pii.first=lev[i].x;
        pii.second=par[lev[i].x];
        res[mpp[pii]]+=val[lev[i].x];
        val[par[lev[i].x]]+=val[lev[i].x];
    }
    for(int i=1; i<n; i++)
        printf("%d ",res[i]);
    printf("\n");
    return 0;
}


