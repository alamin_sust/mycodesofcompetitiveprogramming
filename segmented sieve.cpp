#include<bits/stdc++.h>
using namespace std;
int divi[200002];
bool isprime[100000];
void init()
{
    int i,j;
    for(i=2; i<=1000; i++)
        if(isprime[i]==0)
            for(j=i*i; j<=100000; j+=i)
                isprime[j]=1;
}
main()
{
    int t,i,a,b,j;
    cin>>t;
    init();
    while(t--)
    {
        cin>>a>>b;
        for(i=a; i<=b; i++)
            divi[i-a]=0;
        for(i=1; i*i<=b; i++)
        {
            int sq=i*i;
            for(j=((a-1)/i+1)*i; j<=b; j+=i)
            {
                if(j<sq)
                    continue;
                if(j==sq)
                    divi[j-a]+=1;
                else
                    divi[j-a]+=2;
            }
        }
        int c=0;
        for(i=a; i<=b; i++)
            if(isprime[divi[i-a]]==0)
                c++;
        cout<<c<<endl;
    }

}
