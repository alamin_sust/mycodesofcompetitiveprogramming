#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class ElectronicPetEasy
{
public:
	string isDifficult(int st1, int p1, int t1, int st2, int p2, int t2)
	{
	    int i=st1,j=st2,c1=0,c2=0,flag=0;
	    for(;c1<t1&&c2<t2;)
        {
            if(i==j)
                {flag=1;
                break;}
            if(i<j)
                i+=p1,c1++;
            else
                j+=p2,c2++;
        }
        if(flag==1)
            return "Difficult";
        else
            return "Easy";
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	ElectronicPetEasy objectElectronicPetEasy;

	//test case0
	int param00 = 3;
	int param01 = 3;
	int param02 = 3;
	int param03 = 5;
	int param04 = 2;
	int param05 = 3;
	string ret0 = objectElectronicPetEasy.isDifficult(param00,param01,param02,param03,param04,param05);
	string need0 = "Difficult";
	assert_eq(0,ret0,need0);

	//test case1
	int param10 = 3;
	int param11 = 3;
	int param12 = 3;
	int param13 = 5;
	int param14 = 2;
	int param15 = 2;
	string ret1 = objectElectronicPetEasy.isDifficult(param10,param11,param12,param13,param14,param15);
	string need1 = "Easy";
	assert_eq(1,ret1,need1);

	//test case2
	int param20 = 1;
	int param21 = 4;
	int param22 = 7;
	int param23 = 1;
	int param24 = 4;
	int param25 = 7;
	string ret2 = objectElectronicPetEasy.isDifficult(param20,param21,param22,param23,param24,param25);
	string need2 = "Difficult";
	assert_eq(2,ret2,need2);

	//test case3
	int param30 = 1;
	int param31 = 1000;
	int param32 = 1000;
	int param33 = 2;
	int param34 = 1000;
	int param35 = 1000;
	string ret3 = objectElectronicPetEasy.isDifficult(param30,param31,param32,param33,param34,param35);
	string need3 = "Easy";
	assert_eq(3,ret3,need3);

	//test case4
	int param40 = 1;
	int param41 = 1;
	int param42 = 1;
	int param43 = 2;
	int param44 = 2;
	int param45 = 2;
	string ret4 = objectElectronicPetEasy.isDifficult(param40,param41,param42,param43,param44,param45);
	string need4 = "Easy";
	assert_eq(4,ret4,need4);
   return 0;
}
