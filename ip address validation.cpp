/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int n,i,p,now,dt,dot,l,flag,flag2,colon,lst,fg;
char arr[1010];

int main()
{
    scanf(" %d",&n);
    getchar();
    for(p=1; p<=n; p++)
    {
        gets(arr);
        l=strlen(arr);
        dot=colon=0;
        flag=flag2=0;
        for(i=0; i<l; i++)
        {
            if(arr[i]>='a'&&arr[i]<='f')
                flag2=1;
            if(!((arr[i]>='0'&&arr[i]<='9')||(arr[i]>='a'&&arr[i]<='f')||arr[i]=='.'||arr[i]==':'))
                flag=1;
            if(arr[i]=='.')
                dot++;
            if(arr[i]==':')
                colon++;
        }
        if(flag)
        {
            printf("Neither\n");
        }
        else if(dot==3&&colon==0)
        {
            if(flag2)
                printf("Neither\n");
            else
            {
                arr[l]='.';
                l++;
                dt=0;
                for(i=0; i<l; i++)
                {
                    now=0;
                    while(i<l&&arr[i]!='.')
                    {
                        if(now<1000000)
                        now=now*10+arr[i]-'0';
                        i++;
                    }
                    if(now<=255)
                        dt++;
                }
                if(dt==4)
                    printf("IPv4\n");
                else
                    printf("Neither\n");
            }
        }
        else if(colon==7&&dot==0)
        {
            arr[l]=':';
            l++;
            lst=-1;
            fg=0;
            for(i=0; i<l; i++)
            {
                if(arr[i]==':')
                {
                    if((i-lst)>5||(i-lst)<2)
                            {fg=1;
                            break;}
                    lst=i;
                }
            }
            if(fg==0)
                printf("IPv6\n");
            else
                printf("Neither\n");
        }
        else
        {
            printf("Neither\n");
        }
    }
    return 0;
}



/*
3
This line has junk text.
121.18.19.20
2001:0db8:0000:0000:0000:ff00:0042:8329
*/
