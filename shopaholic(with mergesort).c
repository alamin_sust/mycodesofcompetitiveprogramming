#include<stdio.h>
int arr[20010];

void mergesort(int left,int right)
{
    int mid=(left+right)/2;

    if(left<right)
    {
        mergesort(left,mid);
        mergesort(mid+1,right);
        merge(left,mid,right);
    }
    return;
}

void merge(int left,int mid,int right)
{
     int i,temparr[right-left+1];
     int pos=0,lpos=left,rpos=mid+1;

     while(lpos<=mid&&rpos<=right)
     {
         if(arr[lpos]<arr[rpos])
         temparr[pos++]=arr[lpos++];
         else
         temparr[pos++]=arr[rpos++];
     }
     while(lpos<=mid)
     temparr[pos++]=arr[lpos++];
     while(rpos<=right)
     temparr[pos++]=arr[rpos++];
     for(i=0;i<pos;i++)
     arr[left+i]=temparr[i];
     return;

}

main()
{
    int i,j,t,n,res;
    scanf(" %d",&t);
    for(i=0;i<t;i++)
    {
        scanf(" %d",&n);
        for(j=0;j<n;j++)
        scanf(" %d",&arr[j]);
        mergesort(0,n-1);
        for(res=0,j=n-3;j>=0;j-=3)
        {
            res+=arr[j];
        }
        printf("%d\n",res);
    }
    return 0;
}
