/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

priority_queue<int> pq1,pq2,pq3,pq4;
int n;

int rec(priority_queue<int> pq1,priority_queue<int> pq2,priority_queue<int> pq3,priority_queue<int> pq4,int l)
{
    int ret=99999999;
    if(pq1.size()==n)
    return 0;
    if(l>10)
        return ret;
    if(pq1.size()>0)
    {
        if(pq2.size()>=0&&pq1.top()<pq2.top())
        {
            int tp=pq1.top();
            pq1.pop();
            pq2.push(tp);
            ret=min(ret,1+rec(pq1,pq2,pq3,pq4,l+1));
            pq2.pop();
            pq1.push(tp);
        }
        if(pq3.size()>=0&&pq1.top()<pq3.top())
        {
            int tp=pq1.top();
            pq1.pop();
            pq3.push(tp);
            ret=min(ret,1+rec(pq1,pq2,pq3,pq4,l+1));
            pq3.pop();
            pq1.push(tp);
        }
        if(pq4.size()>=0&&pq1.top()<pq4.top())
        {
            int tp=pq1.top();
            pq1.pop();
            pq4.push(tp);
            ret=min(ret,1+rec(pq1,pq2,pq3,pq4,l+1));
            pq4.pop();
            pq1.push(tp);
        }
    }
    if(pq2.size()>0)
    {
        if(pq1.size()>=0&&pq2.top()<pq1.top())
        {
            int tp=pq2.top();
            pq2.pop();
            pq1.push(tp);
            ret=min(ret,1+rec(pq1,pq2,pq3,pq4,l+1));
            pq1.pop();
            pq2.push(tp);
        }
        if(pq3.size()>=0&&pq2.top()<pq3.top())
        {
            int tp=pq2.top();
            pq2.pop();
            pq3.push(tp);
            ret=min(ret,1+rec(pq1,pq2,pq3,pq4,l+1));
            pq3.pop();
            pq2.push(tp);
        }
        if(pq4.size()>=0&&pq2.top()<pq4.top())
        {
            int tp=pq2.top();
            pq2.pop();
            pq4.push(tp);
            ret=min(ret,1+rec(pq1,pq2,pq3,pq4,l+1));
            pq4.pop();
            pq2.push(tp);
        }
    }
    if(pq3.size()>0)
    {
        if(pq1.size()>=0&&pq3.top()<pq1.top())
        {
            int tp=pq3.top();
            pq3.pop();
            pq1.push(tp);
            ret=min(ret,1+rec(pq1,pq2,pq3,pq4,l+1));
            pq1.pop();
            pq3.push(tp);
        }
        if(pq2.size()>=0&&pq3.top()<pq2.top())
        {
            int tp=pq3.top();
            pq3.pop();
            pq2.push(tp);
            ret=min(ret,1+rec(pq1,pq2,pq3,pq4,l+1));
            pq2.pop();
            pq3.push(tp);
        }
        if(pq4.size()>=0&&pq3.top()<pq4.top())
        {
            int tp=pq3.top();
            pq3.pop();
            pq4.push(tp);
            ret=min(ret,1+rec(pq1,pq2,pq3,pq4,l+1));
            pq4.pop();
            pq3.push(tp);
        }
    }
    if(pq4.size()>0)
    {
        if(pq1.size()>=0&&pq4.top()<pq1.top())
        {
            int tp=pq4.top();
            pq4.pop();
            pq1.push(tp);
            ret=min(ret,1+rec(pq1,pq2,pq3,pq4,l+1));
            pq1.pop();
            pq4.push(tp);
        }
        if(pq2.size()>=0&&pq4.top()<pq2.top())
        {
            int tp=pq4.top();
            pq4.pop();
            pq2.push(tp);
            ret=min(ret,1+rec(pq1,pq2,pq3,pq4,l+1));
            pq2.pop();
            pq4.push(tp);
        }
        if(pq3.size()>=0&&pq4.top()<pq3.top())
        {
            int tp=pq4.top();
            pq4.pop();
            pq3.push(tp);
            ret=min(ret,1+rec(pq1,pq2,pq3,pq4,l+1));
            pq3.pop();
            pq4.push(tp);
        }
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    int in;
    scanf(" %d",&n);

    for(int i=1;i<=n;i++)
    {
        scanf(" %d",&in);
        if(in==1)
        pq1.push(i);
        else if(in==2)
        pq2.push(i);
        else if(in==3)
        pq3.push(i);
        else if(in==4)
        pq4.push(i);
    }

    printf("%d\n",rec(pq1,pq2,pq3,pq4,0));

    return 0;
}

