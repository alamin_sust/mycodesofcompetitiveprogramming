/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 1000000010
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int t,p,n,m,i,j,k,adj[103][103],flag[103][103],from[10010],to[10010],dist[10010];

int main()
{
    freopen("C-small-practice.in","r",stdin);
    freopen("C-small-practice.out","w",stdout);


    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {
        scanf(" %d %d",&n,&m);
        for(i=0; i<n; i++)
            for(j=0; j<n; j++)
                adj[i][j]=inf;
        for(i=0; i<m; i++)
        {
            scanf(" %d %d %d",&from[i],&to[i],&dist[i]);
            adj[from[i]][to[i]]=adj[to[i]][from[i]]=dist[i];
        }
        memset(flag,0,sizeof(flag));
        for(k=0; k<n; k++)
        {
            for(i=0; i<n; i++)
            {
                for(j=0; j<n; j++)
                {
                    if(i==j)
                        continue;
                    if(adj[i][j]>adj[i][k]+adj[k][j])
                    {
                        adj[i][j]=adj[i][k]+adj[k][j];
                       if(adj[i][j]!=inf)
                        {
                        flag[i][j]--;
                        //flag[j][i]--;
                        }
                        flag[i][k]++;
                        //flag[k][i]++;
                        flag[k][j]++;
                        //flag[j][k]++;
                    }
                }
            }
        }
        printf("Case #%d:\n",p);
        for(i=0;i<m;i++)
        {
           // printf("%d %d %d %d\n",from[i],to[i],flag[from[i]][to[i]],flag[to[i]][from[i]]);
            if(flag[from[i]][to[i]]>0||flag[to[i]][from[i]]>0)
                continue;
            printf("%d\n",i);
        }

    }
    return 0;
}


