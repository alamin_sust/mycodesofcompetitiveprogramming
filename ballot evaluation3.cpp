#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#include<map>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int l;


char arr[100010];

int func(int i)
{
    char ar[10];
    int ret=0,k=l-1,d=1,ff=0;
    while(k>=i)
    {
        if(arr[k]==' ')
        {
            k--;
            continue;
        }
        if(arr[k]=='.')
        {
            ff=1;
            continue;
        }
        ret+=(arr[k]-'0')*d;
        d*=10;
        k--;
    }
    if(ff==0)
        ret*=10;
    return ret;
}

main()
{
    int n,m,i,v,res,j,gt,fg,k,dd,p;
    double val;
    while(cin>>n>>m)
    {
        map<string,int>mpp;
        getchar();
        char ar[110];
        for(i=1; i<=n; i++)
        {
            scanf(" %s %lf",ar,&val);
            v=int(val*10+0.5);
            mpp[ar]=v;
        }
        getchar();
        for(i=1; i<=m; i++)
        {
            gets(arr);
            l=strlen(arr);
            char a[60];
            string st;
            for(int kk=0; kk<60; kk++)
                a[kk]='\0';
            for(res=0,j=0,k=0; j<l; j++)
            {
                if(arr[j]==' '||arr[j]=='+'||arr[j]=='<'||arr[j]=='>'||arr[j]=='=')
                {
                    while(arr[j]==' '||arr[j]=='+')
                        j++;
                    st=a;
                    res+=mpp[st],k=0;
                    for(int kk=0; kk<60; kk++)
                        a[kk]='\0';
                    if(arr[j]=='<'||arr[j]=='>'||arr[j]=='=')
                        break;
                    j--;
                }
                else
                    a[k++]=arr[j];
            }
            fg=0;
            if(arr[j]=='<')
            {
                if(arr[j+1]=='=')
                {
                    gt=func(j+2);
                    if(res<=gt) fg=1;
                }
                else
                {
                    gt=func(j+1);
                    if(res<gt) fg=1;
                }
            }
            else if(arr[j]=='>')
            {
                if(arr[j+1]=='=')
                {
                    gt=func(j+2);
                    if(res>=gt) fg=1;
                }
                else
                {
                    gt=func(j+1);
                    if(res>gt) fg=1;
                }
            }
            else
            {
                gt=func(j+1);
                if(res==gt) fg=1;
            }
            printf("Guess #%d was ",i);
            if(fg)
                printf("correct.\n");
            else
                printf("incorrect.\n");
        }
    }
    return 0;
}

/*
6 5
CDU 30.7
SPD 20.8
Gruene 12.1
FDP 11.0
DIELINKE 7.5
CSU 7.2
FDP>11
CDU+SPD<50
SPD+CSU>=28
FDP+SPD+CDU<=42
CDU+FDP+SPD+DIELINKE=70
*/
