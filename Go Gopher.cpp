/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
int rr[]= {0,0,0,-1,1,1,1,-1,-1};
int cc[]= {0,-1,1,0,0,1,-1,1,-1};
using namespace std;

int t,p,n,a,arr[110][110],x,y,currX,currY,flag,i,j,k;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {
        scanf(" %d",&a);
        n = sqrt(a);
        if((n*n)<a)
            n++;

        n-=2;
        n=max(1,n);

        memset(arr,0,sizeof(arr));

        flag=0;

        while(flag==0)
        {
            for(i=0; i<n&&flag==0; i++)
            {
                for(j=0; j<n&&flag==0; j++)
                {
                    currX=i+50;
                    currY=j+50;
                    for(k=0; k<9&&flag==0; k++)
                    {
                        if(arr[currX+rr[k]][currY+cc[k]]==0)
                        {
                            printf("%d %d\n",currX,currY);
                            fflush(stdout);
                            scanf("%d %d",&x,&y);
                            if(x==0&&y==0)
                            {
                                flag=1;
                            }
                            else
                            {
                                arr[x][y]=1;
                            }
                            break;
                        }
                    }
                }
            }
        }

    }


    return 0;
}
