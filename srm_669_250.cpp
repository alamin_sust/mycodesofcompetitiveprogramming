#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

map<string,int>mpp;

class LiveConcert
{
public:
	int maxHappiness(vector <int> h, vector <string> s)
	{
	    mpp.clear();
	    int res=0;
	    for(int i=0;i<s.size();i++)
        {
            res+=max(0,(h[i]-mpp[s[i]]));
            mpp[s[i]]=max(mpp[s[i]],h[i]);
        }
        return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	LiveConcert objectLiveConcert;

	//test case0
	vector <int> param00;
	param00.push_back(10);
	param00.push_back(5);
	param00.push_back(6);
	param00.push_back(7);
	param00.push_back(1);
	param00.push_back(2);
	vector <string> param01;
	param01.push_back("ciel");
	param01.push_back("bessie");
	param01.push_back("john");
	param01.push_back("bessie");
	param01.push_back("bessie");
	param01.push_back("john");
	int ret0 = objectLiveConcert.maxHappiness(param00,param01);
	int need0 = 23;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(3);
	param10.push_back(3);
	param10.push_back(4);
	param10.push_back(3);
	param10.push_back(3);
	vector <string> param11;
	param11.push_back("a");
	param11.push_back("a");
	param11.push_back("a");
	param11.push_back("a");
	param11.push_back("a");
	int ret1 = objectLiveConcert.maxHappiness(param10,param11);
	int need1 = 4;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(1);
	param20.push_back(2);
	param20.push_back(3);
	param20.push_back(4);
	param20.push_back(5);
	param20.push_back(6);
	param20.push_back(7);
	param20.push_back(8);
	param20.push_back(9);
	param20.push_back(10);
	param20.push_back(100);
	vector <string> param21;
	param21.push_back("a");
	param21.push_back("b");
	param21.push_back("c");
	param21.push_back("d");
	param21.push_back("e");
	param21.push_back("e");
	param21.push_back("d");
	param21.push_back("c");
	param21.push_back("b");
	param21.push_back("a");
	param21.push_back("abcde");
	int ret2 = objectLiveConcert.maxHappiness(param20,param21);
	int need2 = 140;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(100);
	vector <string> param31;
	param31.push_back("oyusop");
	int ret3 = objectLiveConcert.maxHappiness(param30,param31);
	int need3 = 100;
	assert_eq(3,ret3,need3);

	//test case4
	vector <int> param40;
	param40.push_back(100);
	param40.push_back(100);
	param40.push_back(100);
	param40.push_back(100);
	param40.push_back(100);
	param40.push_back(100);
	param40.push_back(100);
	param40.push_back(100);
	param40.push_back(100);
	param40.push_back(100);
	param40.push_back(100);
	param40.push_back(100);
	param40.push_back(100);
	vector <string> param41;
	param41.push_back("haruka");
	param41.push_back("chihaya");
	param41.push_back("yayoi");
	param41.push_back("iori");
	param41.push_back("yukiho");
	param41.push_back("makoto");
	param41.push_back("ami");
	param41.push_back("mami");
	param41.push_back("azusa");
	param41.push_back("miki");
	param41.push_back("hibiki");
	param41.push_back("takane");
	param41.push_back("ritsuko");
	int ret4 = objectLiveConcert.maxHappiness(param40,param41);
	int need4 = 1300;
	assert_eq(4,ret4,need4);

}

