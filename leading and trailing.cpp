#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll bigmod(ll n,ll k)
{
    if(k==0)
        return 1;
    if(k%2==1)
        return ((n%1000)*(bigmod(n,k-1)%1000))%1000;
    else
    {
        ll a=bigmod(n,k/2)%1000;
        return (a*a)%1000;
    }
}

main()
{
    ll t,p,n,k,res2;
    double res1;
    cin>>t;
    for(p=1; p<=t; p++)
    {
        scanf(" %lld %lld",&n,&k);
        res1=(double)k*log10((double)n);
        res1= 100.0 * pow(10.0, res1-floor(res1));
        res2=bigmod(n,k);
        printf("%03lld...%03lld\n",(ll)res1,res2);
    }
    return 0;
}

