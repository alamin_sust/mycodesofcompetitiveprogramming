#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int leap(int mon)
{
    if((mon%4)==0&&(mon%400==0||(mon%100)!=0))
     return 1;
    else
    return 0;
}

main()
{
    int p,i,y,d,k,res,n;
    char month[16],in[110];
    int arr[]={0,31,28,31,30,31,30,31,31,30,31,30,31};
    char str[16][16]={"#","JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"};
    cin>>n;
    getchar();
    for(p=1;p<=n;p++)
    {
        gets(in);
        //scanf(" %d-%s%d",&d,month,&y);
        d=(in[1]-'0')+((in[0]-'0')*10);
        month[0]=in[3];
        month[1]=in[4];
        month[2]=in[5];
        y=(in[10]-'0')+((in[9]-'0')*10)+((in[8]-'0')*100)+((in[7]-'0')*1000);
        if(leap(y))
            arr[2]=29;
        else
            arr[2]=28;
        res=d;
        for(i=1;i<=12;i++)
        {
            if(strcmp(str[i],month)==0)
                {k=i;
                break;}
        }
        for(i=1;i<k;i++)
            res+=arr[i];
        if(res<=300)
        printf("Case %d: %d Hundreds\n",p,res);
        else if(res<=360)
        printf("Case %d: %d Tens\n",p,res-300);
        else
        printf("Case %d: %d Ones\n",p,res-360);
    }
    return 0;
}
/*
3
01-JAN-1900
10-JAN-1900
16-DEC-1900
*/
