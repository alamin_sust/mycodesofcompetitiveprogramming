/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


int row,col,i,j,swp,res,flag[110],fg[110];
char arr[110][110];

int main()
{
    cin>>row>>col;
    for(i=0; i<row; i++)
        scanf(" %s",&arr[i]);
    if(row==1)
    {
        printf("0\n");
        return 0;
    }
    for(i=0; i<col; i++)
    {
        for(j=0,swp=0; j<row-1; j++)
        {
            if(flag[j]==2)
            {
                fg[j]=2;
                continue;
            }
            else if(flag[j]==0)
            {
                if(arr[j][i]>arr[j+1][i])
                {
                    swp=1;
                    res++;
                    break;
                }
                else if(arr[j][i]==arr[j+1][i])
                {
                    fg[j]=1;
                }
                else
                {
                    fg[j]=2;
                }
            }
            else
            {
                if(arr[j][i]>arr[j+1][i])
                {
                    swp=1;
                    res++;
                    break;
                }
                else if(arr[j][i]==arr[j+1][i])
                {
                    fg[j]=1;
                }
                else
                {
                    fg[j]=2;
                }
            }
        }
        if(swp==1)
            continue;
        for(j=0;j<row-1;j++)
        {
            flag[j]=fg[j];
        }
    }
    printf("%d\n",res);
    return 0;
}
