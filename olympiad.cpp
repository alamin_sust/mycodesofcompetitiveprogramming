/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll n,k,i,arr[100010],arr2[100010],j,res;

main()
{
    cin>>n>>k;
    for(i=0;i<n;i++)
        scanf(" %I64d",&arr[i]);
    for(i=0;i<n;i++)
        scanf(" %I64d",&arr2[i]);
    sort(arr,arr+n);
    sort(arr2,arr2+n);
    for(i=n-1;j<n&&i>=0;)
    {
        if((arr[i]+arr2[j])>=k)
        {
            res++;
            j++;
            i--;
        }
        else
            j++;
    }
    printf("1 %I64d\n",res);
    return 0;
}


