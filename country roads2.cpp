#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

struct node
{
    int x;
    int value;
};
node det;
priority_queue<node>pq;
bool operator<(node a,node b)
{
    return a.value>b.value;
}


int d[510],stat[510],n;
vector<int>adj[510];
vector<int>val[510];


void dijkstra(int from)
{
    det.x=from;
    det.value=0;
    d[from]=0;
    pq.push(det);
    while(!pq.empty())
    {
        int xx=pq.top().x;
        int vv=pq.top().value;
        pq.pop();
        stat[xx]=1;
        int k=adj[xx].size();
        for(int i=0; i<k; i++)
        {
            int v=adj[xx][i];
            if(stat[v]==0)
            {
                if(d[v]>max(d[xx],val[xx][i]))
                {
                    d[v]=max(d[xx],val[xx][i]);
                    det.x=v;
                    det.value=d[v];
                    pq.push(det);
                }
            }
        }
    }
    return;
}

main()
{
    int t,p,m,from,to,cost,i;
    cin>>t;
    for(p=1; p<=t; p++)
    {
        cin>>n>>m;
        for(i=0; i<n; i++)
        {
            d[i]=inf;
            stat[i]=0;
            adj[i].clear();
            val[i].clear();
        }
        for(i=0; i<m; i++)
        {
            scanf(" %d %d %d",&from,&to,&cost);
            adj[from].push_back(to);
            adj[to].push_back(from);
            val[from].push_back(cost);
            val[to].push_back(cost);
        }
        cin>>from;
        dijkstra(from);
        printf("Case %d:\n",p);
        for(i=0; i<n; i++)
            if(d[i]==inf)
                printf("Impossible\n");
            else
                printf("%d\n",d[i]);
    }
    return 0;
}

