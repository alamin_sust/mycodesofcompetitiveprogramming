#include<stdio.h>
#include<string.h>
int n,c[110],coin[110],target,dp[53][1010][23];
int rec(int i,int amount,int taken)
{
    if(i>=n)
    {
        if(amount==0)
            return 1;
        else
            return 0;
    }
    if(dp[i][amount][taken]!=-1)
        return dp[i][amount][taken];
    int ret1=0,ret2=0;
    if(amount-coin[i]>=0&&taken<c[i])
        ret1=rec(i,amount-coin[i],taken+1);
    ret2=rec(i+1,amount,0);
    return dp[i][amount][taken]=(ret1+ret2)%100000007;

}
int main()
{
    int i,j,k,t,p;
    scanf(" %d",&t);
    for(p=1; p<=t; p++)
    {
        scanf(" %d %d",&n,&target);
        for(i=0; i<=n; i++)
        {
            for(j=0; j<=target; j++)
            {
                for(k=0; k<=20; k++)
                    dp[i][j][k]=-1;
            }
        }
        for(i=0; i<n; i++)
        {
            scanf(" %d",&coin[i]);
        }
        for(i=0; i<n; i++)
        {
            scanf(" %d",&c[i]);
        }
        printf("Case %d: %d\n",p,rec(0,target,0));
    }

    return 0;
}
