#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

int adj[22][22],stat[22],val[22];

int bfs(int from,int to)
{
    queue<int>q;
    q.push(from);
    stat[from]=1;
    while(!q.empty())
    {
       int pr=q.front();
        q.pop();
        for(int i=1;i<=20;i++)
        {
            if(stat[i]==0&&adj[pr][i]==1)
            {
                stat[i]=1;
                q.push(i);
                val[i]=val[pr]+1;
                if(i==to)
                    return val[i];
            }
        }
    }
    return 0;
}

main()
{
    int p=1,n,i,in,from,to,j,res;
    while(cin>>n)
    {
        memset(adj,0,sizeof(adj));
        for(i=1;i<=n;i++)
            {scanf("%d",&in);
            adj[in][1]=1;
            adj[1][in]=1;}
        for(i=2;i<=19;i++)
        {
            cin>>n;
            for(j=1;j<=n;j++)
            {
            scanf("%d",&in);
            adj[in][i]=1;
            adj[i][in]=1;
            }
        }
        cin>>n;
        printf("Test Set #%d\n",p++);
        for(i=1;i<=n;i++)
        {
            memset(val,0,sizeof(val));
            memset(stat,0,sizeof(stat));
            cin>>from>>to;
            res=bfs(from,to);
            printf("%2d to %2d: %d\n",from,to,res);
        }
        printf("\n");
    }
    return 0;
}
