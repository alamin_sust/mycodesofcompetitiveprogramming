/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

main()
{
    ll n,m,now,arr[100010],i,d;
    char ch;
    cin>>n>>m;
    for(i=0;i<n;i++)
    {
        scanf(" %lld",&arr[i]);
    }
    for(i=0;i<m;i++)
    {
        now+=n;
        scanf(" %c %lld",&ch,&d);
        if(ch=='R')
            printf("%lld\n",arr[(now+d-1)%n]);
        else if(ch=='C')
        {
        now=now+d;
        }
        else
        now=now-d;
    }
    return 0;
}

