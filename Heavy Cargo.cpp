#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int n,m,p,t=1,k,adj[210][210],fg,i,j,x,y,cost;
char a1[33],a2[33],arr[210][33];

main()
{
    while(SFd2(n,m)==2&&n&&m)
    {

        for(i=1; i<=n; i++)
            for(j=1; j<=n; j++)
                adj[i][j]=-1;
        k=0;
        for(p=1; p<=m; p++)
        {
            scanf(" %s %s %d",a1,a2,&cost);
            fg=0;
            for(i=1; i<=k; i++)
            {
                if(strcmp(a1,arr[i])==0)
                {
                    x=i;
                    fg=1;
                    break;
                }
            }
            if(fg==0)
            {
                x=++k;
                strcpy(arr[k],a1);
            }
            fg=0;
            for(i=1; i<=k; i++)
            {
                if(strcmp(a2,arr[i])==0)
                {
                    y=i;
                    fg=1;
                    break;
                }
            }
            if(fg==0)
            {
                y=++k;
                strcpy(arr[k],a2);
            }
            adj[x][y]=adj[y][x]=cost;
        }
        scanf(" %s %s",a1,a2);
        for(k=1; k<=n; k++)
            for(i=1; i<=n; i++)
                for(j=1; j<=n; j++)
                    if(adj[i][k]!=-1&&adj[k][j]!=-1&&adj[i][j]<min(adj[i][k],adj[k][j]))
                        adj[i][j]=min(adj[i][k],adj[k][j]);
        for(i=1; i<=k; i++)
        {
            if(strcmp(a1,arr[i])==0)
            {
                x=i;
                break;
            }
        }
        for(i=1; i<=k; i++)
        {
            if(strcmp(a2,arr[i])==0)
            {
                y=i;
                break;
            }
        }
        printf("Scenario #%d\n%d tons\n\n",t++,adj[x][y]);
    }
    return 0;
}
