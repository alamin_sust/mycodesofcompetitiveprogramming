#include<stdio.h>
static char sieve[1000001];
unsigned long reverse(unsigned long x)
{
         unsigned long r=0;
         do
         {
                  r=r*10+(x%10);
                  x/=10;
         }while(x);
         return r;
}

int main()
{
    unsigned long int i,j,n;
    sieve[0]=sieve[1]=0;
    for(i=2; i<1000001; i++) sieve[i]=1;
    for(i=2; i<1001; i++)
    {
             if(sieve[i]!=0)
             {
                              for(j=i+1; j<1000001; j++)
                              {
                                         if (sieve[j]!=0)
                                         {
                                                           if((j%i)==0) sieve[j]=0;
                                         }
                              }
             }
    }
    while(scanf("%lu",&n)!=EOF)
    {
                               if(sieve[n]==0) printf("%lu is not prime.\n",n);
                               else if(sieve[n]==1)
                               {
                                    if((sieve[reverse(n)]==1) && (n!=reverse(n))) printf("%lu is emirp.\n",n);
                                    else printf("%lu is prime.\n",n);
                               }
    }
    return 0;
}
