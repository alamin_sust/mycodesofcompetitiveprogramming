#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class ShufflingCardsDiv2
{
public:
    string shuffle(vector <int> permutation)
    {
        int i,flag=0,n,cnt=0,prs;
        n=permutation.size();
        n/=2;
        prs=n/2;
        sort(permutation.begin(),permutation.end()-n);
        for(i=0; i<(n-1); i++)
        {
            if(abs(permutation[i]-permutation[i+1])<=1)
            {
                cnt++;
            }
        }
        if(cnt<prs)
            flag=1;
        sort(permutation.begin()+n,permutation.end());
        n*=2;
        cnt=0;
        for(; i<(n-1); i++)
        {
            if(abs(permutation[i]-permutation[i+1])<=1)
            {
                cnt++;
            }
        }
        if(cnt<prs)
            flag=1;
        if(flag==1)
            return "Impossible";
        else return "Possible";
    }
};


template<typename T> void print( T a )
{
    cerr << a;
}

void print( long long a )
{
    cerr << a << "L";
}

void print( string a )
{
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a )
{
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ )
    {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need )
{
    if ( have == need )
    {
        cerr << "Case " << n << " passed." << endl;
    }
    else
    {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need )
{
    if( have.size() != need.size() )
    {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ )
    {
        if( have[i] != need[i] )
        {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need )
{
    if ( have == need )
    {
        cerr << "Case " << n << " passed." << endl;
    }
    else
    {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

    ShufflingCardsDiv2 objectShufflingCardsDiv2;

    //test case0
    vector <int> param00;
    param00.push_back(1);
    param00.push_back(2);
    param00.push_back(3);
    param00.push_back(4);
    string ret0 = objectShufflingCardsDiv2.shuffle(param00);
    string need0 = "Possible";
    assert_eq(0,ret0,need0);

    //test case1
    vector <int> param10;
    param10.push_back(4);
    param10.push_back(3);
    param10.push_back(2);
    param10.push_back(1);
    string ret1 = objectShufflingCardsDiv2.shuffle(param10);
    string need1 = "Possible";
    assert_eq(1,ret1,need1);

    //test case2
    vector <int> param20;
    param20.push_back(1);
    param20.push_back(3);
    param20.push_back(2);
    param20.push_back(4);
    string ret2 = objectShufflingCardsDiv2.shuffle(param20);
    string need2 = "Impossible";
    assert_eq(2,ret2,need2);

    //test case3
    vector <int> param30;
    param30.push_back(1);
    param30.push_back(4);
    param30.push_back(2);
    param30.push_back(5);
    param30.push_back(3);
    param30.push_back(6);
    string ret3 = objectShufflingCardsDiv2.shuffle(param30);
    string need3 = "Impossible";
    assert_eq(3,ret3,need3);

    //test case4
    vector <int> param40;
    param40.push_back(8);
    param40.push_back(5);
    param40.push_back(4);
    param40.push_back(9);
    param40.push_back(1);
    param40.push_back(7);
    param40.push_back(6);
    param40.push_back(10);
    param40.push_back(3);
    param40.push_back(2);
    string ret4 = objectShufflingCardsDiv2.shuffle(param40);
    string need4 = "Possible";
    assert_eq(4,ret4,need4);

}

