/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int n;
vector<int> one,two,three,four;

struct node
{
    vector<int> on,tw,th,fo;
};

queue<node>q;
node det,now;

map<string,int>val;

int fc(node nn)
{
    string str="";
    for(int i=0; i<nn.on.size(); i++)
        str+=(char)(nn.on[i]-1+'a');
    str+="0";
    for(int i=0; i<nn.tw.size(); i++)
        str+=(char)(nn.tw[i]-1+'a');
    str+="0";
    for(int i=0; i<nn.th.size(); i++)
        str+=(char)(nn.th[i]-1+'a');
    str+="0";
    for(int i=0; i<nn.fo.size(); i++)
        str+=(char)(nn.fo[i]-1+'a');
    return val[str];
}

string fc2(node nn)
{
    string str="";
    for(int i=0; i<nn.on.size(); i++)
        str+=(char)(nn.on[i]-1+'a');
    str+="0";
    for(int i=0; i<nn.tw.size(); i++)
        str+=(char)(nn.tw[i]-1+'a');
    str+="0";
    for(int i=0; i<nn.th.size(); i++)
        str+=(char)(nn.th[i]-1+'a');
    str+="0";
    for(int i=0; i<nn.fo.size(); i++)
        str+=(char)(nn.fo[i]-1+'a');
    return str;
}

int bfs(vector<int> on,vector<int> tw,vector<int> th,vector<int> fo)
{
    if(on.size()==n)
        return 0;

    det.on=on;
    det.tw=tw;
    det.th=th;
    det.fo=fo;
    q.push(det);
    val[fc2(det)]=1;
    while(!q.empty())
    {
        now=q.front();
        q.pop();
        int vv=val[fc2(now)];
        //printf("%d\n",now.on.size());
        if(now.on.size()>0)
        {
            if(now.tw.size()==0||(now.tw[now.tw.size()-1]>now.on[now.on.size()-1]))
            {
                int tp=now.on[now.on.size()-1];
                now.tw.push_back(tp);
                now.on.pop_back();
                if(fc(now)==0)
                {
                    q.push(now);
                    val[fc2(now)]=vv+1;
                    if(now.on.size()==n)
                        return vv+1;
                }
                now.tw.pop_back();
                now.on.push_back(tp);
            }
            if(now.th.size()==0||(now.th[now.th.size()-1]>now.on[now.on.size()-1]))
            {
                int tp=now.on[now.on.size()-1];
                now.th.push_back(tp);
                now.on.pop_back();
                if(fc(now)==0)
                {
                    q.push(now);
                    val[fc2(now)]=vv+1;
                    if(now.on.size()==n)
                        return vv+1;
                }
                now.th.pop_back();
                now.on.push_back(tp);
            }
            if(now.fo.size()==0||(now.fo[now.fo.size()-1]>now.on[now.on.size()-1]))
            {
                int tp=now.on[now.on.size()-1];
                now.fo.push_back(tp);
                now.on.pop_back();
                if(fc(now)==0)
                {
                    q.push(now);
                    val[fc2(now)]=vv+1;
                    if(now.on.size()==n)
                        return vv+1;
                }
                now.fo.pop_back();
                now.on.push_back(tp);
            }

        }
        if(now.tw.size()>0)
        {
            if(now.on.size()==0||(now.on[now.on.size()-1]>now.tw[now.tw.size()-1]))
            {
                int tp=now.tw[now.tw.size()-1];
                now.on.push_back(tp);
                now.tw.pop_back();
                if(fc(now)==0)
                {
                    q.push(now);
                    val[fc2(now)]=vv+1;
                    if(now.on.size()==n)
                        return vv+1;
                }
                now.on.pop_back();
                now.tw.push_back(tp);
            }
            if(now.th.size()==0||(now.th[now.th.size()-1]>now.tw[now.tw.size()-1]))
            {
                int tp=now.tw[now.tw.size()-1];
                now.th.push_back(tp);
                now.tw.pop_back();
                if(fc(now)==0)
                {
                    q.push(now);
                    val[fc2(now)]=vv+1;
                    if(now.on.size()==n)
                        return vv+1;
                }
                now.th.pop_back();
                now.tw.push_back(tp);
            }
            if(now.fo.size()==0||(now.fo[now.fo.size()-1]>now.tw[now.tw.size()-1]))
            {
                int tp=now.tw[now.tw.size()-1];
                now.fo.push_back(tp);
                now.tw.pop_back();
                if(fc(now)==0)
                {
                    q.push(now);
                    val[fc2(now)]=vv+1;
                    if(now.on.size()==n)
                        return vv+1;
                }
                now.fo.pop_back();
                now.tw.push_back(tp);
            }


        }
        if(now.th.size()>0)
        {
            if(now.on.size()==0||(now.on[now.on.size()-1]>now.th[now.th.size()-1]))
            {
                int tp=now.th[now.th.size()-1];
                now.on.push_back(tp);
                now.th.pop_back();
                if(fc(now)==0)
                {
                    q.push(now);
                    val[fc2(now)]=vv+1;
                    if(now.on.size()==n)
                        return vv+1;
                }
                now.on.pop_back();
                now.th.push_back(tp);
            }
            if(now.tw.size()==0||(now.tw[now.tw.size()-1]>now.th[now.th.size()-1]))
            {
                int tp=now.th[now.th.size()-1];
                now.tw.push_back(tp);
                now.th.pop_back();
                if(fc(now)==0)
                {
                    q.push(now);
                    val[fc2(now)]=vv+1;
                    if(now.on.size()==n)
                        return vv+1;
                }
                now.tw.pop_back();
                now.th.push_back(tp);
            }
            if(now.fo.size()==0||(now.fo[now.fo.size()-1]>now.th[now.th.size()-1]))
            {
                int tp=now.th[now.th.size()-1];
                now.fo.push_back(tp);
                now.th.pop_back();
                if(fc(now)==0)
                {
                    q.push(now);
                    val[fc2(now)]=vv+1;
                    if(now.on.size()==n)
                        return vv+1;
                }
                now.fo.pop_back();
                now.th.push_back(tp);
            }

        }
        if(now.fo.size()>0)
        {
            if(now.on.size()==0||(now.on[now.on.size()-1]>now.fo[now.fo.size()-1]))
            {
                int tp=now.fo[now.fo.size()-1];
                now.on.push_back(tp);
                now.fo.pop_back();
                if(fc(now)==0)
                {
                    q.push(now);
                    val[fc2(now)]=vv+1;
                    if(now.on.size()==n)
                        return vv+1;
                }
                now.on.pop_back();
                now.fo.push_back(tp);
            }
            if(now.tw.size()==0||(now.tw[now.tw.size()-1]>now.fo[now.fo.size()-1]))
            {
                int tp=now.fo[now.fo.size()-1];
                now.tw.push_back(tp);
                now.fo.pop_back();
                if(fc(now)==0)
                {
                    q.push(now);
                    val[fc2(now)]=vv+1;
                    if(now.on.size()==n)
                        return vv+1;
                }
                now.tw.pop_back();
                now.fo.push_back(tp);
            }
            if(now.th.size()==0||(now.th[now.th.size()-1]>now.fo[now.fo.size()-1]))
            {
                int tp=now.fo[now.fo.size()-1];
                now.th.push_back(tp);
                now.fo.pop_back();
                if(fc(now)==0)
                {
                    q.push(now);
                    val[fc2(now)]=vv+1;
                    if(now.on.size()==n)
                        return vv+1;
                }
                now.th.pop_back();
                now.fo.push_back(tp);
            }
        }
    }
    return 0;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    int in[12];
    scanf(" %d",&n);

    for(int i=1; i<=n; i++)
    {
        scanf(" %d",&in[i]);
    }
    for(int i=n; i>=1; i--)
    {
        if(in[i]==1)
        {
            one.push_back(i);
        }
        if(in[i]==2)
        {
            two.push_back(i);
        }
        if(in[i]==3)
        {
            three.push_back(i);
        }
        if(in[i]==4)
        {
            four.push_back(i);
        }
    }

   /* cout<<one<<endl;
    cout<<two<<endl;
    cout<<three<<endl;
    cout<<four<<endl;
*/
    printf("%d\n",bfs(one,two,three,four)-1);

    return 0;
}


