/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll t,n,arr[1050010],dp[1050010];
map<ll,ll>mpp;

ll rec(ll val,ll flag) {

    if(val==0LL)
    {

        ll fg2 = min(2LL,flag+(mpp[val]>0LL?1LL:0LL));
        if(flag==0) return 0;
        if(fg2==1) {
          if(mpp[val]) return  mpp[val]*(mpp[val]+1LL)/2LL;
          return 0LL;
        }
        return mpp[val]*(mpp[val]+1LL)/2LL+mpp[val];
    }
        return dp[val]=(mpp[val]|flag)==0LL?0LL:(mpp[val]==0LL?1LL:mpp[val]);
    ll &ret=dp[val];
    if(ret!=-1LL)
        return 0;
    ret=0LL;

    for(ll i=0LL;i<20LL;i++)
    {
        if((1LL<<i)&val) {
            ll fg = min(2LL,flag+(mpp[val]>0LL?1LL:0LL));
            ret+=(mpp[val]*(mpp[val]+1LL)/2LL+ max(1LL,mpp[val]) * rec((1LL<<i)^val,fg));
        }
    }

    return ret;
}

int main()
{

    scanf(" %lld",&t);
    for(ll p=1LL; p<=t; p++)
    {

        scanf(" %lld",&n);

        mpp.clear();
        memset(dp,-1LL,sizeof(dp));
        ll mx=0LL;
        for(ll i=1LL; i<=n; i++)
        {
            scanf(" %lld",&arr[i]);
            mpp[arr[i]]++;
            mx=max(mx,arr[i]);
        }
        ll kk=0LL;
        while(mx) {
            mx>>=1LL;
            kk++;
        }
        kk=max(kk,1LL);
        printf("%lld\n",rec((1LL<<kk)-1LL,0LL));
    }


    return 0;
}

