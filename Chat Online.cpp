/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int p,q,l,r,i,j,k,flag,fg[3010],aa,bb,a[3010],b[3010],res;

main()
{
    cin>>p>>q>>l>>r;
    for(i=0;i<p;i++)
    {
        scanf(" %d %d",&aa,&bb);
        for(j=aa;j<=bb;j++)
            fg[j]=1;
    }
    for(i=0;i<q;i++)
    {
        scanf(" %d %d",&a[i],&b[i]);
    }
    for(i=l;i<=r;i++)
    {
        flag=0;
        for(j=0;flag==0&&j<q;j++)
        {
            for(k=a[j]+i;k<=(b[j]+i);k++)
            {
                if(fg[k]==1)
                {
                    res++;
                    flag=1;
                    break;
                }
            }
        }
    }
    cout<<res<<endl;
    return 0;
}

