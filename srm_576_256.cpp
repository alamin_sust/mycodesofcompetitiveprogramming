#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <string.h>

using namespace std;

class TheExperimentDiv2
{
public:
	vector <int> determineHumidity(vector <int> intensity, int L, vector <int> leftEnd)
	{
	    vector<int>res;
	    int det[55],n;
	    memset(det,0,sizeof(det));
	    n=leftEnd.size();
	    for(int i=0;i<n;i++)
	    {
	        int k,j;
	        for(k=0,j=leftEnd[i];j<(leftEnd[i]+L);j++)
	        {
	            if(det[j]==0)
	            {k+=intensity[j];
	            det[j]=1;}
	        }
	        res.push_back(k);
	    }
	    return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	TheExperimentDiv2 objectTheExperimentDiv2;

	//test case0
	vector <int> param00;
	param00.push_back(3);
	param00.push_back(4);
	param00.push_back(1);
	param00.push_back(1);
	param00.push_back(5);
	param00.push_back(6);
	int param01 = 3;
	vector <int> param02;
	param02.push_back(3);
	param02.push_back(1);
	param02.push_back(0);
	vector <int> ret0 = objectTheExperimentDiv2.determineHumidity(param00,param01,param02);
	vector <int> need0;
	need0.push_back(12);
	need0.push_back(5);
	need0.push_back(3);
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(5);
	param10.push_back(5);
	int param11 = 2;
	vector <int> param12;
	param12.push_back(0);
	param12.push_back(0);
	vector <int> ret1 = objectTheExperimentDiv2.determineHumidity(param10,param11,param12);
	vector <int> need1;
	need1.push_back(10);
	need1.push_back(0);
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(92);
	param20.push_back(11);
	param20.push_back(1000);
	param20.push_back(14);
	param20.push_back(3);
	int param21 = 2;
	vector <int> param22;
	param22.push_back(0);
	param22.push_back(3);
	vector <int> ret2 = objectTheExperimentDiv2.determineHumidity(param20,param21,param22);
	vector <int> need2;
	need2.push_back(103);
	need2.push_back(17);
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(2);
	param30.push_back(6);
	param30.push_back(15);
	param30.push_back(10);
	param30.push_back(8);
	param30.push_back(11);
	param30.push_back(99);
	param30.push_back(2);
	param30.push_back(4);
	param30.push_back(4);
	param30.push_back(4);
	param30.push_back(13);
	int param31 = 4;
	vector <int> param32;
	param32.push_back(1);
	param32.push_back(7);
	param32.push_back(4);
	param32.push_back(5);
	param32.push_back(8);
	param32.push_back(0);
	vector <int> ret3 = objectTheExperimentDiv2.determineHumidity(param30,param31,param32);
	vector <int> need3;
	need3.push_back(39);
	need3.push_back(14);
	need3.push_back(110);
	need3.push_back(0);
	need3.push_back(13);
	need3.push_back(2);
	assert_eq(3,ret3,need3);

}

