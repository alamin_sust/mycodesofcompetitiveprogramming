/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))
#define MAX 1000010LL


using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

map<ll,ll>mppext;
ll sieve[MAX+10LL],pc,prime[MAX+10LL],mpp[MAX+10],res;

vector<ll>v;

void sieve_(void)
{
    sieve[0]=sieve[1]=1LL;
    for(ll i=2LL;i<=MAX;i++)
    {
        if(sieve[i]==0LL)
        {
            prime[pc++]=i;
            for(ll j=2LL;i*j<=MAX;j++)
                sieve[i*j]=1LL;
        }
    }
    return;
}

ll _gcd(ll a,ll b)
{
    if(b==0LL)
        return a;
    return _gcd(b,a%b);
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    ll i,j,p,t,n,arr[110];

//    printf("%lld\n",_gcd(30,17));

    sieve_();

   // for(i=0LL;i<10LL;i++)
     //   printf("%lld\n",prime[i]);

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++)
    {
        v.clear();
        scanf(" %lld",&n);
        for(i=0LL;i<n;i++)
        {
            scanf(" %lld",&arr[i]);
        }
        mppext.clear();
        res=0LL;
        for(i=0LL;i<n&&res==0LL;i++)
        {
            ll now=arr[i];
            for(j=0LL;j<pc&&now>1LL&&res==0LL;j++)
            {
                while((now%prime[j])==0LL&&now>1LL)
                {
                    if(mpp[j]==p)
                    {
                        res=prime[j];
                        break;
                    }
                    mpp[j]=p;
                    now/=prime[j];
                }
            }
            if(now>1LL)
            {
                if(mppext[now])
                {
                    res=now;
                    break;
                }
                mppext[now]=1LL;
                v.push_back(now);
            }
        }
        if(res)
        printf("%lld\n",res);
        else
        {
            res=1LL;
            for(i=0LL;i<v.size()&&res==1LL;i++)
            {
                for(j=i+1LL;j<v.size()&&res==1LL;j++)
                {
                    res=_gcd(v[i],v[j]);
                    if(res>1LL)
                        break;
                }
            }
            if(res>1LL)
            printf("%lld\n",res);
            else
            {
                res=1LL;
                for(i=0LL;i<v.size();i++)
                {
                    ll now=(ll)sqrt((double)v[i]);
                    if((now*now)==v[i])
                    {
                        res=now;
                        break;
                    }
                }
                printf("%lld\n",res);
            }
        }
    }

    return 0;
}






