/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#pragma comment(linker, �/STACK:801000�)
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node{

double x1,x2,y1,y2;
int id;

};

node det;
stack<node>stk,stk2;

double x[10],y[10];

double direction(int p1,int p2,int p3)
{
    return ((x[p3]-x[p1])*(y[p2]-y[p1]))-((x[p2]-x[p1])*(y[p3]-y[p1]));
}

bool on_segment(int p1,int p2,int p3)
{
    if((min(x[p1],x[p2])<=x[p3]&&max(x[p1],x[p2])>=x[p3])&&(min(y[p1],y[p2])<=y[p3]&&max(y[p1],y[p2])>=y[p3]))
        return true;
    else return false;
}

bool segment_intersect(int p1,int p2,int p3,int p4)
{
    double d1=direction(p3,p4,p1);
    double d2=direction(p3,p4,p2);
    double d3=direction(p1,p2,p3);
    double d4=direction(p1,p2,p4);
    //printf("%lf %lf %lf %lf\n",d1,d2,d3,d4);
    if(((d1>=0&&d2<=0)||(d1<=0&&d2>=0))&&((d3>=0&&d4<=0)||(d3<=0&&d4>=0)))
        return true;
    //else if(d1==0&&on_segment(p3,p4,p1))
   //     return true;
   // else if(d2==0&&on_segment(p3,p4,p2))
   //     return true;
   // else if(d3==0&&on_segment(p1,p2,p3))
   //     return true;
   // else if(d4==0&&on_segment(p1,p2,p4))
   //     return true;
    else
        return false;
}

int main()
{
    int i,n;
    while(cin>>n&&n)
    {
        for(i=1; i<=n; i++)
        {
            scanf(" %lf %lf %lf %lf",&x[1],&y[1],&x[2],&y[2]);
            det.x1=x[1];
            det.y1=y[1];
            det.x2=x[2];
            det.y2=y[2];
            det.id=i;
            stk2.push(det);
            while(!stk.empty())
            {
                x[3]=stk.top().x1;
                y[3]=stk.top().y1;
                x[4]=stk.top().x2;
                y[4]=stk.top().y2;
                if(!segment_intersect(1,2,3,4))
                {
                 //   printf("%d\n",i);
                    det.id=stk.top().id;
                    det.x1=x[3];
                    det.y1=y[3];
                    det.x2=x[4];
                    det.y2=y[4];
                    stk2.push(det);
                }
                stk.pop();
            }
            while(!stk2.empty())
            {
                stk.push(stk2.top());
                stk2.pop();
            }
            //cin>>x[3]>>y[3]>>x[4]>>y[4];
            //if(segment_intersect(1,2,3,4))
            //    printf("Two lines intersects\n");
            //else
            //    printf("Two lines don't intersects\n");
        }
        while(!stk.empty())
            {
                stk2.push(stk.top());
                stk.pop();
            }
        printf("Top sticks:");
        while(!stk2.empty())
        {
            printf(" %d",stk2.top().id);
            stk2.pop();
            if(stk2.empty())
                printf(".\n");
            else
                printf(",");
        }
    }
    return 0;
}

