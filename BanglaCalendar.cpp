/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

char names[13][20]={"","Baishakh", "Jaishtha", "Ashar", "Sraban", "Bhadra", "Ashwin", "Kartik" ,"Agrahayan", "Poush", "Magh", "Falgun", "Chaitra"};
int arr1[13]={0,31,31,31,31,31,30,30,30,30,30,30,30};
int arr2[13]={0,31,31,31,31,31,30,30,30,30,30,31,30};

int arr11[13]={0,31,28,31,30,31,30,31,31,30,31,30,31};
int arr22[13]={0,31,29,31,30,31,30,31,31,30,31,30,31};
int i,p,t,k,k2,m,d,y,bm,bd,em,ed;

map<pair<int,int>, pair<int,int> >leap,noleap,dl,dn;

int isleap(int year)
{
    if((year%400)==0)
        return 1;
    if((year%100)==0)
        return 0;
    if((year%4)==0)
        return 1;
    return 0;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    bm=9;
    bd=18;
    em=1;
    ed=1;
    k=0;
    k2=17;
    int tp;
    tp=0;
    for(i=1;i<=365;i++)
    {
        k++;
        k2++;
        noleap[make_pair(em,k)]=make_pair(bm,k2);
        dn[make_pair(em,k)]=make_pair(tp,0);
        if(k==arr11[em])
            em++,k=0;
        if(k2==arr1[bm])
            bm++,k2=0;

        if(bm>12)
            bm=1,tp=1;
    }

    //printf("%d %d %d %d\n",em,k,bm,k2);

    bm=9;
    bd=18;
    em=1;
    ed=1;
    k=0;
    k2=17;
    tp=0;
    for(i=1;i<=366;i++)
    {
        k++;
        k2++;
        leap[make_pair(em,k)]=make_pair(bm,k2);
        dl[make_pair(em,k)]=make_pair(tp,0);
        if(k==arr22[em])
            em++,k=0;
        if(k2==arr2[bm])
            bm++,k2=0;
        if(bm>12)
            bm=1,tp=1;
    }

    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %d %d %d",&d,&m,&y);
        if(isleap(y))
        {
            //printf("...");
            printf("Case %d: %d, %s, %d\n",p,leap[make_pair(m,d)].second,names[leap[make_pair(m,d)].first],y-594+dl[make_pair(m,d)].first);
        }
        else
        {
            printf("Case %d: %d, %s, %d\n",p,noleap[make_pair(m,d)].second,names[noleap[make_pair(m,d)].first],y-594+dn[make_pair(m,d)].first);
        }
    }

    return 0;
}

