/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int res[110],P[110],M[111],L,hi,lo,i,k,arr[110],mid,N,newL;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    cin>>N;
    for(i=0;i<N;i++)
        scanf(" %d",&arr[i]);
    L=0;

    for(i=0;i<N;i++)
    {
        lo=1;
        hi=L;
        while(lo<=hi)
        {
            mid=(lo+hi)/2+(lo+hi)%2;
            if(arr[M[mid]]<arr[i])
                lo=mid+1;
            else
                hi=mid-1;
        }
        newL=lo;
        P[i]=M[newL-1];
        M[newL]=i;
        if(newL>L)
            L=newL;
    }
    k=M[L];
    for(i=L-1;i>=0;i--)
    {
        res[i]=arr[k];
        k=P[k];
    }

    for(i=0;i<L;i++)
        printf("%d ",res[i]);
    return 0;
}



