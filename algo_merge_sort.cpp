#include<stdio.h>
#include<iostream>
using namespace std;

int arr[100010],tarr[10010];

void merge_(int f,int m,int t)
{
    int left=f,right=m+1,pos=1;
    while(left<=m||right<=t)
    {
        if(right>t)
            tarr[pos++]=arr[left++];
        else if(left>m)
            tarr[pos++]=arr[right++];
        else if(arr[left]>arr[right])
        {
            tarr[pos++]=arr[right++];
        }
        else
        {
            tarr[pos++]=arr[left++];
        }
    }
    for(int j=1, i=f;i<=t;j++,i++)
    {
        arr[i]=tarr[j];
    }
    return;
}

void mergesort(int beg,int en)
{
    int mid=(beg+en)/2;
    if(beg<en)
    {
    mergesort(beg,mid);
    mergesort(mid+1,en);
    merge_(beg,mid,en);
    }
    return;
}

main()
{
    int i,n;
    cin>>n;
    for(i=1;i<=n;i++)
    {
        cin>>arr[i];
    }
    mergesort(1,n);
    for(i=1;i<=n;i++)
    printf("%d ",arr[i]);
    return 0;
}
