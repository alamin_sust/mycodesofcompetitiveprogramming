#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) ( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

long long t,i,j,n,x[1010],y[1010],res,p,mx[1010];

main()
{
    scanf(" %lld",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %lld",&n);
        for(i=1;i<=n;i++)
        {
            scanf(" %lld %lld",&x[i],&y[i]);
        }
        res=0LL;
        for(i=1;i<=n;i++)
        {
            mx[i]=99999999999999999LL;
            for(j=1;j<=n;j++)
            {
                if(i!=j)
                {
                    mx[i]=min(mx[i],dist(x[i],y[i],x[j],y[j]));

                }
            }
            res=max(res,mx[i]);
        }
        if(res<4)
            printf("-1.000000\n");
        else
            printf("%.10lf\n",sqrt((double)res)-1.0);
    }
    return 0;
}

