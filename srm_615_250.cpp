#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class AmebaDiv2
{
public:
	int simulate(vector <int> X, int A)
	{
	    int now=A;
	    for(int i=0;i<X.size();i++)
        {
            if(X[i]==now)
                now*=2;
        }
        return now;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	AmebaDiv2 objectAmebaDiv2;

	//test case0
	vector <int> param00;
	param00.push_back(2);
	param00.push_back(1);
	param00.push_back(3);
	param00.push_back(1);
	param00.push_back(2);
	int param01 = 1;
	int ret0 = objectAmebaDiv2.simulate(param00,param01);
	int need0 = 4;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(1);
	param10.push_back(4);
	param10.push_back(9);
	param10.push_back(16);
	param10.push_back(25);
	param10.push_back(36);
	param10.push_back(49);
	int param11 = 10;
	int ret1 = objectAmebaDiv2.simulate(param10,param11);
	int need1 = 10;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(1);
	param20.push_back(2);
	param20.push_back(4);
	param20.push_back(8);
	param20.push_back(16);
	param20.push_back(32);
	param20.push_back(64);
	param20.push_back(128);
	param20.push_back(256);
	param20.push_back(1024);
	param20.push_back(2048);
	int param21 = 1;
	int ret2 = objectAmebaDiv2.simulate(param20,param21);
	int need2 = 512;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(817);
	param30.push_back(832);
	param30.push_back(817);
	param30.push_back(832);
	param30.push_back(126);
	param30.push_back(817);
	param30.push_back(63);
	param30.push_back(63);
	param30.push_back(126);
	param30.push_back(817);
	param30.push_back(832);
	param30.push_back(287);
	param30.push_back(823);
	param30.push_back(817);
	param30.push_back(574);
	int param31 = 63;
	int ret3 = objectAmebaDiv2.simulate(param30,param31);
	int need3 = 252;
	assert_eq(3,ret3,need3);

}

