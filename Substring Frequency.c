#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char substr[1000010],str[1000010];
int kmp(int x)
{
   int i, j, N, M,pp;
   N = strlen(str);
   M = strlen(substr);

   int *d = (int*)malloc(M * sizeof(int));
   d[0] = 0;

   for(i = 0, j = 0; i < M; i++)
   {
      while(j > 0 && substr[j] != substr[i])
      {
         j = d[j - 1];
      }

      if(substr[j] == substr[i])
      {
         j++;
         d[i] = j;
      }
   }

   for(i = x, j = 0; i < N; i++)
   {
       pp=0;
      while(j > 0 && substr[j] != str[i]&&pp<M)
      {
         j = d[j - 1];
         pp++;
      }
      if(pp==M)
      return -1;
      if(substr[j] == str[i])
      {
         j++;
      }

      if(j == M)
      {
         free(d);
         return i - j + 1;
      }
   }

   free(d);

   return -1;
}

int main()
{
    char temp;
   int n,i,l,k,res,len,p;
   scanf(" %d",&n);
   getchar();
   for(i=1;i<=n;i++)
   {
       gets(str);
       gets(substr);
       len=strlen(substr);
       l=0;
       res=0;
       k=0;
       p=0;
       while(k!=-1)
      {
      k = kmp(l);
      l=k+1;
      temp=str[l-1];
      p=0;
      while(substr[l+p]==temp&&p<len-1)
      p++;
      if(p!=len-1)
      l+=p;
      printf(",,");
      res++;
      }
      printf("Case %d: %d\n",i,res);
   }
   return 0;
}
