/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

vector<ll>adj[55];
ll col[55],par[55],max_bw[55],p=1,bandwidth[55][55];
queue<ll>q;
map<ll,ll>mpp;

void send_flow(ll node,ll src,ll bw)
{
    while(node!=src)
    {
        bandwidth[par[node]][node]-=bw;
        //bandwidth[node][par[node]]+=bw; //if directed graph
        node=par[node];
    }
    return;
}

ll bfs(ll src,ll dst)
{
    memset(par,0,sizeof(par));
   while(!q.empty())
   q.pop();
   q.push(src);
   par[src]=-1;
   col[src]=p;
   max_bw[src]=99999999;
   while(!q.empty())
   {
       int u=q.front();
       for(ll i=0;i<adj[u].size();i++)
       {
           int v=adj[u][i];
           if(bandwidth[u][v]>0&&par[v]==0)
            {
                par[v]=u;
                col[v]=p;
                max_bw[v]=min(max_bw[u],bandwidth[u][v]);
                q.push(v);
                if(v==dst)
                {
                    send_flow(v,src,max_bw[v]);
                    return max_bw[v];
                }
            }
       }
       q.pop();
   }
   return 0;
}

main()
{
    ll i,j,s,t,n,flow,maxflow,m,bw,from,to;
    while(cin>>n>>m)
    {
        if(n==0&&m==0)
            break;
        for(i=1;i<=n;i++)
            {
                adj[i].clear();
                for(j=1;j<=n;j++)
                    bandwidth[i][j]=-1;
            }

        for(i=1;i<=m;i++)
        {
            scanf(" %lld %lld %lld",&from,&to,&bw);
            adj[from].push_back(to);
            adj[to].push_back(from);
            bandwidth[from][to]=bw;
            bandwidth[to][from]=bw;
        }
        maxflow=0;
        p=1;
        memset(col,0,sizeof(col));
        while(1)
        {
            p++;
            flow=bfs(1,2);
            maxflow+=flow;
            if(flow==0)
                break;
        }
        //printf("..");
        mpp.clear();
        for(i=1;i<=n;i++)
        {
            if(col[i]==p)
            mpp[i]=1;
        }
        for(i=1;i<=n;i++)
        {
            for(j=i+1;j<=n;j++)
            {
                if(bandwidth[i][j]>=0&&(mpp[i]+mpp[j]==1))
                    printf("%lld %lld\n",i,j);
            }
        }
        printf("\n");
    }
    return 0;
}

