/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int t,p,n,i,j,arr1[100010],arr2[100010],m,mx,mn,in;
int main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %d %d",&n,&m);
        mx=0;
        mn=10000000;
        for(i=0;i<m;i++)
        {
            scanf(" %d",&in);
            mx=max(in,mx);
            mn=min(in,mn);
        }
        for(i=mn,j=0;i<n;j++,i++)
            arr1[i]=j;
         for(i=mn,j=0;i>=0;j++,i--)
            arr1[i]=j;
        for(i=mx,j=0;i<n;j++,i++)
            arr2[i]=j;
         for(i=mx,j=0;i>=0;j++,i--)
            arr2[i]=j;
        for(i=0;i<n;i++)
        {
            printf("%d",max(arr1[i],arr2[i]));
            if(i==n-1)
                printf("\n");
            else
                printf(" ");
        }
    }
    return 0;
}


