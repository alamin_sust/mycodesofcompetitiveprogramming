#include<stdio.h>
#include<iostream>
#define MAX 20
#define INF 99999999

using namespace std;

typedef struct{
   int x,y;
}Node;

bool notTaken[MAX];
Node node[MAX];
int pos,nBeep;

int abs(int n){
   if(n < 0)
      return -n;
   return n;
}

int sx,sy;
int getNext(int x,int y){
   int min = INF,dist,i;
   for(i = 0; i<nBeep; i++){
      dist = abs(x - node[i].x) + abs(y - node[i].y);
      if(min > dist && notTaken[i]){
         min = dist;
         pos  = i;
      }
   }
   notTaken[pos] = false;
   return min;
}

bool hasNext(){
   int i;
   for(i = 0; i<nBeep; i++){
      if(notTaken[i])
         return true;
   }
   return false;
}

int main(){
   int a,b,dist,m,n,test,x,y,i;
   scanf("%d",&test);
   while(test--){
      scanf("%d%d",&m,&n);
      scanf("%d%d",&sx,&sy);
      scanf("%d",&nBeep);

      for(i = 0; i<nBeep; i++){
         scanf("%d%d",&a,&b);
         node[i].x = a;
         node[i].y = b;
      }
      fill(notTaken,notTaken + nBeep, true);
      int cnt = 0;
      x = sx;
      y = sy;
      for(i = 0; hasNext(); i++){
         dist = getNext(x,y);
         cnt += dist;
         x = node[pos].x;
         y = node[pos].y;
      }
      dist = abs(sx - x) + abs(sy - y);
      cnt += dist;
      printf("The shortest path has length %d\n",cnt);
   }
   return 0;
}
