/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll arr[30],k,a,b,n,p,t;

ll pw(ll x,ll m)
{
    ll i;
    arr[0]=x;
    for(i=1LL;i<=22LL;i++)
        arr[i]=(arr[i-1]*arr[i-1])%k;
    ll ret=1,c=0;
    while(m>0LL)
    {
        if(m%2LL)
        {
            ret=(ret*arr[c])%k;
        }
        m/=2LL;
        c++;
    }
    return ret;
}


ll fc(ll x,ll y)
{
    ll r1=pw(x,a);
    ll r2=pw(y,b);
    if(((r1+r2)%k)==0LL)
        return 1LL;
    return 0LL;

}

int main()
{
    freopen("B-small-attempt0B.in","r",stdin);
    freopen("SherlockandWatsonOut.txt","w",stdout);

    ll i,j,res;

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++)
    {
        scanf(" %lld %lld %lld %lld",&a,&b,&n,&k);
        res=0LL;
        for(i=1LL;i<=n;i++)
        {
            for(j=1LL;j<=n;j++)
            {
                if(i!=j&&fc(i,j))
                {
                    //printf("%lld %lld\n",i,j);
                    res++;}
            }
        }
        printf("Case #%lld: %lld\n",p,res);
    }


    return 0;
}






