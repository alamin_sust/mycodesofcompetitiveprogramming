#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#define ll long long int
using namespace std;

map<ll,ll>mpp;

class NumberGameAgain
{
public:
	long long solve(int k, vector<long long> table)
	{
	    mpp.clear();
	    for(ll i=0LL;i<table.size();i++)
        {
            mpp[table[i]]=1LL;
        }
        sort(table.begin(),table.end());
        for(ll i=table.size()-1LL;i>=0LL;i--)
        {
            if(mpp[table[i]]!=2LL)
            {
                ll tp=table[i];
                ll now=table[i];
                tp>>=1LL;
                while(tp>0LL)
                {
                    if(mpp[tp]==1LL)
                    {
                        mpp[now]=2LL;
                        now=tp;
                    }
                    tp>>=1LL;
                }
            }
        }
        ll ret=(1LL<<(ll)k)-2LL;
        ll mx=(1LL<<(ll)k);
        for(ll i=0LL;i<table.size();i++)
        {
            if(mpp[table[i]]==1LL)
            {
                ll cnt=0LL,tmp=table[i];
                while(tmp<mx)
                {
                    cnt++;
                    tmp<<=1LL;
                }
                ret=ret-((1LL<<cnt)-1LL);
            }
        }
        return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	NumberGameAgain objectNumberGameAgain;

	//test case0
	int param00 = 3;
	vector<long long> param01;
	param01.push_back(2);
	param01.push_back(4);
	param01.push_back(6);
	long long ret0 = objectNumberGameAgain.solve(param00,param01);
	long long need0 = 2;
	assert_eq(0,ret0,need0);

	//test case1
	int param10 = 5;
	vector<long long> param11;
	param11.push_back(2);
	param11.push_back(3);
	long long ret1 = objectNumberGameAgain.solve(param10,param11);
	long long need1 = 0;
	assert_eq(1,ret1,need1);

	//test case2
	int param20 = 5;
	vector<long long> param21;
	long long ret2 = objectNumberGameAgain.solve(param20,param21);
	long long need2 = 30;
	assert_eq(2,ret2,need2);

	//test case3
	int param30 = 40;
	vector<long long> param31;
	param31.push_back(2);
	param31.push_back(4);
	param31.push_back(8);
	param31.push_back(16);
	param31.push_back(32141531);
	param31.push_back(2324577);
	param31.push_back(1099511627775);
	param31.push_back(2222222222);
	param31.push_back(33333333333);
	param31.push_back(4444444444);
	param31.push_back(2135);
	long long ret3 = objectNumberGameAgain.solve(param30,param31);
	long long need3 = 549755748288;
	assert_eq(3,ret3,need3);

	//test case4
	int param40 = 40;
	vector<long long> param41;
	long long ret4 = objectNumberGameAgain.solve(param40,param41);
	long long need4 = 1099511627774;
	assert_eq(4,ret4,need4);

}

