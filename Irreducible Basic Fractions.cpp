#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll cut(ll n)
{
    if(n==1)
        return 1;
    ll res=n;
    if(n%2==0)
    {
        res-=res/2;
        while(n%2==0)
        n/=2;
    }
    for(ll i=3;i*i<=n;i+=2)
    {
        if(n%i==0)
        {
            res-=res/i;
            while(n%i==0)
            n/=i;
        }
    }
    if(n>1)
        res-=(res/n);
    return res;
}

main()
{
    ll n;
    while(cin>>n&&n)
    {
        cout<<cut(n)<<endl;
    }
    return 0;
}

