/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int res,r,c,w,p,t,flag[110],res2,i,j,tp,k;

int main()
{
    //freopen("A-small-attempt1111.in","r",stdin);
    //freopen("battleshipout.txt","w",stdout);
    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {
        scanf(" %d %d %d",&r,&c,&w);
        printf("Case #%d: ",p);
        if(w==1)
        {
            printf("%d\n",r*c);
            continue;
        }
        //res=((c/w)*r)+(w-1);
        memset(flag,0,sizeof(flag));
        for(i=w; i<=c; i+=w)
        {
            flag[i]=1;
        }
        res=0,res2=0,tp=0;
        for(i=c; i>=1; i--)
        {
            if(flag[i]==1)
            {
                flag[i]=3;
                tp=i-w;
                for(k=1,j=i+1; k<w&&j<=c; k++,j++)
                {
                    flag[j]=2;
                }
                for(k=1,j=i-1; k<w&&j>=1; k++,j--)
                {
                    flag[j]=2;
                }
                for(j=1; j<=c; j++)
                {
                    if(flag[j])
                        res++;
                }
                break;
            }
        }

        if(tp>0)
        {
            for(k=1,j=tp-1;k<w&&j>=1;j--,k++)
            {
                flag[j]=4;
            }
            for(k=1,j=tp+1;k<w&&j<=c;k++,j++)
            {
                flag[j]=4;
            }
            for(j=1;j<=c;j++)
            {
                if(flag[j]==1||flag[j]==4)
                    res2++;
            }
        }
        if(r==1)
        printf("%d\n",max(res,res2)*r);
        else
        printf("%d\n",max(res,res2)+((r-1)*(c/w)));
    }
    return 0;
}




