#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
int val,type;
}arr[100010];

int t,p,i,j,n,m,k,res;

int comp(node a,node b)
{
    return a.val<b.val;
}

int comp2(node a,node b)
{
    if(a.val==b.val)
        return a.type>b.type;
    return a.val<b.val;
}
main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n;
        for(j=0,i=1;i<=n;j+=2,i++)
        {
            cin>>arr[j].val>>arr[j+1].val;
            arr[j].type=1;
            arr[j+1].type=-1;
        }
        m=2*n;
        //sort(arr,arr+m,comp);
        sort(arr,arr+m,comp2);
        res=0;
        for(k=0,i=0;i<m;i++)
        {
            k+=arr[i].type;
            res=max(res,k);
        }
        cout<<"Case "<<p<<": "<<res<<endl;
    }
    return 0;
}

