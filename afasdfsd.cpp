#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
using namespace std;

long long int dp[110][110][110],t[110],w[110],n;

long long int rec(long long int i,long long int thick,long long int width)
{
    if(i==n)
    {
        if(thick>=width)
        return thick;
        else
        return 99999999;
    }
    if(dp[i][thick][width]!=-1)
    return dp[i][thick][width];
    long long int ret1=99999999,ret2=99999999;
    //if(thick+t[i]<=width-w[i])
    ret1=rec(i+1,t[i]+thick,width-w[i]);
    ret2=rec(i+1,thick,width);
    return dp[i][thick][width]=min(ret1,ret2);
}


long long int rec2(long long int i,long long int thick,long long int width)
{
    if(i==n)
    {
        if(thick>=width)
        return thick;
        else
        return 99999999;
    }
    if(dp[i][thick][width]!=-1)
    return dp[i][thick][width];
    long long int ret1=99999999,ret2=99999999;
    //if(thick+t[i]<=width-w[i])
    ret1=rec(i+1,thick-t[i],width+w[i]);
    ret2=rec(i+1,thick,width);
    return dp[i][thick][width]=min(ret1,ret2);
}

main()
{
    long long int i,width,thick,res;
    scanf(" %I64d",&n);
    for(i=0,width=0,thick=0;i<n;i++)
    {
        scanf(" %I64d %I64d",&t[i],&w[i]);
        width+=w[i];
        thick+=t[i];
    }
    memset(dp,-1,sizeof(dp));
    res=rec(0,0,width);
    memset(dp,-1,sizeof(dp));
    res=min(res,rec2(0,thick,0));
    printf("%I64d\n",res);
    return 0;
}
