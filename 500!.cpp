#include<stdio.h>
#include<iostream>
#include<BigNum.h>
using namespace std;

 bignum f[1001];

int main(){
   int n;
   f[0] = bignum("1");
   f[1] = bignum("1");
   for (int i = 2; i <= 1000; i++)
      f[i] = f[i - 1] * bignum(i);
   while (cin >> n)
      cout << n << "!" << endl << f[n] << endl;
      return 0;
}
