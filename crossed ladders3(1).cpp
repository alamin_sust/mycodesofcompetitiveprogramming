/*Use b-search,or the bisection method to find the answer*/
#include<iostream>
#include<cstdio>
#include<cmath>
using namespace std;
long double x,y,c,s,func,U,D,e,f;
int main()
{
    while(1)
    {
        cin>>x>>y>>c;
        if(x==y&&c!=0)
        {
            s = sqrt(x*x-4*c*c);
            cout<<s<<endl;
        }
        else if(c==0)
        {
            printf("0.000\n");
        }
        else
        {
            if(x>y)
            U = x;
            else
            U = y;
            D = 0;
            s = (U+D)/2;
            e = sqrt(x*x-s*s);
            f = sqrt(y*y-s*s);
            func = (1/c)-(1/e)-(1/f);
            while(abs(func)>0.0000001)
            {
                if(func>0)
                D = s;
                else
                U = s;
                s = (U+D)/2;
                e = sqrt(x*x-s*s);
                f = sqrt(y*y-s*s);
                func = (1/c)-(1/e)-(1/f);
            }
            cout<<s<<endl;
        }
    }
    return 0;
}
