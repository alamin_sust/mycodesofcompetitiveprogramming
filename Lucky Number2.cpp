/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll dp[1003][1003][2],lucky[1010],len,arr[1010],MOD=1000000007LL;
char str[1010];

void luckygen(void)
{
    for(ll i=4LL; i<=1000LL; i++)
    {
        ll j=i;
        lucky[i]=1LL;
        while(j)
        {
            if((j%10LL)!=4LL&&(j%10LL)!=7LL)
            {
                lucky[i]=0LL;
                break;
            }
            j/=10LL;
        }
    }
    return;
}

void mem(ll l)
{
    for(ll i=0LL;i<=l;i++)
    {
        for(ll j=0LL;j<=l;j++)
        {
            dp[i][j][0]=dp[i][j][1]=-1LL;
        }
    }
    return;
}

ll rec(ll pos,ll dig,ll flag)
{
    if(pos==len)
    {
        if(lucky[dig])
            return 1LL;
        else
            return 0LL;
    }
    ll &ret=dp[pos][dig][flag];
    if(ret!=-1LL)
        return ret;
    ret=0LL;
   // ret=(ret+rec(pos+1LL,dig,flag|(arr[pos]>0LL?1LL:0LL)))%MOD;
    ll kk=arr[pos];
    if(kk>7LL)
    {
        ret=(ret+((kk-2LL)*rec(pos+1LL,dig,1LL))%MOD)%MOD;
        ret=(ret+((2LL)*rec(pos+1LL,dig+1LL,1LL))%MOD)%MOD;
    }
    else if(kk>4LL)
    {
        ret=(ret+((kk-1LL)*rec(pos+1LL,dig,1LL))%MOD)%MOD;
        ret=(ret+((1LL)*rec(pos+1LL,dig+1LL,1LL))%MOD)%MOD;
    }
    else
    {
        ret=(ret+((kk*rec(pos+1LL,dig,1LL))%MOD))%MOD;
    }
    if(arr[pos]==4LL||arr[pos]==7LL)
        ret=(ret+rec(pos+1LL,dig+1LL,flag))%MOD;
    else
        ret=(ret+rec(pos+1LL,dig,flag))%MOD;
    /*for(ll i=0LL; i<=9LL; i++)
    {
        if(flag==0LL&&arr[pos]<i)
            break;
        if(i==4LL||i==7LL)
            ret=(ret+rec(pos+1LL,dig+1LL,flag|(arr[pos]>i?1LL:0LL)))%MOD;
        else
            ret=(ret+rec(pos+1LL,dig,flag|(arr[pos]>i?1LL:0LL)))%MOD;
    }*/
    return ret;
}

int main()
{
    ll t,p,res,a,aa,i;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    luckygen();

    // for(i=0;i<=1000;i++)
    // {
    //     if(lucky[i])
    //         printf("%lld\n",i);
    // }

    scanf(" %lld",&t);
    for(p=1LL; p<=t; p++)
    {
        res=0LL;
        //memset(dp,-1LL,sizeof(dp));

        scanf(" %s",&str);
        len=strlen(str);
        mem(len);
        a=0LL;
        for(i=0LL;i<len;i++)
        {
            if(str[i]=='4'||str[i]=='7')
                a++;
            arr[i]=str[i]-'0';
        }
        if(lucky[a])
            res--;
        //a--;
        //aa=a;
        //len=0LL;
        //while(aa)
        //    aa/=10LL,len++;
        //aa=a;
        //for(i=len-1LL; i>=0LL; i--)
       // {
       //     arr[i]=aa%10LL;
       //     aa/=10LL;
       // }

        res+=rec(0LL,0LL,0LL);

        //scanf(" %lld",&a);
        //aa=a;
        //len=0LL;
        //while(aa)
        //    aa/=10LL,len++;
        //aa=a;
        //for(i=len-1LL; i>=0LL; i--)
       // {
       //     arr[i]=aa%10LL;
       //     aa/=10LL;
       // }
       scanf(" %s",&str);
        len=strlen(str);
        for(i=0LL;i<len;i++)
        {
            arr[i]=str[i]-'0';
        }
        //memset(dp,-1LL,sizeof(dp));
        mem(len);
        printf("%lld\n",(MOD+MOD+rec(0LL,0LL,0LL)-res)%MOD);
    }
    return 0;
}






