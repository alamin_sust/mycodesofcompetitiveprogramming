/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll arr[100010];
struct node
{
    ll val;
    vector<ll>vec;
};

node tree[400010];
ll mx_ll=9223372036854775807LL;

ll _gcd(ll a,ll b)
{
    if(b==0LL)
        return a;
    return _gcd(b,a%b);
}

ll lcm(ll a,ll b)
{
    return (a*b)/_gcd(a,b);
}

node construct_tree(ll pos,ll l,ll r)
{
    if(l==r)
    {
        tree[pos].val=arr[l];
        tree[pos].vec.clear();
        tree[pos].vec.push_back(arr[l]);
        return tree[pos];
    }
    ll mid=(l+r)/2LL;
    node a=construct_tree(pos*2LL,l,mid);
    node b=construct_tree(pos*2LL+1LL,mid+1LL,r);
    ll a_last=a.vec.size()-1LL;
    ll b_last=b.vec.size()-1LL;
    tree[pos].val=_gcd(a.val,b.val);
    vector<ll>tv;
    tv.clear();
    if(a.vec[a_last]<=(mx_ll/b.vec[b_last]))
    {
        tv.insert(tv.end(), a.vec.begin(), a.vec.end());
        tv[a_last]=lcm(a.vec[a_last],b.vec[b_last]);
        tv.insert(tv.end(), b.vec.begin(), b.vec.end()-1LL);
    }
    else
    {
        tv.insert(tv.end(), a.vec.begin(), a.vec.end());
        tv.insert(tv.end(), b.vec.begin(), b.vec.end());
    }
    tree[pos].vec.clear();
    tree[pos].vec.insert(tree[pos].vec.end(),tv.begin(), tv.end());
    return tree[pos];
}

node update(ll pos,ll l,ll r,ll ind)
{
    if(l==r&&l==ind)
    {
        tree[pos].val=arr[l];
        tree[pos].vec.clear();
        tree[pos].vec.push_back(arr[l]);
        return tree[pos];
    }
    if(ind<l||ind>r)
    {
        return tree[pos];
    }
    ll mid=(l+r)/2LL;

    node a=update(pos*2LL,l,mid,ind);
    node b=update(pos*2LL+1LL,mid+1LL,r,ind);
    ll a_last=a.vec.size()-1LL;
    ll b_last=b.vec.size()-1LL;
    tree[pos].val=_gcd(a.val,b.val);
    vector<ll>tv;
    tv.clear();
    if(a.vec[a_last]<=(mx_ll/b.vec[b_last]))
    {
        tv.insert(tv.end(), a.vec.begin(), a.vec.end());
        tv[a_last]=lcm(a.vec[a_last],b.vec[b_last]);
        tv.insert(tv.end(), b.vec.begin(), b.vec.end()-1LL);
    }
    else
    {
        tv.insert(tv.end(), a.vec.begin(), a.vec.end());
        tv.insert(tv.end(), b.vec.begin(), b.vec.end());
    }
    tree[pos].vec.clear();
    tree[pos].vec.insert(tree[pos].vec.end(),tv.begin(), tv.end());

    return tree[pos];
}

ll query(ll pos,ll l,ll r,ll ql,ll qr,ll g)
{
    if(r<ql||l>qr)
    {
        return 0LL;
    }
    if(l==r)
    {
        if(_gcd(tree[pos].val,g)>1LL)
            return 1LL;
        return 0LL;
    }
    if(l>=ql&&r<=qr)
    {
        if(_gcd(tree[pos].val,g)>1LL)
            return (r-l+1LL);
        ll fg=0LL;
        for(ll i=0LL; i<tree[pos].vec.size(); i++)
        {
            if(_gcd(tree[pos].vec[i],g)>1LL)
            {
                fg=1LL;
                break;
            }
        }
        if(fg==0LL)
            return -(r-l+1LL);
    }
    ll mid= (l+r)/2LL;
    return query(pos*2LL,l,mid,ql,qr,g)+query(pos*2LL+1LL,mid+1LL,r,ql,qr,g);
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    ll n,q,x,y,g,i,com;

    scanf(" %lld",&n);

    for(i=1LL; i<=n; i++)
    {
        scanf(" %lld",&arr[i]);
    }

    construct_tree(1LL,1LL,n);


//    for(i=1LL; i<=10; i++)
//    {
//        printf("pos %lld: ",i);
//        for(ll j=0LL; j<tree[i].vec.size(); j++)
//        {
//            printf("%lld ",tree[i].vec[j]);
//        }
//        printf("\n");
//    }



    scanf(" %lld",&q);

    for(i=1LL; i<=q; i++)
    {
        scanf(" %lld",&com);
        if(com==1LL)
        {
            scanf(" %lld %lld",&x,&y);
            arr[x]=y;
            update(1LL,1LL,n,x);
//            for(ll ii=1LL; ii<=10; ii++)
//            {
//                printf("pos %lld: ",ii);
//                for(ll j=0LL; j<tree[ii].vec.size(); j++)
//                {
//                    printf("%lld ",tree[ii].vec[j]);
//                }
//                printf("\n");
//            }
        }
        else
        {
            scanf(" %lld %lld %lld",&x,&y,&g);
            printf("%lld\n",(y-x+1LL)-query(1LL,1LL,n,x,y,g));
        }
    }

    return 0;
}

