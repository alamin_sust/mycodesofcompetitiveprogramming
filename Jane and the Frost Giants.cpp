#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

int row,col,adj1[210][210],adj2[210][210];
char arr[210][210];

void rec1(int i,int j,int val)
{
    if(i>1&&arr[i-1][j]!='#'&&adj1[i-1][j]>val)
    {
        adj1[i-1][j]=val;
        rec1(i-1,j,val+1);
    }
    if(i<row&&arr[i+1][j]!='#'&&adj1[i+1][j]>val)
    {
        adj1[i+1][j]=val;
        rec1(i+1,j,val+1);
    }
    if(j>1&&arr[i][j-1]!='#'&&adj1[i][j-1]>val)
    {
        adj1[i][j-1]=val;
        rec1(i,j-1,val+1);
    }
    if(j<col&&arr[i][j+1]!='#'&&adj1[i][j+1]>val)
    {
        adj1[i][j+1]=val;
        rec1(i,j+1,val+1);
    }
    return;
}

void rec2(int i,int j,int val)
{
    if(i>1&&arr[i-1][j]!='#'&&adj2[i-1][j]>val)
    {
        adj2[i-1][j]=val;
        rec2(i-1,j,val+1);
    }
    if(i<row&&arr[i+1][j]!='#'&&adj2[i+1][j]>val)
    {
        adj2[i+1][j]=val;
        rec2(i+1,j,val+1);
    }
    if(j>1&&arr[i][j-1]!='#'&&adj2[i][j-1]>val)
    {
        adj2[i][j-1]=val;
        rec2(i,j-1,val+1);
    }
    if(j<col&&arr[i][j+1]!='#'&&adj2[i][j+1]>val)
    {
        adj2[i][j+1]=val;
        rec2(i,j+1,val+1);
    }
    return;
}

main()
{
    int mn,i,j,p,t,jane_i,jane_j;
    cin>>t;
    for(p=1; p<=t; p++)
    {
        cin>>row>>col;
        getchar();
        for(i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
            {
                scanf("%c",&arr[i][j]);
                if(arr[i][j]=='J')
                {
                    jane_i=i;
                    jane_j=j;
                }
                //if(arr[i][j]=='F')
                // {
                //   frost_i=i;
                //    frost_j=j;
                // }
                adj1[i][j]=inf;
                adj2[i][j]=inf;
            }
            getchar();
        }
        adj1[jane_i][jane_j]=0;
        rec1(jane_i,jane_j,1);
        //getchar();
        for(i=1; i<=row; i++)
            for(j=1; j<=col; j++)
            {
                if(arr[i][j]=='F')
                {
                    adj2[i][j]=0;
                    rec2(i,j,1);
                }
            }
     /*   for(i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
            {
                if(adj1[i][j]==inf)
                    printf("0");
                else
                    printf("%d",adj1[i][j]);
            }
            cout<<endl;
        }
        cout<<endl;
        for(i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
            {
                if(adj2[i][j]==inf)
                    printf("0");
                else
                    printf("%d",adj2[i][j]);
            }
            cout<<endl;
        }*/
        for(mn=inf,i=1; i<=row; i++)
        {
            for(j=1; j<=col; j++)
            {
                if(i==1||j==1||i==row||j==col)
                {
                    if(adj1[i][j]<adj2[i][j])
                    {
                        mn=min(mn,adj1[i][j]);
                    }
                }
            }

        }
        if(mn==inf)
        {
            printf("Case %d: IMPOSSIBLE\n",p);
        }
        else
            printf("Case %d: %d\n",p,mn+1);
    }
    return 0;
}

/*
2
4 5
##.##
#JF.#
#...#
#...#
3 3
###
#J.
#.F
*/
