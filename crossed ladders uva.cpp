#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.000000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

main()
{
    int t,p;
    double high,low,mid,x,y,c,now;
    while(cin>>x>>y>>c)
    {
        low=0;
        high=min(x,y);
        while(1)
        {
            mid=(high+low)/2;
            now=(c/tan(acos(mid/x)))+(c/tan(acos(mid/y)));
            if(fabs(mid-now)<eps)
                break;
            if(now>mid)
                high=mid;
            else
                low=mid;
        }
        printf("%.3lf\n",now);
    }
    return 0;
}

