#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include<string.h>
#define ll long long

using namespace std;

struct node{
string name;
ll id,pnt,t;
};

struct node2{
string name;
ll id,pnt;
};

bool operator<(node2 a,node2 b)
{
    if(a.pnt==b.pnt)
    return a.name>b.name;
    return a.pnt<b.pnt;
}

bool operator<(node a,node b)
{
    return a.t>b.t;
}



priority_queue<node>pq;
priority_queue<node2>pq2,pq3;

class ContestScoreboard
{
public:
	vector <int> findWinner(vector <string> scores)
	{
	    while(!pq.empty())pq.pop();
	    while(!pq2.empty())pq2.pop();
	    while(!pq3.empty())pq3.pop();
	    for(ll i=0LL;i<scores.size();i++)
        {
            for(ll j=0LL;j<scores[i].length();j++)
            {
                ll k=0LL;
                node det;
                node2 det2;
                det.name="";
                det.id=i;
                det.pnt=0;
                det.t=0;
                while(scores[i][k]!=' ')
                {
                    det.name+=scores[i][k];
                    k++;
                }
                k++;
                det2.name=det.name;
                det2.id=det.id;
                det2.pnt=0LL;
                pq2.push(det2);
                while(k<scores[i].length())
                {
                    string temp="";
                    while(k<scores[i].length()&&scores[i][k]!=' ')
                    {
                       temp+=scores[i][k];
                       k++;
                    }
                    k++;
                    for(ll kk=0LL;kk<temp.size();)
                    {
                        det.pnt=0LL;
                        det.t=0LL;
                        while(temp[kk]!='/')
                        {
                            det.pnt=(det.pnt*10LL)+(temp[kk]-'0');
                            kk++;
                        }
                        kk++;
                        while(kk<temp.size())
                        {
                            det.t=(det.t*10LL)+(temp[kk]-'0');
                            kk++;
                        }
                        pq.push(det);
                    }
                }
                j=k;
            }
        }

        ll arr[100010];
        memset(arr,0,sizeof(arr));
        ll prev=-1;
        while(!pq.empty())
        {
            node det=pq.top();
            node2 det2=pq2.top();
            pq.pop();
            if(det.t!=prev)
            arr[det2.id]=1;
            prev=det.t;
            while(!pq3.empty())
                pq3.pop();
            while(!pq2.empty())
            {
                det2=pq2.top();
                if(det2.id==det.id)
                    det2.pnt+=det.pnt;
                pq3.push(det2);
                pq2.pop();
            }
            pq2=pq3;
        }
        arr[pq2.top().id]=1;
        vector<int>ret;
        ret.clear();
        for(ll i=0;i<scores.size();i++)
        {
            if(arr[i])
                ret.push_back(1);
            else
                ret.push_back(0);

        }
	    return ret;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	ContestScoreboard objectContestScoreboard;

	//test case0
	vector <string> param00;
	param00.push_back("TVG 1/1 1/2 1/3");
	param00.push_back("AJI 1/4 1/5 1/6");
	vector <int> ret0 = objectContestScoreboard.findWinner(param00);
	vector <int> need0;
	need0.push_back(1);
	need0.push_back(1);
	assert_eq(0,ret0,need0);

	//test case1
	vector <string> param10;
	param10.push_back("GLP 1/114 1/195 1/171 1/19 1/146 1/29");
	param10.push_back("BKPF 1/57 1/187 1/277 1/21 1/223 1/35");
	vector <int> ret1 = objectContestScoreboard.findWinner(param10);
	vector <int> need1;
	need1.push_back(1);
	need1.push_back(1);
	assert_eq(1,ret1,need1);

	//test case2
	vector <string> param20;
	param20.push_back("AAA 248/2 495/5 993/7");
	param20.push_back("BBB 244/6 493/7 990/10");
	param20.push_back("CCC 248/2 495/5 993/10");
	vector <int> ret2 = objectContestScoreboard.findWinner(param20);
	vector <int> need2;
	need2.push_back(1);
	need2.push_back(0);
	need2.push_back(0);
	assert_eq(2,ret2,need2);

	//test case3
	vector <string> param30;
	param30.push_back("UBA 10/2 30/4 25/3 999/1000");
	param30.push_back("UNC 1/3 3/20 40/50");
	param30.push_back("UNLP 2/2 3/3 4/4 100/100");
	param30.push_back("UNR 999/1000000 999/999999");
	param30.push_back("UNS 999/100000000");
	vector <int> ret3 = objectContestScoreboard.findWinner(param30);
	vector <int> need3;
	need3.push_back(1);
	need3.push_back(0);
	need3.push_back(1);
	need3.push_back(1);
	need3.push_back(0);
	assert_eq(3,ret3,need3);

}
