/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))
#define MAX 1010

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};


int sieve[MAX+10],prime[MAX+10],pcnt=0;
char in[110];
vector<int>v,v2;

void sieve_(void)
{
    sieve[0]=sieve[1]=1;
    for(int i=2;i<=MAX;i++)
    {
        if(sieve[i]==0)
        {
            prime[pcnt++]=i;
            for(int j=2;i*j<=MAX;j++)
                sieve[i*j]=1;
        }
    }
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    int i;
    sieve_();

    for(i=2;i<=100;i++)
        v.push_back(i);

    int now=0;
//
//    for(i=1;i<=20;i++)
//        printf("%d %d\n",i,prime[i]);
    int p=0;
    int yy=0;
    while(p<20)
    {
        p++;
        v2.clear();
        printf("%d\n",prime[now]);
        fflush(stdout);
        scanf(" %s",in);

        if(in[0]=='y')
        {
            yy++;
            for(i=0;i<v.size();i++)
            {
                if((v[i]%prime[now])==0)
                {
                    v2.push_back(v[i]);
                }
            }

        }
        else
        {
            for(i=0;i<v.size();i++)
            {
                if((v[i]%prime[now])!=0)
                {
                    v2.push_back(v[i]);
                }
            }
        }

        v.clear();

        v=v2;

        now++;
    }

    if(yy<2)
    printf("prime\n");
    else
    printf("composite\n");
    fflush(stdout);

    return 0;
}

