/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

map<ll,ll>mpp1,mpp2;
ll v[1010];
ll dp[505][505],cnt,ncr[1010][1010],n,m,c,M=1000000007LL,now,res[1010];

ll rec(ll pos,ll taken)
{
    //printf("%d %d\n",pos,taken);
    if(pos==n)
    {
        if(taken<=now&&(now-taken)<=cnt)
        {
            //printf("%d %d\n",taken,val);
            //res[taken-1]=(res[taken-1]+val)%M;
            return 1LL*(ncr[cnt][now-taken]);
        }
        return 0LL;
    }
    ll &ret=dp[pos][taken];
    if(ret!=-1LL)
        return ret;
    ret=0LL;
    ret=(ret+(rec(pos+1LL,taken+1LL)*v[pos])%M)%M;
    ret=(ret+rec(pos+1LL,taken))%M;
    return ret;
}


int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    ll i,in;

    scanf(" %lld %lld %lld",&n,&m,&c);

    for(i=0LL;i<n;i++)
    {
        scanf(" %lld",&in);
        mpp1[in]++;
    }
    for(i=0LL;i<m;i++)
    {
        scanf(" %lld",&in);
        mpp2[in]++;
    }
    n=0LL;
    cnt=0LL;
    for(i=1LL;i<=c;i++)
    {
        if(mpp1[i]&&mpp2[i])
        {
            if((mpp1[i]*mpp2[i])==1LL)
                cnt++;
            else
            v[n++]=(mpp1[i]*mpp2[i]);
        }
    }

    for(i=0LL;i<=1000LL;i++)
    {
        for(ll j=0LL;j<=1000LL;j++)
        {
            ncr[i][j]=1LL;
        }
    }
    for(i=2LL;i<=1000;i++)
    {
        for(ll j=1LL;j<i;j++)
        {

            ncr[i][j]=(ncr[i-1][j]+ncr[i-1][j-1])%M;
            //if(i<=5&&j<=5)
           // printf("%d ",ncr[i][j]);
        }
       // if(i<=5)
       // printf("\n");
    }

    /*for(i=0LL;i<n;i++)
    {
        memset(dp,-1LL,sizeof(dp));
        now=i;
        res[i]=rec(0LL,0LL);
    }*/


    for(i=1LL;i<=c;i++)
    {
        memset(dp,-1LL,sizeof(dp));
        now=i+1;
        printf("%lld",rec(0LL,0LL));
        if(i==c)
            printf("\n");
        else
            printf(" ");

    }
    return 0;
}

/*
6 7 4
1 1 2 3 4 4
1 2 3 3 3 4 4
*/
