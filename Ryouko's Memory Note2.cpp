#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
ll l,r;
};
node det;
vector<node>v[100010];
pair<ll,ll>pii;
map<pair<ll,ll>,ll>mpp;

ll arr[100010],store[600010],res,mi=1,res2;
ll func(ll to,ll from)
{
    pii.first=from;
    pii.second=to;
    //det.l=from;
    //det.r=to;
    if(mpp[pii]>0)
       return store[mpp[pii]];
    ll ret=0;
    for(ll i=0;i<v[from].size();i++)
    {
        if(v[from][i].l==-1)
        {
            ret+=abs(from-v[from][i].r)-abs(to-v[from][i].r);
        }
        else if(v[from][i].r==-1)
        {
            ret+=abs(from-v[from][i].l)-abs(to-v[from][i].l);
        }
        else
        {
            ret+=abs(from-v[from][i].l)-abs(to-v[from][i].l);
            ret+=abs(from-v[from][i].r)-abs(to-v[from][i].r);
        }
    }
    mpp[pii]=mi;
   return store[mi++]=(res2-ret);
}

main()
{
    ll i,n,k;
    cin>>n>>k;
    for(i=1;i<=k;i++)
    {
       scanf(" %I64d",&arr[i]);
       if(i!=1)
       res+=abs(arr[i]-arr[i-1]);
    }
    for(i=1;i<=k;i++)
    {
        if(i==1&&i!=k)
        {
           det.l=-1;
           det.r=arr[i+1];
           v[arr[i]].push_back(det);
        }
        else if(i==k&&i!=1)
        {
           det.l=arr[i-1];
           det.r=-1;
           v[arr[i]].push_back(det);
        }
        else if((i+1)<=k&&(i-1)>=1)
        {
            det.l=arr[i-1];
           det.r=arr[i+1];
           v[arr[i]].push_back(det);
        }
    }
    res2=res;
    for(i=1;i<=k;i++)
    {
        if(i==1&&i!=k)
        {
            res=min(res,func(arr[i+1],arr[i]));
        }
        else if(i==k&&i!=1)
        {
            res=min(res,func(arr[i-1],arr[i]));
        }
        else if((i+1)<=k&&(i-1)>=1)
        {
            res=min(res,func(abs(arr[i+1]-arr[i-1])/2LL,arr[i]));
            res=min(res,func(abs(arr[i+1]-arr[i-1])/2LL+1,arr[i]));
            res=min(res,func(arr[i-1],arr[i]));
            res=min(res,func(arr[i+1],arr[i]));
        }
    }
    cout<<res<<endl;
    return 0;
}

