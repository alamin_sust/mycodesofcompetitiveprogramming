#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

main()
{
    ll t,p,a,lcm,lb,i;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %lld %lld",&a,&lcm);
        if(lcm%a!=0)
            {printf("NO SOLUTION\n");
            continue;}
        lb=lcm/a;
        for(i=lb;i<=lcm;i+=lb)
        {
            if((i*a/__gcd(i,a))==lcm)
                break;
        }
        printf("%lld\n",i);
    }
    return 0;
}

