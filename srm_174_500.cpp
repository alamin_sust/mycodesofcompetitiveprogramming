#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class BirthdayOdds
{
public:
	int minPeople(int minOdds, int daysInYear)
	{
	    double k=1.0;
	    for(int i=1;;i++)
        {
            k*=((double)daysInYear-i+1)/(double)daysInYear;
            if((100.0-k*100.0)>=(double)minOdds)
                return i;
        }
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	BirthdayOdds objectBirthdayOdds;

	//test case0
	int param00 = 75;
	int param01 = 5;
	int ret0 = objectBirthdayOdds.minPeople(param00,param01);
	int need0 = 4;
	assert_eq(0,ret0,need0);

	//test case1
	int param10 = 50;
	int param11 = 365;
	int ret1 = objectBirthdayOdds.minPeople(param10,param11);
	int need1 = 23;
	assert_eq(1,ret1,need1);

	//test case2
	int param20 = 1;
	int param21 = 365;
	int ret2 = objectBirthdayOdds.minPeople(param20,param21);
	int need2 = 4;
	assert_eq(2,ret2,need2);

	//test case3
	int param30 = 84;
	int param31 = 9227;
	int ret3 = objectBirthdayOdds.minPeople(param30,param31);
	int need3 = 184;
	assert_eq(3,ret3,need3);
}
