#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class ModModMod
{
public:
    long long findSum(vector <int> m, int R)
    {
        R++;
        long long ret=0LL;
        sort(m.begin(),m.end());
        int r=R/m[0];

        ret+=r*(m[0]*(m[0]-1)/2);
        r=R%m[0];
        ret+=r*(r-1)/2;
        //if(i<=102)
        //   printf("%d\n",r);


        return ret;
    }
};


template<typename T> void print( T a )
{
    cerr << a;
}

void print( long long a )
{
    cerr << a << "L";
}

void print( string a )
{
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a )
{
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ )
    {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need )
{
    if ( have == need )
    {
        cerr << "Case " << n << " passed." << endl;
    }
    else
    {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need )
{
    if( have.size() != need.size() )
    {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ )
    {
        if( have[i] != need[i] )
        {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need )
{
    if ( have == need )
    {
        cerr << "Case " << n << " passed." << endl;
    }
    else
    {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

    ModModMod objectModModMod;

    //test case0
    vector <int> param00;
    param00.push_back(5);
    param00.push_back(3);
    param00.push_back(2);
    int param01 = 10;
    long long ret0 = objectModModMod.findSum(param00,param01);
    long long need0 = 4;
    assert_eq(0,ret0,need0);

    //test case1
    vector <int> param10;
    param10.push_back(33);
    param10.push_back(15);
    param10.push_back(7);
    param10.push_back(10);
    param10.push_back(100);
    param10.push_back(9);
    param10.push_back(5);
    int param11 = 64;
    long long ret1 = objectModModMod.findSum(param10,param11);
    long long need1 = 92;
    assert_eq(1,ret1,need1);

    //test case2
    vector <int> param20;
    param20.push_back(995);
    param20.push_back(149);
    param20.push_back(28);
    param20.push_back(265);
    param20.push_back(275);
    param20.push_back(107);
    param20.push_back(555);
    param20.push_back(241);
    param20.push_back(702);
    param20.push_back(462);
    param20.push_back(519);
    param20.push_back(212);
    param20.push_back(362);
    param20.push_back(478);
    param20.push_back(783);
    param20.push_back(381);
    param20.push_back(602);
    param20.push_back(546);
    param20.push_back(183);
    param20.push_back(886);
    param20.push_back(59);
    param20.push_back(317);
    param20.push_back(977);
    param20.push_back(612);
    param20.push_back(328);
    param20.push_back(91);
    param20.push_back(771);
    param20.push_back(131);
    int param21 = 992363;
    long long ret2 = objectModModMod.findSum(param20,param21);
    long long need2 = 12792005;
    assert_eq(2,ret2,need2);

    //test case3
    vector <int> param30;
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    param30.push_back(100);
    int param31 = 100;
    long long ret3 = objectModModMod.findSum(param30,param31);
    long long need3 = 4950;
    assert_eq(3,ret3,need3);

    //test case4
    vector <int> param40;
    param40.push_back(2934);
    int param41 = 10000000;
    long long ret4 = objectModModMod.findSum(param40,param41);
    long long need4 = 14664070144;
    assert_eq(4,ret4,need4);

}
