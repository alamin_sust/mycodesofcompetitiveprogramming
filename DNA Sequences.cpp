/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int dp[1010][1010][2],l1,l2,n;
char arr1[1010],arr2[1010];

int rec(int x,int y,int fg)
{
    //printf("%d %d\n",x,y);
    if(x>=l1||y>=l2)
        return 0;
    int &ret=dp[x][y][fg];
    if(ret!=-1)
        return ret;
    int i=x,j=y,k=0;
    for(i=x,j=y;i<l1&&j<l2;i++,j++)
    {
        if(arr1[i]!=arr2[j])
           break;
        k++;
    }
    ret=0;
    fg=0;
    if(k>=n)
        fg=1,ret=k+rec(i,j,fg);
    //printf("%d\n",k);

    ret=max(ret,rec(i+1,j,fg));
    ret=max(ret,rec(i,j+1,fg));
    return ret;
}

main()
{

    while(cin>>n&&n)
    {
        scanf(" %s",arr1);
        scanf(" %s",arr2);
        l1=strlen(arr1);
        l2=strlen(arr2);
        memset(dp,-1,sizeof(dp));
        printf("%d\n",rec(0,0,0));
    }
    return 0;
}
