#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#define MAX 1000001
#define REP(i,a) for(i=0;i<a;i++)
using namespace std;

long long int M[MAX];
long long int es(long long int i)
{
    long long int b,c,d;
    double f;
    f=double(i);
    b=(long long int)(f-sqrt(f));
    c=(long long int)log(f);
    d=(long long int)(f*sin(f)*sin(f));

    if(M[b]==-1)
    {
        es(b);

    }
    if(M[c]==-1)
        es(c);
    if(M[d]==-1)
        es(d);

    M[i]=(M[b]+M[c]+M[d])%1000000;

}

main()
{

    long long int i;
    double f;
    long long int b;


    REP(i,MAX)
    M[i]=-1;
    M[0]=1;
    while(scanf("%lld",&i)!=EOF)
    {
        if(i==-1)
            break;
        if(i>0)
        {
            es(i);
        }
        printf("%lld\n",M[i]);
    }

    return 0;
}
