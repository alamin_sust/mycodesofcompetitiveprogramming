/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int rr[]= {0,0,1,-1};
int cc[]= {-1,1,0,0};

char arr[1010][1010];
int n,m,a,b,val[1010][1010],stat[1010][1010],res[1010][1010];
queue<pair<int,int> >q;

void bfs(void)
{
    while(!q.empty())
        q.pop();
    memset(stat,0,sizeof(stat));
    memset(val,0,sizeof(val));
    memset(res,0,sizeof(res));
    stat[0][0]=1;
    val[0][0]=0;
    res[0][0]=0;
    q.push(make_pair(0,0));
    while(!q.empty())
    {
        pair<int,int> u = q.front();
        q.pop();
        for(int i=0; i<4; i++)
        {
            pair<int,int> v;
            v.first=u.first+rr[i];
            v.second=u.second+cc[i];
            if(arr[v.first][v.second]=='.'&&v.first>=0&&v.first<n&&v.second>=0&&v.second<m&&stat[v.first][v.second]==0)
            {
                stat[v.first][v.second]=1;
                val[v.first][v.second]=val[u.first][u.second]+1;
                if(val[v.first][v.second]%2)
                    res[v.first][v.second]=res[u.first][u.second]+b;
                else
                    res[v.first][v.second]=res[u.first][u.second]+a;
                q.push(v);
            }
        }
    }
    if(stat[n-1][m-1]&&arr[0][0]=='.')
    {
        printf("%d\n",res[n-1][m-1]);
    }
    else
        printf("IMPOSSIBLE\n");
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    int i,j;

    while(scanf("%d%d",&m,&n)==2)
    {
        scanf(" %d %d",&a,&b);
        for(i=0; i<n; i++)
        {
            scanf(" %s",&arr[i]);
        }
        bfs();
    }
    return 0;
}


