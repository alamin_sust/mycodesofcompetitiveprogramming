#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int dp[55][102][102][3],arr[110];

int rec(int lev,int tl,int tr,int state)
{
    if(tl>=tr)
        return 0;
    int &ret=dp[lev][tl][tr][state];
    if(ret!=-1)
        return ret;
    ret=0;
    int cum=0;
    for(int i=tl+1;i<tr;i++)
    {
        cum+=arr[i];
        ret=max(cum-rec(lev+1,i,tr,state==0?1:0));
    }
    cum=0;
    for(int i=tr-1;i>tl;i--)
    {
        cum+=arr[i];
        ret=max(cum-rec(lev+1,tl,i,state==0?1:0)));
    }
    return ret;
}

main()
{
    int t,p,n,i,res;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        memset(dp,-1,sizeof(dp));
        cin>>n;
        for(i=1;i<=n;i++)
            scanf(" %d",&arr[i]);
        res=rec(0,0,n+1,0);
        cout<<res<<endl;
    }
    return 0;
}

