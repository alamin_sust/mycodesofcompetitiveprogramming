
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;


main()
{
    int adj[510][510],cost[510][510],i,j,k,n,m,q,from,to,p=1,val;
    while(1)
    {
        cin>>n>>m>>q;
        if(n==0)
            break;
        for(i=1; i<=n; i++)
        {
            for(j=1; j<=n; j++)
            {
                cost[i][j]=inf;
                if(j==i)
                    adj[i][j]=0;
                else
                    adj[i][j]=inf;
            }
        }
        for(i=1; i<=n; i++)
        {
            scanf(" %d",&cost[i][i]);
        }
        for(i=1; i<=m; i++)
        {
            scanf(" %d %d %d",&from,&to,&val);
            adj[from][to]=val;
            adj[to][from]=val;
            cost[from][to]=max(cost[from][from],cost[to][to]);
            cost[to][from]=cost[from][to];
        }
        for(k=1; k<=n; k++)
        {
            for(i=1; i<=n; i++)
            {
                for(j=1; j<=n; j++)
                {
                    if((adj[i][j]+cost[i][j])>(adj[i][k]+adj[k][j]+max(cost[i][k],cost[k][j])))
                    {
                        adj[i][j]=adj[i][k]+adj[k][j];
                        cost[i][j]=max(cost[i][k],cost[k][j]);
                    }
                }
            }
        }
        for(k=1; k<=n; k++)
        {
            for(i=1; i<=n; i++)
            {
                for(j=1; j<=n; j++)
                {
                    if((adj[i][j]+cost[i][j])>(adj[i][k]+adj[k][j]+max(cost[i][k],cost[k][j])))
                    {
                        adj[i][j]=adj[i][k]+adj[k][j];
                        cost[i][j]=max(cost[i][k],cost[k][j]);
                    }
                }
            }
        }
        if(p!=1)
        printf("\n");
        printf("Case #%d\n",p++);
        for(i=1;i<=q;i++)
        {
            scanf("%d %d",&from,&to);
            if(adj[from][to]+cost[from][to]>=inf)
                printf("-1\n");
            else
                printf("%d\n",adj[from][to]+cost[from][to]);

        }
    }
    return 0;
}
