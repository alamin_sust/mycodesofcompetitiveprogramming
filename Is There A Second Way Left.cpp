#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node
{
    ll x,y,val,ind;
};
node det;
bool operator<(node a,node b)
{
    return a.val>b.val;
}

priority_queue<node>pq;
priority_queue<ll>pq2;
ll par[110],edges[210][5];

void par_in(ll n)
{
    for(ll i=1; i<=n; i++)
        par[i]=i;
    return;
}

ll check_if_forest(ll n)
{
    ll flag=0,i,a,bef=-1;
    for(i=1; i<=n; i++)
    {
        a=i;
        while(par[a]!=a)
            a=par[a];
        if(a!=bef&&bef!=-1)
        {
            flag=1;
            break;
        }
        bef=a;
    }
    return flag;
}


main()
{
    ll t,p,n,m,flag,i,a,b,c,res,res2;
    cin>>t;
    for(p=1; p<=t; p++)
    {
        cin>>n>>m;
        flag=0;
        par_in(n);
        while(!pq.empty())
            pq.pop();
        for(i=1; i<=m; i++)
        {
            scanf(" %lld %lld %lld",&a,&b,&c);
            edges[i][0]=a;
            edges[i][1]=b;
            edges[i][2]=c;
            det.x=a;
            det.y=b;
            det.val=c;
            det.ind=i;
            pq.push(det);
        }
        while(!pq2.empty())
            pq2.pop();
        while(!pq.empty())
        {
            a=pq.top().x;
            b=pq.top().y;
            c=pq.top().val;
            while(par[a]!=a)
                a=par[a];
            while(par[b]!=b)
                b=par[b];
            if(a!=b)
            {
                par[a]=b;
                pq2.push(pq.top().ind);
            }
            pq.pop();
        }
        flag=check_if_forest(n);
        if(flag==1)
        {
            printf("Case #%lld : No way\n",p);
            continue;
        }
        res2=999999999999999LL;
        while(!pq2.empty())
        {
            res=0;
            a=pq2.top();
            par_in(n);
            for(i=1; i<=m; i++)
            {
                if(i==a)
                    continue;
                det.x=edges[i][0];
                det.y=edges[i][1];
                det.val=edges[i][2];
                det.ind=i;
                pq.push(det);
            }
            while(!pq.empty())
            {
                a=pq.top().x;
                b=pq.top().y;
                c=pq.top().val;
                while(par[a]!=a)
                    a=par[a];
                while(par[b]!=b)
                    b=par[b];
                if(a!=b)
                {
                    res+=c;
                    par[a]=b;
                }
                pq.pop();
            }
            if(check_if_forest(n)==0)
            {
            flag=1;
            res2=min(res,res2);
            }
            pq2.pop();
        }
        if(flag==1)
            printf("Case #%lld : %lld\n",p,res2);
        else
            printf("Case #%lld : No second way\n",p);
    }
    return 0;
}

