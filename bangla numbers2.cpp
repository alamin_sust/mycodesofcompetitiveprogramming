/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll dig[110],n,p,res[110];
string arr[110];
ll i,k;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    dig[0]=100;
    arr[0]="shata";
    dig[1]=10;
    arr[1]="hajar";
    dig[2]=100;
    arr[2]="lakh";
    dig[3]=100;
    arr[3]="kuti";
    dig[4]=100;
    arr[4]="shata";
    dig[5]=10;
    arr[5]="hajar";
    dig[6]=100;
    arr[6]="lakh";
    dig[7]=100;
    arr[7]="kuti";
    dig[8]=100;
    arr[8]="shata";
    dig[9]=10;
    arr[9]="hajar";

    while(scanf("%lld",&n)!=EOF)
    {
        p++;
        k=0;
        memset(res,0,sizeof(res));
        while(n)
        {
           res[k]=(n%dig[k]);
           n/=dig[k];
           k++;
        }
        if(k>0)
    k--;
   ll    flag=1;
       printf("%4lld. %lld",p,res[k]);
       for(ll i=k-1;i>=0;i--)
       {
           if(flag==1||i==3||i==7)
           cout<<" "<<arr[i];
           flag=0;
           if(res[i])
           printf(" %lld",res[i]),flag=1;
       }
       printf("\n");
   }

    return 0;
}




