#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class IdentifyingWood
{
public:
	string check(string s, string t)
	{
	    for(int i=0,j=0;i<s.size()&&j<t.size();i++)
        {
            if(s[i]==t[j])
                j++;
            if(j==t.size())
                return "Yep, it's wood.";
        }
        return "Nope.";
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	IdentifyingWood objectIdentifyingWood;

	//test case0
	string param00 = "absdefgh";
	string param01 = "asdf";
	string ret0 = objectIdentifyingWood.check(param00,param01);
	string need0 = "Yep, it's wood.";
	assert_eq(0,ret0,need0);

	//test case1
	string param10 = "oxoxoxox";
	string param11 = "ooxxoo";
	string ret1 = objectIdentifyingWood.check(param10,param11);
	string need1 = "Nope.";
	assert_eq(1,ret1,need1);

	//test case2
	string param20 = "oxoxoxox";
	string param21 = "xxx";
	string ret2 = objectIdentifyingWood.check(param20,param21);
	string need2 = "Yep, it's wood.";
	assert_eq(2,ret2,need2);

	//test case3
	string param30 = "qwerty";
	string param31 = "qwerty";
	string ret3 = objectIdentifyingWood.check(param30,param31);
	string need3 = "Yep, it's wood.";
	assert_eq(3,ret3,need3);

	//test case4
	string param40 = "string";
	string param41 = "longstring";
	string ret4 = objectIdentifyingWood.check(param40,param41);
	string need4 = "Nope.";
	assert_eq(4,ret4,need4);
}
