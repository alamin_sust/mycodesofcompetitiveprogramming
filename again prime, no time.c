#include<stdio.h>

main()
{
    long long unsigned int t,i,n,m,fac,k,p;
    scanf(" %llu",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %llu %llu",&m,&n);
        k=0;
        for(fac=1,i=1;i<=n;i++)
        {
            fac*=i;
            while(fac%m==0)
            {k++;
            fac/=m;}
            if(fac>m)
            {
                fac=fac%m;
            }
        }
        if(k!=0)
        printf("Case %llu:\n%lld\n",p,k);
        else
        printf("Case %llu:\nImpossible to divide\n",p);
    }
    return 0;
}
