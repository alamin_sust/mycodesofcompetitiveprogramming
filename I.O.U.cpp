#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

long long int i,res,a[110],adj[110][110],n,m,k,j,x,y,c;

main()
{
    cin>>n>>m;
    memset(adj,-1,sizeof(adj));
    for(i=1;i<=m;i++)
    {
        scanf(" %I64d %I64d %I64d",&x,&y,&c);
        adj[x][y]=c;
        a[x]+=c;
        res+=c;
    }
    for(k=1;k<=n;k++)
    {
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
            {
                if(adj[i][k]!=-1&&adj[k][j]!=-1)
                {
                    if(adj[i][j]<min(adj[i][k],adj[k][j]))
                        adj[i][j]=min(adj[i][k],adj[k][j]);
                }
            }
        }
    }
    for(k=1;k<=n;k++)
    {
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
            {
                if(adj[k][i]!=-1&&adj[k][j]!=-1)
                {
                    if(a[k]>=min(adj[k][i],adj[k][j]))
                    res-=min(adj[k][i],adj[k][j]),a[k]-=min(adj[k][i],adj[k][j]);
                }
            }
        }
    }
    printf("%I64d\n",res);
    return 0;
}
