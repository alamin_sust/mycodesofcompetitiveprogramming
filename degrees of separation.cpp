#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

main()
{
    int n,m,i,k,p=1,j,adj[110][110],mx,cnt,flag;
    char a[110],b[110];
    map<string,int>mpp;
    while(cin>>n>>m)
    {
        mpp.clear();
        if(n==0&&m==0)
            break;
        for(i=1; i<=n; i++)
            for(j=1; j<=n; j++)
                adj[i][j]=inf;
        cnt=0;
        for(i=1; i<=m; i++)
        {
            scanf(" %s %s",a,b);
            if(mpp[a]==0)
            {
                mpp[a]=++cnt;
            }
            if(mpp[b]==0)
            {
                mpp[b]=++cnt;
            }
            adj[mpp[a]][mpp[b]]=1;
            adj[mpp[b]][mpp[a]]=1;
            //cout<<mpp[a]<<mpp[b]<<endl;
        }
        for(k=1; k<=n; k++)
        {
            for(i=1; i<=n; i++)
            {
                for(j=1; j<=n; j++)
                {
                    if(adj[i][j]>(adj[i][k]+adj[k][j]))
                        adj[i][j]=adj[i][k]+adj[k][j];
                }
            }
        }
        flag=mx=0;
        for(i=1; i<=n; i++)
            for(j=i+1; j<=n; j++)
            {
                if(adj[i][j]==inf)
                {
                    flag=1;
                    break;
                }
                mx=max(mx,adj[i][j]);
            }
        if(flag)
            printf("Network %d: DISCONNECTED\n\n",p++);
        else
            printf("Network %d: %d\n\n",p++,mx);
    }
    return 0;
}

