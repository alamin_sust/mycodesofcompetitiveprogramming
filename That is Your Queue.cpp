#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
ll P,q,arr[1100],i,j,k,m,in,det[1100],p=1;
char ch[10];
map<ll,ll>mpp;
stack<ll>stk;
main()
{
    while(scanf("%lld%lld",&P,&q)!=EOF)
    {
        if( P == 0 && q == 0 ) break ;
        deque<ll>que;
        for(int i = 1 ; i <= p && i <= 1000 ; i++ )
            que.push_back(i);
        ll x;
        printf("Case %d:\n",p++);
        while(q--)
        {
            scanf(" %s",ch);
            if( ch[0] == 'N' )
            {
                x = que.front() ;
                que.pop_front();
                que.push_back(x);
                printf("%lld\n",x);
            }
            else
            {
                scanf("%lld",&x);
                for(__typeof ((que).end ()) i = (que).begin (); i != (que).end (); ++i)
                {
                    if(*i == x )
                    {
                        que.erase(i) ;
                        break ;
                    }
                }
                que.push_front(x);
            }
        }
    }
    return 0;
}
/*
3 6
N
N
E 1
N
N
N
10 2
N
N
0 0
*/
