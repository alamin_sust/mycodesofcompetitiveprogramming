#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

long long adj[1003][1003],n,res,p,l,i,j,val[1003][1003];
char arr[1003];

main()
{
    scanf(" %lld",&n);
    getchar();
    for(p=1; p<=n; p++)
    {
        res=0;
        gets(arr);
        l=strlen(arr);
        for(i=0; i<l; i++)
        {
            adj[i][0]=val[i][0]=0;
            for(j=0; j<i+1; j++)
            {
                if((adj[i][0]&(1<<(arr[j]-'a')))==0)
                    val[i][0]++;
                else val[i][0]--;
                adj[i][0]^=(1<<(arr[j]-'a'));
            }
            if(val[i][0]<2)
                res++;
            for(j=1; j<l-i; j++)
            {
                if((adj[i][j-1]&(1<<(arr[j-1]-'a')))==0)
                        val[i][j]=val[i][j-1]+1;
                else val[i][j]=val[i][j-1]-1;;
                adj[i][j]=adj[i][j-1]^(1<<(arr[j-1]-'a'));
                if((adj[i][j]&(1<<(arr[j+i]-'a')))==0)
                        val[i][j]++;
                else val[i][j]--;
                adj[i][j]=adj[i][j]^(1<<(arr[j+i]-'a'));
                if(val[i][j]<2)
                    res++;
            }
        }
        printf("Case %lld: %lld\n",p,res);
    }
    return 0;
}
