/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll t,n,xx[1000010],yy[1000010],col[1000010],low,mid,high;
queue<ll>q;
vector<ll>adj[1000010];

ll dst(ll p1,ll p2, ll mx) {

    return (abs(xx[p1]-xx[p2])+abs(yy[p1]-yy[p2]));

}

ll bfs() {
    ll u=0LL;
    while(!q.empty()) {
        q.pop();
    }
    q.push(u);
    col[u]=1LL;
    ll ret=1LL;

    while(!q.empty()) {
        u=q.front();
        q.pop();
        for(ll i=0LL;i<adj[u].size();i++) {
            ll v=adj[u][i];
            if(col[v]==0LL) {
                col[v]=1LL;
                ret++;
                q.push(v);
            }
        }
    }
    return ret;
}

bool func(ll mx) {
    for(ll i=0LL;i<n;i++) {
        adj[i].clear();
        col[i]=0LL;
    }

    for(ll i=0LL;i<n;i++) {
        for(ll j=0LL;j<i;j++) {
            if(dst(i,j,mx)>=mx) {
                adj[i].push_back(j);
                adj[j].push_back(i);
            }
        }
    }

    ll ret = bfs();
    return ret==n;
}

int main() {

    scanf(" %lld",&t);
    for(ll p=1LL; p<=t; p++)
    {

        scanf(" %lld",&n);

        for(ll i=0LL; i<n; i++)
        {
            scanf(" %lld %lld",&xx[i],&yy[i]);
        }

        low = 0LL;
        high = 2000000002LL;
        ll res =0LL;

        while(low<=high) {

            mid=(low+high)/2LL;
            //printf("%lld\n",mid);
            if(func(mid)) {
                low=mid+1LL;
                res=max(res,mid);
            } else {
                high = mid-1LL;
            }
        }

        printf("%lld\n",res);

    }

return 0;
}

