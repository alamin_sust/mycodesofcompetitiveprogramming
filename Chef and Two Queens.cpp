/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
int rr[]= {0,0,-1,1,1,1,-1,-1};
int cc[]= {-1,1,0,0,1,-1,1,-1};
using namespace std;

int t,p,n,m,x,y,res,mx,my;

int bfs(int xx,int yy)
{
    int ret = 0,i,j;
    for(int i=0; i<8; i++)
    {
        if(rr[i]==1) {
            mx = n-xx;
        } else if(rr[i]==-1) {
            mx = xx-1;
        } else mx=10000010;
        if(cc[i]==1) {
            my = m-yy;
        } else if(cc[i]==-1) {
            my = yy-1;
        } else my=10000010;
        ret+=min(mx,my);
    }

    if(x-xx==y-yy && (y-yy)>=0) {
        ret-=min(n-x,m-y);
    } else if(xx-x==yy-y && (yy-y)>=0) {
        ret-=min(x-1,y-1);
    } else if(y==yy&&x>xx) {
        ret-=n-x;
    } else if(y==yy&&xx>x) {
        ret-=x-1;
    } else if(x==xx&&y>yy) {
        ret-=m-y;
    } else if(x==xx&&yy>y) {
        ret-=y-1;
    } else if(x-xx==yy-y && (yy-y)>=0) {
        ret-=min(n-x,y-1);
    } else if(xx-x==y-yy && (y-yy)>=0) {
        ret-=min(x-1,m-y);
    } else return ret;
    return ret-1;
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    int i,j;

    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {
        scanf(" %d %d %d %d",&n,&m,&x,&y);
        res=(((n*m)-1)*((n*m)-2));
        for(i=1; i<=n; i++)
        {
            for(j=1; j<=m; j++)
            {
                if(i==x&&j==y)
                    continue;
                res-=bfs(i,j);
            }
        }
        printf("%d\n",res);
    }

    return 0;
}
