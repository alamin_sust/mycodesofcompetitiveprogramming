/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll t,p,n,k,arr[100010],res,mx,i,high,low,mid,sum;

int main()
{
    freopen("the_price_is_correct.txt","r",stdin);
    freopen("the_price_is_correct_output.txt","w",stdout);

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++)
    {
        scanf(" %lld %lld",&n,&k);

        for(i=1LL;i<=n;i++)
        {
            scanf(" %lld",&arr[i]);
            arr[i]+=arr[i-1];
        }
        res=0LL;
        for(i=1LL;i<=n;i++)
        {
            low=i;
            high=n;
            mx=0LL;
            while(low<=high)
            {
                mid=(low+high)/2LL;
                sum=arr[mid]-arr[i-1];
                if(sum<=k)
                {
                    mx=max(mid-i+1LL,mx);
                    low=mid+1LL;
                }
                else
                {
                    high=mid-1LL;
                }
            }
            res+=mx;
        }
        printf("Case #%lld: %lld\n",p,res);
    }
    return 0;
}
