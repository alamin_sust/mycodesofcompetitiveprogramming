/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

string s[110],res;
char arr[110][110];
int n,i,len,l[110],k,j,bound[110],con[30][30];
vector<int>adj[110];


struct node{
int id,deg;
};


node nd[110],now;

queue<node>q;

int comp(node a,node b)
{
    return a.deg<b.deg;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    cin>>n;
    for(i=1;i<=n;i++)
     bound[i]=0;
    l[0]=105;
    for(i=1; i<=n; i++)
    {
        scanf(" %s",&arr[i]);
        l[i]=strlen(arr[i]);
        len=max(l[i],len);
    }
    k=0;

    for(i=0; i<len; i++)
    {

        for(j=1; j<=n; j++)
        {
            s[j]+=arr[j][i];
            if(j==1)
                continue;
            if(i>=l[j]&&i<l[j-1]&&bound[j]==bound[j-1])
            {
                printf("Impossible\n");
                return 0;
            }
            if(i<l[j]&&i<l[j-1]&&bound[j]==bound[j-1]&&s[j]!=s[j-1])
            {
                con[arr[j-1][i]-'a'][arr[j][i]-'a']=1;
                bound[j-1]=++k;
            }
            if(s[j]==s[j-1])
                bound[j-1]=bound[j];
        }
    }
   // printf(" %d %d\n",con['s'-'a']['o'-'a'],con['o'-'a']['s'-'a']);
    for(i=0;i<26;i++)
    {
        nd[i].id=i;
        for(j=0;j<26;j++)
        {
            if(i!=j&&con[i][j])
            {
                adj[i].push_back(j);
                nd[j].deg++;
            }
        }
    }
  printf("%d-- %d\n",nd[18].deg,nd[18].id);
    sort(nd,nd+26,comp);
//printf("%d-- %d\n",nd[25].deg,nd[25].id);
    for(i=0;i<26;i++)
    {
        if(nd[i].deg==0)
            q.push(nd[i]),printf("%d..\n",nd[i].id);
    }
    node det;
    while(!q.empty())
    {
        now=q.front();
        q.pop();
        printf("%d %d\n",now.id,now.deg);
        res+=(char)(now.id+'a');
        for(i=0;i<adj[now.id].size();i++)
        {
            nd[adj[now.id][i]].deg--;
            if(nd[adj[now.id][i]].deg==0)
            {det.deg=0,det.id=adj[now.id][i];
                q.push(det);}
        }
    }
    //if(res.size()!=26)
    //    printf("Impossible\n");
   // else
        cout<<res<<"\n";
    return 0;
}




