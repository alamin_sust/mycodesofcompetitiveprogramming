#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#define pi 2*acos(0)
#define inf INT_MAX
using namespace std;

int dp[1010][110],n,in,w[1010],c[1010],profit1,profit2;

int knapsack(int i,int weight)
{
    if(i==n+1)
    return 0;
    if(dp[i][weight]!=-1)
    return dp[i][weight];
    int profit1=0,profit2;
    if((weight+w[i])<=in)
    profit1=c[i]+knapsack(i+1,weight+w[i]);
    //printf("%d\n",weight);
    profit2=knapsack(i+1,weight);
    //printf(" .%d %d\n",profit1,profit2);
    return dp[i][weight]=max(profit1,profit2);
}

main()
{
    int p,t,i,j,q,res;
    scanf(" %d",&t);
    for(i=1;i<=t;i++)
    {
        scanf(" %d",&n);
        for(j=1;j<=n;j++)
        {
        scanf(" %d",&c[j]);
        scanf(" %d",&w[j]);
        }
        //for(j=0;j<=n;j++)
        //{
       // printf(" %d",c[j]);
       // printf(" %d.\n",w[j]);
        //}
        scanf(" %d",&q);
        for(j=1,res=0;j<=q;j++)
        {
            memset(dp,-1,sizeof(dp));
            scanf(" %d",&in);
            res+=knapsack(1,0);
        }
        printf("%d\n",res);
    }
    return 0;
}
