/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll dp[5005][5005],val[5010][20],l,MOD=1000000007LL;
char arr[5010];

ll func(ll l1,ll l2)
{
    if(l1==0LL&&l2==0LL)
        return 0LL;
    ll a,b,p;
    for(ll i=l1,j=l2;i<l2;j+=p,i+=p)
    {
        p=min(18LL,l2-i);
        a=val[i][p];
        b=val[j][p];
        if(a>b)
            return 0LL;
        if(a<b)
            return 1LL;
    }
    return 0LL;
}

ll rec(ll prev,ll pos)
{
     if(pos==l)
        return 1LL;
     if(pos>l)
        return 0LL;
     if((l-pos)<(pos-prev))
        return 0LL;
     if(arr[pos]=='0')
        return 0LL;
     ll &ret=dp[prev][pos];
     if(ret!=-1LL)
        return ret;
     ret=0LL;
     if(func(prev,pos))
        ret=(ret+rec(pos,pos+(pos-prev)))%MOD;
     for(ll i=pos+(pos-prev)+1LL;i<=l;i++)
        ret=(ret+rec(pos,i))%MOD;
     return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);


    ll i,j,k,v;

    scanf(" %I64d",&l);

    scanf(" %s",arr);

    memset(dp,-1,sizeof(dp));

    for(i=0LL;i<l;i++)
    {
        for(j=1LL;j<=18LL;j++)
        {
            if((i+j-1LL)<l)
            {
                v=0LL;
                for(k=0LL;k<j;k++)
                {
                    v=(v*10LL)+(arr[i+k]-'0');
                }
                val[i][j]=v;
            }
        }
    }

    printf("%I64d\n",rec(0LL,0LL));

    return 0;
}

