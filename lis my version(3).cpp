#include<stdio.h>
#include<iostream>
#include<string.h>
using namespace std;

int arr[1010],mem[1010];

int lis(int n)
{
    int res=0;
    for(int i=1;i<=n;i++)
    {
        if(arr[i]<arr[n])
            {
                if(res<mem[i])
                res=mem[i];
            }
    }
    mem[n]=res+1;
    return mem[n];
}

main()
{
    int n,i,mx;
    scanf(" %d",&n);
    memset(mem,0,sizeof(mem));
    for(i=1;i<=n;i++)
        scanf(" %d",&arr[i]);
    for(mx=1,i=1;i<=n;i++)
    {
        mem[i]=1;
        int a=lis(i);
        if(mx<a)
            mx=a;
    }
    printf("%d\n",mx);
    return 0;
}
