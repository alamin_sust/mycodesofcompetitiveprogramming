/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int p,t,i,cur,k,n,loop,nowK,j,res,baki;

int main()
{

    freopen("ethan_finds_the_shortest_path.txt","r",stdin);
    freopen("ethan_finds_the_shortest_path_output.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++) {
        scanf(" %d %d",&n,&k);
        printf("Case #%d:",p);
        if(n==2) {
            printf(" 0\n");
            printf("1\n");
            printf("1 2 %d\n",k);
            continue;
        }
        if(k<3) {
            printf(" 0\n");
            printf("%d\n",n-1);
            printf("1 %d %d\n",n,k);
            cur=n;
            for(i=2;i<n;i++) {
                printf("%d %d %d\n",cur,i,k);
                cur=i;
            }
            continue;
        }
        loop = min(k,n);
        res=0;

        for(i=k-1,j=1;j<loop;i--,j++) {
            res+=i;
        }
        res-=k;

        printf(" %d\n",res);
        printf("%d\n",n);
        nowK = k-1;
        for(i=1;i<loop;i++) {
            if(i==(loop-1)) {
                printf("%d %d %d\n",i,n,nowK);
                printf("1 %d %d\n",n,k);
            } else {
                printf("%d %d %d\n",i,i+1,nowK);
            }
            nowK--;
        }
        baki = n-loop;
        cur=n;
        for(j=0,i=n-1;j<baki;i--,j++) {
            printf("%d %d %d\n",cur,i,k);
            cur=i;
        }
    }

    return 0;
}
