/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int movx[]={1,0,-1,0};
int movy[]={0,-1,0,1};
int r,c,i,j,dir,x,y,p=1;
char arr[110][110];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    while(scanf("%d%d",&c,&r)!=EOF&&(r+c))
    {

        for(i=0;i<r;i++)
        {
            scanf("%s",&arr[i]);
            for(j=0;j<c;j++)
            {
                if(arr[i][j]=='*')
                {
                    x=i;
                    y=j;
                }
            }

        }
        if(x==0)
            dir=0;
        if(y==0)
            dir=3;
        if(x==r-1)
            dir=2;
        if(y==c-1)
            dir=1;
        while(1)
        {
            x+=movx[dir];
            y+=movy[dir];
            if(arr[x][y]=='/')
            {
                if(dir==0)
                    dir=1;
                else if(dir==1)
                    dir=0;
                else if(dir==2)
                    dir=3;
                else
                    dir=2;
            }
            else if(arr[x][y]=='\\')
            {
                if(dir==0)
                    dir=3;
                else if(dir==1)
                    dir=2;
                else if(dir==2)
                    dir=1;
                else
                    dir=0;
            }

            if(x==0||y==0||x==r-1||y==c-1)
            {arr[x][y]='&';
                break;}
        }
        printf("HOUSE %d\n",p++);
        for(i=0;i<r;i++)
        {
            for(j=0;j<c;j++)
            {
                printf("%c",arr[i][j]);
            }
            printf("\n");
        }
    }

    return 0;
}


