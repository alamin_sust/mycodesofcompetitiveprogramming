/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll tree[1000010],arr[100010];

void build(ll pos,ll x,ll y)
{
    if(x==y)
        {tree[pos]=arr[x];
        return;}
    ll low=x;
    ll high=y;
    ll mid=(low+high)/2LL;
    build(pos*2LL,low,mid);
    build(pos*2LL+1LL,mid+1LL,high);
    tree[pos]=max(tree[pos*2LL],tree[pos*2LL+1LL]);
    return;
}

ll qry(ll pos,ll l,ll r,ll x,ll y)
{
   // printf("%lld %lld %lld %lld %lld\n",pos,l,r,x,y);
   if(x<l&&y<l)
    return 0;
   if(x>r&&y>r)
    return 0;
    if(x>=l&&y<=r)
        return tree[pos];
    ll low=x;
    ll high=y;
    ll mid=(low+high)/2LL;
    ll ret=0LL;
        ret=max(ret,qry(pos*2LL,l,r,low,mid));
        ret=max(ret,qry(pos*2LL+1LL,l,r,mid+1LL,high));
    return ret;
}


int main()
{
    ll n,i,x,y,m,res;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    cin>>n;
    for(i=1LL;i<=n;i++)
    {
        scanf(" %lld",&arr[i]);
    }
    cin>>m>>x>>y;

    build(1LL,1LL,n);

    res=0LL;
    //for(in)
   // printf("%lld--\n",tree[1]);
    for(i=1LL;i<=m;i++)
    {
        res+=qry(1LL,min(x,y)+1LL,max(x,y)+1LL,1LL,n);
        //printf("%lld %lld %lld..\n",x,y,res);
        x =(x+7LL)%(n-1LL);
	    y=(y+11LL)%n;
    }
    printf("%lld\n",res);
    return 0;
}





