/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int n,arr[500100],flag[500100][3];
map<pair<int,int> , int>mpp;

int nxt(int pos,int tp)
{
    if(tp==0)
    {
        for(int i=pos+1;i<=n;i++)
        {
            if(arr[i]<arr[pos]) return i;
        }
    }
    if(tp==1)
    {
        for(int i=pos+1;i<=n;i++)
        {
            if(arr[i]>arr[pos]) return i;
        }
    }
    return n+10;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    int i,res,m,na,nb,sa,sb,a,b;

    scanf(" %d",&n);

    for(i=1; i<=n; i++)
        scanf(" %d",&arr[i]);

    for(i=1;i<=n;i++)
    {
        if(arr[i]==arr[i-1])
        {
            flag[i][0]=flag[i-1][0];
            flag[i][1]=flag[i-1][1];
            continue;
        }
        if(arr[i]>arr[i+1])
            flag[i][0]=i+1;
        else if(arr[i]<arr[i+1])
            flag[i][1]=i+1;
        if(flag[i][0]==0)
            flag[i][0]=nxt(i,0);
        if(flag[i][1]==0)
            flag[i][1]=nxt(i,1);
    }


    scanf(" %d",&m);

    for(i=1; i<=m; i++)
    {
        scanf(" %d %d",&a,&b);
        if(mpp[make_pair(a,b)]==1)
        {
            printf("1\n");
            continue;
        }
        if(mpp[make_pair(a,b)]==2)
        {
            printf("0\n");
            continue;
        }
        na=a;
        nb=b;
        res=0;
        sa=0;
        sb=0;
        while(na<=n&&nb<=n)
        {
            if(na==nb)
            {
                res=1;
                break;
            }
            if(na<nb)
            {
                if(sa==0)
                na=flag[na][0];
                else
                na=flag[na][1];
                sa^=1;
            }
            else
            {
                if(sb==0)
                nb=flag[nb][0];
                else
                nb=flag[nb][1];
                sb^=1;
            }

        }
        if(res==1)
            mpp[make_pair(a,b)]=1;
        else
            mpp[make_pair(a,b)]=2;
        printf("%d\n",res);
    }




    return 0;
}
