
/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

map<int,int>mpp;

int x,res,r1,r2,i,n,in;

int main()
{
    while(scanf("%d",&x)==1)
    {
        mpp.clear();
        x*=10000000LL;
        scanf(" %d",&n);
        res=-1LL;
        for(i=0;i<n;i++)
        {
            scanf(" %d",&in);
            if(mpp[x-in]>=1LL)
            {
                if(abs(x-in-in)>res)
                {

                    res=abs(x-in-in);
                    r1=min(x-in,in);
                    r2=max(x-in,in);
                }
            }
            mpp[in]++;
            //printf("..%I64d %I64d\n",in,mpp[in]);
        }
        if(res==-1LL)
        printf("danger\n");
        else
        printf("yes %d %d\n",r1,r2);
    }
    return 0;
}
