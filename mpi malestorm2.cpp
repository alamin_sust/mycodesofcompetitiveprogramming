#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int n,i,j,res,k,adj[110][110];
char in[110];

main()
{
    while(cin>>n)
    {
        for(i=2; i<=n; i++)
        {
            for(j=1; j<i; j++)
            {
                scanf(" %s",in);
                if(in[0]=='x')
                    adj[i][j]=adj[j][i]=inf;
                else
                    adj[i][j]=adj[j][i]=atoi(in);
            }
        }
        for(k=1; k<=n; k++)
        {
            for(i=1; i<=n; i++)
            {
                for(j=1; j<=n; j++)
                {
                    if(adj[i][j]>adj[i][k]+adj[k][j])
                        adj[i][j]=adj[i][k]+adj[k][j];
                }
            }
        }
        res=0;
        for(i=2; i<=n; i++)
        {
            res=max(res,adj[1][i]);
        }
        printf("%d\n",res);
    }
    return 0;
}

