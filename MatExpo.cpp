#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<iostream>
#include<string>
#include<sstream>
#include<vector>
#include<map>
#include<queue>
#include<math.h>
#include<algorithm>

#define pb push_back
#define PI acos(-1.0)
#define SZ(a) (int)a.size()
#define csprnt printf("Case %d: ", cas++);
#define EPS 1e-9
#define MAX 100010
#define INF (1<<30)
#define MOD (unsigned long long)(pow(2, 32))

using namespace std;

struct matrix{
    unsigned long long mat[36][36];
};

matrix base;

matrix mat_mul(matrix a, matrix b)
{
    int i, j, k;
    matrix ret;
    for(i=0;i<36;i++)
    {
        for(j=0;j<36;j++)
        {
            ret.mat[i][j]=0;
            for(k=0;k<36;k++)
            {
                ret.mat[i][j]+=(a.mat[i][k]*b.mat[k][j]);
            }
        }
    }
    return ret;
}

matrix mat_pwr(int p)
{
    if(p==1) return base;
    matrix now;
    now = mat_pwr(p/2);
    now = mat_mul(now, now);
    if(p&1) now = mat_mul(now, base);
    return now;
}

int main()
{
    int t, i, j, k, l, cas=1, c1=0, c2;
    for(i=0;i<10;i++)
    {
        for(j=i+2;j<10;j++)
        {
            c2=0;
            for(k=0;k<10;k++)
            {
                for(l=k+2;l<10;l++)
                {
                    if(abs(k-i)>1 && abs(k-j)>1 && abs(l-i)>1 && abs(l-j)>1)
                        base.mat[c1][c2]=1;
                    c2++;
                }
            }
            c1++;
        }
    }
    scanf("%d", &t);
    while(t--)
    {
        int n;
        unsigned long long tot=0;
        scanf("%d", &n);
        if(n==1)
            tot=36;
        else
        {
            matrix sol = mat_pwr(n-1);
            for(int i=0;i<36;i++) for(int j=0;j<36;j++) tot += sol.mat[i][j];
            tot%=MOD;
        }
        csprnt;
        cout<<tot<<endl;
    }
    return 0;
}
