/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll t,p,res,low,high,mid,R,B,C,M[1010],S[1010],P[1010];

vector<ll>v;

bool func(ll max_time) {

    ll i;
    v.clear();
    for(i=0LL;i<C;i++) {
        ll tp = (max_time-P[i])/S[i];
        if(tp>0LL)
        v.push_back(min(M[i],tp));
    }

    sort(v.begin(),v.end());

    ll cum=0LL,j;
    for(i=v.size()-1LL,j=R-1LL;i>=0LL&&j>=0LL;j--,i--) {
        cum+=v[i];
    }

    return cum>=B;
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    ll i;

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++) {
        scanf(" %lld %lld %lld",&R,&B,&C);
        for(i=0LL;i<C;i++) {
            scanf(" %lld %lld %lld",&M[i],&S[i],&P[i]);
        }

        low=0LL;
        high=3000000000000000010LL;
        res=high;
        while(low<=high) {
            mid=(low+high)/2LL;
            if(func(mid)) {
               high=mid-1LL;
               res=mid;
            } else {
                low=mid+1LL;
            }
        }
        printf("Case #%lld: %lld\n",p,res);
    }

    return 0;
}
