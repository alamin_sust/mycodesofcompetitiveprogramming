/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll n,m,res,k,i,j,x,y,from,to;
vector<ll>row[3010],col[3010];

int main()
{
    scanf(" %I64d %I64d",&n,&m);
    for(i=1;i<=m;i++)
    {
        scanf(" %I64d %I64d",&from,&to);
        row[from].push_back(to);
        col[to].push_back(from);
    }
    for(i=1;i<=n;i++)
    {
        sort(row[i].begin(),row[i].end());
        sort(col[i].begin(),col[i].end());
    }
    res=0LL;
    for(i=1LL;i<=n;i++)
    {
        for(j=1LL;j<=n;j++)
        {
            if(i!=j)
            {
                k=0LL;
                for(x=0LL,y=0LL;x<row[i].size()&&y<col[j].size();)
                {
                    if(row[i][x]==col[j][y])
                    k++,x++,y++;
                    else if(row[i][x]<col[j][y])
                    x++;
                    else
                    y++;
                }
                res+=(k-1LL)*k/2LL;
            }
        }
    }
    printf("%I64d\n",res);
    return 0;
}


