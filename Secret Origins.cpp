#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int n,k,j,flag,in,i;

main()
{
    cin>>n;
    for(i=1;i<=n;i++)
    {
        cin>>in;
        for(j=1;;j<<=1)
        {
            if((j&in)!=0&&((j<<1)&in)==0)
            {
                in^=j;
                in|=(j<<1);
                break;
            }
        }
        for(k=1;j>0;j>>=1)
        {
            if((in&j)!=0)
                k<<=1,in^=j;
        }
        k--;
        in|=k;
        cout<<"Case "<<i<<": "<<in<<endl;
    }
    return 0;
}

