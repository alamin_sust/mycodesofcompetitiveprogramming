/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll res,mx,p=1,P,w,W,i,n,cmax,occ[10010],baki,t;
char arr[110][1010];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    while(1)
    {
        scanf(" %lld %lld",&W,&n);
        if(W==0&&n==0)
            break;
        res=0;
        cmax=0;
        for(i=1;i<=n;i++)
        {
            scanf(" %s %lld",&arr[i],&occ[i]);
            cmax=max(cmax,occ[i]);
        }
        baki=W;
        mx=0;
        for(i=1;i<=n;i++)
        {
            t=strlen(arr[i]);
            P=(ll)(8.0+ceil(40.0*((double)occ[i]-4.0)/((double)cmax-4.0)));
            w=ceil((double)t*(double)P*9.0/16.0);
           // printf("%lld %lld...\n",P,w);
            if(baki<w)
            {
                //printf("%lld--\n",i);
                i--;
                res+=mx;
                mx=0;
                baki=W;
            }
            else
            {
                baki-=(w+10);
                mx=max(mx,P);
            }
        }
        res+=mx;
        printf("CLOUD %lld: %lld\n",p++,res);

    }
    return 0;
}





