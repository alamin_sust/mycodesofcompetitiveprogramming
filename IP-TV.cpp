#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

map<string,int>mpp;

struct node
{
    int x,y,val;
};

node det;

bool operator<(node a,node b)
{
    return a.val>b.val;
}

priority_queue<node>pq;

main()
{
    int t,p,i,n,m,par[2010],k,c;
    char a[1010],b[1010];
    cin>>t;
    for(p=1;p<=t;p++)
    {
        mpp.clear();
        cin>>n>>m;
        for(i=1;i<=n;i++)
            par[i]=i;
            k=0;
        for(i=1;i<=m;i++)
            {
                scanf(" %s %s %d",a,b,&c);
                if(mpp[a]==0)
                {
                 mpp[a]=++k;
                }
                if(mpp[b]==0)
                {
                 mpp[b]=++k;
                }
                det.x=mpp[a];
                det.y=mpp[b];
                det.val=c;
                pq.push(det);
            }
        int res=0;
        while(!pq.empty())
        {
            det.x=pq.top().x;
            det.y=pq.top().y;
            det.val=pq.top().val;
            pq.pop();
            while(par[det.x]!=det.x)
                det.x=par[det.x];
            while(par[det.y]!=det.y)
                det.y=par[det.y];
            if(det.x!=det.y)
                par[det.x]=det.y,res+=det.val;
        }
         printf("%d\n",res);
        if(p!=t)
            printf("\n");
    }
    return 0;
}

