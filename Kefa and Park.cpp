/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int stat[100010],arr[100010],mx,res;
vector<int>adj[100010];
queue<pair<int,int> >q;

void bfs(void)
{
    int i;
    memset(stat,0,sizeof(stat));

    if(arr[1]<=mx)
    q.push(make_pair(1,arr[1]));
    stat[1]=1;

    while(!q.empty())
    {
        pair<int,int>u=q.front();
        q.pop();
        if(adj[u.first].size()==1&&u.first!=1)
            res++;
        int cats=u.second;
        for(i=0;i<adj[u.first].size();i++)
        {
            pair<int,int>v;
            v.first=adj[u.first][i];
            v.second=(arr[v.first]==0?0:(cats+arr[v.first]));
            if(v.second<=mx&&stat[v.first]==0)
            {
                stat[v.first]=1;
                q.push(v);
            }

        }
    }

    return;
}

int main()
{
    int i,from,to,n;
    scanf(" %d %d",&n,&mx);
    for(i=1;i<=n;i++)
    {
        scanf(" %d",&arr[i]);
    }

    for(i=1;i<n;i++)
    {
        scanf(" %d %d",&from,&to);
        adj[from].push_back(to);
        adj[to].push_back(from);
    }

    bfs();

    printf("%d\n",res);
    return 0;
}
















