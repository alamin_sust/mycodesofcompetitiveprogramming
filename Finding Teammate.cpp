/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll taken,t,p,i,n,arr[200010],res,now,val,mpp[200010],rem, M=1000000007LL,prv;
queue<ll>q;



// To compute x^y under modulo m
ll power(ll x, ll y, ll m)
{
    if (y == 0LL)
        return 1LL;
    ll p = power(x, y/2LL, m) % m;
    p = (p * p) % m;

    return (y%2LL == 0LL)? p : (x * p) % m;
}

// Function to return gcd of a and b
ll gcd(ll a, ll b)
{
    if (a == 0LL)
        return b;
    return gcd(b%a, a);
}

// Function to find modular inverse of a under modulo m
// Assumption: m is prime
ll modInverse(ll a, ll m)
{
    ll g = gcd(a, m);
    if (g != 1LL)
        cout << "Inverse doesn't exist";
    else
    {
        // If a and m are relatively prime, then modulo inverse
        // is a^(m-2) mode m

        return power(a, m-2LL, m);
    }
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    mpp[1]=mpp[0]=1LL;
    mpp[2]=1LL;
    mpp[3]=mpp[4]=3LL;


    for(i=4LL;i<=200000LL;i++) {
        mpp[i]=((i-1LL)*mpp[i-2])%M;
    }

    scanf(" %lld",&t);

    for(p=1LL;p<=t;p++) {
        scanf(" %lld",&n);
        for(i=0LL;i<n;i++) {
            scanf(" %lld",&arr[i]);
        }
        sort(arr,arr+n);
        res=1LL;
        while(!q.empty())q.pop();
        for(i=0LL;i<n;) {
            val=0LL;
            now=arr[i];
            while(i<n&&arr[i]==now) {
                val++;
                i++;
            }
            q.push(val);
        }

        rem=0LL;
        prv=0LL;
        taken=0LL;
        while(!q.empty()) {
            now=q.front();
            q.pop();

            if(rem==0LL && (now&1LL)==0LL) {
                if(q.empty()||(q.front()&1LL)) {
                    res = (res*mpp[now+prv])%M;
                    prv=0LL;
                } else {
                    prv+=now;
                }
            } else {

            }

            if(rem==0LL && (now&1LL)==0LL) {
                res = (res*mpp[now])%M;
                taken=0LL;
                rem=0LL;
            } else {
                res = (res*mpp[now+prv])%M;
                if(taken) {
                    res = (res* modInverse(mpp[now],M))%M;
                }
                taken=1LL;
                rem=1LL;
            }

            prv = now;
        }

        printf("%lld\n",res);
    }

    return 0;
}
