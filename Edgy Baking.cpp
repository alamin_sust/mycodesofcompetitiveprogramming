/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

double peri,psum,min_sum,max_sum,close,h[110],w[100],mn,mx;
int t,p,i,n;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++) {
        scanf(" %d %lf",&n,&peri);

        for(i=0;i<n;i++) {
            scanf(" %lf %lf",&h[i],&w[i]);
        }

        mn=min(h[0],w[0]);
        mx=sqrt(h[0]*h[0]+w[0]*w[0]);

        psum = (double)n*2.0*(h[0]+w[0]);

        min_sum = max_sum = 0.0;
        if(psum<peri) {
            close = peri-psum;
            for(i=0;i<n;i++) {
                min_sum+=mn*2LL;
                max_sum+=mx*2LL;

                if(close>=min_sum&& close<=max_sum) {
                    close=0.0;
                    psum=peri;
                    break;
                }

                if(close>=max_sum) {
                    psum = peri-(close-max_sum);
                }
            }
        }

        printf("Case #%d: %.10lf\n",p,psum);
    }

    return 0;
}
