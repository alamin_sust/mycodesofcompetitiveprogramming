/**
    Bismillah hir-Rahman nir-Raheem
    In the name of Allah, the Most Gracious, the Ever Merciful.
*/
#include<stdio.h>
#include<iostream>
#include<sstream>
#include<math.h>
#include<limits.h>
#include<limits>
#include<string.h>
#include<string>
#include<algorithm>
#include<stack>
#include<set>
#include<ctime>
#include<queue>
#include<vector>
#include<map>
#include<stdlib.h>
#include<deque>
#include<utility>
#include <assert.h>
#include <ctime>

using namespace std;

#define sf scanf
#define pf printf
#define ll long long
#define psb push_back
#define ppb pop_back
#define ms(a,b) memset(a,b,sizeof(a))
#define TestCase pf("Case %d: ",t++)

const double pi = acos(-1.0);

struct Point
{
    ll x,y;
    bool operator <(const Point &p) const
    {
        return x<p.x||(x==p.x&&y<p.y);
    }
};

vector<Point>vec;
vector<Point>convx,convx2;

ll cross(Point o, Point a,Point b)
{
    return (a.x-o.x)*(b.y-o.y)-(a.y-o.y)*(b.x-o.x);
}

ll onSegment(Point p, Point q, Point r)
{
    if (q.x <= max(p.x, r.x) && q.x >= min(p.x, r.x) &&
        q.y <= max(p.y, r.y) && q.y >= min(p.y, r.y))
       return 1;

    return 0;
}

ll orientation(Point p, Point q, Point r)
{
    // See 10th slides from following link for derivation of the formula
    // http://www.dcs.gla.ac.uk/~pat/52233/slides/Geometry1x1.pdf
    ll val = (q.y - p.y) * (r.x - q.x) -
              (q.x - p.x) * (r.y - q.y);

    if (val == 0) return 0;  // colinear

    return (val > 0)? 1: 2; // clock or counterclock wise
}

ll doIntersect(Point p1, Point q1, Point p2, Point q2)
{
    // Find the four orientations needed for general and
    // special cases
    ll o1 = orientation(p1, q1, p2);
    ll o2 = orientation(p1, q1, q2);
    ll o3 = orientation(p2, q2, p1);
    ll o4 = orientation(p2, q2, q1);

    // General case
    if (o1 != o2 && o3 != o4)
        return true;

    // Special Cases
    // p1, q1 and p2 are colinear and p2 lies on segment p1q1
    if (o1 == 0 && onSegment(p1, p2, q1)) return true;

    // p1, q1 and p2 are colinear and q2 lies on segment p1q1
    if (o2 == 0 && onSegment(p1, q2, q1)) return true;

    // p2, q2 and p1 are colinear and p1 lies on segment p2q2
    if (o3 == 0 && onSegment(p2, p1, q2)) return true;

     // p2, q2 and q1 are colinear and q1 lies on segment p2q2
    if (o4 == 0 && onSegment(p2, q1, q2)) return true;

    return false; // Doesn't fall in any of the above cases
}

vector<Point> convexHull()
{
    ll n = vec.size(),k=0;
    vector<Point> H(2*n);
    sort(vec.begin(),vec.end());
    ll x=vec[0].x,y=vec[0].y;

    for(int i=0; i<n; i++)
    {
        while(k>=2&&cross(H[k-2],H[k-1],vec[i])<=0)k--;
        H[k++] = vec[i];
    }
    for(int i=n-1,t = k+1; i>=0; i--)
    {
        while(k>=t&&cross(H[k-2],H[k-1],vec[i])<=0)k--;
        H[k++] = vec[i];
    }
    H.resize(k);
    return H;
}

double getDistance(Point a, Point b)
{
    return sqrt((double)((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)));
}

ll chk(void)
{
    for(int i=0;i<convx.size();i++)
    {
        if(convx2[i].x!=convx[i].x||convx2[i].y!=convx[i].y)
            return 1;
    }
    return 0;
}


int main()
{
    int N,M;
    Point p[110],q[110];
    double ax,bx,cx,ay,by,cy;
    while(sf("%d %d",&N,&M)==2&&(N+M))
    {
        vec.clear();
        convx.clear();
        convx2.clear();
        for(int i=0; i<N; i++)
        {
            sf("%lld %lld",&p[i].x,&p[i].y);
            vec.psb(p[i]);
        }
        convx = convexHull();
        convx2 = convx;

        //int l2 = convx2.size();
        //double sum=0.0;
        //for(int j=0;j<l-1;j++)
        //{
        //   sum+=getDistance(convx[j],convx[j+1]);
        //}
        //printf("%lf\n",sum);
        int flag=0;
        vec.push_back(p[N-1]);
        for(int i=0;i<M;i++)
        {
            convx.clear();
            sf("%lld %lld",&q[i].x,&q[i].y);
            vec[N]=q[i];
            if(flag==0){convx = convexHull();
            if(convx.size()==convx2.size())
            {
                if(!chk())
                flag=1;
            }
            }
        }


        vec.clear();
        convx.clear();
        convx2.clear();
        for(int i=0; i<M&&flag==0; i++)
        {
            vec.psb(q[i]);
        }
        convx = convexHull();
        convx2 = convx;

        //int l2 = convx2.size();
        //double sum=0.0;
        //for(int j=0;j<l-1;j++)
        //{
        //   sum+=getDistance(convx[j],convx[j+1]);
        //}
        //printf("%lf\n",sum);

        vec.push_back(q[M-1]);
        for(int i=0;i<N&&flag==0;i++)
        {
            convx.clear();

            vec[M]=p[i];
            convx = convexHull();
            if(convx.size()==convx2.size())
            {
                if(!chk())
                flag=1;
            }
        }

        vec.clear();
        convx.clear();
        for(int i=0; i<N&&flag==0; i++)
        {
            vec.psb(p[i]);
        }
        convx = convexHull();

        if(flag==0)
        for(int i=0;i<convx.size()-1;i++)
        {
            for(int j=0;j<convx2.size()-1;j++)
            {
                if(doIntersect(convx[i], convx[i+1], convx2[j], convx2[j+1]))
                {
                   flag=1;
                }
            }
        }


        if(flag)
            printf("NO\n");
        else
            printf("YES\n");

    }
    return 0;
}

