#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

int dp[1010][1010];
char arr[1010];

int rec(int l,int r)
{
    if(l>r)
        return dp[l][r]=0;
    if(l==r)
        return dp[l][r]=1;
    if(dp[l][r]!=-1)
        return dp[l][r];
    if(arr[l]==arr[r])
        return dp[l][r]=rec(l+1,r-1)+2;
    return dp[l][r]=max(rec(l+1,r),rec(l,r-1));
}

main()
{
    int n,i,l;
    cin>>n;
    getchar();
    for(i=1;i<=n;i++)
    {
        gets(arr);
        memset(dp,-1,sizeof(dp));
        l=strlen(arr);
        printf("%d\n",rec(0,l-1));
    }
    return 0;
}

