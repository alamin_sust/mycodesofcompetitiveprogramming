#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class EllysNewNickname
{
public:
	int getLength(string nickname)
	{
	    int k=0;
	    for(int i=1;i<nickname.size();i++)
	    {
	        if(nickname[i]=='a'||nickname[i]=='e'||nickname[i]=='i'||nickname[i]=='o'||nickname[i]=='u'||nickname[i]=='y')
	        {
	            if(nickname[i-1]=='a'||nickname[i-1]=='e'||nickname[i-1]=='i'||nickname[i-1]=='o'||nickname[i-1]=='u'||nickname[i-1]=='y')
	            k++;
	        }
	    }
	    return nickname.size()-k;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	EllysNewNickname objectEllysNewNickname;

	//test case0
	string param00 = "tourist";
	int ret0 = objectEllysNewNickname.getLength(param00);
	int need0 = 6;
	assert_eq(0,ret0,need0);

	//test case1
	string param10 = "eagaeoppooaaa";
	int ret1 = objectEllysNewNickname.getLength(param10);
	int need1 = 6;
	assert_eq(1,ret1,need1);

	//test case2
	string param20 = "esprit";
	int ret2 = objectEllysNewNickname.getLength(param20);
	int need2 = 6;
	assert_eq(2,ret2,need2);

	//test case3
	string param30 = "ayayayayayaya";
	int ret3 = objectEllysNewNickname.getLength(param30);
	int need3 = 1;
	assert_eq(3,ret3,need3);

	//test case4
	string param40 = "wuuut";
	int ret4 = objectEllysNewNickname.getLength(param40);
	int need4 = 3;
	assert_eq(4,ret4,need4);

	//test case5
	string param50 = "naaaaaaaanaaaanaananaaaaabaaaaaaaatmaaaaan";
	int ret5 = objectEllysNewNickname.getLength(param50);
	int need5 = 16;
	assert_eq(5,ret5,need5);

}
