#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#define ll long long
#include<string.h>
using namespace std;

ll five[33],len,dp[55][55];
string str;

void init(void)
{
    five[0]=1;
    for(ll i=1;i<=30;i++)
    {
        five[i]=five[i-1]*5LL;
    }
    return;
}

ll check(ll f,ll t)
{
    ll k=0;
    for(ll i=f;i<=t;i++)
        k=k*2+str[i]-'0';
    //printf("%lld\n",k);
    for(ll i=0;i<=30;i++)
        {
            if(k<five[i])
                return 0;
            if(k==five[i])
        return 1;}
    return 0;
}

ll rec(ll f,ll t)
{
    if(t==len)
        return 0;
    ll &ret=dp[f][t];
    if(ret!=-1)
        return ret;
    ret=999999;
    for(ll i=t+1;i<=len;i++)
    if(check(t,i-1))
    if(i<len&&str[i]=='0')
    continue;
    else
    ret=min(ret,1+rec(t+1,i));
    return ret;
}

class CuttingBitString
{
public:
	int getmin(string S)
	{
	    if(S[0]=='0')
            return -1;
	    int res=0;
	    init();
	    str=S;
	    len=S.length();
	    memset(dp,-1,sizeof(dp));
	    res=(int)rec(0,0);
	    return res>=999999?-1:res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	CuttingBitString objectCuttingBitString;

	//test case0
	string param00 = "101101101";
	int ret0 = objectCuttingBitString.getmin(param00);
	int need0 = 3;
	assert_eq(0,ret0,need0);

	//test case1
	string param10 = "1111101";
	int ret1 = objectCuttingBitString.getmin(param10);
	int need1 = 1;
	assert_eq(1,ret1,need1);

	//test case2
	string param20 = "00000";
	int ret2 = objectCuttingBitString.getmin(param20);
	int need2 = -1;
	assert_eq(2,ret2,need2);

	//test case3
	string param30 = "110011011";
	int ret3 = objectCuttingBitString.getmin(param30);
	int need3 = 3;
	assert_eq(3,ret3,need3);

	//test case4
	string param40 = "1000101011";
	int ret4 = objectCuttingBitString.getmin(param40);
	int need4 = -1;
	assert_eq(4,ret4,need4);

	//test case5
	string param50 = "111011100110101100101110111";
	int ret5 = objectCuttingBitString.getmin(param50);
	int need5 = 5;
	assert_eq(5,ret5,need5);
}
