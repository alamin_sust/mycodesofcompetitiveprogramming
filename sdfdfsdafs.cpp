#include <bits/stdc++.h>


using namespace std ;

int rigt , let , up , down , row ,col ;
char graph[120][120] ;
bool check[120][120] ;

void rdfs(int x , int y );
void ldfs(int x , int y );
void downdfs(int x , int y );
void updfs(int x , int y );
void ini();

int main()
{
    int tcase, p , w ,x,y ,maxim , cont ;

    scanf("%d",&tcase);

    while(tcase--)
    {
        scanf("%d %d",&row,&col);

        ini();

        scanf("%d",&p);
        cont = 0 ;

        for(int i=0 ; i<p ; i++)
        {
            scanf("%d %d",&y,&x);
            graph[x][y]='*';
        }

        scanf("%d",&w);

        for(int i=0 ; i<w ; i++)
        {
            scanf("%d %d",&y,&x);
            graph[x][y]='#';
        }

//        for(int i=1 ; i<=row ; i++)
//        {
//            for(int j=1 ; j<=col ; j++)
//            {
//                printf("%c",graph[i][j]);
//            }
//            printf("\n");
//        }printf("\n");

        for(int i=1 ; i<=row ; i++)
        {
            for(int j=1 ; j<=col ; j++)
            {
                if(graph[i][j] == '*' && !check[i][j])
                {
                    cont++;
                    rigt = let = up = down = 1 ;
                    rdfs(i,j);
                    ldfs(i,j);
                    updfs(i,j);
                    downdfs(i,j);

                    maxim = max(rigt,max(let,max(up,down)));

                    if(maxim == rigt)
                    {
                        for(int k=j ; k<=col ; k++)
                        {
                            if(graph[i][k] == '#')
                                break;

                            else if(graph[i][k] == '*')
                                check[i][k] = true;
                        }
                    }

                    else if(maxim == let)
                    {
                        for(int k=j ; k>0 ; k--)
                        {
                            if(graph[i][k] == '#')
                                break;

                            else if(graph[i][k] == '*')
                                check[i][k] = true;
                        }
                    }

                    else if(maxim == up)
                    {
                        for(int k=i ; k>0 ; k--)
                        {

                            if(graph[k][j] == '#')
                                break;
                            else if(graph[k][j] == '*')
                                check[k][j] = true;
                        }
                    }

                    else if(maxim == down)
                    {
                        for(int k=i ; k<=row ; k++)
                        {

                            if(graph[k][j] == '#')
                                break;

                            else if(graph[k][j] == '*')
                                check[k][j] = true;
                        }
                    }
                }
            }
        }
        printf("%d\n",cont);
    }
    return 0;
}

void rdfs(int x , int y )
{
    if(y>col || graph [x][y] == '#')
        return;

    if(graph[x][y] == '*' && !check[x][y])
        rigt++;

    rdfs(x,y+1);
}

void ldfs(int x , int y )
{
    if(y<1 || graph [x][y] == '#')
        return;

    if(graph[x][y] == '*' && !check[x][y])
        let++;

    ldfs(x,y-1);
}

void downdfs(int x , int y )
{
    if(x>row || graph [x][y] == '#')
        return;

    if(graph[x][y] == '*' && !check[x][y])
        down++;

    downdfs(x+1,y);
}

void updfs(int x , int y )
{
    if(x<1 || graph [x][y] == '#')
        return;

    if(graph[x][y] == '*' && !check[x][y])
        up++;

    updfs(x-1,y);
}

void ini()
{
    for(int i=0 ; i<110; i++)
    {
        for(int j=0 ; j<110; j++)
        {
            check[i][j] = false;
            graph[i][j]= '.';
        }
    }
}
