#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

long long dp[(1<<21)],p,arr[21][21],n,t,tot,i,j,mx,mask;
main()
{
    scanf(" %lld",&t);
    for(p=1; p<=t; p++)
    {
        scanf(" %lld",&n);
        for(i=0; i<n; i++)
            for(j=0; j<n; j++)
                scanf(" %lld",&arr[i][j]);
        memset(dp,0,sizeof(dp));
        dp[0]=1;
        mx=(1<<n)-1;
        for(mask=0; mask<mx; mask++)
        {
            tot=__builtin_popcount(mask);
            for(i=0; i<n; i++)
            {
                if(arr[tot][i]==1 && (mask & (1<<i))==0)
                    dp[mask|(1<<i)]+=dp[mask];
            }
        }
        printf("%lld\n",dp[mx]);
    }
    return 0;
}
