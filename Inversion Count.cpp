/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
int val,ind;
};

node arr[200010];

int comp(node a,node b)
{
    return a.val>b.val;
}

int t,n,i,res,p;

main()
{
    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %d",&n);
        for(i=0;i<n;i++)
            scanf(" %d",&arr[i].val),arr[i].ind=i;
        sort(arr,arr+n,comp);
        res=0;
        for(i=0;i<n;i++)
            {
                if(arr[i].ind<i)res+=abs(arr[i].ind-i);
            }
        printf("%d\n",res);
    }
    return 0;
}

