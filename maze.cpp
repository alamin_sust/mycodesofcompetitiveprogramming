#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct data
{
    int val,x,y;
};

bool operator<(data a,data b)
{
    return a.val>b.val;
}

priority_queue<data>pq;
char arr[510][510];
int n,m,k,i,j,adj[510][510];

main()
{
    cin>>n>>m>>k;
    getchar();
    for(i=1; i<=n; i++)
    {
        for(j=1; j<=m; j++)
        {
            scanf("%c",&arr[i][j]);
        }
        getchar();
    }
    data det;
    for(i=1; i<=n; i++)
    {
        for(j=1; j<=m; j++)
        {
            if(arr[i][j]=='.')
            {
                det.x=i;
                det.y=j;
                det.val=0;
                if(arr[i][j+1]=='.')
                    det.val++;
                if(arr[i][j-1]=='.')
                    det.val++;
                if(arr[i-1][j]=='.')
                    det.val++;
                if(arr[i+1][j]=='.')
                    det.val++;
                pq.push(det);
                adj[i][j]=det.val;
            }
        }
    }
    while(k)
    {
        det=pq.top();
        pq.pop();
        if(arr[det.x][det.y]=='X')
            continue;
        data det2;
        arr[det.x][det.y]='X';
        if(arr[det.x+1][det.y]=='.')
        {
            det2.x=det.x+1;
            det2.y=det.y;
            det2.val=--adj[det2.x][det2.y];
            pq.push(det2);
        }
        if(arr[det.x-1][det.y]=='.')
        {
            det2.x=det.x-1;
            det2.y=det.y;
            det2.val=--adj[det2.x][det2.y];
            pq.push(det2);
        }
        if(arr[det.x][det.y+1]=='.')
        {
            det2.x=det.x;
            det2.y=det.y+1;
            det2.val=--adj[det2.x][det2.y];
            pq.push(det2);
        }
        if(arr[det.x][det.y-1]=='.')
        {
            det2.x=det.x;
            det2.y=det.y-1;
            det2.val=--adj[det2.x][det2.y];
            pq.push(det2);
        }
        k--;
    }
    for(i=1; i<=n; i++)
    {
        for(j=1; j<=m; j++)
            printf("%c",arr[i][j]);
        printf("\n");
    }
    return 0;
}
