#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int t,p,n,k,w,dp[105][105][105];

struct node
{
    int x,y;
};

node arr[110];

int comp(node a,node b)
{
    return a.y<b.y;
}

int rec(int ind,int rem,int dust)
{
    if(ind==n||rem==0)
        return dust;
    int &ret=dp[ind][rem][dust];
    if(ret!=-1)
        return ret;
    int cap=arr[ind+1].y,s=1,i=ind+2;
    for(;i<=n;s++,i++)
    {
        if((arr[i].y-cap)>w)
            break;
    }
    return ret=max(rec(ind+1,rem,dust),rec(i-1,rem-1,dust+s));
}

main()
{
    cin>>t;

    for(p=1;p<=t;p++)
    {
         memset(dp,-1,sizeof(dp));
        cin>>n>>w>>k;
        for(int i=1;i<=n;i++)
        {
            cin>>arr[i].x>>arr[i].y;
        }
        sort(arr+1,arr+n+1,comp);
        printf("Case %d: %d\n",p,rec(0,k,0));
    }
    return 0;
}

