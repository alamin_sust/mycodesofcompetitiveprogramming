/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int dp[1<<18][19],l;
char arr[110];

int rec(int mask,int active)
{
    if(mask==((1<<l)-1))
        return 0;
    int &ret=dp[mask][active];
    if(ret!=-1)
        return ret;
    ret=99999999;
    int k=0;
    for(int i=active;i>=0;i--)
    {
        if(active>=i&&((1<<i)&mask)!=0)
        k++;
        if(((1<<i)&mask)==0)
        {
          if(mask!=0)
          ret=min(ret,k+min(abs(arr[active]-arr[i])+1,1+26-abs(arr[active]-arr[i]))+rec(((1<<i)|mask),i));
          else
          ret=min(ret,min(1+abs(arr[i]-'A'),1+26-abs(arr[i]-'A'))+rec(((1<<i)|mask),i));
        }
    }
    int k2=0;
    for(int i=active+1;i<l;i++)
    {
        if(active<=i&&((1<<i)&mask)!=0)
        k2++;
        if(((1<<i)&mask)==0)
        {
          if(mask!=0)
          ret=min(ret,k2+min(abs(arr[active]-arr[i])+1,1+26-abs(arr[active]-arr[i]))+rec(((1<<i)|mask),i));
          else
          ret=min(ret,min(1+abs(arr[i]-'A'),1+26-abs(arr[i]-'A'))+rec(((1<<i)|mask),i));
        }
    }
    return ret;
}

int main()
{
    while(1)
    {
        scanf(" %s",&arr);
        l=strlen(arr);
        if(l==1&&arr[0]=='0')
            break;
        memset(dp,-1,sizeof(dp));
        printf("%d\n",rec(0,0));
    }
    return 0;
}


