#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class ABBA
{
public:
    string canObtain(string initial, string target)
    {
        int cnt=0;
        int ilen=initial.size();
        int tlen=target.size();
        for(int i=0; (i+ilen-1)<tlen; i++)
        {
            int flag=0,flag1=0;
            for(int j=0; j<ilen; j++)
            {
                if(target[i+j]!=initial[j])
                {
                    flag=1;
                    break;
                }
            }
            for(int j=ilen-1,k=0; j>=0; k++,j--)
            {
                if(target[i+j]!=initial[k])
                {
                    flag1=1;
                    break;
                }
            }
            if(flag==0)
            {
                cnt=0;
                int pl=i-1;
                int pr=i+ilen;
                //printf("%d %d\n",pl,pr);
                for(int k=0; k<(tlen-ilen); k++)
                {
                    if((cnt%2)==0)
                    {
                        if(pr<tlen&&target[pr]=='A')
                        {
                            pr++;
                            continue;
                        }
                        else if(pl>=0&&target[pl]=='B')
                        {
                            cnt++;
                            pl--;
                            continue;
                        }
                    }
                    else
                    {
                        if(pr<tlen&&target[pr]=='B')
                        {
                            cnt++;
                            pr++;
                            continue;
                        }
                        else if(pl>=0&&target[pl]=='A')
                        {
                            pl--;
                            continue;
                        }

                    }
                    flag=1;
                    break;
                }
            }
            if(flag==0)
            {
                if(cnt%2)
                {
                    for(int j=0,k=ilen-1;j<ilen;k--,j++)
                    {
                        if(initial[j]!=initial[k])
                        {
                            flag=1;
                            break;
                        }
                    }
                }
            }
            if(flag==0)
            {
                return "Possible";
            }


            if(flag1==0)
            {
                cnt=1;
                int pl=i-1;
                int pr=i+ilen;
                //printf("%d %d\n",pl,pr);
                for(int k=0; k<(tlen-ilen); k++)
                {
                    if((cnt%2)==0)
                    {
                        if(pr<tlen&&target[pr]=='A')
                        {
                            pr++;
                            continue;
                        }
                        else if(pl>=0&&target[pl]=='B')
                        {
                            cnt++;
                            pl--;
                            continue;
                        }
                    }
                    else
                    {
                        if(pr<tlen&&target[pr]=='B')
                        {
                            cnt++;
                            pr++;
                            continue;
                        }
                        else if(pl>=0&&target[pl]=='A')
                        {
                            pl--;
                            continue;
                        }

                    }
                    flag1=1;
                    break;
                }
            }
            if(flag1==0)
            {
                if((cnt%2)==0)
                {
                    for(int j=0,k=ilen-1;j<ilen;k--,j++)
                    {
                        if(initial[j]!=initial[k])
                        {
                            flag1=1;
                            break;
                        }
                    }
                }
            }
            if(flag1==0)
            {
                return "Possible";
            }

        }
        return "Impossible";
    }
};


template<typename T> void print( T a )
{
    cerr << a;
}

void print( long long a )
{
    cerr << a << "L";
}

void print( string a )
{
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a )
{
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ )
    {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need )
{
    if ( have == need )
    {
        cerr << "Case " << n << " passed." << endl;
    }
    else
    {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need )
{
    if( have.size() != need.size() )
    {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ )
    {
        if( have[i] != need[i] )
        {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need )
{
    if ( have == need )
    {
        cerr << "Case " << n << " passed." << endl;
    }
    else
    {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

    ABBA objectABBA;

    //test case0
    string param00 = "B";
    string param01 = "ABBA";
    string ret0 = objectABBA.canObtain(param00,param01);
    string need0 = "Possible";
    assert_eq(0,ret0,need0);

    //test case1
    string param10 = "AB";
    string param11 = "ABB";
    string ret1 = objectABBA.canObtain(param10,param11);
    string need1 = "Impossible";
    assert_eq(1,ret1,need1);

    //test case2
    string param20 = "BBAB";
    string param21 = "ABABABABB";
    string ret2 = objectABBA.canObtain(param20,param21);
    string need2 = "Impossible";
    assert_eq(2,ret2,need2);

    //test case3
    string param30 = "BBBBABABBBBBBA";
    string param31 = "BBBBABABBABBBBBBABABBBBBBBBABAABBBAA";
    string ret3 = objectABBA.canObtain(param30,param31);
    string need3 = "Possible";
    assert_eq(3,ret3,need3);

    //test case4
    string param40 = "A";
    string param41 = "BB";
    string ret4 = objectABBA.canObtain(param40,param41);
    string need4 = "Impossible";
    assert_eq(4,ret4,need4);

}
