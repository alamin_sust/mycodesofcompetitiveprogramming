#include <stdio.h>
#include <math.h>

#define min(a,b) a<=b?a:b

double pi,T;

double dist(double d,double r,double t)
{
    double theta = pi*2.0*T/t;
    double temp;

    theta = min( (2.0*pi-theta), theta );

    temp = fabs(d*d+r*r+2.0*d*r*cos(theta));

    d = sqrt(temp);

    return d;
}

main()
{
    int i,n;
    double t,r,d;
     pi=2*acos(0);
    while(scanf("%d %lf",&n,&T)==2)
    {

        scanf("%lf %lf",&r,&t);

        printf("%.4lf",r);

        d = r;

        for(i=0; i<n-1; i++)
        {
            scanf("%lf %lf",&r,&t);
            d = dist(d,r,t);

            printf(" %.4lf",d);
        }
        printf("\n");
    }
     return 0;
}
