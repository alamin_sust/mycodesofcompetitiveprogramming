/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int n,flag;

main()
{
    cin>>n;
    if(n<=3)
        printf("NO\n");
    else
    {
        printf("YES\n");
        while((n-4)>=4)
        {
            if(flag)
            {
                printf("%d + 0 = %d\n",n,n);
            }
            printf("%d - %d = 1\n",n,n-1);
            printf("%d - %d = -1\n",n-3,n-2);
            printf("-1 + 1 = 0\n");
            n-=4;
            flag=1;
        }
        if(flag)
            printf("%d + 0 = %d\n",n,n);
        if(n==4)
        {
            printf("4 * 3 = 12\n");
            printf("12 * 2 = 24\n");
            printf("24 * 1 = 24\n");
        }
        else if(n==5)
        {
            printf("5 * 4 = 20\n");
            printf("3 - 1 = 2\n");
            printf("2 + 2 = 4\n");
            printf("20 + 4 = 24\n");
        }
        else if(n==6)
        {
            printf("6 - 5 = 1\n");
            printf("4 * 3 = 12\n");
            printf("12 * 2 = 24\n");
            printf("1 - 1 = 0\n");
            printf("24 + 0 = 24\n");
        }
        else if(n==7)
        {
            printf("7 - 5 = 2\n");
            printf("6 - 4 = 2\n");
            printf("3 * 2 = 6\n");
            printf("2 * 2 = 4\n");
            printf("6 * 4 = 24\n");
            printf("24 * 1 = 24\n");
        }
    }
    return 0;
}

