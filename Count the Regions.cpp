/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
int rr[]={1,-1,0,0};
int cc[]={0,0,1,-1};

int arr[220][220];
vector<int>arrx,arry;
map<int,int>mppx,mppy;
pair<int,int>pii,pii_to;
queue<pair<int,int> >q;

void make_border(int x1,int y1,int x2,int y2)
{
    for(int i=x1; i<=x2; i++)
    {
        arr[i][y1]=arr[i][y2]=1;
    }
    for(int i=y2; i<=y1; i++)
    {
        arr[x1][i]=arr[x2][i]=1;
    }
    return;
}

void bfs(int x,int y)
{
    arr[x][y]=1;
    pii.first=x;
    pii.second=y;
    q.push(pii);
    while(!q.empty())
    {
        pii=q.front();
        for(int i=0;i<4;i++)
        {
            pii_to.first=pii.first+rr[i];
            pii_to.second=pii.second+cc[i];
            if(pii_to.first>0&&pii_to.first<202&&pii_to.second>0&&pii_to.second<202)
            if(arr[pii_to.first][pii_to.second]==0)
            {
                arr[pii_to.first][pii_to.second]=1;
                q.push(pii_to);
            }
        }
        q.pop();
    }
    return;
}

int main()
{
    int n,i,x1[55],j,k,y1[55],x2[55],y2[55];
    while(cin>>n&&n)
    {
        mppx.clear();
        mppy.clear();
        arrx.clear();
        arry.clear();
        memset(arr,0,sizeof(arr));
        for(i=1; i<=n; i++)
        {
            scanf(" %d %d %d %d",&x1[i],&y1[i],&x2[i],&y2[i]);
            if(mppx[x1[i]]==0)
            {
                mppx[x1[i]]=1;
                arrx.push_back(x1[i]);
            }
            if(mppx[x2[i]]==0)
            {
                mppx[x2[i]]=1;
                arrx.push_back(x2[i]);
            }
            if(mppy[y1[i]]==0)
            {
                mppy[y1[i]]=1;
                arry.push_back(y1[i]);
            }
            if(mppy[y2[i]]==0)
            {
                mppy[y2[i]]=1;
                arry.push_back(y2[i]);
            }
        }
        mppx.clear();
        mppy.clear();
        sort(arrx.begin(),arrx.end());
        sort(arry.begin(),arry.end());
        for(i=0,k=2; i<arrx.size(); k+=2,i++)
        {
            mppx[arrx[i]]=k;
        }
        for(i=0,k=2; i<arry.size(); k+=2,i++)
        {
            mppy[arry[i]]=k;
        }
        for(i=1; i<=n; i++)
        {
           // printf("%d %d %d %d\n",mppx[x1[i]],mppy[y1[i]],mppx[x2[i]],mppy[y2[i]]);
            make_border(mppx[x1[i]],mppy[y1[i]],mppx[x2[i]],mppy[y2[i]]);
        }
       /* for(i=0; i<20; i++)
        {
            for(j=0; j<20; j++)
            {
                printf("%d ",arr[i][j]);
            }
            printf("\n");
        }*/
        k=0;
        for(i=1;i<202;i++)
        {
            for(j=1;j<202;j++)
            {
                if(arr[i][j]==0)
                {
                    bfs(i,j);
                    k++;
                }
            }
        }
        printf("%d\n",k);
    }
    return 0;
}


