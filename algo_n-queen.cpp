#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int res=0,diagdiff[1010],diagadd[1010],n,col[1010];

void solve(int i)
{
    if(i==n)
    {
        res++;
        return;
    }
    for(int j=0;j<n;j++)
    {
        if(col[j]==0&&diagdiff[i-j+n]==0&&diagadd[i+j]==0)
        {
            col[j]=1;
            diagdiff[i-j+n]=1;
            diagadd[i+j]=1;
            solve(i+1);
            col[j]=0;
            diagdiff[i-j+n]=0;
            diagadd[i+j]=0;
        }
    }
    return;
}

main()
{
    cin>>n;
    solve(0);
    cout<<res;
    return 0;
}

