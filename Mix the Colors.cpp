/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<set>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

int i,p,t,n,in;
set<int> st;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++) {
        scanf(" %d",&n);
        st.clear();
        for(i=1;i<=n;i++) {
            scanf(" %d",&in);
            st.insert(in);
        }
        printf("%d\n",n-st.size());
    }

    return 0;
}
