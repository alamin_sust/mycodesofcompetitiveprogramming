/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

deque<int>q1,q2;
int n1,n2,i,k1,k2,arr1[110],arr2[110],res,n;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d",&n);

    scanf(" %d",&k1);
    for(i=1;i<=k1;i++)
    {
        scanf(" %d",&arr1[i]);
        q1.push_back(arr1[i]);
    }

    scanf(" %d",&k2);
    for(i=1;i<=k2;i++)
    {
        scanf(" %d",&arr2[i]);
        q2.push_back(arr2[i]);
    }
    res=0;
    for(i=1;i<=1000100;i++)
    {
        if(q1.size()==n)
        {
            printf("%d 1\n",res);
            return 0;
        }
        else if(q2.size()==n)
        {
            printf("%d 2\n",res);
            return 0;
        }
        n1=q1.front();
        n2=q2.front();
        q1.pop_front();
        q2.pop_front();



        if(n1>n2)
        {
            q1.push_back(n2);
            q1.push_back(n1);
        }
        else
        {
            q2.push_back(n1);
            q2.push_back(n2);
        }
        res++;
    }

    printf("-1\n");

    return 0;
}



