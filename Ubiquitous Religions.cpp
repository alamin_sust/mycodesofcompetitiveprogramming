#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int p=1,i,n,m,par[50010],a,b,j,flag[50010],res;

main()
{
    while(cin>>n>>m)
    {
        if(n==0&&m==0)
            break;
        for(i=1; i<=n; i++)
            par[i]=i,flag[i]=0;
        for(i=1; i<=m; i++)
        {
            scanf(" %d %d",&a,&b);
            while(par[a]!=a)
                a=par[a];
            while(par[b]!=b)
                b=par[b];
            if(a!=b)
                par[a]=b;
        }
        res=0;
        for(i=1; i<=n; i++)
        {
            //printf("%d..\n",par[i]);
            if(flag[i]==0)
            {
                res++;
                flag[i]=1;
                j=i;
                while(par[j]!=j)
                {
                    if(flag[par[j]]==1)
                    {
                        res--;
                        break;
                    }
                    flag[par[j]]=1;
                    j=par[j];
                }
            }
        }
        printf("Case %d: %d\n",p++,res);
    }
    return 0;
}

