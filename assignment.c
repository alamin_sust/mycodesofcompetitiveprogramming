#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

long long dp[20][(1<<20)+2],n,arr[21][21];

long long rec(long long x,long long mask)
{
    if(x==n)
    return 1;
    long long &ret=dp[x][mask];
    if(ret!=-1)
        return ret;
    ret=0;
    for(long long j=0;j<n;j++)
    {
        if(arr[x+1][j]==1&&((1<<j)&mask)==0)
        {
        ret+=rec(x+1,((1<<j)|mask));
        }
    }
    return ret;
}

main()
{
    long long p,t,i,j,res;
    scanf(" %lld",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %lld",&n);
        if(n>15)
        memset(dp,-1,sizeof(dp));
        else
        {long long a=(1<<n)+2;
        for(i=0;i<n;i++)
        {
            for(j=0;j<a;j++)
                dp[i][j]=-1;
        }
        }
        for(i=1;i<=n;i++)
        {
            for(j=0;j<n;j++)
                scanf(" %lld",&arr[i][j]);
        }
        res=rec(0,0);
        printf("%lld\n",res);
    }
    return 0;
}
