#include<sstream>
#include<queue>
#include<stack>
#include<set>
#include<map>
#include<cstdio>
#include<cstdlib>
#include<cctype>
#include<complex>
#include<cmath>
#include<iostream>
#include<iomanip>
#include<string>
#include<vector>
#include<algorithm>
#include<bitset>
#include<list>
#include<string.h>
#include<assert.h>
#include<time.h>

using namespace std;

#define SZ(x) (int) x.size()
#define all(x) x.begin(),x.end()
#define allr(x) x.rbegin(),x.rend()
#define clrall(a,v) memset(a,v,sizeof a)
#define ll long long
#define ull long long unsigned
#define SF scanf
#define PF printf
#define psb push_back
#define ppb pop_back
#define oo (1ll<<40)
#define mp make_pair
#define fs first
#define sc second
#define __ std::ios_base::sync_with_stdio (false)

ll BigMod(ll B,ll P,ll M)
{
    ll R=1;
    while(P>0)
    {
        if(P%2) R=(R*B)%M;
        P/=2;   B=(B*B)%M;
    }
    return R;
}

template<class T1> void deb(T1 e1){cout<<e1<<"\n";}
template<class T1,class T2> void deb(T1 e1,T2 e2){cout<<e1<<" "<<e2<<"\n";}
template<class T1,class T2,class T3> void deb(T1 e1,T2 e2,T3 e3){cout<<e1<<" "<<e2<<" "<<e3<<"\n";}
template<class T1,class T2,class T3,class T4> void deb(T1 e1,T2 e2,T3 e3,T4 e4){cout<<e1<<" "<<e2<<" "<<e3<<" "<<e4<<"\n";}
template<class T1,class T2,class T3,class T4,class T5> void deb(T1 e1,T2 e2,T3 e3,T4 e4,T5 e5){cout<<e1<<" "<<e2<<" "<<e3<<" "<<e4<<" "<<e5<<"\n";}
template<class T1,class T2,class T3,class T4,class T5,class T6> void deb(T1 e1,T2 e2,T3 e3,T4 e4,T5 e5,T6 e6){cout<<e1<<" "<<e2<<" "<<e3<<" "<<e4<<" "<<e5<<" "<<e6<<"\n";}

const int MAX = 400050;

ll dp[MAX];
int vp[MAX];
ll sum[1010];
int loop;

ll rec(ll L)
{
    if(L==0) return 0;
    ll &ret = dp[L];
    int &vet = vp[L];
    if(vet==loop) return ret;
    vet=loop;
    ret=oo;
    for(ll i=1;sum[i]<=L;i++)
    {
        ret=min(ret,rec(L-sum[i])+1);
    }
    for(ll i=1;i*i*i<=L;i++)
    {
        ret=min(ret,rec(L-i*i*i)+1);
    }
    return ret;
}

int main()
{
    freopen("ooo.txt","r",stdin);
    sum[0]=0;
    for(ll i=1;i<1000;i++) sum[i]=i*i+sum[i-1];
    ll L,in;
    loop=1;
    for(ll i=0;i<=400010;i++) rec(i);
    for(ll L=0;L<=400000;L++)
    {
        scanf("%lld",&in);
        //if(L==-1) break;
        ll ret=rec(L);
        if(in!=ret)
        PF("%lld %lld %lld\n",L,ret,in);
    }
    return 0;
}
