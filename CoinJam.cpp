/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int sieve[1000010];

void sieve_(void)
{
    sieve[0]=sieve[1]=1LL;
    for(int i=2LL;i<=1000000LL;i++)
    {
        if(sieve[i]==0LL)
        {
            for(int j=2LL;i*j<=1000000LL;j++)
                sieve[i*j]=1LL;
        }
    }
    return;
}



int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);
    sieve_();
    scanf(" %lld",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %lld %lld",&n,&k);
        printf("Case #%lld:\n",p);
        val=1LL<<(n-1LL);
        cnt=0;
        for(i=0LL;i<(1LL<<(n-2LL));i++)
        {
            val+=(i<<1LL);
            val++;
            if(notprime(val)==1&&check(val)==1)
            {
                kk=0;
                while(val!=0)
                {
                    binn[kk]=val%2LL;
                    kk++;
                    val/=2;
                }
                for(j=kk-1LL;j>=0;j--)
                    printf("%lld",binn[kk]);
                for(j=2LL;j<=10LL;j++)
                    printf(" %lld",res[j]);
                printf("\n");
                cnt++;
            }
        }
    }

    return 0;
}

