#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll dp[25][11][3][3][3][3],k,arr[25],arr2[25];

ll rec(ll pos,ll dig,ll flag1,ll flag2,ll fff,ll minfg)
{
    if(pos==k)
    {
      return flag2;
    }
    ll &ret=dp[pos][dig][flag1][flag2][fff][minfg];
    if(ret!=-1)
        return ret;
    ret=0;
    for(ll i=0;i<=9;i++)
    {
        ll ff=fff;
        if(i==0)
            ff=1;
        ll fg2=ff;
        if(arr[pos]>i||(arr[pos]==i&&flag2==1))
        {
                fg2=1;
        }
        if(arr[k-pos-1]>i||flag1==1)
        {
            ll fg1=1;
            printf("%lld %lld %lld %lld\n",pos,i,fg1,fg2);
            ret+=rec(pos+1,i,fg1,fg2,ff,0);
        }
        else if(arr[k-pos-1]==i)
        {
            ll fg1=0;
            printf("%lld %lld %lld %lld\n",pos,i,fg1,fg2);
            ret+=rec(pos+1,i,fg1,fg2,ff,0);
        }
    }
    return ret;
}

ll rec2(ll pos,ll dig,ll flag1,ll flag2,ll fff,ll minfg)
{
    if(pos==k)
    {
      return flag2&(minfg);
    }
    ll &ret=dp[pos][dig][flag1][flag2][fff][minfg];
    if(ret!=-1)
        return ret;
    ret=0;
    for(ll i=0;i<=9;i++)
    {
        ll ff=fff;
        ll mf=minfg;
        if(arr2[pos]<i)
            mf=0;
        else if(arr2[pos]==i)
            mf=minfg;
        else if(arr2[pos]>i)
            mf=1;
        if(i==0)
            ff=1;
        ll fg2=ff;
        if(arr[pos]>i||(arr[pos]==i&&flag2==1))
        {
                fg2=1;
        }
        if(arr[k-pos-1]>i||flag1==1)
        {
            ll fg1=1;
            printf("%lld %lld %lld %lld\n",pos,i,fg1,fg2);
            ret+=rec2(pos+1,i,fg1,fg2,ff,mf);
        }
        else if(arr[k-pos-1]==i)
        {
            ll fg1=0;
            printf("%lld %lld %lld %lld\n",pos,i,fg1,fg2);
            ret+=rec2(pos+1,i,fg1,fg2,ff,mf);
        }
    }
    return ret;
}


main()
{
    ll t,p,a,b,bb;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>a>>b;
        bb=b;
        k=0;
        memset(dp,-1,sizeof(dp));
        while(bb!=0)
        {
            arr[k++]=bb%10;
            bb/=10;
        }
        //cout<<k<<endl;
        ll res=rec(0,0,0,1,0,0);
        cout<<res<<endl;
        b=a-1;
        bb=b;
        k=0;
        memset(dp,-1,sizeof(dp));
        while(bb!=0)
        {
            arr[k++]=bb%10;
            bb/=10;
        }
        res-=rec(0,0,0,1,0,0);
        ll minval=a;
        bb=b;
        k=0;
        memset(dp,-1,sizeof(dp));
        while(bb!=0)
        {
            arr[k++]=bb%10;
            bb/=10;
        }
        bb=a-1;
        while(bb!=0)
        {
            arr2[k++]=bb%10;
            bb/=10;
        }
        memset(dp,-1,sizeof(dp));
        cout<<res<<endl;
        res-=rec2(0,0,0,1,0,1);
        cout<<res<<endl;
    }
    return 0;
}

