/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll t,p,fwd[100010],bwd[100010],n,q,arr[100010],i,from,to;

int main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %lld %lld",&n,&q);
        for(i=1;i<=n;i++)
            scanf(" %lld",&arr[i]);
        fwd[0]=bwd[n+1]=0;
        for(i=1;i<=n;i++)
        {
            fwd[i]=__gcd(fwd[i-1],arr[i]);
        }
        for(i=n;i>=1;i--)
        {
            bwd[i]=__gcd(bwd[i+1],arr[i]);
        }
        for(i=1;i<=q;i++)
        {
            scanf(" %lld %lld",&from,&to);
            printf("%lld\n",__gcd(fwd[from-1],bwd[to+1]));
        }
    }
    return 0;
}

