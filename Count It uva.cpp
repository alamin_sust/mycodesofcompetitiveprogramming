/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};
ll bigmod(ll a,ll b,ll m){     ll ret=1; while(b>0)      {if(b%2==1){ret=(ret*a)%m;}b/=2;a=(a*a)%m;} return ret;}//a^b%m
ll inverse_mod(ll b, ll m){return bigmod(b,m-2,m);} //(1/b)%m


ll n,t,p;

ll rec(ll val)
{
    if(val==0LL)
        return 0;
    return (val%2LL)+rec(val/2LL);
}


int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    cin>>t;

    for(p=1;p<=t;p++)
    {
        scanf(" %lld",&n);
        printf("%lld\n",rec(n));
    }


    return 0;
}





