
/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};


int dp[1010][1010],arr[1010],n,k;

int lcs(int now,int val)
{
    if(now>n)
        return val;
    int &ret=dp[now][val];
    if(ret!=-1)
        return ret;
    ret=0;
    if(arr[now+1]>arr[now])
        ret=lcs(now+1,val+1);
    else
        ret=max(ret,lcs(now+1,val));
    return ret;
}

main()
{
    int i,j,mx=0;
    cin>>n>>k;
    for(i=1;i<=k;i++)
    {
        for(j=1;j<=n;j++)
        {
            scanf(" %d",&arr[j]);
        }
        arr[n+1]=0;
        memset(dp,-1,sizeof(dp));
        mx=max(mx,lcs(1,1));
    }
    printf("%d\n",mx);
    return 0;
}

