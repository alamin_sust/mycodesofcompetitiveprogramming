#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class RelationClassifier
{
public:
	string isBijection(vector <int> domain, vector <int> range)
	{
        map<int,int>mpp1;
        map<int,int>mpp2;
        mpp1.clear();
        mpp2.clear();
        int flag1=0,flag2=0;
        for(int i=0;i<domain.size();i++)
        {
            if(mpp1[domain[i]]==0)
            {
                mpp1[domain[i]]=range[i];
                continue;
            }
            if(mpp1[domain[i]]!=range[i])
            {
                flag1=1;
            }
        }
        for(int i=0;i<range.size();i++)
        {
            if(mpp2[range[i]]==0)
            {
                mpp2[range[i]]=domain[i];
                continue;
            }
            if(mpp2[range[i]]!=domain[i])
            {
                flag2=1;
            }
        }
        if(flag1|flag2)
            return "Not";
        return "Bijection";
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	RelationClassifier objectRelationClassifier;

	//test case0
	vector <int> param00;
	param00.push_back(1);
	param00.push_back(1);
	vector <int> param01;
	param01.push_back(2);
	param01.push_back(3);
	string ret0 = objectRelationClassifier.isBijection(param00,param01);
	string need0 = "Not";
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(4);
	param10.push_back(5);
	vector <int> param11;
	param11.push_back(2);
	param11.push_back(2);
	string ret1 = objectRelationClassifier.isBijection(param10,param11);
	string need1 = "Not";
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(1);
	vector <int> param21;
	param21.push_back(2);
	string ret2 = objectRelationClassifier.isBijection(param20,param21);
	string need2 = "Bijection";
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(1);
	param30.push_back(2);
	param30.push_back(3);
	param30.push_back(4);
	param30.push_back(5);
	vector <int> param31;
	param31.push_back(1);
	param31.push_back(2);
	param31.push_back(3);
	param31.push_back(4);
	param31.push_back(5);
	string ret3 = objectRelationClassifier.isBijection(param30,param31);
	string need3 = "Bijection";
	assert_eq(3,ret3,need3);

	//test case4
	vector <int> param40;
	param40.push_back(14);
	param40.push_back(12);
	param40.push_back(10);
	param40.push_back(13);
	param40.push_back(20);
	param40.push_back(18);
	param40.push_back(9);
	param40.push_back(17);
	param40.push_back(14);
	param40.push_back(9);
	vector <int> param41;
	param41.push_back(18);
	param41.push_back(6);
	param41.push_back(8);
	param41.push_back(15);
	param41.push_back(2);
	param41.push_back(14);
	param41.push_back(10);
	param41.push_back(13);
	param41.push_back(13);
	param41.push_back(15);
	string ret4 = objectRelationClassifier.isBijection(param40,param41);
	string need4 = "Not";
	assert_eq(4,ret4,need4);

}
