
#include<stdio.h>
int carry=0,ans=0,flag=0;
int carry_func(int x,int y)
{
    if(x==0 && y==0) return carry;
    if(flag!=carry) flag=1;
    if(x%10 + y%10 +flag>9) carry++;
    flag=0;
    carry_func(x/10,y/10);
}
int main()
{
    long long int x,y;
    while(scanf(" %lld%lld",&x,&y)==2)
    {
        carry=0;
        if(x==0 && y==0) break;
        ans=carry_func(x,y);
        if(ans==1)
            printf("%d carry operation.\n",ans);
        else if(ans!=0)printf("%d carry operations.\n",ans);
        else printf("No carry operation.\n");
    }
    return 0;
}
