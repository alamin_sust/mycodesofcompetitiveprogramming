/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll n,m,k,arr[5010],cum[5050],dp[5010][5010];

ll rec(ll pos,ll taken)
{
    if(taken==k)
        return 0;
    if(pos>n)
    {
        return -99999999999999LL;
    }
    ll &ret=dp[pos][taken];
    if(ret!=-1)
        return ret;
    ret=0;
    if((pos+m-1)<=n)
    ret=cum[pos+m-1]-cum[pos-1]+rec(pos+m,taken+1);
    ret=max(ret,rec(pos+1,taken));
    return ret;
}

main()
{
    cin>>n>>m>>k;
    for(ll i=1;i<=n;i++)
    {
        scanf(" %I64d",&arr[i]);
        cum[i]=cum[i-1]+arr[i];
    }
    memset(dp,-1,sizeof(dp));
    cout<<rec(1,0)<<endl;
    return 0;
}

