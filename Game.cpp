/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int dp[53][53][2];
int k1,k2,n1,n2;

int rec(int taken1,int taken2,int turn)
{
    if(turn==0&&taken1>=n1)
        return 0;
    if(turn==1&&taken2>=n2)
        return 1;
    int &ret=dp[taken1][taken2][turn];
    if(ret!=-1)
        return ret;
    ret=turn;
    if(turn==0)
    {
        for(int i=1; i<=k1&&(i+taken1)<=n1; i++)
        {
            ret|=rec(taken1+i,taken2,turn^1);
        }
    }
    else
    {
        for(int i=1; i<=k2&&(i+taken2)<=n2; i++)
        {
            ret&=rec(taken1,taken2+i,turn^1);
        }
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    cin>>n1>>n2>>k1>>k2;
    memset(dp,-1,sizeof(dp));
    if(rec(0,0,0))
        printf("First\n");
    else
        printf("Second\n");
    return 0;
}





