/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int i,j,mpp[100010],n,m,k,res,from,to,val,in;
vector<int>adj[100010],cost[100010];


int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);


    scanf(" %d %d %d",&n,&m,&k);

    for(i=1;i<=m;i++)
    {
        scanf(" %d %d %d",&from,&to,&val);
        adj[from].push_back(to);
        cost[from].push_back(val);
        adj[to].push_back(from);
        cost[to].push_back(val);
    }

    for(i=1;i<=k;i++)
    {
        scanf(" %d",&in);
        mpp[in]=1;
    }
    res=1000000010;
    for(i=1;i<=n;i++)
    {
        if(mpp[i])
        continue;
        for(j=0;j<adj[i].size();j++)
        {
            from=i;
            to=adj[i][j];
            val=cost[i][j];
            if(mpp[to])
            {
                res=min(res,val);
            }
        }
    }
    if(res==1000000010)
        printf("-1\n");
    else
        printf("%d\n",res);

    return 0;
}






