/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int dp[44][2005],d,tme,k;
char arr[100010],type[100010];
vector<int>types[110];
map<string,int>mpp;

int rec(int pos,int taken)
{
    if(taken>d)
        return -99999999;
    if(pos==k)
        return taken;
    int &ret=dp[pos][taken];
    if(ret!=-1)
        return ret;
    ret=-99999999;
    for(int i=0;i<types[pos].size();i++)
    {
        //printf("%d %d %d\n",pos,i,types[pos][i]);
        ret=max(ret,rec(pos+1,taken+types[pos][i]));
    }
    return ret;
}

int main()
{
    int t,p,n,i,res;
    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        scanf(" %d",&d);
        scanf(" %d",&n);
        mpp.clear();
        for(k=1,i=1;i<=n;i++)
        {
            scanf(" %s %s %d",&arr,&type,&tme);
            if(mpp[type]==0)
            {
                types[k].push_back(tme);
                mpp[type]=k;
                k++;
            }
            else
            {
                types[mpp[type]].push_back(tme);
            }
        }
       /* for(i=1;i<k;i++)
        {
            for(int j=0;j<types[i].size();j++)
            {
                printf("%d ",types[i][j]);
            }
            printf("\n");
        }*/
        memset(dp,-1,sizeof(dp));
        res=rec(1,0);
        if(res>=0)
        printf("Case %d: %d\n",p,res);
        else
        printf("Case %d: Movie show canceled!\n",p);
        for(i=1;i<=k;i++)
        types[i].clear();
    }
    return 0;
}
/*
3
610 10
Most_Welcome Comedy 240
Most_Welcome_2 Comedy 180
Mission_Impossible Action 100
Frozen Animation 120
Ice_Age Animation 100
Mama Horror 200
I_Dont_Care Comedy 200
Jhole_Mon_Gacher_Dale Action 110
Dhil_Mari_Tor_Tiner_Chale Action 210
Amti_Ami_Khabo_Pere Animation 240
*/

