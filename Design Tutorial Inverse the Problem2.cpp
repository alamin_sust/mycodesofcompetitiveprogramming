/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int arr[2010][2010],par[2010];

int join(int x,int y)
{
   int a=x;
   int b=y;
   while(par[a]!=a)
    a=par[a];
   while(par[b]!=b)
    b=par[b];
    if(a==b)
        return 0;
    par[a]=b;
    arr[x][y]=arr[y][x]=0;
    return 1;

}

int main()
{
    int n,i,j;
    cin>>n;
    for(i=1;i<=n;i++)
        par[i]=i;
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
        {
           scanf(" %d",&arr[i][j]);
        }
    }
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
        {
           if(i==j&&arr[i][j])
           {printf("NO\n");
           return 0;}
           if(i==j)
            continue;
           if(arr[i][j])
           {
               if(arr[i][j]!=arr[j][i]||join(i,j)==0)
               {
                  printf("%d %d\n",i,j);
                  printf("NO\n");
                  return 0;
               }
           }
        }
    }
    printf("YES\n");
    return 0;
}


