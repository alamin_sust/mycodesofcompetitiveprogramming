/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll idx,rem,nrem,i,arr[100010],n,k,num;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %I64d %I64d",&n,&k);

    rem=1000000000000000010LL;
    idx=0;

    for(i=1LL;i<=k;i++) {
        scanf(" %I64d",&arr[i]);
        nrem=n%arr[i];
        if(nrem<rem) {
            rem=nrem;
            idx=i;
            num=(n-nrem)/arr[i];
        }
    }

    printf("%I64d %I64d\n",idx,num);

    return 0;
}
