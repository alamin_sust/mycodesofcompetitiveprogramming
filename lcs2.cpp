#include<stdio.h>
#include<string.h>

char s1[1010], s2[1010];
int l1, l2;
int dp[1010][1010];
int rec(int i, int j);

int main()
{
    int i, j, k, l, m;
    while(gets(s1)&&s1[0]!='#')
    {
        gets(s2);
        l1=strlen(s1);
        l2=strlen(s2);
        for(i=0; i<=l1; i++)
            for(j=0; j<=l2; j++)
                dp[i][j]=-1;
        m=rec(0, 0);
        printf("%d\n", m);
    }
    return 0;
}

int rec(int i, int j)
{
    if(i==l1)
        return 30*(l2-j);
    if(j==l2)
        return 15*(l1-i);
    int &r=dp[i][j], a, b;
    if(r!=-1)
        return r;
    if(s1[i]==s2[j])
        r=rec(i+1, j+1);
    else
    {
        a=rec(i+1, j)+15;
        b=rec(i, j+1)+30;
        r=a>b?b:a;
    }
    return r;
}

