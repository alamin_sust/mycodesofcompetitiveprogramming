#include <bits/stdc++.h>

using namespace std;

vector<string> split_string(string);

int n,m,i,j,res,curr_cont_left,curr_pack_left;
pair<int,int> pack[2510],cont[2510];

int comp(pair<int,int> a,pair<int,int> b) {
    return a.first<b.first;
}

bool can_inserted(double edge, double r) {

    return (r*r*2.0*2.0)>(2.0*edge*edge);

}

/*
 * Complete the function below.
 */
int maximumPackages(vector<int> S, vector<int> K, vector<int> R, vector<int> C) {
    // Return the maximum number of packages that can be put in the containers.
    int n = S.size();
    int m = R.size();

    for(i=0;i<n;i++) {
        pack[i].first=S[i];
    }
    for(i=0;i<n;i++) {
        pack[i].second=K[i];
    }

    for(i=0;i<m;i++) {
        cont[i].first=R[i];
    }
    for(i=0;i<m;i++) {
        cont[i].second=C[i];
    }

    sort(pack,pack+n,comp);
    sort(cont,cont+m,comp);

    res=0;
    curr_pack_left=pack[0].second;
    curr_cont_left=cont[0].second;
    for(i=0,j=0;i<n&&j<m;) {
        if(!can_inserted((double)pack[i].first,(double)cont[j].first)) {
            curr_cont_left=cont[++j].second;
            continue;
        }
        if(curr_pack_left==0) {
            i++;
            if(i<n)
            curr_pack_left=pack[i].second;
            continue;
        }
        if(curr_cont_left==0) {
            j++;
            if(j<m)
            curr_cont_left=cont[j].second;
            continue;
        }

        if(curr_pack_left==curr_cont_left) {
            res+=curr_pack_left;
            curr_pack_left=curr_cont_left=0;
        } else if(curr_pack_left<curr_cont_left) {
            res+=curr_pack_left;
            curr_cont_left-=curr_pack_left;
        } else {
            res+=curr_cont_left;
            curr_pack_left-=curr_cont_left;
        }
    }


    return res;
}


int main() { ofstream fout(getenv("OUTPUT_PATH"));

string nm_temp;
int n,m;
cin>>n>>m;
vector<int> S(n),K(n),R(m),C(m);
for(int i=0;i<n;i++)
    cin>>S[i];
for(int i=0;i<n;i++)
    cin>>K[i];
for(int i=0;i<m;i++)
    cin>>R[i];
for(int i=0;i<m;i++)
    cin>>C[i];
int result = maximumPackages(S, K, R, C);

fout << result << "\n";

fout.close();

return 0;
}

