#include <iostream>
#include <string>

using namespace std;

int binToI(string s){
  int i;
  int value = 0;
  int n = 128;

  for (i=0; i<s.length(); i++)
    {
      while (s[i] == '|' || s[i] == '.')
        i++;
      if (s[i] == 'o'){
        value = value + n;
      }
      n=n/2;
    }

  return value;
}


int main(){

  int i;
  char c;
  int val;
  string s;


  getline(cin, s);
  while (!cin.eof()){
    val = binToI(s);
    c = (char)val;
    cout<<c;
    getline(cin, s);
  }

  return 0;
}

