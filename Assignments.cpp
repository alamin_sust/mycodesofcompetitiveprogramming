#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll dp[1<<20],n,arr[22][22];

ll rec(ll mask)
{
    if(mask==((1<<n)-1))
        return 1;
    else if(mask>=(1<<n))
        return 0;
    ll bits=__builtin_popcount(mask);
    ll &ret=dp[mask];
    if(ret!=-1)
        return ret;
    ret=0;
    for(ll i=0;i<n;i++)
    {
        if(arr[i][bits]==1&&((1<<i)&mask)==0)
            ret+=rec((1<<i)|mask);
    }
    return ret;
}

main()
{
    ll p,t,i,j;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        memset(dp,-1,sizeof(dp));
        cin>>n;
        for(i=0;i<n;i++)
        {
            for(j=0;j<n;j++)
                cin>>arr[i][j];
        }
        cout<<rec(0)<<endl;
    }
    return 0;
}

