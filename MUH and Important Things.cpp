/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
int prio,ind;
};
node arr[2010];
int i,n,flag,k,res[2010];

int comp(node a,node b)
{
    return a.prio<b.prio;
}

main()
{
    cin>>n;
    for(i=0;i<n;i++)
    {
        scanf(" %d",&arr[i].prio);
        arr[i].ind=i+1;
    }
    sort(arr,arr+n,comp);
    for(i=1;i<n;i++)
    {
        if(arr[i].prio==arr[i-1].prio)
            k++;
    }
    if(k<2)
        printf("NO\n");
    else
    {
        printf("YES\n");
        for(i=0;i<n;i++)
            printf("%d ",arr[i].ind);
        printf("\n");
        for(i=0;i<n;i++)
        {
            if(flag==0&&arr[i].prio==arr[i+1].prio)
            {
                printf("%d %d ",arr[i+1].ind,arr[i].ind);
                i++;
                flag=1;
            }
            else
            {
                printf("%d ",arr[i].ind);
            }
        }
        printf("\n");
        flag=0;
        for(i=n-1;i>=0;i--)
        {
            if(flag==0&&arr[i].prio==arr[i-1].prio)
            {
                res[i]=arr[i-1].ind,
                res[i-1]=arr[i].ind;
                i--;
                flag=1;
            }
            else
            {
                res[i]=arr[i].ind;
            }
        }
        for(i=0;i<n;i++)
            printf("%d ",res[i]);
        printf("\n");
    }
    return 0;
}

