/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

string multiply(string a,string b)
{
  /* initially fill the result string with 0's */
  //cout<<a<<" "<<b<<endl;
  int total_len= a.size() + b.size();
  string result(total_len, '0');
  int num;                                      /* intermediate store */
  int i, j;
  int last_pos_i, last_pos_j, last_pos_k;

  last_pos_i= total_len - 1;

  /* i-loop */
  for (i= b.size() - 1; i >= 0; i --)
  {
    last_pos_j= last_pos_i;

    /* j-loop */
    for (j= a.size() - 1; j >=0; j --)
    {
      last_pos_k= last_pos_j;
      num= (a[j]-'0') * (b[i]-'0');

      /* k-loop : the actual updation of result string takes place here. */
      while (num)
      {
        num += (result[last_pos_k]-'0');
        result[last_pos_k]= ((num % 10)+'0');
        /* 'carry' it forward.. ;)  */
        num= num / 10;
        last_pos_k --;
      }

      last_pos_j --;
    }

    last_pos_i --;
  }
  return result;
}
int main()
{
    int n;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    cin>>n;
    string s1="1",s2;
    for(int i=1;i<=n;i++)
    {
          cin>>s2;
          s1=multiply(s1,s2);
          //cout<<s1<<endl;
    }
    cout<<s1<<endl;
    return 0;
}





