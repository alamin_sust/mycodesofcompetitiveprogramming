#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class BuildingTowers
{
public:
	long long maxHeight(int N, int K, vector <int> x, vector <int> t)
	{
	    long long res=0LL,i;
	    if(x.size()==0)
            return K*N*(N-1LL)/2LL;

	    now=1;

	    for(i=0;i<x.size()&&i<t.size();i++)
        {

        }

	    return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	BuildingTowers objectBuildingTowers;

	//test case0
	int param00 = 10;
	int param01 = 1;
	vector <int> param02;
	param02.push_back(3);
	param02.push_back(8);
	vector <int> param03;
	param03.push_back(1);
	param03.push_back(1);
	long long ret0 = objectBuildingTowers.maxHeight(param00,param01,param02,param03);
	long long need0 = 3;
	assert_eq(0,ret0,need0);

	//test case1
	int param10 = 1000000000;
	int param11 = 1000000000;
	vector <int> param12;
	vector <int> param13;
	long long ret1 = objectBuildingTowers.maxHeight(param10,param11,param12,param13);
	long long need1 = 999999999000000000;
	assert_eq(1,ret1,need1);

	//test case2
	int param20 = 20;
	int param21 = 3;
	vector <int> param22;
	param22.push_back(4);
	param22.push_back(7);
	param22.push_back(13);
	param22.push_back(15);
	param22.push_back(18);
	vector <int> param23;
	param23.push_back(8);
	param23.push_back(22);
	param23.push_back(1);
	param23.push_back(55);
	param23.push_back(42);
	long long ret2 = objectBuildingTowers.maxHeight(param20,param21,param22,param23);
	long long need2 = 22;
	assert_eq(2,ret2,need2);

	//test case3
	int param30 = 780;
	int param31 = 547990606;
	vector <int> param32;
	param32.push_back(34);
	param32.push_back(35);
	param32.push_back(48);
	param32.push_back(86);
	param32.push_back(110);
	param32.push_back(170);
	param32.push_back(275);
	param32.push_back(288);
	param32.push_back(313);
	param32.push_back(321);
	param32.push_back(344);
	param32.push_back(373);
	param32.push_back(390);
	param32.push_back(410);
	param32.push_back(412);
	param32.push_back(441);
	param32.push_back(499);
	param32.push_back(525);
	param32.push_back(538);
	param32.push_back(568);
	param32.push_back(585);
	param32.push_back(627);
	param32.push_back(630);
	param32.push_back(671);
	param32.push_back(692);
	param32.push_back(699);
	param32.push_back(719);
	param32.push_back(752);
	param32.push_back(755);
	param32.push_back(764);
	param32.push_back(772);
	vector <int> param33;
	param33.push_back(89);
	param33.push_back(81);
	param33.push_back(88);
	param33.push_back(42);
	param33.push_back(55);
	param33.push_back(92);
	param33.push_back(19);
	param33.push_back(91);
	param33.push_back(71);
	param33.push_back(42);
	param33.push_back(72);
	param33.push_back(18);
	param33.push_back(86);
	param33.push_back(89);
	param33.push_back(88);
	param33.push_back(75);
	param33.push_back(29);
	param33.push_back(98);
	param33.push_back(63);
	param33.push_back(74);
	param33.push_back(45);
	param33.push_back(11);
	param33.push_back(68);
	param33.push_back(34);
	param33.push_back(94);
	param33.push_back(20);
	param33.push_back(69);
	param33.push_back(33);
	param33.push_back(50);
	param33.push_back(69);
	param33.push_back(54);
	long long ret3 = objectBuildingTowers.maxHeight(param30,param31,param32,param33);
	long long need3 = 28495511604;
	assert_eq(3,ret3,need3);

	//test case4
	int param40 = 7824078;
	int param41 = 2374;
	vector <int> param42;
	param42.push_back(134668);
	param42.push_back(488112);
	param42.push_back(558756);
	param42.push_back(590300);
	param42.push_back(787884);
	param42.push_back(868112);
	param42.push_back(1550116);
	param42.push_back(1681439);
	param42.push_back(1790994);
	param42.push_back(1796091);
	param42.push_back(1906637);
	param42.push_back(2005485);
	param42.push_back(2152813);
	param42.push_back(2171716);
	param42.push_back(2255697);
	param42.push_back(2332732);
	param42.push_back(2516853);
	param42.push_back(2749024);
	param42.push_back(2922558);
	param42.push_back(2930163);
	param42.push_back(3568190);
	param42.push_back(3882735);
	param42.push_back(4264888);
	param42.push_back(5080550);
	param42.push_back(5167938);
	param42.push_back(5249191);
	param42.push_back(5574341);
	param42.push_back(5866912);
	param42.push_back(5936121);
	param42.push_back(6142348);
	param42.push_back(6164177);
	param42.push_back(6176113);
	param42.push_back(6434368);
	param42.push_back(6552905);
	param42.push_back(6588059);
	param42.push_back(6628843);
	param42.push_back(6744163);
	param42.push_back(6760794);
	param42.push_back(6982172);
	param42.push_back(7080464);
	param42.push_back(7175493);
	param42.push_back(7249044);
	vector <int> param43;
	param43.push_back(8);
	param43.push_back(9);
	param43.push_back(171315129);
	param43.push_back(52304509);
	param43.push_back(1090062);
	param43.push_back(476157338);
	param43.push_back(245);
	param43.push_back(6);
	param43.push_back(253638067);
	param43.push_back(37);
	param43.push_back(500);
	param43.push_back(29060);
	param43.push_back(106246500);
	param43.push_back(129);
	param43.push_back(22402);
	param43.push_back(939993108);
	param43.push_back(7375);
	param43.push_back(2365707);
	param43.push_back(40098);
	param43.push_back(10200444);
	param43.push_back(3193547);
	param43.push_back(55597);
	param43.push_back(24920935);
	param43.push_back(905027);
	param43.push_back(1374);
	param43.push_back(12396141);
	param43.push_back(525886);
	param43.push_back(41);
	param43.push_back(33);
	param43.push_back(3692);
	param43.push_back(11502);
	param43.push_back(180);
	param43.push_back(3186);
	param43.push_back(5560);
	param43.push_back(778988);
	param43.push_back(42449532);
	param43.push_back(269666);
	param43.push_back(10982579);
	param43.push_back(48);
	param43.push_back(3994);
	param43.push_back(1);
	param43.push_back(9);
	long long ret4 = objectBuildingTowers.maxHeight(param40,param41,param42,param43);
	long long need4 = 1365130725;
	assert_eq(4,ret4,need4);

}

