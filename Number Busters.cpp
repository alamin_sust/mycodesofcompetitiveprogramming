#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll a,b,c,w,x,z,k,j,steps;

main()
{
    cin>>a>>b>>w>>x>>c;
    z=w-x;
    steps=0;
    while(c>a)
    {
        if(b<x)
        {
            k=0;
            k=(x-b)/z;
            if((x-b)%z!=0)
                k++;
            b+=(z*k);
            steps+=k;
            a-=k;
            c-=k;
        }
        else
        {
            j=(b-x+1)/x;
            if((b-x+1)%x!=0)
                j++;
            k=min(c-a,j);
            steps+=k;
            c-=k;
            b-=(x*k);
        }
    }
    printf("%I64d\n",steps);
    return 0;
}

