#include <stdio.h>
#include<string.h>
#include <limits.h>

#define MAXCOINS 100
#define MAXCENTS 500
#define MAXSUM MAXCOINS * MAXCENTS
#define ABS(A) ((A) < 0 ? -((A)) : (A))

int possible[MAXSUM + 1];
int ncoins, value, sum;

int main()
{
    int problems, i, j, min;

    scanf("%d", &problems);
    while (problems)
    {
        memset(possible, 0, sizeof(possible));
        possible[0] = 1;

        sum = 0;

        scanf("%d", &ncoins);
        for (i = 0; i < ncoins; i++)
        {
            scanf("%d", &value);
            sum += value;

            for (j = MAXSUM - value; j >= 0; j--)
                if (possible[j])
                    possible[j + value] = 1;
        }

        min = INT_MAX;

        for (i = 0; i <= sum; i++)
            if (possible[i] && ABS(2 * i - sum) < min)
                min = ABS(2 * i - sum);

        printf("%d\n", min);

        problems--;
    }

    return 0;
}
