/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll n,k,m,dp[5003][5003],d,arr[100010];

ll rec(ll pos,ll taken)
{
    if(pos>n)
    {
        if(taken==k)
            return 0LL;
        return -9999999999999LL;
    }
    //ll &ret=dp[pos][taken];
    //if(ret!=-1LL)
    // return ret;
    ll ret=0LL;
    for(ll i=pos+1LL; i<=n; i++)
    {
        if(pos!=0&&(i-pos)<=d)
        {
            ret=max(ret,rec(i,taken+1LL)+(arr[i-1]-arr[pos])*m);
        }
        else
        {
            if(((i-pos)<=d)&&pos==0)
                ret=max(ret,rec(i,taken+1LL)+(arr[i-1]-arr[pos]));
            else if(pos==0)
            {
                printf("%lld %lld %lld %lld\n",pos,i,(arr[pos+d]-arr[pos]),(arr[i-1]-arr[pos+d]));
                ret=max(ret,rec(i,taken+1LL)+(arr[pos+d]-arr[pos])+(arr[i-1]-arr[pos+d]));
            }
            else
            {

                printf("%lld %lld %lld %lld.....\n",pos,i,(arr[pos+d]-arr[pos])*m,(arr[i-1]-arr[pos+d]));

                ret=max(ret,rec(i,taken+1LL)+(arr[pos+d]-arr[pos])*m+(arr[i-1]-arr[pos+d]));
            }
        }

    }
    //if((pos-last)<=d&&last!=0LL)
    //   ret=max(rec(pos+1LL,pos,taken+1LL),rec(pos+1LL,last,taken)+arr[pos]*m);
    // else
    //    ret=max(rec(pos+1LL,pos,taken+1LL),rec(pos+1LL,last,taken)+arr[pos]);
    //printf("%lld %lld %lld\n",pos,last,taken);
    //printf("...%lld\n",ret);
    return ret;
}

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    ll t,p,i;

    scanf(" %lld",&t);

    for(p=1LL; p<=t; p++)
    {
        scanf(" %lld %lld %lld %lld",&n,&k,&d,&m);

        for(i=1LL; i<=n; i++)
        {
            scanf(" %lld",&arr[i]);
            arr[i]+=arr[i-1];
        }
        for(ll i1=0LL; i1<=(n+1LL); i1++)
            for(ll i2=0LL; i2<=(n+1LL); i2++)
                dp[i1][i2]=-1LL;

        //memset(dp,-1LL,sizeof(dp));
        printf("%lld\n",rec(0LL,0LL));

    }

    return 0;
}

