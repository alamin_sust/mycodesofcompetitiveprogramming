#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll n,i,now,a[300010];

struct node{
   ll val,x;
};

node arr[300010];

bool comp1(node a,node b)
{
    return a.val<b.val;
}

bool comp2(node a,node b)
{
    return a.x<b.x;
}

main()
{
    scanf(" %I64d",&n);
    for(i=0;i<n;i++)
    {
        scanf(" %I64d",&arr[i].val);
        a[i]=arr[i].val;
        arr[i].x=i;
    }
    sort(arr,arr+n,comp1);
    now=arr[0].val;
    for(i=0;i<n;now++,i++)
    {
        if(arr[i].val<=now)
            a[arr[i].x]=now;
        else
        {
            now=arr[i].val;
        }
    }
    for(i=0;i<n;i++)
    {
        printf("%I64d ",a[i]);
    }
    printf("\n");
    return 0;
}

