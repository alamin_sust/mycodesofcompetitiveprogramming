/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int i,j,k1,k2,cry,lst,l,num1[210],num2[210],res[210];
char arr[310];

int main()
{
    freopen("InputFile-7.txt","r",stdin);
    freopen("BigSumOutput.txt","w",stdout);
    while(scanf("%s",arr)!=EOF)
    {
        memset(num1,0,sizeof(num1));
        memset(num2,0,sizeof(num2));
        memset(res,0,sizeof(res));
        l=strlen(arr);
        k1=k2=0;
        for(i=0;i<l&&arr[i]!=',';i++)
        {
            if(arr[i]>='0'&&arr[i]<='9')
            {
                num1[k1++]=arr[i]-'0';
            }
        }
        i++;
        for(;i<l&&arr[i]!=',';i++)
        {
            if(arr[i]>='0'&&arr[i]<='9')
            {
                num2[k2++]=arr[i]-'0';
            }
        }
        lst=0;
        cry=0;
        for(i=0;i<102;i++)
        {
            res[i]=(num1[i]+num2[i]+cry)%10;
            if(res[i]>0)
                lst=i;
            cry=(num1[i]+num2[i]+cry)/10;
        }
        i=0;
        while(res[i]==0&&i<102)
        {
            i++;
        }
        if(i>lst)
            printf("0");
        else
        for(;i<=lst;i++)
            printf("%d",res[i]);
        printf("\n");
    }
    return 0;
}





