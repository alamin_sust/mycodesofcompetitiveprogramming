#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class CultureShock
{
public:
	string translate(string text)
	{
	    int i;
	    printf("%d\n",text.size());
	    for(i=0;i<text.size();i++)
	    {
	        if(i>1)
	        if(text[i]=='E'&&text[i-1]=='E'&&text[i-2]=='Z')
            if(i==(text.size()-1)||text[i+1]==' ')
            if(i==2||text[i-3]==' ')
	        text[i]='D';
	    }
	    printf("....");
	    return text;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	CultureShock objectCultureShock;

	//test case0
	string param00 = "THE TWENTY SIXTH LETTER OF THE ALPHABET IS ZEE";
	string ret0 = objectCultureShock.translate(param00);
	string need0 = "THE TWENTY SIXTH LETTER OF THE ALPHABET IS ZED";
	assert_eq(0,ret0,need0);

	//test case1
	string param10 = "ZEE";
	string ret1 = objectCultureShock.translate(param10);
	string need1 = "ZED";
	assert_eq(1,ret1,need1);

	//test case2
	string param20 = "SPELLING IN ENGLISH IS EZEE";
	string ret2 = objectCultureShock.translate(param20);
	string need2 = "SPELLING IN ENGLISH IS EZEE";
	assert_eq(2,ret2,need2);

	//test case3
	string param30 = "ZEE ZEE ZED ZEDS ZEE ZEES";
	string ret3 = objectCultureShock.translate(param30);
	string need3 = "ZED ZED ZED ZEDS ZED ZEES";
	assert_eq(3,ret3,need3);

}

