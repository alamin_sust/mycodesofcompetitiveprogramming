This repository contains my solved, partially solved and unsolved problems of various Online and Onsite Programming Contests, Online Judges, Problem Solving Sites etc.
 Most of the problems are from: codeforces.com, uva.onlinejudge.org, codechef.com, different contest hosted by google(codejam, apac etc), facebook(hackercup), hackerrank.com, rosalind.info and topcoder.com.
 
 The names of the codes are similar to the actual name of the problem. if duplicate name appeared, i tried to add a proper suffix/prefix to identify easily.
 
 --- Md. Al-Amin (alaminbbsc@gmail.com)