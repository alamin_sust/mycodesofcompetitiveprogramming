/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll n,p,q,exx,com,val,ind,arr[100010],i,R,res,M=1000000007LL;
double lnA,ex;

ll ch[10010],ch2[10010],k,k2,tp[12][10010],len[12],carry,ii,jj,sum;

void mul(ll v)
{
//    printf("%lld %lld\n",k,k2);
//    for(ii=20;ii>=0;ii--)
//    {
//        printf("%lld ",ch[ii]);
//    }
//    printf("\n");

    k2=0LL;
    while(v)
    {
        ch2[k2++]=(v%10LL);
        v/=10LL;
    }

//    for(ii=20;ii>=0;ii--)
//    {
//        printf("%lld ",ch2[ii]);
//    }
//    printf("\n");

    for(ii=0;ii<12;ii++)
        for(jj=0;jj<1110;jj++)
        tp[ii][jj]=0LL;
    memset(len,0,sizeof(len));
    carry=0LL;
    for(ii=0;ii<k2;ii++)
    {
        carry=0LL;
        for(jj=0;jj<k;jj++)
        {
            tp[ii][ii+len[ii]++]=(ch[jj]*ch2[ii]+carry)%10LL;
            carry=(ch[jj]*ch2[ii]+carry)/10LL;
        }
        while(carry)
        {
            tp[ii][ii+len[ii]++]=carry%10LL;
            carry/=10LL;
        }
    }


    for(ii=0;ii<10005;ii++)
    {
        sum=0LL;
        for(jj=0;jj<k2;jj++)
        {
            sum+=tp[jj][ii];
        }
        ch[ii]=(sum+carry)%10LL;
        carry=(sum+carry)/10LL;
        if(ch[ii])
            k=ii+1LL;
    }

//    for(ii=20;ii>=0;ii--)
//    {
//        printf("%lld ",ch[ii]);
//    }
//    printf("\n");
    return;
}


int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);


    scanf(" %lld",&n);

    for(i=1;i<=n;i++)
    {
        scanf(" %lld",&arr[i]);
    }

    scanf(" %lld",&q);

    for(p=1;p<=q;p++)
    {
        scanf(" %lld",&com);
        if(com==1)
        {
            scanf(" %lld %lld",&ind,&val);
            arr[ind]=val;
        }
        else
        {
            scanf(" %lld",&R);
            res=1LL;
            for(i=0;i<10005;i++)
                ch[i]=0;
            ch[0]=1;
            k=1;
            for(i=1;i<=n;i+=R)
            {

                mul(arr[i]);

                res=(res*arr[i])%M;
            }
            printf("%lld %lld\n",ch[k-1],res);
        }
    }

    return 0;
}




