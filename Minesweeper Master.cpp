#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int r,c,n,bombs,p,t,i,j,k,flag,arr[110][110];

main()
{
    FILE *read,*write;
    read=fopen("codejam_c_input.txt","r");
    write=fopen("codejam_c_output.txt","w");
    fscanf(read," %d",&t);
    for(p=1;p<=t;p++)
    {
        memset(arr,0,sizeof(arr));
        fscanf(read," %d %d %d",&r,&c,&bombs);
        n=max(r,c);
        flag=0;
        k=r*c-bombs-1;
        arr[1][1]=2;
        if(k!=0)
        {
        for(i=2;i<=n&&k>0;i++)
        {
            for(j=1;j<=i;j++)
            {
                if(arr[i][j]==0&&i<=r&&j<=c&&k>0)
                {
                    k--;
                    arr[i][j]=1;
                }
                if(arr[j][i]==0&&i<=c&&j<=r&&k>0)
                {
                    k--;
                    arr[j][i]=1;
                }
            }
        }
        if(arr[1][2]==0&&r>=1&&c>=2)
            flag=1;
        if(arr[2][1]==0&&r>=2&&c>=1)
            flag=1;
        if(arr[2][2]==0&&r>=2&&c>=2)
            flag=1;
        }
        fprintf(write,"Case #%d:\n",p);
        if(flag==1)
            fprintf(write,"Impossible\n");
        else
        {
            for(i=1;i<=r;i++)
            {
                for(j=1;j<=c;j++)
                {
                    if(arr[i][j]==1)
                        fprintf(write,".");
                    else if(arr[i][j]==0)
                        fprintf(write,"*");
                    else
                        fprintf(write,"c");
                }
                fprintf(write,"\n");
            }
        }

    }
    fclose(read);
        fclose(write);
    return 0;
}

