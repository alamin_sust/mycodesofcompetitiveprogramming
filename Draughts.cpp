/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int i,j,dp[10][10][1<<16],det[12][12][3],parity;
char arr[12][12];

int rec(int x,int y,int mask)
{
    // printf("%d %d\n",x,y);
    if(x<0||y<0||x>9||y>9)
        return 0;
    int &ret=dp[x][y/2][mask];
    ret=0;
    if((x+2)<10&&(y+2)<10&&det[x+1][y+1][parity]!=-1&&arr[x+1][y+1]=='B'&&arr[x+2][y+2]!='B'&&((i==(x+2)&&j==(y+2))||arr[x+2][y+2]!='W')&&((1<<det[x+1][y+1][parity])&mask)==0)
    {
        ret=max(ret,1+rec(x+2,y+2,(1<<det[x+1][y+1][parity])|mask));
    }
    if((x+2)<10&&(y-2)>=0&&det[x+1][y-1][parity]!=-1&&arr[x+1][y-1]=='B'&&arr[x+2][y-2]!='B'&&((i==(x+2)&&j==(y-2))||arr[x+2][y-2]!='W')&&((1<<det[x+1][y-1][parity])&mask)==0)
    {
        ret=max(ret,1+rec(x+2,y-2,(1<<det[x+1][y-1][parity])|mask));
    }
    if((x-2)>=0&&(y+2)<10&&det[x-1][y+1][parity]!=-1&&arr[x-1][y+1]=='B'&&arr[x-2][y+2]!='B'&&((i==(x-2)&&j==(y+2))||arr[x-2][y+2]!='W')&&((1<<det[x-1][y+1][parity])&mask)==0)
    {
        ret=max(ret,1+rec(x-2,y+2,(1<<det[x-1][y+1][parity])|mask));
    }
    if((x-2)>=0&&(y-2)>=0&&det[x-1][y-1][parity]!=-1&&arr[x-1][y-1]=='B'&&arr[x-2][y-2]!='B'&&((i==(x-2)&&j==(y-2))||arr[x-2][y-2]!='W')&&((1<<det[x-1][y-1][parity])&mask)==0)
    {
        ret=max(ret,1+rec(x-2,y-2,(1<<det[x-1][y-1][parity])|mask));
    }
    return ret;
}

int main()
{
    int t,p,k1,k2;
    cin>>t;
    for(p=1; p<=t; p++)
    {
        //memset(dp,-1,sizeof(dp));
        for(i=0; i<10; i++)
            scanf(" %s",&arr[i]);
        for(k1=0,k2=0,i=0; i<10; i++)
        {
            for(j=0; j<10; j++)
            {
                det[i][j][1]=det[i][j][2]=-1;
                if(i==0||j==0||i==9||j==9)
                    continue;
                if(arr[i][j]=='B')
                {

                    if((i%2)==1&&(j%2)==0)
                    {
                        det[i][j][1]=k1++;
                       // printf("%d -- %d\n",i,j);

                    }
                    if((i%2)==0&&(j%2)==1)
                    {
                        det[i][j][2]=k2++;
                        // printf("%d <> %d\n",i,j);

                    }
                }
            }
        }

        int ans=0;
        for(i=0; i<10; i++)
        {
            for(j=0; j<10; j++)
            {
                if(arr[i][j]=='W')
                {
                    int hh;
                    if((i%2)==0&&(j%2)==1)
                        hh=(1<<k1),parity=1;
                    else
                        hh=(1<<k2),parity=2;
                    for(int ii=0; ii<10; ii++)
                    {
                        for(int jj=0; jj<5; jj++)
                        {
                            for(int h=0; h<hh; h++)
                                dp[i][j][h]=-1;
                        }
                    }
                    ans=max(ans,rec(i,j,0));
                }
            }
        }

        printf("%d\n",ans);
    }
    return 0;
}
/*
2
.#.#.#.#.#
#.#.#.#.#.
.#.#.B.#.#
#.#.#.#.#.
.#.#.B.#.#
#.#.#.#.#.
.#.#.W.#.#
#.#.#.B.#.
.#.#.#.#.#
#.#.#.#.#.
.#.#.#.#.#
#.#.#.#.#.
.#.#.B.#.#
#.B.#.B.#.
.#.#.B.#.#
#.B.W.#.#.
.#.#.#.#.#
#.#.#.#.#.
.#.B.B.#.#
#.#.#.#.#.
*/
