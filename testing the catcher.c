#include <iostream>
#include <algorithm>
#include <vector>
#include <algorithm>
using namespace std;

int t,N,L,S[3000005],T;
vector <int> V;
bool flag;

int main(){
   while(scanf("%d",&t) == 1){
      if(t != -1) V.push_back(t);
      else{
         if(V.empty()) break;
         N = V.size();
         reverse(V.begin(),V.end());
         L = 0;
         for(int i=0;i<N;++i){
            if(!L){
               S[0] = V[i];
               L = 1;
            }
            else{
               t = upper_bound(S,S+L,V[i])-S;
               S[t] = V[i];
               L = max(t+1,L);
            }
         }
         printf("Test #%d:\n",++T);
         printf("  maximum possible interceptions: %d\n",L);
         V.clear();
      }
   }
}

