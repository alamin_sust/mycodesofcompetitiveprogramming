#include<stdio.h>
#include<math.h>

main()
{
    long long int i,n,in,det,mid,mx,x,y;
    scanf(" %lld",&n);
    for(i=1;i<=n;i++)
    {
        scanf(" %lld",&in);
        det=sqrt(in-1);
        mid=det*det+det+1;
        mx=det+mid;
        if(det%2==0)
        {
            if(in==mid)
            {
                y=det+1;
                x=det+1;
            }
            else if(in<mid)
            {
                x=det+1;
                y=in-(det*det);
            }
            else if(in>mid)
            {
                y=det+1;
                x=mx+1-in;
            }
        }
        else
        {
            if(in==mid)
            {
                y=det+1;
                x=det+1;
            }
            else if(in>mid)
            {
                x=det+1;
                y=mx+1-in;
            }
            else if(in<mid)
            {
                y=det+1;
                x=in-(det*det);
            }
        }
        printf("Case %lld: %lld %lld\n",i,x,y);
    }
    return 0;
}
