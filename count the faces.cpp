#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int k,n,m,i,a,b,par[60],faces;
map<char,int>mpp;
main()
{
    char c1,c2;
    while(cin>>n>>m)
    {
        faces=1;
        for(i=1;i<=n;i++)
            par[i]=i;
        mpp.clear();
        k=0;
        for(i=1;i<=m;i++)
        {
            scanf(" %c %c",&c1,&c2);
            if(mpp[c1]==0)
                mpp[c1]=++k;
            if(mpp[c2]==0)
                mpp[c2]=++k;
            a=mpp[c1];
            b=mpp[c2];
            while(par[a]!=a)
                a=par[a];
            while(par[b]!=b)
                b=par[b];
            if(a==b)
                faces++;
            else
                par[a]=b;
        }
        printf("%d\n",faces);
    }
    return 0;
}

