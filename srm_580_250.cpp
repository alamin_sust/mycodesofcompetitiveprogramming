#include <sstream>
#include<algorithm>
#include<string.h>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class ShoutterDiv2
{
public:
	int count(vector <int> s, vector <int> t)
	{
	    int j,k,res=0,i,from[110],to[110],a,flag[110][110];
	    memset(flag,0,sizeof(flag));
	    for(i=1;s.empty()==0;i++)
        {
            a=s.back();
            s.pop_back();
            from[i]=a;
            //printf("%d\n",from[i]);
        }
        for(i=1;t.empty()==0;i++)
        {
            a=t.back();
            t.pop_back();
            to[i]=a;
            //printf("%d\n",to[i]);
        }
        k=i;
        for(i=1;i<k;i++)
        {
            for(j=1;j<k;j++)
            {
                if(i!=j)
                {
                    if(from[i]<=to[j]&&from[j]<=from[i])
                        if(flag[i][j]==0)
                        {res++;
                        flag[i][j]=flag[j][i]=1;}
                }
            }
        }
	    return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	ShoutterDiv2 objectShoutterDiv2;

	//test case0
	vector <int> param00;
	param00.push_back(1);
	param00.push_back(2);
	param00.push_back(4);
	vector <int> param01;
	param01.push_back(3);
	param01.push_back(4);
	param01.push_back(6);
	int ret0 = objectShoutterDiv2.count(param00,param01);
	int need0 = 2;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(0);
	vector <int> param11;
	param11.push_back(100);
	int ret1 = objectShoutterDiv2.count(param10,param11);
	int need1 = 0;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(0);
	param20.push_back(0);
	param20.push_back(0);
	vector <int> param21;
	param21.push_back(1);
	param21.push_back(1);
	param21.push_back(1);
	int ret2 = objectShoutterDiv2.count(param20,param21);
	int need2 = 3;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(9);
	param30.push_back(26);
	param30.push_back(8);
	param30.push_back(35);
	param30.push_back(3);
	param30.push_back(58);
	param30.push_back(91);
	param30.push_back(24);
	param30.push_back(10);
	param30.push_back(26);
	param30.push_back(22);
	param30.push_back(18);
	param30.push_back(15);
	param30.push_back(12);
	param30.push_back(15);
	param30.push_back(27);
	param30.push_back(15);
	param30.push_back(60);
	param30.push_back(76);
	param30.push_back(19);
	param30.push_back(12);
	param30.push_back(16);
	param30.push_back(37);
	param30.push_back(35);
	param30.push_back(25);
	param30.push_back(4);
	param30.push_back(22);
	param30.push_back(47);
	param30.push_back(65);
	param30.push_back(3);
	param30.push_back(2);
	param30.push_back(23);
	param30.push_back(26);
	param30.push_back(33);
	param30.push_back(7);
	param30.push_back(11);
	param30.push_back(34);
	param30.push_back(74);
	param30.push_back(67);
	param30.push_back(32);
	param30.push_back(15);
	param30.push_back(45);
	param30.push_back(20);
	param30.push_back(53);
	param30.push_back(60);
	param30.push_back(25);
	param30.push_back(74);
	param30.push_back(13);
	param30.push_back(44);
	param30.push_back(51);
	vector <int> param31;
	param31.push_back(26);
	param31.push_back(62);
	param31.push_back(80);
	param31.push_back(80);
	param31.push_back(52);
	param31.push_back(83);
	param31.push_back(100);
	param31.push_back(71);
	param31.push_back(20);
	param31.push_back(73);
	param31.push_back(23);
	param31.push_back(32);
	param31.push_back(80);
	param31.push_back(37);
	param31.push_back(34);
	param31.push_back(55);
	param31.push_back(51);
	param31.push_back(86);
	param31.push_back(97);
	param31.push_back(89);
	param31.push_back(17);
	param31.push_back(81);
	param31.push_back(74);
	param31.push_back(94);
	param31.push_back(79);
	param31.push_back(85);
	param31.push_back(77);
	param31.push_back(97);
	param31.push_back(87);
	param31.push_back(8);
	param31.push_back(70);
	param31.push_back(46);
	param31.push_back(58);
	param31.push_back(70);
	param31.push_back(97);
	param31.push_back(35);
	param31.push_back(80);
	param31.push_back(76);
	param31.push_back(82);
	param31.push_back(80);
	param31.push_back(19);
	param31.push_back(56);
	param31.push_back(65);
	param31.push_back(62);
	param31.push_back(80);
	param31.push_back(49);
	param31.push_back(79);
	param31.push_back(28);
	param31.push_back(75);
	param31.push_back(78);
	int ret3 = objectShoutterDiv2.count(param30,param31);
	int need3 = 830;
	assert_eq(3,ret3,need3);

}
