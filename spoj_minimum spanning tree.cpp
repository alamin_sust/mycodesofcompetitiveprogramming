#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define eps 0.00000001
using namespace std;

struct info
{
    int from,to,weight;
};

info MST[100010];

int comp(const void *p1,const void *p2)
{
    info *y1=(info*)p1;
    info *y2=(info*)p2;
    return (y1->weight-y2->weight);
}

main()
{
    int n,e,i,a,b,x,y,k,parent[10010];
    long long int res;
    scanf(" %d %d",&n,&e);
    for(i=0;i<e;i++)
    {
        scanf(" %d %d %d",&MST[i].from,&MST[i].to,&MST[i].weight);
    }
    qsort(MST,e,sizeof(info),comp);
    for(i=1;i<=n;i++)
    {
        parent[i]=i;
    }
    for(k=0,res=0,i=0;k<n-1&&i<e;i++)
    {
        a=MST[i].from;
        b=MST[i].to;
        while(1)
        {
            if(parent[a]==a)
            {
                x=a;
                break;
            }
            a=parent[a];
        }
        while(1)
        {
            if(parent[b]==b)
            {
                y=b;
                break;
            }
            b=parent[b];
        }
        if(x!=y)
            {parent[y]=x;
            k++;
            res+=MST[i].weight;}
    }
    printf("%lld\n",res);
    return 0;
}

