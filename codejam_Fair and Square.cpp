#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#define pi 2*acos(0)
#define inf INT_MAX
#define eps 0.00000001
using namespace std;

main()
{
    int i,res,from,to,t;
    FILE *read,*write;
    read=fopen("codejam2in.txt","r");
    write=fopen("codejam2out.txt","w");
    fscanf(read," %d",&t);
    for(i=1;i<=t;i++)
    {
        fscanf(read," %d %d",&from,&to);
        res=0;
        if(from<=1&&to>=1)
        res++;
        if(from<=4&&to>=4)
        res++;
        if(from<=9&&to>=9)
        res++;
        if(from<=121&&to>=121)
        res++;
        if(from<=484&&to>=484)
        res++;
        fprintf(write,"Case #%d: %d\n",i,res);
    }
    fclose(read);
    fclose(write);
    return 0;
}
