/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.0000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll xx,yy,hits;
double x,y;

ll func(double tym)
{
    double hitx=tym*x+eps;
    double hity=tym*y+eps;
    xx=(ll)hitx;
    yy=(ll)hity;
    if((xx+yy)<hits)
        return 0LL;
    hitx=floor(hitx)/x;
    hity=floor(hity)/y;
    if((hitx+eps)>hity&&(hity+eps)>hitx)
    {
        xx=1LL;
        yy=1LL;
    }
    else if((hitx+eps)<hity)
    {
        xx=0LL;
        yy=1LL;
    }
    else
    {
        xx=1LL;
        yy=0LL;
    }
    return 1LL;
}

ll t,p;

int main()
{
    double low,mid,high,res;
    ll resx,resy;
    cin>>t>>x>>y;
    for(p=1;p<=t;p++)
    {
        scanf(" %I64d",&hits);
        low=0.0;
        high=1000000010.0;
        res=high;
        while((low+eps)<high)
        {
            mid=(low+high)/2.0;
            if(func(mid))
            {
                if(res>mid)
                {res=mid;
                resx=xx;
                resy=yy;
                }
                high=mid;
            }
            else
                low=mid;
        }
        if(resx&&resy)
        printf("Both\n");
        else if(resx)
        printf("Vanya\n");
        else
        printf("Vova\n");
    }
    return 0;
}
