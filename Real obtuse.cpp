/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;
int t,p,k,a,b,diff;
int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++) {

        scanf(" %d %d %d",&k,&a,&b);

        diff=abs(a-b);

        diff=min(k-diff,diff);

        if((diff*2)==k) printf("0\n");
        else printf("%d\n",diff-1);

    }


    return 0;
}

