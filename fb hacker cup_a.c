#include<stdio.h>
#include<string.h>

main()
{
    FILE *read1,*write1;
    char arr[1010],ch;
    int n,i,j,k,l,det[110],res,temp;
    read1=fopen("beautiful_stringstxt.txt","r");
    write1=fopen("beautiful_stringstxt_output.txt","w");
    fscanf(read1," %d%c",&n,&ch);
    for(i=0;i<n;i++)
    {
        fscanf(read1," %[^\n]",arr);
        l=strlen(arr);
        for(j=1;j<=26;j++)
            det[j]=0;
        for(j=0;j<l;j++)
        {
            arr[j]=toupper(arr[j]);
            det[arr[j]-64]++;
        }
        for(j=1;j<=26;j++)
        {
            for(k=2;k<=26;k++)
            {
                if(det[k]>det[k-1])
                {
                    temp=det[k];
                    det[k]=det[k-1];
                    det[k-1]=temp;
                }
            }
        }
        for(k=26,res=0,j=1;j<=26;j++)
        {
            if(det[j]==0)
                break;
            res+=det[j]*k;
            k--;
        }
        fprintf(write1,"Case #%d: %d\n",i+1,res);
    }
    return 0;
}
