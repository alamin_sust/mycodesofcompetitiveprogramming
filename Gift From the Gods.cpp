/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int arr[15];
int sieve[1000010];

void sieve_(void)
{
    sieve[0]=sieve[1]=1;
    for(int i=2;i<=1000005;i++)
    {
        if(sieve[i]==0)
        {
            for(int j=2;i*j<=1000005;j++)
                sieve[i*j]=1;
        }
    }
    return;
}

bool ispal(int x)
{
    int k,i,j;
    for(k=0;x>0;k++)
    {
        arr[k]=x%10;
        x/=10;
    }
    for(i=0,j=k-1;i<=j;i++,j--)
    {
        if(arr[i]!=arr[j])
            return false;
    }
    return true;
}

int main()
{
    int flag,in;
    sieve_();
    flag=0;
    while(scanf("%d",&in)!=EOF)
    {
        if(flag==0)
        printf("%d\n",in*2);
        if(sieve[in]==0&&ispal(in))
            flag=1;
    }
    return 0;
}


