/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int value[5010][5010],arr[5010],n,m,q,i,res,j,mx,tp,x,y;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d %d %d",&n,&m,&q);

    for(i=1;i<=n;i++)
    {
        for(j=1;j<=m;j++)
        {
            scanf(" %d",&value[i][j]);
        }
        tp=0;
        arr[i]=0;
        for(j=1;j<=m;j++)
        {
            if(value[i][j]==1)
            {
                tp++;
                arr[i]=max(arr[i],tp);
            }
            else
            {
                tp=0;
            }
        }
    }

    for(i=1;i<=q;i++)
    {
        scanf(" %d %d",&x,&y);
        if(value[x][y]==0)
        {
            value[x][y]=1;
        }
        else
        {
            value[x][y]=0;
        }
        tp=0;
        arr[x]=0;
        for(j=1;j<=m;j++)
        {
            if(value[x][j]==1)
            {
                tp++;
                arr[x]=max(arr[x],tp);
            }
            else
            {
                tp=0;
            }
        }
        res=0;
        mx=-1;
        for(j=1;j<=n;j++)
        {
            //printf("%d.",arr[j]);
            if(arr[j]>mx)
            {
                mx=arr[j];
                res=j;
            }
        }
        printf("%d\n",mx);
    }
    return 0;
}


