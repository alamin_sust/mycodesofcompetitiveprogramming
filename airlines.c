#include<stdio.h>
#include<math.h>
#define infinity 99999999.0

main()
{
    long long int p=1,i,j,k,n,q,e,x,y;
    double R=6378.0,pi=3.141592653589793,lat[150],lon[150],adj[150][150],dist;
    while(1)
    {
        char name[150][110],ch;
        scanf(" %lld %lld %lld%c",&n,&e,&q,&ch);
        if(n==0&&e==0&&q==0)
        break;
        if(p!=1)
        printf("\n");
        for(i=1;i<=n;i++)
        {
            scanf(" %s %lf %lf%c",&name[i],&lat[i],&lon[i],&ch);
            lat[i]=(lat[i]*pi)/180.0;
            lon[i]=(lon[i]*pi)/180.0;
            for(j=1;j<=n;j++)
            adj[i][j]=infinity;
        }
        for(i=1;i<=e;i++)
        {
            char in1[110],in2[110];
            scanf(" %s %s%c",in1,in2,&ch);
            for(j=1;j<=n;j++)
            {if(strcmp(in1,name[j])==0)
            {x=j;
            break;}}
            for(j=1;j<=n;j++)
            {if(strcmp(in2,name[j])==0)
            {y=j;
            break;}}

            dist=acos(cos(lat[x])*cos(lat[y])*cos(lon[x]-lon[y])+sin(lat[x])*sin(lat[y]))*R;
            adj[x][y]=dist;
        }

        for(i=1;i<=n;i++)
            {
                for(j=1;j<=n;j++)
                {
                    adj[i][j]=floor(adj[i][j]+0.5);
                }
            }

        for(k=1;k<=n;k++)
        {
            for(i=1;i<=n;i++)
            {
                for(j=1;j<=n;j++)
                {
                    if(adj[i][j]>(adj[i][k]+adj[k][j]))
                    {
                        adj[i][j]=adj[i][k]+adj[k][j];
                    }
                }
            }
        }
        printf("Case #%lld\n",p);
        for(i=1;i<=q;i++)
        {
            char in1[110],in2[110];
            scanf(" %s %s%c",in1,in2,&ch);
            for(j=1;j<=n;j++)
            {if(strcmp(in1,name[j])==0)
            {x=j;
            break;}}
            for(j=1;j<=n;j++)
            {if(strcmp(in2,name[j])==0)
            {y=j;
            break;}}
            if(adj[x][y]==infinity)
            printf("no route exists\n");
            else
            printf("%.0lf km\n",adj[x][y]);
        }
        p++;
    }
    return 0;
}
