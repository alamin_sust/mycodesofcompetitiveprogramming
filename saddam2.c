#include<stdio.h>
#include<math.h>
long long int reverse(long long int num);
long long int palindrome(long long int val);
int main()
{
    long long int N,count,i;
    long long val,originVal,sum;
    while(scanf("%lld",&N)==1LL)
    {
        for(i=0LL; i<N; i++)
        {
            count=0LL;
            scanf("%lld",&originVal);
            while(!palindrome(originVal))
            {
                originVal+=reverse(originVal);
                count++;
            }
            printf("%lld %lld\n",count,originVal);
        }
    }
    return 0;
}

long long int reverse(long long int num)
{
    long long int rev=0LL;
    while(num>0LL)
    {
        rev=rev*10LL;
        rev=rev+num%10LL;
        num=num/10LL;
    }
    return rev;
}

long long int palindrome(long long int val)
{
    if(val==reverse(val))   return 1LL;
    else    return 0LL;
}
