#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 9999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int dp[3][1<<9+5],n,charmask[82],s[82];

int rec(int pos,int mask)
{
    if(pos==3)
        return 0;
    int &ret=dp[pos][mask];
    if(ret!=-1)
        return ret;
    ret=-inf;
    for(int i=0;i<n;i++)
    {
        if((mask&charmask[i])==0)
        {
            ret=max(ret,s[i]+rec(pos+1,mask|charmask[i]));
        }
    }
    return ret;
}

main()
{
    int i,res,a,b,c,p=1;
    while(cin>>n&&n)
    {
        for(i=0;i<n;i++)
        {
        scanf(" %d %d %d %d",&a,&b,&c,&s[i]);
        charmask[i]=(1<<(a-1));
        charmask[i]=(1<<(b-1))|charmask[i];
        charmask[i]=(1<<(c-1))|charmask[i];
        //printf("%d\n",charmask[i]);
        }
        memset(dp,-1,sizeof(dp));
        res=rec(0,0);
        if(res>=0)
        printf("Case %d: %d\n",p++,res);
        else
        printf("Case %d: -1\n",p++);
    }
    return 0;
}

