/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

double arr[25][25],res;
int r,c;

void rec(int nr,int nc,int baki,vector<pair<int,int> > v,double val,vector<double>v2)
{
    if(val>res)
        res=val;
    //printf("%d %d %lf\n",nr,nc,val);
    if(baki==0)
        return;
    double ret=0.0;
    if((nr+1)<=r)
    {
        vector<pair<int,int> >nv;
        nv.clear();
        nv=v;
        vector<double >nv2;
        nv2.clear();
        nv2=v2;
        int flag=0;
        double pv=1.0;
        for(int i=0; i<v.size(); i++)
        {
            if(v[i].first==(nr+1)&&v[i].second==(nc))
            {
                flag=1;
                pv=nv2[i]*(1.0-arr[nr+1][nc]);
                nv2[i]=pv;
                break;
            }
        }
        if(flag)
            rec(nr+1,nc,baki-1,nv,val+arr[nr+1][nc]*pv,nv2);
        else
        {
            nv2.push_back(1.0);
            nv.push_back(make_pair(nr+1,nc));
            rec(nr+1,nc,baki-1,nv,val+arr[nr+1][nc],nv2);
        }
    }
    if((nr-1)>=1)
    {
        vector<pair<int,int> >nv;
        nv.clear();
        nv=v;
        vector<double >nv2;
        nv2.clear();
        nv2=v2;
        int flag=0;
        double pv=1.0;
        for(int i=0; i<v.size(); i++)
        {
            if(v[i].first==(nr-1)&&v[i].second==(nc))
            {
                flag=1;
                pv=nv2[i]*(1.0-arr[nr-1][nc]);
                nv2[i]=pv;
                break;
            }
        }
        if(flag)
            rec(nr-1,nc,baki-1,nv,val+arr[nr-1][nc]*pv,nv2);
        else
        {
            nv2.push_back(1.0);
            nv.push_back(make_pair(nr-1,nc));
            rec(nr-1,nc,baki-1,nv,val+arr[nr-1][nc],nv2);
        }
    }
    if((nc+1)<=c)
    {
        vector<pair<int,int> >nv;
        nv.clear();
        nv=v;
        vector<double >nv2;
        nv2.clear();
        nv2=v2;
        int flag=0;
        double pv=1.0;
        for(int i=0; i<v.size(); i++)
        {
            if(v[i].first==(nr)&&v[i].second==(nc+1))
            {
                flag=1;
                pv=nv2[i]*(1.0-arr[nr][nc+1]);
                nv2[i]=pv;
                break;
            }
        }
        if(flag)
            rec(nr,nc+1,baki-1,nv,val+arr[nr][nc+1]*pv,nv2);
        else
        {
            nv2.push_back(1.0);
            nv.push_back(make_pair(nr,nc+1));
            rec(nr,nc+1,baki-1,nv,val+arr[nr][nc+1],nv2);
        }
    }
    if((c-1)>=1)
    {
        vector<pair<int,int> >nv;
        nv.clear();
        nv=v;
        vector<double >nv2;
        nv2.clear();
        nv2=v2;
        int flag=0;
        double pv=1.0;
        for(int i=0; i<v.size(); i++)
        {
            if(v[i].first==(nr)&&v[i].second==(nc-1))
            {
                flag=1;
                pv=nv2[i]*(1.0-arr[nr][nc-1]);
                nv2[i]=pv;
                break;
            }
        }
        if(flag)
            rec(nr,nc-1,baki-1,nv,val+arr[nr][nc-1]*pv,nv2);
        else
        {
            nv2.push_back(1.0);
            nv.push_back(make_pair(nr,nc-1));
            rec(nr,nc-1,baki-1,nv,val+arr[nr][nc-1],nv2);
        }
    }
    return;
}

int main()
{
    freopen("A-largeC.in","r",stdin);
    freopen("A-largeCout.txt","w",stdout);

    int t,p,sc,sr,s,i,j;
    double a,b;
    char ch;

    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {
        scanf(" %d %d %d %d %d",&r,&c,&sr,&sc,&s);
        sr++;
        sc++;
        scanf(" %lf %lf",&a,&b);
        for(i=1; i<=r; i++)
        {
            for(j=1; j<=c; j++)
            {
                scanf(" %c",&ch);
                if(ch=='A')
                {
                    arr[i][j]=a;
                }
                else
                {
                    arr[i][j]=b;
                }
            }
        }
        vector<pair<int,int> >vec;
        vector<double>vec2;
        vec.clear();
        vec2.clear();
        res=0.0;
        rec(sr,sc,s,vec,res,vec2);
        printf("Case #%d: %.8lf\n",p,res);
    }

    return 0;
}






