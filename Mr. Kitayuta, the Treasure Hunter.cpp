/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#pragma comment(linker, �/STACK:801000�)
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll dp[30010][610],d,gem[30010];

ll rec(ll pos,ll laf)
{
    if(laf<=0)
        return 0LL;
    if(pos>30000)
        return 0LL;
    ll &ret=dp[pos][d-laf+300];
    if(ret!=-1)
        return ret;
    ret=0LL;
    ret=gem[pos]+max(max(rec(pos+laf-1,laf-1),rec(pos+laf,laf)),rec(pos+laf+1,laf+1));
    return ret;
}

int main()
{
    ll n,i,in;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %I64d %I64d",&n,&d);

    for(i=0LL;i<n;i++)
    {
        scanf(" %d",&in);
        gem[in]++;
    }
    memset(dp,-1,sizeof(dp));
    printf("%I64d\n",rec(d,d));
    return 0;
}




