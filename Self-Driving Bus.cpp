/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

vector<int>adj[10010];
int k,cnt,flag[10010];
queue<int>q;

int fc(int from,int to)
{
    while(!q.empty())q.pop();
    int cnt=1;
    q.push(from);
    flag[from]=k;
    while(!q.empty())
    {
        int u=q.front();
        q.pop();
        for(int i=0;i<adj[u].size();i++)
        {
            int v=adj[u][i];
            if(v>=from&&v<=to&&flag[v]!=k)
            {
                flag[v]=k;
                cnt++;
                q.push(v);
            }
        }
    }
    if(cnt==(to-from+1))
        return 1;
    else
        return 0;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    int i,j,n,from,to,res;

    scanf(" %d",&n);
    res=0;
    for(i=1;i<n;i++)
    {
        scanf(" %d %d",&from,&to);
        if(abs(from-to)==1)
            res++;
        adj[from].push_back(to);
        adj[to].push_back(from);
    }

    k=1;
    for(i=1;i<=n;i++)
    {
        for(j=i+2;j<=n;j++)
        {
            if(fc(i,j))
                res++;
            k++;
        }
    }

    printf("%d\n",res+n);

    return 0;
}

