#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class Multiples
{
public:
	int number(int min, int max, int factor)
	{
	    int res,i;
	    for(res=0,i=min;i<=max;i++)
	    {
	        if(i%factor==0)
	        res++;
	    }
	    return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	Multiples objectMultiples;

	//test case0
	int param00 = 0;
	int param01 = 14;
	int param02 = 5;
	int ret0 = objectMultiples.number(param00,param01,param02);
	int need0 = 3;
	assert_eq(0,ret0,need0);

	//test case1
	int param10 = 7;
	int param11 = 24;
	int param12 = 3;
	int ret1 = objectMultiples.number(param10,param11,param12);
	int need1 = 6;
	assert_eq(1,ret1,need1);

	//test case2
	int param20 = -123456;
	int param21 = 654321;
	int param22 = 997;
	int ret2 = objectMultiples.number(param20,param21,param22);
	int need2 = 780;
	assert_eq(2,ret2,need2);

	//test case3
	int param30 = -75312;
	int param31 = 407891;
	int param32 = 14;
	int ret3 = objectMultiples.number(param30,param31,param32);
	int need3 = 34515;
	assert_eq(3,ret3,need3);

}
