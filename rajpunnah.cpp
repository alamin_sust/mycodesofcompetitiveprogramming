#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll f,t,from,to,i,res;
vector<ll>nums;

void init(ll num,ll lev)
{
    nums.push_back(num);
    if(lev<11)
    {
        init(num*10+4,lev+1);
        init(num*10+7,lev+1);
    }
    return;
}

main()
{
    init(0,0);
    sort(nums.begin(),nums.end());
    cin>>f>>t;
    for(i=0;(i+1)<nums.size();i++)
    {
        from=nums[i]+1;
        to=nums[i+1];
        from=max(from,f);
        to=min(to,t);
        if(from<=to)
        {
            res+=nums[i+1]*(to-from+1);
        }
    }
    cout<<res<<endl;
    return 0;
}
