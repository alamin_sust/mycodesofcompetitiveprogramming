#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int v[1000010],seg[1000010],n,det,val,i,p=0,from,to,arr[1000010];
char in[110];

int build(int ind,int f,int t)
{
    if(f==t)
    {v[f]=ind;
        return seg[ind]=arr[f];}
    int mid=(f+t)/2;
    seg[ind]=build(ind*2,f,mid)+build(ind*2+1,mid+1,t);
    return seg[ind];
}

void update(int ind)
{
    while(ind)
    {
        seg[ind]=seg[ind*2]+seg[ind*2+1];
        ind/=2;
    }
    return;
}

int query(int ind,int f,int t,int nf,int nt)
{
    int ret1=0,ret2=0;
    if(nf>=f&&nt<=t)
        {printf(" %d\n",ind);
        return seg[ind];}
    int mid=(nf+nt)/2;
    if(mid>=f)
        ret1=query(ind*2,f,t,nf,mid);
    if(t>mid)
        ret2=query(ind*2+1,f,t,mid+1,nt);
    return ret1+ret2;
}

main()
{
    while(scanf("%d",&n)==1&&n)
    {
        memset(seg,0,sizeof(seg));
        for(i=1;i<=n;i++)
            scanf(" %d",&arr[i]);
        build(1,1,n);
        //for(i=1;i<10;i++)
          //  printf("%d ",seg[i]);
          if(p)
            printf("\n");
          printf("Case %d:\n",++p);
        while(scanf("%s",in)!=EOF)
        {
            if(strcmp(in,"END")==0)
                break;
            if(in[0]=='S')
            {
                scanf(" %d %d",&det,&val);
                seg[v[det]]=val;
                update(v[det]/2);
           // for(i=1;i<10;i++)
           //printf("%d ",seg[i]);
           //printf("\n");
            }
            else
            {
                scanf(" %d %d",&from,&to);
                printf("%d\n",query(1,from,to,1,n));
            }
        }
    }
    return 0;
}

