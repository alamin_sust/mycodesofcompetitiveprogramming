/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll dp[20][20][2][2][20][2][2],n,m,k;
vector<ll>N,K;

ll rec(ll pos,ll dig,ll flag,ll is_less,ll comp_ind,ll started,ll flag2)
{
    if(pos==N.size())
    {
        return is_less;
    }
    ll &ret=dp[pos][dig][flag][is_less][comp_ind][started][flag2];
    if(ret!=-1)
        return ret;
    ret=0;
    int t_started,t_flag2,t_comp_ind,t_is_less;
    if(flag==0)
    {
        t_started=started;
        t_comp_ind=comp_ind;
        t_is_less=is_less;
        t_flag2=flag2;
        if(N[pos]>0)
            t_started=1;
        if(t_started==1)
            t_comp_ind=comp_ind+1;
        else t_comp_ind=0;
        if(t_comp_ind)
        {
            if(comp_ind>=K.size()||K[comp_ind]<N[pos])
                t_is_less=0;
            if(comp_ind<K.size()&&K[comp_ind]>N[pos])
                t_flag2=1;
        }
        ret+=rec(pos+1,N[pos],0,t_is_less&t_flag2,t_comp_ind,t_started,t_flag2);
        for(ll i=0; i<N[pos]; i++)
        {
            t_started=started;
            t_comp_ind=comp_ind;
            t_is_less=is_less;
            t_flag2=flag2;
            if(i>0)
                t_started=1;
            if(t_started==1)
                t_comp_ind=comp_ind+1;
            else t_comp_ind=0;
            if(t_comp_ind)
            {
                if(comp_ind>=K.size()||K[comp_ind]<i)
                    t_is_less=0;
                if(comp_ind<K.size()&&K[comp_ind]>i)
                    t_flag2=1;
            }
            ret+=rec(pos+1,i,1,t_is_less&t_flag2,t_comp_ind,t_started,t_flag2);
        }
    }
    else
    {
        for(ll i=0; i<=9; i++)
        {
            t_started=started;
            t_comp_ind=comp_ind;
            t_is_less=is_less;
            t_flag2=flag2;
            if(i>0)
                t_started=1;
            if(t_started==1)
                t_comp_ind=comp_ind+1;
            else t_comp_ind=0;
            if(t_comp_ind)
            {
                if(comp_ind>=K.size()||K[comp_ind]<i)
                    t_is_less=0;
                if(comp_ind<K.size()&&K[comp_ind]>i)
                    t_flag2=1;
            }
            ret+=rec(pos+1,i,1,t_is_less&t_flag2,t_comp_ind,t_started,t_flag2);
        }
    }
    return ret;
}

main()
{
    ll i,res,now,high,tpmid,low,mid;
    while(cin>>k>>m)
    {
        K.clear();
        while(k>0)
            K.push_back(k%10),k/=10;
        reverse(K.begin(),K.end());
        n=18;
        while(n>0)
            N.push_back(n%10),n/=10;
        reverse(N.begin(),N.end());
        memset(dp,-1,sizeof(dp));
        printf("%lld...\n",rec(0,0,0,0,0,0,0));

        high=1000000000000000010LL;
        low=1LL;
        res=high;
        while(high>=low)
        {
            //printf("%lld\n",mid);
            mid=(high+low)/2LL;
            N.clear();
            tpmid=mid;
            while(tpmid)
                N.push_back(tpmid%10),tpmid/=10;
            reverse(N.begin(),N.end());
            //printf("%lld..%lld\n",N[0],N[1]);
            memset(dp,-1,sizeof(dp));
            now=rec(0,0,0,1,0,0,0);
            if(now==m)
                res=min(res,mid);
            if(now>=m)
                high=mid-1;
            else
                low=mid+1;
        }
        printf("%lld\n",res);
    }
    return 0;
}
