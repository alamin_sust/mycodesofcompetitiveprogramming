#include<stdio.h>
#include<string.h>
#include<math.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define dist(x1,y1,x2,y2) sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) )
using namespace std;

int arr[500010],cube[1010],pyr[1010];

void precal(void)
{
    int i,j,k,l;
    //printf("..");
    for(i=0; i<=75; i++)
    {
            for(j=0; j<=110; j++)
            {
                if((cube[i]+pyr[j])<=400000)
                    arr[cube[i]+pyr[j]]=min(arr[cube[i]+pyr[j]],(i!=0)+(j!=0));
                else
                    break;
            }
    }

    for(i=0; i<=75; i++)
    {
        {
            for(j=0; j<=110; j++)
            {
                for(k=0; k<=110; k++)
                {
                    if((cube[i]+pyr[j]+pyr[k])<=400000)
                        arr[cube[i]+pyr[j]+pyr[k]]=min(arr[cube[i]+pyr[j]+pyr[k]],(i!=0)+(j!=0)+(k!=0));
                    else
                        break;
                }
            }
        }
    }

    for(i=0; i<=75; i++)
    {
        {
            for(j=0; j<=75; j++)
            {
                for(k=0; k<=110; k++)
                {
                    if((cube[i]+cube[j]+pyr[k])<=400000)
                        arr[cube[i]+cube[j]+pyr[k]]=min(arr[cube[i]+cube[j]+pyr[k]],(i!=0)+(j!=0)+(k!=0));
                    else
                        break;
                }
            }
        }
    }


    for(i=0; i<=75; i++)
    {
            for(j=0; j<=75; j++)
            {
                for(k=0; k<=75; k++)
                {
                    if((cube[i]+cube[j]+cube[k])<=400000)
                        arr[cube[i]+cube[j]+cube[k]]=min(arr[cube[i]+cube[j]+cube[k]],(i!=0)+(j!=0)+(k!=0));
                    else
                        break;
                }
            }

    }

    for(i=0; i<=100; i++)
    {
            for(j=0; j<=100; j++)
            {
                for(k=0; k<=110; k++)
                {
                    if((pyr[i]+pyr[j]+pyr[k])<=400000)
                        arr[pyr[i]+pyr[j]+pyr[k]]=min(arr[pyr[i]+pyr[j]+pyr[k]],(i!=0)+(j!=0)+(k!=0));
                    else
                        break;
                }
            }
    }

    return;
}

int main()
{
    int i,in;
    for(i=0; i<=400000; i++)
    {
        arr[i]=4;
    }
    arr[0]=0;
    for(i=1; i<=75; i++)
    {
        cube[i]=(i*i*i);
        arr[cube[i]]=1;
    }
    for(i=1; i<=110; i++)
    {
        pyr[i]=(i*(i+1)*(2*i+1)/6);
        arr[pyr[i]]=1;
    }
    precal();
   // for(i=0; i<=400000; i++)
   //   {
   //     if(arr[i]>10)
    //        printf("no");
    //  }
    while(1)
    {
        scanf(" %d",&in);
        if(in==-1)
            break;
        printf("%d\n",arr[in]);
    }
    return 0;
}
