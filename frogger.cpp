#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll n,i,j,k,p=1;
double x[210],y[210],adj[210][210];

main()
{
    while(scanf("%lld",&n)==1&&n)
    {
        for(i=1;i<=n;i++)
        {
            scanf(" %lf %lf",&x[i],&y[i]);
        }
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
            {
                adj[i][j]=dist(x[i],y[i],x[j],y[j]);
            }
        }
        for(k=1;k<=n;k++)
        {
            for(i=1;i<=n;i++)
            {
                for(j=1;j<=n;j++)
                {
                    if(adj[i][j]>max(adj[i][k],adj[k][j]))
                        adj[i][j]=max(adj[i][k],adj[k][j]);
                }
            }
        }
        printf("Scenario #%lld\n",p++);
        printf("Frog Distance = %.3lf\n\n",adj[1][2]);
    }
    return 0;
}

