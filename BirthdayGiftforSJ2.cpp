/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll cnt,k,arr[110],a,b,dp[35][1010],cum[1010],res;
map<ll,ll>mpp;

void rec(ll pos,ll val)
{
    if(pos==cnt)
        return;
    ll &ret=dp[pos][val];
    if(ret!=-1LL)
        return;
    mpp[val]=1LL;
    ret=0LL;
    for(ll i=0LL;(i+val)<=1000LL;i+=arr[pos])
    {
        rec(pos+1LL,val+i);
    }
    return;
}

int main()
{
    ll p,i,j,t;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    cnt=0LL;
    for(i=2LL;i<=1000LL;i++)
    {
        for(j=2LL,k=i*i;k<=1000LL&&j<=1000LL;j++)
        {
            if(mpp[k]==0LL)
            mpp[k]=1LL,arr[cnt++]=k;
        }
    }
   // printf("%d\n",cnt);

    memset(dp,-1LL,sizeof(dp));
    rec(0LL,0LL);
    mpp[0]=0LL;
    for(i=1LL;i<=1000LL;i++)
    {
        cum[i]=cum[i-1]+mpp[i];
        //if(mpp[i]==0)
       // printf("%d ",i);
    }
    scanf(" %lld",&t);
    for(p=1LL;p<=t;p++)
    {
        res=0LL;
       scanf(" %lld %lld",&a,&b);
       if(b>900LL)
       {
           res+=(b-900LL);
           b-=res;
       }
       res+=(cum[b]-cum[a-1]);
       printf("%lld\n",res);
    }
    return 0;
}
