/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int rr[]= {0,0,1,-1};
int cc[]= {-1,1,0,0};

char arr[1010][1010];
int n,m,stat[1010][1010],val[1010][1010],stat2[5][1010][1010],val2[5][1010][1010];
struct node
{
    int x,y;
};
node det;
queue<node>q;

int bfs3(char a,char b,char c)
{
    // printf("%c %c %c\n",a,b,c);
    while(!q.empty())q.pop();
    memset(stat,0,sizeof(stat));
    memset(val,0,sizeof(val));
    for(int i=1; i<=n; i++)
    {
        for(int j=1; j<=m; j++)
        {
            if(arr[i][j]==a||arr[i][j]==b)
            {
                stat[i][j]=1;
                det.x=i;
                det.y=j;
                q.push(det);
            }
        }
    }
    int ret=0;
    while(!q.empty())
    {
        node u=q.front();
        q.pop();
        for(int i=0; i<4; i++)
        {
            node v;
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            if(v.x>=1&&v.x<=n&&v.y>=1&&v.y<=m&&stat[v.x][v.y]==0&&arr[v.x][v.y]!='#')
            {
                val[v.x][v.y]=val[u.x][u.y]+1;
                if(arr[v.x][v.y]==c)
                {
                    return val[v.x][v.y];
                }
                stat[v.x][v.y]=1;
                q.push(v);
            }
        }

    }
    return 1000000010;
}

void bfs2(void)
{
    memset(stat2,0,sizeof(stat2));
    memset(val2,0,sizeof(val2));
    int res=1000000010;
    for(int k='1'; k<='3'; k++)
    {
        while(!q.empty())
            q.pop();
        for(int i=1; i<=n; i++)
        {
            for(int j=1; j<=m; j++)
            {
                if(arr[i][j]==k)
                {
                    det.x=i;
                    det.y=j;
                    q.push(det);
                    stat2[k-'0'][i][j]=1;
                }
            }
        }
        while(!q.empty())
        {
            node u=q.front();
            q.pop();
            for(int i=0; i<4; i++)
            {
                node v;
                v.x=u.x+rr[i];
                v.y=u.y+cc[i];
                if(v.x>=1&&v.x<=n&&v.y>=1&&v.y<=m&&stat2[k-'0'][v.x][v.y]==0&&arr[v.x][v.y]=='.')
                {
                    stat2[k-'0'][v.x][v.y]=1;
                    val2[k-'0'][v.x][v.y]=val2[k-'0'][u.x][u.y]+1;
                    q.push(v);
                }

            }
        }
    }

    for(int i=1; i<=n; i++)
    {
        for(int j=1; j<=m; j++)
        {
            if(arr[i][j]=='.')
            {
                if(val2[1][i][j]&&val2[2][i][j]&&val2[3][i][j])
                {
                    res=min(res,val2[1][i][j]+val2[2][i][j]+val2[3][i][j]);
                }
            }
        }
    }
    int aa=bfs3('1','1','2');
    int bb=bfs3('2','2','3');
    int cc=bfs3('3','3','1');
    res=min(res,aa+bb);
    res=min(res,cc+bb);
    res=min(res,aa+cc);
    if(res==1000000010)
        printf("-1\n");
    else
        printf("%d\n",res-2);
    return;
}

void bfs1(char a,char b,char c)
{
    // printf("%c %c %c\n",a,b,c);
    while(!q.empty())q.pop();
    memset(stat,0,sizeof(stat));
    memset(val,0,sizeof(val));
    for(int i=1; i<=n; i++)
    {
        for(int j=1; j<=m; j++)
        {
            if(arr[i][j]==a||arr[i][j]==b)
            {
                stat[i][j]=1;
                det.x=i;
                det.y=j;
                q.push(det);
            }
        }
    }
    int ret=0;
    while(!q.empty())
    {
        node u=q.front();
        q.pop();
        for(int i=0; i<4; i++)
        {
            node v;
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            if(v.x>=1&&v.x<=n&&v.y>=1&&v.y<=m&&stat[v.x][v.y]==0&&arr[v.x][v.y]!='#')
            {
                val[v.x][v.y]=val[u.x][u.y]+1;
                if(arr[v.x][v.y]==c)
                {
                    printf("%d\n",val[v.x][v.y]-1);
                    return;
                }
                stat[v.x][v.y]=1;
                q.push(v);
            }
        }

    }
    printf("-1\n");
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    int i,j,k,_12=0,_23=0,_31=0,one=0,two=0,three=0;
    scanf(" %d %d",&n,&m);
    getchar();
    for(i=1; i<=n; i++)
    {
        for(j=1; j<=m; j++)
        {
            scanf("%c",&arr[i][j]);
            if(arr[i][j]=='1')
                one=1;
            if(arr[i][j]=='2')
                two=1;
            if(arr[i][j]=='3')
                three=1;
        }
        getchar();
    }
    for(i=1; i<=n; i++)
    {
        for(j=1; j<=m; j++)
        {
            for(k=0; k<4; k++)
            {
                if((arr[i][j]=='1'&&arr[i+rr[k]][j+cc[k]]=='2')||(arr[i][j]=='2'&&arr[i+rr[k]][j+cc[k]]=='1'))
                    _12=1;
                if((arr[i][j]=='3'&&arr[i+rr[k]][j+cc[k]]=='2')||(arr[i][j]=='2'&&arr[i+rr[k]][j+cc[k]]=='3'))
                    _23=1;
                if((arr[i][j]=='1'&&arr[i+rr[k]][j+cc[k]]=='3')||(arr[i][j]=='3'&&arr[i+rr[k]][j+cc[k]]=='1'))
                    _31=1;
            }
        }
    }
    // printf("%d %d %d...\n",_12,_23,_31);

    if((one+two+three)<=1)
    {
        printf("0\n");
        return 0;
    }

    if((one+two+three)==2)
    {
        if((one+two)==2)
        {
            if(_12)
                printf("0\n");
            else
                bfs1('1','1','2');
            return 0;
        }
        if((three+two)==2)
        {
            if(_23)
                printf("0\n");
            else
                bfs1('2','2','3');
            return 0;
        }
        if((three+one)==2)
        {
            if(_31)
                printf("0\n");
            else
                bfs1('3','3','1');
            return 0;
        }
    }

    if(_12&&_23&&_31)
    {
        printf("0\n");
        return 0;
    }
    if((_12&&_23)||(_23&&_31)||(_12&&_31))
    {
        printf("0\n");
        return 0;
    }
    if(_12)
    {
        bfs1('1','2','3');
        return 0;
    }
    if(_23)
    {
        bfs1('2','3','1');
        return 0;
    }
    if(_31)
    {
        bfs1('3','1','2');
        return 0;
    }
    bfs2();
    return 0;
}
/*
10 10
11........
..........
..........
.........2
.........2
..........
..........
...33.....
..........
..........
*/
