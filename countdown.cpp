/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

map<string,int>mpp;
char name[110],name2[110];
vector<int>adj[1010];
int val[1010],d;
string arr[1010];
queue<int>q;

struct node{
string name;
int val;
};

node res[1010];

int comp(node a,node b)
{
    if(a.val==b.val)
        return a.name<b.name;
    return a.val>b.val;
}

int bfs(int src)
{
    int u,v;
    while(!q.empty())
        q.pop();
    q.push(src);
    val[src]=0;
    int ret=0;
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(int i=0;i<adj[u].size();i++)
        {
            v=adj[u][i];
            val[v]=val[u]+1;
            if(val[v]==d)
                ret++;
            else
                q.push(v);
        }
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    int p,t,i,n,j,u,v,cnt=0,m;
    scanf(" %d",&t);

    for(p=1; p<=t; p++)
    {
        scanf(" %d %d",&n,&d);
        mpp.clear();
        cnt=0;
        for(i=1; i<=n; i++)
        {
            scanf(" %s",name);
            if(mpp[name]==0)
            {
                mpp[name]=++cnt;
            }
            u=mpp[name];
            arr[mpp[name]]=name;
            scanf(" %d",&m);
            for(j=1; j<=m; j++)
            {
                scanf(" %s",name2);
                if(mpp[name2]==0)
                {
                    mpp[name2]=++cnt;
                }
                arr[mpp[name2]]=name2;
                v=mpp[name2];
                adj[u].push_back(v);
            }
        }

        for(i=1;i<=cnt;i++)
        {
            res[i].val=bfs(i);
            res[i].name=arr[i];
        }
        sort(res+1,res+1+cnt,comp);

        printf("Tree %d:\n",p);
        for(i=1;res[i].val>0&&i<=3;i++)
        {
            cout<<res[i].name<<" "<<res[i].val<<"\n";
            if(i==3&&i<cnt&&res[i+1].val==res[i].val)
            {

              while(i<cnt&&res[i+1].val==res[i].val)
              {
                  i++;
                  cout<<res[i].name<<" "<<res[i].val<<"\n";
              }
            }

        }
        printf("\n");
        for(i=1;i<=cnt;i++)
            adj[i].clear();

    }

    return 0;
}

/*
3
8 2
Barney 2 Fred Ginger
Ingrid 1 Nolan
Cindy 1 Hal
Jeff 2 Oliva Peter
Don 2 Ingrid Jeff
Fred 1 Kathy
Andrea 4 Barney Cindy Don Eloise
Hal 2 Lionel Mary
6 1
Phillip 5 Jim Phil Jane Joe Paul
Jim 1 Jimmy
Phil 1 Philly
Jane 1 Janey
Joe 1 Joey
Paul 1 Pauly
6 2
Phillip 5 Jim Phil Jane Joe Paul
Jim 1 Jimmy
Phil 1 Philly
Jane 1 Janey
Joe 1 Joey
Paul 1 Pauly
*/
