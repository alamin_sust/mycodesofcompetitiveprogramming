#include<stdio.h>

struct node{
    int val;
    node *ptr;
};

struct singly{
     node *head,*tail;
     singly(){head=tail=NULL;}
     void insert(int x);
     void print();
};

void singly::insert(int x)
{
    node *temp= new node;
    temp->val=x;
    tail->ptr=temp;
    tail=temp;
    if(head==NULL)
    head=tail=temp;
    delete temp;
    return;
}

void singly::print()
{
    node *temp=new node;
    temp=head;
    if(head==NULL)
    {printf("EMPTY\n");
    return;}
    while(temp->val!=NULL)
    {
        printf("%d ",temp->val);
        temp=temp->ptr;
    }
    printf("\n");
    return;
}

main()
{
    singly s;
    s.insert(5);
    s.print();
    return 0;
}
