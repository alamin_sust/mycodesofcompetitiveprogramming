/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

double res,d,s,arr[100010],prevmx;
int t,p,n,i,c;

int main()
{

    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    scanf(" %d",&t);

    for(p=1;p<=t;p++) {
        scanf(" %d",&n);
        for(i=0;i<n;i++) {
            scanf(" %lf",&arr[i]);
        }
        scanf(" %d %lf %lf", &c, &d,&s);

        res=arr[0]*(c-1);
        prevmx=arr[0];

        for(i=1;i<n;i++) {
            if(arr[i]>prevmx) {
                res=(arr[i]*(c-1));
                prevmx=arr[i];
            }
        }
        printf("%.10lf\n",res);
    }

    return 0;
}
