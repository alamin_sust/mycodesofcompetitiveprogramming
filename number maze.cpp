#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

int rr[]= {0,0,1,-1};
int cc[]= {-1,1,0,0};
int d[1010][1010],adj[1010][1010],r,c,status[1010][1010];

struct node
{
    int x;
    int y;
    int value;
};

node det;
priority_queue<node>pq;

bool operator<(node a,node b)
{
    return a.value>b.value;
}

void dijkstra(int xx,int yy)
{
    d[xx][yy]=0;
    det.x=xx;
    det.y=yy;
    det.value=adj[xx][yy];
    pq.push(det);
    while(!pq.empty())
    {
        xx=pq.top().x;
        yy=pq.top().y;
        int vv=pq.top().value;
        pq.pop();
        status[xx][yy]=1;
        for(int i=0; i<4; i++)
        {
            int rrr=xx+rr[i];
            int ccc=yy+cc[i];
            if(rrr<=r&&rrr>0&&ccc<=c&&ccc>0)
              if(status[rrr][ccc]==0)
                if(d[rrr][ccc]>d[xx][yy]+adj[rrr][ccc])
                {
                    d[rrr][ccc]=d[xx][yy]+adj[rrr][ccc];
                    det.x=rrr;
                    det.y=ccc;
                    det.value=d[rrr][ccc];
                    pq.push(det);
                }
        }
    }
    return;
}

main()
{
    int t,p,i,j;
    cin>>t;
    for(p=1; p<=t; p++)
    {
        cin>>r>>c;
        for(i=1; i<=r; i++)
            for(j=1; j<=c; j++)
            {
                d[i][j]=inf;
                status[i][j]=0;
                scanf(" %d",&adj[i][j]);
            }
        dijkstra(1,1);
        printf("%d\n",d[r][c]+adj[1][1]);
    }
    return 0;
}

