/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll dp[20][20][2][2],n,m,k;
vector<ll>N,K;

ll rec(ll ind,ll ind2,ll flag,ll flag2)
{
    if(ind==N.size())
        return 1;
    if(ind2>=K.size()&&flag2==0)
        return 0;
    ll &ret=dp[ind][ind2][flag][flag2];
    if(ret!=-1)
        return ret;
    ret=0;
    ll fg=flag,fg2=flag2,mx=9,mx2=9;
    if(fg==0)
        mx=N[ind];
    if(ind2<K.size()&&fg2==0)
        mx2=K[ind2];
    for(int i=0;i<=min(mx,mx2);i++)
    {
        fg=1;
        fg2=1;
        if(flag2==0&&i==mx2&&ind2<K.size())
            fg2=0;
        if(flag==0&&i==mx)
            fg=0;
        if(ind2==0&&i==0)
        ret+=rec(ind+1,0,fg,0);
        else
        ret+=rec(ind+1,ind2+1,fg,fg2);
    }
    return ret;
}

main()
{
    ll i,res,now,high,tpmid,kk,low,mid;
    while(cin>>k>>m)
    {
        kk=k;
        K.clear();
        while(kk>0)
            K.push_back(kk%10),kk/=10;
        reverse(K.begin(),K.end());
        high=1000000000000000010LL;
        low=k;
        res=high;
        while(high>=low)
        {
            //printf("%lld\n",mid);
            mid=(high+low)/2LL;
            N.clear();
            tpmid=mid;
            while(tpmid)
                N.push_back(tpmid%10),tpmid/=10;
            reverse(N.begin(),N.end());
            memset(dp,-1,sizeof(dp));
            now=rec(0,0,0,0)-1;
            if(now==m)
                res=min(res,mid);
            if(now>=m)
                high=mid-1;
            else
                low=mid+1;
        }
        printf("%lld\n",res==1000000000000000010LL?0:res);
    }
    return 0;
}

