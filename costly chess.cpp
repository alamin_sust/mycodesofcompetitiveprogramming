#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;

int rr[]={1,2,-1,-2,1,2,-1,-2};
int cc[]={2,1,2,1,-2,-1,-2,-1};
int stat[10][10],d[10][10];


struct node
{
    int x,y,val;
};

node det;

bool operator<(node a,node b)
{
    return a.val>b.val;
}


void dijkstra(int x1,int y1)
{
    priority_queue<node>pq;
    det.x=x1;
    det.y=y1;
    det.val=0;
    d[x1][y1]=0;
    pq.push(det);
    while(!pq.empty())
    {
        int xx=pq.top().x;
        int yy=pq.top().y;
        int vv=pq.top().val;
        stat[xx][yy]=1;
        pq.pop();
        for(int i=0;i<8;i++)
        {
            int xxx=xx+rr[i];
            int yyy=yy+cc[i];
            int vvv=xx*xxx+yy*yyy;
            if(xxx>=0&&yyy>=0&&xxx<8&&yyy<8)
            {
                if(stat[xxx][yyy]==0&&d[xxx][yyy]>(d[xx][yy]+vvv))
                {
                    d[xxx][yyy]=vv+vvv;
                    det.x=xxx;
                    det.y=yyy;
                    det.val=d[xxx][yyy];
                    pq.push(det);
                }
            }
        }
    }
    return;
}

main()
{
    int x1,y1,x2,y2,i,j;
    while(cin>>x1>>y1>>x2>>y2)
    {
        for(i=0;i<8;i++)
            for(j=0;j<8;j++)
               {d[i][j]=inf;
               stat[i][j]=0;}
        dijkstra(x1,y1);
        if(d[x2][y2]==inf)
            printf("-1\n");
        else
            printf("%d\n",d[x2][y2]);
    }
    return 0;
}
