#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class OkonomiyakiShop
{
public:
	int count(vector <int> osize, int K)
	{
	    int res=0;
	    sort(osize.begin(),osize.end());
	    for(int i=0;i<osize.size();i++)
        {
            for(int j=i+1;j<osize.size();j++)
            {
                if((osize[j]-osize[i])<=K)
                    res++;
            }
        }
        return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	OkonomiyakiShop objectOkonomiyakiShop;

	//test case0
	vector <int> param00;
	param00.push_back(1);
	param00.push_back(4);
	param00.push_back(6);
	param00.push_back(7);
	param00.push_back(9);
	int param01 = 3;
	int ret0 = objectOkonomiyakiShop.count(param00,param01);
	int need0 = 6;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(1);
	param10.push_back(1);
	param10.push_back(3);
	param10.push_back(3);
	param10.push_back(3);
	int param11 = 2;
	int ret1 = objectOkonomiyakiShop.count(param10,param11);
	int need1 = 10;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(1);
	param20.push_back(5);
	param20.push_back(9);
	param20.push_back(14);
	param20.push_back(20);
	int param21 = 3;
	int ret2 = objectOkonomiyakiShop.count(param20,param21);
	int need2 = 0;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(7);
	param30.push_back(2);
	param30.push_back(6);
	param30.push_back(3);
	param30.push_back(4);
	param30.push_back(2);
	param30.push_back(7);
	param30.push_back(8);
	param30.push_back(3);
	param30.push_back(4);
	param30.push_back(9);
	param30.push_back(1);
	param30.push_back(8);
	param30.push_back(4);
	param30.push_back(3);
	param30.push_back(7);
	param30.push_back(5);
	param30.push_back(2);
	param30.push_back(1);
	param30.push_back(9);
	param30.push_back(9);
	param30.push_back(4);
	param30.push_back(5);
	int param31 = 6;
	int ret3 = objectOkonomiyakiShop.count(param30,param31);
	int need3 = 234;
	assert_eq(3,ret3,need3);

}
