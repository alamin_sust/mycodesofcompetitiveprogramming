/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int dp[1003][1003][2],lucky[1010],len,arr[1010],MOD=1000000007;
char str[1010];


void luckygen(void)
{
    for(int i=4; i<=1000; i++)
    {
        int j=i;
        lucky[i]=1;
        while(j)
        {
            if((j%10)!=4&&(j%10)!=7)
            {
                lucky[i]=0;
                break;
            }
            j/=10;
        }
    }
    return;
}

void mem(int l)
{
    for(int i=0;i<=l;i++)
    {
        for(int j=0;j<=l;j++)
        {
            dp[i][j][0]=dp[i][j][1]=-1;
        }
    }
    return;
}

int rec(int pos,int dig,int flag)
{
    if(pos==len)
    {
        if(lucky[dig])
            return 1;
        else
            return 0;
    }
    int &ret=dp[pos][dig][flag];
    if(ret!=-1)
        return ret;
    ret=0;
    //ret=(ret+rec(pos+1LL,dig,flag|(arr[pos]>0LL?1LL:0LL)))%MOD;
    for(int i=0; i<=9; i++)
    {
        if(flag==0&&arr[pos]<i)
            break;
        if(i==4||i==7)
            ret=(ret+rec(pos+1,dig+1,flag|(arr[pos]>i?1:0)))%MOD;
        else
            ret=(ret+rec(pos+1,dig,flag|(arr[pos]>i?1:0)))%MOD;
    }
    return ret;
}

int main()
{
    int t,p,res,a,aa,i;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    luckygen();

    // for(i=0;i<=1000;i++)
    // {
    //     if(lucky[i])
    //         printf("%lld\n",i);
    // }

    scanf(" %d",&t);
    for(p=1; p<=t; p++)
    {
        res=0;
        memset(dp,-1,sizeof(dp));

        scanf(" %s",&str);
        len=strlen(str);
        //mem(len);
        a=0;
        for(i=0;i<len;i++)
        {
            if(str[i]=='4'||str[i]=='7')
                a++;
            arr[i]=str[i]-'0';
        }
        if(lucky[a])
            res--;
        //a--;
        //aa=a;
        //len=0LL;
        //while(aa)
        //    aa/=10LL,len++;
        //aa=a;
        //for(i=len-1LL; i>=0LL; i--)
       // {
       //     arr[i]=aa%10LL;
       //     aa/=10LL;
       // }

        res+=rec(0,0,0);

        //scanf(" %lld",&a);
        //aa=a;
        //len=0LL;
        //while(aa)
        //    aa/=10LL,len++;
        //aa=a;
        //for(i=len-1LL; i>=0LL; i--)
       // {
       //     arr[i]=aa%10LL;
       //     aa/=10LL;
       // }
       scanf(" %s",&str);
        len=strlen(str);
        for(i=0;i<len;i++)
        {
            arr[i]=str[i]-'0';
        }
        memset(dp,-1,sizeof(dp));
        //mem(len);
        printf("%lld\n",(rec(0,0,0)-res+MOD)%MOD);
    }
    return 0;
}





