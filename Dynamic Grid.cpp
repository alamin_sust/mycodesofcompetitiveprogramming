/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int rr[]={0,0,1,-1};
int cc[]={-1,1,0,0};

struct node{
int x,y;
};

queue<node>q;

char arr[110][110],ch[10];
int stat[105][105],r,c,t,p;

node u,v;

void bfs(int sx,int sy)
{
    while(!q.empty())
    q.pop();
    u.x=sx;
    u.y=sy;
    q.push(u);
    stat[sx][sy]=1;
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(int i=0;i<4;i++)
        {
            v.x=u.x+rr[i];
            v.y=u.y+cc[i];
            if(v.x>=0&&v.y>=0&&v.x<r&&v.y<c&&arr[v.x][v.y]=='1'&&stat[v.x][v.y]==0)
            {
                stat[v.x][v.y]=1;
                q.push(v);
            }
        }
    }
    return;
}

int bfsres(void)
{
    int ret=0;
    memset(stat,0,sizeof(stat));
    for(int i=0;i<r;i++)
    {
        for(int j=0;j<c;j++)
        {
            if(arr[i][j]=='1'&&stat[i][j]==0)
            {
                 bfs(i,j);
                 ret++;
            }
        }
    }
    return ret;
}

int main()
{
    freopen("A-small-attempt000.in","r",stdin);
    freopen("A-small-attempt000out.txt","w",stdout);

    int n,x,y,z;
    scanf(" %d",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %d %d",&r,&c);
        for(int i=0;i<r;i++)
        {
            scanf(" %s",&arr[i]);
        }
        scanf(" %d",&n);
        printf("Case #%d:\n",p);
        for(int i=0;i<n;i++)
        {
            scanf(" %s",ch);
            if(ch[0]=='Q')
            {
                printf("%d\n",bfsres());
            }
            else
            {
                scanf(" %d %d %d",&x,&y,&z);
                if(z==0)
                arr[x][y]='0';
                else
                arr[x][y]='1';
            }
        }
    }
    return 0;
}


