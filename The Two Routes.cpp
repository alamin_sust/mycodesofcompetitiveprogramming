/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int n,m,adj[1010][1010],i,j,k,from,to;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d %d",&n,&m);

    for(i=1; i<=n; i++)
        for(j=1; j<=n; j++)
            adj[i][j]=99999999;

    for(i=1; i<=m; i++)
    {
        scanf(" %d %d",&from,&to);
        adj[from][to]=adj[to][from]=1;
    }

    if(adj[1][n]==1)
    {
        for(i=1; i<=n; i++)
        {
            for(j=1; j<=n; j++)
            {
                if(adj[i][j]!=99999999)
                adj[i][j]=99999999;
                else
                adj[i][j]=1;
            }
        }

        for(k=1; k<=n; k++)
        {
            for(i=1; i<=n; i++)
            {
                for(j=1; j<=n; j++)
                {
                    if(adj[i][j]>(adj[i][k]+adj[k][j]))
                        adj[i][j]=adj[i][k]+adj[k][j];
                }
            }
        }
        if(adj[1][n]!=99999999)
        {
            printf("%d\n",adj[1][n]);
        }
        else
        {
            printf("-1\n");
        }

    }
    else
    {
        for(k=1; k<=n; k++)
        {
            for(i=1; i<=n; i++)
            {
                for(j=1; j<=n; j++)
                {
                    if(adj[i][j]>(adj[i][k]+adj[k][j]))
                        adj[i][j]=adj[i][k]+adj[k][j];
                }
            }
        }
        if(adj[1][n]!=99999999)
        {
            printf("%d\n",adj[1][n]);
        }
        else
        {
            printf("-1\n");
        }
    }

    return 0;
}


