#include <stdio.h>
#include <math.h>

main()
{
   double n,R,r,t,bluearea,greenarea,pi,pi_n,t1;
   pi = 2.0 * acos(0.0);
   while(scanf("%lf%lf",&R,&n) != EOF) {
      if(n==1) {
         printf("%.10lf %.10lf %.10lf\n",R,t1,t1);
         continue;
      }
      else if(n==2) {
         t1 = (pi*R*R);
         greenarea = t1/2.00000000000000;
         t = 0.0;
         printf("%.10lf %.10lf %.10lf\n",R/2.0000000000000000,t,greenarea);
         continue;
      }
      pi_n = pi / n;
      r = 1.0000000000000 + ( 1.00000000000/ sin(pi_n) );
      r = R / r;
      t = 2.0000000000000 * r;
      t = t*t;
      t *= (1.00000000000/tan(pi_n)) *n;
      t /= 4.000000000000;
      t1 = pi*r*r;
      bluearea = t - t1*(n-2.000000000000)*0.50000000000000;
      greenarea = (pi*R*R) - bluearea - (n*t1);
      printf("%.10lf %.10lf %.10lf\n",r,bluearea,greenarea);
   }
   return 0;
}
