#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class PipeCuts
{
public:
	double probability(vector <int> weldLocations, int L)
	{
	    int k=0,l=0;
	    sort(weldLocations.begin(),weldLocations.end());
	    for(int i=0;i<weldLocations.size();i++)
        {
            for(int j=i+1;j<weldLocations.size();l++,j++)
            {
                if((weldLocations[j]-weldLocations[i])>L||weldLocations[i]>L||(100-weldLocations[j])>L)
                    k++;
            }
        }
        return (double)k/(double)l;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	PipeCuts objectPipeCuts;

	//test case0
	vector <int> param00;
	param00.push_back(25);
	param00.push_back(50);
	param00.push_back(75);
	int param01 = 25;
	double ret0 = objectPipeCuts.probability(param00,param01);
	double need0 = 1.0;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(25);
	param10.push_back(50);
	param10.push_back(75);
	int param11 = 50;
	double ret1 = objectPipeCuts.probability(param10,param11);
	double need1 = 0.0;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(25);
	param20.push_back(50);
	param20.push_back(75);
	int param21 = 24;
	double ret2 = objectPipeCuts.probability(param20,param21);
	double need2 = 1.0;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(99);
	param30.push_back(88);
	param30.push_back(77);
	param30.push_back(66);
	param30.push_back(55);
	param30.push_back(44);
	param30.push_back(33);
	param30.push_back(22);
	param30.push_back(11);
	int param31 = 50;
	double ret3 = objectPipeCuts.probability(param30,param31);
	double need3 = 0.7222222222222222;
	assert_eq(3,ret3,need3);
}
