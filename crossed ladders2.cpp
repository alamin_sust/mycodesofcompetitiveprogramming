#include<cstdio>
#include<cmath>
using namespace std;
double x,y,c,s,func,U,D,e,f;
int main(void)
{
    while(scanf("%lf%lf%lf",&x,&y,&c)==3)
    {
        if(x==y&&c!=0)
        {
            s = sqrt(x*x-4*c*c);
            printf("%.3lf\n",s);
        }
        else if(c==0)
        {
            printf("0.000\n");
        }
        else
        {
            if(x>y)
            U = x;
            else
            U = y;
            D = 0.0;
            s = (U+D)/2.0;
            e = sqrt(x*x-s*s);
            f = sqrt(y*y-s*s);
            func = (1.0/c)-(1.0/e)-(1.0/f);
            while(abs(func)>0.0000001)
            {
                if(func>0)
                D = s;
                else
                U = s;
                s = (U+D)/2.0;
                e = sqrt(x*x-s*s);
                f = sqrt(y*y-s*s);
                func = (1.0/c)-(1.0/e)-(1.0/f);
            }
            printf("%.3lf\n",s);
        }
    }
    return 0;
}
