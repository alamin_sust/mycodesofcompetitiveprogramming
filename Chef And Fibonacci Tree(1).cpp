/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll f[200100],in,M=1000000007LL,dpt[100010],x,par[100010];
vector<ll>arr[100010],adj[100010],nd[100010],lookat[100010];
queue<ll>q;
char ch[10];

ll bfs(void)
{
    ll u,v;
    q.push(1);
    dpt[1]=1;
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(ll i=0; i<adj[u].size(); i++)
        {
            v=adj[u][i];
            if(dpt[v]==0)
            {
                dpt[v]=dpt[u]+1;
                q.push(v);
            }
        }
    }
    return dpt[u];
}

ll bfs2(ll src)
{
    ll u,v;
    while(!q.empty()) q.pop();
    q.push(src);
    while(!q.empty())
    {
        u=q.front();
        q.pop();
        for(ll i=0; i<adj[u].size(); i++)
        {
            v=adj[u][i];
            if((dpt[v]%100)==1)
            {
                 lookat[v].push_back(src);
            }
            else
                q.push(v);
        }
    }
    return dpt[u];
}

void rec(ll pos,vector<ll> v)
{
    for(ll i=0;i<v.size();i++)
    nd[pos].push_back(v[i]);
    if((dpt[pos]%100)==1)
    {
        v.push_back(pos);
    }
    for(ll i=0;i<adj[pos].size();i++)
    {
        rec(adj[pos][i],v);
    }
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen("outlljj.txt","w",stdout);

    ll cnt,fn,mxdpt,i,j,res,n,m,k;

    f[1]=f[2]=1LL;
    for(i=3; i<=200010; i++)
    {
        f[i]=(f[i-1]+f[i-2])%M;
    }

    scanf(" %lld %lld",&n,&m);

    for(i=2; i<=n; i++)
    {
        scanf(" %lld",&in);
        par[i]=in;
        adj[in].push_back(i);
    }

    mxdpt=bfs();

    if((mxdpt*m)<=50000000LL)
    {
        for(i=1; i<=m; i++)
        {
            scanf(" %s",ch);
            if(ch[0]=='U')
            {
                scanf(" %lld %lld",&x,&k);
                arr[x].push_back(k);
            }
            else
            {
                scanf(" %lld",&x);
                res=0LL;
                for(j=0; j<arr[x].size(); j++)
                {
                    fn=arr[x][j];
                    res=(res+f[fn])%M;
                }
                cnt=0LL;
                while(x!=1LL)
                {
                    cnt++;
                    x=par[x];
                    for(j=0; j<arr[x].size(); j++)
                    {
                        fn=arr[x][j]+cnt;
                        res=(res+f[fn])%M;
                    }

                }
                printf("%lld\n",res);
            }
        }

    }
    else
    {
        vector<ll>v;
        rec(1,v);
        for(i=1; i<=m; i++)
        {
            scanf(" %s",ch);
            if(ch[0]=='U')
            {
                scanf(" %lld %lld",&x,&k);
                arr[x].push_back(k);
                if((dpt[x]%100)!=1)
                bfs2(x);
            }
            else
            {
                res=0LL;
                scanf(" %lld",&x);
                res=0LL;
                for(j=0;j<nd[x].size();j++)
                {
                    fn=(dpt[x]-dpt[nd[x][j]]);
                    for(ll jj=0;jj<arr[nd[x][j]].size();jj++)
                    {
                        res=(res+f[arr[nd[x][j]][jj]+fn])%M;
                    }
                    for(ll jj=0;jj<lookat[nd[x][j]].size();jj++)
                    {
                        for(ll kk=0;kk<arr[lookat[nd[x][j]][jj]].size();kk++)
                        {
                            res=(res+f[dpt[x]-dpt[arr[lookat[nd[x][j]][jj]][kk]]])%M;
                        }
                    }
                }
                printf("%lld\n",res);
            }
        }
    }

    return 0;
}

