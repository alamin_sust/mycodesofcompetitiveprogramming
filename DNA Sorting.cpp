#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int t,p,i,j,k,s,n,l;
char tp[110];

struct node
{
    char str[110];
    int val;
};

node arr[110];

main()
{
    cin>>t;
    for(p=0;p<t;p++)
    {
        cin>>l>>n;
        getchar();
        for(i=0;i<n;i++)
        {
            gets(arr[i].str);
            strcpy(tp,arr[i].str);
            s=0;
            for(j=0;j<l;j++)
            {
                for(k=1;k<l;k++)
                {
                    if(tp[k]<tp[k-1])
                    {
                        swap(tp[k],tp[k-1]);
                        s++;
                    }
                }
            }
            arr[i].val=s;
        }
        for(i=0;i<n;i++)
        {
            for(j=1;j<n;j++)
            {
                if(arr[j].val<arr[j-1].val)
                {
                    swap(arr[j],arr[j-1]);
                }
            }
        }
        if(p)
            printf("\n");
        for(i=0;i<n;i++)
            printf("%s\n",arr[i].str);
    }
    return 0;
}

