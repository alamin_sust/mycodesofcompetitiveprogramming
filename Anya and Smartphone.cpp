/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll n,m,k,i,res,in,v[100010],mpp[100010],tp1,val1,val2;

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    cin>>n>>m>>k;

    for(i=1;i<=n;i++)
    {
        scanf(" %I64d",&in);
        mpp[in]=i;
        v[i]=in;
    }
    res=m;
    for(i=1;i<=m;i++)
    {
        scanf(" %I64d",&in);
        res+=(mpp[in]-1)/k;
        tp1=mpp[in];
        if(tp1>1)
        {
           val1=in;
           val2=v[tp1-1];
           mpp[val1]=tp1-1;
           mpp[val2]=tp1;
           v[tp1]=val2;
           v[tp1-1]=val1;
        }
    }
    printf("%I64d\n",res);
    return 0;
}





