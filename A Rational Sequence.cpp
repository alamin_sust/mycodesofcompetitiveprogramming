/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll t,p,pp,pt,q,val,i,k,arr[100010];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    scanf(" %lld",&t);

    for(pt=1LL;pt<=t;pt++)
    {
        scanf(" %lld %lld/%lld",&pp,&p,&q);
        string s="";
        while(p!=q)
        {
            if(p>q)
            {
                s+="R";
                p-=q;
            }
            else
            {
                s+="L";
                q-=p;
            }
        }
        val=1;
        for(i=s.length()-1LL;i>=0LL;i--)
        {
            val*=2LL;
            if(s[i]=='R')
            val++;
            printf("%lld...\n",i);
        }
        val++;

        k=0LL;
        while(val)
        {
            arr[k++]=val&1LL;
            val>>=1LL;
        }
        p=1LL;
        q=1LL;
        for(i=k-2LL;i>=0LL;i--)
        {
            if(arr[i]==0)
                q+=p;
            else
                p+=q;

        }
        printf("%lld %lld/%lld\n",pp,p,q);
    }

    return 0;
}


