#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) ( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node{
ll x,y,val;
};

bool operator<(node a,node b)
{
    return a.val>b.val;
}

node det;
priority_queue<node>pq;

ll t,p,i,j,n,par[800],adj[800][800],x[800],y[800],a,b,m;

main()
{
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n;
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
                adj[i][j]=0;
            par[i]=i;
            scanf(" %lld %lld",&x[i],&y[i]);
        }
        cin>>m;
        for(i=1;i<=m;i++)
        {
            scanf(" %lld %lld",&a,&b);
            adj[a][b]=adj[b][a]=1;
            while(par[a]!=a)
                a=par[a];
            while(par[b]!=b)
                b=par[b];
            if(a!=b)
                par[a]=b;
        }
        for(i=1;i<=n;i++)
        {
            for(j=i+1;j<=n;j++)
            {
                if(adj[i][j]==0)
                {
                    det.x=i;
                    det.y=j;
                    det.val=dist(x[i],y[i],x[j],y[j]);
                    pq.push(det);
                }
            }
        }
        ll fg=0;
        while(!pq.empty())
        {
            a=pq.top().x;
            b=pq.top().y;
            while(par[a]!=a)
                a=par[a];
            while(par[b]!=b)
                b=par[b];
            if(a!=b)
            {
                par[a]=b;
                fg=1;
                printf("%lld %lld\n",pq.top().x,pq.top().y);
            }
            pq.pop();
        }
        if(fg==0)
            printf("No new highways need\n");
        if(p!=t)
        printf("\n");
    }
    return 0;
}

