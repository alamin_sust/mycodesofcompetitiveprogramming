/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999999LL
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

map<ll,ll>mpp1,mpp2,mpp3,mpp4,mask1,mask3;
ll l,type,approval,num,flag,prio;
ll n,m,x,i,j,k,tp;
char arr[110],dummy[110],dir[110];

int main()
{
    freopen("access.in","r",stdin);
    freopen("access.out","w",stdout);
    while(cin>>n)
    {
        mpp1.clear();
        mpp2.clear();
        mpp3.clear();
        mpp4.clear();
        mask1.clear();
        mask3.clear();
        for(i=1LL;i<=n;i++)
        {
            scanf(" %s %s %s",dir,dummy,arr);
            l=strlen(arr);
            type=2LL;
            for(j=0LL;j<l;j++)
            {
                if(arr[j]=='/')
                    type=1LL;
            }
            if(dir[0]=='d')
                approval=1;
            else
                approval=2;
            num=0LL;
            for(k=0LL,j=0LL;j<4LL;j++)
            {
                tp=0LL;
                while(arr[k]>='0'&&arr[k]<='9'&&k<l)
                {
                    tp=tp*10LL+(arr[k]-'0');
                    k++;
                }
                num=(num*256LL)+tp;
                k++;
            }
            if(type==1LL)
            {
                tp=0LL;
                while(k<l)
                {
                    tp=tp*10LL+(arr[k]-'0');
                    k++;
                }
                if(approval==1LL)
                {
                    mpp1[num*100LL+tp]=i;
                    mask1[num]=1LL;
                    for(j=0LL;j<(32LL-tp);j++)
                    mask1[num]*=2LL;
                    mask1[num]--;
                }
                else
                {
                    mpp3[num*100LL+tp]=i;
                    mask3[num]=1LL;
                    for(j=0LL;j<(32LL-tp);j++)
                    mask3[num]*=2LL;
                    mask3[num]--;
                }
            }
            else
            {
                if(approval==1LL)
                {
                    mpp2[num]=i;
                }
                else
                {
                    mpp4[num]=i;
                }
            }
        }
        scanf(" %I64d",&m);
        for(i=0LL;i<m;i++)
        {
            scanf(" %s",arr);
            num=0LL;
            for(k=0LL,j=0LL;j<4LL;j++)
            {
                tp=0LL;
                while(arr[k]>='0'&&arr[k]<='9'&&k<l)
                {
                    tp=tp*10LL+(arr[k]-'0');
                    k++;
                }
                num=(num*256LL)+tp;
                k++;
            }
            flag=0LL;
            prio=inf;
            if(mpp1[num*100LL+32LL])
                prio=mpp1[num*100LL+32LL],flag=1LL;

            for(x=31LL,k=2LL,j=4294967294LL;j>=0LL&&x>=0LL;x--,k*=2LL)
            {
                if(mpp1[((num/k)*k)*100LL+x]>0LL&&mpp1[((num/k)*k)*100LL+x]<prio)
                {
                    prio=mpp1[((num/k)*k)*100LL+x];
                    flag=1LL;
                    //break;
                }
                j-=k;
            }
            if(mpp2[num]>0LL&&mpp2[num]<prio)
            {
                prio=mpp2[num];
                flag=1LL;
            }

            if(mpp3[num*100LL+32LL]>0LL&&mpp3[num*100LL+32LL]<prio)
                prio=mpp3[num*100LL+32LL],flag=0LL;

            for(x=31LL,k=2LL,j=4294967294LL;j>=0LL&&x>=0LL;x--,k*=2LL)
            {
                if(mpp3[((num/k)*k)*100LL+x]>0LL&&mpp3[((num/k)*k)*100LL+x]<prio)
                {
                    prio=mpp3[((num/k)*k)*100LL+x];
                    flag=0LL;
                    //break;
                }
                j-=k;
            }
          if(mpp4[num]>0LL&&mpp4[num]<prio)
            {
                prio=mpp4[num];
                flag=0LL;
            }
           if(flag)
            {
                printf("D");
            }
            else
            {
                printf("A");
            }

        }
        printf("\n");

    }
    return 0;
}

//4
//allow from 10.0.0.1
//deny from 10.0.0.0/8
//allow from 192.168.0.0/16
//deny from 192.168.0.1
//5
//10.0.0.1
//10.0.0.2
//194.85.160.133
//192.168.0.1
//192.168.0.2
