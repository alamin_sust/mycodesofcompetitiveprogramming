#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int arr[110][110],tp[1010],det[110],k;
char in[1000010];
void dfs(int i,int j)
{
    //printf("%d--\n",k);
    arr[i][j]=-1;
    if(arr[i+1][j]==k)
        dfs(i+1,j);
    if(arr[i][j+1]==k)
        dfs(i,j+1);
    if(arr[i-1][j]==k)
        dfs(i-1,j);
    if(arr[i][j-1]==k)
        dfs(i,j-1);
    return;
}

main()
{
    int l,dd,i,n,j,flag,x,y;
    char ch;
    while(scanf("%d",&n)!=EOF&&n)
    {
        getchar();
        memset(arr,-1,sizeof(arr));
        for(i=1; i<=n; i++)
            for(j=1; j<=n; j++)
                arr[i][j]=0;
        flag=0;
        for(i=1; i<n; i++)
        {
            gets(in);
            l=strlen(in);
            k=0;
            dd=1;
            for(j=l-1;j>=0;j--)
            {
                if(in[j]>='0'&&in[j]<='9')
                {
                   if(j+1==l||in[j+1]==' ')
                    dd=1,k++,tp[k]=0;
                   tp[k]+=dd*(in[j]-'0');
                   dd*=10;
                }
            }
            //printf("%d..\n",k);
            int ext=0;
            for(j=1;j<k;j+=2)
                if(arr[tp[j+1]][tp[j]]>0)
                    ext+=2;
                else
                arr[tp[j+1]][tp[j]]=i;
                //printf(".....%d\n",tp[j]);
            if((k-ext)!=(2*n))
                flag=1;
                //printf("%d:::\n",k);
        }
      /*  for(i=0; i<=n+1; i++)
        {
            for(j=0; j<=n+1; j++)
            {
                printf("%d ",arr[i][j]);
            }
            printf("\n");
        }*/
        if(flag==0)
        {
           // printf(";;;;");
            k=0;
            for(i=0; i<n; i++)
                det[i]=0;
            for(i=1; i<=n; i++)
            {
                for(j=1; j<=n; j++)
                {
                  //  printf("%d s %d\n",i,j);
                    if(arr[i][j]!=-1&&arr[i][j]<n&&det[arr[i][j]]==0)
                    {
                        det[arr[i][j]]=1;
                        k=arr[i][j];
                    //    printf("%d-------%d-%d\n",i,j,k);
                        dfs(i,j);
                    }
                }
            }
        /*    for(i=1; i<=n; i++)
            {
                for(j=1; j<=n; j++)
                {
                    printf("%d ",arr[i][j]);
                }
                printf("\n");
            }*/
            for(i=1; i<=n&&flag==0; i++)
            {
                for(j=1; j<=n; j++)
                {
                    if(arr[i][j]!=-1)
                    {
                        flag=1;
                        break;
                    }
                }
            }
        }
        if(flag==0)
            printf("good\n");
        else
            printf("wrong\n");
    }
    return 0;
}
/*
1
2
1 1
2
1 1 1 2
2
1 1 1 2 2 1
2

2
1 1 1 2 1 2
3
1 1 1 2 1 3
2 1 2 2 2 3
3
1 1 1 2     1 3
2 1 2 2     2 3
3
1 1 1 2 2 1
3 3 3 2 2 3
3
1 1 1 2 1 3 1 1 1 2 1 3
2 1 2 2 2 3 2 1 2 2 2 3
0
*/
