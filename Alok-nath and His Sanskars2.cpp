/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int flag;
map<string,int>mpp;

ll n,agg,cntr,k,arr[110];

int comp(ll a,ll b)
{
    return a>b;
}

void rec(ll pos,string val,vector<ll>vc,ll taken)
{
   // cntr++;
    //cout<<val<<endl;
    if(flag==1)
        return;
    if(mpp[val])
        return;
    if(taken+(n-pos)<k)
        return;
    mpp[val]=1;
    if(pos==n)
    {
        for(ll i=1LL;i<=k;i++)
        {
            if(vc[i]<0LL)
                return;
        }

        flag=1;
        return;
    }
    char j;
    int tpflag=0;
    for(ll i=1LL,j='1'; i<=k; j++,i++)
    {
        if(tpflag==0&&vc[i]==-1LL)
        {
            tpflag=1;
            vc[i]=arr[pos];
            rec(pos+1,val+(char)j,vc,taken+1LL);
            vc[i]=-1;
        }
        else if(vc[i]>=0LL&&(vc[i]+arr[pos])<=agg)
        {
            vc[i]+=arr[pos];
            rec(pos+1,val+(char)j,vc,taken);
            vc[i]-=arr[pos];
        }
       /*if(vc[i]+arr[pos]+(vc[i]==-1LL?1LL:0LL)<=agg)
        {
            ll tmp=vc[i];
            ll tkn=(vc[i]==-1LL?1LL:0LL);
            vc[i]+=arr[pos]+tkn;
            rec(pos+1,val+(char)j,vc,tkn+taken);
            vc[i]=tmp;
       }*/
    }
    return;
}

int main()
{
    int t,p,i;
    //gen();
    cin>>t;
    for(p=1; p<=t; p++)
    {
        cin>>n>>k;
        agg=0LL;
        for(i=0; i<n; i++)
            scanf(" %lld",&arr[i]),agg+=arr[i];
        sort(arr,arr+n,comp);
        if((agg%k)!=0LL||(agg/k)<arr[0])
        {
            printf("no\n");
            continue;
        }
       // for(i=0;i<n;i++)
         // printf("%lld.. ",arr[i]);
        agg/=k;
        flag=0;
        vector<ll>vcc;
        vcc.clear();
        for(i=0;i<=k;i++)
            vcc.push_back(-1LL);
        mpp.clear();
        rec(0LL,"",vcc,0LL);
        //printf("%lld..\n",cntr);
        if(flag)
            printf("yes\n");
        else
            printf("no\n");
    }
    return 0;
}

