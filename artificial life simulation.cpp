/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
int rr[]= {0,0,1,-1};
int cc[]= {-1,1,0,0};

struct node
{
    ll stx,sty,nowx,nowy,id,age,parx,pary,yrs,born;
    string path,dna;
};
node life[100010],det,det2;
queue<node>q;
ll arr[1010][1010],row,col,m,n,years,lifespan,cnt,r,cnt2,r2,flag[10000010],r3,hhh[5];

string getdna(string dna1,string dna2)
{
    ll i,l;
    string child_dna="",chain=dna1+dna2;
    l=dna1.length()+dna2.length();

    for(i=0;i<l;i++)
        flag[i]=0;

    for(i=0;i<(l/2);i++)
    {
        while(1)
        {
            r3=rand()%l;
            if(flag[r3]==0)
            {
                flag[r3]=1;
                child_dna+=chain[r3];
                break;
            }
        }
    }

    return child_dna;
}


void init(void)
{
    ll i,j;
    while(!q.empty())
        q.pop();
    for(i=0; i<=row+1LL; i++)
        for(j=0; j<=col+1LL; j++)
            arr[i][j]=0;
}

void bfs(void)
{

    for(ll i=1; i<=n; i++)
    {

        det.yrs=0;
        det.nowx=life[i].nowx;
        det.nowy=life[i].nowy;
        det.id=life[i].id;
        det.age=0;
        det.dna=life[i].dna;
        det.parx=life[i].parx;
        det.pary=life[i].pary;
        det.path="";
        //printf("-----");
        q.push(det);
       //printf("-----");
    }

    while(!q.empty())
    {
        det=q.front();
        //printf("%lld %lld\n",det.nowx,det.nowy,det.age);
       // getchar();
        q.pop();
        if(det.yrs>=years)
            continue;
        if(det.age>lifespan)
        {
            arr[det.nowx][det.nowy]=0;
            continue;
        }
        cnt=100LL;
        while(cnt--)
        {
            r=rand()%4;
         //   hhh[r]++;
            if((det.nowx+rr[r])>=1LL&&(det.nowx+rr[r])<=row&&(det.nowy+cc[r])>=1LL&&(det.nowy+cc[r])<=col&&arr[det.nowx+rr[r]][det.nowy+cc[r]]==0)
            {
                if(r==0)
                  life[arr[det.nowx][det.nowy]].path+='L';
                else if(r==1)
                  life[arr[det.nowx][det.nowy]].path+='R';
                else if(r==2)
                  life[arr[det.nowx][det.nowy]].path+='D';
                else if(r==3)
                  life[arr[det.nowx][det.nowy]].path+='U';

                arr[det.nowx][det.nowy]=0;
                arr[det.nowx+rr[r]][det.nowy+cc[r]]=det.id;
                det2.nowx=det.nowx+rr[r];
                det2.nowy=det.nowy+cc[r];
                det2.age=det.age+1LL;
                det2.yrs=det.yrs+1LL;
                det2.parx=det.parx;
                det2.pary=det.pary;
                det2.dna=det.dna;
                det2.id=det.id;
                q.push(det2);
                if(arr[det.nowx+rr[r]][det.nowy+cc[r]-1]!=0&&arr[det.nowx+rr[r]-1][det.nowy+cc[r]-1]==0&&arr[det.nowx+rr[r]-1][det.nowy+cc[r]]==0&&arr[det.nowx+rr[r]+1][det.nowy+cc[r]-1]==0&&arr[det.nowx+rr[r]+1][det.nowy+cc[r]]==0)
                {
                    cnt2=100LL;
                    det2.born=det2.yrs;
                    while(cnt2--)
                    {
                        r2=rand()%2;
                        if(r2==0&&(det.nowx+rr[r])>1)
                        {
                            arr[det.nowx+rr[r]-1][det.nowy+cc[r]-1]=++m;
                            det2.nowx=det.nowx+rr[r]-1;
                            det2.nowy=det.nowy+cc[r]-1;
                            det2.age=0;
                            det2.yrs=det.yrs+1LL;
                            det2.parx=arr[det.nowx+rr[r]][det.nowy+cc[r]-1];
                            det2.pary=det.id;
                            det2.dna=getdna(life[det2.parx].dna,life[det2.pary].dna);
                            det2.id=m;
                            det2.stx=det.nowx+rr[r]-1;
                            det2.sty=det.nowy+cc[r]-1;
                            det2.path="";
                            life[m]=det2;
                            q.push(det2);

                            arr[det.nowx+rr[r]-1][det.nowy+cc[r]]=++m;
                            det2.nowx=det.nowx+rr[r]-1;
                            det2.nowy=det.nowy+cc[r];
                            det2.age=0;
                            det2.yrs=det.yrs+1LL;
                            det2.parx=arr[det.nowx+rr[r]][det.nowy+cc[r]-1];
                            det2.pary=det.id;
                            det2.dna=getdna(life[det2.parx].dna,life[det2.pary].dna);
                            det2.id=m;
                            det2.stx=det.nowx+rr[r]-1;
                            det2.sty=det.nowy+cc[r];
                            det2.path="";
                            life[m]=det2;
                            q.push(det2);

                            break;
                        }
                        else if(r2==1&&(det.nowx+rr[r])<row)
                        {
                            arr[det.nowx+rr[r]+1][det.nowy+cc[r]-1]!=++m;
                            det2.nowx=det.nowx+rr[r]+1;
                            det2.nowy=det.nowy+cc[r]-1;
                            det2.age=0;
                            det2.parx=arr[det.nowx+rr[r]][det.nowy+cc[r]-1];
                            det2.pary=det.id;
                            det2.dna=getdna(life[det2.parx].dna,life[det2.pary].dna);
                            det2.id=m;
                            det2.stx=det.nowx+rr[r]+1;
                            det2.sty=det.nowy+cc[r]-1;
                            det2.yrs=det.yrs+1LL;
                            det2.path="";
                            life[m]=det2;
                            q.push(det2);

                            arr[det.nowx+rr[r]+1][det.nowy+cc[r]]!=++m;
                            det2.nowx=det.nowx+rr[r]+1;
                            det2.nowy=det.nowy+cc[r];
                            det2.age=0;
                            det2.parx=arr[det.nowx+rr[r]][det.nowy+cc[r]-1];
                            det2.pary=det.id;
                            det2.dna=getdna(life[det2.parx].dna,life[det2.pary].dna);
                            det2.id=m;
                            det2.stx=det.nowx+rr[r]+1;
                            det2.sty=det.nowy+cc[r];
                            det2.yrs=det.yrs+1LL;
                            det2.path="";
                            life[m]=det2;
                            q.push(det2);
                            break;
                        }
                    }
                }
                break;
            }
        }
    }
    return;
}

int main()
{
    ll p,i,j;
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    init();
    printf("What's Your World's Dimension?: ");
    scanf(" %lld %lld",&row,&col);
    printf("How many lives are created at the beginning?: ");
    scanf(" %lld",&n);
    printf("Enter the co-ordinates and DNA sequence of the lives: ");
    for(p=1; p<=n; p++)
    {
        scanf(" %lld %lld",&life[p].stx,&life[p].sty);
        cin>>life[p].dna;
        life[p].nowx=life[p].stx;
        life[p].nowy=life[p].sty;
        life[p].id=p;
        arr[life[p].stx][life[p].sty]=p;
        life[p].age=0;
        life[p].parx=0;
        life[p].pary=0;
        life[p].path="";
        life[p].born=0;
    }
    printf("life cycle period: ");
    scanf(" %lld",&lifespan);

    printf("How many years you want to look after? ");
    scanf(" %lld",&years);
    m=n;
    //printf(".......");
    bfs();
    for(i=1;i<=row;i++)
    {
        for(j=1;j<=col;j++)
        {
            printf("%lld ",arr[i][j]);
        }
        printf("\n");
    }

    for(i=1;i<=m;i++)
    {
        printf("Life(%lld): father:%lld  mother:%lld born_year:%lld DNA:",i,life[i].parx,life[i].pary,life[i].born);
        cout<<life[i].dna<<endl;
        cout<<" LIFE PATH:"<<life[i].path<<endl;
    }
    //printf("%lld %lld %lld %lld\n",hhh[0],hhh[1],hhh[2],hhh[3]);
    return 0;
}

/*
50 50
5
2 2 ATCC
50 33 GCTA
90 67 AATC
30 12 CTCA
40 99 CATT
1000
1005
*/



