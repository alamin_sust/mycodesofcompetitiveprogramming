/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int len,num;
double dp[32][32][32][2];


double rec(int pos,int taken,int digit,int flag)
{
   // printf("%d %d\n",pos,flag);
    if(pos==len)
    {if(digit==0)
        return 0.0;
        return (double)taken/(double)digit;}

    double &ret=dp[pos][taken][digit][flag];
    if(ret>-0.5)
        return ret;
    ret=0.0;
    if(flag==0)
    {
        ret+=rec(pos+1,taken+1,digit+1,0);
        ret+=rec(pos+1,taken,digit==0?0:digit+1,0);
    }
    else
    {
        if((1<<(len-pos-1))&num)
        ret+=rec(pos+1,taken+1,digit+1,1);
        ret+=rec(pos+1,taken,digit==0?0:digit+1,((1<<(len-pos-1))&num)==0?1:0);
    }
    return ret;
}


int main()
{
    double res;
    int t,p,a,b,tp;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>a>>b;
        if(a>b)
            swap(a,b);
        len=0;
        a--;
        tp=a;
        for(int i=0;i<32;i++)
            for(int j=0;j<32;j++)
            for(int k=0;k<32;k++)
            for(int l=0;l<2;l++)
            dp[i][j][k][l]=-1.0;
        num=a;
        while(tp>0)
        {
            tp/=2;
            len++;
        }
        res=rec(0,0,0,1);
       //printf("%lf\n",res);

        len=0;
        tp=b;
        num=b;
        for(int i=0;i<32;i++)
            for(int j=0;j<32;j++)
            for(int k=0;k<32;k++)
            for(int l=0;l<2;l++)
            dp[i][j][k][l]=-1.0;
        while(tp>0)
        {
            tp/=2;
            len++;
        }
        res=rec(0,0,0,1)-res;

        printf("%.6lf\n",res/(double)(b-a));
    }
    return 0;
}


