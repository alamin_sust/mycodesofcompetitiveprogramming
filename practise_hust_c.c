#include<iostream>
#include<vector>
#include<stdio.h>
#include<queue>
#define max 10010

using namespace std;

vector <long> edge[max];

long color[max],node[max],num,indeg[max],visited[max];

void tsort(long n)
{
 queue <long> que;
     for(long j=1;j<=n;j++)
     {
      if(indeg[j]==0)
      {
       que.push(j);
      }
     }
     while(!que.empty())
     {
      long u=que.front();
      //cout<<" "<<u<<" ";
      node[num]=que.front();
      num++;
      que.pop();
      for(long j=0;j<edge[u].size();j++)
      {
       indeg[edge[u][j]]--;
       if(indeg[edge[u][j]]==0)
       {
        que.push(edge[u][j]);
       }
      }
     }
}

void bfs(long source,long n)
{
     long i;
 for(i=1;i<=n;i++)
 {
  color[i]=1;
 }
 color[source]=2;
 queue <long> q;
 q.push(source);
 while(!q.empty())
 {
  long u=q.front();
   visited[u]=1;
  //cout<<u<<"- ";
  q.pop();
  for(i=0;i<edge[u].size();i++)
  {
    if(color[edge[u][i]]==1)
    {
     color[edge[u][i]]=2;
     q.push(edge[u][i]);
     visited[edge[u][i]]=1;
    }
  }
 }
}

int main()
{
 long t,from,to,n,m,i,j,k,res,cas=1;

 cin>>t;
 for(i=0;i<t;i++)
 {
  cin>>n>>m;
  res=0;
  for(j=1;j<=n;j++) {
                    indeg[j]=0;
                    visited[j]=0;
                    }
  for(j=0;j<m;j++)
  {
   cin>>from>>to;
   indeg[to]++;
   edge[from].push_back(to);
   //edge[to].push_back(from);
  }
  num=0;
  tsort(n);
  for(j=0;j<num;j++)
  {
   //cout<<node[j]<<" ";
   if(visited[node[j]]==0)
   {
    bfs(node[j],n);
    res++;
   }
  }
  for(j=1;j<=n;j++)
  {
   if(indeg[j]!=0 && visited[j]==0)
   {
    res++;
    visited[j]=1;
    bfs(j,n);
   }
  }
  printf("Case %ld: %ld\n",cas,res);
  cas++;
  for(j=1;j<=n;j++)
  {
                    edge[j].clear();
  }
 }
 return 0;
}
