#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>

using namespace std;

main()
{
    int t,p,n,i,j,x1,x2,y1,y2,flag,a,b;
    char arr[28][28],ch;
    FILE *read,*write;
    read=fopen("read20.txt","r");
    write=fopen("write20.txt","w");
    fscanf(read," %d",&t);
    for(p=1; p<=t; p++)
    {
        fscanf(read," %d",&n);
        fscanf(read,"%c",&ch);
        for(i=1; i<=n; i++)
        {
            for(j=1; j<=n; j++)
                fscanf(read,"%c",&arr[i][j]);
            fscanf(read,"%c",&ch);
        }
        flag=0;
        for(i=1; i<=n&&flag==0; i++)
        {
            for(j=1; j<=n; j++)
                if(arr[i][j]=='#')
                {
                    x1=i;
                    y1=j;
                    flag=1;
                    break;
                }
        }
        flag=0;
        for(i=n; i>=1&&flag==0; i--)
        {
            for(j=n; j>=1; j--)
                if(arr[i][j]=='#')
                {
                    x2=i;
                    y2=j;
                    flag=1;
                    break;
                }
        }
        flag=0;
        a=x2-x1;
        b=y2-y1;
        if(a<0)
            a=-a;
        if(b<0)
            b=-b;
        if(a!=b)
            flag=1;
        if(flag==0)
        {
            for(i=x1; flag==0&&i<=x2; i++)
                for(j=y1; j<=y2; j++)
                    if(arr[i][j]!='#')
                    {
                        flag=1;
                        break;
                    }
                    else arr[i][j]='.';
        }
        if(flag==0)
        {
            for(i=1; flag==0&&i<=n; i++)
                for(j=1; j<=n; j++)
                    if(arr[i][j]=='#')
                    {
                        flag=1;
                        break;
                    }
        }
        if(flag==0)
            fprintf(write,"Case #%d: YES\n",p);
        else
            fprintf(write,"Case #%d: NO\n",p);
    }
    fclose(read);
    fclose(write);
    return 0;
}

