#include<stdio.h>
#include<math.h>
double r, I, E;
int R, N;
double PI = 2 * acos(0.0);
void radius(void)
{
if (N < 3)
{
r = (double)R / N;
return;
}
double num = sin(2.0 / N * PI);
double denom = 2 * sin((N - 2) / (2.0 * N) * PI) + sin((2.0 / N) * PI);
r = R * num / denom;
}
void internal(void)
{
if (N < 3)
{
I = 0;
return;
}
I = N * r * sqrt(R * R - 2 * r * R) - 0.5 * PI * r * r * (N - 2.0);
}
void external(void)
{
E = PI * R * R - N * PI * r * r - I;
}

int main(int argc, char **argv)
{
while (scanf("%d%d",&R,&N)!=EOF)
{
radius();
internal();
external();
printf("%.10lf %.10lf %.10lf\n", r, I, E);
}
return 0;
}
