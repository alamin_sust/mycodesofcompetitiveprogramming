/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

ll t,p,i,j,n,tw[110],arr[100010],res,M=1000000007LL;

void fc(void)
{
    tw[0]=2LL;
    for(ll ii=1LL;ii<=40LL;ii++)
    {
        tw[ii]=(tw[ii-1]*tw[ii-1])%M;
    }
    return;
}

ll pw(ll x)
{
    ll ret=1LL;
    for(ll ii=0LL;x>0;ii++)
    {
        if(x%2LL)
        {
            ret=(ret*tw[ii])%M;
        }
        x/=2LL;
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %lld",&t);
    fc();
    for(p=1LL;p<=t;p++)
    {
        scanf(" %lld",&n);
        for(i=1LL;i<=n;i++)
        {
            scanf(" %lld",&arr[i]);
        }

        sort(arr+1LL,arr+n+1LL);

        res=0LL;
        for(i=1LL;i<=n;i++)
        {
            for(j=i+1LL;j<=n;j++)
            {
                res=(res + ( ((arr[j]-arr[i])*(pw(j-i-1LL)))%M) )%M;
            }
        }


        printf("%lld\n",res);
    }


    return 0;
}





