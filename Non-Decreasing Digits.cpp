#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 99999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int rr[]={2,1,2,1,-2,-1,-2,-1};

int dp[12][66],res[66];

void rec(void)
{
    int i,j;
    for(i=1;i<=64;i++)
        dp[1][i]=1;
    for(i=1;i<=10;i++)
        dp[i][1]=1;

    for(i=2;i<=10;i++)
    {
        for(j=2;j<=64;j++)
            dp[i][j]=dp[i-1][j]+dp[i][j-1];
    }
    for(i=1;i<=64;i++)
        for(j=1;j<=10;j++)
           res[i]+=dp[j][i];
    return;
}

main()
{
    int tc,in,i,t;
    rec();
    cin>>t;
    for(i=1;i<=t;i++)
    {
        cin>>tc>>in;
        printf("%d %d\n",tc,res[in]);
    }
    return 0;
}

