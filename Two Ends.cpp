/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int dp[1010][1010],arr[1010];

int rec(int pos1,int pos2)
{
    //printf("%d %d...\n",pos1,pos2);
    if(pos2<pos1)
        return 0;
    int &ret=dp[pos1][pos2];
    if(ret!=-1)
        return ret;
    ret=0;
    if(pos1<pos2)
    {
        if(arr[pos1+1]>arr[pos2])
        {
            ret=max(ret,rec(pos1+2,pos2)+arr[pos1]);
        }
        else if(arr[pos1+1]==arr[pos2])
        {
            ret=max(ret,rec(pos1+2,pos2)+arr[pos1]);
            //ret=max(ret,rec(pos1+1,pos2-1)+arr[pos1]);
        }
        else
        {
            ret=max(ret,rec(pos1+1,pos2-1)+arr[pos1]);
        }
        if(arr[pos1]>arr[pos2-1])
        {
            ret=max(ret,rec(pos1+1,pos2-1)+arr[pos2]);
        }
        else if(arr[pos1]==arr[pos2-1])
        {
            ret=max(ret,rec(pos1+1,pos2-1)+arr[pos2]);
            //ret=max(ret,rec(pos1,pos2-2)+arr[pos2]);
        }
        else
        {
            ret=max(ret,rec(pos1,pos2-2)+arr[pos2]);
        }
    }
    //ret=max(ret,rec(pos1+1,pos2-1)+arr[pos1]);
    //ret=max(ret,rec(pos1+1,pos2-1)+arr[pos2]);
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    int p=1,res,n;

    while(scanf("%d",&n)!=EOF&&n)
    {
        res=0;
        for(int i=0;i<n;i++)
        {
            scanf(" %d",&arr[i]);
            res+=arr[i];
        }
        memset(dp,-1,sizeof(dp));

        printf("In game %d, the greedy strategy might lose by as many as %d points.\n",p++,2*rec(0,n-1)-res);
    }

    return 0;
}

