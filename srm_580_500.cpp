#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class EelAndRabbit
{
public:
	int getmax(vector <int> l, vector <int> t)
	{
	    int max1=0,max2=0,i,res,from[110],to[110];
	    for(i=1;l.empty()==0;i++)
        {
            from[i]=l.back();
            l.pop_back();
        }
        for(i=1;t.empty()==0;i++)
        {
            to[i]=from[i]+t.back();
            from[i]=t.back();
            t.pop_back();
            printf("%d %d\n",from[i],to[i]);
        }
        k=i;
        for(i=1;i<k;i++)
        {
            flag[i]=1;
            for(j=1;j<k;j++)
            {
                if(i!=j)
                {
                   if(from[j]>=from[i]&&to[j]<=from[i])
                   {
                       if(flag[j]==0)
                       {temp1++;
                       flag1[j]=1;}
                   }
                }
            }
            for(j=1;j<k;j++)
            {
                if(i!=j)
                {
                   if(from[j]>=to[i]&&to[j]<=to[i])
                   {
                       if(flag[j]==0)
                       {temp2++;
                       flag2[j]=1;}
                   }
                }
            }
            if(temp1>max1)

        }
	    return res;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	EelAndRabbit objectEelAndRabbit;

	//test case0
	vector <int> param00;
	param00.push_back(2);
	param00.push_back(4);
	param00.push_back(3);
	param00.push_back(2);
	param00.push_back(2);
	param00.push_back(1);
	param00.push_back(10);
	vector <int> param01;
	param01.push_back(2);
	param01.push_back(6);
	param01.push_back(3);
	param01.push_back(7);
	param01.push_back(0);
	param01.push_back(2);
	param01.push_back(0);
	int ret0 = objectEelAndRabbit.getmax(param00,param01);
	int need0 = 6;
	assert_eq(0,ret0,need0);

	//test case1
	vector <int> param10;
	param10.push_back(1);
	param10.push_back(1);
	param10.push_back(1);
	vector <int> param11;
	param11.push_back(2);
	param11.push_back(0);
	param11.push_back(4);
	int ret1 = objectEelAndRabbit.getmax(param10,param11);
	int need1 = 2;
	assert_eq(1,ret1,need1);

	//test case2
	vector <int> param20;
	param20.push_back(1);
	vector <int> param21;
	param21.push_back(1);
	int ret2 = objectEelAndRabbit.getmax(param20,param21);
	int need2 = 1;
	assert_eq(2,ret2,need2);

	//test case3
	vector <int> param30;
	param30.push_back(8);
	param30.push_back(2);
	param30.push_back(1);
	param30.push_back(10);
	param30.push_back(8);
	param30.push_back(6);
	param30.push_back(3);
	param30.push_back(1);
	param30.push_back(2);
	param30.push_back(5);
	vector <int> param31;
	param31.push_back(17);
	param31.push_back(27);
	param31.push_back(26);
	param31.push_back(11);
	param31.push_back(1);
	param31.push_back(27);
	param31.push_back(23);
	param31.push_back(12);
	param31.push_back(11);
	param31.push_back(13);
	int ret3 = objectEelAndRabbit.getmax(param30,param31);
	int need3 = 7;
	assert_eq(3,ret3,need3);

}

