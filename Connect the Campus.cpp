#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1))
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct data{
ll x,y;
};

data node[1010];

struct data2{
ll x,y,val;
};

data2 adj;

priority_queue<data2>pq;

bool operator<(data2 a,data2 b)
{
    return a.val>b.val;
}

ll par[1010];

main()
{
    ll n,i,j,m;
    while(scanf("%lld",&n)!=EOF&&n)
    {
        for(i=1;i<=n;i++)
        scanf(" %lld %lld",&node[i].x,&node[i].y);
        for(i=1;i<=n;i++)
            for(j=1;j<=n;j++)
            {
                adj.x=i;
                adj.y=j;
                adj.val=dist(node[i].x,node[i].y,node[j].x,node[j].y);
                pq.push(adj);
            }
        scanf(" %lld",&m);
        for(i=1;i<=m;i++)
        {
            SFd2(adj.x,adj.y);
            adj.val=-1;
            pq.push(adj);
        }
        for(i=1;i<=n;i++)
            par[i]=i;
        double res=0;
        for(i=1;!pq.empty();i++)
        {
            //printf("%lld\n",pq.top().val);
            //pq.pop();
            ll u=pq.top().x;
            ll v=pq.top().y;
            ll cost=pq.top().val;
            pq.pop();
            if(u==v)
                continue;
            while(par[u]!=u)
            {
                u=par[u];
            }
            while(par[v]!=v)
            {
                v=par[v];
            }
            if(i<=m)
                par[u]=v;
            else if(u!=v)
                {par[u]=v;
                //printf("..%lld\n",cost);
                res+=sqrt(cost);}
        }
        printf("%.2lf\n",res);
    }
    return 0;
}

