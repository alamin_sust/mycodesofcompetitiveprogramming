#include<stdio.h>
main()
{
    int n,i,b,g,extra,min,k,det,p;
    FILE *rr1;
    FILE *ww1;
    rr1=fopen("input.txt","r+");
    ww1=fopen("output.txt","w+");
    fscanf(rr1," %d %d",&b,&g);
    if(b>g)
    {min=(b-1)/g;
    det=1;
    extra=(b+g-1)%(min+1);}
    else if(g>b)
    {min=(g-1)/b;
    det=2;
    extra=(b+g-1)%(min+1);
    }
    else
    {min=g/b;
    det=1;
    extra=(b+g)%(min+1);}
    if(det==1)
    {
        k=0;
        p=0;
        while(p<g)
        {
           for(i=0;k<(b+g)&&i<min;k++,i++)
           fprintf(ww1,"B");
           if(k<(b+g))
           {fprintf(ww1,"G");
           k++;
           p++;}
        }
        for(;k<(b+g);k++)
        fprintf(ww1,"B");
    }
    else if(det==2)
    {
        k=0;
        p=0;
        while(p<b)
        {
           for(i=0;k<(b+g-extra)&&i<min;k++,i++)
           fprintf(ww1,"G");
           if(k<(b+g-extra))
           {fprintf(ww1,"B");
           k++;
           p++;}
        }
        for(;k<(b+g);k++)
        fprintf(ww1,"G");
    }
    fprintf(ww1,"\n");
    fclose(ww1);
    fclose(rr1);
    return 0;
}
