#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class DecipherabilityEasy
{
public:
	string check(string s, string t)
	{

	    for(int i=0;i<s.size();i++)
        {
            string tp="";
            for(int j=0;j<s.size();j++)
            {
                if(i!=j)
                    tp+=s[j];
            }
            if(tp==t)
                return "Possible";
        }
        return "Impossible";
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	DecipherabilityEasy objectDecipherabilityEasy;

	//test case0
	string param00 = "sunuke";
	string param01 = "snuke";
	string ret0 = objectDecipherabilityEasy.check(param00,param01);
	string need0 = "Possible";
	assert_eq(0,ret0,need0);

	//test case1
	string param10 = "snuke";
	string param11 = "skue";
	string ret1 = objectDecipherabilityEasy.check(param10,param11);
	string need1 = "Impossible";
	assert_eq(1,ret1,need1);

	//test case2
	string param20 = "snuke";
	string param21 = "snuke";
	string ret2 = objectDecipherabilityEasy.check(param20,param21);
	string need2 = "Impossible";
	assert_eq(2,ret2,need2);

	//test case3
	string param30 = "snukent";
	string param31 = "snuke";
	string ret3 = objectDecipherabilityEasy.check(param30,param31);
	string need3 = "Impossible";
	assert_eq(3,ret3,need3);

	//test case4
	string param40 = "aaaaa";
	string param41 = "aaaa";
	string ret4 = objectDecipherabilityEasy.check(param40,param41);
	string need4 = "Possible";
	assert_eq(4,ret4,need4);

	//test case5
	string param50 = "aaaaa";
	string param51 = "aaa";
	string ret5 = objectDecipherabilityEasy.check(param50,param51);
	string need5 = "Impossible";
	assert_eq(5,ret5,need5);

	//test case6
	string param60 = "topcoder";
	string param61 = "tpcoder";
	string ret6 = objectDecipherabilityEasy.check(param60,param61);
	string need6 = "Possible";
	assert_eq(6,ret6,need6);

	//test case7
	string param70 = "singleroundmatch";
	string param71 = "singeroundmatc";
	string ret7 = objectDecipherabilityEasy.check(param70,param71);
	string need7 = "Impossible";
	assert_eq(7,ret7,need7);

}

