/*tic tac toe*/
#include <stdio.h>
#include <stdlib.h>
void computer(void);
void show(void);
char check(void);
void func(void);
void play(void);
char matrix[3][3];

int main(void)
{
char done;
while(1)
{
printf("Tic Tac Toe.\n\n\n");

done = ' ';
func();

do {
show();
play();
done = check();
if(done!= ' ') break;
computer();
done = check();
} while(done== ' ');

if(done=='X') printf("You won!\n");
else printf("computer won!!!!\n");
show();
}

return 0;
}

void func(void)
{
int i, j;

for(i=0; i<3; i++)
for(j=0; j<3; j++) matrix[i][j] = ' ';
}

void play(void)
{
int x, y;

printf("Enter X,Y coordinates for your move: ");
scanf("%d %d", &x, &y);

x--; y--;

if(matrix[x][y]!= ' '){
printf("Invalid move, try again.\n");
play();
}
else matrix[x][y] = 'X';
}


void computer(void)
{
int i, j;
for(i=0; i<3; i++){
for(j=0; j<3; j++)
{
if(matrix[0][0]=='0'&&matrix[0][1]=='0'&&matrix[0][2]==' ')
{
i=0;
j=2;
break;
}

else if(matrix[1][0]=='0'&&matrix[1][1]=='0'&&matrix[1][2]==' ')
{
i=1;
j=2;
break;
}
else if(matrix[2][0]=='0'&&matrix[2][1]=='0'&&matrix[2][2]==' ')
{
i=2;
j=2;
break;
}

else if(matrix[0][0]=='0'&&matrix[0][2]=='0'&&matrix[0][1]==' ')
{
i=0;
j=1;
break;
}
else if(matrix[0][2]=='0'&&matrix[0][1]=='0'&&matrix[0][0]==' ')
{
i=0;
j=0;
break;
}
else if(matrix[1][0]=='0'&&matrix[1][2]=='0'&&matrix[1][1]==' ')
{
i=1;
j=1;
break;
}
else if(matrix[1][2]=='0'&&matrix[1][1]=='0'&&matrix[1][0]==' ')
{
i=1;
j=0;
break;
}
else if(matrix[1][1]=='0'&&matrix[1][2]=='0'&&matrix[1][0]==' ')
{
i=1;
j=0;
break;
}
else if(matrix[2][0]=='0'&&matrix[2][2]=='0'&&matrix[2][1]==' ')
{
i=2;
j=1;
break;
}
else if(matrix[2][1]=='0'&&matrix[2][2]=='0'&&matrix[2][0]==' ')
{
i=2;
j=0;
break;
}


else if(matrix[0][0]=='0'&&matrix[1][0]=='0'&&matrix[2][0]==' ')
{
i=2;
j=0;
break;
}
else if(matrix[0][1]=='0'&&matrix[1][1]=='0'&&matrix[2][1]==' ')
{
i=2;
j=1;
break;
}
else if(matrix[0][2]=='0'&&matrix[1][2]=='0'&&matrix[2][2]==' ')
{
i=2;
j=2;
break;
}
else if(matrix[0][0]=='0'&&matrix[2][0]=='0'&&matrix[1][0]==' ')
{
i=1;
j=0;
break;
}
else if(matrix[2][0]=='0'&&matrix[1][0]=='0'&&matrix[0][0]==' ')
{
i=0;
j=0;
break;
}
else if(matrix[0][1]=='0'&&matrix[2][1]=='0'&&matrix[1][1]==' ')
{
i=1;
j=1;
break;
}
else if(matrix[2][1]=='0'&&matrix[1][1]=='0'&&matrix[0][1]==' ')
{
i=0;
j=1;
break;
}
else if(matrix[0][2]=='0'&&matrix[2][2]=='0'&&matrix[1][2]==' ')
{
i=1;
j=2;
break;
}
else if(matrix[1][2]=='0'&&matrix[2][2]=='0'&&matrix[0][2]==' ')
{
i=0;
j=2;
break;
}
else if(matrix[0][2]=='0'&&matrix[1][1]=='0'&&matrix[2][0]==' ')
{
i=2;
j=0;
break;
}

else if(matrix[1][1]=='0'&&matrix[2][0]=='0'&&matrix[0][2]==' ')
{
i=0;
j=2;
break;
}

else if(matrix[2][0]=='0'&&matrix[0][2]=='0'&&matrix[1][1]==' ')
{
i=1;
j=1;
break;
}

else if(matrix[0][0]=='0'&&matrix[1][1]=='0'&&matrix[2][2]==' ')
{
i=2;
j=2;
break;
}

else if(matrix[0][0]=='0'&&matrix[2][2]=='0'&&matrix[1][1]==' ')
{
i=1;
j=1;
break;
}
else if(matrix[2][2]=='0'&&matrix[1][1]=='0'&&matrix[0][0]==' ')
{
i=2;
j=2;
break;
}

else if(matrix[0][0]=='0'&&matrix[0][1]=='0'&&matrix[0][2]==' ')
{
i=0;
j=2;
break;
}
else if(matrix[0][2]=='X'&&matrix[1][1]=='X'&&matrix[2][0]==' ')
{
i=2;
j=0;
break;
}

else if(matrix[1][1]=='X'&&matrix[2][0]=='X'&&matrix[0][2]==' ')
{
i=0;
j=2;
break;
}

else if(matrix[2][0]=='X'&&matrix[0][2]=='X'&&matrix[1][1]==' ')
{
i=1;
j=1;
break;
}

else if(matrix[0][0]=='X'&&matrix[1][1]=='X'&&matrix[2][2]==' ')
{
i=2;
j=2;
break;
}

else if(matrix[0][0]=='X'&&matrix[2][2]=='X'&&matrix[1][1]==' ')
{
i=1;
j=1;
break;
}
else if(matrix[2][2]=='X'&&matrix[1][1]=='X'&&matrix[0][0]==' ')
{
i=2;
j=2;
break;
}

else if(matrix[0][0]=='X'&&matrix[0][1]=='X'&&matrix[0][2]==' ')
{
i=0;
j=2;
break;
}

else if(matrix[0][0]=='X'&&matrix[0][1]=='X'&&matrix[0][2]==' ')
{
i=0;
j=2;
break;
}

else if(matrix[1][0]=='X'&&matrix[1][1]=='X'&&matrix[1][2]==' ')
{
i=1;
j=2;
break;
}
else if(matrix[2][0]=='X'&&matrix[2][1]=='X'&&matrix[2][2]==' ')
{
i=2;
j=2;
break;
}

else if(matrix[0][0]=='X'&&matrix[0][2]=='X'&&matrix[0][1]==' ')
{
i=0;
j=1;
break;
}
else if(matrix[0][2]=='X'&&matrix[0][1]=='X'&&matrix[0][0]==' ')
{
i=0;
j=0;
break;
}
else if(matrix[1][0]=='X'&&matrix[1][2]=='X'&&matrix[1][1]==' ')
{
i=1;
j=1;
break;
}
else if(matrix[1][2]=='X'&&matrix[1][1]=='X'&&matrix[1][0]==' ')
{
i=1;
j=0;
break;
}
else if(matrix[1][1]=='X'&&matrix[1][2]=='X'&&matrix[1][0]==' ')
{
i=1;
j=0;
break;
}
else if(matrix[2][0]=='X'&&matrix[2][2]=='X'&&matrix[2][1]==' ')
{
i=2;
j=1;
break;
}
else if(matrix[2][1]=='X'&&matrix[2][2]=='X'&&matrix[2][0]==' ')
{
i=2;
j=0;
break;
}


else if(matrix[0][0]=='X'&&matrix[1][0]=='X'&&matrix[2][0]==' ')
{
i=2;
j=0;
break;
}
else if(matrix[0][1]=='X'&&matrix[1][1]=='X'&&matrix[2][1]==' ')
{
i=2;
j=1;
break;
}
else if(matrix[0][2]=='X'&&matrix[1][2]=='X'&&matrix[2][2]==' ')
{
i=2;
j=2;
break;
}
else if(matrix[0][0]=='X'&&matrix[2][0]=='X'&&matrix[1][0]==' ')
{
i=1;
j=0;
break;
}
else if(matrix[2][0]=='X'&&matrix[1][0]=='X'&&matrix[0][0]==' ')
{
i=0;
j=0;
break;
}
else if(matrix[0][1]=='X'&&matrix[2][1]=='X'&&matrix[1][1]==' ')
{
i=1;
j=1;
break;
}
else if(matrix[2][1]=='X'&&matrix[1][1]=='X'&&matrix[0][1]==' ')
{
i=0;
j=1;
break;
}
else if(matrix[0][2]=='X'&&matrix[2][2]=='X'&&matrix[1][2]==' ')
{
i=1;
j=2;
break;
}
else if(matrix[1][2]=='X'&&matrix[2][2]=='X'&&matrix[0][2]==' ')
{
i=0;
j=2;
break;
}

else if(matrix[i][j]==' ')
{
break;

}
}
if(matrix[i][j]==' ') break;

}



matrix[i][j] = 'O';
if(i*j==9){
printf("draw\n");
exit(0);
}
}


void show(void)
{
int t;

for(t=0; t<3; t++) {
printf(" %c | %c | %c ",matrix[t][0],
matrix[t][1], matrix [t][2]);
if(t!=2) printf("\n---|---|---\n");
}
printf("\n");
}


char check(void)
{
int i;

for(i=0; i<3; i++)
if(matrix[i][0]==matrix[i][1] &&
matrix[i][0]==matrix[i][2]) return matrix[i][0];

for(i=0; i<3; i++)
if(matrix[0][i]==matrix[1][i] &&
matrix[0][i]==matrix[2][i]) return matrix[0][i];

if(matrix[0][0]==matrix[1][1] &&
matrix[1][1]==matrix[2][2])
return matrix[0][0];

if(matrix[0][2]==matrix[1][1] &&
matrix[1][1]==matrix[2][0])
return matrix[0][2];

return ' ';
}
