/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int n;
vector<int>v;
map<int,int>mpp;

void rec(int pos,int num)
{
    if(pos==9)
    {
        //printf("%d\n",num);
        if(num)
        v.push_back(num);
        return;
    }
    rec(pos+1,num);
    rec(pos+1,num*10+4);
    rec(pos+1,num*10+7);
    return;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);


    rec(0,0);
    sort(v.begin(),v.end());
    int k=1;
    for(int i=0;i<v.size();i++)
    {
        if(mpp[v[i]]==0)
        mpp[v[i]]=k++;

    }



    while(cin>>n)
    cout<<mpp[n]<<endl;

    return 0;
}





