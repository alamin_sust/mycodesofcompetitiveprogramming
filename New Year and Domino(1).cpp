/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int i,j,r,c,r1,c1,r2,c2,res,row[510][510],col[510][510],q,x,y;
char arr[510][510];

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);

    scanf(" %d %d",&r,&c);
    getchar();

    for(i=1;i<=r;i++)
    {
        for(j=1;j<=c;j++)
        {
            scanf("%c",&arr[i][j]);
        }
        getchar();
    }

    for(i=1;i<=r;i++)
    {
        for(j=1;j<=c;j++)
        {
            if(arr[i][j]=='.')
            {
                if(arr[i][j+1]=='.')
                    col[i][j]++;
                if(arr[i+1][j]=='.')
                    row[i][j]++;
            }
        }
    }

    for(i=1;i<=r;i++)
    {
        for(j=1;j<=c;j++)
        {
            col[i][j]+=(col[i-1][j]+col[i][j-1]-col[i-1][j-1]);
            row[i][j]+=(row[i-1][j]+row[i][j-1]-row[i-1][j-1]);
        }
    }

    scanf(" %d",&q);

    for(i=1;i<=q;i++)
    {
        scanf(" %d %d %d %d",&r1,&c1,&r2,&c2);
        x=r2;
        y=c2-1;
        res=col[x][y]-col[r1-1][y]-col[x][c1-1]+col[r1-1][c1-1];

        x=r2-1;
        y=c2;
        res+=row[x][y]-row[r1-1][y]-row[x][c1-1]+row[r1-1][c1-1];

        printf("%d\n",res);
    }

    return 0;
}

