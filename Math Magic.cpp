/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int fact[1002],cnt;
int dp[2][1002][1002],n,m,k,MOD=1000000007,LCM[1010][1010];

int solve_it(void)
{
    int curr=0;
    memset(dp[curr], 0, sizeof(dp[curr]));

    for(int i=0; i<cnt; i++)
        dp[0][fact[i]][fact[i]]=1;


    for(int p=1; p<k; p++)
    {
        curr^=1;
        for(int i=1; i<=n; i++)
            for(int j=0; j<cnt; j++)
                dp[curr][i][fact[j]]=0;

        for(int i=1; i<=n; i++)
        {

            for(int j=0; j<cnt; j++)
            {
                if(dp[curr^1][i][fact[j]]>0)
                {
                    for(int x=0; x<cnt; x++)
                    {
                        if(i+fact[x]>n)
                            continue;
                        dp[curr][i+fact[x]][LCM[fact[j]][fact[x]]]=(dp[curr][i+fact[x]][LCM[fact[j]][fact[x]]]+dp[curr^1][i][fact[j]])%MOD;
                    }
                }
            }
        }
    }
    return dp[curr][n][m];
}

int main()
{
    for(int i=1; i<=1000; i++)
        for(int j=i; j<=1000; j++)
        {
            LCM[i][j]=LCM[j][i]=i/__gcd(i,j)*j;
        }
    while(cin>>n>>m>>k)
    {
        cnt=0;
        for(int i = 1; i<=m; i++)
        {
            if((m%i)==0)
                fact[cnt++]=i;
        }
        printf("%d\n",solve_it());

    }
    return 0;
}

