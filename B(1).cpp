#include<stdio.h>
#include<string.h>
#include<math.h>
#include<algorithm>
#include<stack>
#include<queue>
#include<map>
#include<set>
#include<vector>
#include<iostream>
#define ull long long unsigned
#define pi (2*acos(0))
#define inf 99999999
#define eps 0.0000001
#define ll long long
#define dist(x1,y1,x2,y2) sqrt(((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1)))
using namespace std;

ll M=100009LL,l,dp[1005][1005];
char arr[1010];

ll rec(ll lev,ll taken)
{
    if(taken==l||lev==l)
        return 1;
    ll &ret=dp[lev][taken];
    if(ret!=-1)
        return ret;
    ret=0LL;
    //ret=(ret+rec(lev+1,taken))%M;
    for(ll i=taken;i<l;i++)
    {
        if(i==0)
            ret=(ret+rec(lev+1,i+1))%M;
        else if(arr[taken-1]!=arr[i])
            ret=(ret+rec(lev+1,i+1))%M;
    }
    return ret%M;
}

main()
{
    ll t,i,p,res;
    scanf(" %I64d",&t);
    getchar();
    for(p=1;p<=t;p++)
    {
        gets(arr);
        l=strlen(arr);
        memset(dp,-1,sizeof(dp));
        res=(rec(0,0)+1)%M;
        printf("Case %I64d: %I64d\n",p,res);
    }
    return 0;
}

