#include <sstream>
#include <queue>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class SilverbachConjecture
{
public:
	vector <int> solve(int n)
	{

	    int f1=0,f2=0;

	    vector<int>vc;
	    for(int i=4;i<=2000;i++)
        {
            f1=f2=0;
            for(int j=2;j<i;j++)
            {
                if((i%j)==0)
                {
                    f1=1;
                    break;
                }
            }
            for(int j=2;j<(n-i);j++)
            {
                if(((n-i)%j)==0)
                {
                    f2=1;
                    break;
                }
            }
            if(f1==1&&f2==1)
            {
                vc.push_back(i);
                vc.push_back(n-i);
                break;
            }
        }
        return vc;
	}
};


template<typename T> void print( T a ) {
    cerr << a;
}

void print( long long a ) {
    cerr << a << "L";
}

void print( string a ) {
    cerr << '"' << a << '"';
}

template<typename T> void print( vector<T> a ) {
    cerr << "{";
    for ( int i = 0 ; i != a.size() ; i++ ) {
        if ( i != 0 ) cerr << ", ";
        print( a[i] );
    }
    cerr << "}" << endl;
}

template<typename T> void assert_eq( int n, T have, T need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

template<typename T> void assert_eq( int n, vector<T> have, vector<T> need ) {
    if( have.size() != need.size() ) {
        cerr << "Case " << n << " failed: returned " << have.size() << " elements; expected " << need.size() << " elements.";
        print( have );
        print( need );
        return;
    }
    for( int i= 0; i < have.size(); i++ ) {
        if( have[i] != need[i] ) {
            cerr << "Case " << n << " failed. Expected and returned array differ in position " << i << ".";
            print( have );
            print( need );
            return;
        }
    }
    cerr << "Case " << n << " passed." << endl;
}
void assert_eq( int n, string have, string need ) {
    if ( have == need ) {
        cerr << "Case " << n << " passed." << endl;
    } else {
        cerr << "Case " << n << " failed: expected ";
        print( need );
        cerr << " received ";
        print( have );
        cerr << "." << endl;
    }
}

int main( int argc, char* argv[] )
{

	SilverbachConjecture objectSilverbachConjecture;

	//test case0
	int param00 = 20;
	vector <int> ret0 = objectSilverbachConjecture.solve(param00);
	vector <int> need0;
	need0.push_back(8);
	need0.push_back(12);
	assert_eq(0,ret0,need0);

	//test case1
	int param10 = 30;
	vector <int> ret1 = objectSilverbachConjecture.solve(param10);
	vector <int> need1;
	need1.push_back(15);
	need1.push_back(15);
	assert_eq(1,ret1,need1);

	//test case2
	int param20 = 999;
	vector <int> ret2 = objectSilverbachConjecture.solve(param20);
	vector <int> need2;
	need2.push_back(699);
	need2.push_back(300);
	assert_eq(2,ret2,need2);

	//test case3
	int param30 = 45;
	vector <int> ret3 = objectSilverbachConjecture.solve(param30);
	vector <int> need3;
	need3.push_back(15);
	need3.push_back(30);
	assert_eq(3,ret3,need3);

}
