/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

int k,dp[103][10][10][2],t,p,l,M=1000000007,res;
char arr[110];

int rec(int pos,int now,int prev,int flag)
{
    //printf("%d %d %d\n",pos,prev,ek);
    if(pos==l)
        return 0;
    int &ret=dp[pos][now][prev][flag];
    if(ret!=-1)
        return ret;
    ret=0;
    if(now==1&&prev==2)
        ret=(ret+1)%M;
    int i=0;
    for(i=0; i<arr[pos]-'0'; i++)
    {
        if(prev==2&&i==1)
           ret=(0+((ret+rec(pos+1,i,now,1))%M))%M;
        else
            ret=(ret+rec(pos+1,i,now,1))%M;
    }
    if(flag==0)
    {
        if(prev==2&&i==1)
            ret=(0+((ret+rec(pos+1,i,now,0))%M))%M;
        else
            ret=(ret+rec(pos+1,i,now,0))%M;
    }
    else
    {
        for(; i<=9; i++)
        {
            if(prev==2&&i==1)
                ret=(0+((ret+rec(pos+1,i,now,1))%M))%M;
            else
                ret=(ret+rec(pos+1,i,now,1))%M;
        }
    }
    return ret;
}

int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    cin>>t;

    for(p=1; p<=t; p++)
    {
        scanf(" %s",&arr);
        l=strlen(arr);
        k=0;
        for(int i=1;i<l;i++)
        {
            if(arr[i]=='1'&&arr[i-1]=='2')
                k++;
        }
        memset(dp,-1,sizeof(dp));
        res=rec(0,0,0,0);
        scanf(" %s",&arr);
        l=strlen(arr);
        memset(dp,-1,sizeof(dp));
        res=rec(0,0,0,0)-res;
        if(res<0)
            res=(res+M)%M;
        res=(res+k)%M;
        printf("%d\n",res);
    }
    return 0;
}




