#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll seg[1000010],child_upd[1000010];

void build(ll ind,ll f,ll t)
{
    if(f==t)
        {seg[ind]=0;
        child_upd[ind]=0;
        return;}
    ll left=ind*2;
    ll right=left+1;
    ll mid=(f+t)/2;
    build(left,f,mid);
    build(right,mid+1,t);
    child_upd[ind]=0;
    seg[ind]=seg[left]+seg[right];
    return;
}

void update(ll ind,ll f,ll t,ll ul,ll ur,ll val)
{
    //printf("..");
    if(ul>t||ur<f)
        return;
    if(f>=ul&&t<=ur)
    {
        seg[ind]+=val*(t-f+1);
        child_upd[ind]+=val;
        return;
    }
   ll left=ind*2;
   ll right=ind*2+1;
   ll mid=(f+t)/2;
    update(left,f,mid,ul,ur,val);
    update(right,mid+1,t,ul,ur,val);
    seg[ind]=seg[left]+seg[right]+(child_upd[ind]*(t-f+1));
    return;
}

ll query(ll ind,ll f,ll t,ll ql,ll qr,ll carry)
{
    if(ql>t||qr<f)
        return 0;
    if(ql<=f&&qr>=t)
    {
        return seg[ind]+carry*(t-f+1);
    }
    ll left=ind*2;
    ll right=ind*2+1;
    ll mid=(f+t)/2;
    return query(left,f,mid,ql,qr,carry+child_upd[ind])+query(right,mid+1,t,ql,qr,carry+child_upd[ind]);
}


main()
{
    ll t,p,i,com,n,q,from,val,to;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        cin>>n>>q;
        build(1,1,n);
        for(i=1;i<=q;i++)
        {
            cin>>com;
            if(com==0)
            {
                cin>>from>>to>>val;
                update(1,1,n,from,to,val);
            }
            else
            {
                cin>>from>>to;
                printf("%lld\n",query(1,1,n,from,to,0));
            }
        }
    }
    return 0;
}

