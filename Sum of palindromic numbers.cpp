/*
Author: Md. Al- Amin
CSE, SUST
*/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#include<map>
#include<queue>
#include<stack>
#include<vector>
#define ll long long
#define inf 1000000010
#define pi (2*acos(0))
//int rr[]={0,0,-1,1};
//int cc[]={-1,1,0,0};
using namespace std;

ll arr[100010],t,p,l,r;

ll fnc(ll num)
{
    ll i,k=0LL,tp[10];
    while(num)
    {
        tp[k++]=num%10LL;
        num/=10LL;
    }
    for(ll i=0LL,j=k-1LL;i<=j;i++,j--)
    {
        if(tp[i]!=tp[j])
            return 0LL;
    }
    return 1LL;
}

int main()
{
    ll i;
    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);

    for(i=1LL;i<=100000LL;i++)
    {
        if(fnc(i))
            arr[i]=i;
        arr[i]+=arr[i-1];
    }

    scanf(" %lld",&t);

    for(p=1;p<=t;p++)
    {
        scanf(" %lld %lld",&l,&r);
        printf("%lld\n",arr[r]-arr[l-1]);
    }

    return 0;
}

