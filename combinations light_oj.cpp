#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define MOD 1000003
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll f[1000010];

ll mod_inv(ll a,ll b)
{
    if(b==1)
        return a;
    if(b%2)
    {
        return (mod_inv(a,b-1)*a)%MOD;
    }
    else
    {
        ll r=mod_inv(a,b/2);
        return (r*r)%MOD;
    }

}

main()
{
    ll i,p,t,d,n,res,r;
    f[1]=1;
    for(i=2;i<=1000000;i++)
        f[i]=(f[i-1]*i)%MOD;
    cin>>t;
    for(p=1;p<=t;p++)
    {
        scanf(" %lld %lld",&n,&r);
        if(n==r||r==0)
            {printf("Case %lld: 1\n",p);
            continue;}
        d=(f[r]*f[n-r])%MOD;
        res=(f[n]*mod_inv(d,MOD-2))%MOD;
        printf("Case %lld: %lld\n",p,res);
    }
    return 0;
}

