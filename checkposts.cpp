#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll arr[100100],val[100010],index[100010],ind,res=1LL,res2,lowlink[100010];
stack<ll>stk;
map<ll,ll>mpp;
vector<ll>adj[100010];

void tarjan(ll v)
{
    index[v]=ind;
    lowlink[v]=ind;
    ind++;
    mpp[v]=1;
    stk.push(v);
    for(ll i=0; i<adj[v].size(); i++)
    {
        ll w=adj[v][i];
        if(index[w]==0)
        {
            tarjan(w);
            //printf("%d..%d--%d**%d\n",lowlink[v],lowlink[w],v,w);
            lowlink[v]=min(lowlink[v],lowlink[w]);

        }
        else if(mpp[w]==1)
        {
            //printf("%d.%d-%d*%d\n",lowlink[v],index[w],v,w);
            lowlink[v]=min(lowlink[v],index[w]);
        }
    }
    if(index[v]==lowlink[v])
    {
        ll ct=0,i;
        while(!stk.empty())
        {
            mpp[stk.top()]=0;
            arr[ct++]=val[stk.top()];
            if(stk.top()==v)
            {
                stk.pop();
                break;
            }
            stk.pop();
        }
        sort(arr,arr+ct);
        for(i=1; i<ct; i++)
        {
            if(arr[i]!=arr[i-1])
                break;
        }
        res2+=arr[0];
        res=(res*i)%1000000007LL;
    }
    return;
}

main()
{
    ll n,e,i,v,w;
    cin>>n;
    for(i=1; i<=n; i++)
    {
        cin>>val[i];
        lowlink[i]=0;
        index[i]=0;
    }
    cin>>e;
    for(i=1; i<=e; i++)
        cin>>v>>w,adj[v].push_back(w);
    for(i=1; i<=n; i++)
    {
        if(index[i]==0)
            ind=i,tarjan(i);
    }
    cout<<res2<<" "<<res<<endl;
    return 0;
}
