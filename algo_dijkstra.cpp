#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node
{
    int x,val;
};

bool operator<(node a,node b)
{
    return a.val>b.val;
}

node det;
vector<node>adj[10010];
priority_queue<node>pq;
int n,d[10010];

void dijkstra(int s)
{
    int i;
    for(i=1; i<=n; i++)
        d[i]=inf;
    d[s]=0;
    det.val=0;
    det.x=s;
    pq.push(det);
    while(!pq.empty())
    {
        int u=pq.top().x;
        pq.pop();
        for(i=0; i<adj[u].size(); i++)
        {
            int v=adj[u][i].x;
            int uv=adj[u][i].val;
            if(d[v]>d[u]+uv)
            {
                d[v]=d[u]+uv;
                det.val=d[v];
                det.x=v;
                pq.push(det);
            }
        }
    }
    return;
}

main()
{
    int e,i,f,t,v;
    cin>>n>>e;
    for(i=1; i<=e; i++)
    {
        cin>>f>>t>>v;
        det.x=t;
        det.val=v;
        adj[f].push_back(det);
    }
    dijkstra(1);
    for(i=1; i<=n; i++)
        cout<<d[i]<<" ";
    return 0;
}

/*
5 10
1 2 10
1 4 5
2 4 2
4 2 3
4 3 9
3 5 4
5 3 6
5 1 7
4 5 2
2 3 1
*/
