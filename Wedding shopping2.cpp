/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int dp[202][22],n,arr[22][22],money,m[22];

int rec(int rem,int num)
{
    if(num<0)
        return money-rem;
    int &ret=dp[rem][num];
    if(ret!=-1)
        return ret;
    ret=0;
    for(int i=0;i<m[num];i++)
    {
        if(rem>=arr[num][i])
        ret=max(ret,rec(rem-arr[num][i],num-1));
    }
    return ret;
}

main()
{
    int t,i,res,p,j;
    scanf(" %d",&t);
    for(p=1;p<=t;p++)
    {
        memset(dp,-1,sizeof(dp));
        scanf(" %d %d",&money,&n);
        for(i=0;i<n;i++)
            {
                scanf(" %d",&m[i]);
                for(j=0;j<m[i];j++)
                    scanf(" %d",&arr[i][j]);
            }
        res=rec(money,n-1);
        if(res)
        {
            printf("%d\n",res);
        }
        else
        {
            printf("no solution\n");
        }
    }
    return 0;
}

