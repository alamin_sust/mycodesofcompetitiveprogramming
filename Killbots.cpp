/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define int_max 2147483647
#define int_min -2147483648
#define long_max 9223372036854775807
#define long_min -9223372036854775808
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define mem(arr,val) memset(arr,val,sizeof(arr))

using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};
//int rr[]={0,0,1,-1};
//int cc[]={-1,1,0,0};

struct node
{
    int x,y;
};

node det,v;
char arr[1010][1010];
queue<node>q,q2,q3;
int hx,hy,nx,ny;

int bfs(void)
{
    int ret=0,state=0;
    while((!q.empty())||(!q2.empty())||(!q3.empty()))
    {
        while(!q2.empty())
        {
            det=q2.front();
            q2.pop();
            if(arr[det.x][det.y]=='*')
                continue;
            arr[det.x][det.y]='.';
            if(det.x==hx)
            {
                nx=hx;
                ny=det.y+((det.y-hy)>0?-1:1);
            }
            else if(det.y==hy)
            {
                ny=hy;
                nx=det.x+((det.x-hx)>0?-1:1);
            }
            else
            {
                nx=det.x+((det.x-hx)>0?-1:1);
                ny=det.y+((det.y-hy)>0?-1:1);
            }
            v.x=nx;
            v.y=ny;
            if(nx==hx&&ny==hy)
                ret++;
            else if(arr[nx][ny]=='.')
            {
                arr[nx][ny]='1';
                q3.push(v);
            }
            else if(arr[nx][ny]=='1')
            {
                arr[nx][ny]='*';
            }
            else if(arr[nx][ny]=='+')
            {
                arr[nx][ny]='1';
                q3.push(v);
            }
            else if(arr[nx][ny]=='#')
            {
                arr[nx][ny]='1';
                q3.push(v);
            }
        }
        printf("%d...\n",ret);
        while(!q3.empty())
        {
            det=q3.front();
            q3.pop();
            if(arr[det.x][det.y]=='*')
                continue;
            arr[det.x][det.y]='.';
            if(det.x==hx)
            {
                nx=hx;
                ny=det.y+((det.y-hy)>0?-1:1);
            }
            else if(det.y==hy)
            {
                ny=hy;
                nx=det.x+((det.x-hx)>0?-1:1);
            }
            else
            {
                nx=det.x+((det.x-hx)>0?-1:1);
                ny=det.y+((det.y-hy)>0?-1:1);
            }
            v.x=nx;
            v.y=ny;
            if(nx==hx&&ny==hy)
                ret++;
            else if(arr[nx][ny]=='.')
            {
                arr[nx][ny]='#';
                q2.push(v);
            }
            else if(arr[nx][ny]=='#')
            {
                arr[nx][ny]='*';
            }
            else if(arr[nx][ny]=='+')
            {
                arr[nx][ny]='#';
                q2.push(v);
            }
            else if(arr[nx][ny]=='1')
            {
                arr[nx][ny]='#';
                q2.push(v);
            }
        }
        int len=q.size();
        while(len--)
        {
            det=q.front();
            q.pop();
            if(state==0)
            {
                if(arr[det.x][det.y]=='*')
                    continue;
                arr[det.x][det.y]='.';
                if(det.x==hx)
                {
                    nx=hx;
                    ny=det.y+((det.y-hy)>0?-1:1);
                }
                else if(det.y==hy)
                {
                    ny=hy;
                    nx=det.x+((det.x-hx)>0?-1:1);
                }
                else
                {
                    nx=det.x+((det.x-hx)>0?-1:1);
                    ny=det.y+((det.y-hy)>0?-1:1);
                }
                v.x=nx;
                v.y=ny;
                if(nx==hx&&ny==hy)
                    ret++;
                else if(arr[nx][ny]=='.')
                {
                    arr[nx][ny]='-';
                    q.push(v);
                }
                else if(arr[nx][ny]=='#')
                {
                    arr[nx][ny]='*';
                }
                else if(arr[nx][ny]=='-')
                {
                    arr[nx][ny]='*';
                    q.push(v);
                }
                else if(arr[nx][ny]=='+')
                {
                    arr[nx][ny]='-';
                    q.push(v);
                }
            }
            else
            {

                if(arr[det.x][det.y]=='*')
                    continue;
                arr[det.x][det.y]='.';
                if(det.x==hx)
                {
                    nx=hx;
                    ny=det.y+((det.y-hy)>0?-1:1);
                }
                else if(det.y==hy)
                {
                    ny=hy;
                    nx=det.x+((det.x-hx)>0?-1:1);
                }
                else
                {
                    nx=det.x+((det.x-hx)>0?-1:1);
                    ny=det.y+((det.y-hy)>0?-1:1);
                }
                v.x=nx;
                v.y=ny;
                if(nx==hx&&ny==hy)
                    ret++;
                else if(arr[nx][ny]=='.')
                {
                    arr[nx][ny]='+';
                    q.push(v);
                }
                else if(arr[nx][ny]=='#')
                {
                    arr[nx][ny]='*';
                }
                else if(arr[nx][ny]=='+')
                {
                    arr[nx][ny]='*';
                    q.push(v);
                }
                else if(arr[nx][ny]=='-')
                {
                    arr[nx][ny]='+';
                    q.push(v);
                }

            }

        }
        state^=1;

    }
    return ret;
}


int main()
{
    //freopen(".txt","r",stdin);
    //freopen(".txt","w",stdout);
    int i,j,robs,row,col;
    i=0;
    while(scanf("%s",&arr[i])!=EOF)
    {
        if(arr[i][0]!='$')
        {
            i++;
            continue;
        }
        row=i;
        col=strlen(arr[0]);
        robs=0;
        while(!q.empty())
            q.pop();
        while(!q2.empty())
            q2.pop();
        while(!q3.empty())
            q3.pop();
        for(i=0; i<row; i++)
        {
            for(j=0; j<col; j++)
            {
                if(arr[i][j]=='@')
                {
                    hx=i;
                    hy=j;
                }
                else if(arr[i][j]=='#')
                {
                    robs++;
                    det.x=i;
                    det.y=j;
                    q2.push(det);
                }
            }
        }
        for(i=0; i<row; i++)
        {
            for(j=0; j<col; j++)
            {
                if(arr[i][j]=='+')
                {
                    robs++;
                    det.x=i;
                    det.y=j;
                    q.push(det);
                }
            }
        }


        printf("%d\n",bfs());
        i=0;
    }

    return 0;
}

/*
...*....
.......#
.....*..
....+...
.+..@.*.
.......+
.*...+..
.+....#.
$
...
..@
.*.
.+.
$
*/

