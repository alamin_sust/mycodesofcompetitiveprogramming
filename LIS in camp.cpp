#include<stdio.h>
#include<string.h>
#include<iostream>
using namespace std;

int mem[1010],lft[1010],num[10010];

void printpath(int n)
{
    if(lft[n]!=-1)
    printpath(lft[n]);
    printf("%d ",num[n]);
    return;
}

int lis(int n)
{
    if(mem[n]==-1)
    {
        int ans=1;
        for(int i=0;i<n;i++)
        {
            if(num[i]<=num[n])
            {
                if(ans<lis(i)+1)
                {
                    ans=lis(i)+1;
                    lft[n]=i;
                }
            }
        }
        mem[n]=ans;
    }
    return mem[n];
}

main()
{
    int n,ans;
    memset(mem,-1,sizeof(mem));
    memset(lft,-1,sizeof(lft));
    scanf(" %d",&n);
    ans=0;
    for(int i=0;i<n;i++)
    {
        scanf(" %d",&num[i]);
    }
    int start;
    for(int i=0;i<n;i++)
    {
        if(ans<lis(i))
        {ans=lis(i);
        start=i;}
    }
    printf("%d\n",ans);
    printpath(start);
    return 0;
}
