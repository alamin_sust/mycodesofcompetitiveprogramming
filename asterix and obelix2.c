#include<stdio.h>
#include<limits.h>

#define infinity INT_MAX
#define MAX_VERTEX 10
#define BLACK 1
#define GRAY 2

int distance=0;

typedef struct VERTEXINFO
{
    int predecessor;
    int distance;
    int mark;
}VERTEXINFO;

int findshortestpath(int adjacency[MAX_VERTEX][MAX_VERTEX],int vertices,int source,int destination,int shortestpath[MAX_VERTEX])
{
    VERTEXINFO pathInfo[MAX_VERTEX];
    int i=0,nodes=0,sourceNode=0,minDistace=0,tempPath[MAX_VERTEX];

    /*set initial distance to zero*/

    distance=0;

    //set initially infinity to all vertices

    for(i=1;i<=vertices;i++)
    {
        pathInfo[i].predecessor=0;
        pathInfo[i].distance= infinity;
        pathInfo[i].mark=GRAY;
    }
    //initialize source vertex
        pathInfo[source].predecessor=0;
        pathInfo[source].distance=0;
        pathInfo[source].mark=BLACK;
    //find path from source vertex to destination vertex
    sourceNode= source;

    do  //until destination vertex is not found
    {
        for(i=1;i<=vertices;i++)
        {
            //check whether it is direct path or not
            if(adjacency[sourceNode][i]>0&&pathInfo[i].mark==GRAY)
            {
                if((pathInfo[sourceNode].distance+adjacency[sourceNode][i])<pathInfo[i].distance)
                {
                    pathInfo[i].predecessor=sourceNode;
                    pathInfo[i].distance=pathInfo[sourceNode].distance+adjacency[sourceNode][i];
                }
            }

        }
        minDistace=infinity;
        sourceNode=0;

        //find vertices for minimum cost

        for(i=1;i<=vertices;i++)
        {
            if((pathInfo[i].mark==GRAY)&&(pathInfo[i].distance<minDistace))
            {
                minDistace=pathInfo[i].distance;
                sourceNode=i;
            }
        }
        //check whether source or destination vertices are not isolated
        if(sourceNode==0)
        return 0;

        pathInfo[sourceNode].mark=BLACK;
    }while(sourceNode!=destination);

    //save the shortest path

    sourceNode=destination;

    do
    {
        nodes++;
        tempPath[nodes]=sourceNode;
        sourceNode= pathInfo[sourceNode].predecessor;
    }while(sourceNode!=0);

    for(i=1;i<=nodes;i++)
    {
        shortestpath[i]=tempPath[nodes-i+1];
    }

    for(i=1;i<nodes;i++)
    {
        distance+=adjacency[shortestpath[i]][shortestpath[i+1]];
    }
    return nodes;
}



int main()
{
    int pp=1;
    while(1)
    {int res,cost[MAX_VERTEX],weg,r,q,p,maximum,fr,to_,i=0,j=0,n=0,to=0,from=0,nodes=0,shortestPath[MAX_VERTEX],adjacent[MAX_VERTEX][MAX_VERTEX];

    //printf(" How many vertices?: ");
    scanf(" %d %d %d",&n,&r,&q);
    //fill the matrix
    for(i=1;i<=n;i++)
    scanf("%d",&cost[i]);
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
        {
            adjacent[i][j]=0;
        }
    }
    for(i=1;i<=r;i++)
    {
        scanf(" %d %d %d",&fr,&to_,&weg);
        adjacent[fr][to_]=weg;
        adjacent[to_][fr]=weg;
    }
    printf("Case #%d\n",pp);
    for(p=1;p<=q;p++)
    {
    //printf("\nFind shortest path:\n     From: ");
    scanf(" %d",&from);
   // printf("     To: ");
    scanf(" %d",&to);

    nodes=findshortestpath(adjacent,n,from,to,shortestPath);
    nodes=findshortestpath(adjacent,n,from,to,shortestPath);

    //print shortest path :)
    res=distance;
    if(distance!=0)
    {
       // printf("\nThe shortest path is\n");
       // printf("Go from vertex %d",shortestPath[1]);
        for(maximum=0,i=1;i<=nodes;i++)
          {if(cost[shortestPath[i]]>maximum)
          maximum=cost[shortestPath[i]];}
          printf("%d\n",res+maximum);
       // printf("\nMinimum distance : %d\n",distance);
    }
    else
    printf("-1\n");
    }
    pp++;
    }
    return 0;
}
