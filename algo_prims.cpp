#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

struct node
{
    int x,val,par;
};

struct node2
{
    int u,v,val;
};

bool operator<(node a,node b)
{
    return a.val>b.val;
}

node det;
node2 det2;
vector<node2>res;
vector<node>adj[10010];
priority_queue<node>pq;
int status[10010],n;

void prims(int s)
{
    int i;
    memset(status,0,sizeof(status));
    det.val=0;
    det.x=s;
    det.par=-1;
    pq.push(det);
    while(!pq.empty())
    {
        int u=pq.top().x;
        if(pq.top().par!=-1&&status[u]==0)
        {
            det2.val=pq.top().val;
            det2.v=u;
            det2.u=pq.top().par;
            res.push_back(det2);
        }
        status[u]=1;
        pq.pop();
        for(i=0; i<adj[u].size(); i++)
        {
            int v=adj[u][i].x;
            if(status[v]==0)
            {
                det.val=adj[u][i].val;
                printf("%d\n",det.val);
                det.x=v;
                det.par=u;
                pq.push(det);
            }
        }
    }
    return;
}

main()
{
    int e,i,f,t,v;
    cin>>n>>e;
    for(i=1; i<=e; i++)
    {
        cin>>f>>t>>v;
        det.x=t;
        det.val=v;
        adj[f].push_back(det);
        det.x=f;
        adj[t].push_back(det);
    }
    prims(1);
    for(i=0; i<res.size(); i++)
        cout<<res[i].u<<"->"<<res[i].v<<" "<<res[i].val<<endl;
    return 0;
}

/*
9 14
1 2 4
2 3 8
3 4 7
4 5 9
5 6 10
6 7 2
7 8 1
8 1 8
2 8 11
3 9 2
9 8 7
9 7 6
3 6 4
4 6 14
*/
