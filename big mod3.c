#include<queue>
#include<vector>
#define pi 2*acos(0)
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

ll BigMod(ll b,ll p,ll m);
int main()
{
    ll B,P,M,T,ans;
    while(scanf("%lld%lld%lld",&B,&P,&M)!=EOF)
    {
        ans=BigMod(B,P,M);
        printf("%lld\n",ans);
    }
    return 0;
}
ll BigMod(ll b,ll p,ll m)
{
    ll r;
    if(p==0)
        return 1;
    else if(p%2==0)
    {
        r=BigMod(b,p/2,m);
        return (r*r)%m;
    }
    else
        return ((b%m)*BigMod(b,p-1,m))%m;
}
