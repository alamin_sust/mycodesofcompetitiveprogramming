#include<iostream>
#include<cstring>
#include<cstdio>
using namespace std;

int n,dp[17][1<<17],arr[17][17];

int bitmask(int level,int mask)
{
    if(level>=n)
        return 0;
    if(dp[level][mask]!=-1)
        return dp[level][mask];
    for(int i=0;i<n;i++)
    {
        if((mask&(1<<i))==0)
        {
             dp[level][mask]=max(dp[level][mask],bitmask(level+1,(mask|(1<<i)))+arr[level][i]);
        }
    }
   return dp[level][mask];
}

main()
{
    int t,i,k,j;
    cin>>t;
    for(i=1;i<=t;i++)
    {
        memset(dp,-1,sizeof(dp));
        cin>>n;
        for(j=0;j<n;j++)
        {
            for(k=0;k<n;k++)
                cin>>arr[j][k];
        }
        printf("Case %d: %d\n",i,bitmask(0,0));
    }
    return 0;
}
