#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MM 200008

int inp[25],track[MM];

int solve(int sum,int n,int val)
{
    int i,j,k,l,inx;
    track[0]=1;
    for(i=1; i<=sum; i++)
    {
        track[i]=0;
    }
    for(i=0; i<n; i++)
    {
        for(j=sum; j>=0; j--)
        {
            if(track[j]==1)
            {
                inx=j+inp[i];
                if(track[inx]!=1 && inx<=sum)
                {
                    track[inx]=1;
                }
            }
        }
    }
    return track[val];
}

int main()
{
    int test,t,i,j,blen,nbar,sum,res;
    scanf("%d",&test);
    for(t=0; t<test; t++)
    {
        scanf("%d",&blen);
        scanf("%d",&nbar);
        sum=0;
        for(i=0; i<nbar; i++)
        {
            scanf("%d",&inp[i]);
            sum+=inp[i];
        }
        res=solve(sum,nbar,blen);
        if(blen>sum)
        printf("NO\n");
        else if(res)
        {
            printf("YES\n");
        }

        else
        {
            printf("NO\n");
        }
        memset(inp,0,sizeof(inp));
    }
    return 0;
}
