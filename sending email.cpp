#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#define pi 2*acos(0)
#define inf 999999999
#define eps 0.00000001
#define fr(i,n) for(i=0;i<n;i++)
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
using namespace std;
long long int d[20010],n;
vector<long long int>adj[20010];
vector<long long int>val[20010];
struct node
{
    long long int x;
    long long int value;
};
node det;
priority_queue<node>pq;
bool operator<(node a,node b)
{
    return a.value>b.value?true:false;
}

void dijkstra(long long int from)
{
    d[from]=0;
    det.x=from;
    det.value=0;
    pq.push(det);
    while(!pq.empty())
    {
        int small=pq.top().x;
        int sv=pq.top().value;
        pq.pop();
        int k=adj[small].size();
        //if(sv==d[small])
            for(int j=0; j<k; j++)
            {
                if(d[adj[small][j]]>(d[small]+val[small][j]))
                {
                    d[adj[small][j]]=d[small]+val[small][j];
                    det.x=adj[small][j];
                    det.value=d[adj[small][j]];
                    pq.push(det);
                }
            }
    }
    return;
}

main()
{
    long long int i,t,p,a,b,cost,from,to,m;
    cin>>t;
    for(p=1; p<=t; p++)
    {
        cin>>n>>m>>from>>to;
        for(i=0; i<n; i++)
        {
            d[i]=inf;
            val[i].clear();
            adj[i].clear();
        }
        for(i=0; i<m; i++)
        {
            scanf(" %lld %lld %lld",&a,&b,&cost);
            adj[a].push_back(b);
            val[a].push_back(cost);
            adj[b].push_back(a);
            val[b].push_back(cost);
        }
        dijkstra(from);
        if(d[to]>=inf)
            printf("Case #%lld: unreachable\n",p);
        else
            printf("Case #%lld: %lld\n",p,d[to]);
    }
    return 0;
}

