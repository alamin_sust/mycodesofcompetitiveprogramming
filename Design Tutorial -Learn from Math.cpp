/*Author : Md. Al- Amin
           20th batch
           Dept. of CSE, SUST*/
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#include<stack>
#include<map>
#include<set>
#include<queue>
#include<vector>
#define pi (2*acos(0))
#define SF scanf
#define SFd1(a) scanf("%d",&a)
#define SFd2(a,b) scanf("%d%d",&a,&b)
#define SFd3(a,b,c) scanf("%d%d%d",&a,&b,&c)
#define PF printf
#define inf 99999999
#define eps 0.00000001
#define ll long long
#define ull long long unsigned
#define fr(i,n) for(i=0;i<n;i++)
#define ms(dp,a) memset(dp,a,sizeof(dp))
#define dist(x1,y1,x2,y2) sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) )
#define PB push_back
#define MAX 1000002
using namespace std;
//int rr[]={1,2,-1,-2,1,2,-1,-2};
//int cc[]={2,1,2,1,-2,-1,-2,-1};

int sieve[MAX];

void sieve_(void)
{
    sieve[0]=sieve[1]=1;
    for(int i=2;i<=MAX;i++)
    {
        if(sieve[i]==0)
        {
            for(int j=2;i*j<=MAX;j++)
                sieve[i*j]=1;
        }
    }
    return;
}

main()
{
    int n,i,j;
    sieve_();
    cin>>n;
    i=4;
    j=n-4;
    while(i<=j)
    {
        if(sieve[i]&&sieve[j])
        {
            printf("%d %d\n",i,j);
            return 0;
        }
        i++;
        j--;
    }
    return 0;
}

